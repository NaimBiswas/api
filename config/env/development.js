var cwdPath = process.cwd();
module.exports = {
  //development Configuration here127.0.0.1
  // db : 'mongodb://localhost:27017/' + (process.env.DB_NAME || 'perfeqtaDB'),
  db: 'mongodb://mda-dps1462:B3p3rf3cta_x2y@52.24.57.147:27017/mda-dps1462',
  // db: 'mongodb://v31100:DevDanav@192.168.1.170:27017/v31100',
  // db: "mongodb://dev-clean-db:DevDanav@192.168.1.172:27017/dev-clean-db",
  serverName: "local",
  certpath: "cert.cer",
  keypath: "key.pem",
  staticURL: "erro.msg@mailinator.com",
  staticpath: cwdPath + '/tmp/',
  hbspath: cwdPath + '/lib/mailer/templates/',
  port: 3000,
  buildColumn: false,
  reportUrl:'http://localhost:4002',
  batchHeartBeat: 20 * 60 * 1000,
  notifyLogUrl: 'team@e.beperfeqta.com',
  notifyErrorEmail: 'team@e.beperfeqta.com',
  MandrillAPIKEY: 'dummy',
  //START :: PQT-2777 by Yamuna 
  logoutGap: 6,
  loginSnapshot: {
    host: "localhost",
    tableName: 'develop',
    database: 'loginSnapshots',
    userName: 'perfeqta',
    password: 'dummy'
  },
  alertMail: "miral@yopmail.com",
  logoutUserMail: 'miral.b@productivet.com, vaibhav.v@productivet.com',
  //END :: PQT-2777 by Yamuna
  vcscret: "ilivelifefreely",
  notifyLogUrl: 'yamuna@yopmail.com', //QC3-11389 by Yamuna
  apiProject: {
    version: "v1",
    secretKey: "KuchhBhi",
    options: {
      pageSize: 10,
      page: 1
    }
  },
  cryptoProtocols: {
    encryptDecryptOptions: {
      algorithm: "aes-256-cbc",
      secret: "DevilStorm"
    },
    tokenOptions: {
      algorithm: "RS256"
    }
  },
  oAppSecurityProtocols: {
    encryptDecryptOptions: {
      algorithm: "aes-256-cbc",
      secret: "3zTvzr3p67VC61jmV54rIYu1545x4TlY"
    },
    tokenOptions: {
      algorithm: "RS256"
    }
  },
  mail: {
    user: "perfeqtaqa",
    pass: "Perfeqta@123",
    host: "smtp.gmail.com",
    port: "587",
    from: "team@e.beperfeqta.com"
  },
  currentTimeZone: "CST",
  timeZones: {
    IST: -330,
    CST: 300
  },
  timeZonesP: {
    IST: "Asia/Kolkata",
    CST: "America/Chicago"
  },
  currentTimeZoneP: "CST",
  http: "http",
  envurl: "http://localhost:9000",
  apiEnvUrl: "http://localhost:3000",
  host: "dev.beperfeqta.com",
  basePath: "/dev/api/",
  linkExpiresInHours: 72,
  database: {
    host: "127.0.0.1",
    port: 3306,
    db: "qc3devlogs",
    dbtable: "loginfo",
    username: "root",
    password: "root@123",
    dbType: "mysql"
  },
  mq: {
    connection: 'localhost',
    accountMq: {
      host: "localhost",
      queue: "qc3dev"
    },
    scheduleMq: {
      host: "localhost",
      queue: "scheduleQue"
    },
    pluginMq: {
      host: "localhost",
      queue: "pluginchannels"
    },
    importSchema: {
      host: "localhost",
      queue: "importSchemaQue"
    },
    keys: ["perfeqta.logging"]
  },
  log: {
    level: "debug",
    mode: {
      console: true,
      disk: false,
      mq: false
    }
  },
  deploy: {
    port: 4901
  },
  fileServer: {
    url: {
      get: "http://192.168.1.188:3000/file/",
      post: "http://192.168.1.188:3000/file/"
    },
    enabled: false
  },
  exportFileServer: {
    url: {
      get: "http://localhost:3000/exportfile/",
      post: "http://localhost:3000/exportfile",
      postExpanded: {
        hostname: "localhost",
        port: 3000,
        path: "/exportfile",
        generateDocuments: "/generatedocuments",
        exportEntity: "/exportfile/entity"
      }
    },
    enabled: false
  },
  dmsproject: {
    dmsUrl: "https://dmsdemo.beperfeqta.com/REST",
    showDMS: true,
    Project: 1078,
    Customer: 1079,
    PERFEQTAAPP: 1146,
    PERFEQTAAPPVERSION: 1147,
    PERFEQTAAPPID: 1149
  },
  rabbitmqPluginConnectionUrl: "amqp://localhost",
  cacheOfbatchlockData: "mongo",
  redisServer: {
    host: "127.0.0.1",
    port: 6379,
    password: ""
  },
  tokenTimeOut:(10 * 60), // in minutes PQT-2075: saurabh

  /* sessionSecret: 'developmentSessionSecret',
    tokenSecret : 'jwtTokenSecret',
    port : 3000,
    url : "https://demoapi.youfantastic.com:5001",
    frontUrl : "https://demo.youfantastic.com",
    mail : {
        user : "Productivet Technologies, LLC",
        pass : "qSWeYTieVC_qd9fy0voPMg",
        host : 'smtp.mandrillapp.com',
        port : '587',
        from : 'team@e.youfantastic.com'
    },
    badgeSystem : {
        activeCron : true
    },
    product : "You Fantastic!",
    company : "Productive Technologies",
    support : 'support@youfantastic.com',
    mailconfirmationurl : "https://demo.youfantastic.com/user/mail/confirmation/token/",
    mailpassreseturl : "https://demo.youfantastic.com/#/passwordreset/",
    authExpireHours: 72 * 60 * 60 * 1000,
    maxFailMailSendAttempt : 5,
    failMailSendAttemptAfterEveryHour : 8 */
};
