// Mahammad - new batch entry structure

var collectionName = 'batchEntries';

db.getCollection(collectionName)
    .find({}, {
        batchEntries: 1,
        batchAttributesInfo: 1,
        schemaId: 1
    })
    .forEach(function (a) {
        if (a.batchEntries) {
            for (var batchNo in a.batchEntries) {

                print(batchNo);

                (a.batchEntries[batchNo] || []).forEach(function (record, index) {
                    var rec = record;
                    rec["batchNo"] = batchNo;
                    rec["sequenceNo"] = index;
                    rec["schemaId"] = a.schemaId;
                    rec["batchEntriesId"] = a._id;
                    db.getCollection('batchEntriesRecords').insert(rec);
                });

                if (a.batchAttributesInfo && a.batchAttributesInfo[batchNo]) {
                    var info = a.batchAttributesInfo[batchNo];
                    info["batchNo"] = batchNo;
                    info["schemaId"] = a.schemaId;
                    info["batchEntriesId"] = a._id;
                    db.getCollection('batchEntriesAttributes').insert(info);
                }
            }
        }
    });


// ======================================================================

// Mahammad - removing extra info from old structure

db.getCollection('batchEntries').update({}, {
    $unset: {
        batchAttributesInfo: '',
        batchEntries: ''
    }
}, {
    multi: true
});