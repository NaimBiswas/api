db.getCollection('types').update({
     "title" : "Auto Calculated"
},
{$push:{"formats": {
           "_id" : "123456789012345678901246",
           "title" : "Date",
           "metadata" : [ 
               {
                   "_id" : "123456789012345678901247",
                   "title" : "Date",
                   "name" : "datetime",
                   "isOptional" : true,
                   "dataType" : "strings",
                   "allowItems" : [ 
                       {
                           "_id" : "123456789012345678901248",
                           "title" : "DD/MM/YYYY"
                       }, 
                       {
                           "_id" : "123456789012345678901249",
                           "title" : "MM/DD/YYYY"
                       }, 
                       {
                           "_id" : "123456789012345678911250",
                           "title" : "YYYY/MM/DD"
                       }, 
                       {
                           "_id" : "123456789012345678901250",
                           "title" : "MM/DD/YY"
                       }, 
                       {
                           "_id" : "123456789012345678901251",
                           "title" : "DD/MMM/YY"
                       }, 
                       {
                           "_id" : "123456789012345678901252",
                           "title" : "MONTH/DD/YYYY"
                       }, 
                       {
                           "_id" : "123456789012345678901256",
                           "title" : "DD/MMM/YYYY"
                       }
                   ]
               }
           ]
       }}});
db.getCollection('types').update({
     "title" : "Auto Calculated"
},
{$push:{"formats": {
           "_id" : "123456789012333678901246",
           "title" : "Time",
           "metadata" : [ 
               {
                   "_id" : "123456789072345678901247",
                   "title" : "Time",
                   "name" : "datetime",
                   "isOptional" : true,
                   "dataType" : "strings",
                   "allowItems" : [ 
                       {
                           "_id" : "123456789012345678901257",
                           "title" : "HH:MM"
                       }, 
                       {
                           "_id" : "123456789012345678901258",
                           "title" : "HH:MM AM/PM"
                       }, 
                       {
                           "_id" : "123456789012345678901259",
                           "title" : "HH:MM:SS"
                       }, 
                       {
                           "_id" : "123456789012345678901262",
                           "title" : "HH:MM:SS AM/PM"
                       }
                   ]
               }
           ]
       }}});
