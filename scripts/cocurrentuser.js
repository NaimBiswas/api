/**
* @name concurrentuser
* @author Surjeet Bhadauriya <surjeet.b@productivet.com>
*
* @version 0.0.0
*/

// ************************************INFO****************************************
// *                                                                              *
// *                       RUN AT THE TIME OF FRESH DEPLOYMENT                    *
// *                                                                              *
// ********************************************************************************

// 1. Check the number of logged in users who are in Active mode(optional)
db.getCollection('users').find({isConCurrentUserActive: true, isActive: true})
//

// 2. Check the number of logged in users(optional)
db.getCollection('users').find({isConCurrentUserActive: true})
//

// 3. Clear the logged in user pool(mandatory)
db.getCollection('users').update({}, {$set: {'isConCurrentUserActive': false}}, {multi: true})
