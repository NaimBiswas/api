-------------------- Added by Miral ---------------------------


//QC3-9296 - to add schemaCompletedDate field
//Skipp option add in foles and permission's general
db.resources.insert({
    "title" : "Skip Schedule",
    "entity" : "skipSchedule",
    "isActive" : true
 })

 //add allow to skip option for old schedules
 db.schedules.update(
    {'allowToSkip': {$exists : false}},
    {$set: {'allowToSkip': true}},
    {multi: true}
)
