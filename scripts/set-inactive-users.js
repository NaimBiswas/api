/**
* @name set-inactive-users
* @author Lokesh Boran <lokesh.b@productivet.com>
*
* @version generic
* @createdDate 2017-08-18
*
* @description This script is responsible to update users collection isActive field to false.
*/

var count = db.usersbackup.count();
if(count==0){
  db.users.copyTo('usersbackup');
}
//replace 'mda' to your comma seprated string array, which is username.
db.getCollection('users').update({username:{$nin:['mda']}},{$set:{isActive: false}},{multi:true});
