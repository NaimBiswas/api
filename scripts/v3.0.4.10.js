db.getCollection('thresholds')
     .find({
         'conditions.leftOperand.type.format.title': 'Date'
     })
     .forEach(function(threshold) {
         if (threshold) {
             (threshold.conditions || []).forEach(function(cond) {
                 if (cond.leftOperand && cond.leftOperand.type && cond.leftOperand.type.format && cond.leftOperand.type.format.title === 'Date') {
                     cond['dateSelection'] = "Specific Date";
                 }
             });
             if (threshold.conditions.length) {

                 db.getCollection('thresholds').update({
                     _id: threshold._id
                 }, {
                     $set: {
                         "conditions": threshold.conditions
                     }
                 });
             }
         }
     });
