var finalObj = [];
db.questions.find().forEach(function(item){
          if(typeof item.validation != "undefined" && typeof item.validation.validationSet != "undefined" && typeof item.validation.validationSet.existingSet != "undefined"){
              var obj = {
                     linkedToId : (item._id).valueOf(),
                     linkedToType : 'questions',
                     linkedToName : item.title,
                     linkedToVersion : item.version,
                     linkedToStatus : item.isActive,
                     linkedOfType : 'sets',
                     linkedOfId : item.validation.validationSet.existingSet._id,
                     linkedOfName : item.validation.validationSet.existingSet.title,
                     linkedOfVersion : item.validation.validationSet.existingSet.version
                  }
                  finalObj.push(obj);
          }     
      })
print(finalObj);
db.linkedTbl.insertMany(finalObj);