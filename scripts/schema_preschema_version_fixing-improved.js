db.getCollection('schema').find({}, {version: 1, isActive: 1}).forEach(function (schema) {
    db.getCollection('preSchema').find({_id: schema._id}, {version: 1, isActive: 1})
        .forEach(function (pschema) {
            
            if (pschema.version > schema.version) {
                 print('schemaAuditLogs/schema update in id: ' + schema._id);
                    db.preSchemaLogs.find({
                        version: {$gt: schema.version, $lte: pschema.version}, identity: schema._id,
                        $where: function(){
                             return this.version % 1 == 0;
                            }
                        }).forEach(function (x) {
                                    print('schemaLogs update in id: ' + x._id);
                                    db.schemaLogs.update({_id: x._id},x, {upsert: true});
                                });
                        
                    db.preSchemaAuditLogs.find({
                        version: {$gt: schema.version, $lte: pschema.version}, identity: schema._id,
                        $where: function(){
                             return this.version % 1 == 0;
                            }
                        }).forEach(function (x) {
                                    db.schemaAuditLogs.update({_id: x._id},x, {upsert: true});
                                    delete x._id;
                                    x._id = x.identity;
                                    delete identity;
                                            
                                    db.schema.update({_id: x._id},x, {upsert: true});
                                    print('schemaAuditLogs/schema update for version: ' + x.version + ', id: '+ x._id);
                                });    
            }
          
            
        });
    });
    
