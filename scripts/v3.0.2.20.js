db.getCollection('schema').find({}).forEach(function(schm){
    if(schm && schm.attributes && schm.attributes.length > 0){
        schm.attributes.forEach(function(atr,aidx){
            if(atr && atr.validation && atr.validation.validationSet && atr.validation.validationSet.existingSet && typeof atr.validation.validationSet.existingSet === 'string'){
               var eset = db.getCollection('sets').find({title : atr.validation.validationSet.existingSet}).toArray();
                if(eset.length > 0){
                    atr.validation.validationSet.existingSet = eset[0];
                }
                else{
                    printjson("title : "+ schm.title + " , attribute Index :: " + aidx + " set name ::" + atr.validation.validationSet.existingSet);
                    printjson("OMG not Found");
                }
			}
        });
        db.schema.updateOne({_id : schm._id},{$set : {'attributes' : schm.attributes}});
    }
});



db.attributes.find({}).forEach(function(atr){
    if(atr && atr.validation && atr.validation.validationSet && atr.validation.validationSet.existingSet &&  typeof atr.validation.validationSet.existingSet === 'string'){
        var eset = db.getCollection('sets').find({title : atr.validation.validationSet.existingSet}).toArray();
        if(eset && eset.length > 0){
          atr.validation.validationSet.existingSet = eset[0];
          db.attributes.updateOne({_id:atr._id}, {$set:{"validation": atr.validation}});
        }
		else{
            printjson("title : "+ atr.title + ", set name :: " + atr.validation.validationSet.existingSet);
            printjson("OMG not Found");
		}
    }
})
