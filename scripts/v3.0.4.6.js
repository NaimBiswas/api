-----------------  Date : 06june2017 -------------------------------------------------------------------

db.users.find({}).forEach(function (usr) {
var isToUpdate = false;
if (usr && usr.roles && usr.roles.length > 0) {
//check if role is present in DB.
var roleCopy = [];
roleCopy = Array.from(usr.roles);
roleCopy.forEach(function (rl, idx) {
if (rl && rl._id) {
var objId = ObjectId(rl._id);
var roleInDb = db.roles.find(objId).toArray();
if (roleInDb && roleInDb.length == 0) {
isToUpdate = true;
usr.roles.splice(idx, 1);
}
}
});
var dispRoleName = [];
//update displayRole
usr.roles.forEach(function (rl) {
if (rl && rl.title) {
dispRoleName.push(rl.title);
}
});
usr.displayroles = dispRoleName.join(', ');
}
if (isToUpdate) {
db.users.updateOne({ _id: usr._id }, { $set: { displayroles: usr.displayroles, roles: usr.roles } });
}
});



---------------------------------------------------------------------------

db.roles.find({}).forEach(function(rl){ 
printjson(" ===================" + rl.title + " ==================="); 
var isUpdated = false; 
if(rl && rl.modules && rl.modules.length){ 
rl.modules.forEach(function(mdl){ 
printjson(" ===================" + mdl._id + " ==================="); 
if(mdl && mdl.permissions && mdl.permissions.length){ 
var tempArr = mdl.permissions; 
//checking from behind so that splice does not have problem. 
for(var i = (tempArr.length - 1); i >= 0; i--){ 
if(tempArr[i] && tempArr[i].entity && tempArr[i].entityType === 'schemas'){ 
//check for valid Id and modules 
var appId = tempArr[i].entity; 
printjson(tempArr[i]); 
if(!tempArr[i].entity){ 
printjson("Entity Not Found"); 
//remove this as this is not containging app id. 
mdl.permissions.splice(i,1); 
isUpdated = true; 
}else{ 
if(typeof tempArr[i].entity === 'string' && tempArr[i].entity.length === 24){ 
appId = ObjectId(tempArr[i].entity); 
} 
var app = db.schema.findOne({_id : appId},{module : 1}); 
if(app && app.module && app.module._id){ 
//check for this with current module if matched fine else delete it. 
if(app.module._id != tempArr[i].entity){ 
printjson(app.module._id + " !== "+ tempArr[i].entity); 
printjson("APP Model and per not found"); 
mdl.permissions.splice(i,1); 
isUpdated = true; 
} 
} 
} 
} 
} 
} 
}); 
} 
if(isUpdated){ 
printjson(rl.title); 
db.roles.update({_id : rl._id},{$set : {modules : rl.modules}}); 
} 
});



