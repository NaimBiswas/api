/**
* @name updateCreatedActivityInfoDateFromSchema-v300420E
* @author Lokesh Boran <lokesh.b@productivet.com>
*
* @version v300420E
* @createdDate 2017-07-18
*
* @description This script is responsible to update qcentryactivity collection createdDate field on status 'Created' records. createdDate is updated from the respective entityRecord.
*/

var schemas = db.getCollection('schema').find({}, {
  title: 1
}).toArray();
var i = 0;
var schemasLength = schemas.length;

function loadSchemaRecords() {
  if (schemasLength < i || !schemas[i]) {
    return;
  }

  var schemaId = schemas[i]._id.valueOf();
  var schemaRecords = db.getCollection(schemaId).find({}, {
    createdDate: 1
  }).toArray();

  for (var j = 0; j < schemaRecords.length; j++) {
    var recordId = schemaRecords[j]._id.valueOf();
    var recordCreatedDate = schemaRecords[j].createdDate;


    var qcentries = db.getCollection('qcentryactivity').find({
      "qcformId": schemaId,
      "qcentryId": recordId,
      status: 'Created'
    }, {
      createdDate: 1,
      status: 1
    }).sort({
      createdDate: -1
    }).toArray();

    if (qcentries[0] && recordCreatedDate != qcentries[0].createdDate) {
      var id = qcentries[0]._id;
      db.qcentryactivity.update({
        _id: id
      }, {
        $set: {
          createdDate: recordCreatedDate
        }
      });
    }
  }

  i = i + 1;

  loadSchemaRecords(i);
}

loadSchemaRecords(i);
