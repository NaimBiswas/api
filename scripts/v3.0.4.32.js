//Improvement: QC3-3458

db.getCollection('schema').find({},{procedures:1}).forEach(function(app){
    if(app.procedures && app.procedures.length > 0){
      app.procedures.forEach(function(proc){
        if(proc.title.endsWith('(Con)')){
          if(!proc.conditional){
            proc.conditional = true;
          }
        }
        if(proc.conditionalWorkflows && proc.conditionalWorkflows.length > 0){
          proc.conditionalWorkflows.forEach(function(workflow){
            if (workflow.procedureToExpand && !workflow.procedureToExpand.conditional){
              workflow.procedureToExpand.conditional = true;
            }
          });
        }
      });
      //Update Goes Here
      db.schema.updateOne({_id:app._id}, {$set:{procedures : app.procedures}});
    }
  });

  // ----------------------------------------------------------------------------------------------------------

  db.getCollection('schemaAuditLogs').find({},{procedures:1,_id:1}).forEach(function(app){
    if(app.procedures && app.procedures.length > 0){
      app.procedures.forEach(function(proc){
        if(proc.title.endsWith('(Con)')){
          if(!proc.conditional){
            proc.conditional = true;
          }
        }
        if(proc.conditionalWorkflows && proc.conditionalWorkflows.length > 0){
          proc.conditionalWorkflows.forEach(function(workflow){
            if (workflow.procedureToExpand && !workflow.procedureToExpand.conditional){
              workflow.procedureToExpand.conditional = true;
            }
          });
        }
      });
      //Update Goes Here
      db.schemaAuditLogs.updateOne({_id:app._id}, {$set:{procedures : app.procedures}});
    }
  });
