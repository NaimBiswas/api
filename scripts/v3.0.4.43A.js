----------------- Added by Prachi ---------------

db.getCollection("attributes").find({'type.format.title':'Date'},{type:1,title:1}).forEach(function(attrib){
    if(!attrib.type.format.metadata.datetime){
        attrib.type.format.metadata.datetime = "123456789012345678901249";
        db.getCollection("attributes").update({ "_id": attrib._id },{ "$set": attrib});
        print('Updated Successfully');
    }       
});


db.getCollection("schema").find({},{type:1,title:1,attributes:1}).forEach(function(app){
   app.attributes.forEach(function(attrib){
    if(attrib.type && attrib.type.format && attrib.type.format.title == 'Date'
        && !attrib.type.format.metadata.datetime){
       attrib.type.format.metadata.datetime = "123456789012345678901249";
       db.getCollection("schema").updateOne({ "_id": app._id },{ "$set": {attributes: app.attributes}});
       print('Updated Successfully');
    }  
  });    
 });


db.getCollection("schemaAuditLogs").find({},{type:1,title:1,attributes:1}).forEach(function(app){
   app.attributes.forEach(function(attrib){
    if(attrib.type && attrib.type.format && attrib.type.format.title == 'Date'
        && !attrib.type.format.metadata.datetime){
       attrib.type.format.metadata.datetime = "123456789012345678901249";
       db.getCollection("schemaAuditLogs").updateOne({ "_id": app._id },{ "$set": {attributes: app.attributes}});
       print('Updated Successfully');
    }  
  });    
 });
