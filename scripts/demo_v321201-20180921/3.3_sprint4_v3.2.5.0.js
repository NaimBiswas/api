//PQT-162 & PQT-628 & PQT-629

/**
 * @author Yamuna Sardhara <yamuna.s@productivet.com>
 * @description Add preSchema resourse to resourses collection
 * @version 1.0 
 */
var isPreSchemaAvailable = db.getCollection('resources').findOne({
    title: "PreSchema"
});
if (!isPreSchemaAvailable) {
    db.getCollection('resources').insert({
        "title": "PreSchema",
        "entity": "preSchema",
        "isActive": true
    });
}

/**
 * @author Yamuna Sardhara <yamuna.s@productivet.com>
 * @description To add default collection called preSchema into permissions of roles 
 * @version 1.0 
 */
var isPreSchemaAvailable = db.getCollection('resources').findOne({
    title: "PreSchema"
});
if (isPreSchemaAvailable) {
    db.getCollection('roles').find({}).forEach(function (role) {
        if (role && role.modules) {
            role.modules.forEach(function (roleModule) {
                if (roleModule && roleModule.permissions && !roleModule.permissions.find(function (rolePerm) {
                        return rolePerm._id == isPreSchemaAvailable._id
                    })) {
                    var schemaPermission = roleModule.permissions.find(function (rolePerm) {
                        return rolePerm.entity == 'schema'
                    });
                    roleModule.permissions.push({
                        "_id": isPreSchemaAvailable._id,
                        "title": isPreSchemaAvailable.title,
                        "entity": isPreSchemaAvailable.entity,
                        "view": schemaPermission ? schemaPermission.view : false,
                        "create": schemaPermission ? schemaPermission.create : false,
                        "edit": schemaPermission ? schemaPermission.edit : false,
                    });
                }
            })
            db.getCollection('roles').updateOne({
                "_id": role._id
            }, {
                $set: {
                    "modules": role.modules
                }
            });
            printjson('Update in ' + role._id);
        }
    });
}

/**
 * @author Yamuna Sardhara <yamuna.s@productivet.com>
 * @description add properties to preSchema and Schema
 * @version 1.0 
 */

db.getCollection('schema').find({}).forEach(function (schema) {
    if (schema.version % 1 === 0) {
        if (!schema.hasOwnProperty('isDraft')) {
            db.getCollection('schema').updateOne({
                "_id": schema._id
            }, {
                $set: {
                    "isDraft": false
                }
            });
            printjson('Update in ' + schema._id);
        }
    }

});

//Run http://localhost:3000/schemaToMajorVersion (hostname/schemaToMajorVersion)
//After 10 minutes run belowed scripts

/**
 * @author Yamuna Sardhara <yamuna.s@productivet.com>
 * @description copy schema collection to preSchema
 * @version 1.0 
 */

db.schema.find().forEach(function (x) {
    db.preSchema.insert(x);
});
db.schemaAuditLogs.find().forEach(function (x) {
    db.preSchemaAuditLogs.insert(x);
});
db.schemaLogs.find().forEach(function (x) {
    db.preSchemaLogs.insert(x);
});
