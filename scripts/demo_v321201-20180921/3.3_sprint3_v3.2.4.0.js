
//PQT-171 which om 3.2.3.0

/**
* @author  Mahammad Chharchhodawala <mahammad.c@productivet.com>
* @description set apps object of search favourites as per new schema of apps in Advance Search. (PQT-171)
* @version 1.0.0
*/
db.searchFavorites.insertMany(
    [{
            "search": {
                "qcform": "",
                "searchByRadio": "quickdate",
                "month": "",
                "year": "",
                "date": "2018-07-04T11:04:51.623Z",
                "attribute": [
                    null
                ],
                "attributeValue": [
                    ""
                ],
                "attributedetails": [],
                "batchEntryId": "",
                "startDate": "",
                "endDate": "",
                "sitelevel": [],
                "status": [],
                "searchByDrop": "0",
                "searchCriteria": "apps",
                "apps": [],
                "assignee": [],
                "module": [],
                "role": [],
                "commands": [
                    "checkAll"
                ]
            },
            "type": {
                "advancedQuickSearch": true
            },
            "searchCriteria": "apps",
            "title": "Records You Created Today",
            "createdBy": ObjectId("5a716ff323f24c561c01c91f"),
            "modifiedBy": ObjectId("5a716ff323f24c561c01c91f"),
            "timeZone": "Asia/Kolkata",
            "createdByName": "Ravan Lunkesh (ravan)",
            "modifiedByName": "Ravan Lunkesh (ravan)",
            "createdDate": ISODate("2018-07-03T10:00:45.860Z"),
            "modifiedDate": ISODate("2018-07-04T11:07:46.132Z"),
            "isMajorVersion": true,
            "version": 1,
            "versions": [{
                    "version": 1,
                    "record": ObjectId("5b3b494d50df6f3f56cbc58b")
                }
            ]
        },
        {
            "search": {
                "qcform": "",
                "searchByRadio": "quickdate",
                "month": "",
                "year": "",
                "date": "2018-07-04T11:04:51.623Z",
                "attribute": [
                    null
                ],
                "attributeValue": [
                    ""
                ],
                "attributedetails": [],
                "batchEntryId": "",
                "startDate": "",
                "endDate": "",
                "sitelevel": [],
                "status": [],
                "searchByDrop": "0",
                "searchCriteria": "apps",
                "apps": [],
                "assignee": [],
                "module": [],
                "role": [],
                "commands": [
                    "checkAll",
                    "accessedRecords"
                ]
            },
            "type": {
                "advancedQuickSearch": true
            },
            "searchCriteria": "apps",
            "title": "Records You Accessed Today",
            "createdBy": ObjectId("5a716ff323f24c561c01c91f"),
            "modifiedBy": ObjectId("5a716ff323f24c561c01c91f"),
            "timeZone": "Asia/Kolkata",
            "createdByName": "Ravan Lunkesh (ravan)",
            "modifiedByName": "Ravan Lunkesh (ravan)",
            "createdDate": ISODate("2018-07-03T10:00:45.860Z"),
            "modifiedDate": ISODate("2018-07-04T11:07:46.132Z"),
            "isMajorVersion": true,
            "version": 1,
            "versions": [{
                    "version": 1,
                    "record": ObjectId("5b3b494d50df6f3f56cbc58b")
                }
            ]
        },
        {
            "search": {
                "qcform": "",
                "searchByRadio": "quickdate",
                "month": "",
                "year": "",
                "date": "2018-07-04T11:04:51.623Z",
                "attribute": [
                    null
                ],
                "attributeValue": [
                    ""
                ],
                "attributedetails": [],
                "batchEntryId": "",
                "startDate": "",
                "endDate": "",
                "sitelevel": [],
                "status": [],
                "searchByDrop": "0",
                "searchCriteria": "apps",
                "apps": [],
                "assignee": [],
                "module": [],
                "role": [],
                "commands": [
                    "checkAll",
                    "reviewPending"
                ]
            },
            "type": {
                "advancedQuickSearch": true
            },
            "searchCriteria": "apps",
            "title": "Records Pending Your Review",
            "createdBy": ObjectId("5a716ff323f24c561c01c91f"),
            "modifiedBy": ObjectId("5a716ff323f24c561c01c91f"),
            "timeZone": "Asia/Kolkata",
            "createdByName": "Ravan Lunkesh (ravan)",
            "modifiedByName": "Ravan Lunkesh (ravan)",
            "createdDate": ISODate("2018-07-03T10:00:45.860Z"),
            "modifiedDate": ISODate("2018-07-04T11:07:46.132Z"),
            "isMajorVersion": true,
            "version": 1,
            "versions": [{
                    "version": 1,
                    "record": ObjectId("5b3b494d50df6f3f56cbc58b")
                }
            ]
        },
        {
            "search": {
                "qcform": "",
                "searchByRadio": "quickdate",
                "month": "",
                "year": "",
                "date": "",
                "attribute": [],
                "attributeValue": [],
                "attributedetails": [],
                "batchEntryId": "",
                "startDate": "",
                "endDate": "",
                "sitelevel": [],
                "status": [],
                "searchByDrop": "",
                "searchCriteria": "apps",
                "apps": [],
                "assignee": [],
                "module": [],
                "role": [],
                "commands": [
                    "checkAll",
                    "assignToMe"
                ]
            },
            "type": {
                "advancedQuickSearch": true
            },
            "searchCriteria": "apps",
            "title": "Records Assigned To You",
            "createdBy": ObjectId("5a716ff323f24c561c01c91f"),
            "modifiedBy": ObjectId("5a716ff323f24c561c01c91f"),
            "timeZone": "Asia/Kolkata",
            "createdByName": "Ravan Lunkesh (ravan)",
            "modifiedByName": "Ravan Lunkesh (ravan)",
            "createdDate": ISODate("2018-07-03T11:36:19.594Z"),
            "modifiedDate": ISODate("2018-07-03T11:36:19.594Z"),
            "isMajorVersion": true,
            "version": 1,
            "versions": [{
                "version": 1,
                "record": ObjectId("5b3b5fb350df6f3f56cbc6c7")
            }]
        }
    ]);


//PQT-171

/**
* @author Yamuna Sardhara <yamuns.s@productivet.com>
* @description set apps object of search favourites as per new schema of apps in Advance Search. (PQT-171)
* @version 1.0.0
*/

var schemas = db.schema.find().toArray();
var masterSchemas = db.masterqcsettings.find().toArray();
 db.getCollection('searchFavorites').find({'isEntity':false, isWindow:false},{'search':1,'searchCriteria':1}).toArray().forEach(function (favourite){
      var appIds = [];
      var schema = '';
      var searchAppObj = {};
      if(favourite && favourite.search && favourite.search.apps && favourite.search.apps.length){
          favourite.search.apps = favourite.search.apps.map(function(appId){
              if(favourite.searchCriteria == 'apps'){
                  schema = schemas.find(function(schema){return schema._id == appId.id});
              }else{
                  schema = masterSchemas.find(function(schema){return schema._id == appId.id});
              }
          if (schema){
           searchAppObj = {
              "id" : appId.id,
              "caption" : schema.title,
              "title" : schema.title + (schema.isActive ? '' : ' (Inactive)'),
              "signature" : favourite.searchCriteria == 'apps' ? "App(s)" : "Master App(s)",
              "type" : favourite.searchCriteria,
              "module" : schema.module,
              "isActive" : schema.isActive
           }
          }

          return searchAppObj;
      });
       db.getCollection('searchFavorites').updateOne({
           "_id": favourite._id
       }, {
          $set: {
            "search.apps": favourite.search.apps
          }
         });
      printjson('Update in ' + favourite._id);
      }
  });