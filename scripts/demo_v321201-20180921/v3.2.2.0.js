
// Added by Yamuna for QC3-11210
db.calendar.find({'masterQCSettingsId':{$exists:true}}).forEach(function(e){
    if(!e.masterQCSchemaIds){
        var masterApp = db.masterqcsettings.findOne({_id:ObjectId(e.masterQCSettingsId)});
        e.masterQCSchemaIds = masterApp.schema;
        db.getCollection('calendar').update({},{"$set":{"masterQCSchemaIds":e.masterQCSchemaIds}},{multi : true});
    }
});
