//Do not use robomongo for following scripts (Use commandline)

//PQT-4 & PQT-15

/**
* @author Jyotil Raval <jyotil.r@productivet.com>
* @description Converting object of assignee to array of object. (PQT-4)
* @version 3.3
*/

(function () {
  var users = db.getCollection('users').find({},{'roles':1}).toArray();
  db.getCollection('assignment').find().toArray().forEach(function (assignments) {

    var hasAssigneeObject = false;
    assignments.assignment.forEach(function (assign) {
      if (!Array.isArray(assign.assignee)) {
        hasAssigneeObject = true;
        var tempAssignee = assign.assignee;
        assign.assignee = [];
        assign.assignee.push(tempAssignee);
        assign.roles = users.find(function (user) {
          return user._id.valueOf() == tempAssignee.id;
        }).roles.map(function (role) {
          return {
            id: role._id,
            title: role.title
          };
        });
      }
    });
    if(hasAssigneeObject){
      db.getCollection('assignment').updateOne({
        "_id": assignments._id
      }, {
        $set: {
          "assignment": assignments.assignment
        }
      });
      printjson('Update in ' + assignments._id);
    }
  });
  var schemaIds = db.getCollection('schema').find({},{'_id':1}).toArray().map(function (schema){
    return schema._id.valueOf();
  });
  schemaIds.forEach(function (schemaId){
    var entries = db.getCollection(schemaId).find({"assignment" : {$exists: true}},{'assignment':1}).toArray();
    entries.forEach(function (entry){
      var hasAssigneeObject = false;
      if (!Array.isArray(entry.assignment.assignee)) {

        hasAssigneeObject = true;
        var tempAssign = entry.assignment.assignee;
        entry.assignment.assignee = [];
        entry.assignment.assignee.push(tempAssign);
        entry.assignment.roles = users.find(function (usr){
          return usr._id.valueOf() == tempAssign.id;
        }).roles.map(function (role){
          return {
            'id' : role._id,
            'title' : role.title
          }
        });
      }
      if(hasAssigneeObject){
        db.getCollection(schemaId).updateOne({
          "_id": entry._id
        }, {
          $set: {
            "assignment": entry.assignment
          }
        });
        printjson('Update in SchemaId: ' + schemaId + ' - EntryId: ' + entry._id);
      }
    });
  });
})();

/**
* @author Prachi Thakkar <prachi.t@productivet.com>
* @description Update workflowreview array in all the app records.
* @version 3.3
*/

(function() {
  var schemaIds = db.getCollection('schema').find({},{'_id':1}).toArray().map(function (schema){
    return schema._id.valueOf();
  });
  schemaIds.forEach(function (schemaId){
    var entries = db.getCollection(schemaId).find({"workflowreview" : {$exists: true}},{'workflowreview':1, 'isRejected':1}).toArray();
    entries.forEach(function (entry){
      var isUpdate = false;
      entry.workflowreview = entry.workflowreview.reduce(function(c, v){
        if((v.reviewstatus == '1' || v.reviewstatus == '2') && !v.reviews){
          isUpdate = true;
          v.reviews = [];
        }else if(v.reviewstatus == '0' && v.reviewedUserId && !v.reviews){
          isUpdate = true;
          v.reviews = [];
          v.reviews.push({
            "userId" : v.reviewedUserId,
            "fullName" : v.revieweduser,
            "date" : v.verificationDate,
            "comment" : v.comment,
            "status" : NumberInt(0)
          });
        }
        if (v.anyOne == undefined) {
          isUpdate = true;
          v.anyOne = true;
        }
        c.push(v);
        return c;
      }, []);
      if (isUpdate) {
        db.getCollection(schemaId).updateOne({"_id": entry._id},{$set: {"workflowreview": entry.workflowreview}});
        print('Review Updated Successfully!');
      }
    });
  });
})();


/**
* @author Surjeet Singh Bhadauriya <surjeet.b@productivet.com>
* @description Update workflowreview array in all the app.
* @version 1.0.0
*/

(function() {
  db.getCollection('schema').find({},{'_id': 1, 'workflowreview': 1}).forEach(function (schema){
    var isUpdate = false;
    if (schema.workflowreview && schema.workflowreview.length) {
      schema.workflowreview = schema.workflowreview.map(function (workflowreview) {
        if (workflowreview.anyOne == undefined) {
          workflowreview.anyOne = NumberInt(1);
          isUpdate = true;
        }
        return workflowreview;
      });
      if (isUpdate) {
        db.getCollection('schema').update({"_id": schema._id},{$set: {"workflowreview": schema.workflowreview}});
        print(schema._id, 'updated Successfully!');
      }
    }
  });
})();


/**
* @author Surjeet Singh Bhadauriya <surjeet.b@productivet.com>
* @description PQT-3: Update all user with key assignmentEmailNotification.. this is only one time script
* @version 1.0.0
*/

// db.getCollection('users').update({assignmentEmailNotification: {$exists: false}}, {$set: {assignmentEmailNotification: true}}, {multi: true})

//The below script can be modified to one line which is shown in the above commented line.
//But becuase it's necessary to show the logs that's why I made the below script.

var updatedUserCount = 0;
var totalUsersCount = db.getCollection('users').count({})
print('Number of users to be update: ', totalUsersCount);
print('Updating...');

db.getCollection('users').find({}).forEach(function(user){
  if(user.assignmentEmailNotification == undefined){
    user.assignmentEmailNotification = true;
    db.getCollection('users').update({_id: user._id}, {$set: {assignmentEmailNotification: user.assignmentEmailNotification}})
    print(user._id, 'updated');
    updatedUserCount++;
  }
});

print('Total number of users', totalUsersCount);
print('Number of updated users', updatedUserCount);
if(updatedUserCount == totalUsersCount){
  print('Success: All the users has been updated successfully.');
}else{
  print('Failed: Total number of users != Number of updated users');
}

/**
* @author Vivek Thummar <vivek.t@productivet.com>
* @description Complaince workflow flag false by default.
* @version 1.0.0
*/

db.getCollection('schema').update({isLockReviewWorkflow: {$exists:0}},{$set : {isLockReviewWorkflow : true}}, {multi:true})
