//Do not use robomongo for following scripts (Use commandline)

//PQT-20

/**
* @author Yamuna Sardhara <yamuns.s@productivet.com>
* @description Add isrejected flag in all entries which are not rejected yet. (PQT-20)
* @version 1.0.0
*/

(function() {
var schemaIds = db.getCollection('schema').find({},{'_id':1}).toArray().map(function (schema){
  return schema._id.valueOf();
});
schemaIds.forEach(function (schemaId){
  var entries = db.getCollection(schemaId).find().toArray();
  entries.forEach(function (entry){
      db.getCollection(schemaId).update({"isRejected" : {$exists: false}}, {$set: {"isRejected": false}},{new: true});
      print('Entries Updated Successfully!');
});
});
});
