//===================================================================================================================================

//QC3-9701 - to remove duplicate record

var duplicates = [];
db.reviewpendingworkflow.aggregate([
    {
        $group: {
            _id: {
                entryId: "$entryId", schemaId: "$schemaId",
                masterQCSettingsId: "$masterQCSettings.masterQCSettingsId",
                sequenceId: "$masterQCSettings.sequenceId",
            },
            dups: { "$addToSet": "$_id" },
            count: { "$sum": 1 }
        }
    },
    {
        $match: {
            count: { "$gt": 1 }
        }
    }
],
    { allowDiskUse: true }
)
    .forEach(function (doc) {
        doc.dups.shift();
        doc.dups.forEach(function (dupId) {
            duplicates.push(dupId);
        }
        )
    });

printjson(duplicates);

db.reviewpendingworkflow.remove({ _id: { $in: duplicates } });

//===================================================================================================================================

//QC3-9701 - to remove duplicate record

print('Following entries will be removed. (schemaId/entryId)');
db.getCollection('reviewpendingworkflow').find({}).forEach(function(obj){
        var entry = db[obj.schemaId].findOne({_id:obj.entryId},{workflowreview:1});
        if(entry && entry.workflowreview && entry.workflowreview.length && (entry.workflowreview||[]).every(function (review) { return review.reviewstatus == 0;})){
                print(obj.schemaId+'/'+obj.entryId.valueOf());
                db.reviewpendingworkflow.remove(obj._id);
        }
})

//===================================================================================================================================

//QC3-9296 - to remove entry obj from calendar if void the record

db.getCollection('calendar').find({ 'ismasterQcForm': false, 'entry': { $exists: 1 } }).forEach(function (obj) {
    if (db[obj.schema._id.valueOf()].findOne({ '_id': obj.entry.entryId, 'entries.status.status': 3, 'calendarIds.0': { $exists: 1 }, 'calendarverifieduser.0': { $exists: 1 } })) {
        db[obj.schema._id.valueOf()].update({ _id: obj.entry.entryId }, { $set: { calendarIds: [], calendarverifieduser: [] } });
        db.getCollection('calendar').update({ _id: obj._id }, { $unset: { 'entry': 1 } });
    }
})

//===================================================================================================================================

//QC3-9296 - to remove entry obj from calendar if void all the record for master app

db.getCollection('calendar').find({ 'ismasterQcForm': true, 'entry': { $exists: true } }).forEach(function (obj) {
    var counter = 0;
    obj.entry.masterschemaIds.forEach(function (item) {
        if (db[item].findOne({ 'calendarIds.0': obj._id.valueOf(), 'entries.status.status': 3, 'calendarIds.0': { $exists: 1 }, 'calendarverifieduser.0': { $exists: 1 } })) {
            counter++;
        }
    });
    if (obj.entry.masterschemaIds.length == counter) {
        obj.entry.masterschemaIds.forEach(function (item) {
            db[item].update({ 'calendarIds.0': obj._id.valueOf() }, { $set: { calendarIds: [], calendarverifieduser: [] } });
        });
        db.getCollection('calendar').update({ _id: obj._id }, { $unset: { 'entry': 1 } });
    }
});

//===================================================================================================================================

//QC3-9296 - to add schedulestatus field in old records for single app

db.getCollection('calendar').find({ismasterQcForm:false,entry:{$exists:true},'entry.schedulestatus':{$exists:false}}).forEach(function(obj){

  var scheduleStatus = ' ';
       if(obj.entry.recordStatus == 4){
           scheduleStatus = "Incomplete";
       }else if(obj.entry.recordStatus == 0 || obj.entry.recordStatus == 1){
           scheduleStatus = "Completed";
       }

  db.getCollection('calendar').update({_id:obj._id},{$set:{'entry.scheduleStatus':scheduleStatus}});
})


//===================================================================================================================================

//QC3-9296 - to add schedulestatus field in old records for master app

db.getCollection('calendar').find({ismasterQcForm:true,entry:{$exists:true},'entry.schedulestatus': 'Pending'}).forEach(function(obj){

  var master = db.getCollection('masterqcsettings').findOne({_id:obj.master._id});

  if(obj.entry.masterschemaIds.length == master.schema.length){
    var scheduleStatus = "Completed";
  }else{
    var scheduleStatus = "Incomplete";
  }
  db.getCollection('calendar').update({_id:obj._id},{$set:{'entry.schedulestatus':scheduleStatus}});
});


//===================================================================================================================================

//QC3-9155 - to create index with username

db.loginattempts.dropIndexes()
db.loginattempts.createIndex( { username: "text",email: "text",name: "text" } )
