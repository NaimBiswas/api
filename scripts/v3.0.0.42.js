db.resources.insert({title: 'Group Settings', entity: 'groupsettings', isActive: true})
db.resources.updateOne({entity : 'schema'},{$set : {title : 'App Builder'}});
db.resources.updateOne({entity : 'entries'},{$set : {title : 'App'}});
db.resources.updateOne({entity : 'calendar'},{$set : {title : 'Schedules'}});
db.resources.updateOne({entity : 'thresholds'},{$set : {title : 'Smart Alerts'}});
db.resources.updateOne({entity : 'thresholdsNotification'},{$set : {title : 'Smart Alerts Notification'}});
db.resources.updateOne({entity : 'masterqcsettings'},{$set : {title : 'Master Settings'}});

db.getCollection('entities').find({}).forEach(function(ent){var id = ent._id.valueOf();var count = db[id].find({}).count();ent.values.forEach(function (val) {var cnt = 0;if (count > 0) {db[id].find({}).forEach(function (pdata) {if (pdata.entityRecord.toString() === val.toString()) {cnt++;}});}if (cnt == 0) {db[id].insert({'entityRecord': val,'createdBy': ent.createdBy,'modifiedBy': ent.modifiedBy,'createdByName': ent.createdByName,'modifiedByName': ent.modifiedByName,'createdDate': ent.createdDate,'modifiedDate': ent.modifiedDate,'version': ent.version,'isMajorVersion': ent.isMajorVersion,'versions': []});}});});

db.getCollection('schema').find({}).forEach(function(obj){var arr = [];obj.workflowreview.forEach(function(review){ review.user.forEach(function(user){var wuser =  db.getCollection('users').findOne({_id : ObjectId(user.id)});if(wuser != null) {user.firstname = wuser.firstname;user.lastname = wuser.lastname; }});arr.push(review);});
db.getCollection('schema').update({_id:obj._id}, {$set:{"workflowreview": arr}});});



db.versions.insert({"productVersion":"v3.0.0.42","web":{"major":"3","minor":"0","build":"0","revision":"42"},"api":{"major":"3","minor":"0","build":"0","revision":"42"},"db":{"major":"3","minor":"0","build":"0","revision":"42"},"report":{"major":"3","minor":"0","build":"0","revision":"42"},"reportApi":{"major":"3","minor":"0","build":"0","revision":"42"}})





