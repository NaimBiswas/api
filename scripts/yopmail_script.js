script to update email-ids when app gets fail
---------------------------------------------------------------------------------

===================
1. For schema:
===================

1. db.schema.find().forEach(function(schema){
   if(schema.emailIdForQCFormFail){
       var updatedEmails = [];
       var emails = schema.emailIdForQCFormFail.split(',');       
       if(emails){
          emails.forEach(function(e){
          e = e.trim();
          var startPos = e.indexOf('@');           
          updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');    
       })
      }           
       print(updatedEmails.join());
       db.schema.updateOne({_id: schema._id},{$set: {emailIdForQCFormFail: updatedEmails.join()}});       
    }  
})



Minified Version
------------------------------------------------------

db.schema.find().forEach(function(schema){if(schema.emailIdForQCFormFail){var updatedEmails=[];var emails=schema.emailIdForQCFormFail.split(',');if(emails){emails.forEach(function(e){e=e.trim();var startPos=e.indexOf('@');updatedEmails.push(e.substr(0,startPos+1)+'yopmail.com');})}
print(updatedEmails.join());db.schema.updateOne({_id:schema._id},{$set:{emailIdForQCFormFail:updatedEmails.join()}});}})


===================
2. For user:
===================

db.users.find().forEach(function(user){ if(user.email){ user.email = user.email.trim(); var startPos = user.email.indexOf('@'); user.email = user.email.substr(0, startPos + 1) + 'yopmail.com'; } db.users.updateOne({_id: user._id},{$set: {email: user.email}});})

db.users.find().forEach(function(user){ if(user.newEmail){ user.newEmail = user.email.trim(); var startPos = user.newEmail.indexOf('@'); user.newEmail = user.newEmail.substr(0, startPos + 1) + 'yopmail.com'; } db.users.updateOne({_id: user._id},{$set: {newEmail: user.newEmail}});})


============================
3. For calendarThresholds:
============================

db.calendarThresholds.find().forEach(function(threshold){
   if(threshold.email){
       var updatedEmails = [];
       var emails = threshold.email.split(',');      
       if(emails){
          emails.forEach(function(e){
          e = e.trim();
          var startPos = e.indexOf('@');          
          updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');    
       });
      }          
       print(updatedEmails.join());
       print(threshold._id + ' updated successfully');
       db.calendarThresholds.updateOne({_id: threshold._id},{$set: {email: updatedEmails.join()}});      
    }  
});


db.calendarThresholds.find().forEach(function(calendar){
 if(calendar.default && calendar.default.support){
     var updatedEmails = [];
     var emails = calendar.default.support.split(',');      
    if(emails){
        emails.forEach(function(e){
        e = e.trim();
        var startPos = e.indexOf('@');          
       updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');    
    });
    }          
    print(updatedEmails.join());
     print(calendar._id + ' updated successfully');
     db.calendarThresholds.updateOne({_id: calendar._id},{$set: {"default.support": updatedEmails.join()}});      
 }  
});


Minified Version
------------------------------------------------------

db.calendarThresholds.find().forEach(function(threshold){ if(threshold.email){ var updatedEmails = []; var emails = threshold.email.split(','); if(emails){ emails.forEach(function(e){ e = e.trim(); var startPos = e.indexOf('@'); updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com'); }); } print(updatedEmails.join()); print(threshold._id + ' updated successfully'); db.calendarThresholds.updateOne({_id: threshold._id},{$set: {email: updatedEmails.join()}}); } });



=======================
4. For thresholds:
=======================

db.thresholds.find().forEach(function(threshold){
   if(threshold.reportconfiguration && threshold.reportconfiguration.emailaddress && threshold.reportconfiguration.emailaddress !== ''){
       var updatedEmails = [];
       var emails = threshold.reportconfiguration.emailaddress.split(',');      
       if(emails){
          emails.forEach(function(e){
          e = e.trim();
          var startPos = e.indexOf('@');          
          updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');    
       });
      }          
       print(updatedEmails.join());
       print(threshold._id + ' updated successfully');
       db.thresholds.updateOne({_id: threshold._id},{$set: {'reportconfiguration.emailaddress': updatedEmails.join()}});      
    }  
});


Minified Version
------------------------------------------------------

db.thresholds.find().forEach(function(threshold){ if(threshold.reportconfiguration && threshold.reportconfiguration.emailaddress && threshold.reportconfiguration.emailaddress !== ''){ var updatedEmails = []; var emails = threshold.reportconfiguration.emailaddress.split(','); if(emails){ emails.forEach(function(e){ e = e.trim(); var startPos = e.indexOf('@'); updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com'); }); } print(updatedEmails.join()); print(threshold._id + ' updated successfully'); db.thresholds.updateOne({_id: threshold._id},{$set: {'reportconfiguration.emailaddress': updatedEmails.join()}}); } });


--------------------------------------------------------------------------------------

=====================
5. For schedules
=====================

db.schedules.find().forEach(function(schedules){
  if(schedules.email){
      var updatedEmails = [];
      var emails = schedules.email.split(',');  
     
     if(emails){
         emails.forEach(function(e){
         e = e.trim();
         var startPos = e.indexOf('@');          
        updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');    
     });
     }
       print(updatedEmails.join());
      print(schedules._id + ' updated successfully');
   
      db.schedules.updateOne({_id: schedules._id},{$set: {email: updatedEmails.join()}});
   }
 if(schedules.reviewemail){
     var reviewupdatedEmails = [];
     var reviewEmails =schedules.reviewemail.split(',');  
     
     if(reviewEmails){
         reviewEmails.forEach(function(e){
         e = e.trim();
         var startPos = e.indexOf('@');          
        reviewupdatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');    
     });
     }  
     
   
       db.schedules.updateOne({_id: schedules._id},{$set: {reviewemail:reviewupdatedEmails.join()}});    
     
  }  
});

-----------------------------------------------------------------------------------------------

======================
6. For Calendar
======================

db.calendar.find().forEach(function(calendar){
  if(calendar.email){
      var updatedEmails = [];
      var emails = calendar.email.split(',');      
     if(emails){
         emails.forEach(function(e){
         e = e.trim();
         var startPos = e.indexOf('@');          
        updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');    
     });
     }          
     print(updatedEmails.join());
      print(calendar._id + ' updated successfully');
      db.calendar.updateOne({_id: calendar._id},{$set: {email: updatedEmails.join()}});      
  }  
});



db.calendar.find().forEach(function(calendar){
 if(calendar.default && calendar.default.support){
     var updatedEmails = [];
     var emails = calendar.default.support.split(',');      
    if(emails){
        emails.forEach(function(e){
        e = e.trim();
        var startPos = e.indexOf('@');          
       updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');    
    });
    }          
    print(updatedEmails.join());
     print(calendar._id + ' updated successfully');
     db.calendar.updateOne({_id: calendar._id},{$set: {'default.support': updatedEmails.join()}});      
 }  
});

db.calendar.find().forEach(function(calendar){
      if(calendar.skippedMasterApps){
        var obj = Object.keys(calendar.skippedMasterApps)||{};
            if(Object.keys(obj).length){
                      obj.forEach(function(key){
              var emails = calendar.skippedMasterApps[key].skippedBy.email;
              var emailStatrtPos = emails.indexOf('@');  
               calendar.skippedMasterApps[key].skippedBy.email = emails.substr(0, emailStatrtPos + 1) + 'yopmail.com'; 
              print(calendar);             
              });
              db.calendar.updateOne({_id: calendar._id},{$set: {'skippedMasterApps': calendar.skippedMasterApps}});                     
              }          
      }
});


-----------------------------------------------------------------------------------------------


============================
7. For loginattempts
============================

db.loginattempts.find().forEach(function(loginattempts){ if(loginattempts.email){ loginattempts.email = loginattempts.email.trim(); var startPos = loginattempts.email.indexOf('@'); loginattempts.email = loginattempts.email.substr(0, startPos + 1) + 'yopmail.com'; } db.loginattempts.updateOne({_id: loginattempts._id},{$set: {email: loginattempts.email}});})


---------------------------------------------------------------------------------------------------------

===============================
8. For thresholdsNotification
===============================

db.thresholdsNotification.find().forEach(function(thresholdsNotification){ if(thresholdsNotification.email){ thresholdsNotification.email = thresholdsNotification.email.trim(); var startPos = thresholdsNotification.email.indexOf('@'); thresholdsNotification.email = thresholdsNotification.email.substr(0, startPos + 1) + 'yopmail.com'; } db.thresholdsNotification.updateOne({_id: thresholdsNotification._id},{$set: {email: thresholdsNotification.email}});})


-----------------------------------------------------------------------------------------------------

===============================
9. For generalSettings
===============================

db.generalSettings.find().forEach(function(setting){ if(setting.supportContactDetails){ setting.supportContactDetails = setting.supportContactDetails.trim(); var startPos = setting.supportContactDetails.indexOf('@'); setting.supportContactDetails = setting.supportContactDetails.substr(0, startPos + 1) + 'yopmail.com'; } db.generalSettings.updateOne({_id: setting._id},{$set: {supportContactDetails: setting.supportContactDetails}});})

===============================
10. For appAccessConfig
===============================

db.appAccessConfig.find().forEach(function(appAccessConfig){ if(appAccessConfig.emails){ appAccessConfig.emails.forEach(function(email){ if(email.email){ var emailStatrtPos = email.email.indexOf('@'); email.email = email.email.substr(0, emailStatrtPos + 1) + 'yopmail.com'; } }); print(appAccessConfig.emails); db.appAccessConfig.updateOne({_id: appAccessConfig._id},{$set: {emails: appAccessConfig.emails}}); } });

===================
11. For preSchema:
===================

db.preSchema.find().forEach(function(schema){
   if(schema.emailIdForQCFormFail){
       var updatedEmails = [];
       var emails = schema.emailIdForQCFormFail.split(',');       
       if(emails){
          emails.forEach(function(e){
          e = e.trim();
          var startPos = e.indexOf('@');           
          updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');    
       })
      }           
       print(updatedEmails.join());
       db.preSchema.updateOne({_id: schema._id},{$set: {emailIdForQCFormFail: updatedEmails.join()}});       
    }  
})