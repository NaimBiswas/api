/**
* @name Qulification Performance
* @author Prachi Thakkar <prachi.t@productivet.com>
*
* @version 1.0
*/
//QC3-10299 Improvement - Qualification Performance - Feature to create window based on different types
//Script for getting windows if there is no window type, if no window type then make it 3(Customize Window)
//------------------------------------------------------------------------------------------------------------------------------------
db.windows.find({}).forEach(function (item) {
  if(!item.windowType){
    item.windowType = "3";
    db.windows.updateOne({_id:item._id}, {$set:{windowType : item.windowType}});
    print('Window updated successfully.');
  }
});
