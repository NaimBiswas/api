var finalObj = [];
db.schema.find().forEach(function (item) {
    if (typeof item.procedures != "undefined") {
        item.procedures.forEach(function (subitem) {
            var obj = {
                linkedToId: (item._id).valueOf(),
                linkedToType: 'schema',
                linkedToName: item.title,
                linkedToVersion: item.version,
                linkedToStatus: item.isActive,
                linkedOfType: 'procedures',
                linkedOfId: subitem._id,
                linkedOfName: subitem.title,
                linkedOfVersion: subitem.version
            }
            finalObj.push(obj);
        })
    }

    if (typeof item.entities != "undefined") {
        item.entities.forEach(function (result) {
            var obj = {
                linkedToId: (item._id).valueOf(),
                linkedToType: 'schema',
                linkedToName: item.title,
                linkedToVersion: item.version,
                linkedToStatus: item.isActive,
                linkedOfType: 'entities',
                linkedOfId: result._id,
                linkedOfName: result.title,
                linkedOfVersion: result.version
            }
            finalObj.push(obj);
            if (result.conditionalWorkflows && result.conditionalWorkflows.length > 0) {
                result.conditionalWorkflows.forEach(function (conWorkflows) {
                    if (conWorkflows.procedureToExpand) {
                        var obj = {
                            linkedOfId: conWorkflows.procedureToExpand._id.valueOf(),
                            linkedOfName: conWorkflows.procedureToExpand.title,
                            linkedOfVersion: conWorkflows.procedureToExpand.version,
                            linkedOfType: 'entities',
                            linkedToId: item._id.valueOf(),
                            linkedToType: 'schema',
                            // linkedToVersion:,
                            linkedToName: item.title,
                            linkedToStatus: item.isActive
                        };
                        finalObj.push(obj);
                    }
                });
            }
        });
    }

    if (typeof item.attributes != "undefined") {
        item.attributes.forEach(function (subitem) {
            var obj = {
                linkedToId: (item._id).valueOf(),
                linkedToType: 'schema',
                linkedToName: item.title,
                linkedToVersion: item.version,
                linkedToStatus: item.isActive,
                linkedOfType: 'attributes',
                linkedOfId: subitem._id,
                linkedOfName: subitem.title,
                linkedOfVersion: subitem.version
            }
            finalObj.push(obj);
        })
    }
})
print(finalObj);
db.linkedTbl.insertMany(finalObj);


