var finalObj = [];
db.entities.find().forEach(function (entity) {
    if (typeof entity.questions != "undefined") {
        entity.questions.forEach(function (item) {
            if (typeof item.validation != "undefined" && typeof item.validation.validationSet != "undefined" && typeof item.validation.validationSet.existingSet != "undefined") {
                var obj = {
                    linkedToId: (entity._id).valueOf(),
                    linkedToType: 'entities',
                    linkedToName: entity.title,
                    linkedOfType: 'sets',
                    linkedOfId: item.validation.validationSet.existingSet._id,
                    linkedOfName: item.validation.validationSet.existingSet.title,
                }
                finalObj.push(obj);
            }
        });
    }
});
db.linkedTbl.insertMany(finalObj);

