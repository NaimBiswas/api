db.getCollection('batchentrysettings').update({},{$set:{"batchEntryFormat" : [ 
       {
           "name" : "YYYY/MM/DD - Auto Increment",
           "entryType" : 0,
           "batchEntryFormatValue" : "",
           "batchCurrentValue" : 1,
           "isSelected" : false
       }, 
       {
           "name" : "MM/DD/YYYY - Auto Increment",
           "entryType" : 1,
           "batchEntryFormatValue" : "",
           "batchCurrentValue" : 1,
           "isSelected" : false
       }, 
       {
           "name" : "Custom - Auto Increment",
           "entryType" : 2,
           "batchEntryFormatValue" : "",
           "batchCurrentValue" : 1,
           "isSelected" : false
       }, 
       {
           "name" : "Auto Increment",
           "entryType" : 3,
           "batchCurrentValue" : 1,
           "isSelected" : false,
           "batchEntryFormatValue" : ""
       }, 
       {
           "name" : "Manual",
           "entryType" : 4,
           "batchEntryFormatValue" : "",
           "batchCurrentValue" : 1,
           "isSelected" : true
       }
   ]}})
