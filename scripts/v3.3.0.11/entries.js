var mimeTypes = {
  csv: 'text/csv',
  txt: 'text/plain',
  png: 'image/png',
  gif: 'image/gif',
  jpg: 'image/jpeg',
  jpeg: 'image/jpeg',
  ico: 'image/vnd.microsoft.icon',
  pdf: 'application/pdf',
  doc: 'application/msword',
  xls: 'application/vnd.ms-excel',
  ppt: 'application/vnd.ms-powerpoint',
  pps: 'application/vnd.ms-powerpoint',
  xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  ppsx: 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
  docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  pptx: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  msg: 'application/msoutlook',
  eml: 'message/rfc822'
};

var uploadedBy = ['OneDrive', 'Google Drive', 'PC', 'Dropbox', 'Amazon S3', 'MFiles'];

var update = true;

var output = {};

function processQuestion(question, parent, entry, output) {
  if (question.type.title === "File Attachment") {
    if (entry.entries[question['s#'] + '-' + parent['s#']]) {
      var value = entry.entries[question['s#'] + '-' + parent['s#']][parent['s#']];
      var link = entry.entries[question['s#'] + '-' + parent['s#']]['link'] || '';

      if (value) {

        if (!Array.isArray(value)) {
          value = [value];
        }

        entry.entries[question['s#'] + '-' + parent['s#']][parent['s#']] = value.map(function (v) {
          return getFileObject(v, link, output);
        });
      }

      delete entry.entries[question['s#'] + '-type' + parent['s#']];
      delete entry.entries[question['s#'] + '-size' + parent['s#']];
    }
  }
}

function getFileObject(value, link, output) {

  var object = {
    "id": value.id || value.ID || value.amazons3ID || getAmazons3ID(link),
    "title": value.name || value.title || link.name || link.title || value || link,
    "name": getFileName(value.title || value.name || link.title || link.name || value || link),
    "url": getUrl(value.url || link.url || link),
    "size": value.size || link.size || null,
    "version": value.version || getVersion(link),
    "isUploaded": true,
    "fieldType": "file",
    "sequenceId": value.sequenceId || (Math.round(Math.random() * 10000) + Date.now())
  };

  object.extension = value.extension || getExtension(object.title);
  object.fileStamp = value.fileStamp || link.fileStamp || getFileStamp(object.title);
  object.mimeType = value.mimeType || mimeTypes[object.extension];
  object.uploadedBy = value.uploadedBy || getUploadedBy(value.type || link.type, link, object);

  output.push(object);

  printjson(object);

  return object;
}

function getAmazons3ID(link) {
  if (link) {
    return link.amazons3ID || link.ID || null;
  } else {
    return null;
  }
}

function getVersion(link) {
  if (link) {
    return link.version || null;
  } else {
    return null;
  }
}

function getUrl(link) {
  if (typeof link === 'string' && link.startsWith('https')) {
    return link;
  } else {
    return null;
  }
}

function getFileName(value) {
  if (value.indexOf('-') === 13) {
    return value.substr(value.indexOf('-') + 1);
  } else {
    return value;
  }
}

function getFileStamp(value) {
  if (value.indexOf('-') === 13) {
    return value.substr(0, value.indexOf('-'));
  } else {
    return Date.now();
  }
}

function getExtension(value) {
  if (typeof value === 'string') {
    if (value.startsWith('https')) {
      return parseQueryString(value).format
    } else {
      return value.substr(value.lastIndexOf('.') + 1);
    }
  } else {
    return '';
  }
}

function getUploadedBy(type, link, object) {
  var url = object.url;

  if (link && link.amazons3ID) {
    return 'Amazon S3';
  } else if (url) {
    if (url.startsWith('https://1drv.ms')) {
      return 'OneDrive';
    } else if (url.startsWith('https://drive.google.com')) {
      return 'Google Drive';
    } else if (url.startsWith('https://dl.dropboxusercontent.com')) {
      return 'Dropbox';
    } else if (url.indexOf('amazonaws.com') != -1) {
      object.id = '5c49c97660a6d83cedea2eb4';
      object.version = 1;
      return 'Amazon S3';
    } else if (url.indexOf('content.aspx?') != -1) {
      if (!object.id) {
        var arr = url.substring(url.indexOf('/0/') + 3, url.indexOf('/files/')).split('/');
        object.id = arr[0];
        object.version = arr[1] || 1;
      }
      return 'MFiles';
    } else {
      if (type && uploadedBy.indexOf(type) != -1) {
        return type;
      } else {
        return 'PC';
      }
    }
  } else {
    if (type && uploadedBy.indexOf(type) != -1) {
      return type;
    } else {
      return 'PC';
    }
  }
}

function parseQueryString(url) {
  var queryString = url.substr(url.lastIndexOf('?') + 1);
  var params = {};
  var queries = queryString.split("&");

  for (var i = 0; i < queries.length; i++) {
    var temp = queries[i].split('=');
    params[temp[0]] = temp[1];
  }

  return params;
}


var schemas = db.getCollection('schemaAuditLogs').find({
  $or: [{
    'entities.questions.type.title': "File Attachment"
  }, {
    'procedures.questions.type.title': "File Attachment"
  }]
}, {
  procedures: 1,
  entities: 1,
  identity: 1,
  version: 1
}).toArray();

schemas.forEach(function (schema) {
  // 1. schemaId
  // 2. schemaId + 'AuditLogs'
  var schemaId = schema.identity.valueOf() + 'AuditLogs';
  output[schemaId] = {};

  var entries = db.getCollection(schemaId).find({
    parentVersion: schema.version
  }, {
    entries: 1
  }).toArray();

  entries.forEach(function (entry) {
    var entryId = entry._id.valueOf();
    output[schemaId][entryId] = [];

    // ==================== 1.1 Entities questions ====================
    (schema.entities || []).forEach(function (entity) {
      entity.questions.forEach(function (question) {
        processQuestion(question, entity, entry, output[schemaId][entryId]);
      });
    });

    // ==================== 1.2 Procedures questions ====================
    (schema.procedures || []).forEach(function (procedure) {
      procedure.questions.forEach(function (question) {
        processQuestion(question, procedure, entry, output[schemaId][entryId]);
      });
    });
  });

  if (update) {
    entries.forEach(function (entry) {
      var entryId = entry._id.valueOf();
      if (output[schemaId][entryId] && output[schemaId][entryId].length) {
        db.getCollection(schemaId).update({
          _id: entry._id
        }, {
          $set: {
            entries: entry.entries
          }
        })
      }
    });
  }
});