var mimeTypes = {
    csv: 'text/csv',
    txt: 'text/plain',
    png: 'image/png',
    gif: 'image/gif',
    jpg: 'image/jpeg',
    jpeg: 'image/jpeg',
    ico: 'image/vnd.microsoft.icon',
    pdf: 'application/pdf',
    doc: 'application/msword',
    xls: 'application/vnd.ms-excel',
    ppt: 'application/vnd.ms-powerpoint',
    pps: 'application/vnd.ms-powerpoint',
    xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    ppsx: 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
    docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    pptx: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    msg: 'application/msoutlook',
    eml: 'message/rfc822'
};

var uploadedBy = ['OneDrive', 'Google Drive', 'PC', 'Dropbox', 'Amazon S3', 'MFiles'];

var update = true;

var output = {};

function updateInstructionFile(object, output) {
    var value = object.file;
    var link = object.dmsfile;

    value = Array.isArray(value) ? value[0] || '' : value || '';
    link = Array.isArray(link) ? link[0] || '' : link || '';

    if (value || link) {
        object.file = [getFileObject(value, link, output)];
    }

    delete object.dmsfile;
}

function getFileObject(value, link, output) {

    var object = {
        "id": value.id || value.ID || value.amazons3ID || getAmazons3ID(link),
        "title": value.name || value.title || link.name || link.title || value || link,
        "name": getFileName(value.title || value.name || link.title || link.name || value || link),
        "url": getUrl(value.url || link.url || link),
        "size": value.size || link.size || null,
        "version": value.version || getVersion(link),
        "isUploaded": true,
        "fieldType": "file",
        "sequenceId": value.sequenceId || (Math.round(Math.random() * 10000) + Date.now())
    };

    object.extension = value.extension || getExtension(object.title);
    object.fileStamp = value.fileStamp || link.fileStamp || getFileStamp(object.title);
    object.mimeType = value.mimeType || mimeTypes[object.extension];
    object.uploadedBy = value.uploadedBy || getUploadedBy(value.type || link.type, link, object);

    output.push(object);

    printjson(object);

    return object;
}

function getAmazons3ID(link) {
    if (link) {
        return link.amazons3ID || link.ID || null;
    } else {
        return null;
    }
}

function getVersion(link) {
    if (link) {
        return link.version || null;
    } else {
        return null;
    }
}

function getUrl(link) {
    if (typeof link === 'string' && link.startsWith('https')) {
        return link;
    } else {
        return null;
    }
}

function getFileName(value) {
    if (value.indexOf('-') === 13) {
        return value.substr(value.indexOf('-') + 1);
    } else {
        return value;
    }
}

function getFileStamp(value) {
    if (value.indexOf('-') === 13) {
        return value.substr(0, value.indexOf('-'));
    } else {
        return Date.now();
    }
}

function getExtension(value) {
    if (typeof value === 'string') {
        if (value.startsWith('https')) {
            return parseQueryString(value).format
        } else {
            return value.substr(value.lastIndexOf('.') + 1);
        }
    } else {
        return '';
    }
}

function getUploadedBy(type, link, object) {
    var url = object.url;

    if (link && link.amazons3ID) {
        return 'Amazon S3';
    } else if (url) {
        if (url.startsWith('https://1drv.ms')) {
            return 'OneDrive';
        } else if (url.startsWith('https://drive.google.com')) {
            return 'Google Drive';
        } else if (url.startsWith('https://dl.dropboxusercontent.com')) {
            return 'Dropbox';
        } else if (url.indexOf('amazonaws.com') != -1) {
            object.id = '5c49c97660a6d83cedea2eb4';
            object.version = 1;
            return 'Amazon S3';
        } else if (url.indexOf('content.aspx?') != -1) {
            if (!object.id) {
                var arr = url.substring(url.indexOf('/0/') + 3, url.indexOf('/files/')).split('/');
                object.id = arr[0];
                object.version = arr[1] || 1;
            }
            return 'MFiles';
        } else {
            if (type && uploadedBy.indexOf(type) != -1) {
                return type;
            } else {
                return 'PC';
            }
        }
    } else {
        if (type && uploadedBy.indexOf(type) != -1) {
            return type;
        } else {
            return 'PC';
        }
    }
}

function parseQueryString(url) {
    var queryString = url.substr(url.lastIndexOf('?') + 1);
    var params = {};
    var queries = queryString.split("&");

    for (var i = 0; i < queries.length; i++) {
        var temp = queries[i].split('=');
        params[temp[0]] = temp[1];
    }

    return params;
}

// 1. schema
// 2. schemaAuditLogs
// 3. preSchema
// 4. preSchemaAuditLogs
var collection = 'schema';

var schemas = db.getCollection(collection).find({}, {
    file: 1,
    dmsfile: 1,
    procedures: 1,
    entities: 1
}).toArray();

schemas.forEach(function (schema) {
    var schemaId = schema._id.valueOf();
    output[schemaId] = {};
    output[schemaId]['schema'] = [];
    output[schemaId]['entities'] = {};
    output[schemaId]['procedures'] = {};

    // ==================== 1.0 Schema ====================
    updateInstructionFile(schema, output[schemaId]['schema']);

    // ==================== 1.1 Entities ====================
    (schema.entities || []).forEach(function (entity) {
        var entityId = entity._id.valueOf();
        output[schemaId]['entities'][entityId] = [];

        updateInstructionFile(entity, output[schemaId]['entities'][entityId]);
    });

    // ==================== 1.2 Procedures ====================
    (schema.procedures || []).forEach(function (procedure) {
        var procedureId = procedure._id.valueOf();
        output[schemaId]['procedures'][procedureId] = [];

        updateInstructionFile(procedure, output[schemaId]['procedures'][procedureId]);
    });

    if (update) {
        db.getCollection(collection).update({
            _id: schema._id
        }, {
            $set: {
                file: schema.file,
                procedures: schema.procedures,
                entities: schema.entities
            },
            $unset: {
                dmsfile: 1
            }
        })
    }
});