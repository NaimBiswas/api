Run schema.js 4 times
1. schema
2. schemaAuditLogs
3. preSchema
4. preSchemaAuditLogs

Run entries.js 2 times
1. schemaId
2. schemaId + 'AuditLogs'

Run entityRecords.js 2 times
1. entityId
2. entityId + 'AuditLogs'

Run entities-procedures.js 4 times
1. entities
2. entitiesAuditLogs
3. procedures
4. proceduresAuditLogs
