var mimeTypes = {
    csv: 'text/csv',
    txt: 'text/plain',
    png: 'image/png',
    gif: 'image/gif',
    jpg: 'image/jpeg',
    jpeg: 'image/jpeg',
    ico: 'image/vnd.microsoft.icon',
    pdf: 'application/pdf',
    doc: 'application/msword',
    xls: 'application/vnd.ms-excel',
    ppt: 'application/vnd.ms-powerpoint',
    pps: 'application/vnd.ms-powerpoint',
    xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    ppsx: 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
    docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    pptx: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    msg: 'application/msoutlook',
    eml: 'message/rfc822'
};

var update = true;

var output = {};

function getFileObject(_value, output) {

    if (Array.isArray(_value)) {
        if (_value[0]) {
            value = _value[0];
        } else {
            value = '';
        }
    } else {
        value = _value;
    }

    var object = {
        "id": null,
        "title": value.title || value,
        "name": value.name || getFileName(value),
        "url": null,
        "size": null,
        "version": null,
        "isUploaded": true,
        "fieldType": "file",
        "sequenceId": value.sequenceId || (Math.round(Math.random() * 10000) + Date.now())
    };

    object.extension = value.extension || getExtension(object.title);
    object.fileStamp = value.fileStamp || getFileStamp(object.title);
    object.mimeType = value.mimeType || mimeTypes[object.extension];
    object.uploadedBy = 'PC';

    output.push(object);

    printjson(object);

    return object;

}

function getAmazons3ID(link) {
    if (link) {
        return link.amazons3ID || link.ID || null;
    } else {
        return null;
    }
}

function getVersion(link) {
    if (link) {
        return link.version || null;
    } else {
        return null;
    }
}

function getUrl(link) {
    if (typeof link === 'string' && link.startsWith('https')) {
        return link;
    } else {
        return null;
    }
}

function getFileName(value) {
    if (value.indexOf('-') === 13) {
        return value.substr(value.indexOf('-') + 1);
    } else {
        return value;
    }
}

function getFileStamp(value) {
    if (value.indexOf('-') === 13) {
        return value.substr(0, value.indexOf('-'));
    } else {
        return Date.now();
    }
}

function getExtension(value) {
    if (typeof value === 'string') {
        if (value.startsWith('https')) {
            return parseQueryString(value).format
        } else {
            return value.substr(value.lastIndexOf('.') + 1);
        }
    } else {
        return '';
    }
}

function getUploadedBy(type, link, object) {
    var url = object.url;

    if (link && link.amazons3ID) {
        return 'Amazon S3';
    } else if (url) {
        if (url.startsWith('https://1drv.ms')) {
            return 'OneDrive';
        } else if (url.startsWith('https://drive.google.com')) {
            return 'Google Drive';
        } else if (url.startsWith('https://dl.dropboxusercontent.com')) {
            return 'Dropbox';
        } else if (url.indexOf('amazonaws.com') != -1) {
            object.id = '5c49c97660a6d83cedea2eb4';
            object.version = 1;
            return 'Amazon S3';
        } else if (url.indexOf('content.aspx?') != -1) {
            if (!object.id) {
                var arr = url.substring(url.indexOf('/0/') + 3, url.indexOf('/files/')).split('/');
                object.id = arr[0];
                object.version = arr[1] || 1;
            }
            return 'MFiles';
        } else {
            if (type && uploadedBy.indexOf(type) != -1) {
                return type;
            } else {
                return 'PC';
            }
        }
    } else {
        if (type && uploadedBy.indexOf(type) != -1) {
            return type;
        } else {
            return 'PC';
        }
    }
}

function parseQueryString(url) {
    var queryString = url.substr(url.lastIndexOf('?') + 1);
    var params = {};
    var queries = queryString.split("&");

    for (var i = 0; i < queries.length; i++) {
        var temp = queries[i].split('=');
        params[temp[0]] = temp[1];
    }

    return params;
}

['manualimport', 'manualimportAuditLogs'].forEach(function (collection) {
    output[collection] = [];

    var imports = db.getCollection(collection).find({}, {
        file: 1
    }).toArray();

    imports.forEach(function (item) {

        var value = item.file;

        if (value) {
            item.file = [getFileObject(value, output[collection])];
        }

        if (update) {
            db.getCollection(collection).update({
                _id: item._id
            }, {
                $set: {
                    file: item.file
                }
            });
        }
    });
});