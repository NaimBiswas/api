db.schema.find().forEach(function(schm){
    var flg = false;
   if(schm.procedures && schm.procedures.length > 0){
       schm.procedures.forEach(function(prd){
           if(prd && prd.questionUsedInAcceptance){
               for(var ky in prd.questionUsedInAcceptance){
                   prd.questionUsedInAcceptance[ky].forEach(function(qua){
                    if(qua.pqse){
                        qua.pseq = qua.pqse;
                        delete qua.pqse;
                        flg = true;
                    }
                   });
               }
           }
       });
   }
   if(flg){
       printjson(schm.title);
       db.schema.updateOne({_id : schm._id},{$set : {'procedures' : schm.procedures}});
   }
});


--------------------------------------------------------------------------------------




db.attributes.find({}).forEach(function(atr){
    if(atr && atr.type && atr.type.format && atr.type.format.title && atr.type.format.title === 'Date'){
        if(atr.validation && atr.validation.validationSet && atr.validation.validationSet.existingSet){
            db.attributes.updateOne({_id : atr._id},{$unset : {'validation.validationSet' : ""}});
        }
    } 
});





db.getCollection('schema').find({}).forEach(function(schm){
    if(schm && schm.attributes && schm.attributes.length > 0){
        schm.attributes.forEach(function(atr,aidx){
          if(atr && atr.type && atr.type.format && atr.type.format.title && atr.type.format.title === 'Date'){
            if(atr.validation && atr.validation.validationSet && atr.validation.validationSet.existingSet){
                delete atr.validation.validationSet;
	     }
            }
        });
        db.schema.updateOne({_id : schm._id},{$set : {'attributes' : schm.attributes}});
    }
     
});



