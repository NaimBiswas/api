db.getCollection('schema').find({}, {version: 1, isActive: 1}).forEach(function (schema) {
    db.getCollection('preSchema').find({_id: schema._id}, {version: 1, isActive: 1})
        .forEach(function (pschema) {
            
            if (pschema.isActive != schema.isActive) {
                print('schemaAuditLogs/schema update in id: ' + schema._id);
                db.schema.update({_id: schema._id},{$set: {isActive: pschema.isActive}});
            }
            
        });
    });
   
   
