//PQT-1308
/**
 * @author Yamuna Sardhara <yamuna.s@productivet.com>
 * @description Add License Agreement resourse to resourses collection
 * @version 1.0 
 */
var isLicenseAgreementAvailable = db.getCollection('resources').findOne({
    title: "License Agreement"
});
if (!isLicenseAgreementAvailable) {
    db.getCollection('resources').insert({
        "title" : "License Agreement",
        "entity" : "licenseAgreement",
        "isActive" : true
    });
}


//PQT-1308
// demo schema for licenseAgreement collection
 db.licenseAgreement.insert({"licenseAgreementVersion" : "1.0", 
                            "licenseAgreementContent" : ""});

/**
 * @author Yamuna Sardhara <yamuna.s@productivet.com>
 * @description To add default collection called preSchema into permissions of roles 
 * @version 1.0 
 */
var isLicenseAgreementAvailable = db.getCollection('resources').findOne({
    title: "License Agreement"
});
if (isLicenseAgreementAvailable) {
    db.getCollection('roles').find({}).forEach(function (role) {
        if (role && role.modules) {
            role.modules.forEach(function (roleModule) {
                if (roleModule && roleModule.permissions && !roleModule.permissions.find(function (rolePerm) {
                        return rolePerm._id == isLicenseAgreementAvailable._id
                    })) {
                    roleModule.permissions.push({
                        "_id": isLicenseAgreementAvailable._id,
                        "title": isLicenseAgreementAvailable.title,
                        "entity": isLicenseAgreementAvailable.entity,
                        "view": true,
                        "create": true,
                        "edit": true,
                    });
                }
            })
            db.getCollection('roles').updateOne({
                "_id": role._id
            }, {
                $set: {
                    "modules": role.modules
                }
            });
            printjson('Update in ' + role._id);
        }
    });
}

/**
 * @author Ankur Patel <ankur.p@productivet.com>
 * @description Add Bi Reports resourse to resourses collection
 * @version 1.0 
 */
var ressourceData = db.getCollection('resources').findOne({entity:'bireports'})
if(!ressourceData) {
  db.getCollection('resources').insert({
  "title" : "BI Reports",
  "entity" : "bireports",
  "isActive" : true})
}
    
  
