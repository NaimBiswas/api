/**
 * @author Mahammad <mahammmad.c>
 * PQT-650
 */

db.types.remove({
    "title": "Dropdown List"
});
db.types.insert({
    "_id": "5b6c3597144a0ed2d3b10dd2",
    "title": "Dropdown List",
    "formats": [{
        "_id": "122256789012345678902235",
        "title": "None"
    }]
});


/**
 * @author Jyotil Raval <jyotil.r@productivet.com>
 * @description changes into 'thresholds' for type 'Hourly' (PQT-720)
 * @version 3.3
 */
db
    .getCollection('thresholds')
    .updateMany({
        'frequency.type': 'Hourly'
    }, {
        $set: {
            'reportconfiguration.exatmatchrecord': 'Send Records Matching Smart Alert Condition',
            'reportconfiguration.whichMatchingRecord': 'extent'
        }
    });

/**
 * @author Jyotil Raval <jyotil.r@productivet.com>
 * @description changes into 'thresholds' for type not 'Hourly' (PQT-720)
 * @version 3.3
 */
db
    .getCollection('thresholds')
    .updateMany({
        'frequency.type': {
            $ne: 'Hourly'
        }
    }, {
        $set: {
            'reportconfiguration.exatmatchrecord': 'Send Records Matching Smart Alert Condition',
            'reportconfiguration.whichMatchingRecord': 'all'
        }
    });

/**
 * @author Jyotil Raval <jyotil.r@productivet.com>
 * @description changes into 'calendarThresholds' for type 'Hourly' (PQT-720)
 * @version 3.3
 */
db
    .getCollection('calendarThresholds')
    .updateMany({
        'type': 'Hourly'
    }, {
        $set: {
            'exatmatchrecord': 'Send Records Matching Smart Alert Condition',
            'whichMatchingRecord': 'extent'
        }
    });

/**
 * @author Jyotil Raval <jyotil.r@productivet.com>
 * @description changes into 'calendarThresholds' for type not 'Hourly' (PQT-720)
 * @version 3.3
 */
db
    .getCollection('calendarThresholds')
    .updateMany({
        'type': {
            $ne: 'Hourly'
        }
    }, {
        $set: {
            'exatmatchrecord': 'Send Records Matching Smart Alert Condition',
            'whichMatchingRecord': 'all'
        }
    });