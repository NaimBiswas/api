db.getCollection('schema').find({}).forEach(function(sch){
    if(sch  && sch.procedures && sch.procedures.length > 0){
        sch.procedures.forEach(function(prod,pidx){
            if(prod && prod.questionUsedInProcedureCondition){
                for(var k in prod.questionUsedInProcedureCondition){
                    if(prod.questionUsedInProcedureCondition[k] && prod.questionUsedInProcedureCondition[k].length > 0){
                        prod.questionUsedInProcedureCondition[k].forEach(function(qip,qidx){
                            if(qip.procedureSeq){
                                qip.pseq = qip.procedureSeq;
                                delete qip.procedureSeq;
                                printjson("Got !");
                            }
                        });
                       
                    }
                }
            }
        });
    }
    db.schema.updateOne({_id : sch._id},{$set : {procedures : sch.procedures}});
});


db.getCollection('schema').find({}).forEach(function(sch){
    if(sch && sch.entities && sch.entities.length > 0){
        sch.entities.forEach(function(prod,pidx){
            if(prod && prod.questionUsedInProcedureCondition){
                for(var k in prod.questionUsedInProcedureCondition){
                    if(prod.questionUsedInProcedureCondition[k] && prod.questionUsedInProcedureCondition[k].length > 0){
                        prod.questionUsedInProcedureCondition[k].forEach(function(qip,qidx){
                            if(qip.procedureSeq){
                                qip.pseq = qip.procedureSeq;
                                delete qip.procedureSeq;
                                printjson("Got !");
                            }
                        });
                       
                    }
                }
            }
        });
    }
    db.schema.updateOne({_id : sch._id},{$set : {entities : sch.entities}});
});
