db.settings.update({"_id" :"default" },{$set : {"support":"support@beperfeqta.com"}})

db.masterqcsettings.find().forEach(function(ms){
    var arr = [];
    var flg = false;
    if(ms && ms.schema && ms.schema.length > 0){
        
        ms.schema.forEach(function(scm){
            if(scm){
                var adt = db.schema.findOne(ObjectId(scm._id));
                if(adt && !adt.isActive){
                    printjson(" title " + ms.title + " ::: Schema Title :" + scm.title);
                    flg = true;
                 }
                 else{
                     arr.push(scm);
                 }
                 
            }
        });
     }
     if(flg){
        db.masterqcsettings.updateOne({_id : ms._id},{$set : {schema : arr}});
     }
});

db.masterqcsettings.find().forEach(function(ms){
    var arr = [];
    var flg = false;
    if(ms && ms.qcform && ms.qcform.length > 0){
        ms.qcform.forEach(function(scm){
            if(scm){
                var adt = db.schema.findOne(ObjectId(scm.id));
                if(adt && !adt.isActive){
                    printjson(" title " + ms.title + " ::: Schema Title :" + scm.title);
                    flg = true;
                 }
                 else{
                     arr.push(scm);
                 }
                 
            }
        });
     }
     if(flg){
        db.masterqcsettings.updateOne({_id : ms._id},{$set : {qcform : arr}});
     }
});
