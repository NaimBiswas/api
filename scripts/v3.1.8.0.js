//===================================================================================================================================

//QC3-9701 - to remove duplicate record

var duplicates = [];
db.reviewpendingworkflow.aggregate([
    {
        $group: {
            _id: {
                entryId: "$entryId", schemaId: "$schemaId",
                masterQCSettingsId: "$masterQCSettings.masterQCSettingsId",
                sequenceId: "$masterQCSettings.sequenceId",
            },
            dups: { "$addToSet": "$_id" },
            count: { "$sum": 1 }
        }
    },
    {
        $match: {
            count: { "$gt": 1 }
        }
    }
],
    { allowDiskUse: true }
)
    .forEach(function (doc) {
        doc.dups.shift();
        doc.dups.forEach(function (dupId) {
            duplicates.push(dupId);
        }
        )
    });

printjson(duplicates);

db.reviewpendingworkflow.remove({ _id: { $in: duplicates } });

//===================================================================================================================================

//QC3-9701 - to remove duplicate record

print('Following entries will be removed. (schemaId/entryId)');
db.getCollection('reviewpendingworkflow').find({}).forEach(function(obj){
        var entry = db[obj.schemaId].findOne({_id:obj.entryId},{workflowreview:1});
        if(entry && entry.workflowreview && entry.workflowreview.length && (entry.workflowreview||[]).every(function (review) { return review.reviewstatus == 0;})){
                print(obj.schemaId+'/'+obj.entryId.valueOf());
                db.reviewpendingworkflow.remove(obj._id);
        }
})

//===================================================================================================================================

//QC3-9296 - to remove entry obj from calendar if void the record

db.getCollection('calendar').find({ 'ismasterQcForm': false, 'entry': { $exists: 1 } }).forEach(function (obj) {
    if (db[obj.schema._id.valueOf()].findOne({ '_id': obj.entry.entryId, 'entries.status.status': 3, 'calendarIds.0': { $exists: 1 }, 'calendarverifieduser.0': { $exists: 1 } })) {
        db[obj.schema._id.valueOf()].update({ _id: obj.entry.entryId }, { $set: { calendarIds: [], calendarverifieduser: [] } });
        db.getCollection('calendar').update({ _id: obj._id }, { $unset: { 'entry': 1 } });
    }
})

//===================================================================================================================================

//QC3-9296 - to remove entry obj from calendar if void all the record for master app

db.getCollection('calendar').find({ 'ismasterQcForm': true, 'entry': { $exists: true } }).forEach(function (obj) {
    var counter = 0;
    obj.entry.masterschemaIds.forEach(function (item) {
        if (db[item].findOne({ 'calendarIds.0': obj._id.valueOf(), 'entries.status.status': 3, 'calendarIds.0': { $exists: 1 }, 'calendarverifieduser.0': { $exists: 1 } })) {
            counter++;
        }
    });
    if (obj.entry.masterschemaIds.length == counter) {
        obj.entry.masterschemaIds.forEach(function (item) {
            db[item].update({ 'calendarIds.0': obj._id.valueOf() }, { $set: { calendarIds: [], calendarverifieduser: [] } });
        });
        db.getCollection('calendar').update({ _id: obj._id }, { $unset: { 'entry': 1 } });
    }
});

//===================================================================================================================================

//QC3-9296 - to add schedulestatus field in old records for single app

db.getCollection('calendar').find({ismasterQcForm:false,entry:{$exists:true},'entry.schedulestatus':{$exists:false}}).forEach(function(obj){

  var scheduleStatus = '';
       if(obj.entry.recordStatus == 4){
           scheduleStatus = "Incomplete";
       }else if(obj.entry.recordStatus == 0 || obj.entry.recordStatus == 1){
           scheduleStatus = "Completed";
       }

  db.getCollection('calendar').update({_id:obj._id},{$set:{'entry.scheduleStatus':scheduleStatus}});
});

//===================================================================================================================================

//QC3-9155 - to create index with username

db.loginattempts.dropIndexes()
db.loginattempts.createIndex( { username: "text",email: "text",name: "text" } )

//===================================================================================================================================

//QC3-9296 - to add schemaCompletedDate field
//Skipp option add in foles and permission's general
db.resources.insert({
    "title" : "Skip Schedule",
    "entity" : "skipSchedule",
    "isActive" : true
 })

 //add allow to skip option for old schedules
 db.schedules.update(
    {'allowToSkip': {$exists : false}},
    {$set: {'allowToSkip': true}},
    {multi: true}
)

//===================================================================================================================================

db.getCollection('calendar').update({"isSkippedAll":true},{"$set":{"entry.schedulestatus":"Completed","entry.completedDate":new Date()}})


// ------------------------------------- Added by Mahammad -------------------------------------

//QC3-9296 - to add schemaCompletedDate field

db.getCollection('calendar').find({ismasterQcForm:true,entry:{$exists:true},'entry.schemaCompletedDate':{$exists:false}}).count()

var count = 1;
db.getCollection('calendar').find({ismasterQcForm:true,entry:{$exists:true},'entry.schemaCompletedDate':{$exists:false}}).forEach(function(obj){
  var array = [];  
  obj.entry.masterschemaIds.forEach(function(item){
      db[item].find({'calendarIds.0': obj._id.valueOf()},{modifiedDate:1}).forEach(function(entry){
          array.push({schemaId:item,modifiedDate:entry.modifiedDate});
      });  
  });
  var schemaCompletedDate = array.reduce(function(obj,item){ obj[item.schemaId] = item.modifiedDate; return obj},{});
  print(count++);
  db.getCollection('calendar').update({_id:obj._id},{$set:{'entry.schemaCompletedDate':schemaCompletedDate}});
})

//=============================================================

db.getCollection('calendar').find({'entry.voidSchemas.0':{$exists:true}}).count()

//===========================================================

 db.getCollection('calendar').find({'entry.voidSchemas.0':{$exists:true}}).toArray()
.forEach(function(item){
   if(item && item.entry && item.entry.masterschemaIds && item.entry.masterschemaIds.length){  
   var voidSchemas = [];
   item.entry.masterschemaIds.forEach(function (schemaId) {
       var entry = db[schemaId+''].findOne({'calendarIds': {$in:[item._id+'']},'entries.status.status':3});
       if(entry){
         voidSchemas.push({
           schemaId: schemaId,
           modifiedDate: entry.modifiedDate,
           modifiedBy : entry.modifiedBy,
           modifiedByName: entry.modifiedByName
         });
       }
   }); 
   db.getCollection('calendar').update({_id: item._id},{$set: {'entry.voidSchemas': voidSchemas}})
   }
});

//====================================================================

db.calendar.find({'entry.voidSchemas':{$exists:true},'entry.voidSchemas.0':{$exists:false}});
db.calendar.updateMany({'entry.voidSchemas':{$exists:true},'entry.voidSchemas.0':{$exists:false}},{$set:{'entry.voidSchemas':[]}});


// -----------------------------------------------

db.getCollection('calendar').find({ismasterQcForm:false,entry:{$exists:true},'entry.completedDate':{$exists:true}}).count()

db.getCollection('calendar').find({ismasterQcForm:false,entry:{$exists:true},'entry.completedDate':{$exists:true}})
.forEach(function(obj){
       db[obj.schema._id.valueOf()].find({'_id': obj.entry.entryId}).forEach(function(entry){
            db.getCollection('calendar').update({_id:obj._id},{$set:{'entry.completedDate':entry.modifiedDate}});
       });  
})

// --------------------------------------------
db.calendar.find({ismasterQcForm: true,entry: {$exists:true}}).count()

var count = 1;
db.calendar.find({ismasterQcForm: true,entry: {$exists:true}}).toArray().forEach(function(item,index){
    if(item.entry.calendarverifieduser){
        for(var schemaId in item.entry.calendarverifieduser){
            var entry = db[schemaId+''].findOne({'calendarIds': {$in:[item._id+'']}});
            if(entry && entry.modifiedBy){
                var user = db.users.findOne({_id: ObjectId(entry.modifiedBy)}, {firstname: 1, lastname: 1, username: 1});
                if(user && user.firstname && user.lastname && user.username){
                    var modifiedByName = user.firstname + ' ' + user.lastname + ' (' + user.username + ')';
                    print(modifiedByName);
                    var obj = {};
                    obj[item._id.valueOf()] = modifiedByName;
                    item.entry.calendarverifieduser[schemaId] = [obj];
                }  
            }
            
        }
        print(count++);
        db.calendar.update({_id: item._id},{$set: {'entry.calendarverifieduser': item.entry.calendarverifieduser}})
    }
})

// ===================================================================================
db.calendar.find({ismasterQcForm: false,entry: {$exists:true}}).count()

var count = 1;
db.calendar.find({ismasterQcForm: false,entry: {$exists:true}}).toArray().forEach(function(item,index){
    if(item.entry.calendarverifieduser){      
        for(var schemaId in item.entry.calendarverifieduser){
            var entry = db[item.schema._id.valueOf()].findOne({'_id': item.entry.entryId});
            if(entry && entry.modifiedBy){
                var user = db.users.findOne({_id: ObjectId(entry.modifiedBy)}, {firstname: 1, lastname: 1, username: 1});
                if(user && user.firstname && user.lastname && user.username){
                    print(user.firstname + ' ' + user.lastname + ' (' + user.username + ')');
                    var obj = {};
                    obj[item._id.valueOf()] = entry.modifiedByName;
                    item.entry.calendarverifieduser = [obj];
                }         
            }
        }
	    
        print(count++);
        db.calendar.update({_id: item._id},{$set: {'entry.calendarverifieduser': item.entry.calendarverifieduser}})
    }
})

// ===================================================================================

db.getCollection('calendar').find({entry:{"$exists":true},
"master._id":{"$exists":true},
"entry.schedulestatus":{"$ne":"Completed"},
})
.forEach(function(calendar){
    var master = db.getCollection('masterqcsettings').findOne({_id:calendar.master._id});
    var schemaIds = [];
    master.schema.forEach(function(schema){
             schemaIds.push(schema._id)   
        });
        
    if(calendar.entry.masterschemaIds){
        var notInMaster = [];
        var isNotInMaster = false;
        calendar.entry.masterschemaIds.forEach(function(masterId){
                if(schemaIds.indexOf(masterId)==-1){
                        notInMaster.push(masterId);
                        isNotInMaster = true;
                }
        });
        
        var notInCalendar = [];
        var isNotInCalendar = false;
        schemaIds.forEach(function(schemaId){
                if(calendar.entry.masterschemaIds.indexOf(schemaId)==-1){
                        notInCalendar.push(schemaId);
                        isNotInCalendar = true;
                }
        });
    }
    
    if(isNotInCalendar == false){
       db.getCollection('calendar')
            .update({_id:calendar._id},{
                 "$set":{
                    "entry.schedulestatus":"Completed",
                    "entry.completedDate":new Date()
                 }});
        print({
            notInMaster:notInMaster,
            isNotInMaster:isNotInMaster,
            notInCalendar:notInCalendar,
            isNotInCalendar:isNotInCalendar,
            calendar:calendar
            })
    }
})

// ===========================================================
db.getCollection('calendar').find({ "master":{"$exists":true} },{"master":1}).forEach(function(a){

  db.getCollection('masterqcsettings').find({ "_id":ObjectId(a.master._id.valueOf())}).forEach(function(b){
    b.schema.forEach(function(c){

      db.getCollection(c._id).find({"entry":{"$exists":true}},{"entry":1}).forEach(function(d){
        if(d && d.entry){
          var calIds=[];;
          d.entry.calendarIds.forEach(function(cid){
            calIds.push(cid.valueOf());
          })
          db.getCollection(c._id).update({"_id":d._id},{"$set":{"calendarIds":calIds, "calendarverifieduser":d.entry.calendarverifieduser},"$unset":{"entry":""}});
        }
      });
    });
  });
});
// ==========================================================
db.getCollection('calendar').find({ "master":{"$exists":true} },{"master":1}).forEach(function(a){
  //print(a._id+"a:"+a.master._id);
  db.getCollection('masterqcsettings').find({ "_id":ObjectId(a.master._id.valueOf())}).forEach(function(b){
    b.schema.forEach(function(c){
      var d=db.getCollection(c._id).count({"entry":{"$exists":true}},{"entry":1});
      if(d && d>0){
        print(c._id+"title::"+c.title+"="+d);
      }
    });
  });
});

// =========================================================
db.getCollection('calendar').find({ismasterQcForm:true,entry:{$exists:true}},{"entry":true}).count();
// =================================================
var count = 1;
db.getCollection('calendar').find({ismasterQcForm:true,entry:{$exists:true}},{"entry":true}).forEach(function(obj){
  var array = [];
  obj.entry.masterschemaIds.forEach(function(item){
    db[item].find({'calendarIds.0': obj._id.valueOf()},{modifiedDate:1}).forEach(function(entry){
      array.push({schemaId:item,modifiedDate:entry.modifiedDate});
    });
  });
  var schemaCompletedDate = array.reduce(function(obj,item){ obj[item.schemaId] = item.modifiedDate; return obj},{});
  print(count++);
  db.getCollection('calendar').update({_id:obj._id},{$set:{'entry.schemaCompletedDate':schemaCompletedDate}});
});

// ===================================================
db.calendar.find({ismasterQcForm: true,entry: {$exists:true}}).count()

var count = 1;
db.calendar.find({ismasterQcForm: true,entry: {$exists:true}}).toArray().forEach(function(item,index){
  if(item.entry.calendarverifieduser && !Array.isArray(item.entry.calendarverifieduser)){
    for(var schemaId in item.entry.calendarverifieduser){
      var entry = db[schemaId+''].findOne({'calendarIds': {$in:[item._id+'']}});
      if(entry && entry.modifiedBy){
        var user = db.users.findOne({_id: ObjectId(entry.modifiedBy.valueOf())}, {firstname: 1, lastname: 1, username: 1});
        if(user && user.firstname && user.lastname && user.username){
          var modifiedByName = user.firstname + ' ' + user.lastname + ' (' + user.username + ')';
          print(modifiedByName);
          var obj = {};
          obj[item._id.valueOf()] = modifiedByName;
          item.entry.calendarverifieduser[schemaId] = [obj];
        }
      }

    }
    print(count++);
    db.calendar.update({_id: item._id},{$set: {'entry.calendarverifieduser': item.entry.calendarverifieduser}})
  }
});


// ------------------------------------ Added by Vaibhav -----------------------------

//sudo mongo --host 127.0.0.1 --port 27017 --username mda-staging-20180306 --password DevDanav --authenticationDatabase mda-staging-20180306


// ===========================================================


db.getCollection('calendar').find({ "master":{"$exists":true} },{"master":1}).forEach(function(a){
    
      db.getCollection('masterqcsettings').find({ "_id":ObjectId(a.master._id.valueOf())}).forEach(function(b){
            b.schema.forEach(function(c){
            
                   db.getCollection(c._id).find({"entry":{"$exists":true}},{"entry":1}).forEach(function(d){
                        if(d && d.entry){
                            var calIds=[];;
                            d.entry.calendarIds.forEach(function(cid){
                                calIds.push(cid.valueOf());
                            })
                            db.getCollection(c._id).update({"_id":d._id},{"$set":{"calendarIds":
calIds,
                                "calendarverifieduser":d.entry.calendarverifieduser},"$unset":{"entry":""}
                                });
                            }
                        });
                
          });
        });
    });

// ==========================================================
db.getCollection('calendar').find({ "master":{"$exists":true} },{"master":1}).forEach(function(a){
     db.getCollection('masterqcsettings').find({ "_id":ObjectId(a.master._id.valueOf())}).forEach(function(b){
            b.schema.forEach(function(c){
               var d=db.getCollection(c._id).count({"entry":{"$exists":true}},{"entry":1});
                if(d && d>0){
                print(c._id+"title::"+c.title+"="+d);
                }
            });
        });
});

// =========================================================
db.getCollection('calendar').find({ismasterQcForm:true,entry:{$exists:true},"entry.masterschemaIds":{$exists:true}},{"entry":true}).count();
// =================================================
var count = 1;
db.getCollection('calendar').find({ismasterQcForm:true,entry:{$exists:true},"entry.masterschemaIds":{$exists:true}},{"entry":true}).forEach(function(obj){
 var array = [];  

 obj.entry.masterschemaIds.forEach(function(item){
     db[item].find({'calendarIds.0': obj._id.valueOf()},{modifiedDate:1}).forEach(function(entry){
         array.push({schemaId:item,modifiedDate:entry.modifiedDate});
     });  
 });
 var schemaCompletedDate = array.reduce(function(obj,item){ obj[item.schemaId] = item.modifiedDate; return obj},{});
 print(count++);
 db.getCollection('calendar').update({_id:obj._id},{$set:{'entry.schemaCompletedDate':schemaCompletedDate}});
});

// ===================================================
db.calendar.find({ismasterQcForm: true,entry: {$exists:true}}).count()

var count = 1;
db.calendar.find({ismasterQcForm: true,entry: {$exists:true}}).toArray().forEach(function(item,index){
    if(item.entry.calendarverifieduser && !Array.isArray(item.entry.calendarverifieduser)){
        for(var schemaId in item.entry.calendarverifieduser){
            var entry = db[schemaId+''].findOne({'calendarIds': {$in:[item._id+'']}});
            if(entry && entry.modifiedBy){
                var user = db.users.findOne({_id: ObjectId(entry.modifiedBy.valueOf())}, {firstname: 1, lastname: 1, username: 1});
                if(user && user.firstname && user.lastname && user.username){
                    var modifiedByName = user.firstname + ' ' + user.lastname + ' (' + user.username + ')';
                    print(modifiedByName);
                    var obj = {};
                    obj[item._id.valueOf()] = modifiedByName;
                    item.entry.calendarverifieduser[schemaId] = [obj];
                }  
            }
            
        }
        print(count++);
        db.calendar.update({_id: item._id},{$set: {'entry.calendarverifieduser': item.entry.calendarverifieduser}})
    }
});

