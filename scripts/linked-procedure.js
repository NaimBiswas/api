var finalObj = [];
db.procedures.find().forEach(function (item) {
  if (typeof item.questions != "undefined" && typeof item.questions != "undefined") {
    item.questions.forEach(function (subitem) {
      var obj = {
        linkedToId: (item._id).valueOf(),
        linkedToType: 'procedures',
        linkedToName: item.title,
        linkedToVersion: item.version,
        linkedToStatus: item.isActive,
        linkedOfType: 'questions',
        linkedOfId: subitem._id,
        linkedOfName: subitem.title,
        linkedOfVersion: subitem.version,
      }
      finalObj.push(obj);
    })
  }
})
print(finalObj);
db.linkedTbl.insertMany(finalObj);
