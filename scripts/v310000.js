// update all entity records with isDeleted: false flag

db.entities.find().forEach(function (item) {
   db[item._id.valueOf()].updateMany({}, {
       $set: {
           isDeleted: false
       }
   });
   printjson(item.title + ' is Updated')
});
