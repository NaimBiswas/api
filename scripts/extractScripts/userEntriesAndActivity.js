//For uniqUsers details verify script: loggedInUsers.js
/**
* @name Get List of App Entries & User Logging Activities
* @author Prachi Thakkar
*
* @version 0.0.0
*/
'use strict'

var express = require('express');
var db = require('../../lib/db');
var router = express.Router();
var _ = require('lodash');
var fs = require('fs');
var q = require('q');
var moment = require('moment');

function getApp() {
  var deferred = q.defer();
  db.collection("schema").find({},{isActive:1, title:1}).toArray(function (error, apps) {
    deferred.resolve(apps)
  });
  return deferred.promise;
}

function getRecords(data, updatedData) {
  var obj = {};
  var deferred = q.defer();
  if (data.length > 0) {
    var schema = data.shift();
    db.collection(schema._id+'').find({}, {'_id':1}).toArray(function (error, records) {
      obj['App Name'] = schema.title;
      obj['App Entries'] = records.length;
      obj['App Status'] = schema.isActive ? 'true' : 'false';
      updatedData.push(_.cloneDeep(obj));
      getRecords(data, updatedData).then(function(data){
        deferred.resolve(data);
      });
    });
  }else {
    deferred.resolve(updatedData);
  }
  return deferred.promise;
}

// http://localhost:3000/extractScripts/getAppRecordCount
router.get('/getAppRecordCount', function (req, res, next) {
  getApp().then(function(apps) {
    var deferred = q.defer();
    if (apps && apps.length) {
      getRecords(apps,[]).then(function(result) {
        res.status(200).send({success:true, data: result});
      });
    }
    return deferred.promise;
  });
});

function getUserLoggingAttempts() {
  var deferred = q.defer();
  db.collection("loginattempts").find({date: {$gte: new Date("2017-12-18T00:00:00Z"), $lt: new Date("2017-12-28T23:59:59Z")}},{username:1, email:1, date:1, access:1}).sort({date: -1}).toArray(function (error, users) {
    deferred.resolve(users)
  });
  return deferred.promise;
}

// http://localhost:3000/extractScripts/loggedInUserActivity
router.get('/loggedInUserActivity', function (req, res, next) {
  getUserLoggingAttempts().then(function(users) {
    var result = {};
    var grantedAccess = users.reduce(function(a, c) {
      if (c.access == "granted") {
        a[c.username] = (a[c.username] || 0) + 1;
      }
      return a;
    }, {});
    var deniedAccess = users.reduce(function(a, c) {
      if (c.access == "denied") {
        a[c.username] = (a[c.username] || 0) + 1;
      }
      return a;
    }, {});
    result = {
      succefullLoginAttepts : grantedAccess,
      unsuccefullLoginAttepts : deniedAccess
    }
    res.status(200).send({success:true, data: result});
  });
});

module.exports = router;
