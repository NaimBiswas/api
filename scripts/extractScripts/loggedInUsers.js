//User Logging Activity
//Created By : Prachi
// mongodb://localhost:27017/mda

var mongoskin = require('mongoskin');
var json2xls = require('json2xls');
var _ = require('lodash');
var fs = require('fs');
var q = require('q');
var moment = require('moment');
// try 52.33.42.144 or 52.10.138.186
var db = mongoskin.db('mongodb://hoxworth:B3p3rf3cta_x2y@52.33.42.144:27017/hoxworth', {
  native_parser: true,
  'auto_reconnect': true,
  'poolSize': 1000,
  socketOptions: {
    keepAlive: 500,
    connectTimeoutMS: 50000,
    socketTimeoutMS: 0
  }
});

function getUserLoggingAttempts() {
  var deferred = q.defer();
  db.collection("loginattempts").find({access: 'granted', date: {$gte: new Date("2019-03-04T00:00:00Z"), $lt: new Date("2019-03-08T23:59:59Z")}},{username:1, email:1, date:1}).sort({date: -1}).toArray(function (error, users) {
    deferred.resolve(users)
  });
  return deferred.promise;
}

var obj = {};
getUserLoggingAttempts().then(function(users) {
  var uniqUsers = _.uniqBy(users, 'username');
  _.forEach(uniqUsers, function(user, index) {
    uniqUsers[index].date = moment(uniqUsers[index].date).format('MM/DD/YYYY hh:mm:ss');
  });
  var xls = json2xls(uniqUsers);
  fs.writeFileSync('HOXWORTH-Live_UserLogging.xlsx', xls, 'binary');
  console.log("Created Successfully");
});
