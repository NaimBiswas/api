//QC3-4884 Backlog - The script to extract data when requested by MDA
//Freshdesk ID : 1437 // Need to ask
//Created By : Vaibhav Vora
//For Output Need to run command : node scripts/filename.js
//Get list of All apps that trigger an Equipment Problem Log, All apps that trigger an Investigation Log
//Some apps can trigger either one, depending on the rules that are set up.  Example app = Infectious Disease ULTRIO Reagent Qualification.

var mongoskin = require('mongoskin');
var json2xls = require('json2xls'); var _ = require('lodash');
var fs = require('fs');
var q = require('q')
var _ = require('lodash');
var moment = require('moment');
var db = mongoskin.db('mongodb://mda-preval:B3p3rf3cta_x2y@52.24.57.147/mda-preval', {
  // var db = mongoskin.db('mongodb://localhost:27017/mda-staging-11jan', {
 //   var db = mongoskin.db('mongodb://localhost:27017/roles', {
  native_parser: true,
  'auto_reconnect': true,
  'poolSize': 1000,
  socketOptions: {
    keepAlive: 500,
    connectTimeoutMS: 50000,
    socketTimeoutMS: 0
  }
});

var result = [];
var getType = {};
var data="";
function test() {
  var deferred = q.defer();
  db.collection('types').find({"title": 'Textbox'}, {}).toArray(function (error, types) {
    getType = types[0];
    deferred.resolve(getType)
  });
  return deferred.promise;
}
function tp(){
  var deferred = q.defer();
  db.collection('schema').find({}, { procedures: 1, title: 1 }).toArray(function (error, schema) {
    deferred.resolve(schema)
  });
  return deferred.promise;
};

test().then(function (getType) {


  var schema = db.collection("schema")
  .find({}, { procedures: 1, title: 1 });

  tp().then(function(schema){
    _.forEach(schema, function(s){

      _.forEach(s.procedures, function(p){

        _.forEach(p.conditionalWorkflows, function(q){

          // Equipment Problem Log : 588674a079923d0fa8419127
          // Investigation Log : 5862108b7c36264210511cf2

          if (q.conditions  && q.selctedType=='Initiate App' && (q.appToExpand._id=='5862108b7c36264210511cf2' || q.appToExpand._id=='588674a079923d0fa8419127')) {
              if( q.appToExpand  ){
                 if(q.conditions.length>1){
                   var conditions='';
                    var data = "";
                    var ObjResult = {};
                    _.forEach(q.conditions, function(c, i){
                        if(c.operator && (i!=0)){
                          if(c.operator == '||'){
                            conditions=conditions+" OR ";
                          }else if(c.operator == '&&'){
                            conditions=conditions+" AND ";
                          }else{
                            conditions=conditions+" AND ";
                          }
                        }
                          conditions=conditions+c.question.title;
                          conditions=conditions+" is";
                          conditions=conditions+((c.expectedResult == '2'?(' '+displayProper(c)) :(c.expectedResult==1?' Pass':' Fail')));

                   });
                     // parser(c,q);
                      data="When ";
                      data=data+conditions;
                      data=data+' THEN';
                     if(q.selctedType=='Initiate App'){
                        data=data+' Initiate App ';
                      }
                      data=data+q.appToExpand.title
                      ObjResult['App Title'] = s.title;
                      ObjResult['Procedure Title'] = p.title;
                      ObjResult['Question Title'] = q.title;
                      ObjResult['rule'] = data;
                      ObjResult['Initiate App Title']=q.appToExpand.title.substring(0,q.appToExpand.title.lastIndexOf(" "));
                      result.push(ObjResult);

                }else{
                    _.forEach(q.conditions, function(c, i){
                      var ObjResult = {};
                      var data = "";
                      //parser(c,q);
                      data="When ";
                      data=data+c.question.title;
                      data=data+" is";
                      data=data+ ((c.expectedResult == '2'?(' '+displayProper(c)) :(c.expectedResult==1?' Pass':' Fail')));
                      data=data+' THEN';
                      if(q.selctedType=='Initiate App'){
                        data=data+' Initiate App ';
                      }
                      data=data+q.appToExpand.title;
                      ObjResult['App Title'] = s.title;
                      ObjResult['Procedure Title'] = p.title;
                      ObjResult['Question Title'] = q.title;
                      ObjResult['rule'] = data;
                      ObjResult['Initiate App Title']=q.appToExpand.title.substring(0,q.appToExpand.title.lastIndexOf(" "));
                      result.push(ObjResult);
                   });
                }
              }


          }

        });
      });

    });
    var xls = json2xls(result);
    fs.writeFileSync('mda-preval.xlsx', xls, 'binary');
    console.log("Created Successfully");
  });


});

function getDateFormat(formatId) {
  var allowItems = _.reduce(getType.formats, function (o, obj) {
    if (obj.title === 'Date') {
      o.push(obj.metadata[0].allowItems)
    }
    return o;
  }, [])[0];
  var dateFormt = _.reduce(allowItems, function (o, item) {
    if (item._id === formatId) {
      o.push(item);
    }
    return o;
  }, [])[0];
  if (dateFormt) {
    if (dateFormt['title'] === 'DD/MM/YYYY') {
      return 'dd/MM/yyyy';
    } else if (dateFormt['title'] === 'MM/DD/YYYY') {
      return 'MM/dd/yyyy';
    } else if (dateFormt['title'] === 'YYYY/MM/DD') {
      return 'yyyy/MM/dd';
    } else if (dateFormt['title'] === 'MM/DD/YY') {
      return 'MM/dd/yy';
    } else if (dateFormt['title'] === 'DD/MMM/YY') {
      return 'dd/MMM/yy';
    } else if (dateFormt['title'] === 'MONTH/DD/YYYY') {
      return 'MMMM/dd/yyyy';
    } else if (dateFormt['title'] === 'DD/MMM/YYYY') {
      return 'dd/MMM/yyyy';
    }
    return '';
  } else {
    return '';
  }
}

function getTimeFormat(formatId) {
  var allowItems = _.reduce(getType.formats, function (o, obj) {
    if (obj.title === 'Time') {
      o.push(obj.metadata[0].allowItems)
    }
    return o;
  }, [])[0];
  var timeFormt = _.reduce(allowItems, function (o, item) {
    if (item._id === formatId) {
      o.push(item);
    }
    return o;
  }, [])[0];

  var returnValue = [];
  if (timeFormt) {
    if (timeFormt['title'] === 'HH:MM') {
      returnValue[0] = false;
      returnValue[1] = false;
    } else if (timeFormt['title'] === 'HH:MM AM/PM') {
      returnValue[0] = true;
      returnValue[1] = false;
    } else if (timeFormt['title'] === 'HH:MM:SS') {
      returnValue[0] = false;
      returnValue[1] = true;
    } else if (timeFormt['title'] === 'HH:MM:SS AM/PM') {
      returnValue[0] = true;
      returnValue[1] = true;
    }
  }
  return returnValue;
}
 function displayProper(c) {
      var returnValue = ''
     if(c.value){
        if(Array.isArray(c.value)){
          _.forEach(c.value, function (v) {
            returnValue += v.id + ',';
          })
          return returnValue.substr(0, returnValue.length - 1);
        } else if (c.question && c.question.type.format.title==="Date") {
          if (c.today === '1') {
            return 'Today';
          } else{
             var filterdDate='';
            if (c.question.type.format.metadata.format === '' || !c.question.type.format.metadata.format ||  _.isUndefined(c.question.type.format.metadata.format)) {
              if (_.isUndefined(c.value) || c.value=='Invalid Date' || !c.value || c.value == '') {
                return '';
              }
              return moment(moment.utc(c.value).toDate()).format('MM/DD/YYYY');
            }else {
              switch(c.question.type.format.metadata.format){

                case 'dd/MM/yyyy':
                filterdDate ='DD/MM/YYYY';
                break;

                case 'MM/dd/yyyy':
                filterdDate = 'MM/DD/YYYY';
                break;

                case 'yyyy/MM/dd':
                filterdDate = 'YYYY/MM/DD';
                break;

                case 'MM/dd/yy':
                filterdDate = 'MM/DD/YY';
                break;

                case 'MONTH/dd/yyyy':
                filterdDate = 'MMMM/DD/YYYY';
                break;

                case 'MMMM/dd/yyyy':
                filterdDate = 'MMMM/DD/YYYY';
                break;

                case 'dd/MMM/yy':
                filterdDate = 'DD/MMM/YY';
                break;

                case 'dd/MMM/yyyy':
                filterdDate = 'DD/MMM/YYYY';
                break;

                default:
                filterdDate = 'MM/DD/YYYY';

              }
              if (_.isUndefined(c.value) || c.value=='Invalid Date' || !c.value || c.value == '') {
                return '';
              }
              return moment(moment.utc(c.value).toDate()).format(filterdDate);
            }
          }

        }
        else {
          return c.value;
        }
      }
    }
