var mongoskin = require('mongoskin');
var json2xls = require('json2xls');
var _ = require('lodash');
var fs = require('fs');
var q = require('q')
// var db = mongoskin.db('mongodb://localhost:27017/mda-staging-11jan', {
var db = mongoskin.db('mongodb://mda-prod-20180327:DevDanav@192.168.1.188:27017/mda-prod-20180327', {
  native_parser: true,
  'auto_reconnect': true,
  'poolSize': 1000,
  socketOptions: {
    keepAlive: 500,
    connectTimeoutMS: 50000,
    socketTimeoutMS: 0
  }
});

function getApp() {
  var deferred = q.defer();
  db.collection("schema").find({isActive:true},{title:1, isActive:1, entities: 1}).toArray(function (error, apps) {
    deferred.resolve(apps)
  });
  return deferred.promise;
}

function getAppRecords(apps) {
  var getTestData = getTestData || {};
  _.forEach(apps, function(app) {
    if (app && app.entities && app.entities.length) {
      var value = app._id + "";
      getTestData[value] = getTestData[value] || {};
      db.collection(value).find({}).toArray(function(err, data) {
        if (data && data.length) {

          _.forEach(data, function(mmm) {
            var getSeq = _.map(app.entities, 's#');
            mmm['entries'] = _.omit(mmm['entries'], 'isattributesadded', 'status', 'settings', 'FailMesssageForQCForm', 'appToExpandMessage', 'draftStatus', 'islinkcalendar', 'errorAppDataStore', 'mappedentries', 'tempAppDataStore');
            _.forEach(getSeq, function(seq) {
              _.forEach(mmm['entries'], function(op, key) {
                if (key && key.endsWith('-'+seq)) {
                  if (_.size(op) == 0) {
                    var getEnt = _.find(app.entities, {'s#' : seq});
                    if (getEnt.questions) {
                      var getSelectedDate = _.find(getEnt.questions, {'s#' : key.substring(0,13)});
                      if (getSelectedDate) {
                        console.log('App Title ::  ' + app.title);
                        console.log('App Id ::  ' + app._id);
                        console.log('Entity Title ::  ' + getEnt.title);
                        console.log('Attribute Title ::  ' + getSelectedDate.title);
                        console.log('Record Id ::  ' + mmm._id);
                        console.log('-=-=-Wrong Data Blank-=-=-');
                      }
                    }


                    // if (getEnt.filteredEntQuesSelected) {
                    //   var getSelectedDate = _.find(getEnt.filteredEntQuesSelected, {id : key.substring(0,13)});
                    //   if (getSelectedDate) {
                    //     console.log('App Title ::' + app.title);
                    //     console.log('App Id ::' + app._id);
                    //     console.log('Record Id ::' + mmm._id);
                    //     console.log('-=-=-Blank-=-=-');
                    //   }else {
                    //     console.log('No Data Found');
                    //   }
                    // }
                  }
                }
              });
            });
          });
        }
      });
    }
  });
}

getApp().then(function(apps) {
  // var test = '1476290766419-1476292276098';
  // console.log(test);
  // console.log(test.substring(0,13));
  getAppRecords(apps);
});
