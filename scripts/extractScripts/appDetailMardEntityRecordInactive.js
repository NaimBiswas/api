//QC3-4884 Backlog - The script to extract data when requested by MDA
//Freshdesk ID : Not Available
//Created By : Prachi
//For Output Need to run command : node scripts/filename.js
//list of apps that have the “mark entity records as inactive” option checked

var mongoskin = require('mongoskin');
var json2xls = require('json2xls'); var _ = require('lodash');
var fs = require('fs');
var q = require('q')
var _ = require('lodash');
var moment = require('moment');
var db = mongoskin.db('mongodb://mda-preval:B3p3rf3cta_x2y@52.24.57.147/mda-preval', {
  //  var db = mongoskin.db('mongodb://localhost:27017/mda-staging-11jan', {
  native_parser: true,
  'auto_reconnect': true,
  'poolSize': 1000,
  socketOptions: {
    keepAlive: 500,
    connectTimeoutMS: 50000,
    socketTimeoutMS: 0
  }
});

var getApp = [];
db.collection("schema").find({}, { entities: 1, title: 1, isActive:1 }).toArray(function (error, apps) {
  _.forEach(apps, function(app) {
    if (app && app.entities) {
      _.forEach(app.entities, function(entity) {
        if (entity && entity.markEntityValueInActive && entity.markEntityValueInActive == true) {
          // if (app.isActive) {
          getApp.push({'App Name' : app.title,'Entity Name' : entity.title, 'App Status' : app.isActive});
          // }
        }
      });
    }
  });
  console.log(getApp);
});
