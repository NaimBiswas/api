//Count of App Entries
//Created By : Prachi

var mongoskin = require('mongoskin');
var json2xls = require('json2xls');
var _ = require('lodash');
var fs = require('fs');
var q = require('q')
//db : 'mongodb://mda:B3p3rf3cta_x2y@172.31.3.204:27017,172.31.19.191:27017/mda?replicaSet=rs0'
//mongodb://dbadmin:P3rf3qta_DbAdm1n@34.214.245.163:27017/mda
var db = mongoskin.db('mongodb://mda:B3p3rf3cta_x2y@34.214.245.163:27017/mda?replicaSet=rs0', {
  native_parser: true,
  'auto_reconnect': true,
  'poolSize': 1000,
  socketOptions: {
    keepAlive: 500,
    connectTimeoutMS: 50000,
    socketTimeoutMS: 0
  }
});

function getApp() {
  var deferred = q.defer();
  db.collection("schema").find({$or : [ {title:/Annual/i},{title:/Bi-Annual/i},{title:/Quarterly/i},{title:/As Needed/i}]},{isActive:1, title:1, procedures:1}).toArray(function (error, apps) {
    deferred.resolve(apps)
  });
  return deferred.promise;
}

var obj = {};
getApp().then(function(apps) {
  var deferred = q.defer();
  var data = [];
  if (apps && apps.length) {
    _.forEach(apps, function(app){
      _.forEach(app.procedures, function(proc){
        _.forEach(proc.questions, function(q){
          data.push({
            "App Title" : app.title,
            "Procedure Title" : proc.title,
            "Question Title" : q.title
          })
        })
      })
    })
    var xls = json2xls(data);
      fs.writeFileSync('MDA-Live.xlsx', xls, 'binary');
      console.log("Created Successfully");
  }
  return deferred.promise;
});
