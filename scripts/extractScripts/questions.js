//QC3-4884 Backlog - The script to extract data when requested by MDA
//Freshdesk ID : 1475
//Created By : Prachi
//For Output Need to run command : node scripts/filename.js
//Extract of all Questions and Apps using °C & °C

var mongoskin = require('mongoskin');
var json2xls = require('json2xls'); var _ = require('lodash');
var fs = require('fs');
var q = require('q')
var _ = require('lodash');
var moment = require('moment');
var db = mongoskin.db('mongodb://mda-staging:B3p3rf3cta_x2y@52.24.57.147/mda-staging', {
  //  var db = mongoskin.db('mongodb://localhost:27017/mda-staging-11jan', {
  native_parser: true,
  'auto_reconnect': true,
  'poolSize': 1000,
  socketOptions: {
    keepAlive: 500,
    connectTimeoutMS: 50000,
    socketTimeoutMS: 0
  }
});

var getListOfQuestionsMotu = [];
var getListOfQuestionsNanu = [];
db.collection("questions").find().toArray(function (error, questions) {
  _.forEach(questions, function(q) {
    if (q.type.format && q.type.format.metadata && q.type.format.metadata.MeasurementUnit == "°C") {
      getListOfQuestionsMotu.push(q.title);
    }else if (q.type.format && q.type.format.metadata && q.type.format.metadata.MeasurementUnit == "˚C") {
      getListOfQuestionsNanu.push(q.title);
    }
  });
  console.log(getListOfQuestionsMotu);
  console.log(getListOfQuestionsNanu);
});

// var getListOfQuestionsMotu = [];
// var getListOfQuestionsNanu = [];
// db.collection("schema").find().toArray(function (error, apps) {
//   _.forEach(apps, function (app) {
//     _.forEach(app.procedures, function (proc) {
//       _.forEach(proc.questions, function(q) {
//         if (q.type.format && q.type.format.metadata && q.type.format.metadata.MeasurementUnit == "°C") {
//           getListOfQuestionsMotu.push({
//             'App Name' : app.title,
//             'Procedure Name' : proc.title,
//             'Question Name' : q.title
//           });
//         }else if (q.type.format && q.type.format.metadata && q.type.format.metadata.MeasurementUnit == "˚C") {
//           getListOfQuestionsNanu.push({
//             'App Name' : app.title,
//             'Procedure Name' : proc.title,
//             'Question Name' : q.title
//           });
//         }
//       });
//     });
//   });
//   console.log(getListOfQuestionsMotu);
//   console.log(getListOfQuestionsNanu);
// });
