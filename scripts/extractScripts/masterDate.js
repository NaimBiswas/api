//QC3-4884 Backlog - The script to extract data when requested by MDA
//Freshdesk ID : Not Available
//Created By : Prachi
//For Output Need to run command : node scripts/filename.js
//Get Master App and All user App in That Master App (Detail) / Single App details

var mongoskin = require('mongoskin');
var json2xls = require('json2xls');var _ = require('lodash');
var fs = require('fs');
var q = require('q')
var db = mongoskin.db('mongodb://localhost:27017/roles', {
  native_parser: true,
  'auto_reconnect': true,
  'poolSize': 1000,
  socketOptions: {
    keepAlive: 500,
    connectTimeoutMS: 50000,
    socketTimeoutMS: 0
  }
});

//Get Master App and All user App in That Master App (Detail)

var result = [];

function test() {
  var deferred = q.defer();
  db.collection('schema').find({},{ title:1, _id:1}).toArray(function (error, schema) {
    deferred.resolve(schema)
  });
  return deferred.promise;
}

//For Master App and containing single apps
test().then(function(schema) {
  var master = db.collection("masterqcsettings")
  .find({isActive:true},{ schema:1,title:1 });
  master.forEach(function(t){
    t.schema.forEach(function(e){
      var ObjResult = {};
      ObjResult['Master Title'] = t.title;
      ObjResult['App Title'] = '';
      schema.forEach(function(s) {
        if (s._id == e._id) {
          ObjResult['App Title'] = s.title;
        }
      });
      result.push(ObjResult);
    });
    var xls = json2xls(result);
    fs.writeFileSync('Production_MDA.xlsx', xls, 'binary');
    console.log("Created Successfully");
  });
});

//For getting all the single apps
test().then(function(schema) {
  var ObjResult = {};
  ObjResult['App Title'] = '';
  schema.forEach(function(s) {
    ObjResult['App Title'] = s.title;
    console.log(ObjResult);
  });
});
