//Count of App Entries
//Created By : Prachi

var mongoskin = require('mongoskin');
var json2xls = require('json2xls');
var _ = require('lodash');
var fs = require('fs');
var q = require('q')
//db : 'mongodb://mda:B3p3rf3cta_x2y@172.31.3.204:27017,172.31.19.191:27017/mda?replicaSet=rs0'
//mongodb://dbadmin:P3rf3qta_DbAdm1n@34.214.245.163:27017/mda
var db = mongoskin.db('mongodb://mda:B3p3rf3cta_x2y@52.33.42.144:27017/mda', {
// var db = mongoskin.db('mongodb://localhost:27017/web', {
  native_parser: true,
  'auto_reconnect': true,
  'poolSize': 1000,
  socketOptions: {
    keepAlive: 500,
    connectTimeoutMS: 50000,
    socketTimeoutMS: 0
  }
});

function getApp() {
  var deferred = q.defer();
  db.collection("schema").find({},{isActive:1, title:1}).toArray(function (error, apps) {
    deferred.resolve(apps)
  });
  return deferred.promise;
}

var obj = {};
getApp().then(function(apps) {
  var deferred = q.defer();
  if (apps && apps.length) {
    getRecords(apps,[]).then(function(result) {
      var xls = json2xls(result);
      fs.writeFileSync('MDA-Live.xlsx', xls, 'binary');
      console.log("Created Successfully");
    });
  }
  return deferred.promise;
});

function getRecords(data, updatedData) {
  var deferred = q.defer();
  if (data.length > 0) {
    var schema = data.shift();
    db.collection(schema._id+'').find({batchEntryId: {$exists: true}}).toArray(function (error, batchRecords) {
      db.collection(schema._id+'').find({batchEntryId: {$exists: false}}, {'_id':1}).toArray(function (error, records) {
        db.collection(schema._id+'').find({}, {'_id':1}).toArray(function (error, allRecords) {
          var test = {};
          test[schema._id] = [];
          if (batchRecords && batchRecords.length) {
            batchRecords.forEach(function(entry) {
              test[schema._id].push(entry.batchEntryId);
            });
            if (test[schema._id] && test[schema._id].length) {
              var batchCount = _.uniqWith(test[schema._id], _.isEqual);
              obj['App Name'] = schema.title;
              obj['App Status'] = schema.isActive ? 'true' : 'false';
              obj['Batch Count'] = batchCount.length;
              obj['Batch Entries'] = batchRecords.length;
              obj['App Entries'] = records.length;
              obj['All App Entries'] = allRecords.length;
            }else {
              obj['App Name'] = schema.title;
              obj['App Status'] = schema.isActive ? 'true' : 'false';
              obj['Batch Count'] = test[schema._id].length;
              obj['Batch Entries'] = batchRecords.length;
              obj['App Entries'] = records.length;
              obj['All App Entries'] = allRecords.length;
            }
          }else {
            obj['App Name'] = schema.title;
            obj['App Status'] = schema.isActive ? 'true' : 'false';
            obj['Batch Count'] = test[schema._id].length;
            obj['Batch Entries'] = batchRecords.length;
            obj['App Entries'] = records.length;
            obj['All App Entries'] = allRecords.length;
          }
          updatedData.push(_.cloneDeep(obj));
          getRecords(data, updatedData).then(function(data){
            deferred.resolve(data);
          });
        });
      });
    });
  }else {
    deferred.resolve(updatedData);
  }
  return deferred.promise;
}
