db.resources.insert({
title: "Entity Records",
entity: "entityRecords",
isActive: true
})

db.generalSettings.update({},{$set: {"hideProcTitle" : false}})

db.settings.update({},{$set: {truncateDecimal: false}})

db.versions.insert({"productVersion":"v3.0.0.33","web":{"major":"3","minor":"0","build":"0","revision":"33"},"api":{"major":"3","minor":"0","build":"0","revision":"33"},"db":{"major":"3","minor":"0","build":"0","revision":"33"},"report":{"major":"3","minor":"0","build":"0","revision":"33"},"reportApi":{"major":"3","minor":"0","build":"0","revision":"33"}})


