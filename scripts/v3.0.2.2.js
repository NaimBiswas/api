//Script to Update Question, Procedure, Schema  for Question type File Attachment, selectedSet blank to Proper Data
//update question of type File Attachment with Set Value.
db.getCollection('questions').updateMany({'type.title' : 'File Attachment','validation.validationSet.selectedFileFormat' : []},{$set : {
    'validation.validationSet.selectedFileFormat' :[ 
                {
                    "id" : "pdf"
                }, 
                {
                    "id" : "csv"
                }, 
                {
                    "id" : "xls/xlsx"
                }, 
                {
                    "id" : "png"
                }, 
                {
                    "id" : "bmp"
                }, 
                {
                    "id" : "jpg/jpeg"
                }, 
                {
                    "id" : "ppt/pptx"
                }, 
                {
                    "id" : "doc/docx"
                }
            ]
    }});
    
    
    
    //update All Procedure, question of type = file Attachment
    
    db.getCollection('procedures').updateMany({'questions.type.title' : 'File Attachment','questions.validation.validationSet.selectedFileFormat' : []},
{$set : {'questions.$.validation.validationSet.selectedFileFormat' : [ 
                {
                    "id" : "pdf"
                }, 
                {
                    "id" : "csv"
                }, 
                {
                    "id" : "xls/xlsx"
                }, 
                {
                    "id" : "png"
                }, 
                {
                    "id" : "bmp"
                }, 
                {
                    "id" : "jpg/jpeg"
                }, 
                {
                    "id" : "ppt/pptx"
                }, 
                {
                    "id" : "doc/docx"
                }
            ]
                }});
                
                
                
                
//update Schema.Procedure Question File Attachment
db.getCollection('schema').find({'procedures.questions.type.title' : 'File Attachment','procedures.questions.validation.validationSet.selectedFileFormat' : []}).forEach(function(dt){
    dt.procedures.forEach(function(pt,pidx){
        pt.questions.forEach(function(qt,qidx){
            if(qt.type.title == 'File Attachment'){
                qt.validation.validationSet.selectedFileFormat = [ 
                {
                    "id" : "pdf"
                }, 
                {
                    "id" : "csv"
                }, 
                {
                    "id" : "xls/xlsx"
                }, 
                {
                    "id" : "png"
                }, 
                {
                    "id" : "bmp"
                }, 
                {
                    "id" : "jpg/jpeg"
                }, 
                {
                    "id" : "ppt/pptx"
                }, 
                {
                    "id" : "doc/docx"
                }
            ]
                }
            });
        });
        db.schema.updateMany({'title' : dt.title},{$set : {procedures : dt.procedures}});
    });
