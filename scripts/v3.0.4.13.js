------ please check before execute ------------

db.getCollection('resources').insertMany([{
 "title" : "Report Templates",
 "entity" : "reportTemplates",
 "isActive" : true
},
{
 "title" : "Report Setting",
 "entity" : "reportSettings",
 "isActive" : true
},
{
 "title" : "Custom Report",
 "entity" : "customReportColumnDetail",
 "isActive" : true
},
{
 "title" : "Control Chart Parameters",
 "entity" : "controlChartParameters",
 "isActive" : true
}])
