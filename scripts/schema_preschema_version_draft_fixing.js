db.getCollection('schema').find({},{version: 1, isDraft: 1}).forEach(function (schema) {
   var isdraftStatus = (schema.version % 1 === 0)?false: true;
        if (!schema.hasOwnProperty('isDraft')) {
            db.getCollection('schema').updateOne({
                "_id": schema._id
            }, {
                $set: {
                    "isDraft": isdraftStatus
                }
            });
            printjson('Update in ' + schema._id);
        }
    

});
