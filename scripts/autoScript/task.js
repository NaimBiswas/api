'use strict'
/**
* @name task
* @author Surjeet Bhadauriya <surjeet.b@productivet.com>
*
* @version 0.0.0
* @desc Make your method and put it inside the scripts object
*/

var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('../../lib/logger').configure(__dirname, enableBunyan);
var debug = require('../../lib/logger').debug('lib:autoScript');
var db = require('../../lib/db');
var async = require("async");

var scripts = {
  updateUserEmail: updateUserEmail,
  updateSchemaEmail: updateSchemaEmail,
  updateCalendarThresholdsEmail: updateCalendarThresholdsEmail,
  updateThresholdsEmail: updateThresholdsEmail,
  updatedSchedulesEmail: updatedSchedulesEmail,
  updateCalendarEmail: updateCalendarEmail,
  updateLoginAttemptsEmail: updateLoginAttemptsEmail,
  updateThresholdsNotification: updateThresholdsNotification,
  updateGeneralSettingsEmail: updateGeneralSettingsEmail
};

function updateUserEmail() {
  var updateUserCount = 0;
  db.users.find({}, {username: 1, email: 1, newEmail: 1}).forEach(function(user){
    updateUserCount++;
    if(user.email){
      user.email = user.email.trim();
      var startPos = user.email.indexOf('@');
      user.email = user.email.substr(0, startPos + 1) + 'yopmail.com';
    }
    if(user.newEmail){
      user.newEmail = user.email.trim();
      var startPos = user.newEmail.indexOf('@');
      user.newEmail = user.newEmail.substr(0, startPos + 1) + 'yopmail.com';
    }
    db.users.updateOne({_id: user._id},{$set: {email: user.email, newEmail: user.newEmail}});
    console.log(user.username + ' has been updated successfully.');
  });
}

function updateSchemaEmail() {
  db.schema.find().forEach(function(schema){
    if(schema.emailIdForQCFormFail){
      var updatedEmails = [];
      var emails = schema.emailIdForQCFormFail.split(',');
      if(emails){
        emails.forEach(function(e){
          e = e.trim();
          var startPos = e.indexOf('@');
          updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');
        })
      }
      db.schema.updateOne({_id: schema._id},{$set: {emailIdForQCFormFail: updatedEmails.join()}});
      console.log(schema._id + ' has been updated successfully.');
    }
  })
}


function updateCalendarThresholdsEmail() {
  db.calendarThresholds.find().forEach(function(threshold){
    if(threshold.email){
      var updatedEmails = [];
      var emails = threshold.email.split(',');
      if(emails){
        emails.forEach(function(e){
          e = e.trim();
          var startPos = e.indexOf('@');
          updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');
        });
      }
      db.calendarThresholds.updateOne({_id: threshold._id},{$set: {email: updatedEmails.join()}});
      console.log(threshold._id + ' has been updated successfully.');
    }
  });

  db.calendarThresholds.find().forEach(function(calendar){
    if(calendar.default && calendar.default.support){
      var updatedEmails = [];
      var emails = calendar.default.support.split(',');
      if(emails){
        emails.forEach(function(e){
          e = e.trim();
          var startPos = e.indexOf('@');
          updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');
        });
      }
      console.log(updatedEmails.join());
      console.log(calendar._id + ' updated successfully');
      db.calendarThresholds.updateOne({_id: calendar._id},{$set: {"default.support": updatedEmails.join()}});
    }
  });
}


function updateThresholdsEmail() {
  db.thresholds.find().forEach(function(threshold){
    if(threshold.reportconfiguration && threshold.reportconfiguration.emailaddress && threshold.reportconfiguration.emailaddress !== ''){
      var updatedEmails = [];
      var emails = threshold.reportconfiguration.emailaddress.split(',');
      if(emails){
        emails.forEach(function(e){
          e = e.trim();
          var startPos = e.indexOf('@');
          updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');
        });
      }
      db.thresholds.updateOne({_id: threshold._id},{$set: {'reportconfiguration.emailaddress': updatedEmails.join()}});
      console.log(threshold._id + ' has been updated successfully.');
    }
  });
}

function updatedSchedulesEmail() {
  db.schedules.find().forEach(function(schedules){
    if(schedules.email){
      var updatedEmails = [];
      var emails = schedules.email.split(',');

      if(emails){
        emails.forEach(function(e){
          e = e.trim();
          var startPos = e.indexOf('@');
          updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');
        });
      }
      db.schedules.updateOne({_id: schedules._id},{$set: {email: updatedEmails.join()}});
      console.log(schedules._id + ' has been updated successfully.');
    }
    if(schedules.reviewemail){
      var reviewupdatedEmails = [];
      var reviewEmails =schedules.reviewemail.split(',');
      if(reviewEmails){
        reviewEmails.forEach(function(e){
          e = e.trim();
          var startPos = e.indexOf('@');
          reviewupdatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');
        });
      }
      db.schedules.updateOne({_id: schedules._id},{$set: {reviewemail:reviewupdatedEmails.join()}});
      console.log(schedules._id + ' has been updated successfully.');
    }
  });
}

function updateCalendarEmail() {
  db.calendar.find().forEach(function(calendar){
    if(calendar.email){
      var updatedEmails = [];
      var emails = calendar.email.split(',');
      if(emails){
        emails.forEach(function(e){
          e = e.trim();
          var startPos = e.indexOf('@');
          updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');
        });
      }
      db.calendar.updateOne({_id: calendar._id},{$set: {email: updatedEmails.join()}});
      console.log(calendar._id + ' has been updated successfully.');

    }
  });

  db.calendar.find().forEach(function(calendar){
    if(calendar.default && calendar.default.support){
      var updatedEmails = [];
      var emails = calendar.default.support.split(',');
      if(emails){
        emails.forEach(function(e){
          e = e.trim();
          var startPos = e.indexOf('@');
          updatedEmails.push(e.substr(0, startPos + 1) + 'yopmail.com');
        });
      }
      console.log(calendar._id + 'has been updated successfully.');
      db.calendar.updateOne({_id: calendar._id},{$set: {'default.support': updatedEmails.join()}});
    }
  });
}

function updateLoginAttemptsEmail() {
  db.loginattempts.find().forEach(function(loginattempts){
    if(loginattempts.email){
      loginattempts.email = loginattempts.email.trim();
      var startPos = loginattempts.email.indexOf('@');
      loginattempts.email = loginattempts.email.substr(0, startPos + 1) + 'yopmail.com'; }
      db.loginattempts.updateOne({_id: loginattempts._id},{$set: {email: loginattempts.email}});
      console.log(loginattempts._id + ' has been updated successfully.');
    })
  }

  function updateThresholdsNotification() {
    db.thresholdsNotification.find().forEach(function(thresholdsNotification){
      if(thresholdsNotification.email){
        thresholdsNotification.email = thresholdsNotification.email.trim();
        var startPos = thresholdsNotification.email.indexOf('@');
        thresholdsNotification.email = thresholdsNotification.email.substr(0, startPos + 1) + 'yopmail.com';
      }
      db.thresholdsNotification.updateOne({_id: thresholdsNotification._id},{$set: {email: thresholdsNotification.email}});
      console.log(thresholdsNotification._id + ' has been updated successfully.');
    });
  }

  function updateGeneralSettingsEmail() {
    db.generalSettings.find().forEach(function(setting){
      if(setting.supportContactDetails){
        setting.supportContactDetails = setting.supportContactDetails.trim();
        var startPos = setting.supportContactDetails.indexOf('@');
        setting.supportContactDetails = setting.supportContactDetails.substr(0, startPos + 1) + 'yopmail.com';
      }
      db.generalSettings.updateOne({_id: setting._id},{$set: {supportContactDetails: setting.supportContactDetails}});
      console.log(setting._id + ' has been updated successfully.');
    });
  }


  module.exports = scripts;
