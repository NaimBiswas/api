'use strict'
/**
* @name autoScript
* @author Surjeet Bhadauriya <surjeet.b@productivet.com>
*
* @version 0.0.0
*/

var async = require("async");
var task = require('./task');

//generating list of task
var listOfTask = [];
for (var key in task) {
  listOfTask.push(task[key]);
}

//make it async.series([], cb) when all scripts are converted into callback
//async.parallel will cause problem when there are many tasks given to it
/** prerequisites:
@listOfTask: [] array
@cb: callback function
*/
async.parallel(listOfTask, function (err, result) {
  //for now this callback will never going to execute..
  //because all the scripts needs to be updated based on callback.. means after completing the task return a callback...
  //every script should retrun a callback
  if (err) {
    console.log(err);
    return;
  }
  console.log(result);
});
