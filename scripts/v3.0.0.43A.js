// Branch Name : rcbi-lslive-v300443A-20171227

// From now when we add a new attribute to any site with "Date" type then please follow below script rather than using an old one.

{
    "_id" : "111526b7c69852227e677745",
    "title" : "Audit Date",
    "type" : {
        "_id" : "57175257c74217130039948c",
        "title" : "Textbox",
        "format" : {
            "_id" : "123456789012345678901246",
            "title" : "Date",
             "metadata" : {
                "datetime" : "123456789012345678901249"  (This should be in post)
            }
        }
    },
    "isActive" : true,
    "module" : {
        "_id" : "56e6e8b7c6d7c6b31e4ef999"
    }
}

// QC3-9675 Custom report - App - Date attribute not displayed in valid format

// Update Attribute Collection
db.getCollection("attributes").find({'type.format.title':'Date'},{type:1,title:1}).forEach(function(attrib){
    if(!attrib.type.format.metadata.datetime){
        attrib.type.format.metadata.datetime = "123456789012345678901249";
        db.getCollection("attributes").update({ "_id": attrib._id },{ "$set": attrib});
        print('Updated Successfully');
    }
});

// Update Schema Collection
db.schema.find({},{title:1,attributes:1}).forEach(function(app){
    app.attributes.forEach(function(attrib){
     if(attrib.type && attrib.type.format && attrib.type.format.title == 'Date'
         && !attrib.type.format.metadata.datetime){
        attrib.type.format.metadata.datetime = "123456789012345678901249";
        db.schema.updateOne({ "_id": app._id },{ "$set": {attributes: app.attributes}});
        print('Updated Successfully');
     }
    });
  });
