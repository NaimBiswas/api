===========================================================
db.getCollection('calendar').find({ "master":{"$exists":true} },{"master":1}).forEach(function(a){

  db.getCollection('masterqcsettings').find({ "_id":ObjectId(a.master._id.valueOf())}).forEach(function(b){
    b.schema.forEach(function(c){

      db.getCollection(c._id).find({"entry":{"$exists":true}},{"entry":1}).forEach(function(d){
        if(d && d.entry){
          var calIds=[];;
          d.entry.calendarIds.forEach(function(cid){
            calIds.push(cid.valueOf());
          })
          db.getCollection(c._id).update({"_id":d._id},{"$set":{"calendarIds":calIds, "calendarverifieduser":d.entry.calendarverifieduser},"$unset":{"entry":""}});
        }
      });
    });
  });
});
==========================================================
db.getCollection('calendar').find({ "master":{"$exists":true} },{"master":1}).forEach(function(a){
  //print(a._id+"a:"+a.master._id);
  db.getCollection('masterqcsettings').find({ "_id":ObjectId(a.master._id.valueOf())}).forEach(function(b){
    b.schema.forEach(function(c){
      var d=db.getCollection(c._id).count({"entry":{"$exists":true}},{"entry":1});
      if(d && d>0){
        print(c._id+"title::"+c.title+"="+d);
      }
    });
  });
});

=========================================================
db.getCollection('calendar').find({ismasterQcForm:true,entry:{$exists:true}},{"entry":true}).count();
=================================================
var count = 1;
db.getCollection('calendar').find({ismasterQcForm:true,entry:{$exists:true}},{"entry":true}).forEach(function(obj){
  var array = [];
  obj.entry.masterschemaIds.forEach(function(item){
    db[item].find({'calendarIds.0': obj._id.valueOf()},{modifiedDate:1}).forEach(function(entry){
      array.push({schemaId:item,modifiedDate:entry.modifiedDate});
    });
  });
  var schemaCompletedDate = array.reduce(function(obj,item){ obj[item.schemaId] = item.modifiedDate; return obj},{});
  print(count++);
  db.getCollection('calendar').update({_id:obj._id},{$set:{'entry.schemaCompletedDate':schemaCompletedDate}});
});
===================================================
db.calendar.find({ismasterQcForm: true,entry: {$exists:true}}).count()

var count = 1;
db.calendar.find({ismasterQcForm: true,entry: {$exists:true}}).toArray().forEach(function(item,index){
  if(item.entry.calendarverifieduser){
    for(var schemaId in item.entry.calendarverifieduser){
      var entry = db[schemaId+''].findOne({'calendarIds': {$in:[item._id+'']}});
      if(entry && entry.modifiedBy){
        var user = db.users.findOne({_id: ObjectId(entry.modifiedBy)}, {firstname: 1, lastname: 1, username: 1});
        if(user && user.firstname && user.lastname && user.username){
          var modifiedByName = user.firstname + ' ' + user.lastname + ' (' + user.username + ')';
          print(modifiedByName);
          var obj = {};
          obj[item._id.valueOf()] = modifiedByName;
          item.entry.calendarverifieduser[schemaId] = [obj];
        }
      }

    }
    print(count++);
    db.calendar.update({_id: item._id},{$set: {'entry.calendarverifieduser': item.entry.calendarverifieduser}})
  }
});
