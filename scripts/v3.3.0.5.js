// ===========================================
// By mahammad: createdBy and modifiedBy should be ObjectId in 3.3 or later version


db.getCollection('searchFavorites').find({}, {
    createdBy: 1,
    modifiedBy: 1
}).toArray().forEach(function (item) {
    db.getCollection('searchFavorites').update({
        _id: item._id
    }, {
        $set: {
            createdBy: ObjectId(item.createdBy.valueOf()),
            modifiedBy: ObjectId(item.modifiedBy.valueOf())
        }
    });

    print(item._id.valueOf());
});

// ===========================================
// by shivani - PQT-1961

var isMailLogs = db.getCollection('resources').findOne({
    title: "Mail Logs"
});
if (!isMailLogs) {
    db.getCollection('resources').insert({
        "title": "Mail Logs",
        "entity": "mailLogs",
        "isActive": true
    });
}

// ===========================================
