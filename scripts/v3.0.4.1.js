db.getCollection('schedules').find({}).forEach(function (doc){
    var flag = false;
    if(doc.attribute.length > 0){
        doc.attribute.forEach(function (att){
             if(att.parentId && att['s#'].indexOf('-') == -1 ){
                 flag = true;
                 att.queSeqId = att['s#'];
                att['s#'] = att['s#'] +'-'+att.parentId;
                }
           })
    }
     if(flag){
          db.getCollection('schedules').update({_id:doc._id}, {$set:doc});
      }
});


db.getCollection('schema').find({}).forEach(function (doc){
  var schemaId = doc._id.valueOf();
  var entries = db[schemaId].find({}).forEach(function (entry){
    if(entry.workflowreview && entry.workflowreview.length > 0){
      var reviewtableentry = db.getCollection('reviewpendingworkflow').find({"entryId" : entry._id});
      if(reviewtableentry.toArray().length == 0){
        entry.workflowreview.forEach(function (workflow){
          if(workflow.reviewstatus == 1 && (entry.entries.status.status == 0 || entry.entries.status.status == 1)){
            var reviewerPendingEntry = {
              entryId : '',
              schemaId : '',
              parentSchemaVersion : '',
              levelIds : [],
              verificationDate : '',
              reviewworkflow : {}
            };
            reviewerPendingEntry.entryId = entry._id;
            reviewerPendingEntry.schemaId = schemaId;
            reviewerPendingEntry.parentSchemaVersion = entry.parentVersion;
            reviewerPendingEntry.levelIds = entry.levelIds;
            reviewerPendingEntry.verificationDate = entry.verificationDate;
            reviewerPendingEntry.createdDate = new Date(entry.createdDate).toISOString();
            if(entry.masterQCSettings){
              reviewerPendingEntry.masterQCSettings = entry.masterQCSettings;
            }
            reviewerPendingEntry.reviewworkflow =workflow;
            printjson(doc.title);
            db["reviewpendingworkflow"].insert(reviewerPendingEntry,function (err,a){
               
            });
          }
        });
      }
    }
  }); 
});
