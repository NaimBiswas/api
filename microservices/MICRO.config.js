module.exports = {
    deploy : {
        /**
         * version and client should not have 
         * special character it should have
         * plain text witn no space
         * it is used to make unique microservice connections
         */
        version : "v33022",
        client : "avera_validation"
    },
    longTimeout : 60 * 60 * 1000,
    shortTimeout : 3 * 1000,
    amqpStatusPublishInterval : 15 * 1000,
    rabbitRestart : {
        count : 4,
        afterCheckInterval : 2 * 1000
    }
}
