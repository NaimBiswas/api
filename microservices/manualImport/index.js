
var path = require('path');
process.env.FILE_STORE = path.join(__dirname, (process.env.FILE_STORE || '../../tmp/'));

var MICRO = require('../MICRO');
var serviceListings = require('../serviceListings');
var manualImport = require('./manualImport');
var serviceName = serviceListings.manualImport.serviceName;

var senecaQue = MICRO.getRecieverInstance(serviceName,manualImport).que();

var senecaTcp = MICRO.getRecieverInstance(serviceName,manualImport).tcp();

senecaTcp.startStatusListner(senecaQue);

