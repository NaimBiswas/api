
var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('../../lib/logger').configure(__dirname, enableBunyan);
var db = require('../../lib/resources').db;

var validation = require('../../routes/manualimport/validation');
var entityValidation = require('../../routes/manualimport/entityvalidation');


module.exports = function schedule() {
    
    this.senecaFunction.senecaMethod("import",function sum(msg, respond){

        var req = msg.msgBody
        if (req.query.isimportview == 'true') {
            if (req.body.type == 'entity') {
                entityValidation.validate(req, { respond : respond }, undefined);
            }
            else {
                validation.validate(req, { respond : respond }, undefined);
            }
        }
        else{
            req.data.status = 200;
            respond(req.data)
        //   (req.status ? res.status(req.status) : res).json(req.data);
        }

    })

}