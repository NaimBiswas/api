var Util = require('util')
var _ = require('lodash')
var Jsonic = require('jsonic')

// function makedie(instance, ctxt) {
//   ctxt = _.extend(ctxt, instance.die ? instance.die.context : {})

//   var diecount = 0

//   var die = function(err) {
//     var so = instance.options()

//     // undead is only for testing, do not use in production
//     var undead = (so.debug && so.debug.undead) || (err && err.undead)

//     if (0 < diecount) {
//       if (!undead) {
//         throw new Error('[DEATH LOOP]')
//       }
//       return
//     } else {
//       diecount++
//     }

//     var die_trace =
//       '\n' + new Error('die trace').stack.match(/^.*?\n.*\n(.*)/)[1]

//     try {
//       if (!err) {
//         err = new Error('unknown')
//       } else if (!Util.isError(err)) {
//         err = new Error(_.isString(err) ? err : Util.inspect(err))
//       }

//       err.fatal$ = true

//       var logdesc = {
//         kind: ctxt.txt,
//         plugin: ctxt.plugin,
//         tag: ctxt.tag,
//         id: ctxt.id,
//         code: err.code,
//         notice: err.message,
//         err: err,
//         callpoint: ctxt.callpoint && ctxt.callpoint()
//       }

//       instance.log.fatal.apply(instance, logdesc)

//       var stack = err.stack || ''
//       stack = stack.replace(/^.*?\n/, '\n')

//       var procdesc =
//         '\n  pid=' +
//         process.pid +
//         ', arch=' +
//         process.arch +
//         ', platform=' +
//         process.platform +
//         ',\n  path=' +
//         process.execPath +
//         ',\n  argv=' +
//         Util.inspect(process.argv).replace(/\n/g, '') +
//         ',\n  env=' +
//         Util.inspect(process.env).replace(/\n/g, '')

//       var fatalmodemsg = instance.fixedargs.fatal$
//         ? '\n  ALL ERRORS FATAL: action called with argument fatal$:true ' +
//             '(probably a plugin init error, or using a plugin seneca instance)'
//         : ''

//       var stderrmsg =
//         '\n\n' +
//         'Seneca Fatal Error\n' +
//         '==================\n\n' +
//         'Message: ' +
//         err.message +
//         '\n\n' +
//         'Code: ' +
//         err.code +
//         '\n\n' +
//         'Details: ' +
//         Util.inspect(err.details, { depth: null }) +
//         '\n\n' +
//         'Stack: ' +
//         stack +
//         '\n\n' +
//         'Instance: ' +
//         instance.toString() +
//         fatalmodemsg +
//         die_trace +
//         '\n\n' +
//         'When: ' +
//         new Date().toISOString() +
//         '\n\n' +
//         'Log: ' +
//         Jsonic.stringify(logdesc) +
//         '\n\n' +
//         'Node:\n  ' +
//         Util.inspect(process.versions).replace(/\s+/g, ' ') +
//         ',\n  ' +
//         Util.inspect(process.features).replace(/\s+/g, ' ') +
//         ',\n  ' +
//         Util.inspect(process.moduleLoadList).replace(/\s+/g, ' ') +
//         '\n\n' +
//         'Process: ' +
//         procdesc +
//         '\n\n'

//       if (so.errhandler) {
//         so.errhandler.call(instance, err)
//       }

//       if (instance.closed) {
//         return
//       }

//       if (!undead) {
//         instance.act('role:seneca,info:fatal,closing$:true', { err: err })


//         // Miral changes
//         // instance.close(
//         //   // terminate process, err (if defined) is from seneca.close
//         //   function(err) {
//         //     if (!undead) {
//         //       process.nextTick(function() {
//         //         if (err) {
//         //           Print.err(err)
//         //         }

//         //         Print.err(stderrmsg)
//         //         Print.err(
//         //           '\n\nSENECA TERMINATED at ' +
//         //             new Date().toISOString() +
//         //             '. See above for error report.\n\n'
//         //         )

//         //         so.system.exit(1)
//         //       })
//         //     }
//         //   }
//         // )
//       }

//       // Miral changes
//       // make sure we close down within options.deathdelay seconds
//       // if (!undead) {
//       //   var killtimer = setTimeout(function() {
//       //     Print.err(stderrmsg)
//       //     Print.err(
//       //       '\n\nSENECA TERMINATED (on timeout) at ' +
//       //         new Date().toISOString() +
//       //         '.\n\n'
//       //     )

//       //     so.system.exit(2)
//       //   }, so.deathdelay)

//       //   if (killtimer.unref) {
//       //     killtimer.unref()
//       //   }
//       // }
//     } catch (panic) {
//       var msg =
//         '\n\n' +
//         'Seneca Panic\n' +
//         '============\n\n' +
//         panic.stack +
//         '\n\nOriginal Error:\n' +
//         (arguments[0] && arguments[0].stack ? arguments[0].stack : arguments[0])
//       // Print.err(msg)
//     }
//   }

//   die.context = ctxt

//   return die
// }



function configure(seneca){
  if(seneca && seneca.private$ && seneca.private$.optioner && seneca.private$.optioner.set){
    seneca.private$.optioner.set({debug:{undead : true}})
  }
}


module.exports = {
  // makedie : makedie,
  configure : configure
}