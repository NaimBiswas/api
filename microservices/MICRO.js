
var senecaQue;
// var seneca = require('seneca');
var seneca = require('seneca');
var Amqp = require('amqplib/callback_api');
var microConfig = require('./MICRO.config');
var serviceListings = require('./serviceListings');
var senecaCustoms = require('./lib/seneca-customs');

function getUniqueServiceName(serviceName){
    // return serviceName;
    return microConfig.deploy.version+"_"+microConfig.deploy.client+"_"+serviceName;
}


function getServiceObject(serviceName){
    return serviceListings[serviceName];
}

function getRecieverInstance(serviceName,service){
    
    var serviceConfigObject = getServiceObject(serviceName);
    var port = serviceConfigObject.port
    var serviceName_un = getUniqueServiceName(serviceConfigObject.serviceName)
    
    var senecaInstance = seneca({timeout : microConfig.longTimeout});

    senecaInstance.senecaFunction = senecaFunction(serviceName_un,senecaInstance);
    senecaInstance.startStatusListner = startStatusListner;
    
    senecaCustoms.configure(senecaInstance);

    function que(){
        senecaInstance.transportType = "amqp";
        senecaInstance = senecaInstance.use('seneca-amqp-transport',{
                "amqp": {
                    "listener" : {
                        queues: {
                            options : {
                                arguments : null
                            }
                        }
                    }
                }
            })  
            .use(service)
            .listen({ type: 'amqp',
                        pin: 'role:'+serviceName_un});

        senecaInstance.amqpStatus = "open";
        return senecaInstance;
    }
    function tcp(){
        senecaInstance.transportType = "tcp";
        return senecaInstance
            .use(service)
            .listen({ type: 'http',port : port, pin: 'role:'+serviceName_un})
    }

    function startStatusListner(senecaQue){

        senecaInstance.senecaFunction.senecaMethod("amqpStatusListner",function amqpStatusReciever(msg, respond){

            console.log("Service name : ",serviceName);
            if(msg.status == "close"){
                senecaQue.amqpStatus = "close";
            }

            if(msg.status == "close" && senecaQue.amqpStatus == "open" ){
                // senecaQue.amqpStatus = "close";
                senecaQue.close();
            }
            else if(msg.status == "open" && senecaQue.amqpStatus == "close"){
                senecaQue = getRecieverInstance(serviceName,service).que();
            }
            else if(msg.status == "firstTimeOpen"){
                // senecaQue.amqpStatus = "close";
                senecaQue.close();
                senecaQue = getRecieverInstance(serviceName,service).que();
            }
            respond({"status":"true"})
        })
    }


    return {
        que : que,
        tcp : tcp
    }

}



function getPublisherInstance(serviceName){

    var serviceConfigObject = getServiceObject(serviceName);
    var serviceName_un = getUniqueServiceName(serviceConfigObject.serviceName)
    var port = serviceConfigObject.port;

    var onlyTcp = true;

    function getSenecaQue(){
        return seneca({timeout : microConfig.longTimeout})
            .use('seneca-amqp-transport',
                {
                    "amqp": {
                        "client": {
                            "queues": {
                                "options": {
                                    "autoDelete": false,
                                    "exclusive": false,
                                    "arguments": null
                                }
                            }
                        },
                        "publish" : {
                            persistent: true
                        }
                    }
                })
            .client({   type: 'amqp',
                        pin: 'role:'+serviceName_un,
                        publish: {
                            persistent: true
                        }});
    }

    var senecaQue = getSenecaQue();
    senecaCustoms.configure(senecaQue);

    var senecaTcp  = seneca({timeout : microConfig.longTimeout})
        .client({ type: 'http',port : port,pin: 'role:'+serviceName_un})

    senecaCustoms.configure(senecaTcp);

    function call(methodName,msgBody,cb){
        queConnectionCheck(function(status){
            var actBody = {
                role : serviceName_un,
                cmd : methodName,
                msgBody : msgBody
            };


            if(onlyTcp){
                senecaTcp.act(actBody,cb);
                return;
            }

            if(status == "open"){
                senecaQue.act(actBody,function(err,res){
                    if(err && err.details && err.details.message == "Channel closed"){
                        senecaTcp.act(actBody,cb);
                        senecaQue = getSenecaQue();
                    }
                    else{
                        cb(err,res);
                    }
                })
            }
            else{
                senecaTcp.act(actBody,cb)
            }
        })
    }

    function chooseOnlyTcp(){
        onlyTcp = true;
    }
    return {
        call : call,
        chooseOnlyTcp :chooseOnlyTcp
    }

}

function queConnectionCheck(cb){
    try{
        Amqp.connect('amqp://localhost', function(err, conn) {
            if(err){
                cb("close");
            }
            if(conn){
                cb("open");
                conn.close()
            }
        });
    }
    catch(e){
        cb("close");
    }
    
}



function senecaFunction(serviceName,sencaContext){
    function senecaMethod(methodName,cb){
        sencaContext.add({
            role : serviceName,
            cmd : methodName
        },cb)
    }
    return {
        senecaMethod : senecaMethod
    };
}

module.exports = {
    getRecieverInstance : getRecieverInstance,
    senecaFunction : senecaFunction,
    getPublisherInstance : getPublisherInstance,
    queConnectionCheck : queConnectionCheck,
    getUniqueServiceName: getUniqueServiceName
}