var exec = require('child_process').exec;
var MICRO = require("../MICRO");
var _ = require("lodash");
var serviceListings = require("../serviceListings");
var microConfig = require('../MICRO.config');

var seneca  = require('seneca')()

// seneca.client({ type: 'http',
//   // port : val.port, 
//   pin: 'role:'+serviceName}) 

_.forEach(serviceListings,function(val){
  var serviceName_un = MICRO.getUniqueServiceName(val.serviceName);
  seneca.client({ type: 'http',
                  port : val.port, 
                  pin: 'role:'+serviceName_un})  
})


var isFirstTime = true;
var afterRestartPublishCount = 0;
var rabbitMQNotWorking = false;

setInterval(function(){
  publishStatus();
}, microConfig.amqpStatusPublishInterval);



var serviceName;
function publishStatus(){
  MICRO.queConnectionCheck(function(status){
      
      if(status == "close"){
        exec('sudo service rabbitmq-server start');
        if(afterRestartPublishCount < microConfig.rabbitRestart.count){
          afterRestartPublishCount++;
          setTimeout(function() {
            publishStatus()
          }, microConfig.rabbitRestart.afterCheckInterval);
        }
        else{
          rabbitMQNotWorking = true;
          //Rabbitmq not working MAIL code 
        }
      }
      else if(status == "open"){
        rabbitMQNotWorking = false;
        afterRestartPublishCount = 0;
      }
      
      if(isFirstTime && status == "open"){
        status = "firstTimeOpen";
        isFirstTime = false;
      }
      if(!rabbitMQNotWorking){
        _.forEach(serviceListings,function(serviceModule) {
            var serviceName_un = MICRO.getUniqueServiceName(serviceModule.serviceName);
            console.log(serviceName_un);
            seneca.act({role:serviceName_un,cmd:"amqpStatusListner",status : status},function(err,response){
              console.log("res-"+serviceName_un)
            });
        });
      }
      
  });

}

