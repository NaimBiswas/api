

var _ = require("lodash");

var outputObj = {
  connectionMade : {
    schedule : 0
  }
};

function test(){

  var MICRO = require("../MICRO");
  var serviceListings = require("../serviceListings");
  
  var seneca  = require('../../lib/seneca/seneca')()
      .client({ type: 'tcp'})
  
  
  
  setInterval(function(){
    var sequanceString = "";

    MICRO.queConnectionCheck(function(status){
      
      _.forEach(serviceListings,function(serviceModule) {
        seneca.act({role:serviceModule.serviceName,cmd:"amqpStatusListner"},function(err,response){
          outputObj.connectionMade[serviceModule.serviceName]++;
        });
      });
    });
  },2000);
  
  
}


test()
setTimeout(function() {
  var properConnection = true;
  _.forEach(outputObj.connectionMade,function(serviceModule){
    if(!(serviceModule>3)){
      properConnection = false;
    }
  })
  
  if(properConnection){
    console.log("\x1b[32m","Test Pass : ",properConnection);
    console.log("\x1b[32m","Total connection tries : ",outputObj.connectionMade.schedule);
  }
  else{
    console.log("\x1b[31m","Test Fail : ",properConnection);
    console.log("\x1b[31m","Total connection tries : ",outputObj.connectionMade.schedule);
  }
}, 10000);



