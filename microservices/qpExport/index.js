
var MICRO = require('../MICRO');
var serviceListings = require('../serviceListings');
var qpExport = require('./qpExport');
var serviceName = serviceListings.qpExport.serviceName;

var senecaQue = MICRO.getRecieverInstance(serviceName,qpExport).que();

var senecaTcp = MICRO.getRecieverInstance(serviceName,qpExport).tcp();

senecaTcp.startStatusListner(senecaQue);

