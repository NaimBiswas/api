
var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('../../lib/logger').configure(__dirname, enableBunyan);
var db = require('../../lib/resources').db;
var exportManipulator = require('../../routes/qulificationperformance/exportWindows.js');

module.exports = function schedule() {
    
    this.senecaFunction.senecaMethod("export",function sum(msg, respond){

        exportManipulator.exportWindowManipulation(msg.msgBody).then(function(resp){
              db.collection('windowQueueFiles').update({ _id: db.ObjectID(resp.savequeueId) }, { $set: { isGenerate: true, status: 'Done' } }, function (err, result) {
              });
        })

        respond({"status":"true",transportType : this.transportType})
    })

}