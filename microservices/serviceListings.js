module.exports = {
    schedule : {
        serviceName : "schedule",
        port : 8007
    },
    qpExport : {
        serviceName : "qpExport",
        port : 8008
    },
    manualImport : {
        serviceName : "manualImport",
        port : 8009
    }
}