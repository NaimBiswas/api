# PQT-15 
# http://pm.productivet.com:8080/browse/PQT-15
### Story - App Builder > Reviewer Workflow - Feature to configure the signature sequence as required all users to sign or any one from selected

## **Implementation**
______________

> To indentify if any one user is required to sign
or all users are required we will now put a flag on 
each level of workflow say **```anyOne```** in entry record
## **Model : ```anyOne```**
```
true / false
```
```
ex..
IN ENTRY RECORD || IN SCHEMA
{
    workflowReview :[
        {
            anyOne : true,
            reviewStatus : 0,
            ...
            ...
        }
    ]
}
```
* By default **```anyOne```** will be true as we set in UI
* Same format is used in **```Entry record```** and in **```Schema```** 
 
> Before the implementation of multiple user signature on same level 
we did not have to maintain an array of signed users we were just
maintaining reviewer by and date on worflowReview's each level object

> Now we are maintaining the array where we put who has signed in 
perticular level of workflow say **```reviews```**

## **Model : ```reviews```**
```
{
    reviews : [
        {
            userId: "string (user ObjectId)",
            fullName: "string",
            comment: "string", 
            date: "string (ISO format)", 
            status: "0 / 2 / 3 (0 = signed, 2 = pending, 3 = rejected(PQT-1))"
        }
    ]
}
```
```
ex..
IN ENTRY RECORD
{
    workflowReview :[
        {
            anyOne : false,
            userSelectionType : 0,
            setworkflowuser : ['username','username'],
            users : [...]
            reviews : [
                {
                    userId: "125255125254",
                    fullName: "firstName Middlename (username)",
                    comment: "Hello how are", 
                    date: "2018-05-24T06:26:39.583Z", 
                    status: 0
                },
                {
                    userId: "125255125254",
                    fullName: "firstName Middlename (username)",
                    comment: "Hello how are", 
                    date: "2018-05-24T06:26:39.583Z", 
                    status: 3
                },
                ...
                ...
            ],
            
        },
        ...
        ...
    ]
}
```
* ***```userSelectionType```*** 0 means from all users of 
***```roles```***
* ***```userSelectionType```*** 1 means from selected users of ***```users```***

* If ***```userSelectionType```*** is 1 and ***```anyOne```*** is true selected that means all users of perticular role has to sign otherwise that level
will not considered as signed

* To maintain diffrence between signed and unsigned users we have ***```reviews```*** array and ***```setworkflowuser```*** OR ***```users```***
by making diffrence between them we can check if perticular level is signed
or not 

* Perticular level can have one of three review status say **```reviewstatus```**
0 for Signed, 
1 for Ready for review, 
2 for Pending

* If all users in reviews array from users/setworkflowuser has signed the app on 
perticular level then we may put **```reviewstatus```** as 0

## Scripts for previous data
____________
> To make reviews array that was not maintained for single user and put anyOne,
It was done for all entry records of all schema

> To put anyOne in workflow levels of all schema


--------------
--------------
--------------

# PQT-1
# http://pm.productivet.com:8080/browse/PQT-1
### Story: Feature to have an option to reject an app record from reviewer workflow. (LifeServe)

## **Implementation**
______________

> Rejecting the record is also kind of reviewing the record
so we are maintaining it in same **```reviews```** array of 
workflow level we put **```status : 3```** to show rejected

```
ex...
reviews : [
    {
        userId: "125255125254",
        fullName: "firstName Middlename (username)",
        comment: "Hello how are", 
        date: "2018-05-24T06:26:39.583Z", 
        status: 3
    },
    ...
    ...
],
```

* Once we reject the record we put it in reviews array with status : 3 
but once someone **```save and accept```** the record at that time for 
now to show that user who has rejected is in pending mode, we make another 
entry in reviews array witn **```status : 2```**

```
ex...
reviews : [
    {
        userId: "125255125254",
        fullName: "firstName Middlename (username)",
        comment: "Hello how are", 
        date: "2018-05-24T06:26:39.583Z", 
        status: 3
    },
    {
        userId: "125255125254",
        fullName: "firstName Middlename (username)",
        comment: "Hello how are", 
        date: "2018-05-24T06:26:39.583Z", 
        status: 2
    },
    ...
    ...
],
```

* We also maintain record level status of reject as **```isRejected```**
and **```rejectedObj```**

```
IN ENTRY RECORD
ex...
{
    isRejected : true,
    rejectedObj : {
        "userId": assignByInfo[0]._id,
        "fullName" : assignByInfo[0].firstname + ' ' + assignByInfo[0].lastname + ' (' + assignByInfo[0].username + ')',
        "date": rejectAssigneeObj.assignedDate,
        "status" : 3,
        "comment": rejectAssigneeObj.comments,
        "level" : reviewIndex
    }
}
```

* we also put reject information in assignee as **```isRejected : true```*
__________
__________
__________


# PQT-4
# http://pm.productivet.com:8080/browse/PQT-4
### Story: Feature to assign an app record to multiple users (LifeServe)

## **Implementation**
______________

> In each entry record we had **```assignment```** object
it had **```assignee```** object to maintain single assignee information

> Now we are making that **```assignee```** from OBJECT to ARRAY 
and information will be same as before

> Same as above we are doing in assignment collection where we are maintaining
assignee history 

> Now we also maintain roles in assignment object now to maintain multiple roles
as multiple assignee of those roles.

```
IN ENTRY RECORD
ex...
{
    assignment : {
        roles: [...]
        assignee : [{
            username : 'username string',
            ...
            ...
        }]
    },
}
```
```
IN ASSIGNMENT COLLECTION
ex...
{
    assignment : [
        {
            roles: [...],
            assignee : [{
                username : 'username string',
                ...
                ...
            }],
            ...
            ...
        }
    ]
    
},
```

## Scripts for previous data
____________
> To make assignee from object to array AND put roles array in assignment object in all entries of all schema 

__________
__________
__________


# PQT-7
# http://pm.productivet.com:8080/browse/PQT-7
### Story: Feature to have a preference option to follow the compliance of the Reviewer Workflow (LifeServe)

## **Implementation**
______________

> To check if we revoke the workflowreview as previously or should
not revoke it we maintain a status **```isLockReviewWorkflow```**
in schema, it can be **```true / false```**

## Scripts for previous data
____________

> To make **```isLockReviewWorkflow```** to true default for old data in all
schema only.



__________
__________
__________


# PQT-3
# http://pm.productivet.com:8080/browse/PQT-3
### Story: Feature to provide notification settings(email) for Assignee function to end user(LifeServe)

## **Implementation**
______________

> User will configure true / false in **```assignmentEmailNotification```** in
user object

## Scripts for previous data
____________

> To make **```assignmentEmailNotification```** to false for all old users