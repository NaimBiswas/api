# PQT-78 
# http://pm.productivet.com:8080/browse/PQT-78
### Story - App/Master App Record > Reject Process - Assign the record to the previous user who save and accept the record

## **Implementation**
______________
We save our last save and accept user information in **```lastUpdated```** object of **```Entry record```**

## **Model : ```lastUpdated```**
```
{
    "userId" : "string - (ex:5ac1efa95f1899161412f0f1)",
    "fullName" : "string - (ex : miral miral (miral))",
    "comment" : "string - (ex : sdsd)",
    "date" : "string (ex : 2018-05-24T06:26:39.583Z)",
    "username" : "string (ex : muser3)",
    "firstname" : "string (ex : muser3)",
    "lastname" : "string (ex : muser3)"
}
```

## **Impact**
______________
* /src/app/secure/forms/entries/entries.controller.js
* /src/app/components/entryFormTabs/entryFormTabs.directive.js

If user click on **save and accept** from entry or master then it should 
compulsarily set **```lastUpdated```** object

* /src/app/components/rejectAssignee/rejectAssignee.controller.js

In **```popup of reject review```** it should show user from **```lastUpdated```**


----------------------
----------------------
----------------------
# PQT-81
# http://pm.productivet.com:8080/browse/PQT-81
### Story - App/Master App Record > Reject Process - Assign the record to the previous user who save and accept the record

## **Implementation**
______________
After sign, to show save and accept logs, say.. ```USER1 has updated this rocord on 2020-02-02:10:10:10 and updated fields are : (title,ent1Question, ent2question, procedure1 question)```
and
```USER2 has updated this rocord on 2020-02-03:10:10:10 and updated fields are : (title,ent1Question)```
then we are maintaining logs, say.. **```liteLogs```** in **```entry record```**

## **Model : ```liteLogs```**
```
IN ENTRY RECORD
{
    liteLogs : [
        {
            "updatedBy" : {
                "userId" : "string - 5ac1efa95f1899161412f0f1",
                "userObjectId" : "ObjectId ObjectId(5ac1efa95f1899161412f0f1)",
                "username" : "miral",
                "firstname" : "miral",
                "lastname" : "miral",
                "email" : "miral@yopmail.com",
                "fullName" : "miral miral (miral)"
            },
            "modifiedDate" : "Date 2018-05-31T14:57:16.774Z",
            "updatedFields" : [ 
                "Simple question 1",
                "Simple question 2",
            ]
        },
        ...
        ...
    ]
    ...
    ...
}
```


>NOTE : when compliance is followed, means, when **```isLockReviewWorkflow```** is true. We will ```not maintain liteLogs``` in entry record. But In specific case as below.. 
1. user sets **```isLockReviewWorkflow```** to false in schema initialy.
2. create entry 
3. do first sign
3. set **```isLockReviewWorkflow```** to true in schema 
4. do save and accept.

Currently, in this case array of **```workflowReview```** is added in **```workflowReviewHistory```** of entry record, But now, 
At the same time it should remove liteLogs from entry record and add it to seperate collection say.. **```entryLiteLogHistory```** in below format

> We also maintain mapping between perticular lite log and workflow review (for which lite logs were added.)
by **```workflowHistoryIndex```** in **```liteLogHistory```** object

## Model : **```entryLiteLogHistory```** (collection) 
```
entryLiteLogHistory 
{
    entryId : ObjectId,
    schemaId : ObjectId,
    liteLogHistory : [{
        workflowHistoryIndex : 5,
        liteLogs : []
    }]
}
```


## **Code explaination**
_______________

We are already maintaining audit logs in ```<schemaId>Logs``` collection
and code has been done in 
```
>api/lib/db/db.js
    >function logs()
```
At the end of this function we have array of logs say.. ```set``` of attribute changes 
which will be inserted in ```<schemaId>Logs``` collection 

> This set has all field changes which are not visible by end user like... ```workflowReview array, verifiedByUsers etc..```, which user has not changed, And, This array also do not have actual
field title in it, it has only ```sequenceId```

> We take this ```set``` array and filter out the changed ```user visible``` attribute titles
and insert in **```liteLogs```** of **```entry record```**, And, attach actual title from ```sequenceId```

> We have service and function to manage those above things say... 
```
> api/lib/utilities/entriesUtiliese.js
    > function logEntrySave_AfterSign()
```

### Checks :
> In this function ```logEntrySave_AfterSign```, we check if entry is done after sign or not,
If entry is done after sign then only do all process.

> That function say.. ```log``` is called for all save process of entire project so we check if it is from ```entry only``` or not.

> If compliance is followed then we will remove **```liteLogs```** and will put it in **```liteLogsHistory```**


----------------------
----------------------
----------------------
# PQT-21
# http://pm.productivet.com:8080/browse/PQT-21
### Story - Feature to lock the activity as Reject in the Activity Information link and Activity Information Reports

## **Implementation**
______________
We are already maintaining activity information for ```Create,View,Update```, 
So we will do same code for reject which will just insert ```Rejected``` status entry in ```qcentryactivity```
say... ```vm.activityinfo.status = 'Created';```

Code change will be in 
```
/home/miral/protprojects/perfeqta-folder/web/src/app/components/rejectAssignee/rejectAssignee.controller.js
```


----------------------
----------------------
----------------------
# PQT-79
# http://pm.productivet.com:8080/browse/PQT-79
### Story - App/Master App Record - Allow app record assignment in draft submissions and Restrict app record assignments after save & accept record

## **Implementation**
______________
We just have to restrict user from **```editing```** the **```record's Assignee```** after **```save and accept```**

## So we just ```Check``` : 
> If it is new record then obviously allow to edit the assignee. say.. check if Entry Record > entries is available or not, because it will be available only if record is inserted in DB either by ```Draft or Save and accept```

> If record is once inserted in DB but saved as draft then Entry ``` Record > entries.status.status``` will be ```4``` so we check if entry record is status id 4 then and then allow to change assignee

> Assignee is changed from rejectAssignee popup also. but we will not restrict there to change assignee

> NOTE : user is able to change assignee when reject after save. we do not restrict there
__________
_________
________
PQT20