'use strict'
//Make usre session_logger is always required before everything else.
// You can set environment variable "enableBunyan" while launching like "enableBunyan=true node ./bin/www";
var colors = require('colors');
var enableBunyan = process.env.enableBunyan === 'true';

var session_logger = require('./lib/logger').configure(__dirname, enableBunyan);
global.hasSessionLogger = true;

//Chandni : Code start for Swagger
//var swaggerJSDoc = require('swagger-jsdoc');
//var swagger = require("swagger-node-express");
//Chandni : Code end for Swagger


var settings = require('./lib/settings');
var mailer = require('./lib/mailer');
var express = require('express');
var zlib = require('zlib');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var useragent = require('express-useragent');
var cors = require('cors');
var routes = require('./routes');
var busboy = require('connect-busboy');
var fs = require('fs');
var _ = require('lodash');
var log = require('./logger');
var PublishHelper = require('./loggerPublisher/publishHelper.js');
var log4js = require('log4js');
var config = require('./config/config.js');
var schedulePublisher = require('./loggerPublisher/schedulePublisher');
var importSchemaPublisher = require('./loggerPublisher/importSchemaPublisher');

// var logger4js = log4js.getLogger()
log4js.configure({
    appenders: [
        { type: 'file', filename: './logger/capturedLogs.log', category: 'captureLog', backups: 50, maxLogSize: 20480 }
    ]
});

//Chandni : Code start for Swagger

// Initialize swagger-jsdoc -> returns validated swagger spec in json format
//var swaggerSpec = swaggerJSDoc(options);
var app = express();
//  var subpath = express();
// swagger.setApiInfo({
//         title: "PERFEQTA API",
//         description: "API to do something, manage something...",
//         termsOfServiceUrl: "",
//         contact: "chandni.p@productivet.com",
//         license: "",
//         licenseUrl: ""
//     });
//Chandni : Code end for Swagger

  var logger4js = log4js.getLogger('captureLog');

  process.env.FILE_STORE = path.join(__dirname, (process.env.FILE_STORE || './tmp/'));

  if (!fs.existsSync(process.env.FILE_STORE)) {
    fs.mkdirSync(process.env.FILE_STORE);
  }
  app.use(useragent.express());

  //Chandni : Code start for Swagger
  // app.set('views', path.join(__dirname, 'views'));

//   app.get('/api-docs.json', function(req, res) {
//     res.setHeader('Content-Type', 'application/json');
//     res.send(swaggerSpec);
//   });
  // app.use(bodyParser());
  //app.use("/doc", subpath);
  //swagger.setAppHandler(subpath);
  // var swagger = require('swagger-node-express').createNew(subpath);
//   app.use(express.static('dist'));
//   subpath.get('/', function (req, res) {
//     var loggedin = true;
//     if (loggedin) {
//         res.sendfile(__dirname + '/dist/index1.html');
//     }else {
//       res.sendfile(__dirname + '/dist/login.html');
//     }

//      });

  //swagger.configureSwaggerPaths('', 'api-docs', '');




  // var domain = 'localhost';
  // if(argv.domain !== undefined)
  //     domain = argv.domain;
  // else
  //     console.log('No --domain=xxx specified, taking default hostname "localhost".');
  // var applicationUrl = 'http://' + domain;
  // swagger.configure(applicationUrl, '1.0.0');
  //Chandni : Code end for Swagger

  //Allow swagger to call the APIs, from documentation
  //var whitelist = process.env.VIRTUAL_HOST ? [process.env.VIRTUAL_HOST.replace('api.', 'web.')] : undefined;
  //var corsOptions = {
  //    origin: function corsOrginCallback(origin, callback) {
  //        //In development mode
  //        if (!whitelist) {
  //            callback(null, true);
  //            return;
  //        }

  //        //In production mode (docker)
  //        var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
  //        callback(null, true || originIsWhitelisted);
  //    }
  //};
  //app.use(cors(corsOptions));


  // Start by jaydip for add entries route schema
  var dbConfig = require('./lib/db/db');
  var db = require('./lib/resources').db;
  var entries = require('./routes/entries');

  var createDynamicRouteForEntries = function (req, res, next) {
    //log.log('createDynamicRouteForEntries');
    //log.log(req.path);
    var currentId;
    var entities = [];
    if (!_.isUndefined(req.path) && req.path.toLowerCase().startsWith('/entries/')) {
      var entries = require('./routes/entries');
      //log.log('111 Middleware first line for create route.');
      currentId = req.path.split('/')[2];
      //log.log('currentId :: ' + currentId);
      if (!_.isUndefined(currentId)) {
        entities.push({
          collection: currentId,
          audit: true,
          index: {
            title: ''
          }
        });
        //log.log('333 : Registering dynamic entries routes :: ' + entities.length);
        dbConfig.configure.apply(this, entities);
        //log.log("444 : Registering dynamic entries routes " + currentId);
        entries.use('/' + currentId, require('./routes/entries/entries')(currentId));
        //log.log('111 Process Request NEXT()');
        next();
      } else {
        //log.log('222 Process Request NEXT()');
        next();
      }
    } else if (!_.isUndefined(req.path) && req.path.toLowerCase().startsWith('/entityrecords/') ) {
      var entityRecords = require('./routes/entityRecords');
      //log.log('1111 Middleware first line for create route for entity records.');
      currentId = req.path.split('/')[2];
      //log.log('currentId :: ' + currentId);
      if (!_.isUndefined(currentId)) {
        entities.push({
          collection: currentId,
          audit: true,
          index: {
            title: ''
          }
        });
        //log.log('3333 : Registering dynamic entityRecords routes :: ' + entities.length);
        dbConfig.configure.apply(this, entities);
        //log.log("4444 : Registering dynamic entityRecords routes " + currentId);
        entityRecords.use('/' + currentId, require('./routes/entityRecords/entityRecords')(currentId));
        //log.log('1111 Process Request(Entity Records) NEXT()');
        next();
      }
    } else {
      //log.log('333 without entries Process Request NEXT()');
      next();
    }
  };

  app.use(createDynamicRouteForEntries);


  schedulePublisher.connect();
  importSchemaPublisher.connect();

  // End by jaydip for entries route for schema

  /**
  * @name Reponse Status
  * @author Surjeet Bhadauriya <surjeet.b@productivet.com>
  * @desc Log responses excepting 200, 201 and 304
  */
  app.use(function (req, res, next) {
    res.on('finish', function () {
      if (res.statusCode !== 200 && res.statusCode !== 201 && res.statusCode !== 304) {
        var obj = {
          "status": "Response Status",
          "source": req.path,
          "statusCode": res.statusCode,
          "statusMessage": res.statusMessage,
          'user': {
            'id': 'global',
            'username': 'global'
          },
          "timestamp": new Date(),
          "env": settings.default.product,
          "level": "info"
        };

        if (req.user) {
          obj.user.id = req.user._id;
          obj.user.username = req.user.username;
        }

        PublishHelper.publish('perfeqta.logging', { content: obj }); //Rabbitmq Log
        logger4js.info(obj); //Disk Log
      }
    });
    next();
  });

  /*
  * responce modifier by vivek
  * over write send method of responce
  * responce compress in gzip format before send
  */
/*  var compression = require('compression');
 app.use(compression()); */

  app.use(function (req, res, next) {
    if(req.url != '/'){
      if(req.method != 'OPTIONS' && req.url.indexOf('/doc/') == -1){
        var send = res.send;
        res.send = function (string) {
          var self = this;
          var body = string instanceof Buffer ? string.toString() :((typeof string == 'object') ? JSON.stringify(string) : string);
          zlib.gzip(body, function (_, result) {
            res.set({
              'Content-Encoding': 'gzip'
            });
            send.call(self, result);
          });
        };
      }
    }
    next();
  });


  //by akashdeep.s@productivet.com
  app.use(function (req, res, next) {

    //var allowedOrigins = ['http://localhost:9000', 'https://dev.beperfeqta.com', 'https://qa.beperfeqta.com','https://po.beperfeqta.com', 'https://demo.beperfeqta.com', 'https://prot.beperfeqta.com'];
    //var origin = req.headers.origin;
    //console.log("allowedOrigins", allowedOrigins);
    //console.log("origin", origin);
    //if (allowedOrigins.indexOf(origin) > -1) {
    //    res.setHeader('Access-Control-Allow-Origin', origin);
    //}
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, DELETE, PATCH");
    //akashdeep.s - QC3-4428 - added role-schema-update to bypass at patch
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization,X-Authorization , If-Modified-Since, Cache-Control, Pragma, client-offset, x-content-type-options,x-frame-options,role-schema-update,If-Match,client-tz,allow-put");
    next();
  });
  // view engine setup
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'hbs');

  //This is required to capture IP when hosted under reverse proxy
  app.enable('trust proxy');


  // uncomment after placing your favicon in /public
  //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

  if (enableBunyan) {
    app.use(require('express-bunyan-logger')({
      logger: session_logger({ component: 'express' })
    }));
  }

  //Promisify all required modules
  require('./lib/promise-me');

  app.use(logger('dev'));
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  app.use(busboy());

  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, 'public')));
  app.use(express.static(path.join(__dirname, 'pluginImages')));

  //Lets routes module configure app so that all active routes
  //get mounted
  routes.configure(app);
  //setup oauth2 - currently only password grant type is supported.
  require('./lib/oauth').configure(app).registerErrorHandler();


  // catch 404 and forward to error handler
  app.use(function notFoundMiddleware(req, res, next) {
    var err = new Error('Route Not Found :: ' + req.path);
    err.status = 404;
    next(err);
  });

  //by surjeet.b@productivet.com for handling central errors..
  process.on('uncaughtException', function (err) {
    log.log('Caught exception: ' + err,'error');
    console.log(err);
    logError(err);
  });

  // development error handler
  // will print stacktrace
  if (app.get('env') === 'development') {
    app.use(function showDetailedErrorMiddleware(err, req, res, next) {
      if (err.message === 'Signature verification failed') {
        res.status(401);
        res.json({
          message: "Unauthorized access"
        });
        return;
      }

      res.status(err.status || 500);
      res.json({
        message: err.message,
        error: err
      });
    });
  }
  // production error handler
  // no stacktraces leaked to user
  app.use(function showProductionErrorMiddleware(err, req, res, next) {
    if (err.message === 'Signature verification failed') {
      res.status(401);
      res.json({
        message: "Unauthorized access"
      });
      return;
    }

    if (err && err.message !== 'Signature verification failed') {
      logError(err);
    }

    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {}
    });
  });

  /**
  * @name logError
  * @author Surjeet Bhadauriya <surjeet.b@productivet.com>
  * @desc Log error. 1. MQ Log 2. Disk Log 3. Mail Log
  */
  function logError(err) {
    try {
      setTimeout(function () {
        var obj = {
          "statusCode":err.status || 500,
          "status": 'API Error',
          "error": err.message,
          "errordescription": err.stack,
          "timestamp": new Date(),
          "env": settings.default.product,
          "level": "error"
        };
        PublishHelper.publish('perfeqta.logging', { content: obj }); //Rabbitmq logger
        logger4js.error(obj); //Disk logger

        err.stack = err.stack || 'No description available.';
        var model = {};
        _.merge(model, {'error': err, 'errorSatck': err.stack, settings});

        log.log('inside file: app.js - function: sendMail - status: sending mail','info');
        log.log('Get Static URL from config : ' + config.staticURL,'info'); //Prachi

        mailer.send('system-error', model , config.staticURL , function sendMailCallback(e, b) {
          if (e) {
            log.log(e, 'error');
          } else {
            log.log('API Error email has sent successfully', 'info');
          }
          return;
        });
      }, 1000);
    } catch (e) {
      log.log(e,'info');
    }
  }

  module.exports = app;
