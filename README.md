To get started do the following

1. Clone the repo `git clone git@ptgit.cloudapp.net:/qc3/api`
2. do `cd web && bower install && npm install` (if bower is not installed, do `npm install bower -g`)
3. do `./serve` (if gulp not installed, do `npm install gulp -g`)
3. do `./scheduler` to fireup the scheduler

>> Make sure that `mongod` is running.
> You would be able to change the database name from `./serve` file.

# Common ToDos

## To add a new entity
1. In `lib/db/index.js` add following entry
`{
    collection: '<ENTITY>',   //<- This will become the collection name
    audit: true, //<- This will enable auditing and logging, it will create <ENTITY>AuditLogs and <ENTITY>Logs
    index: {  //<- This is basically a mongodb index object, just pass in whatever you'd pass into createIndex
      title: 'text', //<- Example of full-text index
      formatId: 1 //<- Example of asc. sorted index on formatId
    }
  }`
