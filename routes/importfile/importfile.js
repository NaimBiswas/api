'use strict'

var express = require('express');
var router = express.Router();
var fs = require('fs-extra');
var db = require('../../lib/resources').db;
var shell = require('shelljs');
var _ = require('lodash');
var csv = require('csv-array');
var mailer = require('../../lib/mailer');
var settings = require('../../lib/settings');
var debug = require('../../lib/logger').debug('routes:upload');
var restler = require('restler');
var failDataCol = [];
var passDataCol = [];
var mapping = {};
var parentVersion = '';
var setOriginalValue = [];
var username = '';
var userId = '';
var mappingId = '';
var keyValueAttribute;
var levelLength;
var setsarray = [];
var allsites = [];
var attrPass1 = [];
var attrPass2 = [];
var oldSchemas = [];
var userdata = {};
var processSchema = {};
var tempSites = [];
var logger = require('../../logger');
var config = require('../../config/config.js');
var request = require("request");

var currentFileName = __filename;
/**
 * @swagger
 * definition:
 *   File:
 *     properties:
 *       file:
 *         type: string
 */

/**
 * @swagger
 * /importfile:
 *   post:
 *     tags: [Import Data]
 *     description: Upload a file
 *     produces:
 *       - application/json
 *     consumes:
 *       - multipart/form-data
 *     parameters:
 *       - name: file
 *         description: The uploaded file data
 *         in: formData
 *         required: true
 *         type: file
 *         paramType: body
 *         dataType: file
 *     responses:
 *       200:
 *         description: An object of file
 *         schema:
 *           $ref: '#/definitions/File'
 */
//Post file
// router.post('/', function(req, res, next) {
//   req.pipe(req.busboy);
//   req.busboy.on('file', function (fieldname, file, filename) {
//     debug("Uploading: " + filename);
//     var filename = (new Date()).getTime() + '-' + filename;

//     //Path where file will be uploaded
//     var fstream = fs.createWriteStream(process.env.FILE_STORE + filename);

//     file.pipe(fstream);

//     fstream.on('close', function () {
//       debug("Upload Finished of " + filename);
//       var newFileName = process.env.FILE_STORE + filename;
//       if (!fs.existsSync(newFileName)) {
//         res.status(404).json({ 'message' : 'file not found' })
//         return;
//       }

//       var items;
//       fs.readFile(newFileName, 'utf8' , function (err, data) {

//         if (err) {
//           // log.log(err);

//         }
//         else {
//           items = [];
//           var length = data.split('\n')[0].split(',').length;

//           var RowData = data.replace(/\r/gi,'').split('\n');
//           for (var i = 0; i < RowData.length - 1 ; i++) {
//             items[i] = RowData[i].split(',');
//           }
//           if(req.query.isAutomated){
//             failDataCol = [];
//             passDataCol = [];
//             mapping = {};
//             parentVersion = '';
//             username = '';
//             userId = '';
//             mappingId = '';
//             levelLength;
//             allsites = [];
//             attrPass1 = [];
//             attrPass2 = [];
//             oldSchemas = [];

//             processOldData(items, req, res);
//           }

//           //res.status(200).json(items);
//         }
//       })
//       res.status(200).json({file: filename});
//     });
//   });

// });


router.post('/', function (req, res, next) {
    
    if (config && config.fileServer &&
    config.fileServer.enabled &&
    config.fileServer.url &&
    config.fileServer.url.post) {

        req.body.strictSSL =false;
        req.pipe(request.post(config.fileServer.url.post, req.body)).pipe(res);

        // handlePost(req, res,next)
        
        // .on('end', function(req,res) {
        //     console.log("******************")
        //     console.log(req)
        //     console.log(res)
        // })

    }
    else {
        handlePost(req, res,next)
    }
});



function handlePost(req, res,next){

    req.pipe(req.busboy);
    req.busboy.on('file', function (fieldname, file, filename) {
      debug("Uploading: " + filename);
        filename = (new Date()).getTime() + '-' + filename;
  
      //Path where file will be uploaded
      var fstream = fs.createWriteStream(process.env.FILE_STORE + filename);
  
      file.pipe(fstream);
  
      fstream.on('close', function () {
        debug("Upload Finished of " + filename);
        var newFileName = process.env.FILE_STORE + filename;
        if (!fs.existsSync(newFileName)) {
            
          res.status(404).json({ 'message' : 'file not found' })
          return;
        }
  
        var items;
        fs.readFile(newFileName, 'utf8' , function (err, data) {
  
          if (err) {
            // log.log(err);
  
          }
          else {
            items = [];
            //var length = data.split('\n')[0].split(',').length;
  
            var RowData = data.replace(/\r/gi,'').split('\n');
            for (var i = 0; i < RowData.length - 1 ; i++) {
              items[i] = RowData[i].split(',');
            }
            if(req.query.isAutomated){
              failDataCol = [];
              passDataCol = [];
              mapping = {};
              parentVersion = '';
              username = '';
              userId = '';
              mappingId = '';
              levelLength;
              allsites = [];
              attrPass1 = [];
              attrPass2 = [];
              oldSchemas = [];
  
              processOldData(items, req, res);
            }
  
            //res.status(200).json(items);
          }

            // res.status(200).json(data);
        })
        res.status(200).json({file: filename});
      });
    });

}

// function afterPost(req,data){

//     // if (err) {
//     // // log.log(err);

//     // }
//     // else {
//     items = [];
//     var length = data.split('\n')[0].split(',').length;

//     var RowData = data.replace(/\r/gi,'').split('\n');
//     for (var i = 0; i < RowData.length - 1 ; i++) {
//         items[i] = RowData[i].split(',');
//     }
//     if(req.query.isAutomated){
//         failDataCol = [];
//         passDataCol = [];
//         mapping = {};
//         parentVersion = '';
//         username = '';
//         userId = '';
//         mappingId = '';
//         levelLength;
//         allsites = [];
//         attrPass1 = [];
//         attrPass2 = [];
//         oldSchemas = [];

//         processOldData(items, req, res);
//     }

//     //res.status(200).json(items);
//     // }
// }


/**
 * @swagger
 * /importfile/{filename}:
 *   get:
 *     tags: [Import Data]
 *     description: Returns file
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: filename
 *         description: File Name
 *         in: path
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: File Data
 */

// router.get('/:file', function(req, res, next) {
//   if(!req.params.file){
//     res.status(422).json({'message' : 'file not provided'})
//     return;
//   }

//   var filename = process.env.FILE_STORE + req.params.file;
//   // var filename =
//   if (!fs.existsSync(filename)){
//     res.status(404).json({'message' : 'file not found'})
//     return;
//   }

//   var items = [];
//   csv.parseCSV(filename, function(data){
//    res.status(200).json(data);
//   }, false);
//   /*fs.readFile(filename, 'utf8' , function (err, data) {

//     if(err){
//       log.log(err);
//     }
//     else{
//       items = [];


//       var length = data.split('\n')[0].split(',').length;

//       var RowData = data.replace(/\r/gi,'').split('\n');
//       log.log(RowData);
//       for (var i = 0; i < RowData.length - 1 ; i++) {
//         items[i] = RowData[i].split(',');
//       }

//       res.status(200).json(items);
//     }
//   });*/

//   // res.download(filename);
// });



router.get('/:file', function(req, res, next) {

    if (config && config.fileServer &&
        config.fileServer.enabled &&
        config.fileServer.url &&
        config.fileServer.url.get) {
        request(`${config.fileServer.url.get}${req.params.file}`,{strictSSL:false})
          .on('response',  function (resApi) {
    
            console.log("dfd");
    
            resApi.pipe(fs.createWriteStream(process.env.FILE_STORE + req.params.file));
            resApi.on( 'end', function(){
              // go on with processing
              
              handleAfterFileDownload(req,res)
    
            });
    
          });
      }
      else{
        handleAfterFileDownload(req,res)
      }

});


function handleAfterFileDownload(req,res){

  var filename = process.env.FILE_STORE + req.params.file;
  // var filename =
  if (!fs.existsSync(filename)){
    res.status(404).json({'message' : 'file not found'})
    return;
  }
  csv.parseCSV(filename, function(data){
   res.status(200).json(data);
  }, false);
}

function processOldData(items, req, res) {
  //   var rData = req.data;
  mappingId = req.query.mappingId;
  var allowHeader = req.query.containsHeaders;
  parentVersion = req.query.parentVersion;
  var filtersitelevels = [];
  userId =req.query.userId;
    username = req.query.username;
    db.sets.find({}).toArray(function (err, sets) {
        setsarray = sets;
        db.schema.find({ _id: db.ObjectID(req.query.schemaId) }).toArray(function (err, schema) {
        processSchema = schema[0];
        db.users.find({ _id: db.ObjectID(req.query.userId) }).toArray(function (err, usr) {
        userdata = usr[0];
        db.siteNames.find({}).toArray(function(err,siteNames){
        db.sites.find({}).toArray(function (err, sites) {
      allsites = sites;
      _.forEach(siteNames,function (sitename) {
        _.forEach(sites, function (site) {
          if(site.level._id === sitename._id)
          {
            if(filtersitelevels.indexOf(sitename) == -1){
              filtersitelevels.push(sitename)
            }
          }
        })
      })
      levelLength = filtersitelevels.length;
      db.manualimport.findOne({_id : db.ObjectID(mappingId)} , function (err, importdata) {
        mapping = importdata;
        var schemaId = mapping.schema._id;
        db[mapping.schema._id].find({}).toArray(function (err, oldscs) {
          oldSchemas = oldscs;
          _.forEach(items, function (data, index) {
            if (allowHeader == 'true') {
              if (index > 0) {
                allValidation(data, index);
              }
            }
            else {
              allValidation(data, index);
            }
          });
          //check for key attribute validation from existing passdatacol
          _.forEach(passDataCol, function (pdata, idz) {
            if(pdata)
            validateAttribute(pdata, idz);
          });
          //check for key attribute from database data
          _.forEach(attrPass1, function (pdata, idz) {
            if (pdata)
            validateAttributeDb(pdata, idz);
          });
          var processedData = attrPass2.concat(failDataCol);
        //   log.log("processedData :: "+processedData.length);
        //   log.log("attrPass2 :: " + attrPass2);
          db["automatedEntries"].insertManyAsync(failDataCol).then(function (data) {
            logger.log("data saved");
              res.status(req.status).json(req.data);
          }).catch(function (err) {
            logger.log(err);
          });
          if(req.query.automatedEntryId){
            var calendarId = req.query.automatedEntryId
            db["calendarAutomated"].removeAsync({_id:db.ObjectID(calendarId)}).then(function (data) {
                logger.log("data removed");
            }).catch(function (err) {
                logger.log(err);
            });
          }

          if(attrPass2.length > 0){

            var i = 0;
            var pdLength = attrPass2.length;
            var fn = function (){
                if (i < pdLength) {
                  var options = { headers : { authorization : req.headers.authorization } };
                  var psdt = attrPass2[i];
                  delete psdt.csvdata;
                  delete psdt.FailReasons;
                  delete psdt.status;
                  delete psdt._id;
                  delete psdt.mappingId;
                restler.postJson(config.apiEnvUrl + '/entries/' + schemaId, psdt, options)
                .on('complete', function (cmplt) {
                        i++;
                        fn();
                    })
                .on('timeout', function () {
                        i++;
                        fn();
                    })
                .on('error', function (err) {
                        i++;
                        fn();
                    });


                } else {
                    // log.log('Entry saved successfully...!!!');
                    return true; //res.json("Success");
                }
            }
            fn();
          }
        var entity = {
            user : username,
            email : req.query.email,
            formName : mapping.schema.title,
            date : new Date(),
            sourcePath : req.query.sourcePath,
            destinationPath : req.query.destinationPath,
            totalrecords : processedData.length,
            passrecords : attrPass2.length,
            failrecords : failDataCol.length
          }
          _.merge(entity, settings);
        //   log.log(entity);
        //   log.log('inside file: importfile.js - function: processOldData - status: sending mail');

          mailer.send('data-imported', entity, req.query.email,function sendMailCallback(e,b) {
            if(e){
              debug.err('Failed sending email - schedule-notify');
              debug.err(entity);
              debug.err(e);
            }else{
              debug(entity.email.underline + ' got registered sucessfully');
            }
          });
        })
      })
    })
  });
    });
    });
    });
};


//1:
function validateAttribute(val, inx) {
    var optchk = {
        'entries': {}
    };

    // log.log(passDataCol);
    _.forEach(processSchema.keyvalue, function (keyvalue) {
        if (keyvalue.hasOwnProperty('parentId')) {
            if (!_.isUndefined(val.entries[keyvalue['s#'] + '-' + keyvalue.parentId])) {
                var objws = {};
                objws[keyvalue.parentId] = val.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId];
                optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId] = objws;
            }
        } else {
            optchk.entries[keyvalue['s#']] = {
                'attributes': val.entries[keyvalue['s#']].attributes
            }
        }
    })
    if (Object.getOwnPropertyNames(optchk.entries).length == 0) {
        attrPass1.push(val);
    }
    else {
        var fltr = _.filter(passDataCol, optchk);
        if (fltr.length > 1) {
            val.status = 'Fail';
            _.forEach(processSchema.keyvalue, function (keyvalue) {
                val.FailReasons[keyvalue['s#']] = 'Either attribute or entity value already exist. Please provide other value.';
            })
            failDataCol.push(val);
      // passDataCol.splice(inx, 1);
        }
        else {
            attrPass1.push(val);
        }
    }
}

//2:
function validateAttributeDb(val, inx) {
    var optchk = {
        'entries': {}
    };
    _.forEach(processSchema.keyvalue, function (keyvalue) {
        if (keyvalue.hasOwnProperty('parentId')) {
            if (keyvalue.type.format.title === 'Date') {
                var objdas = {};
                objdas[keyvalue.parentId] = new Date(val.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId]).toISOString();
                optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId] = objdas;
            } else {
                if (!_.isUndefined(val.entries[keyvalue['s#'] + '-' + keyvalue.parentId])) {
                    var objedf = {};
                    objedf[keyvalue.parentId] = val.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId];
                    optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId] = objedf;
                }
            }
        } else {
            if (keyvalue.type.format.title === 'Date') {
                optchk.entries[keyvalue['s#']] = {
                    'attributes': new Date(val.entries[keyvalue['s#']].attributes).toISOString()
                }
            } else {
                optchk.entries[keyvalue['s#']] = {
                    'attributes': val.entries[keyvalue['s#']].attributes
                }
            }
        }
    });

    if (Object.getOwnPropertyNames(optchk.entries).length == 0) {
        attrPass2.push(val);
    } else {
        var fltr = _.filter(oldSchemas, optchk);
        if (fltr.length > 0) {
            val.status = 'Fail';
            _.forEach(processSchema.keyvalue, function (keyvalue) {
                val.FailReasons[keyvalue['s#']] = 'Either attribute or entity value already exist. Please provide other value.';
            })
            failDataCol.push(val);
        } else {
            attrPass2.push(val);
        }
    }
}
//3:
function allValidation(val, inx) {
    // log.log(sites);
    var entry = {
        reviewStatus: 'Draft',
        entries: {},
        levels: [],
        levelIds: [],
        csvdata: [],
        FailReasons: {}
    }
    entry.importId = mapping._id;
    entry.createdDateInfo = new Date();
    entry.createdByInfo = { _id: userdata._id, username: userdata.username, firstname: userdata.firstname, lastname: userdata.lastname };
    entry.levels = [];
    entry.levelIds = [];
    if (processSchema.hideSites) {
        _.forEach(processSchema.allicableSites, function (site) {
            var a = _.find(allsites , function (obj) {
                if (obj._id == site[0]._id) {
                    return obj;
                }
            })
            var sitobj = {
                siteId: a._id,
                title: a.title,
                levelId: a.level._id
            };
            entry.levels.push(sitobj);
            entry.levelIds.push(site[0]._id);
      // entry.csvdata.splice(entry.csvdata.length - 1, 1);
      // entry.csvdata.push({ 'value': sitobj.title, 'seq': mapping.schema.mapping[colidx].mappingField._id });
        });
    }

    var passValidate = [];
    // var colData = val.split(',');
    _.forEach(val, function (col, colidx) {
        col = col.trim();
        // entry.csvdata.push(col);

        var selectedData = {};
        _.forEach(mapping.fileindex , function (value) {
            if (value == colidx) {
                _.forEach(mapping.schema.mapping, function (data, idx) {
                    if (colidx == idx) {
                        if (data.mappingField.type) {
                            switch (data.mappingField.type.title) {
                                case "Textbox":
                                    passValidate.push(textBoxValidation(col, data.mappingField, entry));
                                    break;
                                case "Checkbox":
                                    passValidate.push(checkBoxValidation(col, data.mappingField, entry));
                                    break;
                                case "Dropdown":
                                    passValidate.push(dropDownValidation(col, data.mappingField, entry));
                                    break;
                                case "Auto Calculated":
                                    passValidate.push(textBoxValidation(col, data.mappingField, entry));
                                    break;
                                case "Textarea":
                                    passValidate.push(true);
                                    break;
                                default:
                                    passValidate.push(false);
                                    break;
                            }
                        }
                        selectedData = data;

                    }
                });

                entry.entries.status = {};
                entry.entries.status.status = 4;
                // log.log('-----------------------------------');
                // log.log(selectedData.mappingField);
                if (!_.isUndefined(selectedData.mappingField)) {
                    if (selectedData.mappingField.optionalProcedure == true) {
                        entry.entries.settings = {};
                        entry.entries.settings[selectedData.mappingField.procedureseqId] = true;
                    }

                    if (selectedData.mappingField.procedureseqId) {
                        entry.csvdata.push({ 'value': col, 'seq': selectedData.mappingField['s#'] });
                        if (!(entry.entries[selectedData.mappingField['s#']])) {
                            entry.entries[selectedData.mappingField['s#'] + '-' + selectedData.mappingField.procedureseqId] = {};
                        }
                        if (selectedData.mappingField.type.title != 'Dropdown') {
                            if (selectedData.mappingField.type.title == 'Textbox' && selectedData.mappingField.type.format.title == 'None') {
                                var setValue;

                                _.forEach(setOriginalValue, function (setData) {
                                    if (col.toLowerCase() == setData.toLowerCase()) {
                                        setValue = setData;
                                    }
                                });
                                entry.entries[selectedData.mappingField['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = _.isUndefined(setValue) ? col : setValue;
                            } else if (selectedData.mappingField.type.title == 'Textbox' && selectedData.mappingField.type.format.title == 'Date') {
                                entry.entries[selectedData.mappingField['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = new Date(col);
                            } else if (selectedData.mappingField.type.title == 'Textbox' && selectedData.mappingField.type.format.title == 'Time') {
                                if (getTimeFormat(selectedData.mappingField.type.format.metadata.datetime)) {
                                    if (col.indexOf(':') !== -1) {
                                        var value = col.split(':');
                                        if ((!isNumber(value[0]) || parseInt(value[0]) > 24) && (!isNumber(value[1]) || parseInt(value[1]) > 60)) {
                                            col = new Date();
                                        } else {
                                            var h = isNumber(value[0]) ? parseInt(value[0]) : 0;
                                            var m = isNumber(value[1]) ? parseInt(value[1]) : 0;
                                            // QC3-2806 - Jyotil - Audit Trail Info Time
                                            col = moment(new Date().setHours(h, m, 0, 0)).set('year', 1970).set('month', 0).set('date', 1);
                                            // QC3-2806 - Jyotil - Audit Trail Info Time
                                        }
                                    } else {
                                        col = new Date();
                                    }
                                } else {
                                    if (col.indexOf(':') !== -1) {
                                        var value = col.split(':');
                                        if ((!isNumber(value[0]) || parseInt(value[0]) > 12) && (!isNumber(value[1]) || parseInt(value[1]) > 60)) {
                                            col = new Date();
                                        } else {
                                            var h = isNumber(value[0]) ? parseInt(value[0]) : 0;
                                            var m = isNumber(value[1]) ? parseInt(value[1]) : 0;
                                            // QC3-2806 - Jyotil - Audit Trail Info Time
                                            col = moment(new Date().setHours(h, m, 0, 0)).set('year', 1970).set('month', 0).set('date', 1);
                                            // QC3-2806 - Jyotil - Audit Trail Info Time
                                        }
                                    } else {
                                        col = new Date();
                                    }
                                }
                                entry.entries[selectedData.mappingField['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = new Date(col);
                            } else if (selectedData.mappingField.type.title == 'Textbox') {
                                entry.entries[selectedData.mappingField['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = parseFloat(col);
                            } else {
                                entry.entries[selectedData.mappingField['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = col;
                            }
                        } else {
                            var setValue;

                            _.forEach(setOriginalValue, function (setData) {
                                if (col.toLowerCase() == setData.toLowerCase()) {
                                    setValue = setData;
                                }
                            });

                            if (col.indexOf('\r') != -1) {
                                entry.entries[selectedData.mappingField['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = setValue.replace('\r', '').trim();
                            } else {
                                entry.entries[selectedData.mappingField['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = setValue;
                            }

                        }

                        if (selectedData.mappingField.type.title == 'Checkbox') {
                            var chkdata = col.split('|');
                            var setValue = [];

                            _.forEach(setOriginalValue, function (setData) {
                                _.forEach(chkdata, function (csvData) {
                                    if (csvData.toLowerCase() == setData.toLowerCase()) {
                                        setValue.push(setData);
                                    }
                                })
                            })

                            entry.entries[selectedData.mappingField['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = setValue;
                        }

                        // Add by Jaydipsinh for adding entity's question in entry object
                        if (!_.isUndefined(selectedData.mappingField.isEntity) && selectedData.mappingField.isEntity) {
                            if (!_.isUndefined(processSchema.entities)) {
                                var entity = _.filter(processSchema.entities, { 's#': selectedData.mappingField.procedureseqId });

                                if (!_.isUndefined(entity) && entity.length > 0) {

                                    var questions = _.filter(entity[0].questions, { 'isKeyValue': false });
                                    if (selectedData.mappingField.type.title == 'Textbox' && !(selectedData.mappingField.type.format.title == 'None' || selectedData.mappingField.type.format.title == 'Date' || selectedData.mappingField.type.format.title == 'Time')) {
                                        col = parseFloat(col);
                                    }
                                    var values = [];
                                    if (selectedData.mappingField.type.title == 'Textbox' && selectedData.mappingField.type.format.title == 'Date') {
                                        values = _.filter(entity[0].values, function (value) {
                                            return moment.utc(new Date(value[selectedData.mappingField['s#']]).setHours(0, 0, 0, 0)).isSame(new Date(col).setHours(0, 0, 0, 0));
                                        });
                                    } else {
                                        var objsa = {};
                                        var ecmaprblm = selectedData.mappingField['s#'];
                                        objsa[ecmaprblm] = col;
                                        values = _.filter(entity[0].values, objsa);
                                    }

                                    if (values.length > 0 && values[0].isActive) {
                                        _.forEach(questions, function (q) {
                                            if (!(entry.entries[q['s#']])) {
                                                entry.entries[q['s#'] + '-' + selectedData.mappingField.procedureseqId] = {};
                                                // Add other question value from selected key entity question value
                                                if (q.type.title != 'Dropdown') {
                                                    if (q.type.title == 'Textbox' && q.type.format.title == 'None') {
                                                        entry.entries[q['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = values[0][q['s#']];
                                                    } else if (q.type.title == 'Textbox' && q.type.format.title == 'Date') {
                                                        entry.entries[q['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = new Date(values[0][q['s#']]);
                                                    } else if (q.type.title == 'Textbox' && q.type.format.title == 'Time') {
                                                        entry.entries[q['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = new Date(values[0][q['s#']]);
                                                    } else if (q.type.title == 'Textbox') {
                                                        entry.entries[q['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = parseFloat(values[0][q['s#']]);
                                                    } else {
                                                        entry.entries[q['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = values[0][q['s#']];
                                                    }
                                                } else {
                                                    if (values[0][q['s#']].indexOf('\r') != -1) {
                                                        entry.entries[q['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = values[0][q['s#']].replace('\r', '').trim();
                                                    } else {
                                                        entry.entries[q['s#'] + '-' + selectedData.mappingField.procedureseqId][selectedData.mappingField.procedureseqId] = values[0][q['s#']];
                                                    }
                                                }
                                            }
                                        });
                                    } else {
                                        // Remove Entity Value from object
                                        delete entry.entries[selectedData.mappingField['s#'] + '-' + selectedData.mappingField.procedureseqId];
                                        passValidate.push(false);
                                        entry.FailReasons[entity[0].questions['s#']] = "Either entity value is Inactive or value does not found. Please provide other value.";
                                    }
                                }
                            }
                        }
                    } else {
                        //if attribute and sites
                        if (selectedData.mappingField['s#']) {
                            entry.csvdata.push({ 'value': col, 'seq': selectedData.mappingField['s#'] });
                            if (_.find(processSchema.attributes, { 'title': selectedData.mappingField.title })) {
                                keyValueAttribute = _.find(processSchema.keyvalue, { 'title': selectedData.mappingField.title })
                            }
                            // parseInt(col) &&
                            if (selectedData.mappingField.type.title != 'Dropdown') {
                                var setValue;
                                if (!_.isUndefined(selectedData.mappingField.validation) && !_.isUndefined(selectedData.mappingField.validation.validationSet)) {
                                    if (keyValueAttribute && keyValueAttribute !== undefined && keyValueAttribute.isUniqueKeyValue == true) {
                                        _.forEach(setOriginalValue, function (setData) {
                                            if (col.toLowerCase() == setData.toLowerCase()) {
                                                setValue = setData;
                                            }
                                        });
                                        entry.entries[selectedData.mappingField['s#']] = {
                                            attributes: setValue,
//removed intentionally bcz it is causing problem
// iskey : true
                                        };
                                    } else {

                                        var setValue;

                                        _.forEach(setOriginalValue, function (setData) {
                                            if (col.toLowerCase() == setData.toLowerCase()) {
                                                setValue = setData;
                                            }
                                        });

                                        entry.entries[selectedData.mappingField['s#']] = {
                                            attributes: setValue,
// iskey : false
                                        };
                                    }
                                } else {
                                    entry.entries[selectedData.mappingField['s#']] = {
                                        attributes: (parseFloat(col) ? parseFloat(col) : col),
                                    };
                                }

                                if (selectedData.mappingField.type.format.title == 'Date') {
                                    entry.entries[selectedData.mappingField['s#']] = {
                                        attributes: new Date(col),
                                        // iskey : true
                                    };
                                }
                            } else {
                                    var setValue;
                                    // log.log(setOriginalValue);
                                    _.forEach(setOriginalValue, function (setData) {
                                        // log.log(setData);
                                        if (col.toLowerCase() == setData.toLowerCase()) {
                                            setValue = setData;
                                        }
                                    });
                                    // log.log(setValue);
                                    // entry.entries[selectedData.mappingField['s#']].attributes = setValue;
                                    entry.entries[selectedData.mappingField['s#']] = {
                                        attributes: setValue,
// iskey : true
                                    };
// log.log(entry.entries[selectedData.mappingField['s#']]);

                            }
                            if (selectedData.mappingField.type.title == 'Checkbox') {
                                var chkdata = col.split('|');
                                var setValue = [];

                                _.forEach(setOriginalValue, function (setData) {
                                    _.forEach(chkdata, function (csvData) {
                                        if (csvData.toLowerCase() == setData.toLowerCase()) {
                                            setValue.push(setData);
                                        }
                                    })
                                })
                                    entry.entries[selectedData.mappingField['s#']] = {
                                        attributes: setValue,
// iskey : true
                                    };

                            }
                        } else {
                            if (!processSchema.hideSites) {
                                //by surjeet.b@productivet.com... checking whether sites entered are really according to their levels
                                entry.csvdata.push({ 'value': col, 'seq': mapping.schema.mapping[colidx].mappingField._id });
                                tempSites.push(mapping.schema.mapping[colidx].mappingField._id);
                                if (processSchema.allicableSites.length > 0) {
                                    _.find(processSchema.allicableSites[mapping.schema.mapping[colidx].mappingField._id], function (site) {
                                        if ((site.title).toLowerCase() === col.toLowerCase().trim()) {
                                            var sitobj = {
                                                siteId: site._id,
                                                title: site.title,
                                                levelId: mapping.schema.mapping[colidx].mappingField._id
                                            };
                                            entry.levels.push(sitobj);
                                            entry.levelIds.push(site._id);
                                            entry.csvdata.splice(entry.csvdata.length - 1, 1);
                                            entry.csvdata.push({ 'value': sitobj.title, 'seq': mapping.schema.mapping[colidx].mappingField._id });
                                        }
                                    });
                                } else {
                                    _.find(userdata.applicableSites[mapping.schema.mapping[colidx].mappingField._id], function (site) {
                                        if ((site.title).toLowerCase() === col.toLowerCase().trim()) {
                                            var sitobj = {
                                                siteId: site._id,
                                                title: site.title,
                                                levelId: mapping.schema.mapping[colidx].mappingField._id
                                            };
                                            entry.levels.push(sitobj);
                                            entry.levelIds.push(site._id);
                                            entry.csvdata.splice(entry.csvdata.length - 1, 1);
                                            entry.csvdata.push({ 'value': sitobj.title, 'seq': mapping.schema.mapping[colidx].mappingField._id });
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
            }
        })
    });
    if (entry.levels.length != levelLength) {
        _.forEach(tempSites, function (site) {
            entry.FailReasons[site] = 'Sites not filled according to their level.';
        })
        passValidate.push(false);
    }
    //log.log('--------------------------');
    //log.log(passValidate);
    entry.parentVersion = parentVersion;
    entry.mappingId = mappingId;

    if (passValidate.indexOf(false) == -1) {
        entry.status = "Pass";
        passDataCol.push(entry);
    } else {
        entry.status = "Fail";
        failDataCol.push(entry);
    }
    return;

};

//4:
function textBoxValidation(scol, mapField, entry) {
    switch (mapField.type.format.title) {
        case "None":
            return validateField(scol, mapField, entry, false);
            break;
        case "Number":
            return validateField(scol, mapField, entry, true);
            break;
        case "Exponential":
            return validateField(scol, mapField, entry, true);
            break;
        case "Percentage":
            return validateField(scol, mapField, entry, true);
            break;
        case "Currency":
            return validateField(scol, mapField, entry, true);
            break;
        case "Scientific":
            return validateField(scol, mapField, entry, true);
            break;
        case "Date":
            return validateDate(scol, mapField, entry);
            break;
        case "Time":
            return validateTime(scol, mapField, entry);
            break;
        default:
            return true;
            break;
    }

};

function checkBoxValidation(scol, mapField, entry) {
    var flag = 0;
    var data = scol.split('|');
    if (data.length > 0) {
        _.forEach(data, function (chval) {
            _.forEach(mapField.validation.validationSet.existingSet.values, function (setval) {
                if (chval.toLowerCase() == setval.title.toLowerCase()) {
                    if (setOriginalValue.indexOf(setval.title) == -1) {
                        setOriginalValue.push(setval.title);
                    }
                    flag++;
                }
            });
        });
    }
    if (flag !== data.length) {
        entry.FailReasons[mapField['s#']] = "Value does not match with checkbox set values. Please provide other value.";
    }
    return (flag == data.length);
};

function dropDownValidation(scol, mapField, entry) {
    var flag = false;
    var setvalues = [];
    if (_.isUndefined(mapField.validation.validationSet.existingSet.values)) {
        var setobj;
        setobj = _.find(setsarray, function (setval) {
            return _.isObject(mapField.validation.validationSet.existingSet) ? mapField.validation.validationSet.existingSet.title : mapField.validation.validationSet.existingSet == setval.title;
        });
        setvalues = setobj.values;
    } else {
        setvalues = mapField.validation.validationSet.existingSet.values;
    }
    _.forEach(setvalues, function (setval) {
        if (scol.toString().trim().toLowerCase() == setval.title.toString().trim().toLowerCase()) {
            if (setOriginalValue.indexOf(setval.title) == -1) {
                setOriginalValue.push(setval.title);
            }
            flag = true;
        }
    });
    if (!flag) {
        entry.FailReasons[mapField['s#']] = "Value does not match with dropdown set values. Please provide other value.";
    }
    return flag;
};

// Code for validate condition provided in questions.
function validateField(col, question, entry, isNumber) {
    if (isNumber) {
        if (!(/^\d+$/.test(col) || (/^\s*[-+]?(\d*\.?\d+|\d+\.)(e[-+]?[0-9]+)?\s*$/i.test(col)))) {
            entry.FailReasons[question['s#']] = "Value does not match. Please provide valid number.";
            return false;
        }
    }
    // Case for Validting regularExpression, validationSet and conditions on question
    if (!_.isUndefined(question.validation)) {
        if (!_.isUndefined(question.validation.regularExpression) && question.validation.regularExpression) {
            if (!(new RegExp(question.validation.regularExpression.regEx)).test(col)) {
                entry.FailReasons[question['s#']] = _.isUndefined(question.validation.regularExpression.validationMessage) ? 'Value does not match. Please provide valid value for regular expression.' : question.validation.regularExpression.validationMessage;
                return false;
            }
        }

        if (!_.isUndefined(question.validation.validationSet) && question.validation.validationSet) {
            var setTitle = _.isObject(question.validation.validationSet.existingSet) ? question.validation.validationSet.existingSet.title : question.validation.validationSet.existingSet

            var setobj = _.find(setsarray, function (setval) {
                return setTitle == setval.title;
            });

            var data = _.filter(setobj.values, function (value) {
                return value.isActive === true;
            });


            var returnData = _.filter(data, function (object) {
                if (isValidNumber(object.value)) {
                    if ((!isNaN(parseFloat(object.value)) ? parseFloat(object.value) : object.value.toLowerCase()) === (!isNaN(parseFloat(col)) ? parseFloat(col) : col.toLowerCase())) {
                        if (setOriginalValue.indexOf(object.value) == -1) {
                            setOriginalValue.push(object.value);
                        }
                    }
                    return true;
                } else {
                    return object.value === col
                }
            }).length > 0;

            if (returnData) {
                return true;
            } else {
                entry.FailReasons[question['s#']] = _.isUndefined(question.validation.validationSet.validationMessage) ? 'Value does not match. Please provide valid value from sets' : question.validation.validationSet.validationMessage;
                return false;
            }
        }

        if (!_.isUndefined(question.validation.condition) && !_.isUndefined(question.validation.condition.conditionTitle) && question.validation.condition) {
            if (!validateCondition(question, col)) {
                entry.FailReasons[question['s#']] = _.isUndefined(question.validation.condition.validationMessage) ? 'Value does not match. Please provide valid value.' : question.validation.condition.validationMessage;
                return false;
            }
        }
    } else {
        return true;
    }
};

/* function splitTime(dispalyTime) {
    var timestring = dispalyTime.replace(' ', ':');
    var timearray = timestring.split(':');
    var hour = '';
    var min = '';
    if (timearray[3] == 'am') {
        if (parseInt(timearray[0]) === 12) {
            timearray[0] = 0;
        }
        hour = parseInt(timearray[0]);
        min = parseInt(timearray[1]);
    }
    if (timearray[3] == 'pm') {
        if (parseInt(timearray[0]) === 12) {
            timearray[0] = 0;
        }
        hour = parseInt(timearray[0]) + 12;
        min = parseInt(timearray[1]);
    }
} */

function getTimeFormat(formatId) {

    var allowItems = _.reduce(allTypes[0].formats, function (o, obj) {

        if (obj.title === 'Time') {
            o.push(obj.metadata[0].allowItems)
        }
        return o;
    }, [])[0];
    var timeFormt = _.reduce(allowItems, function (o, item) {
        if (item._id === formatId) {
            o.push(item);
        }
        return o;
    }, [])[0];

    if (timeFormt) {
        if (timeFormt['title'] === 'HH:MM' || timeFormt['title'] === 'HH:MM:SS') {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

// validate time
function validateTime(time, mapField, entry) {
    // Check whether date is number or not if date is number then consider invalid date

    if (time.indexOf(':') !== -1 && time.split(':').length <= 2) {
        var value = time.split(':');
        if (!isNumber(value[0]) || parseInt(value[0]) > 24) {
            entry.FailReasons[mapField['s#']] = "Please provide valid time.";
            return false;
        }
        if (!isNumber(value[1]) || parseInt(value[1]) > 60) {
            entry.FailReasons[mapField['s#']] = "Please provide valid time.";
            return false;
        }
        // need to write code for time validation

        if (!_.isUndefined(mapField.validation) && !_.isUndefined(mapField.validation.condition)) {

            //value new date && scope time from question

            return performedTimeValidation(mapField.validation.condition, time, entry, mapField['s#']);
        }



// ...
    } else {
        entry.FailReasons[mapField['s#']] = "Please provide valid time.";
        return false;
    }
    return true;
};

//validate date
function validateDate(date, mapField, entry) {
    // Check whether date is number or not if date is number then consider invalid date
    var timestamp = Date.parse(isNumber(date) ? '' : date);
    if (_.isNaN(timestamp)) {
        entry.FailReasons[mapField['s#']] = "Please provide valid Date.";
        return false;
    } else {
        if (!_.isUndefined(mapField.validation) && !_.isUndefined(mapField.validation.condition)) {
            return performedDateValidation(mapField.validation.condition, new Date(date), entry, mapField['s#']);
        } else {
            return true;
        }
    }
};

function isNumber(value) {
    return (/^\d+$/.test(value) || (/^\s*[-+]?(\d*\.?\d+|\d+\.)(e[-+]?[0-9]+)?\s*$/i.test(value)))
}

function performedTimeValidation(scope, value, entry, sequenceId) {

    // var momentValue = moment(moment.utc(value).toDate()).format('h:mm:ss a');
    // splitTime(momentValue);
    // value = new Date().setHours(h, m, 0, 0);
    var timeValue1;
    if (value.indexOf(':') !== -1) {
        var timeValue = value.split(':');
        if ((!isNumber(timeValue[0]) || parseInt(timeValue[0]) > 24) && (!isNumber(timeValue[1]) || parseInt(timeValue[1]) > 60)) {
            value = new Date();
        } else {
            var h = isNumber(timeValue[0]) ? parseInt(timeValue[0]) : 0;
            var m = isNumber(timeValue[1]) ? parseInt(timeValue[1]) : 0;
            // QC3-2806 - Jyotil - Audit Trail Info Time
            value = moment(new Date().setHours(h, m, 0, 0)).set('year', 1970).set('month', 0).set('date', 1);
            // QC3-2806 - Jyotil - Audit Trail Info Time
        }
    } else {
        value = new Date();
    }

    if (!_.isUndefined(scope.value)) {
        if (scope.value.indexOf(':') !== -1) {
            timeValue1 = scope.value.split(':');
            if ((!isNumber(timeValue1[0]) || parseInt(timeValue1[0]) > 24) && (!isNumber(timeValue1[1]) || parseInt(timeValue1[1]) > 60)) {
                scope.value = new Date();
            } else {
                // QC3-2806 - Jyotil - Audit Trail Info Time
                scope.value = moment(new Date().setHours((isNumber(timeValue1[0]) ? parseInt(timeValue1[0]) : 0, m, 0, 0), (isNumber(timeValue1[1]) ? parseInt(timeValue1[1]) : 0))).set('year', 1970).set('month', 0).set('date', 1);
                // QC3-2806 - Jyotil - Audit Trail Info Time
            }
        } else {
            scope.value = new Date();
        }
    }

    if (!_.isUndefined(scope.minimum)) {
        if (scope.minimum.indexOf(':') !== -1) {
            timeValue1 = scope.minimum.split(':');
            if ((!isNumber(timeValue1[0]) || parseInt(timeValue1[0]) > 24) && (!isNumber(timeValue1[1]) || parseInt(timeValue1[1]) > 60)) {
                scope.value = new Date();
            } else {
                // QC3-2806 - Jyotil - Audit Trail Info Time
                scope.minimum = moment(new Date().setHours((isNumber(timeValue1[0]) ? parseInt(timeValue1[0]) : 0, m, 0, 0), (isNumber(timeValue1[1]) ? parseInt(timeValue1[1]) : 0))).set('year', 1970).set('month', 0).set('date', 1);
                // QC3-2806 - Jyotil - Audit Trail Info Time
            }
        } else {
            scope.minimum = new Date();
        }
    }

    if (!_.isUndefined(scope.maximum)) {
        if (scope.maximum.indexOf(':') !== -1) {
            timeValue1 = scope.maximum.split(':');
            if ((!isNumber(timeValue1[0]) || parseInt(timeValue1[0]) > 24) && (!isNumber(timeValue1[1]) || parseInt(timeValue1[1]) > 60)) {
                scope.maximum = new Date();
            } else {
                // QC3-2806 - Jyotil - Audit Trail Info Time
                scope.maximum = moment(new Date().setHours((isNumber(timeValue1[0]) ? parseInt(timeValue1[0]) : 0, m, 0, 0), (isNumber(timeValue1[1]) ? parseInt(timeValue1[1]) : 0))).set('year', 1970).set('month', 0).set('date', 1);
                // QC3-2806 - Jyotil - Audit Trail Info Time
            }
        } else {
            scope.maximum = new Date();
        }
    }

    // var momentScopeValue = moment(moment.utc(scope.value).toDate()).format('h:mm:ss a');
    // splitTime(momentScopeValue);
    // scope.value = new Date().setHours(hour, min, 0, 0);

    // var momentScopeMinimum = moment(moment.utc(scope.minimum).toDate()).format('h:mm:ss a');
    // splitTime(momentScopeMinimum);
    // scope.minimum = new Date().setHours(hour, min, 0, 0);

    // var momentScopMeximum = moment(moment.utc(scope.maximum).toDate()).format('h:mm:ss a');
    // splitTime(momentScopMeximum);
    // scope.maximum = new Date().setHours(hour, min, 0, 0);

    switch (scope.conditionTitle) {
        case '>':
            if (value > scope.value) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }

        case '<':
            if (value < scope.value) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        case '>=':
            if (value >= scope.value) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        case '<=':
            if (value <= scope.value) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        case '=':
            if (value == scope.value) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        case '!=':
            if (value != scope.value) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        case '>= && <=':
            if (value >= scope.minimum && value <= scope.maximum) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        case '!(>= && <=)':
            if (!(value >= scope.minimum && value <= scope.maximum)) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        default:
            return false;
    }
};

function performedDateValidation(scope, value, entry, sequenceId) {
    if (scope.today === '1') {
        scope.value = new Date().setHours(0, 0, 0);
    }
    switch (scope.conditionTitle) {
        case '>':
            if (new Date(value).setHours(0, 0, 0, 0) > new Date(scope.value).setHours(0, 0, 0, 0)) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        case '<':
            if (new Date(value).setHours(0, 0, 0, 0) < new Date(scope.value).setHours(0, 0, 0, 0)) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        case '>=':
            if (new Date(value).setHours(0, 0, 0, 0) >= new Date(scope.value).setHours(0, 0, 0, 0)) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        case '<=':
            if (new Date(value).setHours(0, 0, 0, 0) <= new Date(scope.value).setHours(0, 0, 0, 0)) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        case '=':
            if (moment.utc(new Date(value).setHours(0, 0, 0, 0)).isSame(new Date(scope.value).setHours(0, 0, 0, 0))) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        case '!=':
            if (!moment.utc(new Date(value).setHours(0, 0, 0, 0)).isSame(new Date(scope.value).setHours(0, 0, 0, 0))) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        case '>= && <=':
            if ((new Date(value).setHours(0, 0, 0, 0) >= new Date(scope.minimumDate).setHours(0, 0, 0, 0) && new Date(value).setHours(0, 0, 0, 0) <= new Date(scope.maximumDate).setHours(0, 0, 0, 0))) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
                return false;
            }
        case '!(>= && <=)':
            if (!(new Date(value).setHours(0, 0, 0, 0) >= new Date(scope.minimumDate).setHours(0, 0, 0, 0) && new Date(value).setHours(0, 0, 0, 0) <= new Date(scope.maximumDate).setHours(0, 0, 0, 0))) {
                return true;
            } else {
                entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date.' : scope.validationMessage;
                return false;
            }
        default:
            entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date.' : scope.validationMessage;
            return false;
    }
};


function validateCondition(scope, value) {
    if (scope.type.format.title === 'Number' || scope.type.format.title === 'Percentage' || scope.type.format.title === 'Currency' || scope.type.format.title === 'Scientific') {
        var decimals = parseInt(scope.type.format.metadata.Decimal);
        decimals = decimals > 25 ? 25 : decimals;
        if (_.isNaN(decimals) || decimals == '') {
            value = Math.floor(parseFloat(value));
        } else {
            value = _.isUndefined(value) ? '' : parseFloat(parseFloat(value).toFixed(decimals));
        }
    }

    value = !isNaN(parseFloat(value)) ? parseFloat(value) : value.length;
    switch (scope.validation.condition.conditionTitle) {
        case '>':
            return value > parseFloat(scope.validation.condition.value);
        case '<':
            return value < parseFloat(scope.validation.condition.value);
        case '>=':
            return value >= parseFloat(scope.validation.condition.value);
        case '<=':
            return value <= parseFloat(scope.validation.condition.value);
        case '=':
            return value == parseFloat(scope.validation.condition.value);
        case '!=':
            return value != parseFloat(scope.validation.condition.value);
        case '>= && <=':
            return (value >= parseFloat(scope.validation.condition.minimum) && value <= parseFloat(scope.validation.condition.maximum));
        case '!(>= && <=)':
            return !(value >= parseFloat(scope.validation.condition.minimum) && value <= parseFloat(scope.validation.condition.maximum));
        default:
            return false;
    }
}

function validateSets(value, scope, cb) {

    var setTitle = _.isObject(scope.validation.validationSet.existingSet) ? scope.validation.validationSet.existingSet.title : scope.validation.validationSet.existingSet

    var setobj = _.find(setsarray, function (setval) {
        return setTitle == setval.title;
    });

    var data = _.filter(setobj.values, function (value) {
        return value.isActive === true;
    });

    cb(_.filter(data, function (object) {
        if (isValidNumber(object.value)) {
            return (!isNaN(parseFloat(object.value)) ? parseFloat(object.value) : object.value) === (!isNaN(parseFloat(value)) ? parseFloat(value) : value);
        } else {
            return object.value === value
        }
    }).length > 0);
}

// Code for check if given value is valid number or string
function isValidNumber(value) {
    // Currently we are adding patch for just checking - in the value
    return value.indexOf('-') === -1 || value.indexOf('-') === 0;
}


module.exports = router;
