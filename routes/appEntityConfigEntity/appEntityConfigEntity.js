'use strict'

var schema = require('./schema');
var softSchema = require('./softSchema');
var db = require('../../lib/db');
var config = require('../../lib/config');

var _ = require('lodash');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'appEntityConfigEntity',
    collection: 'appEntityConfigEntity',
    schema: schema,
    softSchema: softSchema
  }//,
  // types: {
  //   GET: {
  //     ALL: {
  //       after: afterGet
  //     }
  //   }
  // }
});

// function afterGet(req,res){
//
//   var linkedEntAttrKeyVal = req.query.entityRecordId)
//
//   if(req.query.entityRecordId){
//     _.forEach(req.data[0].linkedEntity,function(entity){
//
//       var rOptions = {};
//       rOptions['entityRecord.'+attribFIn] =  linkedEntAttrKeyVal
//       rOptions['isDeleted'] = false;
//
//       db[entity._id].count(rOptions,function(err,count){
//
//           if(err){
//             count = 0;
//           }
//
//           entityObj[entity._id] = {
//             _id: entity._id,
//             title: entity.title,
//             questions: entity.questions,
//             entityAttributeSeqId: entityAttributeSeqId,
//             isExpand: false,
//             highlightEntityRecord: !_.isUndefined(entity.highlightEntityRecord) ? true : false,
//             count:count
//           };
//           if (index == 0) {
//             entityObj[entity._id].isExpand = true;
//           }
//
//           if(entities.length<=(Object.keys(entityObj).length)){
//             deffered.resolve(entityObj);
//           }
//
//         })
//     })
//   }
//
//
//
//   // console.log(res.data)
//   res.status(req.status || 200).json(req.data);
//
// }

var debug = route.debug;

module.exports = route.router;
