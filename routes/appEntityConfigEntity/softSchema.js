'use strict'
/**
 * @name appEntityConfigEntity-schema
 * @author Jyotil <jyotil.r@productivet.com>
 *
 * @version 1.0
 */
 var types = require('../../lib/validator/types');

 var rId = types.rId;
 var array = types.array;
 var optionalArray= types.optionalArray;
 var rString = types.rString;
 var rBool = types.rBool;
 var object = types.object.bind(types);
 var string = types.string;

var softSchema = {
  module: array(object({
    _id: rId.label('Type id')
  })),
  entity: object({
    _id: rId.label('Type id'),
    title: rString.label('Type title')
  }),
  linkedApp : optionalArray(object({
    _id: rId.label('Type id'),
    title: rString.label('Type title'),
    isDefault: rBool.label('Default'),
    isDeleted: rBool.label('Deleted')
  })),
  linkedEntity : optionalArray(object({
    _id: rId.label('Type id'),
    title: rString.label('Type title'),
    isDeleted: rBool.label('Deleted')
  })),
  timeZone: string.label('Time Zone')
};

module.exports = softSchema;
