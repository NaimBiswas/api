'use strict'

var express = require('express');
var router = express.Router();
var fs = require('fs-extra');
var logger = require('../../logger');
var currentFileName = __filename;
var debug = require('../../lib/logger').debug('routes:upload');
var request = require("request");
var config = require('../../config/config.js');

router.post('/', function (req, res, next) {

  if (config && config.fileServer &&
    config.fileServer.enabled &&
    config.fileServer.url &&
    config.fileServer.url.post) {


      req.body.strictSSL =false;
      req.pipe(request.post(config.fileServer.url.post, req.body)).pipe(res);

  }
  else {
    handlePost(req, res)
  }
});

// function handlePost(req, res) {
//   logger.log("API Called : api/file :POST method", 'info', currentFileName);
//   req.pipe(req.busboy);
//   req.busboy.on('file', function (fieldname, file, filename) {
//     debug("Uploading: " + filename);
//     var filename = (new Date()).getTime() + '-' + filename;
//     filename = decodeURI(filename);
//     //Path where file will be uploaded
//     var fstream = fs.createWriteStream(process.env.FILE_STORE + filename);

//     file.pipe(fstream);
//     fstream.on('close', function () {
//       debug("Upload Finished of " + filename);
//       logger.log("fsstream : close", 'info', currentFileName);
//       res.status(201).json({ file: filename });
//     });
//   });
// }

function handlePost(req, res) {
  logger.log("API Called : api/file :POST method", 'info', currentFileName);
  var sequenceId;
  req.pipe(req.busboy);
  req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
    if (fieldname === 'sequenceId' && val) {
      sequenceId = val;
    }
  });
  req.busboy.on('file', function (fieldname, file, filename, encoding, mimeType) {
    debug("Uploading: " + filename);

    var actualFilename = filename;
    var splitedFilename = actualFilename.split('.');
    var extension = splitedFilename[splitedFilename.length-1];
    var filestamp = (new Date()).getTime();

    filename =  filestamp + '-' + filename.replace(/[`~!@#$%^&*()_|+\=÷¿?;:'"<>\{\}\[\]\\\/]/gi, '_');
    var filesize = [];
    filename = decodeURI(filename);
    file.on('data', function(data) {
      filesize.push(data.length);
    });
    //Path where file will be uploaded
    var fstream = fs.createWriteStream(process.env.FILE_STORE + filename);

    file.pipe(fstream);
    fstream.on('close', function () {
      debug("Upload Finished of " + filename);
      if (filesize.length) {
        filesize = filesize.reduce(function(f,n){
          return f+n;
          },0);
       filesize = (filesize/(1024*1024)).toFixed(4);
      }
      logger.log("fsstream : close", 'info', currentFileName);
      res.status(201).json({ file: filename, size: filesize, actualFilename,  mimeType, extension, filestamp, sequenceId});
    });
  });
}


router.get('/:file', function (req, res, next) {
  if (config && config.fileServer &&
    config.fileServer.enabled &&
    config.fileServer.url &&
    config.fileServer.url.get) {
    console.log("*****************")
    req.body.strictSSL =false;
    req.pipe(request.get(`${config.fileServer.url.get}${req.params.file}`, req.body)).pipe(res);
  }
  else {
    handleGet(req, res)
  }
});

function handleGet(req, res) {
  logger.log("API Called : api/file :GET method", 'info', currentFileName);

  if (!req.params.file) {
    logger.log("if(!req.params.file) :file not provided", 'error', currentFileName);
    res.status(422).json({ 'message': 'file not provided' })
    return;
  }

  var filename = process.env.FILE_STORE + req.params.file;

  filename = decodeURI(filename);

  if (!fs.existsSync(filename)) {
    logger.log("if(!req.params.file) :file not found", 'error', currentFileName);
    res.status(404).json({ 'message': 'file not found' })
    return;
  }
  var fileToShow = filename.substring(filename.lastIndexOf('/') + 15, filename.length);
  res.download(filename, fileToShow)
}

module.exports = router;
