To setup file upload redirect to other server do the following

1. set follwoing things in `development.js` along with other attributes of module.exports

    ```
    fileServer:{
            url:{
                get:'http://192.168.1.76:3000/file/',
                post:'http://192.168.1.76:3000/file/'
            },        
            enabled:true,
    }
    ```
2. merge the changes made in file `/home/miral/protprojects/perfeqta-folder/api/routes/file/file.js` of branch API: `development_nextRelease_fileUploadRedirect`