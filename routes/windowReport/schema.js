'use strict'
/**
* @name resources-schema
* @author shoaib ganja <shoaib.g@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var rString = types.rString;
var bool = types.bool;
var string = types.string;
var object = types.object.bind(types);
var rId = types.rId;
var any = types.any;

var schema = {
  module: object({
    _id: rId.label('Module id'),
    title: rString.label('Module title'),
  }).label('Module'),
  title: string.label('Title'),
  app: any,
  attributes: any,
  date: any,
  dates: any,
  windowAttribute: any,
  windowAttributeValue: any,
  // startDate: any ,
  // endDate: any,
  includeInActiveRecords: any,
  levelIds: any,
  windowType: any,
  month: any,
  year: any,
  searchByDrop: any,
  searchByRadio: any,
  searchCriteria : any,
  testType: any,
  testTypeOption: any,
  status: any,
  isActive: bool.label('Active?')
};

module.exports = schema;
