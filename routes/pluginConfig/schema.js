
'use strict'
/**
 * @name questions-schema
 * @author vijeta  <vijeta.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var anyArray = types.anyArray;
var rBool = types.rBool;
var bool = types.bool;
var number = types.number;
var string = types.string;
var required = types.required;
var stringArray = types.stringArray;
var any = types.any;
var rBigString = types.bigString;
var bigString = types.bigString;
var optionaldate = types.optionaldate;
var smallString = types.smallString;
var largeNumber = types.largeNumber;



var schema = {
    title : string,
    entity : string,
    isActive : bool,
    config : any,
    timeZone: string.label('Time Zone')

};

module.exports = schema;
