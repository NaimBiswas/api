'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var db = require('../../lib/db');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'pluginConfig',
    collection: 'pluginConfig',
    schema: schema,
    softSchema: softSchema
  }
});

module.exports = route.router;

/**
 * @swagger
 * definition:
 *   Version:
 *     properties:
 *       version:
 *         type: integer
 *       record:
 *         type: integer
 */

/**
 * @swagger
 * definition:
 *    Config:
 *      properties:
 *        accesstoken:
 *          type: string
 */

/**
 * @swagger
 * definition:
 *    PluginConfig:
 *      properties:
 *        id:
 *          type: string
 *        title:
 *          type: string
 *        entity:
 *          type: string
 *        isActive:
 *          type: boolean
 *        config:
 *          type: array
 *          items:
 *            type: object
 *            allOf: 
 *              - $ref: '#/definitions/Config'
 *        createdBy:
 *          type: string
 *        modifiedBy:
 *          type: string
 *        timeZone:
 *          type: string
 *        createdByName:
 *          type: string
 *        modifiedByName:
 *          type: string
 *        createdDate:
 *          type: dateTime
 *        modifiedDate:
 *          type: dateTime
 *        versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *        version:
 *           type: integer
 *        isMajorVersion:
 *           type: boolean
 */

/**
 * @swagger
 * /pluginConfig:
 *  get:
 *    tags: [Administration]
 *    description: List of plugin configurations
 *    produces:
 *      - application/json
 *    parameters:
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ id,title])
 *         in: query
 *         required: false
 *         type: string
 *    responses:
 *       200:
 *         description: An array of plugin configurations
 *         schema:
 *           $ref: '#/definitions/PluginConfig'
 */