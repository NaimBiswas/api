var db = require('../../../lib/resources').db;
var _ = require('lodash');
var q = require('q');
var ObjectId = require('mongodb').ObjectID;

function renameProcess(formid) {
    var deferred = q.defer();
    console.log(formid);
    db.collection('ReportSchemaColumns').findOne({ appId: formid }, function (err, columns) {
        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        db.collection('customReportColumnDetail').findOne({ appId: formid }, function (err, renamecols) {
            if (err) {
                console.log(err);
                deferred.reject(err);
            }
            var arr = [];
            var finalObj = {};
            console.log("-------------------inreportcolumns--------------------------------------------------------");
            if (renamecols) {
                console.log("already available");
                _.forEach(columns.columnInfo, function (cols) {
                    if (cols.extraInformation.maintype != 'zother' && cols.extraInformation.maintype != 'attribute' && cols.extraInformation.maintype != 'reviewer workflow') {
                        var tmp = _.find(renamecols.columns, { questionid: cols.questionid })
                        console.log("-------------------flag--------------------------------------------------------");
                        console.log(tmp);
                        if (!tmp) {
                            cols.originalname = cols.displayName;
                            arr.push(cols);
                        }
                        else {
                            cols.originalname = tmp.originalname;
                            cols.newname = tmp.newname;
                            arr.push(cols)
                        }
                    }
                })

                arr = _.map(arr, function (item) {
                    return _.omit(item, 'extraInformation');
                })
                 console.log("-------------------update--------------------------------------------------------");
                console.log(arr);
                renamecols.columns = arr;
                // renamecols.columns = arr ? _.union(renamecols.columns,arr) : renamecols.columns;
                console.log("-------------------result--------------------------------------------------------");
                console.log(renamecols.columns);
                deferred.resolve(renamecols);

            } else {
                console.log("not available");
                _.forEach(columns.columnInfo, function (cols) {
                    if (cols.extraInformation.maintype != 'zother' && cols.extraInformation.maintype != 'attribute' && cols.extraInformation.maintype != 'reviewer workflow') {
                        cols.originalname = cols.displayName;
                        arr.push(cols);
                    }
                })
                arr = _.map(arr, function (item) {
                    return _.omit(item, 'extraInformation');
                })
                console.log("-------------------result2--------------------------------------------------------");
                console.log(arr);
                finalObj.appId = formid;
                finalObj.appName = columns.title;
                finalObj.columns = arr;
                deferred.resolve(finalObj);
            }

        });
    });
    return deferred.promise;
}

module.exports = {
    renameProcess: renameProcess
}
