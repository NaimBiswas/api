var db = require('../../../lib/resources').db;
var _ = require('lodash');
var q = require('q');
var ObjectId = require('mongodb').ObjectID;
var helper2 = require("./helper2");


function test(formid) {
  var deferred = q.defer();
  console.log(formid);
  db.collection('customReportColumnDetail').findOne({ appId: formid }, function (err, data) {
    // if (err) {
    //     console.log('getSchema: Error');
    //     // logService.debug('inside the getSchema() error reject ::- ' + err);
    //     console.log(err);
    //     deferred.reject(err);
    // }
    console.log("--------------data--------------");
    console.log(data);
    var columnObj = {};
    if (data == null) {
      columnObj.appId = formid;
    }
    else {
      columnObj = data;
    }
    //version = '3.02';
    db.collection('schema').findOne({ _id: ObjectId(formid) }, function (err, responsedata) {
      var latestSchemaVersion = responsedata.version;
      console.log("------------------------------latestSchemaVersion----------------");
      console.log(latestSchemaVersion);
      db.bind(formid);
      db.collection(formid).distinct('parentVersion', { 'entries.status.status': { $ne: 3 } }, function (err, data) {
        if (err) {
          deferred.reject(err);
        }
        var arr = [];
        var version = columnObj.schemaAuditVersion;
        _.forEach(data, function (d, i) {
          arr.push(parseFloat(d));
        });
        //    arr = arr.getUnique();
        if (arr.indexOf(latestSchemaVersion) == -1) {
          arr.push(latestSchemaVersion);
        }
        console.log("---------------arr---------");
        console.log(arr);
        arr.sort();
        console.log(arr);
        if (version) {
          var a = arr.indexOf(version);
          console.log('-------------------------' + a)
          arr.splice(0, a + 1);
        }
        console.log('Error in getRecordVersionDistinct :');
        console.log(version);
        console.log(arr);
        // db.collection('schemaAuditLogs').aggregate([
        // {$match:
        // {identity : ObjectId(formid),
        //   version : { $in: arr}}
        // }],function(err, result) {
        db.collection('schemaAuditLogs').find(
          {
            identity: ObjectId(formid),
            version: { $in: arr }
          }).toArray(function (err, result) {
            if (err) {
              console.log('Error in getting data :' + err);
              deferred.reject(err);
            }
            //   console.log(result);
            var FinalArray = [];
            var schemaAuditLogs = result;
            _.forEach(schemaAuditLogs, function (log, index) {
              columnObj.schemaVersion = log.version;
              var res = [];

              // res[0]=helper2.getAttributes(log);
              res[0] = helper2.getquestion(log);
              res[1] = helper2.getentities(log);

              var finalFields = [];

              _.forEach(res, function (group) {
                _.forEach(group, function (item) {
                  finalFields.push(item)
                  console.log(finalFields);
                })
              });
              // finalFields.version = log.version;
              FinalArray.push(finalFields);
              // fields = [];
              finalFields = [];
              //  console.log(FinalArray);

            })

            //var mainfieldArray = [];
            if (!columnObj.columns) {
              columnObj.columns = FinalArray[0];
            }
            //console.log(columnObj.columns);
            for (var i = 0; i < FinalArray.length; i++) {
              var tempArrayToCompare = FinalArray[i];

              _.forEach(columnObj.columns, function (item, indx) {
                var test = _.find(tempArrayToCompare, { PQComboId: item.PQComboId });
                if (!_.isUndefined(test)) {
                  item.version.push({ version: test.tempschema, comboId: test.comboId });
                  item.originalname = test.originalname;
                }
                //   _.forEach(tempArrayToCompare, function (ap, idx) {
                // if (ap.PQComboId === item.PQComboId) {
                //  //  var fltr= _.differenceBy(item.permissions, ap.permissions, '_id');
                //   item.version.push({version:ap.schemaver, comboId:ap.comboId});
                //   item.originalname = ap.originalname;
                // }
                //    });
              });

              var FilteredArray = _.filter(tempArrayToCompare, function (obj) {

                return !_.find(columnObj.columns, { PQComboId: obj.PQComboId });

              });
              _.forEach(FilteredArray, function (item) {
                item.version.push({ version: item.tempschema, comboId: item.comboId });
                columnObj.columns.push(item);
              });

            }
            deferred.resolve(columnObj);
          });
      });
    });
  });
  return deferred.promise;

}
module.exports = {
  test: test
}
