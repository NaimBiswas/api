var db = require('../../../lib/resources').db;
var _ = require('lodash');
var q = require('q');
var ObjectId = require('mongodb').ObjectID;


//Flat the schema
function getAttributes(schema) {
    // logService.debug('Come in getAttributes function');
    //  var deferred = q.defer()
    var originalObjArr = [];
    // console.log("getAttributes");
    if (schema.hasOwnProperty('attributes') && schema.attributes.length > 0) {
        //  getKeyValue(schema).then(function (keyvallist) {
        //   keyvallist= getKeyValue(schema);
        _.forEach(schema.attributes, function (atrrelement, attrindx) {
            //    var originalobj = { 'title': '' };
            var originalobj = {};

            originalobj.originalname = atrrelement.title;//org
            originalobj.newComboId = schema.identity.toString() + '_' + atrrelement['s#'] + '_' + atrrelement['s#'];
            originalobj.PQComboId = schema.identity.toString() + '_' + atrrelement._id;
            //   originalobj['s#'] = atrrelement['s#']
            originalobj.version = [];
            originalobj.tempschema = schema.version;
            originalObjArr.push(originalobj);//org
        })
        // })

    } else {
        return originalObjArr;
    }
    return originalObjArr;
    // return deferred.promise;
}

//get questiong based on Schema
function getquestion(schema) {

    var originalObjArr = [];
    if (schema.hasOwnProperty('procedures') && schema.procedures.length > 0) {

        var countProcRepeat = _.countBy(schema.procedures, function (obj) {
            return obj._id;
        });

        var procIds = [];
        console.log(countProcRepeat);
        _.forEach(schema.procedures, function (proc, procindx) {
            //  var PComboId = proc._id;
            var PComboId = schema.identity.toString() + '_' + proc._id;

            if (countProcRepeat[proc._id] > 1 && procIds.indexOf(proc._id) != -1) {

                PComboId += '_' + proc['s#'];

            }
            procIds.push(proc._id);
            var quesIds = [];
            if (proc.hasOwnProperty('questions') && proc.questions.length > 0) {

                var countQuesRepeat = _.countBy(proc.questions, function (obj) {
                    return obj._id;
                });

                _.forEach(proc.questions, function (ques, questindx) {

                    var QComboId = ques._id;
                    var originalobj = {};//org

                    if (countQuesRepeat[ques._id] > 1 && quesIds.indexOf(ques._id) != -1) {

                        QComboId += '_' + ques['s#'];
                    }
                    quesIds.push(ques._id);

                    originalobj.originalname = ques.title;//org
                    //     originalobj['s#'] = ques['s#'];
                    originalobj.comboId = ques.comboId;
                    originalobj.newComboId = schema.identity.toString() + '_' + proc['s#'] + '_' + ques['s#'];
                    originalobj.version = [];
                    originalobj.tempschema = schema.version;
                    originalobj.PQComboId = PComboId + '_' + QComboId;
                    originalObjArr.push(originalobj);
                    //     })
                })

                //      return result;
            }

        })

    } else {
        //  deferred.resolve(result);
        return originalObjArr;
    }
    return originalObjArr;
    // return deferred.promise
}
//get the entitybased on the particular form
function getentities(schema) {
    // var deferred = q.defer()
    var originalObjArr = [];
    if (schema.hasOwnProperty('entities') && schema.entities.length > 0) {
        var countEntityRepeat = _.countBy(schema.entities, function (obj) {
            return obj._id;
        });
        var entityIds = [];
        console.log(countEntityRepeat);
        _.forEach(schema.entities, function (entity, entityindx) {
            //  var PQComboId = entity._id;
            var PQComboId = schema.identity.toString() + '_' + entity._id;

            if (countEntityRepeat[entity._id] > 1 && entityIds.indexOf(entity._id) != -1) {
                PQComboId += '_' + entity['s#'];
            }
            entityIds.push(entity._id);
            if (entity.hasOwnProperty('questions') && entity.questions.length > 0) {

                _.forEach(entity.questions, function (ques, questindx) {

                    var originalobj = {};//org
                    originalobj.originalname = ques.title;//org
                    //      originalobj['s#'] = ques['s#'];
                    originalobj.comboId = ques.comboId;
                    originalobj.newComboId = schema.identity.toString() + '_' + entity['s#'] + '_' + ques['s#'];
                    originalobj.PQComboId = PQComboId + '_' + ques['s#'];
                    originalobj.version = [];
                    originalobj.tempschema = schema.version;
                    originalObjArr.push(originalobj);//org

                })
            }
        })
    } else {
        //   deferred.resolve(result);
        return originalObjArr;
    }
    return originalObjArr;
    // return deferred.promise
}

module.exports = {
    getAttributes: getAttributes,
    getquestion: getquestion,
    getentities: getentities
}
