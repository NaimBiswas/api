var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('../../lib/logger/logger').configure(__dirname, enableBunyan);
var db = require('../../lib/db');
var logger = require('../../logger');
var q = require('q');
var moment = require('moment');
var tz = require('moment-timezone');
var valid = require('../appSync/validation');
var common = require('../appSync/common');
var amqp = require('amqplib/callback_api');
var _ = require('lodash');
var Pusher = require('pusher');
var config = require('../../config/config.js');

var j;
var iteratation = 0;
var jsonObject;
var failedRecords = [];
var entityValues = [];
var entries = {};
var entityRecord = {};
var failreason = {};
var conne = {};
var syncId;
var attrPass1 = [];
var attrPass2 = [];
var checkSitesLevel = [];

var pusher = new Pusher({
    appId: '355220',
    key: 'e2f188ad9a896068bc3f',
    secret: '3a710a92b90538114e6c',
    cluster: 'ap2',
    encrypted: true
});

amqp.connect('amqp://' + config.mq.connection, function (err, conn) {
    if (err) {
        return err;
    }
    conn.createChannel(function (err, ch) {
        if (err) {
            //todo get know about the channel connection errro.
            return err;
        }
        var q = config.mq.pluginMq.queue;
        ch.assertQueue(q, { durable: false });
        ch.prefetch(1);
        ch.consume(q, function (msg) {
            failedRecords = [];
            if (msg && msg.content) {
                cleanTempVariables();
                try {
                    prepareData(JSON.parse(msg.content)).then(function (data) {
                        logger.log("All went good");
                        cleanTempVariables();
                        ch.ack(msg);
                    }, function (err) {
                        updateDbFlag(JSON.parse(msg.content))
                        ch.ack(msg);
                        cleanTempVariables();
                    })
                        .catch(function (err) {
                            updateDbFlag(JSON.parse(msg.content))
                            ch.ack(msg);
                            cleanTempVariables();
                        });
                }
                catch (e) {
                    updateDbFlag(JSON.parse(msg.content))
                    ch.ack(msg);
                    cleanTempVariables();
                }
            } else {
                ch.ack(msg);
            }
        }, { noAck: false });
    });
});

function cleanTempVariables() {
    j = 0;
    iteratation = 0;
    jsonObject;
    failedRecords = [];
    entityValues = [];
    entries = {};
    entityRecord = {};
    failreason = {};
    conne = {};
    syncId;
    attrPass1 = [];
    attrPass2 = [];
}

function updateDbFlag(data) {
    db.collection('plugin').update({ _id: db.ObjectID(data.id) }, { $set: { isSyncing: false } }, function (err, res) {
        if (err) {
            logger.log(err);
        } else {
            logger.log("success");
        }
    });
}

// Check Permission of Plugin in PluginConfiguration
function checkPluginPermission(instanceID, callback) {
    db.pluginlist.find({ 'entity': instanceID, 'isActive': true }).toArray(function (err, results) {
        if (results.length) {
            callback(true);
        } else {
            callback(false);
        }
    });
}

// Check Permission of Instance of Plugin in PluginConfiguration
function checkInstancePermission(instanceID, callback) {
    db.pluginConfig.find({ '_id': db.ObjectID(instanceID), 'isActive': true }).toArray(function (err, results) {
        if (results.length) {
            callback(true);
        } else {
            callback(false);
        }
    });
}

function getDataWithValidation(req, newobject) {
    logger.log("--------step 8------getDataWithValidation---", 'debug', 'receive.js');

    var deffer = q.defer();
    if (newobject) {
        logger.log("--------step 9------importantcsv---", 'debug', 'receive.js');
        logger.log(req.mappingtool);

        checkPluginPermission(req.mappingtool, function (checkPluginIsActive) {
            if (checkPluginIsActive) {
                if (newobject.mappingObject[0]) {
                    checkInstancePermission(newobject.mappingObject[0].instance, function (checkInstanceIsActive) {
                        if (checkInstanceIsActive) {
                            var mappingtool = require('../pluginCommon/' + req.mappingtool);
                            mappingtool.getRecords(newobject.pluginAppRecordIds || [],
                                newobject.configDetail[0] || [],
                                req.sapp, newobject.mappingObject[0], req)
                                .then(function (res) {
                                    logger.log("--------step 9------getRecords---", 'debug', 'receive.js');
                                    _.forEach(res, function (data) {
                                        try {
                                            validateData(newobject, data, req);
                                        }
                                        catch (err) {
                                            return deffer.reject(err);
                                        }

                                    });
                                    _.forEach(failedRecords || [], function (pdata, indx) {
                                        if (_.size(pdata.failreason) == 0) {
                                            if (req.type == 'app') {
                                                validateAttribute(newobject.processSchema[0], pdata, indx);
                                            } else {
                                                validateAttributeEntity(newobject.processSchema[0], pdata, indx);
                                            }
                                        }
                                    });
                                    _.forEach(attrPass1 || [], function (pdata, idz) {
                                        if (pdata) {
                                            if (req.type == 'app') {
                                                validateAttributeDb(newobject.processSchema[0], newobject.schemaentries, pdata, idz);
                                            } else {
                                                validateAttributeDbEntity(newobject.processSchema[0], newobject.schemaentries, pdata, idz);
                                            }
                                        }
                                    });

                                    _.forEach(attrPass1 || [], function (pdata, idz) {
                                        if (pdata && _.size(pdata.failreason) == 0) {
                                            delete pdata.failreason;
                                        }
                                    });

                                    if (failedRecords.length) {
                                        logger.log("--------step 15------insertFailedRecords---", 'debug', 'receive.js');

                                        insertFailedRecords(failedRecords).then(function (data) {
                                            logger.log("-------------------beforepusher------------------------", 'debug', 'receive.js');
                                            pusher.trigger('plugin-sync', req.user + '' + config.serverName, {
                                                "message": newobject.mappingObject[0].title + " is successully synced."
                                            });
                                            if (req.autoSyncId && req.transactionId) {
                                                db.collection('autoSyncHistory').findOneAndUpdate({ 'autoSyncId': req.autoSyncId, 'transactionId': req.transactionId }, { $set: { 'endDate': new Date() } }, function (err, success) {
                                                    if (err) {
                                                        logger.log(err);
                                                    }
                                                })
                                            }

                                            var tempFailedRecords = {};

                                            if (req.mappingtool == 'zoho') {
                                                tempFailedRecords = failedRecords[0];
                                            } else {
                                                tempFailedRecords = failedRecords[failedRecords.length - 1];
                                            }
                                            if (req.mappingtool == 'qc2') {
                                                var tempLength;
                                                if (req.pluginUniqueIdentifier) {
                                                    tempLength = +(req.pluginUniqueIdentifier) + failedRecords.length;
                                                } else {
                                                    tempLength = failedRecords.length;
                                                }

                                                db.collection('plugin').update({ _id: db.ObjectID(req.id) }, { $set: { isSyncing: false, pluginUniqueIdentifier: tempLength } }, function (err, res) {
                                                    if (err) {
                                                        logger.log(err);
                                                    } else {
                                                        logger.log("success");
                                                    }
                                                });
                                            } else {
                                                db.collection('plugin').update({ _id: db.ObjectID(req.id) }, { $set: { isSyncing: false, pluginUniqueIdentifier: tempFailedRecords.pluginUniqueIdentifier } }, function (err, res) {
                                                    if (err) {
                                                        logger.log(err);
                                                    } else {
                                                        logger.log("success");
                                                    }
                                                });
                                            }

                                            logger.log("-------------------afterpusher------------------------", 'debug', 'receive.js');
                                            deffer.resolve('OK');
                                        }, function (err) {
                                            logger.log(err);
                                            return deffer.reject(err);
                                        });
                                    } else {
                                        logger.log("--------step 15------insertFailedRecords---", 'debug', 'receive.js');

                                        pusher.trigger('plugin-sync', req.user + '' + config.serverName, {
                                            "message": newobject.mappingObject[0].title + " is successully synced."
                                        });

                                        if (req.autoSyncId && req.transactionId) {
                                            db.collection('autoSyncHistory').findOneAndUpdate({ 'autoSyncId': req.autoSyncId, 'transactionId': req.transactionId }, { $set: { 'endDate': new Date() } }, function (err, success) {
                                                if (err) {
                                                    logger.log(err);
                                                }
                                            })
                                        }
                                        db.collection('plugin').update({ _id: db.ObjectID(req.id) }, { $set: { isSyncing: false } }, function (err, res) {
                                            if (err) {
                                                logger.log(err);
                                            } else {
                                                logger.log("-------------------success------------------------");
                                            }
                                        });
                                        return deffer.resolve('OK');
                                        //  res.send("Sync completed");
                                    }
                                }, function (err) {
                                    return deffer.reject(err);
                                });
                        }
                        else {
                            return deffer.reject('No Perm');
                        }
                    });
                }
                else {
                    return deffer.reject('No Perm');
                }
            }
            else {
                return deffer.reject('No Perm');
            }
        });
    } else {
        setTimeout(function () {
            deffer.resolve('OK');
        }, 0)
    }
    return deffer.promise;
}

function prepareData(req) {
    var deffer = q.defer();
    logger.log("Step 1: Prepare Data");
    if (req.type) {
        getData(req).then(function (data) {
            deffer.resolve(data);
        }, function (err) {
            deffer.reject(err);
        });
    } else {
        common.getPlugin(req.id).then(function (resp) {
            if (resp && !resp.length) {
                return deffer.reject('NOT OK');
            }
            var data = {};
            data.id = req.id;
            data.appId = resp[0].appId;
            data.sapp = resp[0].sappTitle;
            data.user = resp[0].modifiedBy.toString();
            data.configId = resp[0].instance;
            data.appVersion = resp[0].appVersion;
            data.mappingtool = resp[0].mappingTool;
            data.type = resp[0].type;
            data.autoSyncId = req.autoSyncId;
            data.transactionId = req.transactionId;
            data.pluginUniqueIdentifier = req.pluginUniqueIdentifier ? req.pluginUniqueIdentifier : '';
            getData(data).then(function (data) {
                return deffer.resolve(data);
            }, function (err) {
                return deffer.reject(err);
            });
        }, function (err) {
            return deffer.reject(err);
        });
    }
    return deffer.promise;
}

//get apprecord,mapping,schema,pluginrecord's Id

function getData(req) {
    logger.log("Step 2: Get Data");

    var deffer = q.defer();
    syncId = +new Date();
    failedRecords = [];
    var finalObject = {
        mappingObject: [],
        pluginAppRecordIds: [],
        tempRecordIds: [],
        processSchema: [],
        configDetail: {},
        sites: [],
        entityValues: []
    };
    q.all([
        common.getPlugin(req.id),
        common.getTempRecord(req.id),
        common.getUserData(req.user),
        common.getSchema(req.appId, req.appVersion, req.type),
        common.getPluginConfig(req.configId || ''),
        common.getSites(),
        common.sets(),
        common.getAppRecord(req.appId, req.type),
        common.getTypes()
    ]).then(function (result) {
        finalObject.mappingObject = result[0];
        finalObject.tempRecordIds = result[1];
        finalObject.userdata = result[2];
        finalObject.processSchema = result[3];
        finalObject.configDetail = result[4];
        finalObject.sites = result[5];
        finalObject.setsarray = result[6];
        finalObject.schemaentries = result[7];
        finalObject.types = result[8];

        var entities = [];
        if (req.type == 'app') {
            entities = finalObject.processSchema[0].entities;
        }
        if (req.type == 'entity') {
            var linkedEntitiesIds = [];
            _.forEach(finalObject.processSchema[0].questions || [], function (question) {
                if (question && question.linkToEntity && Object.keys(question.linkToEntity).length) {
                    linkedEntitiesIds.push({ _id: question.linkToEntity.entityId })
                }
            });
            entities = linkedEntitiesIds;
        }
        getEntityValue(entities).then(function (data) {
            if (req.mappingtool == 'zoho' || req.mappingtool == 'csv' || req.mappingtool == 'infusionsoft' || req.mappingtool == 'qc2' || req.mappingtool == 'quickbooks') {
                //performSync(req, finalObject);
                getDataWithValidation(req, finalObject).then(function (data) {
                    // Vishesh
                    return deffer.resolve(data);
                }, function (err) {
                    return deffer.reject(err);
                });
            } else {
                findUniqRecords(req.mappingtool, finalObject.configDetail[0] || [], req.sapp, finalObject.mappingObject[0])
                    .then(function (data1) {
                        finalObject.pluginAppRecordIds = data1;
                        if (finalObject.tempRecordIds.length) {
                            _.forEach(finalObject.tempRecordIds, function (obj) {
                                if (obj.salesforceId) {
                                    if (finalObject.pluginAppRecordIds.indexOf(obj.salesforceId) != -1) {
                                        finalObject.pluginAppRecordIds.splice(finalObject.pluginAppRecordIds.indexOf(obj.salesforceId), 1);
                                    }
                                }
                            });
                        }
                        if (finalObject.pluginAppRecordIds.length == 0) {
                            if (req.autoSyncId && req.transactionId) {
                                db.collection('autoSyncHistory').findOneAndUpdate({ 'autoSyncId': req.autoSyncId, 'transactionId': req.transactionId }, { $set: { 'endDate': new Date() } }, function (err, success) {
                                    if (err) {
                                        logger.log(err);
                                    }
                                })
                            }
                            db.plugin.update({ _id: db.ObjectID(req.id) }, { $set: { isSyncing: false } });
                            logger.log("-------------------beforepusher------------------------", 'debug', 'receive.js');
                            pusher.trigger('plugin-sync', req.user + '' + config.serverName, {
                                "message": finalObject.mappingObject[0].title + " is successully synced."
                            });
                            logger.log("There are no unique records to sync.");
                            return deffer.resolve('OK');
                        }
                        //performSync(req, finalObject);
                        getDataWithValidation(req, finalObject).then(function (data) {
                            return deffer.resolve(data);
                        }, function (err) {
                            return deffer.reject(err);
                        });
                    }, function (err) {
                        return deffer.reject(err);
                    });
            }
        }, function (err) {
            return deffer.reject(err);
        });
    }).catch(function (error) {
        deffer.reject(error);
    });
    return deffer.promise;
}

function validateData(newobject, jsonObject1, req) {
    logger.log("--------step 10 ------validateData---", 'debug', 'receive.js');
    entries = {};
    entityRecord = {};
    var levels = [];
    var levelIds = [];
    failreason = {};
    var sfData = [];
    var tempSites = [];
    if (newobject && newobject.mappingObject && newobject.mappingObject[0] && newobject.mappingObject[0].psqueDetail) {
        _.forEach(newobject.mappingObject[0].psqueDetail, function (obj, index) {
            if (obj.sapp && (obj.sapp.title || obj.sapp.title == '0')) {
                var actualValue;
                if (!_.isUndefined(jsonObject1[obj.sapp.title])) {
                    actualValue = jsonObject1[obj.sapp.title];
                } else {
                    actualValue = (jsonObject1[obj.sapp.label]);
                }
                if (_.isArray(actualValue)) {
                    actualValue = actualValue[0] ? actualValue[0] : ''
                }
                if (newobject.mappingObject[0].mappingTool == "csv" && newobject.mappingObject[0].isAllowHeader == false) {
                    actualValue = actualValue.replace(/\"/g, "");
                }
                var valueToDisplay = actualValue;
                var valueToEntry = actualValue;
                if (obj.paap.queformat && obj.paap.queformat.format && obj.paap.queformat.format.title == 'Date') {
                    if (valueToDisplay !== null) {
                        var allowItems = _.reduce(newobject.types[0].formats, function (o, obj) {
                            if (obj.title === 'Date') {
                                o.push(obj.metadata[0].allowItems)
                            }
                            return o;
                        }, [])[0];
                        var dateFormt = _.reduce(allowItems, function (o, item) {
                            if (item._id === obj.paap.queformat.format.metadata.datetime) {
                                o.push(item);
                            }
                            return o;
                        }, [])[0];

                        if (!isNumber(valueToDisplay) && new Date(valueToDisplay) instanceof Date && !_.isNaN(Date.parse(valueToDisplay))) {
                            valueToDisplay = moment(actualValue).format('MM/DD/YYYY');
                            var one;
                            if (newobject.mappingObject[0].mappingTool == 'csv') {
                                one = moment.tz(actualValue, dateFormt.title, config.timeZonesP[config.currentTimeZoneP]).toISOString();
                            } else {
                                one = moment.tz(actualValue, config.timeZonesP[config.currentTimeZoneP]).toISOString();
                            }
                            valueToEntry = one;
                        }
                    }
                }
                if (obj.paap.queformat && obj.paap.queformat.format && (obj.paap.queformat.format.title === 'Number' || obj.paap.queformat.format.title === 'Percentage' || obj.paap.queformat.format.title === 'Currency' || obj.paap.queformat.format.title === 'Scientific')) {
                    var decimals = parseInt(obj.paap.queformat.format.metadata.Decimal);
                    decimals = decimals > 25 ? 25 : decimals;

                    if (_.isNaN(decimals) || decimals == '') {
                        valueToDisplay = valueToEntry;
                        if (+(valueToEntry) || (+(valueToEntry) === 0)) {
                            valueToEntry = +(valueToEntry);
                        }
                        valueToDisplay = valueToEntry;
                    } else {
                        valueToEntry = Math.floor(parseFloat(actualValue));
                    }
                }
                if (obj.paap.queformat && obj.paap.queformat.format && obj.paap.queformat.format.title == 'Exponential') {
                    valueToEntry = actualValue * (Math.pow(10, !_.isUndefined(obj.paap.queformat.format.metadata.power) ? obj.paap.queformat.format.metadata.power : 0));
                    valueToDisplay = valueToEntry;
                }
                if (obj.paap.queformat && obj.paap.queformat.format && obj.paap.queformat.format.title == 'None' && obj.paap.addBarcodeRule) {
                    if (!_.isUndefined(actualValue) && !_.isNull(actualValue) && actualValue != '') {

                        var prefixRule = !_.isUndefined(obj.paap.addBarcodeRulePrefix) ? obj.paap.addBarcodeRulePrefix : "";
                        var suffixRule = !_.isUndefined(obj.paap.addBarcodeRuleSuffix) ? obj.paap.addBarcodeRuleSuffix : "";
                        // remove rule from prefix
                        _.forEach(prefixRule.split(','), function (v) {
                            if (actualValue.startsWith(v)) {
                                actualValue = actualValue.substring(v.length);
                            }
                        });
                        // remove rule from suffix
                        _.forEach(suffixRule.split(','), function (v) {
                            if (actualValue.endsWith(v)) {
                                actualValue = actualValue.substring(0, (actualValue.length - (v.length)));
                            }
                        });
                        valueToDisplay = actualValue;
                        valueToEntry = actualValue;
                    }
                }
                if (obj.paap.queformat && obj.paap.queformat.format && obj.paap.queformat.format.title == 'Time') {
                    // var checkInteger = Number.isInteger(valueToDisplay);
                    if (valueToDisplay && valueToDisplay !== null) {
                        var array = [];
                        if ((valueToDisplay.toString()).indexOf(':') !== -1) {
                            var valueToDisplaytemp; //Vishesh API - 480
                            if (newobject.mappingObject[0].mappingTool != 'csv') {
                                valueToDisplaytemp = new Date(valueToDisplay).toLocaleTimeString();
                            }
                            else {
                                valueToDisplaytemp = valueToDisplay;
                            }
                            if (valueToDisplaytemp != "Invalid Date") {
                                actualValue = valueToDisplaytemp;
                                array = valueToDisplaytemp.split(':');
                                var h = parseInt(array[0]);
                                var m = parseInt(array[1]);
                                var s = parseInt(array[2]) || 00;
                                // valueToDisplaytemp = moment.tz({ year: 1970, month: 0, day: 1, hour: h, minute: m, second: s}, config.timeZonesP[config.currentTimeZoneP]).format('HH:mm:ss');
                                // actualValue = moment.tz({ year: 1970, month: 0, day: 1, hour: h, minute: m }, config.timeZonesP[config.currentTimeZoneP]).format('HH:mm:ss');
                                var one = moment.tz({ year: 1970, month: 0, day: 1, hour: h, minute: m, second: s }, config.timeZonesP[config.currentTimeZoneP]).toISOString();
                                valueToEntry = one;
                            }
                            else {
                                actualValue = valueToDisplay;
                            }

                        } else {
                            actualValue = valueToDisplay;
                        }
                    }
                    else {
                        actualValue = valueToDisplay;
                    }
                }

                if (obj.paap.quetype == 'attributes') {
                    sfData.push({ 'value': valueToDisplay, 'seq': obj.paap.comboId });
                    if (!checkValidation(obj.paap, actualValue, obj.sapp.stype, newobject)) {
                        valueToEntry = (obj.paap.queformat.format.title == "Number") ? parseInt(valueToEntry) : valueToEntry; //Vishesh API-481
                        entries[obj.paap.comboId || obj.paap['s#']] = {
                            'attributes': valueToEntry
                        };
                    }
                } else if (obj.paap.quetype == 'procedure') {
                    var x = obj.paap.comboId.substring(obj.paap.comboId.indexOf('-') + 1, obj.paap.comboId.length);
                    var y = obj.paap.comboId.substring(0, obj.paap.comboId.indexOf('-'));
                    sfData.push({ 'value': valueToDisplay, 'seq': y });
                    if (!checkValidation(obj.paap, actualValue, obj.sapp.stype, newobject)) {
                        if (obj.paap.queformat.title == 'Checkbox') {
                            var datas = [];
                            datas.push(valueToEntry.toString());
                            entries[obj.paap.comboId] = {
                                [x]: datas
                            };

                        } else {
                            entries[obj.paap.comboId] = {
                                [x]: valueToEntry
                            };
                        }
                    }
                } else if (obj.paap.quetype == 'entity') {
                    logger.log("------entity----", 'debug', 'receive.js')
                    var x = obj.paap.comboId.substring(obj.paap.comboId.indexOf('-') + 1, obj.paap.comboId.length);
                    var y = obj.paap.comboId.substring(0, obj.paap.comboId.indexOf('-'));

                    sfData.push({ 'value': valueToDisplay, 'seq': y });
                    if (_.isUndefined(dateFormt)) {
                        var dateFormt = [];
                    }
                    if (!entityValidation(obj.paap, actualValue, obj.sapp.stype, newobject.processSchema[0], entityValues, dateFormt.title || [])) {
                        entries[obj.paap.comboId] = {
                            [x]: valueToEntry
                        };
                    }
                } else if (obj.paap.quetype == 'site') {
                    sfData.push({ 'value': valueToDisplay, 'seq': obj.paap.comboId });
                    tempSites.push(valueToDisplay);
                    var foundSite = newobject.processSchema[0].allicableSites[index].find(function (site) {
                        return site.title == (valueToDisplay || '').toString().trim();
                    });

                    // if(foundSite && foundSite._id && foundSite.title) {
                    foundSite.title = foundSite.title.replace("–", "-");
                    var sitobj = {
                        levelId: index,
                        siteId: foundSite._id,
                        title: foundSite.title
                    };
                    levels.push(sitobj);
                    levelIds.push(foundSite._id);
                // }
                } else if (obj.paap.quetype == 'entities') {
                    sfData.push({ 'value': valueToDisplay, 'seq': obj.paap.comboId });
                    if (!checkValidation(obj.paap, actualValue, obj.sapp.stype, newobject)) {

                        if (obj.paap.queformat.title == 'Checkbox') {
                            var datas = [];
                            datas.push(valueToEntry.toString());
                            entityRecord[obj.paap.comboId] = datas;

                        } else {
                            entityRecord[obj.paap.comboId] = valueToEntry;
                        }

                    }
                }
            }
            else {
                sfData.push({ 'value': '', 'seq': obj.paap.comboId });
            }
        });
    }

    if (newobject && newobject.mappingObject[0] && newobject.mappingObject[0].isDefaultSites) {
        if (newobject.mappingObject[0].levels) {
            _.forEach(newobject.mappingObject[0].levels, function (obj) {
                var a = _.find(newobject.sites, function (o) {
                    return o._id == obj._id;
                });
                var siteObj = {
                    levelId: a.level._id,
                    siteId: a._id.toString(),
                    title: a.title
                }
                levels.push(siteObj);
                levelIds.push(a._id.toString());
            });
        }
    }

    if (newobject && newobject.mappingObject[0] && newobject.processSchema[0] && !newobject.mappingObject[0].isDefaultSites && newobject.processSchema[0].siteYesNo && newobject.processSchema[0].siteYesNo == 'no') {
        _.forEach(newobject.processSchema[0].allicableSites, function (site) {
            var a = _.find(newobject.sites, function (obj) {
                if (obj._id == site[0]._id) {
                    return obj;
                }
            })
            var sitobj = {
                siteId: a._id,
                title: a.title,
                levelId: a.level._id
            };
            levels.push(sitobj);
            levelIds.push(site[0]._id);
        });
    }
    logger.log("Complete Sync");

    if (levels && newobject.mappingObject[0].type == 'app') {
        if (levels.length != newobject.processSchema[0].allicableSites.length) {
            _.forEach(tempSites, function (obj, ix) {
                failreason[ix] = 'Sites not filled according to their level.';
            });
        }
    }

    var dumpDataObj = {};

    if (newobject.mappingObject[0].type == 'app') {
        entries.status = {};
        entries.status.status = 4;
        dumpDataObj = {
            'reviewStatus': 'Draft',
            'entries': entries,
            'levels': levels,
            'levelIds': levelIds,
            'createdDateInfo': new Date(),
            'createdByInfo': { _id: newobject.userdata[0]._id, username: newobject.userdata[0].username, firstname: newobject.userdata[0].firstname, lastname: newobject.userdata[0].lastname },
            'parentVersion': newobject.mappingObject[0].appVersion,
            'salesforceId': jsonObject1.Id,
            'createdBy': newobject.userdata[0]._id,
            'modifiedBy': newobject.userdata[0]._id,
            'isMajorVersion': true,
            'isAuditing': false,
            'createdByName': newobject.userdata[0].firstname + " " + newobject.userdata[0].lastname + " (" + newobject.userdata[0].username + ")",
            'modifiedByName': newobject.userdata[0].firstname + " " + newobject.userdata[0].lastname + " (" + newobject.userdata[0].username + ")",
            'createdDate': new Date(),
            'modifiedDate': new Date(),
            'failreason': failreason,
            'values': sfData,
            'syncId': syncId,
            'isDeleted': false,
            'mappingId': newobject.mappingObject[0]._id.toString(),
            'type': 'app',
            'transactionId': req.transactionId
        }
    }

    if (newobject.mappingObject[0].type == 'entity') {
        entityRecord['isActive'] = true;
        dumpDataObj = {
            'entityRecord': entityRecord,
            'isDeleted': false,
            'salesforceId': jsonObject1.Id,
            'createdBy': newobject.userdata[0]._id,
            'modifiedBy': newobject.userdata[0]._id,
            'createdByName': newobject.userdata[0].firstname + " " + newobject.userdata[0].lastname + " (" + newobject.userdata[0].username + ")",
            'modifiedByName': newobject.userdata[0].firstname + " " + newobject.userdata[0].lastname + " (" + newobject.userdata[0].username + ")",
            'createdDate': new Date(),
            'modifiedDate': new Date(),
            'isAuditing': false,
            'failreason': failreason,
            'values': sfData,
            'syncId': syncId,
            'mappingId': newobject.mappingObject[0]._id.toString(),
            'type': 'entity',
            'transactionId': req.transactionId
        }
    }

    if (req.mappingtool == 'infusionsoft' || req.mappingtool == 'qc2') {
        dumpDataObj['pluginUniqueIdentifier'] = jsonObject1.Id;
    } else if (req.mappingtool == 'zoho') {
        dumpDataObj['pluginUniqueIdentifier'] = jsonObject1['Modified Time'];
    } else if (req.mappingtool == 'salesforce') {
        dumpDataObj['pluginUniqueIdentifier'] = jsonObject1['CreatedDate'];
    } else if (req.mappingtool == 'quickbooks') {
        dumpDataObj['pluginUniqueIdentifier'] = jsonObject1['CreateTime'];
    } else {
        dumpDataObj['pluginUniqueIdentifier'] = '';
    }
    failedRecords.push(dumpDataObj);

}


function checkValidation(paap, value, stype, newobject) {
    logger.log("--------step 11 ------checkValidation---", 'debug', 'receive.js');
    try {
        var x;
        if (paap && paap.comboId && paap.comboId.indexOf('-') != -1) {
            x = paap.comboId.substring(0, paap.comboId.indexOf('-'));
        }
        if (paap && !value) {
            if (paap.isOptinal) {
                return false;
            } else {
                failreason[x || paap.comboId] = "Please provide valid value."
                return true;
            }
        }
        if (paap && paap.linkToEntity && _.size(paap.linkToEntity) > 0) {
            return linkEntityValidation(value, paap);
        }

        if (paap && paap.queformat && paap.queformat.format) {
            switch (paap.queformat.format.title) {
                case "None":
                    return validateField(paap, value, stype, newobject, false);
                    break;
                case "Number":
                    return validateField(paap, value, stype, newobject, true);
                    break;
                case "Exponential":
                    return validateField(paap, value, stype, newobject, true);
                    break;
                case "Percentage":
                    return validateField(paap, value, stype, newobject, true);
                    break;
                case "Currency":
                    return validateField(paap, value, stype, newobject, true);
                    break;
                case "Scientific":
                    return validateField(paap, value, stype, newobject, true);
                    break;
                case "Date":
                    return validateDate(paap, value, stype, newobject);
                    break;
                case "Time":
                    return validateTime(paap, value, stype, newobject);
                    break;
                default:
                    return true;
                    break;
            }
        }
    } catch (e) {
        logger.log(e, 'debug', 'receive,js');
    }
}

//entityValues


function getEntities(entities) {
    logger.log("---------step 5------getEntities--", 'debug', 'receive.js')
    var deffer = q.defer();
    db.collection([entities].toString()).find({ 'entityRecord.isActive': true }).toArray(function (err, data) {
        if (err) {
            return deffer.reject(data);
        }
        return deffer.resolve(data);
    });
    return deffer.promise;
}

function getEntityValue(entities) {
    logger.log("---------step 4------getEntityValue--", 'debug', 'receive.js')
    var deffer = q.defer();
    var entityPromise = [];
    _.forEach(entities, function (entity) {
        entityPromise.push(getEntities(entity._id.toString()));
    });
    q.all(entityPromise).then(function (entDatas) {
        _.forEach(entities, function (entity, iteratation) {
            entityValues[entities[iteratation]._id] = [];
            entityValues[entities[iteratation]._id].push(entDatas[iteratation]);
        });
        return deffer.resolve('OK');
    }, function (err) {
        return deffer.reject(err);
    });
    return deffer.promise;
}

function entityValidation(paap, value, stype, processSchema, entityValues, dateFormat) {
    var x = paap.comboId.substring(paap.comboId.indexOf('-') + 1, paap.comboId.length);
    var y = paap.comboId.substring(0, paap.comboId.indexOf('-'));
    if (value) {
        var entity = _.filter(processSchema.entities, { 's#': x });
        if (entity) {
            if (entityValues && entityValues[entity[0]._id]) {
                var values;
                if (paap.queformat.format.title == 'Date') {
                    values = _.filter(entityValues[entity[0]._id][0], function (entityValue) {
                        value = moment(value).format(dateFormat);
                        var two = moment.tz(value, dateFormat, config.timeZonesP[config.currentTimeZoneP]).toISOString();
                        return entityValue.entityRecord[y] == two;

                    });
                } else {

                    values = _.filter(entityValues[entity[0]._id][0], function (entityValue) {
                        return entityValue.entityRecord[y] == value;
                    });
                }
            } else {
                return true
            }
        }
        if (values && values.length) {
            var questions = _.filter(entity[0].questions, { 'isKeyValue': false });
            _.forEach(questions, function (q) {
                entries[q['s#'] + '-' + entity[0]['s#']] = {};
                entries[q['s#'] + '-' + entity[0]['s#']][entity[0]['s#']] = values[0].entityRecord[q['s#']];
            });
            return false;
        } else {
            failreason[y] = "Key attribute is not matched or is inactive.";
            return true;
        }
    } else {
        failreason[y] = "Please provide any value";
        return true;
    }

}

function validateField(paap, value, stype, newobject, isNumber) {
    logger.log("--------step 12 ------validateField---", 'debug', 'receive.js');
    if (paap && paap.comboId && paap.comboId.indexOf('-') != -1) {
        var x = paap.comboId.substring(0, paap.comboId.indexOf('-'));
    }

    if (value) {
        if (isNumber) {
            if (!(/^\d+$/.test(value) || (/^\s*[-+]?(\d*\.?\d+|\d+\.)(e[-+]?[0-9]+)?\s*$/i.test(value)))) {
                failreason[x || paap.comboId] = "Value does not match. Please provide valid number.";
                return false;
            } else {
                // Case for Validting regularExpression, validationSet and conditions on question
                if (paap.quevalidation) {

                    if (paap.quevalidation.regularExpression) {
                        // written col instead of value
                        // written col instead of value
                        if (!(new RegExp(paap.quevalidation.regularExpression.regEx)).test(value)) {
                            failreason[x || paap.comboId] = paap.quevalidation.regularExpression.validationMessage || 'Value does not match the configured regular expression. Please provide other value.';
                            return true;
                        } else {
                            return false;
                        }
                    }
                    if (paap.quevalidation.validationSet) {
                        var setTitle = _.isObject(paap.quevalidation.validationSet.existingSet) ? paap.quevalidation.validationSet.existingSet._id : paap.quevalidation.validationSet.existingSet;

                        var setobj = _.find(newobject.setsarray, function (setval) {
                            return setTitle == setval._id;
                        });

                        var data = _.filter(setobj.values, function (value) {
                            return value.isActive === true;
                        });

                        if ((_.findIndex(data, function (o) {
                            return o.value == value.toString();
                        }) != -1)) {
                            return false;
                        } else {
                            failreason[x || paap.comboId] = paap.quevalidation.validationSet.existingSet.validationMessage || 'Value does not match the configured set. Please provide other value.';
                            return true
                        }
                    }
                    if (paap.quevalidation.condition && paap.quevalidation.condition.conditionTitle) {
                        if (!valid.validateCondition(paap.quevalidation, value, paap.queformat)) {
                            failreason[x || paap.comboId] = paap.quevalidation.condition.validationMessage || 'Value does not match. Please provide valid value.';
                            return true;
                        } else {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
        }
    } else {
        failreason[x || paap.comboId] = "Please provide valid value."
        return true
    }
};

function validateDate(paap, value, stype, newobject) {
    if (value) {
        if (paap.comboId.indexOf('-') != -1) {
            var x = paap.comboId.substring(0, paap.comboId.indexOf('-'));
        }
        // Check whether date is number or not if date is number then consider invalid date
        if (newobject && newobject.mappingObject && newobject.mappingObject[0].mappingTool != 'csv') {

            if (isNumber(value) || !(new Date(value) instanceof Date) || _.isNaN(Date.parse(value))) {
                failreason[x || paap.comboId] = "Please provide valid date format.";
                return true;
            }

            if (paap.quevalidation && paap.quevalidation.condition) {
                if (!valid.performedDateValidation(paap.quevalidation, value)) {

                    failreason[x || paap.comboId] = _.isUndefined(paap.quevalidation.condition.validationMessage) ? 'Please provide valid Date for import.' : paap.quevalidation.condition.validationMessage;
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            if (getDateFormat(paap.queformat.format.metadata.datetime, value, newobject)) {
                var timestamp = Date.parse(isNumber(value) ? '' : value);
                if (_.isNaN(timestamp)) {
                    var datearray = value.split('/');
                    var onlyDate = datearray[0];
                    var month = datearray[1];
                    var year = datearray[2];
                    value = new Date(year, month, onlyDate);
                }

                if (paap.quevalidation && paap.quevalidation.condition) {
                    if (!valid.performedDateValidation(paap.quevalidation, value)) {

                        failreason[x || paap.comboId] = _.isUndefined(paap.quevalidation.condition.validationMessage) ? 'Please provide valid Date for import.' : paap.quevalidation.condition.validationMessage;
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {

                failreason[x || paap.comboId] = "Please provide valid date format.";
                return true;
            }
        }
    } else {
        failreason[x || paap.comboId] = "Please provide valid value."
        return true
    }
};

function getDateFormat(formatId, date, newobject) {

    var allowItems = _.reduce(newobject.types[0].formats, function (o, obj) {
        if (obj.title === 'Date') {
            o.push(obj.metadata[0].allowItems)
        }
        return o;
    }, [])[0];
    var dateFormt = _.reduce(allowItems, function (o, item) {
        if (item._id === formatId) {
            o.push(item);
        }
        return o;
    }, [])[0];
    var checkedDate = '';
    if (formatId == "") {
        dateFormt = {};
        dateFormt['title'] = 'MM/DD/YYYY';
    }

    if (dateFormt) {
        if (dateFormt['title'] === 'DD/MM/YYYY') {
            checkedDate = moment(date, 'DD/MM/YYYY').format('DD/MM/YYYY');
            if (checkedDate == date) {
                return true
            }
        } else if (dateFormt['title'] === 'MM/DD/YYYY') {
            checkedDate = moment(date, 'MM/DD/YYYY').format('MM/DD/YYYY');
            if (checkedDate == date) {
                return true
            }
        } else if (dateFormt['title'] === 'YYYY/MM/DD') {
            checkedDate = moment(date, 'YYYY/MM/DD').format('YYYY/MM/DD');
            if (checkedDate == date) {
                return true
            }
        } else if (dateFormt['title'] === 'MM/DD/YY') {
            checkedDate = moment(date, 'MM/DD/YY').format('MM/DD/YY');
            if (checkedDate == date) {
                return true
            }
        } else if (dateFormt['title'] === 'DD/MMM/YY') {
            checkedDate = moment(date, 'DD/MMM/YY').format('DD/MMM/YY');
            if (checkedDate.toLowerCase() == date.toLowerCase()) {
                return true
            }
        } else if (dateFormt['title'] === 'MONTH/DD/YYYY') {
            checkedDate = moment(date, 'MMMM/DD/YYYY').format('MMMM/DD/YYYY');
            if (checkedDate.toLowerCase() == date.toLowerCase()) {
                return true
            }
        } else if (dateFormt['title'] === 'DD/MMM/YYYY') {
            checkedDate = moment(date, 'DD/MMM/YYYY').format('DD/MMM/YYYY');
            if (checkedDate.toLowerCase() == date.toLowerCase()) {
                return true
            }
        }
        return false;
    } else {
        return true;
    }
}

function validateTime(paap, time, stype, newobject) {
    if (time) {
        if (paap.comboId.indexOf('-') != -1) {
            var x = paap.comboId.substring(0, paap.comboId.indexOf('-'));
        }
        if (time.indexOf(':') !== -1 && time.split(':').length <= 3) {
            var value = time.split(':');
            if (!isNumber(value[0]) || parseInt(value[0]) > 24) {
                failreason[x || paap.comboId] = "Please provide valid time.";
                return true;
            }
            if (!isNumber(value[1]) || parseInt(value[1]) > 60) {
                failreason[x || paap.comboId] = "Please provide valid time.";
                return true;
            }
            // need to write code for time validation


            if (paap.quevalidation && paap.quevalidation.condition) {
                if (performedTimeValidation(paap.quevalidation.condition, time)) {
                    return false;
                } else {
                    failreason[x || paap.comboId] = _.isUndefined(paap.quevalidation.condition.validationMessage) ? 'Please provide valid Date for import.' : paap.quevalidation.condition.validationMessage;
                    return true;
                }
            }

        } else {
            failreason[x || paap.comboId] = "Please provide valid time.";
            return true;
        }
    } else {
        failreason[x || paap.comboId] = "Please provide valid value."
        return true
    }
    return false;
};

function getTimeFormat(formatId) {
    var allowItems = _.reduce(allTypes[0].formats, function (o, obj) {

        if (obj.title === 'Time') {
            o.push(obj.metadata[0].allowItems)
        }
        return o;
    }, [])[0];
    var timeFormt = _.reduce(allowItems, function (o, item) {
        if (item._id === formatId) {
            o.push(item);
        }
        return o;
    }, [])[0];

    if (timeFormt) {
        return timeFormt['title'] === 'HH:MM' || timeFormt['title'] === 'HH:MM:SS';
    } else {
        return false;
    }
}

function insertFailedRecords(failed) {
    var deferred = q.defer();
    db.collection('pluginTempRecords').insertMany(failed, function (err, inserted) {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve(inserted);
        }
    });
    return deferred.promise;
}

function validateAttribute(processSchema, val, inx) {
    logger.log("--------step 13 ------validateAttribute---", 'debug', 'receive.js');

    // attrPass1 = [];
    var optchk = {
        'entries': {}
    };

    _.forEach(processSchema.keyvalue, function (keyvalue) {
        if (keyvalue.hasOwnProperty('parentId')) {
            if (!_.isUndefined(val.entries[keyvalue['s#'] + '-' + keyvalue.parentId])) {
                optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId] = {}
                optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId] = val.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId];
            }
        } else {
            optchk.entries[keyvalue['s#']] = {
                'attributes': val.entries[keyvalue['s#']].attributes
            }
        }
    });
    if (Object.getOwnPropertyNames(optchk.entries).length == 0) {
        attrPass1.push(val);

    } else {

        var fltr = _.filter(attrPass1, optchk);

        if (fltr.length > 0) {
            _.forEach(processSchema.keyvalue, function (keyvalue) {
                val.failreason[keyvalue['s#']] = 'Either attribute or entity value already exist. Please provide other value.';
            });

        } else {
            attrPass1.push(val);
        }
    }
}

function validateAttributeDb(processSchema, oldSchemas, val, inx) {
    logger.log("--------step 14 ------validateAttributeDb---", 'debug', 'receive.js');

    // attrPass2 = [];
    var optchk = {
        'entries': {}
    };

    _.forEach(processSchema.keyvalue, function (keyvalue) {
        if (keyvalue.hasOwnProperty('parentId')) {
            if (keyvalue.type.format.title === 'Date') {
                optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId] = {};
                optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId] = val.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId];
            } else {
                if (!_.isUndefined(val.entries[keyvalue['s#'] + '-' + keyvalue.parentId])) {
                    optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId] = {};
                    optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId] = val.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId];
                }
            }
        } else {
            optchk.entries[keyvalue['s#']] = {
                'attributes': val.entries[keyvalue['s#']].attributes
            }
        }
    });

    if (Object.getOwnPropertyNames(optchk.entries).length == 0) {
        attrPass2.push(val);
    } else {
        var fltr = _.filter(oldSchemas, optchk);
        if (fltr.length > 0) {
            _.forEach(processSchema.keyvalue, function (keyvalue) {
                val.failreason[keyvalue['s#']] = 'Either attribute or entity value already exist. Please provide other value.';
            });
        } else {
            attrPass2.push(val);
        }
    }
}

function findUniqRecords(entity, configdetails, sapp, mapping) {
    logger.log("--------step 7------findUniqRecords---", 'debug', 'receive.js');

    var deferred = q.defer();
    if (entity == 'zoho' || entity == 'csv' || entity == 'infusionsoft') {
        deferred.resolve('');
    } else {
        var mappingtool = require('../pluginCommon/' + entity);
        mappingtool.getRecordIds(configdetails, sapp, mapping).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
    }

    return deferred.promise;
}

function isNumber(value) {
    return (/^\d+$/.test(value) || (/^\s*[-+]?(\d*\.?\d+|\d+\.)(e[-+]?[0-9]+)?\s*$/i.test(value)))
}


function performedTimeValidation(scope1, value, entry, sequenceId) {
    var scope = {};
    scope = _.cloneDeep(scope1);
    var h;
    var m;
    var h1;
    var m1;
    var one;

    if (value.indexOf(':') !== -1) {
        var timeValue = value.split(':');
        if ((!isNumber(timeValue[0]) || parseInt(timeValue[0]) > 24) && (!isNumber(timeValue[1]) || parseInt(timeValue[1]) > 60)) {
            value = new Date();
        } else {
            h = isNumber(timeValue[0]) ? parseInt(timeValue[0]) : 0;
            m = isNumber(timeValue[1]) ? parseInt(timeValue[1]) : 0;
            value = moment.tz({ year: 1970, month: 0, day: 1, hour: h, minute: m }, config.timeZonesP[config.currentTimeZoneP]).toISOString();
        }
    } else {
        value = new Date();
    }

    if (scope.value) {
        if (scope.value.indexOf(':') !== -1) {
            var timeValue1 = scope.value.split(':');
            if ((!isNumber(timeValue1[0]) || parseInt(timeValue1[0]) > 24) && (!isNumber(timeValue1[1]) || parseInt(timeValue1[1]) > 60)) {
                scope.value = new Date();
            } else {
                h1 = isNumber(timeValue1[0]) ? parseInt(timeValue1[0]) : 0, m, 0, 0;
                m1 = isNumber(timeValue1[1]) ? parseInt(timeValue1[1]) : 0;
                scope.value = moment.tz({ year: 1970, month: 0, day: 1, hour: h1, minute: m1 }, config.timeZonesP[config.currentTimeZoneP]).toISOString();
            }
        } else {
            scope.value = new Date();
        }
    } else if (scope.currenttime) {
        var currunttime = moment().format("HH:mm:ss");
        var array = currunttime.split(':')
        scope.value = moment.tz({ year: 1970, month: 0, day: 1, hour: array[0], minute: array[1] }, config.timeZonesP[config.currentTimeZoneP]).toISOString()
    }

    if (scope.minimum) {
        if (scope.minimum.indexOf(':') !== -1) {
            one = moment.tz(scope.minimum, config.timeZonesP[config.currentTimeZoneP]);
            h1 = one.hours();

            m = one.minute();

            scope.minimum = moment.tz({ year: 1970, month: 0, day: 1, hour: h1, minute: m }, config.timeZonesP[config.currentTimeZoneP]).toISOString();

        } else {
            scope.minimum = new Date();
        }
    }

    if (scope.maximum) {
        if (scope.maximum.indexOf(':') !== -1) {
            one = moment.tz(scope.maximum, config.timeZonesP[config.currentTimeZoneP]);
            h = one.hours();
            m = one.minute();
            scope.maximum = moment.tz({ year: 1970, month: 0, day: 1, hour: h, minute: m }, config.timeZonesP[config.currentTimeZoneP]).toISOString();
        } else {
            scope.maximum = new Date();
        }
    }
    switch (scope.conditionTitle) {
        case '>':
            return value > scope.value;
        case '<':
            return value < scope.value;
        case '>=':
            return value >= scope.value;
        case '<=':
            return value <= scope.value;
        case '=':
            return value = scope.value;
        case '!=':
            return value != scope.value;
        case '>= && <=':
            if (value >= scope.minimum && value <= scope.maximum) {
                return true;
            } else {
                return false;
            }
        case '!(>= && <=)':
            if (!(value >= scope.minimum && value <= scope.maximum)) {
                return true;
            } else {
                return false;
            }
        default:
            return false;
    }
};

function linkEntityValidation(value, paap) {
    logger.log("--------step 12 ------linkEntityValidation---", 'debug', 'receive.js');
    if (value || value == '0') {

        var values = _.filter(entityValues[paap.linkToEntity.entityId][0], function (entityValue) {
            return entityValue.entityRecord[paap.linkToEntity.entityKeyAttributeId] == value;
        });
        if (values.length) {
            return false;
        } else {
            failreason[paap.comboId] = "Value does not match. Please provide other value.";
            return true
        }
    } else {
        failreason[paap.comboId] = "Please provide valid value.";
    }

}

function validateAttributeEntity(processSchema, val, inx) {
    var optchk = {
        'entityRecord': {}
    };
    _.forEach(processSchema.questions, function (keyvalue) {
        if (keyvalue.isKeyValue) {
            optchk.entityRecord[keyvalue['s#']] = val.entityRecord[keyvalue['s#']]

        }
    })
    if (Object.getOwnPropertyNames(optchk.entityRecord).length == 0) {
        attrPass1.push(val);
    } else {
        var fltr = _.filter(failedRecords, optchk);
        if (fltr.length > 1) {
            if (!_.find(attrPass1, optchk)) {
                attrPass1.push(val);
            } else {
                _.forEach(processSchema.questions, function (keyvalue) {
                    if (keyvalue.isKeyValue) {
                        val.failreason[keyvalue['s#']] = 'Key attribute value already exists. Please provide other value.';
                    }
                })
            }
        } else {
            attrPass1.push(val);
        }
    }
}

function validateAttributeDbEntity(processSchema, oldSchemas, val, inx) {
    var optchk = {
        'entityRecord': {}
    };
    _.forEach(processSchema.questions, function (keyvalue) {
        if (keyvalue.isKeyValue) {
            optchk.entityRecord[keyvalue['s#']] = val.entityRecord[keyvalue['s#']];
        }
    });
    if (Object.getOwnPropertyNames(optchk.entityRecord).length == 0) {
        attrPass2.push(val);
    } else {
        var keyArr = Object.getOwnPropertyNames(optchk.entityRecord)
        var fltr = _.filter(oldSchemas, function (enSchema) {
            return keyArr.some(function (val) {
                return optchk.entityRecord[val] == enSchema.entityRecord[val]
            })
        });
        if (fltr.length) {
            _.forEach(processSchema.questions, function (keyvalue) {
                if (keyvalue.isKeyValue) {
                    val.failreason[keyvalue['s#']] = 'Key attribute value already exists. Please provide other value.';
                }
            })
        } else {
            attrPass2.push(val);
        }
    }
}