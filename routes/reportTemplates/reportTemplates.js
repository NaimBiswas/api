'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var config = require('../../config/config');
var request = require('request');
var logger = require('../../logger');

var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'reportTemplates',
    collection: 'reportTemplates',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    GET: {
      ONE: {
        after: doAction
      }
    },
  }
});

function doAction(req, res, next) {
 
  logger.log("---------------------------------------------------------------------------------");
  logger.log("bookmark opened");
  logger.log("---------------------------------------------------------------------------------");
  req.data.action = "Viewed";
  var dataObj = {
    user: req.user,
    data: req.data,
    ip: req.ip,
    type: "Custom Reports",
  }
  var options = {
    url: config.reportUrl + "/bilogs",
    method: "GET",
    body: dataObj,
    json: true
  }
  request(options, function (error, response, body) {
    if (error) {
      console.log(error);
    } else {
      console.log("dump data");
    }
  });
  res.status(200).json(req.data);
}

module.exports = route.router;

/**
 * @swagger
 * definition:
 *   Version:
 *     properties:
 *       version:
 *         type: integer
 *       record:
 *         type: integer
 */

/**
 * @swagger
 * definition:
 *    ReportTemplates:
 *      properties:
 *        id: 
 *          type: integer
 *        templateId:
 *          type: integer
 *        bookmarkDescription:
 *          type: string
 *        bookmarkName:
 *          type: string
 *        isActive:
 *          type: boolean
 *        isRemoved:
 *          type: boolean
 *        createdDate: 
 *          type: dateTime
 *        createdByName:
 *          type: string
 *        modifiedDate:
 *          type: dateTime
 *        modifiedByName: 
 *          type: string
 *        modifiedBy:
 *          type: integer
 *        versions:  
 *          type: array
 *          items:
 *             type: object
 *             allOf:
 *              - $ref: '#/definitions/Version'
 *        version:
 *          type: integer
 *        isMajorVersion: 
 *          type: boolean
 */

/**
 * @swagger
 * /reportTemplates:
 *  get:
 *    tags: [Reports]
 *    description: Shows details of custom reports
 *    produces: 
 *      - application/json
 *    parameters:
 *        - name: pagesize
 *          description: Page Size
 *          in: query
 *          required: false
 *          type: string
 *        - name: select
 *          description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,title,type.title,modifiedDate,modifiedBy,version,versions,isActive])
 *          in: query
 *          required: false
 *          type: string
 *        - name: sort
 *          description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
 *          in: query
 *          required: false
 *          type: string
 *    responses:
 *      200:
 *        description: An array of custom reports     
 *        schema:
 *          $ref: '#/definitions/ReportTemplates'        
 */