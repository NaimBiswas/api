'use strict'
// @name : resources-schema
// @author : Jinal Somaiya
//
// @version : 1.0
var types = require('../../lib/validator/types');

var rString = types.rString;
var bool = types.bool;
var string = types.string;
var schema = {
    bookmarkName: rString.label('title'),
    bookmarkDescription: rString.label('Description'),
    isActive: bool.label('Active?'),
    isRemoved : bool.label('Removed?'),
    timeZone: string.label('Time Zone')
};

module.exports = schema;
