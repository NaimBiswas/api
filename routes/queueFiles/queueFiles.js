'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var db = require('../../lib/resources').db;
//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'queueFiles',
    collection: 'queueFiles',
    schema: schema,
    softSchema: softSchema
  }
});

module.exports = route.router;
