'use strict'
/**
 * @name modules-schema
 * @author Amit Dave <dave.a@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var rBool = types.rBool;
var rNumber = types.rNumber;
var string = types.string;
var required = types.required;
var any = types.any;
var bool = types.bool;
var smallString=types.rSmallString;


// var schema = object({
//   joinName : any,
//   join : any,
//   isActive: bool.label('Active?')
// });

var schema = object({
  viewName : smallString.label("Join name"),
  viewType : smallString.label("Join Type"),
  isActive: bool.label('Active?'),
  moduleDetails:any,
  projectionFields:any,
  appDetails:any,
  joinFields:any,
  miscelleneousInfo:any,
  timeZone: string.label('Time Zone')
 });



module.exports = schema;
