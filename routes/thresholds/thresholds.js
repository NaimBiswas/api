'use strict';
var schema = require('./schema');
var softSchema = require('./softSchema');
var db = require('../../lib/resources').db;
var moment = require('moment-timezone');
var _ = require('lodash');
var log = require('../../logger');
var initialDate;

// This call will automatically build following routes GET (pagged) GET/:_id
// PUT/:_id Chandni : Code start for Swagger

/**
 * @swagger
 * definition:
 *   Reportconfiguration:
 *     properties:
 *       formate:
 *         type: string
 *       exatmatchrecord:
 *         type: string
 *       emailaddress:
 *         type: string
 *       messageinstruction:
 *         type: string
 */

/**
 * @swagger
 * definition:
 *   LeftOperand:
 *     properties:
 *       title:
 *         type: string
 *       type:
 *         $ref: '#/definitions/Type'
 *       validation:
 *         $ref: '#/definitions/Validation'
 *       isKeyValue:
 *         type: boolean
 *       isAppearance:
 *         type: boolean
 *       s#:
 *         type: string
 *       comboId:
 *         type: string
 *       parentId:
 *         type: string
 */

/**
 * @swagger
 * definition:
 *   Condition1:
 *     properties:
 *       rightOperand:
 *         type: integer
 *       comparison:
 *         type: string
 *       leftOperand:
 *         $ref: '#/definitions/LeftOperand'
 */

/**
 * @swagger
 * definition:
 *   SmartAlert:
 *     properties:
 *       _id:
 *         type: string
 *       title:
 *         type: string
 *       module:
 *         $ref: '#/definitions/DataModel'
 *       qcform:
 *         $ref: '#/definitions/DataModelWithoutTitle'
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       createdBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       modifiedDate:
 *         type: dateTime
 *       startDate:
 *         type: dateTime
 *       endDate:
 *         type: dateTime
 *       thresholdType:
 *         type: string
 *       description:
 *         type: string
 *       schemaTitles:
 *         type: string
 *       isActive:
 *         type: boolean
 *       schema:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Schema'
 *       frequency:
 *         $ref: '#/definitions/Frequency'
 *       reportconfiguration:
 *         $ref: '#/definitions/Reportconfiguration'
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       isMajorVersion:
 *         type: boolean
 *       version:
 *         type: integer
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       conditions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Condition1'
 */

/**
 * @swagger
 * /thresholds:
 *   get:
 *     tags: [Smart Alert]
 *     description: Returns all smart alerts
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,title,schemaTitles,modifiedDate,modifiedBy,isActive,schema])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of smart alerts
 *         schema:
 *           $ref: '#/definitions/SmartAlert'

 */

/**
 * @swagger
 * /thresholds/{id}:
 *   get:
 *     tags: [Smart Alert]
 *     description: Returns a smart alert
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Smart Alert's id
 *         in: path
 *         required: true
 *         type: integer
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An object of smart alert
 *         schema:
 *           $ref: '#/definitions/SmartAlert'

 */

//Chandni : Code end for Swagger POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'thresholds',
    collection: 'thresholds',
    schema: schema,
    softSchema: softSchema
  },
  types: {

    /**
     * @swagger
     * /thresholds:
     *   post:
     *     tags: [Smart Alert]
     *     description: Creates a new Smart Alert
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: smartAlert
     *         description: Smart Alert object
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/SmartAlert'
     *     responses:
     *       201:
     *         description: Successfully created
     */
    POST: {
      ONE: {
        after: afterPost
      }
    },
    /**
     * @swagger
     * /thresholds/{id}:
     *   patch:
     *     tags: [Smart Alert]
     *     description: Updates a Smart Alert
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: Smart Alert's id
     *         in: path
     *         required: true
     *         type: integer
     *       - name: smartAlert
     *         description: Smart Alert object
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/SmartAlert'
     *     responses:
     *       200:
     *         description: Successfully updated
     */
    PATCH: {
      ONE: {
        after: afterPatch
      }
    }
  }
});
var debug = route.debug;

function afterPatch(req, res, next) {
  // req.data contains the posted / patched remove existing data from calendar for
  // req.data and add new requested data
  db
    .calendarThresholds
    .removeManyAsync({
      thresholdId: req.data._id
    })
    .then(function (data) {
      var calandarEntries = generateEntries(req.data);
      (req.status ?
        res.status(req.status) :
        res).json(req.data);
      storeEntries(calandarEntries);
    });

}

function storeEntries(entries) {
  db
    .calendarThresholds
    .insertManyAsync(entries)
    .then(function (data) {
      debug('Calander entries generated and stored sucessfully!');
    })
    .catch(function (err) {
      debug('Failed to store generated calander entries!');
      debug(err);
    });
}

function generateEntries(threshold) {
  var calandarEntries = [];
  var startDate = new Date(threshold.startDate);
  var endDate = new Date(threshold.endDate);
  var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
  var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
  var date;
  if (threshold.frequency.type === 'Hourly'){
    startDate = moment(startDate);
    endDate = moment(endDate);
    var startTime = _.cloneDeep(moment(threshold.frequency.hourly.repeats.startTime));
    var endTime = _.cloneDeep(moment(threshold.frequency.hourly.repeats.endTime));
    var currentDate = _.cloneDeep(moment(startDate));
    while (endDate.diff(currentDate, 'days') >= 0) {
      var currentTime = _.cloneDeep(moment(startTime));
      var calendarDate = _.cloneDeep(currentDate);
      while (endTime.diff(currentTime, 'hour') > 0) {
        calendarDate.hour(currentTime.hour()).minute(0).second(0);
        currentTime.add(1, 'hour');
        calendarDate.add(1, 'hour');
        calendardetails(new Date(calendarDate));
      }
      currentDate.add(1, 'day');
    }
  } else if (threshold.frequency.type === 'Daily') {
    var days = threshold.frequency.daily.repeats.every;
    var filteredDays = (parseInt(diffDays) / parseInt(days)) + 1;
    var dateDaily = new Date(threshold.startDate);
    for (var day = 0; day < parseInt(filteredDays); day++) {
      if (day == 0 && threshold.frequency.daily.repeats.every == 1) {
        calendardetails(dateDaily);
      } else {
        var calendarDateDaily = dateDaily.setDate(dateDaily.getDate() + parseInt(days));
        calendardetails(calendarDateDaily);
      }
    }
  } else if (threshold.frequency.type === 'Weekly') {
    var skipDays = (parseInt(threshold.frequency.weekly.repeats.every) - 1) * 7;
    var currentDate = new Date(startDate);
    var startDateforskip = startDate;
    var skipDate = new Date(startDateforskip.setDate(startDateforskip.getDate() + parseInt(skipDays)));
    var addincalendarDate;
    for (var weekly = 0; weekly < diffDays + 1; weekly++) {
      var weekday = currentDate.getDay();
      var a = new Date(skipDate);
      addincalendarDate = new Date(a.setDate(a.getDate() + parseInt(7)));
 
      var condFlag;
      if (threshold.frequency.weekly.repeats.every == '1') {
        //akashdeep.s - QC3-6193 - checking for equal to condition as well
        condFlag = eval(currentDate >= skipDate && currentDate <= addincalendarDate);
      } else {
        //client issue date: 20180104
        condFlag = eval(currentDate >= skipDate && currentDate < addincalendarDate);
      }

      if(condFlag) {
        for (var i = 0; i < threshold.frequency.weekly.repeats.days.length; i++) {
          if (threshold.frequency.weekly.repeats.days[i] == "Mon") {
            if (weekday == 1) {
              date = new Date(currentDate);
              calendardetails(date);
            }
          }
          if (threshold.frequency.weekly.repeats.days[i] == "Tue") {
            if (weekday == 2) {
              date = new Date(currentDate);
              calendardetails(date);
            }
          }
          if (threshold.frequency.weekly.repeats.days[i] == "Wed") {
            if (weekday == 3) {
              date = new Date(currentDate);
              calendardetails(date);
            }
          }
          if (threshold.frequency.weekly.repeats.days[i] == "Thu") {
            if (weekday == 4) {
              date = new Date(currentDate);
              calendardetails(date);
            }
          }
          if (threshold.frequency.weekly.repeats.days[i] == "Fri") {
            if (weekday == 5) {
              date = new Date(currentDate);
              calendardetails(date);
            }
          }
          if (threshold.frequency.weekly.repeats.days[i] == "Sat") {
            if (weekday == 6) {
              date = new Date(currentDate);
              calendardetails(date);
            }
          }
          if (threshold.frequency.weekly.repeats.days[i] == "Sun") {
            if (weekday == 0) {
              date = new Date(currentDate);
              calendardetails(date);
            }
          }
        }
      }

      var date1 = new Date(addincalendarDate).setHours(0, 0, 0, 0);
      var currentdate = new Date(currentDate).setHours(0, 0, 0, 0);
      if (date1 === currentdate) {
        skipDate = new Date(skipDate.setDate(skipDate.getDate() + parseInt(skipDays) + parseInt(7)));
        currentDate = new Date(currentDate.setDate(currentDate.getDate() + 1));
        diffDays++;
        continue;
      }
      currentDate = new Date(currentDate.setDate(currentDate.getDate() + 1));

    }

  } else if (threshold.frequency.type === 'Monthly') {
    var totalmonths = (endDate.getFullYear() - startDate.getFullYear()) * 12;
    totalmonths -= startDate.getMonth();
    totalmonths += endDate.getMonth() + 1;
    var startYear = startDate.getFullYear();

    if (threshold.frequency.subType === 'EveryMonthly') {
      var repeattotalmonth = parseInt(totalmonths) / parseInt(threshold.frequency.monthly.repeats.dayOfMonth.month);
      var skipMonth = (parseInt(threshold.frequency.monthly.repeats.dayOfMonth.month) - 1);
      var firstmonth = startDate.getMonth() + parseInt(skipMonth);
      for (var ct = 1; ct <= repeattotalmonth; ct++) {
        var totalMonthDays = new Date(startYear, firstmonth, 0).getDate();
        for (var d = 1; d <= totalMonthDays; d++) {
          if (d === parseInt(threshold.frequency.monthly.repeats.dayOfMonth.day)) {
            date = new Date(startYear, firstmonth, d);

            if (date > startDate && date < endDate) {
              calendardetails(date);
            }
          }
        }

        firstmonth = firstmonth + (skipMonth + 1);
      }
    } else if (threshold.frequency.subType === 'WeeklyMonthly') {

      if (threshold.frequency.monthly.repeats.dayOfWeek.every === 'first' || threshold.frequency.monthly.repeats.dayOfWeek.every === 'second' || threshold.frequency.monthly.repeats.dayOfWeek.every === 'third' || threshold.frequency.monthly.repeats.dayOfWeek.every === 'fourth') {

        setWeeklyMonthly();
      }

    }

  }

  function setWeeklyMonthly() {

    // var startDate = new Date(threshold.startDate); var endDate = new
    // Date(threshold.endDate);
    var totalmonths = (endDate.getFullYear() - startDate.getFullYear()) * 12;
    totalmonths -= startDate.getMonth();
    totalmonths += endDate.getMonth() + 1;
    var startYear = startDate.getFullYear();
    var repeatmonthWeekly = parseInt(totalmonths) / parseInt(threshold.frequency.monthly.repeats.dayOfWeek.month);
    var skipWeeklyMonth = (parseInt(threshold.frequency.monthly.repeats.dayOfWeek.month) - 1);
    var firstmonthWeekly = (startDate.getMonth()) + parseInt(skipWeeklyMonth);
    for (var ct = 1; ct <= repeatmonthWeekly; ct++) {
      var firstDate = new Date();
      var lastDate = new Date();
      if (threshold.frequency.monthly.repeats.dayOfWeek.every === 'first') {
        firstDate = new Date(startYear, firstmonthWeekly, 1);
        lastDate = new Date(startYear, firstmonthWeekly, 7);
      } else if (threshold.frequency.monthly.repeats.dayOfWeek.every === 'second') {
        firstDate = new Date(startYear, firstmonthWeekly, 8);
        lastDate = new Date(startYear, firstmonthWeekly, 14);
      } else if (threshold.frequency.monthly.repeats.dayOfWeek.every === 'third') {
        firstDate = new Date(startYear, firstmonthWeekly, 15);
        lastDate = new Date(startYear, firstmonthWeekly, 21);
      } else if (threshold.frequency.monthly.repeats.dayOfWeek.every === 'fourth') {
        firstDate = new Date(startYear, firstmonthWeekly, 22);
        lastDate = new Date(startYear, firstmonthWeekly, 28);
      }
      for (var idate = firstDate; idate <= lastDate; idate.setDate(idate.getDate() + 1)) {
        var day = idate.getDay();
        if (threshold.frequency.monthly.repeats.dayOfWeek.day === 'monday' && day === 1 || threshold.frequency.monthly.repeats.dayOfWeek.day === 'tuesday' && day === 2 || threshold.frequency.monthly.repeats.dayOfWeek.day === 'wednesday' && day === 3 || threshold.frequency.monthly.repeats.dayOfWeek.day === 'thursday' && day === 4 || threshold.frequency.monthly.repeats.dayOfWeek.day === 'friday' && day === 5 || threshold.frequency.monthly.repeats.dayOfWeek.day === 'saturday' && day === 6 || threshold.frequency.monthly.repeats.dayOfWeek.day === 'sunday' && day === 0) {
          var dateMonthly = new Date(idate);
          calendardetails(dateMonthly);
        }

      }

      firstmonthWeekly = firstmonthWeekly + (skipWeeklyMonth + 1);

    }
  }

  function calendardetails(calendarDate) {
    var calendarobj = {
      schema: {
        _id: '',
        title: ''
      },
      module: {
        _id: '',
        title: ''
      }
    };
    calendarobj.title = threshold.title;
    calendarobj.schema = threshold.schema;
    calendarobj.module = threshold.module;
    calendarobj.thresholdType = threshold.thresholdType || 'App';
    calendarobj.matchingRecord = threshold.matchingtrcord;
    calendarobj.exatmatchrecord = threshold.reportconfiguration.exatmatchrecord;
    calendarobj.whichMatchingRecord = threshold.reportconfiguration.whichMatchingRecord || 'all';
    calendarobj.thresholdCondition = threshold.conditions;
    calendarobj.scheduledBy = threshold.scheduledBy || threshold.createdBy;
    calendarobj.type = threshold.frequency.type;
    calendarobj.date = new Date(calendarDate);
    calendarobj.initialDate = !_.isUndefined(initialDate) ?
      new Date(initialDate) :
      '';
    calendarobj.isActive = threshold.isActive;
    calendarobj.email = threshold.reportconfiguration.emailaddress;
    calendarobj.messageinstruction = threshold.reportconfiguration.messageinstruction;
    calendarobj.notified = false;
    calendarobj.thresholdId = threshold._id;
    var thresholdStartDate = new Date(threshold.startDate);
    calendarobj.startDate = new Date(thresholdStartDate.getFullYear(), thresholdStartDate.getMonth(), thresholdStartDate.getDate() + 1, 0, 0, 0);
    calendarDate = calendarobj.date;
    if (threshold.frequency.type == 'Hourly') {
      var thresholdEndDate = new Date(threshold.endDate);
      calendarobj.endDate = new Date(thresholdEndDate.getFullYear(), thresholdEndDate.getMonth(), thresholdEndDate.getDate() + 1, 0, 0, 0);
      calendarobj.startTime = threshold.frequency.hourly.repeats.startTime;
      calendarobj.endTime = threshold.frequency.hourly.repeats.endTime;
      initialDate = calendarDate.setDate(calendarDate.getDate());
    } else {
      initialDate = calendarDate.setDate(calendarDate.getDate() + 1);
    }
    calandarEntries.push(calendarobj);
    // log.log(initialDate);
  }
  return calandarEntries;
}

function afterPost(req, res, next) {
  // req.data contains the posted / patched threshold object with createdBy and
  // modifiedBy And all, orignal object can be accessed from req.items
  storeEntries(generateEntries(req.data));
  res
    .status(req.status)
    .json(req.data);
  // res.json(calandarEntries);
}

module.exports = route.router;