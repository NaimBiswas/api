'use strict';
/**
* @name thresholds-schema
* @author Jyotil Raval <jyotil.r@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var rBool = types.rBool;
var rDate = types.rDate;
var date = types.date;
var string = types.string;
var rSmallString = types.rSmallString;
var bigString = types.bigString;
var rBigString = types.rBigString;
var extraLargeString = types.extraLargeString;
var rExtraLargeString = types.rExtraLargeString;
var required = types.required;
var forbidden = types.forbidden;
var stringArray = types.stringArray;
var rNumber = types.rNumber;
var number = types.number;
var optionaldate = types.optionaldate;
var anyArray = types.anyArray;
var any = types.any;
var bool = types.bool;

var threshold = {
  title: rString.label('Threshold title'),
  description: any.label('Threshold Description').allow(''),
  module: object({
    _id: rId.label('module id'),
    title: rString.label('module title')
  }).required().label('module object'),

  thresholdType: rString.label('Smart Alert Type'),

  schemaTitles: extraLargeString.label('Threshold schemaTitles'),

  schema: array(object({
    _id: rId.label('Schema id'),
    title: rString.label('Schema title')
  })).required().label('schema object'),

  qcform :array(object({
    id : rId.label('attribute id')
  })).required().label('qcform object'),

  matchingtrcord: object({
    operator: rSmallString.label('matchingtrcord operator'),
    value: number.label('matchingtrcord value')
  }).required().label('matchingtrcord object'),

  frequency: object({
    type: rString.label('Frequency type'),
    subType: rString.label('SubType type'),
    hourly: object({
      repeats: object({
        startTime: rDate.label('Start Time'),
        endTime: rDate.label('End Time')
      })
    }).when('type', {is:'Hourly', then:required(), otherwise: forbidden()}).label('Hourly object'),
    daily: object({
      repeats: object({
        every: number.label('Daily Every')
        .when('subType', {is:'Every', then:required()})
        .when('subType', {is:'EveryBusinessDay', then:forbidden()}),
        everyBusinessDay: string.label('Every Business Day')
        .when('subType', {is:'EveryBusinessDay', then:required()})
        .when('subType', {is:'Every', then:forbidden()})
      })
    }).when('type', {is:'Daily', then:required(), otherwise: forbidden()}).label('Daily object'),
    weekly: object({
      repeats: object({
        every: rNumber.label('Weekly Every'),
        days: stringArray.label('Weekly days')
      })
    }).when('type', {is:'Weekly', then:required(), otherwise: forbidden()}).label('Weekly object'),
    monthly: object({
      repeats: object({
        dayOfMonth: object({
          day: rNumber.label('DayOfMonth Day'),
          month: rNumber.label('DayOfMonth Month')
        }).when('subType', {is:'EveryMonthly', then:required()}).label('DayOfMonth object'),
        dayOfWeek: object({
          every: rString.label('DayOfWeek Every'),
          day: rString.label('DayOfWeek Day'),
          month: rNumber.label('DayOfWeek Month')
        }).when('subType', {is:'WeeklyMonthly', then:required()}).label('DayOfWeek object')
      })
    }).when('type', {is:'Monthly', then:required(), otherwise: forbidden()}).label('Monthly object')
  }).required().label('Frequency object'),

  startDate: rDate.label('Start date'),

  endDate: optionaldate.label('End date')
  .when('occurenceState',  {is:'false',then: required()}),

  reportconfiguration: object({
    formate: rString.label('reportconfiguration formate'),
    exatmatchrecord: rSmallString.label('reportconfiguration exatmatchrecord'),
    whichMatchingRecord: rSmallString.label('How many Matching record needs to send?'),
    emailaddress: rExtraLargeString.label('reportconfiguration emailaddress'),
    messageinstruction: any.label('Message Instruction').allow('')
  }).required().label('reportconfiguration object'),

  email: string.label('Email').allow(''),

  conditions: any,
  sendEmailIfNoRecords: bool.label('Send Email Notification even if no matching records are found'),
  isActive :rBool.label('is Active?'),
  timeZone: string.label('Time Zone'),
  lastRunTime: any
};

module.exports = threshold;
