'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var settings = require('../../lib/settings');
var config = require('../../lib/config');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'allAuthToken',
    collection: 'allAuthToken',
    schema: schema,
    softSchema: softSchema
  },
  types:{
  }
});


module.exports = route.router;
