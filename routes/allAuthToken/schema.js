'use strict'
/**
 * @name generalSettings-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */

var types = require('../../lib/validator/types');

var rBigString = types.rBigString;
var extraLargeString = types.extraLargeString;
var bigString = types.bigString;
var date = types.date;
var object =types.object;
var string = type.string;
var rId = types.rId;
var rString = types.rString;

var schema = {
  token: extraLargeString.label('token'),
  user: object({
    _id: rId.label('user id'),
    username: rString.label('username'),
    firstname: rString.label('First Name'),
    lastname: rString.label('Last Name')
  }),
  expireDate: date.label('Expire Date'),
  usersUUID : bigString.label('User UUID'),
  lastUsed : date.label('Last Used Date'),
  tokenType : string.label('Token Type'), // this will be either User or App.
  timeZone: string.label('Time Zone')
};

module.exports = schema;
