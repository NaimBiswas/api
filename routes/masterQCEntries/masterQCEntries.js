
// by surjeet.b@productivet.com
'use strict'
var colors = require('colors');
var schema = require('./schema');
var softSchema = require('./softSchema');
var db = require('../../lib/db');
var crypto = require('crypto');
var _ = require('lodash');
var mailer = require('../../lib/mailer');
var settings = require('../../lib/settings');
var moment = require('moment');
var logger = require('../../logger');
var currentFileName = __filename;
//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST

var tempSchemaID = '';

function configure(name) {
  logger.log("function configure(name) :Called" , 'info', currentFileName);
  var route = require('../../lib/routeBuilder').build({
    entity: {
      name: name,
      collection: name,
      schema: schema,
      softSchema: softSchema
    },
    types:{
      POST:{
        ONE: {
          before : defaultPost,
          // after  : afterPost
        },
        ONESOFT: {
          before: beforePostForSaveDraft
        }
      },
      PATCH:{
        ONE: {
          before : defaultPatch,
          // after  : afterPatch
        },
        ONESOFT: {
          before: beforePatchForSaveDraft
        }
      }
    }
  });

  var debug = route.debug;

  function beforePostForSaveDraft(req, res, next) {
    logger.log("function beforePostForSaveDraft(req, res, next) :Called" , 'info', currentFileName);

    next();
  }

  function beforePatchForSaveDraft(req, res, next) {
    logger.log("function beforePatchForSaveDraft(req, res, next) :Called" , 'info', currentFileName);
    db.users.findOneAsync({username: req.user.username})
    .then(function (user) {
      if (req.body.verificationCode || !(_.isUndefined(req.body.verificationCode))) {
        if(user.verificationCode === crypto.createHash('sha1').update(req.body.verificationCode).digest('hex')) {
          next();
        }
        else {
          res.status(200).json({invalid: true});
        }
      }
      else {
        next();
      }
    })
  }

  function defaultPost(req, res, next) {
    logger.log("function defaultPost(req, res, next) :Called" , 'info', currentFileName);
    //console.log(req.body);
    if(req.body.schemaID){
      var attr = [];
      tempSchemaID = req.body.schemaID;
      var userData;
      db.users.findOneAsync({username: req.user.username})
      .then(function (user) {
        userData = user;
      })
      db.schema.findOneAsync({title: tempSchemaID})
      .then(function (entity) {

        _.forEach(entity.attributes, function (attribute) {
          //console.log(attribute);
          if (attribute.type.format.title === 'Date') {
            var tempDate = new Date(req.body.entries[attribute['s#']].attributes);
            req.body.entries[attribute['s#']].attributes = tempDate.getMonth() + 1 + '/' + tempDate.getDate() + '/' + tempDate.getFullYear();
          }
          if (attribute.type.format.title === 'Time') {
            var tempTime = new Date(req.body.entries[attribute['s#']].attributes);
            req.body.entries[attribute['s#']].attributes = tempTime.getHours() + ':' + tempTime.getMinutes() + ':' + tempTime.getSeconds();
          }
          attr.push({ 'title': attribute['title'], 'value': _.isArray( req.body.entries[attribute['s#']].attributes)?  (req.body.entries[attribute['s#']].attributes).toString():  req.body.entries[attribute['s#']].attributes});
        })
        if (req.body.entries.status.status === 0 && entity.isSendEmail) {
          delete req.body.schemaID;
          if (entity) {
            var entityModifiedDate = moment(moment.utc(new Date()).toDate()).format('MM/DD/YYYY HH:mm');
            + (entity.createdDate).getSeconds();
            _.merge(entity, {'settings': settings.default});
            _.merge(entity, {'tempSchemaID': tempSchemaID});
            _.merge(entity, {'objAttribute': attr})
            _.merge(entity, {'entityModifiedDate': entityModifiedDate});
            _.merge(entity, {'entityModifiedBy': userData.firstname + ' ' + userData.lastname + ' (' + userData.username + ')'})
            console.log('inside file: scheduler.js - function: defaultPost - status: sending mail');

            mailer.send('qcform-status-notify', entity, entity.emailIdForQCFormFail,function sendMailCallback(e,b) {
              if(e){
                logger.log("collection: qcform-status-notify :failed" , 'error', currentFileName);
                debug.err('Failed sending email - qcform-status-notify');
                debug.err(entity);
                debug.err(e);
                next();
              }else{
                logger.log("collection: qcform-status-notify :success" , 'info', currentFileName);
                debug('QC Form Status has sent to ' + entity.emailIdForQCFormFail);
                next();
              }
            });
          }
          else {
            debug('something went wrong!');
            next();
          }
        }else {
          next();
        }
      })
    } else {
      next();
    }
  }

  function defaultPatch(req, res, next) {
    logger.log("function defaultPatch(req, res, next) :Called" , 'info', currentFileName);

    if (req.body.schemaID) {
      var attr = [];
      tempSchemaID = req.body.schemaID;
      db.users.findOneAsync({username: req.user.username})
      .then(function (user) {
        if (req.body.verificationCode || !(_.isUndefined(req.body.verificationCode))) {
          if(user.verificationCode === crypto.createHash('sha1').update(req.body.verificationCode).digest('hex')) {
            db.schema.findOneAsync({title: tempSchemaID})
            .then(function (entity) {
              _.forEach(entity.attributes, function (attribute) {
                if (attribute.type.format.title === 'Date') {
                  var tempDate = new Date(req.body.entries[attribute['s#']].attributes);
                  req.body.entries[attribute['s#']].attributes = tempDate.getMonth() + 1 + '/' + tempDate.getDate() + '/' + tempDate.getFullYear();
                }
                if (attribute.type.format.title === 'Time') {
                  var tempTime = new Date(req.body.entries[attribute['s#']].attributes);
                  req.body.entries[attribute['s#']].attributes = tempTime.getHours() + ':' + tempTime.getMinutes() + ':' + tempTime.getSeconds();
                }
                attr.push({ 'title': attribute['title'], 'value': _.isArray( req.body.entries[attribute['s#']].attributes)?  (req.body.entries[attribute['s#']].attributes).toString():  req.body.entries[attribute['s#']].attributes});
              })
              if (req.body.entries.status.status === 0 && entity.isSendEmail) {
                delete req.body.schemaID;
                if (entity) {
                  //console.log(user);
                  var entityModifiedDate = moment(moment.utc(new Date()).toDate()).format('MM/DD/YYYY HH:mm');
                  + (entity.createdDate).getSeconds();
                  _.merge(entity, {'settings': settings.default});
                  _.merge(entity, {'tempSchemaID': tempSchemaID});
                  _.merge(entity, {'objAttribute': attr})
                  _.merge(entity, {'entityModifiedDate': entityModifiedDate});
                  _.merge(entity, {'entityModifiedBy': user.firstname + ' ' + user.lastname + ' (' + user.username + ')'})
                  console.log('inside file: scheduler.js - function: notify - status: sending mail');

                  mailer.send('qcform-status-notify', entity, entity.emailIdForQCFormFail,function sendMailCallback(e,b) {
                    if(e){
                      logger.log("collection: qcform-status-notify :failed" , 'error', currentFileName);
                      debug.err('Failed sending email - qcform-status-notify');
                      debug.err(entity);
                      debug.err(e);
                      next();
                    }else{
                      logger.log("collection: qcform-status-notify :success" , 'info', currentFileName);
                      debug('QC Form Status has sent to ' + entity.emailIdForQCFormFail);
                      next();
                    }
                  });
                }
                else {
                  debug('something went wrong!');
                  next();
                }
              }else {
                next();
              }
            })
            // next();
          }
          else {
            res.status(200).json({invalid: true});
          }
        }
        else {
          next();
        }
      })
    } else {
      next();
    }
  }

  return route.router;
}

module.exports = configure;
