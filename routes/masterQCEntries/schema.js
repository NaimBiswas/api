'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');
var joi = require('joi');

var object = types.object.bind(types);
var id = types.id;
var rId = types.rId;
var rString = types.rString;
var number = types.number;
var string = types.string;
var array = types.array;
var rDate = types.rDate;
var date = types.date;
var rBool = types.rBool;
var any = types.any;
var rNumber0 = types.rNumber0;
var rNumber = types.rNumber;

var schema = object({
  sequenceId: rString.label('s# Id'),
  createdDateInfo: rDate.label('Created Date'),
  createdByInfo: object({
    _id: rId.label('Create by user id'),
    username: rString.label('Created By user name')
  }).required().label('Create by'),
  levels: array(object({
    levelId: rNumber0.label('level Id'),
    siteId: rId.label('site Id'),
    title: rString.label(' site title')
  })).required().label('levels object'),
  entries: any,
  masterSchemaEntries: array(object({
    schemaId: rId.label('schema Id'),
    entryId: id.label('entry Id')
  })).label('master Schema Entries'),
  timeZone: string.label('Time Zone')


  // masterQCSettingsId: rId.label('Master QC Settings Id'),
  // draftStatus: number.label('Save status info on save as draft'),
  //
  // levelIds: joi.array().required().label('Sites ids'),
  // levels: array(object({
  //   levelId: number.label('Level id'),
  //   siteId: rId.label('Site id'),
  //   title: rString.label('Site title')
  // })).required().label('Level object'),
  // reviewStatus: rString.label('Review status'),
  // verificationCode: string.label('Verification Code'),
  // verificationDate: date.label('Verification Date'),
  // verifiedBy: string.label('Verified By').allow(''),
  // parentVersion: rNumber.label('parent schema version number'),
  // schemaID: any
});

module.exports = schema;
