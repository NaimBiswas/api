
var app = require('express')();
var dbConfig = require('../../lib/db/db');
var _ = require('lodash');
var debug = require('../../lib/logger').debug('routes:masterQCEntries');
var db = require('../../lib/resources').db;

db.masterqcsettings.find({}).toArray(function(err, result) {
  var masterQCEntries = [];
  // For
  _.forEach(result, function(schema, key) {
    masterQCEntries.push({
      collection: schema._id.toString('hex'),
      audit: true,
      index: {
        title: ''
      }
    });
  });

  debug('Registering dynamic masterQCEntries routes :: Lenght :: ' + masterQCEntries.length);
  //debug(entities);

  dbConfig.configure.apply(this, masterQCEntries);

  _.forEach(result, function(schema, key) {
    debug("Registering " + ('/' + schema._id.toString('hex')).underline.bold);
    app.use('/' + schema._id.toString('hex'), require('./masterQCEntries')(schema._id.toString('hex')));
  });

});

module.exports = app;
