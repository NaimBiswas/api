'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');
var joi = require('joi');

var object = types.object.bind(types);
var id = types.id;
var string = types.string;
var number = types.number;
var array = types.array;
var date = types.date;
var bool = types.bool;
var any = types.any;

var schema = object({
  sequencyId: string.label('s# Id'),
  createdDateInfo: date.label('Created Date'),
  createdByInfo: object({
    _id: id.label('Create by user id'),
    username: string.label('Created By user name')
  }).label('Create by'),
  levels: array(object({
    levelId: number.label('level Id'),
    siteId: id.label('site Id'),
    title: string.label(' site title')
  })).label('levels object'),
  entries: any,
  masterSchemaEntries: array(object({
    schemaId: id.label('schema Id'),
    entryId: id.label('entry Id')
  })).label('master Schema Entries'),
  timeZone: string.label('Time Zone')
  // entries: any,
  // masterQCSettingsId: id.label('Master QC Settings Id'),
  // draftStatus: number.label('Save status info on save as draft'),
  // createdDateInfo: date.label('Created Date'),
  // createdByInfo: object({
  //   _id: id.label('Created by user id'),
  //   username: string.label('Created By user name')
  // }).label('Created by'),
  // levelIds: joi.array().label('Sites ids'),
  // levels: array(object({
  //   levelId: number.label('Leval id'),
  //   siteId: id.label('Site id'),
  //   title: string.label('Site title')
  // })).label('Level object'),
  // reviewStatus: string.label('Review status'),
  // verificationCode: string.label('Verification Code'),
  // verificationDate: date.label('Verification Date'),
  // verifiedBy: string.label('Verified By').allow(''),
  // parentVersion: number.label('parent schema version number'),
  // schemaID: any
});

module.exports = schema;
