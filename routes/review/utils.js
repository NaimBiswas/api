var _ = require("lodash");
var express = require("express");
var moment = require("moment-timezone");
var decode = require("../../lib/oauth/model").getAccessToken;
var lzstring = require("lz-string");
var dbConfig = require("../../lib/db/db");
var loggedInUserDetails = require("../../lib/oauth/loggedInUserDetails");

var db = require("../../lib/db");
var batchEntryService = require("../../lib/batchEntryService/index");

var options = {};
options.clientTZ = "America/Chicago";

function projectedListForAdvancedSearch(schema, entry, options) {
  var projectedEntry = {
    _id: entry._id,
    App: schema.title
  };

  if (
    !entry.modifiedByName &&
    entry.modifiedBy &&
    options &&
    options.users &&
    options.users.length
  ) {
    var userDetails = _.find(options.users || [], function(user) {
      return user._id.toString() == entry.modifiedBy;
    });
    if (userDetails) {
      entry.modifiedByName = (userDetails.firstname || "")
        .concat(" ")
        .concat(userDetails.lastname || "")
        .concat(" (")
        .concat(userDetails.username || "N/A")
        .concat(")");
    }
  }

  if (entry["BatchEntry"]) {
    projectedEntry["BatchEntry"] = entry["BatchEntry"];
  }
  projectedEntry["Batch Entry ID"] = entry["BatchEntry"]
    ? entry["BatchEntry"].title
    : "N/A";
  return projectedEntry;
}

function fetchFormBatchEntry(entries, schema, options, cb) {
  try {
    var schemaId = schema._id.toString();
    if (options && options.masterApp && options.masterApp.length) {
      schemaId = options.masterApp[0];
    }

      batchEntryService.getBatchEntries(schemaId).then(function (result) {
        (result || []).forEach(function(batchEntry) {
          _.forIn(batchEntry.batchEntries || {}, function(value, key) {
            if (value && value.length) {
              value.forEach(function(item) {
                entries.forEach(function(ent) {
                  if (item.formId === ent._id.toString()) {
                    ent["BatchEntry"] = {
                      _id: batchEntry._id,
                      title: key
                    };
                    return;
                  }
                });
              });
            }
          });
        });
        cb();
      });
  } catch (e) {
    cb();
  }
}


function getTimeFormat(rawFormat){
  switch(rawFormat){
    case 'HH:MM':
    return "HH:mm";
    break;

    case 'HH:MM:SS':
    return "HH:mm:ss";
    break;

    case 'HH:MM AM/PM':
    return "hh:mm A";
    break;

    case 'HH:MM:SS AM/PM':
    return "hh:mm:ss A";
    break;

    default:
    return "HH:mm";
  }
}

function displayValueByFormat(c, value, clientTZ,options) {
  //var returnValue = "";
  var filterdDate = "";
  if (c && c.type && c.type.format && c.type.format.title === "Date") {



    if(!c.type.format.metadata.format && c.type.format.metadata.datetime){
      c.type.format.metadata.format = options.idWiseDateFormat[c.type.format.metadata.datetime];
    }

    filterdDate = (c.type.format.metadata.format || "MM/DD/YYYY")
      .toLocaleUpperCase()
      .replace("MONTH", "MMMM");

    if (value) {
      return moment
        .utc(new Date(value))
        .tz(clientTZ)
        .format(filterdDate);
    } else {
      return value;
    }
  } else if (value && c && c.type && c.type.format && c.type.format.title === "Time") {

    if(!c.type.format.metadata.format && c.type.format.metadata.datetime){
      c.type.format.metadata.format = options.idWiseDateFormat[c.type.format.metadata.datetime];
    }

    if(c.type.format && c.type.format.metadata && c.type.format.metadata.format ){
      return moment
      .utc(new Date(value))
      .tz(clientTZ)
      .format(getTimeFormat(c.type.format.metadata.format));
    }
    else{
      return moment
      .utc(new Date(value))
      .tz(clientTZ)
      .format("HH:mm a");
    }
  } else if (value && c && c.type && c.type.format && c.type.title === "File Attachment") {
    if(value && value.length && value.forEach){
      value.forEach(function (val) {
      val.type = c.type.title;
    });
  }
    return value;
  } else {
    return value === 0 ? "0" : value;
  }
}

function getStausString(app) {
  if (!(app.entries && app.entries.status)) {
    return "Draft";
  }
  var status = app.entries.status.status;
  var hasPendingWorkflowReview = false;
  if (app.workflowreview && app.workflowreview.length) {
    hasPendingWorkflowReview = _.some(app.workflowreview || [], function(
      review
    ) {
      return review.reviewstatus == 1;
    });
  }
  if (status == 3) {
    return "Void";
  } else if (status == 4) {
    return "Draft";
  } else if (hasPendingWorkflowReview && status == 1) {
    return "Review Pending - Passed";
  } else if (hasPendingWorkflowReview && status == 0) {
    return "Review Pending - Failed";
  } else if (status == 1) {
    return "Passed";
  }
  if (hasPendingWorkflowReview && status == 2) {
    //QC3-7516 by bhavin..
    return "Review Pending";
  } else {
    return "Failed";
  }
}

function getEntities(entities, entry, entryObject,options) {
  if (!entities || !entry) {
    return [];
  }

  return _.reduce(entities, function entityCallback(returnArray, ent) {
      var fakeEntity = {
        _id: ent._id,
        title: ent.title,
        "s#": ent["s#"],
        instructionForUser: ent.isInstructionForUser
          ? ent.instructionForUser
          : "",
        version: ent.version,
        questions: [],
        isSetLabel:ent.isSetLabel,
        entityLabelTitle:ent.entityLabelTitle
      };

      (ent.questions || []).forEach(function(_question) {
        var question = {
          "s#": _question["_s#"],
          title: _question.title,
          comboId: _question.comboId,
          parentId: _question.parentId,
          fullQuestion: _question,
          isKeyValue: _question.isKeyValue,
          isAppearance: _.isUndefined(_question.isAppearance)?true:_question.isAppearance,
          type: _question.type?_question.type.title?_question.type.title:"":"",
          key: "entries." + _question["s#"] + "-" + ent["s#"] + "." + ent["s#"],
        };


        if (!entry.entries[_question["s#"] + "-" + ent["s#"]]) {
          question.value = "";
          return;
        } else if (_question) {
          question.value =
            displayValueByFormat(
              _question,
              entry.entries[_question["s#"] + "-" + ent["s#"]][ent["s#"]],
              options.clientTZ,
              options
            ) || "";

          if (typeof question.value === "object" && question.value.length) {
            if (question.value && question.value.length && question.value[0].type =="File Attachment") {
              question.value = question.value;
              question.type = "File Attachment";
            } else {
              question.value = question.value.join(", ");
            }
          }
        } else {
          question.value = entry.entries[_question["s#"] + "-" + ent["s#"]]
            ? entry.entries[_question["s#"] + "-" + ent["s#"]][ent["s#"]]
            : "";
          if (typeof question.value === "object" && question.value.length) {
            if (question.value && question.value.length && question.value[0].type =="File Attachment") {
              question.value = question.value[0];
              question.type = "File Attachment";
            } else {
              question.value = question.value.join(", ");
            }
          }
        }

        if(entryObject && _question.isKeyValue){


          entryObject.shortInfos[_question.title] = {key : question.key};
          entryObject.shortInfos[_question.title].value = question.value;
          entryObject.shortInfos[_question.title].parent = ent.title;

          entryObject.shortInfos[_question.title].sequence = options.shortObjectSequance;
          // options.shortObjectSequance++;

          if (question.type) {
            entryObject.shortInfos[_question.title].type = question.type;
          }
        }

        fakeEntity.questions.push(question);
      });

      returnArray.push(fakeEntity);
      return returnArray;
    },
    []
  );
}

function getAttributes(attributes, entry, entryObject,options) {
  if (!attributes || !entry) {
    return [];
  }

  return _.reduce(attributes, function entityCallback(returnArray, attribute) {
      var fakeEntity = {
        _id: attribute._id,
        title: attribute.title,
        "s#": attribute["s#"],
        version: attribute.version,
        fullQuestion: attribute,
        parentId:attribute.parentId,
        type: attribute.type?attribute.type.title?attribute.type.title:"":"",
        key: "entries." + attribute["s#"] + ".attributes",
      };


      if (!entry.entries[attribute["s#"]]) {
        fakeEntity["value"] = "";
        return returnArray;
      } else if (!_.isUndefined(attribute)) {

        fakeEntity.value =
          displayValueByFormat(
            attribute,
            entry.entries[attribute["s#"]]["attributes"],
            options.clientTZ,
            options
          ) || "";
        if (typeof fakeEntity.value === "object" && fakeEntity.value.length) {
          if (fakeEntity.value && fakeEntity.value.length && fakeEntity.value[0].type =="File Attachment") {
            fakeEntity.value = fakeEntity.value[0];
            fakeEntity.type = "File Attachment";
          } else {
            fakeEntity.value = fakeEntity.value.join(", ");
          }
        }
      } else {
        fakeEntity.value = !_.isUndefined(entry.entries[attribute["s#"]])
          ? entry.entries[attribute["s#"]]["attributes"]
          : "";
          if (typeof fakeEntity.value === "object" && fakeEntity.value.length) {
            if (fakeEntity.value && fakeEntity.value.length && fakeEntity.value[0].type =="File Attachment") {
              fakeEntity.value = fakeEntity.value[0];
              fakeEntity.type = "File Attachment";
            } else {
              fakeEntity.value = fakeEntity.value.join(", ");
            }
          }
      }

      if (entryObject) {
        entryObject.shortInfos[attribute.title] = { key: fakeEntity.key };
        entryObject.shortInfos[attribute.title].value = fakeEntity.value;

        entryObject.shortInfos[attribute.title].sequence = options.shortObjectSequance;
        // options.shortObjectSequance++;
      }

      returnArray.push(fakeEntity);
      return returnArray;
    },
    []
  );
}

function applicableSitesContainsSites(applicableSites, sites) {
  return sites.every(function(value) {
    return applicableSites.indexOf(value) >= 0;
  });
}

function getApplicableSites(applicableSites) {
  return [].concat.apply([], applicableSites);
}


function getAuthorizedSites(req, cb) {
  var xToken = req.headers['x-authorization'] || req.query['x-authorization'];
  if (xToken) {

    db['allAuthToken'].findOne({
      token: xToken
    }, function (e, token) {
      if (e || !token) {
        cb ({ status: 403, message: "You are not authorized." }, null);
        return;
      }

      db['users'].findOne({
        _id: db.ObjectID(token.userId || token.user._id)
      }, function (err, user) {
        if (err) {
          cb ({ status: 403, message: "You are not authorized." }, null);
          return;
        }

        var options = req.body || {};

        req.user = user || {};

        options.user = user;

        options.clientOffSet = req.headers["client-offset"] || 0;
        options.clientTZ = req.headers["client-tz"] || "America/Chicago";

        if (req.user['applicableSites']) {
          var accessiableSites = req.user['applicableSites'];
          req.user['applicableSitesList'] = _.map(_.cloneDeep(req.user['applicableSites']), function (sites) {
            return _.map(sites, function(subSites){ return subSites._id});
          });
          req.user['applicableSites'] = _.map([].concat.apply([], accessiableSites), function (site) {
            return site._id;
          });
        }

        var applicableSites = getApplicableSites(req.user["applicableSites"]);
        options.applicableSites = applicableSites;

        loggedInUserDetails.getAllPermission(req.user,function(err,user){
          options.permissions = user["permissions"] || [];
          cb(null, options);
        })
      });
    });
  }
  else {
    cb({ status: 403, message: "You are not authorized." }, null);
  }
}

function getDistinctVersionsEntries(entries) {
  return _.uniq(
    (entries || []).map(function(entry) {
      return parseFloat(entry.parentVersion); //QC3-10412 Prachi
    })
  );
}

function dbConfigApply(collectionName){

  var entities = [];
  entities.push({
    collection: collectionName,
    audit: true,
    index: {
      title: ''
    }
  });

  dbConfig.configure.apply(this, entities);
}


function getCreatedByAndModifiedByUserIds(entries){
  return _.reduce(entries,function(userIdArray,singleEntry){

    if(!singleEntry.createdByName){
      userIdArray.push(db.ObjectID(singleEntry.createdBy));
    }
    if(!singleEntry.modifiedByName){
      userIdArray.push(db.ObjectID(singleEntry.modifiedBy));
    }

    return userIdArray;
  },[])
}

module.exports = {
  dbConfigApply:dbConfigApply,
  getStausString: getStausString,
  getAttributes: getAttributes,
  getEntities: getEntities,
  projectedListForAdvancedSearch: projectedListForAdvancedSearch,
  displayValueByFormat: displayValueByFormat,
  getAuthorizedSites: getAuthorizedSites,
  getDistinctVersionsEntries: getDistinctVersionsEntries,
  getCreatedByAndModifiedByUserIds:getCreatedByAndModifiedByUserIds
};
