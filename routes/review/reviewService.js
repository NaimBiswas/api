"use strict";

var _ = require("lodash");
var Promise = require("bluebird");
var express = require("express");
var moment = require("moment-timezone");
var db = require("../../lib/db");
var Utils = require("./utils");
var advancedSearchByAppCore = require('../advancedSearch/advancedSearchByAppCore');

function attachQuery(options, schema){
  getEntryQuery(options);
  advancedSearchByAppCore.makeAttributeEntityFilter(options, schema);
}

function getEntryQuery(options){
  if(!options.noPermissionCheck){
    options.query["workflowreview"]= {
      "$elemMatch": {
        "$or": [
          {
            "userSelectionType": "0",
            "roles": {
              "$in": options.roles
            }
          },
          {
            "userSelectionType": {
              "$ne": "0"
            },
            "user": {
              "$elemMatch": {
                "id" : {"$in":[options.userId.toString(),options.userId]}
              }
            }
          }
        ]
      }
    };
  }

  if(options.entryId){
    options.query['_id'] = db.ObjectID(options.entryId);
  }

  return options.query;
}

function getEntryData(options,cb){
  db.collection(options.collection)
  .find(options.query)
  .sort(options.sort)
  .skip(options.skip)
  .limit(options.limit)
  .toArray(function (err, entries) {
    cb(err,entries)
  });
}

function getEntryDataCount(options,cb){
  db[options.collection].count(options.query,function(err,entries){
    cb(err,entries)
  });
}

function getMasterData(ids, cb) {
  if (!ids) {
    cb("No Records Found",null);
  } else {
    var query = {'_id': ids, isActive: true};
    db.masterqcsettings
    .find(query,{schema:1, title:1, module: 1})
    .toArray(function schemaDetailsCb(err, data) {
      if(err && !data[0]){
        return cb(err, null);
      }

      cb(null, data);

    });
  }
}

function getSchemaDetails(appId,options, cb) {
  db.schema
  .find({ _id: db.ObjectID(appId) })
  .toArray(function schemaDetailsCb(err, data) {
    if(data[0]){

      if(options.noPermissionCheck){
        cb(null,data[0]);
        return ;
      }

      if(options.permissions &&
        options.permissions[data[0].module._id] &&
        options.permissions[data[0].module._id][data[0]._id.toString()] &&
        options.permissions[data[0].module._id][data[0]._id.toString()][4]==1){

          cb(null,data[0]);
          return
        }
        cb({message: "no review permission"},null);
    }
    else{
      cb(err,null);
    }
  });
}

function getLettestSchemaTitle(schemaIds, cb) {
  db.schema
    .find({ _id: {$in: schemaIds} }, { _id: 0, title: 1 })
    .toArray(function lettestSchemaTitleCb(err, data) {
       if(data){
        cb(null,data);
       }
       else{
        cb(err,null);
       }
    });
}

function getAuditlogIDs(entriesDistinctVersionList, schemaVersionArray) {
  return (schemaVersionArray || []).reduce(function versionsCallback(
    idArray,
    currentValue
  ) {
    if (entriesDistinctVersionList.indexOf(currentValue.version) != -1) {
      idArray.push(currentValue.record);
    }
    return idArray;
  },
  []);
}

function getSchemaVersionWise(entriesDistinctVersionList,schemaVersionArray,cb) {
  var auditQuery = {
    _id: {
      $in: getAuditlogIDs(entriesDistinctVersionList, schemaVersionArray)
    }
  };

  db["schemaAuditLogs"]
  .find(auditQuery)
  .toArray(function schemaAuditLogCallback(err, auditLogs) {

    if(err){
      return cb(err,null)
    }


    var schemaVersionWise = (auditLogs || [])
    .reduce(function versionWiseLogs(returnObject, currentValue) {
      returnObject[currentValue.version] = currentValue;
      return returnObject;
    },
    {});
    cb(null,schemaVersionWise)
  });
}

function getCommonSiteIdsinAllEntries(entries) {
  var siteIds = [];

  _.forEach(entries, function(singleEntry) {
    _.forEach(singleEntry.levels, function(level) {
      if (siteIds.indexOf(level.siteId) == -1) {
        siteIds.push(level.siteId);
      }
    });
  });

  siteIds = _.reduce(
    siteIds,
    function(siteIdsResult, siteIdString) {
      siteIdsResult.push(db.ObjectID(siteIdString));
      return siteIdsResult;
    },
    []
  );

  return siteIds;
}

function getSiteIdsWithNames(siteIds, cb) {
  db.sites.aggregate(
    [
      { $match: { _id: { $in: siteIds } } },
      {
        $lookup: {
          from: "siteNames",
          localField: "level._id",
          foreignField: "_id",
          as: "siteName"
        }
      },
      {
        $project: { _id: 1, siteName: 1 }
      }
    ],
    function(err, sitesWithName) {
      if (err) {
        return cb(err,null);
      }

      sitesWithName = sitesWithName.reduce(function(returnObject, siteObject) {
        returnObject[siteObject._id] = siteObject.siteName[0].title;
        return returnObject;
      }, {});

      cb(null, sitesWithName);
    }
  );
}

function attachSiteNameTitleToEntryObject(entry, siteIdWithNames) {
  entry.levels.forEach(function(level) {
    level.siteName = siteIdWithNames[level.siteId];
  });
}

function addConditionalProcedures(parentSchema){
  _.forEach(parentSchema.entities,function(entity, index) {
    _.forEach(entity.conditionalWorkflows || [],function(workflow) {
      if(workflow.procedureToExpand && _.isUndefined(workflow.procedureToExpand['isProcedure'])) {
        // For solving QC3-10283 - for differentiate conditional procedure Vs Entity - JD
        // Adding into entities rather then procedure. Due to this sequence is maintain as display in workfow
        workflow.procedureToExpand['isProcedure'] = true;
        // Use splice for insert procedure at specific index
        parentSchema.entities.splice((index+1), 0, workflow.procedureToExpand);
      }
    });
  })
}

function idWiseDateFormat(cb){



  db.types.find({
    title:"Textbox"
  }).toArray(function(err,typedata){

    var idWiseDateFormat = {};

    _.forEach(typedata[0].formats,function(format){
      if(format.title == "Date" || format.title == "Time"){
        _.forEach((format.metadata[0] || {}).allowItems,function(item){
          idWiseDateFormat[item._id] = item.title;
        })
      }
    })
    cb(null,idWiseDateFormat);
  })
}

// For solving QC3-10283 - Return true or false based on any questions contains value - JD
function needToDisplay(proc, entries) {
  var count = 0;
  _.forEach(proc.questions, function(question) {
    if(entries[question['s#']+'-'+proc['s#']] &&
    !_.isNull(entries[question['s#']+'-'+proc['s#']][proc['s#']]) &&
    !_.isUndefined(entries[question['s#']+'-'+proc['s#']][proc['s#']])) {
      count++;
    }
  });
  return count > 0 ? true : false;
}

// For solving QC3-10283 - Remove blank procedure + entities from display list - JD
function filterProceduresWithBlankValues(parentSchema, entry) {
  if(entry && entry.entries) {
    // Filtering Procedures
    parentSchema.procedures = _.filter(parentSchema.procedures, function(procedure) {
      // condition written for optional procedures
      if(procedure.isOptionalProcedure) {
        return entry.entries.settings && entry.entries.settings[procedure['s#']] ? needToDisplay(procedure, entry.entries) : false;
      } else {
        return needToDisplay(procedure, entry.entries);
      }
    });

    // Filtering Entities
    parentSchema.entities = _.filter(parentSchema.entities, function(entity) {
      return needToDisplay(entity, entry.entries);
    });
  }
}

function getFormatedEntryObject(entry, parentSchema, options) {
  options.shortObjectSequance = 1;

  var entryObject = {
    shortInfos: {},

    //dodh daya thaine apel response
    _id: entry._id,
    // version: entry.version,
    child:entry.child,
    parentVersion: entry.parentVersion,
    latestSchemaVersion: options.latestSchemaVersion,
    createdByName: entry.createdByName?entry.createdByName:options.usersObject[entry.createdBy],
    modifiedByName: entry.modifiedByName?entry.modifiedByName:options.usersObject[entry.modifiedBy],
    createdDate: moment
    .utc(new Date(entry.createdDate))
    .tz(options.clientTZ)
    .format("MM/DD/YYYY HH:mm:ss"),
    modifiedDate: moment
    .utc(new Date(entry.modifiedDate))
    .tz(options.clientTZ)
    .format("MM/DD/YYYY HH:mm:ss"),
    levels: entry.levels,
    createdBy: entry.createdBy,
    modifiedBy: entry.modifiedBy,
    createdByInfo: entry.createdByInfo,
    workflowreview: entry.workflowreview,
    isRejected: entry.isRejected,
    liteLogs: entry.liteLogs,
    entryStatus : entry.entries.status,
    rejectedObj: entry.rejectedObj,
    assignment: entry.assignment,
    verificationDate: entry.verificationDate,
    calendarIds: entry.calendarIds, //QC3-9853
    calendarverifieduser: entry.calendarverifieduser, //QC3-9853
    reviewStatus: entry.reviewStatus,
    batchEntryId: entry.batchEntryId,
    masterQCSettings: entry.masterQCSettings,
    lastUpdated: entry.lastUpdated,
    schema: parentSchema
    ? {
      _id: parentSchema.identity,
      title: parentSchema.title,
      // attributes: parentSchema.attributes,
      // procedures: parentSchema.procedures,
      isActive: parentSchema.isActive,
      workflowreview: parentSchema.workflowreview,
      allicableSites: parentSchema.allicableSites, //PQT-1
      module: parentSchema.module,
      description: parentSchema.description,
      allowcreater:parentSchema.allowcreater,
      // allowCreaterObj: parentSchema.allowCreaterObj,
      file: parentSchema.file,
      // entities: parentSchema.entities,
      latestSchemaKeyvalue: parentSchema.keyvalue,
      headMsgPrint: parentSchema.headMsgPrint,
      isLockRecord: parentSchema.isLockRecord,
      headMsgPrintForPass: parentSchema.headMsgPrintForPass,
      headMsgPrintForFail: parentSchema.headMsgPrintForFail,
      instructionURL: parentSchema.instructionURL,
      siteYesNo: parentSchema.siteYesNo,
      isRejected: parentSchema.isRejected //PQT-20
    }
    : undefined
  };

  entryObject.child = (entryObject.child || []).concat(entry.childprocInitiate || []);

  // entryObject.shortInfos["Modified By"] = parentSchema.title;
  // entryObject.shortInfos["Modified By"] = parentSchema.title;

  if (parentSchema) {
    addConditionalProcedures(parentSchema);

    // For solving QC3-10283 - Remove blank procedures + entities from display list - JD
    filterProceduresWithBlankValues(parentSchema, entry);

    entryObject["attributes"] = Utils.getAttributes(
      parentSchema.attributes,
      entry,
      entryObject,
      options
    );
    options.shortObjectSequance++;
    entryObject["entities"] = Utils.getEntities(
      parentSchema.entities,
      entry,
      entryObject,
      options
    );
    // options.shortObjectSequance++;

    entryObject["procedures"] = Utils.getEntities(
      parentSchema.procedures,
      entry,
      entryObject,
      options
    );
    entryObject["keyvalues"] = Utils.getAttributes(
      parentSchema.keyvalue,
      entry,
      undefined, // QC3-10196 - Jyotil
      options // QC3-10196 - Jyotil
    );
  }

  entryObject["status"] = Utils.getStausString(entry);
  entryObject["FailMesssages"] = [];
  if (entry.entries.FailMesssageForQCForm) {
    _.forEach(entry.entries.FailMesssageForQCForm, function(value, key){
      entryObject["FailMesssages"].push(value)
    });
  }
  entryObject.shortInfos["Review Status"] = entryObject["status"];

  //Su joie che e bol
  entryObject.shortInfos["Created Date"] = entryObject.createdDate;
  entryObject.shortInfos["Modified Date"] = entryObject.modifiedDate;
  entryObject.shortInfos["Modified By"] = entryObject.modifiedByName;
  entryObject.shortInfos["Record Status"] = entryObject.status;
  // entryObject.shortInfos["Batch Entry ID"] = entryObject.batchEntryId;

  return entryObject;
}

//QC3-10168
function getUsers(userIds,cb){
  db.users.find({
    _id:{
      "$in":userIds
    }
  }).toArray(function(err,userArray){


    var userObject = _.reduce(userArray,function(_userObject,user){
      _userObject[user._id.toString()] = `${user.firstname} ${user.lastname} (${user.username})`
      return _userObject;
    },{})

    cb(err,userObject);
  });
}

module.exports = {
  getEntryData: Promise.promisify(getEntryData),
  getSchemaDetails: Promise.promisify(getSchemaDetails),
  getLettestSchemaTitle: Promise.promisify(getLettestSchemaTitle),
  getSchemaVersionWise: Promise.promisify(getSchemaVersionWise),
  getMasterData: Promise.promisify(getMasterData),
  getEntryDataCount: Promise.promisify(getEntryDataCount),
  getSiteIdsWithNames:Promise.promisify(getSiteIdsWithNames),
  idWiseDateFormat:Promise.promisify(idWiseDateFormat),
  getUsers:Promise.promisify(getUsers),
  getCommonSiteIdsinAllEntries:getCommonSiteIdsinAllEntries,
  attachSiteNameTitleToEntryObject:attachSiteNameTitleToEntryObject,
  getFormatedEntryObject:getFormatedEntryObject,
  attachQuery:attachQuery
}
