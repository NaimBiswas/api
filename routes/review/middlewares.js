
var _ = require("lodash");
var Promise = require("bluebird");
var express = require("express");
var moment = require("moment");

var db = require("../../lib/db");

var Utils = require("./utils");
var ReviewService = require("./reviewService");
var advancedSearchByAppCore = require('../advancedSearch/advancedSearchByAppCore');

function attachOptions(req,res,next){

    var options = {};
    options.clientTZ = req.headers["client-tz"] || "America/Chicago";

    Utils.getAuthorizedSites(req, function (err, siteFilters) {
        if (err) {
          res.status(err.status).json(err.message);
        } else {

          Object.assign(options, siteFilters);
          options = createFilters(options);
          req.options = options;

          next()

        }
      });
}

function createFilters(body) {

  var pageSize = parseInt(body.pageSize || 10);
  var pageNumber = parseInt(body.pageNumber || 1);

  var options = {
    pageSize: pageSize,
    pageNumber: pageNumber,
    skip: pageSize * (pageNumber - 1),
    appFilter: {},
    roles: (body.user.roles || []).map(function (role) {
      return role.title;
    }) || [],
    userId: body.user._id,
    noPermissionCheck: body.noPermissionCheck,
    collection: body.appId,
    fetchMaster: body.fetchMaster,
    limit: pageSize,
    user: body.user,
    permissions: body.permissions,
    apps: body.apps,
    schemaIds: [],
    attributes: body.attributes,
    applicableSites: body.applicableSites,
    includeInActiveRecords: body.includeInActiveRecords,
    batchEntryId: body.batchEntryId,
    clientOffSet: body.clientOffSet,
    clientTZ: body.clientTZ,
    entryId: body.entryId,
    sort: body.sort || {
      'createdDate': -1
    },
    query: body.query || {}
  };

  if (body.sort) {
    options.sort = body.sort;
  }
  if (body && body.apps && body.apps.length) {
    options.schemaIds = { '$in': body.apps.map(function (app) { return db.ObjectID(app) }) };
  }
  if (body.dates && (body.dates.start || body.dates.end)) {
    options.query['createdDate'] = advancedSearchByAppCore.generateDateFilterScript(body.dates.start, body.dates.end);
  }
  if (body.status && body.status.length) {
    options.query['$or'] = advancedSearchByAppCore.generateStatusFilter(body.status);
  }
  if (body.batchEntryId) {
    options.query['batchEntryId'] = body.batchEntryId.toString();
  }
  if (body.levelIds && body.levelIds.length) {
    options.query['levelIds'] = {
      $all: body.levelIds
    };
  }
  if (body.isBulkReview) {
    options.query['batchEntryId'] = {
      $exists: false
    };
  }
  if (body.isRejected) {
    options.query['$or'] = options.query['$or'] || [];
    options.query['$or'].push({
      'isRejected': body.isRejected
    });
  }
  if (body.assignee && body.assignee.length) {
    options.query['assignment.assignee.id'] = {
      '$in': body.assignee
    };
  }

  if (!options.noPermissionCheck) {
    if (body.applicableSites && body.applicableSites.length) {
      options.query['$where'] = JSON.stringify(body.applicableSites).replace(/\s/g, '').concat('.indexOf(this.levelIds[this.levelIds.length - 1])>=0');
    }

    if (body.fetchMaster) {
      options.query['masterQCSettings'] = {
        $exists: true
      };
      options.query['masterQCSettings.masterQCSettingsId'] = { $in : options.apps};
    } else {
      options.query['masterQCSettings'] = {
        $exists: false
      };
    }
  }
  
  return options;
}

function handleMasterApp(req,res,next){
    if (req.body.fetchMaster && req.body.apps && req.body.apps.length) {
        fetchMasterAppRecords();
    }else{
        next();
    }
    
    function fetchMasterAppRecords(){
        ReviewService.getMasterData(req.options.schemaIds).then(function (resultMain) {
            // options.permissions[result.module._id]
            var result = resultMain[0];
            var schemaIds = [];

            result.schema = result.schema.filter(function (schema) {
              schemaIds.push(db.ObjectID(schema._id));
              return req.options.permissions &&
              req.options.permissions[result.module._id] &&
              req.options.permissions[result.module._id][schema._id] &&
              req.options.permissions[result.module._id][schema._id][4]==1;
            });

            ReviewService.getLettestSchemaTitle(schemaIds).then(function(lettestSchemas){
              // _.merge(result.schema, lettestSchemas);
              req.options['masterDetails'] = resultMain;
              next()
            });

          }).catch(function (err) {
            res.status(404).json(err);
          });
    }
}

module.exports = {
    attachOptions:attachOptions,
    handleMasterApp:handleMasterApp
}
