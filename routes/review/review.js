"use strict";


var express = require("express");
var promise = require('bluebird');
var _ = require("lodash");

var db = require("../../lib/db");

var Utils = require("./utils.js");
var Middlewares = require("./middlewares");
var ReviewService = require("./reviewService");


module.exports = {
  attachRoutes:attachRoutes,
  getReviewData:getReviewData
}


function getReviewData(req, res, next) {

    Utils.dbConfigApply(req.options.collection);

    var entries;
    var schema;
    var siteIdWithNames;
    var entryCount;
      
    ReviewService.getSchemaDetails(req.options.collection,req.options).then(function(schemaDetail){
      schema = schemaDetail;
      
      ReviewService.attachQuery(req.options, schema);
      
      return Promise.all([
        ReviewService.getEntryData(req.options),
        ReviewService.getEntryDataCount(req.options),
        ReviewService.idWiseDateFormat()
      ]);

    }).then(function entriesAndSchemaRes(response){
      entries = response[0];
      req.options.latestSchemaVersion = schema.version;
      entryCount = response[1];
      req.options.idWiseDateFormat = response[2];

      if(entries.length && schema){
        //QC3-10168
        return Promise.all([
          ReviewService.getSiteIdsWithNames(ReviewService.getCommonSiteIdsinAllEntries(entries)),
          ReviewService.getUsers(Utils.getCreatedByAndModifiedByUserIds(entries))
        ])
      }
      else{
        throw {status: 200, result: []};
      }
  })
  .then(function siteAndUsers(siteAndUsers){
    siteIdWithNames = siteAndUsers[0];
    req.options.usersObject = siteAndUsers[1];

    var entriesDistinctVersionList = Utils.getDistinctVersionsEntries(entries);
    return ReviewService.getSchemaVersionWise(entriesDistinctVersionList,schema.versions)
  })
  .then(function schemaVersionWiseRes(schemaVersionWise){

      var responseArray = (entries || []).map(function responseCallback(entry) {
        var parentSchema = schemaVersionWise[entry.parentVersion];
        ReviewService.attachSiteNameTitleToEntryObject(entry,siteIdWithNames);
        return ReviewService.getFormatedEntryObject(entry,_.cloneDeep(parentSchema),req.options)
      });
      var result = {
        records: responseArray,
        recordCount: entryCount,
        masterDetails: req.options['masterDetails']
      };

      res.status(200).json(result);
  })
  .catch(function(err){
    res.status(200).send({
      records:[],
      recordCount:0,
      masterDetails: req.options['masterDetails'],
      error: err
    });
  });

    }




function attachRoutes(app) {
  var router = express.Router();

  router.post("/signAllEntries", function(req, res, next) {
    if(req.body.updateAll){
      db[req.body.schemaIds].updateMany({_id: {$in: (req.body.entryIds || []).map(function(entryId){
        return db.ObjectID(entryId);
      })}}, {$set: {workflowreview: req.body.commonWorkflowreview}}, function(error, response){
        // db['qcactivityinfo'].insertMany([]);
        res.send(response);
      });
    } else {
      var updateRecursively = function(counter){
        if(getReviewData(req.body.entryIds || []).length == counter){
          res.send({messege: 'Sign successfully.'});
        } else {
          db[req.body.schemaIds].updateOne({_id: db.ObjectID(req.body.entryIds[counter])}, {$set: {workflowreview: req.body.workflowreview[req.body.entryIds[counter]]}}, function(error, response){
            counter++;
            updateRecursively(counter);
          });
        }
      }
      updateRecursively(0);
    }

  });

  router.post("/entryDetails", Middlewares.attachOptions ,function(req, res, next) {
    var options = {};
    options.clientTZ = req.headers["client-tz"] || "America/Chicago";
    var body = req.body;
    var schemaId = body.schemaId;
    var entryId = body.entryId;
    db.collection(schemaId)
    .find({_id:db.ObjectID(entryId)})
    .toArray(function (err, entries) {

      db.collection("schema")
      .find({_id:db.ObjectID(schemaId)})
      .toArray(function (err, schema) {
        ReviewService.idWiseDateFormat().then(function(idWiseDateFormat){
          options.idWiseDateFormat = idWiseDateFormat;


          ReviewService.getUsers(Utils.getCreatedByAndModifiedByUserIds(entries))
            .then(function(usersObject){

              options.usersObject = usersObject;
              if(schema[0]){
                var entriesDistinctVersionList = Utils.getDistinctVersionsEntries(entries);
                ReviewService
                .getSchemaVersionWise(entriesDistinctVersionList,schema[0].versions)
                .then(function(schemaVersionWise){
                  var parentSchema = schemaVersionWise[entries[0].parentVersion];
                  var entryData = ReviewService.getFormatedEntryObject(entries[0],parentSchema,options)
                  res.status(200).send(entryData);
                })
              } else{
                res.status(404).send({message:"not found"});
              }


            })


        })
      })
    });
  });

  router.post("/", Middlewares.attachOptions,
      Middlewares.handleMasterApp,
      getReviewData);

  app.use("/review", router);
};
