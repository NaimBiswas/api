'use strict'
var types = require('../../lib/validator/types');
var db = require('../../lib/db');
var tdb = require('../../lib/db/tdb.js');
var restler = require('restler');
var _ = require('lodash');
var config = require('../../config/config.js');
var csventriescore = require('./csventriescore');
var logger = require('../../logger');
var currentFileName = __filename;
var defaultRoute = require('../defaultRoute/defaultRoute.js');
var q = require('q');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST


/**
* @swagger
* definition:
*   CsvEntries:
*     properties:
*       _id:
*         type: string
*       reviewStatus:
*         type: string
*       entries:
*         type: object
*       csvdata:
*         type: object
*       FailReasons:
*         type: object
*       type:
*         type: object
*       importId:
*         type: object
*       createdDateInfo:
*         type: object
*       parentVersion:
*         type: object
*       mappingId:
*         type: object
*       status:
*         type: object
*       createdByInfo:
*         $ref: '#/definitions/ModifiedBy'
*/

/**
* @swagger
* /csventries:
*   get:
*     tags: [Import Data]
*     description: Returns all csv entries
*     produces:
*       - application/json
*     parameters:
*       - name: pagesize
*         description: Page Size
*         in: query
*         required: true
*         type: string
*       - name: q[mappingId]
*         description: Mapping Id
*         in: query
*         required: true
*         type: string
*       - name: q[status]
*         description: Status
*         in: query
*         required: true
*         type: string
*     responses:
*       200:
*         description: An array of csv entries
*         schema:
*           $ref: '#/definitions/CsvEntries'
*/

/**
* @swagger
* definition:
*   CsvEntryPostModel:
*     properties:
*       type:
*         type: string
*       csv:
*         type: string
*/


/**
* @swagger
* /csventries:
*   post:
*     tags: [Import Data]
*     description: creates new csv entries
*     produces:
*       - application/json
*     parameters:
*       - name: csventrie
*         description: Csv Entry
*         in: query
*         required: true
*         type: string
*       - name: schemaId
*         description: Schema Id
*         in: query
*         required: true
*         type: string
*       - name: mappingId
*         description: Mapping Id
*         in: query
*         required: true
*         type: string
*     responses:
*       200:
*         description: An array of csv entries
*         schema:
*           $ref: '#/definitions/CsvEntryPostModel'
*/

var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'csventries',
    collection: 'csventries',
    schema: types.any,
    softSchema: types.any
  },
  types: {
    POST: {
      ONE: {
        before: defaultPost
      }
    }
  }
});


// return route.router;
function defaultPost(req, res, next) {
  logger.log("function: defaultPost - start", 'info', currentFileName);

  if(!req.query.schemaId){
    db["csventries"].deleteMany({'mappingId' : req.query.mappingId});
    return res.json("Success");
  }

  db['users'].findOne({_id: db.ObjectID(req.user._id)},{username:1, firstname: 1, lastname: 1}, function(e,user) {
    if (e) {
      return;
    } else if(user){
      var col = "csventries";
      var schema = req.query.schemaId; //to call for entering records
      var mapping = req.query.mappingId; // to call csventries
      db[col].find({'status' : 'Pass','mappingId' : mapping}).toArray(function (err, csventries) {
        logger.log("collection: " + col + " find" , 'info', currentFileName);        
        var recordsData =  _.cloneDeep(csventries);
        var csventriesData =  _.cloneDeep(csventries);
        var entries = [];
        var entityRecordsList = [];
        var today = new Date();
        var timeStampSequence = today.getTime();
        csventriescore.mapRecords(user, csventriesData, entityRecordsList, entries, today, timeStampSequence)
        if (entityRecordsList.length) {
          csventriescore.postHandler(req, res, schema, entityRecordsList, null, function () {
            if (err) {
              return res.status(400).send(err);
            } else {
              db["csventries"].deleteMany({'mappingId' : mapping});
              return res.json({type : recordsData[0].type || 'qcform', csv : timeStampSequence });
            }
          });
        }else if (entries.length) {
          db['schema'].findOne({_id: db.ObjectID(schema)}, function(err,schemaKey) {
            if (err || !schemaKey) {
              return res.status(400).send("App not available");
            } else {
              checkAutoNumber(schemaKey,entries).then(function(entries){
                csventriescore.postHandler(req, res, schema, entries, schemaKey, function (err, result) {
                  if (err) {
                    return res.status(400).send(err);
                  }else {
                    db["csventries"].deleteMany({'mappingId' : mapping});
                    return res.json({type : recordsData[0].type || 'qcform', csv : timeStampSequence });
                  }
                });
              }).catch(function(err){
                return res.status(400).send(err);
              })

            }
          })
        } else {
          logger.log("collection: " + col + " find" , 'info', currentFileName);
          return res.status(400).send("No Records Match.");
        }
      });
    } else {
      next();
    }
  });
}

/*
* arguments: 1) app-schema  2) csv data
* aim : add auto generate number in csv app data entry
* return : entry object
* By vivek
*/
function checkAutoNumber(schemaKey,entries){
  var deffered = q.defer();
  var autoNumberObj = _.find(schemaKey.attributes,function(att){
    return att.type.title == 'Auto Generate';
  });
  if(autoNumberObj){
    defaultRoute.getNextAutoNumber(autoNumberObj._id,entries.length).then(function(data){
      var autoNumberCounter = data.autoGeneratedAttributeCounter + 1;
      entries.forEach(function(entry){
        entry.entries[autoNumberObj['s#']] = {attributes :autoNumberObj.autoGenerateNumberPrefix + autoNumberCounter + autoNumberObj.autoGenerateNumberSuffix}
        autoNumberCounter++;
      })
      deffered.resolve(entries);
    }).catch(function(){
      deffered.reject({error : 'Auto increment number error'})
    })
  }else{
    deffered.resolve(entries);
  }
  return deffered.promise;
}

function beforePost(req, res, next) {
  logger.log("function: beforePost - start", 'info', currentFileName);

  if(!req.query.schemaId){
    db["csventries"].deleteMany({'mappingId' : req.query.mappingId});
    return res.json("Success");
  }
  var col = "csventries";
  var schema = req.query.schemaId;
  var mapping = req.query.mappingId;
  db[col].find({'status' : 'Pass','mappingId' : mapping}).toArray(function (err, pdata) {
    var adas = pdata;
    var options = { headers : { authorization : req.headers.authorization } };
    var i = 0;
    var pdLength = pdata.length;
    var entries = [];
    var dt = new Date();
    var sq = dt.getTime();
    /* use restler to post data so that audi and log can be maintened*/
    var fn = function (){
      if (i < pdLength) {
        var psdt = pdata[i];
        delete psdt.csvdata;
        delete psdt.FailReasons;
        delete psdt.status;
        delete psdt._id;
        delete psdt.mappingId;

        if (psdt.type == 'entity') {
          delete psdt.entries.status;
          psdt.entries.isActive = true;
          _.forEach(psdt.entries, function (v, k) {
            if (typeof v == 'object') {
              psdt.entries[k] = v.attributes;
            }
          });
          var nschema = {
            isDeleted : false,
            entityRecord : psdt.entries
          }
          nschema['csv'] = sq;
          restler.postJson(config.apiEnvUrl + '/entityrecords/' + schema, nschema, options)
          .on('complete', function (cmplt) {
            //entries.push(cmplt._id);
            i++;
            fn();
          })
          .on('timeout', function () {
            i++;
            fn();
          })
          .on('error', function (err) {
            i++;
            fn();
          });
        }
        else {
          psdt['csv'] = sq;
          restler.postJson(config.apiEnvUrl + '/entries/' + schema, psdt, options)
          .on('complete', function (cmplt) {
            entries.push(cmplt._id);
            i++;
            fn();
          })
          .on('timeout', function () {
            i++;
            fn();
          })
          .on('error', function (err) {
            i++;
            fn();
          });
        }
      }
      else {
        db["csventries"].deleteMany({'mappingId' : mapping});
        return res.json({type : adas[0].type || 'qcform', csv : sq });
      }
    }
    fn();
    /* for now remove bulk storage*/
    //db[schema].insertManyAsync(pdata).then(function (gfd) {
    //    db[schema].update({}, { $unset : { status : 1, csvdata: 1 } }, false,true);
    //    return res.json("Success");
    //    console.log(gfd);
    //}, function (perr) {
    //    console.log(perr);
    //});
  }, function (err) {
    return res.status(400).send(err);
  });


  // console.log(db[req.query.schemaId]);
  // console.log(db["csventries_" + req.query.csventries]);
  // db['csventries_'+req.query.csventries].find({'status':'Pass'}).toArray(function (data) {
  //   console.log(data);
  // db[req.query.schemaId].insertManyAsync(data).then(function (entries) {
  //     console.log("data saved");
  // }).catch(function (err) {
  //     console.log(err);
  // });
  // db[req.query.schemaId].update({},$unset:{status:''}).then(function (entries) {
  //     console.log("data saved");
  // }).catch(function (err) {
  //     console.log(err);
  // });
  // })

}

module.exports = route.router;
