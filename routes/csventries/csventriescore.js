'use strict'
/**
* @name db
* @author Loki <lokesh.a@productivet.com>
*
* @version 0.0.0
*/
var moment = require('moment');
var q = require('q');
var _ = require('lodash');
var resources = require('../../lib/resources');
var validate = require('../../lib/validator');
var db = require('../../lib/db');
var tdb = require('../../lib/db/tdb.js');
var logger = require('../../logger');
var currentFileName = __filename;
var qcActivity = [];

function postHandler(req, res, entity, records, schemaDetail, cb) {
  logger.log("function: postHandler - start", 'info', currentFileName);
  db[entity].insertMany(records, function postThenCallback(err, result) {
    logger.log("collection: " + entity + " find" , 'info', currentFileName);
    if (err) {
      logger.log("collection: " + entity + " find : " + err , 'error', currentFileName);
      cb(err);
    }else {
      if(result && result.ops && result.ops.length){
        qcActivity = [];
        result.ops.forEach(function(obj){
          // Start - QC3-8865 - By Jyotil - create an array for qcEntryActivity with required object in db.
          if (schemaDetail) {
            qcActivity.push(qcActivityMapper(obj, schemaDetail));
          }
          // End - QC3-8865 - By Jyotil
          /**
           * By MIRAL 8431
           * bug was about #req.ip was not sent
           * line was before looks like
           * // db[entity].auditThis1(obj, false, entity, req.ip);
           * so it should have only two parameter in this function
           * because initial two parameters were alredy bound to the
           * function
           */
          db[entity].auditThis1(obj, req.ip);
        });
        // Start - QC3-8865 - By Jyotil - call setQcEntryActivity function for entry in qcEntryActivity
        if (schemaDetail && qcActivity && qcActivity.length) {
          setQcEntryActivity(qcActivity);
        }
        // End - QC3-8865 - By Jyotil
      } else if(result._id){
        db[entity].auditThis1(result, req.ip);
      }
      req.status = 201;
      req.data = result;
      cb(null, result);
      // return;
    }
  });
}

/**
 * @author: Jyotil
 * @description : QC3-8865 - mapper function to create object for {qcEntryActivity}
 * @param : {Object} qcEntry
 * @param : {Object} schemaDetail
 */
function qcActivityMapper(qcEntry, schemaDetail) {
  var attributes = (schemaDetail.keyvalue || []).filter(function (keyValue) {
    return (keyValue && keyValue['s#'] && qcEntry && qcEntry.entries && qcEntry.entries[keyValue['s#']]);
  }).map(function (keyValue) {
    var keyValueObj = {
      'title' : keyValue.title,
      'type' : keyValue.type,
    };
    if (keyValue['s#'].toString().split('-')[1]) {
      keyValueObj.value = qcEntry.entries[keyValue['s#']][keyValue['s#'].toString().split('-')[1]]
    } else {
      keyValueObj.id = keyValue._id;
      keyValueObj.value = qcEntry.entries[keyValue['s#']]['attributes']
    }
    return keyValueObj;
  });

  return {
    'title' : schemaDetail.title,
    'attributes' : attributes,
    'qcformId' : schemaDetail._id.toString(),
    'qcentryId' : qcEntry._id.toString(),
    'createdDate' : qcEntry.createdDate,
    'status' : 'Created',
    'createdBy' : db.ObjectID(qcEntry.createdBy),
    'modifiedBy' : db.ObjectID(qcEntry.modifiedBy),
    'createdByName' : qcEntry.createdByName,
    'modifiedByName' : qcEntry.modifiedByName,
    'modifiedDate' : qcEntry.modifiedDate,
    'version' : 1,
    'isMajorVersion' : true
  };
}
// End - QC3-8865 - By Jyotil

/**
 * @author: Jyotil
 * @description : QC3-8865 -insert into qcentryactivity with {qcEntryActivity}
 * @param : {Array} qcEntryActivity
 */
 function setQcEntryActivity(qcEntryActivity) {
   db['qcentryactivity'].insertMany(qcEntryActivity, function postThenCallback(err, Success) {
     if (err) {
       logger.log("collection: " + entity + " find : " + err , 'error', currentFileName);
     } else {
       logger.log('qcEntryActtivity are created')
     }
   });
 }
// End - QC3-8865 - By Jyotil

function mapRecords(user, csventries, entityRecordsList, entries, today, timeStampSequence) {
  logger.log("function: mapRecords - start", 'info', currentFileName);
  _.forEach((csventries||[]), function (csventry) {
    delete csventry.csvdata;
    delete csventry.FailReasons;
    delete csventry.status;
    delete csventry._id;
    delete csventry.mappingId;
    csventry.createdBy = user._id;
    csventry.modifiedBy = user._id;

    csventry.isMajorVersion = true;
    csventry.isAuditing = false;
    csventry.createdByName = user.firstname + " " + user.lastname + " ("+user.username+")";
    csventry.modifiedByName = user.firstname + " " + user.lastname + " ("+user.username+")";
    csventry.createdDateInfo = today.toISOString().toString();
    csventry.createdDate = today;
    csventry.modifiedDate = today;

    if (csventry.type == 'entity') {
      delete csventry.entries.status;
      csventry.entries.isActive = true;
      _.forEach(csventry.entries, function (v, k) {
        if (typeof v == 'object') {
          csventry.entries[k] = v.attributes;
        }
      });
      var nschema = {
        entityRecord : csventry.entries,
        isDeleted : false,
        createdBy: csventry.createdBy,
        createdByName: csventry.createdByName,
        modifiedBy: csventry.modifiedBy,
        modifiedByName: csventry.modifiedByName,
        createdDate: csventry.createdDate,
        modifiedDate: csventry.modifiedDate,
        isAuditing: false
      }
      nschema['csv'] = timeStampSequence;
      entityRecordsList.push(nschema);
    } else {
      csventry['csv'] = timeStampSequence;
      entries.push(csventry);
    }
  });
}

module.exports = {
  postHandler: postHandler,
  mapRecords: mapRecords
};
