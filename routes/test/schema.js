var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var rBool = types.rBool;
var rNumber = types.rNumber;
var string = types.string;
var required = types.required;



var schema = {
  title: rString.label('Module Title'),
  isActive: rBool.label('Module Active?')
};


module.exports = schema;
