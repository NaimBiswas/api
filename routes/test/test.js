'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'test',
    collection: 'test',
    schema: schema,
    softSchema: softSchema
  }
});

route.router.get('/what/is/my/ip',function (req, res, next) {
  res.json({ ip: req.ip });
});



module.exports = route.router;
