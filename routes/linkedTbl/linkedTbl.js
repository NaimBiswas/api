'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'linkedTbl',
    collection: 'linkedTbl',
    schema: schema,
    softSchema: softSchema
  }
});
/**
 * @swagger
 * definition:
 *   Site_level:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 */

 /**
 * @swagger
 * definition:
 *   Version:
 *     properties:
 *       version:
 *         type: integer
 *       record:
 *         type: integer
 */

/**
 * @swagger
 * definition:
 *   Site:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       level:
 *         $ref: '#/definitions/Site_level'
 *       isActive:
 *         type: boolean
 *       parents:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Site_level'
 *       children:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Site_level'
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       createdBy:
 *         $ref: '#/definitions/CreatedBy'
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       version:
 *           type: integer
 *       isMajorVersion:
 *           type: boolean
 */

/**
 * @swagger
 * /sites:
 *   get:
 *     tags: [Sites]
 *     description: Returns all sites
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,title,level._id,parents.title,modifiedDate,modifiedBy,isActive])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of sites
 *         schema:
 *           $ref: '#/definitions/Site'
 */

 /**
  * @swagger
  * /sites/{id}:
  *   get:
  *     tags: [Sites]
  *     description: Returns a object site
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: Site's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: pagesize
  *         description: Page Size
  *         in: query
  *         required: false
  *         type: string
  *     responses:
  *       200:
  *         description: An object of site
  *         schema:
  *           $ref: '#/definitions/Site'
  */

 /**
  * @swagger
  * /sites:
  *   post:
  *     tags: [Sites]
  *     description: Creates a new site
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: site
  *         description: Site object
  *         in: body
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Site'
  *     responses:
  *       201:
  *         description: Successfully created
  */

  /**
   * @swagger
   * /sites/{id}:
   *   patch:
   *     tags: [Sites]
   *     description: Updates a site
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: id
   *         description: Site's id
   *         in: path
   *         required: true
   *         type: integer
   *       - name: site
   *         description: Site object
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/Site'
   *     responses:
   *       200:
   *         description: Successfully updated
   */

  /**
   * @swagger
   * definition:
   *   LinkedTbl:
   *      properties:
   *        id:
   *          type: integer
   *        linkedOfId:
   *          type: integer
   *        linkedOfName:
   *          type: string
   *        linkedOfVersion:
   *          type: integer 
   *        linkedofType:
   *          type: string
   *        linkedToId:
   *          type: integer
   *        linkedToType:
   *          type: string
   *        linkedToName:
   *          type: string
   *        linkedToStatus:
   *          type: boolean
   */

  /**
   * @swagger
   * /linkedTbl:
   *   get:
   *    tags: [Attribute]
   *    description: List of apps used with current attribute
   *    produces:
   *       - application/json
   *    parameters:
   *       - name: page
   *         description: Page Number
   *         in: query
   *         required: false
   *         type: string
   *       - name: pagesize
   *         description: Page Size
   *         in: query
   *         required: false
   *         type: string
   *    responses:
   *      200:
   *        description: Array of apps    
   *        schema:
   *          $ref: '#/definitions/LinkedTbl' 
   */

module.exports = route.router;
