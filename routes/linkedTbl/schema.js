'use strict'
/**
 * @name sites-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var string = types.string;
var number = types.number;
var bool = types.bool;
var schema = {
  linkedToId: string.label('Linked To Title'),
  linkedToName: string.label('Linked To Name'),
  linkedToVersion: number.label('Linked To Version'),
  linkedToType: string.label('Linked To Type'),
  linkedToStatus:bool.label('Linked To Status'),
  linkedOfId: string.label('Linked For Title'),
  linkedOfName: string.label('Linked For Name'),
  linkedOfVersion: number.label('Linked For Version'),
  linkedOfType: string.label('Linked For Type'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
