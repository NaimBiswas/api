//Start: QC3-5223 - Backlog - Sets - The existing sets of the user shall update dynamically.
//Changed By: Jyotil
//Description: Added seeding file.
var express = require('express');
var db = require('../../lib/db');
var log = require('../../logger');
var _ = require('lodash');
var restler = require('restler');
var config = require('../../config/config.js');

module.exports = function(app){
  var apiRoutes = express.Router();

  //checkSetExists - check if Existing User exists.
  function checkSetExists(req,res,next) {
    db.sets.find({'title':'Existing Users'}).toArray(function (err, sets) {
        if (err) {
          next(err);
          return;
        }
        //Start: QC3-9046 - Jyotil - if Existing user is created then delete old set.
        if(sets && sets.length){
          db.sets.remove(db.ObjectId(sets[0]._id))
        }
        //End: QC3-9046 - Jyotil
        next();
    });
  }

  // getUsers - get the user data and put it into the variable.
  function getUsers(req, res, next) {
    req.userValues = [];
    db.users.find({},{username:1, isActive:1}).toArray(function (err, users) {
      if (err) {
          next(err);
          return;
        }
        _.forEach(users, function(user){
          req.userValues.push({
            "title" : user.username,
            "value" : user.username,
            "isActive" : user.isActive
          });
        });
        next();
    });
  }

  function updateSet(req, res) {
    var insertSetData = {
      'title':'Existing Users',
      'values': req.userValues,
      'isActive' : true
    };
    var options = {};
    if (req.headers.authorization) {
      options = { headers : { 'Authorization' : req.headers.authorization } };
    } else if (req.headers['x-authorization']) {
      options = { headers : { 'X-Authorization' : req.headers['x-authorization'] } };
    }

    restler.
    postJson(config.http + '://' + req.headers.host + '/sets',insertSetData, options)
    .on('complete', function (cmplt) {
      res.status(200).json({'insertation':'User is added to SET Collection'});
    })
    .on('timeout', function () {
      res.status(200).json({'timeout':'timeout'});
    })
    .on('error', function (err) {
      res.status(500).json({'error':err});
    });
  }


  apiRoutes.get('/setDefaultUserInSet',checkSetExists,getUsers,updateSet);

  app.use('/seeding', apiRoutes);
}
//End: QC3-5223 - Backlog - Sets - The existing sets of the user shall update dynamically.
