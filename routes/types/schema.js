'use strict'
/**
 * @name formatTypes-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */

var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var array = types.array;
var rId = types.rId;
var rString = types.rString;
var rBool = types.rBool;
var stringArray = types.stringArray;

var schema = {
  _id: rId.label('Type Id'),
  title: rString.label('Type title'),
  formats: array(object({
    _id: rId.label('Format id'),
    title: rString.label('Format title'),
    metadata: array(object({
      _id: rId.label('Metadata id'),
      name: rString.label('name'),
      title: rString.label('Metadata title'),
      isOptional: rBool.label('Metadata isOptional'),
      dataType: rString.label('Metadata dataType'),
      allowItems: array(object({
        _id: rId.label('AllowItems id'),
        title: rString.label('AllowItems title')
      }))
    }))
  }))
};

module.exports = schema;
