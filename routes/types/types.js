'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'types',
    collection: 'types',
    schema: schema,
    softSchema: softSchema
  }
});

/**
 * @swagger
 * definition:
 *   Metadata_all:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       name:
 *         type: string
 *       isOptional:
 *         type: boolean
 *       dataType:
 *         type: string
 */

/**
 * @swagger
 * definition:
 *   Formats_all:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       metadata:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Metadata_all'
 */

/**
 * @swagger
 * definition:
 *   Types_all:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       formats:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Formats_all'
 */

/**
 * @swagger
 * /types:
 *   get:
 *     tags: [General]
 *     description: Returns all types
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ title])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of types
 *         schema:
 *           $ref: '#/definitions/Types_all'
 */


module.exports = route.router;
