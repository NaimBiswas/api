'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');
var joi = require('joi');

var object = types.object.bind(types);
var string = types.string;
var date = types.date;
var any = types.any;

var schema = object({
  productVersion: any.label('Product Version'),
  dbScripts: any.label('Database Scripts'),
  web : object({
    major: any.label('major'),
    minor: any.label('minor'),
    build: any.label('build'),
    revision: any.label('revision')
  }).label('Web Version'),
  api : object({
    major: any.label('major'),
    minor: any.label('minor'),
    build: any.label('build'),
    revision: any.label('revision')
  }).label('API Version'),
  db : object({
    major: any.label('major'),
    minor: any.label('minor'),
    build: any.label('build'),
    revision: any.label('revision')
  }).label('DB Version'),
  report : object({
    major: any.label('major'),
    minor: any.label('minor'),
    build: any.label('build'),
    revision: any.label('revision')
  }).label('Report Version'),
  reportApi : object({
    major: any.label('major'),
    minor: any.label('minor'),
    build: any.label('build'),
    revision: any.label('revision')
  }).label('Report API Version'),
  branch: string.label('Branch Name'),
  timeZone: string.label('Time Zone')
});

module.exports = schema;
