'use strict'
var softSchema = require('./softSchema');
var schema = require('./schema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'versions',
    collection: 'versions',
    softSchema: softSchema,
    schema : schema
  }
});

/**
 * @swagger
 * /versions:
 *   post:
 *     tags: [Version]
 *     description: Creates a new version
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: version
 *         description: Version object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/VersionModel'
 *     responses:
 *       201:
 *         description: Successfully created
 */
module.exports = route.router;
