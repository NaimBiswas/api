'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var _ =require('lodash');
var db = require('../../lib/db');
//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'activityRequestParameter',
    collection: 'activityRequestParameter',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    POST: {
      ONE: {
        before: beforePost,
      }
    },
  }
});

function beforePost(req, res, next) {
  _.forEach(req.body.userIdlist, function (user,idx) {  
    req.body.userIdlist[idx] = db.ObjectID(user);
  })
  next();
}
/**
 * @swagger
 * definition:
 *   FormIdlist:
 *     properties:
 *       Id:
 *         type: integer
 *       formName:
 *         type: string
 */

/**
 * @swagger
 * definition:
 *   ActivityRequestParameter:
 *     properties:
 *       formIdlist:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/FormIdlist'
 *       userIdlist:
 *           type: array
 *           items:
 *              type: integer
 *       startDate:
 *         type: dateTime
 *       endDate:
 *         type: dateTime
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         type: integer
 *       createdBy:
 *         type: integer
 *       id:
 *         type: integer
 */


/**
 * @swagger
 * /activityRequestParameter:
 *   post:
 *     tags: [Reports]
 *     description: Creates a new activityRequestParameter
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: activityRequestParameter
 *         description: ActivityRequestParameter object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/ActivityRequestParameter'
 *     responses:
 *       201:
 *         description: Successfully created
 */

module.exports = route.router;