'use strict'
/**
 * @name resources-schema
 * @author shoaib ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var any = types.any;
var string = types.string;

var schema = {
  formIdlist : any,
  userIdlist : any,
  startDate : any,
  endDate : any,
  status : any,
  timeZone: string.label('Time Zone')
};

module.exports = schema;
