(function () {
    'use strict';
    var jwt = require('jsonwebtoken');
    var config = require('../../../config/config.js');
    var controller = function () { };
    var tokenValidatorService = require("../../../lib/utilities/validateToken");
    var tokenUserService = require("../../../lib/utilities/tokenUser");
    function tokenValidator(req, res, next) {
        var xToken = req.headers['x-authorization'] || req.query['x-authorization'];
        if (xToken) { 
            tokenValidatorService.validateToken(xToken).then(function(token){
                tokenUserService.getTokenUser(token).then(function(userData){
                    res.json(userData);
                }, function(err){
                    sendErrorResponse(res);
                })
            },function(error){
                sendErrorResponse(res);
            })
        }
    }

    function sendErrorResponse(res){
        res.status(403).json({
          message: 'You are not authorized.'
        });
    }

    function vc4TokenValidate(req, res, next){
        var token = req.headers['x-vc4-token'];
        jwt.verify(token, config.vcscret, function (err, decoded) {
            if (err) {
                return res.status(401).json({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                req.userId = decoded._id;
                req.username = decoded.username;
                next();
            }
        })
    }

    controller.prototype.tokenValidator = tokenValidator;
    controller.prototype.vc4TokenValidate = vc4TokenValidate;
    module.exports = new controller();

})();