(function () {
    'use strict';

    var controller = function () { };
    var db = require('../../../lib/db');
    var _ = require('lodash');
    function getRole(req, res, next) {
            if(req.query && req.query.query != 'undefined'){
                console.log(req.query);
                var query = JSON.parse(req.query.query);
                if (query.where) {
                    for(var key in query.where){
                        if(query.where[key] && query.where[key].type == 'regex'){
                            var data = query.where[key];
                            //queryObject.where[key] = {};
                            query.where[key] =new RegExp(data.value,'i')
                        }
                    }
                    req.body.where = query.where;
                }
            }
        
        db.roles.count(req.body.where,function (err, count) {
            if (err) {
                console.log(err);
                res.status(500).json(err);
            } else {
                db.roles.
                    find(req.body.where,req.body.select)
                    .sort(req.body.sort)
                    .skip(req.body.pageskip)
                    .limit(req.body.pagesize)
                    .toArray(function (err, data) {
                        res.status(200).send({
                            data: data,
                            count: count
                        });
                    })
            }
        })

    }

    controller.prototype.getRole = getRole;

    module.exports = new controller();

})();