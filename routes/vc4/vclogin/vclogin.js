(function () {
    'use strict';

    var controller = function () { };
    var db = require('../../../lib/db');
    var crypto = require('crypto');
    var jwt = require('jsonwebtoken');
    var _ = require('lodash');
    var config = require('../../../config/config.js');
    function login(req, res, next) {
        db.users.findOne({
            username: req.body.username.toLowerCase(),
            password: crypto.createHash('sha1').update(req.body.password).digest('hex')
        }, function (err, data) {
            if (err) {
                res.status(500).json(err);
            } else {
                if (data) {
                    var roleArray = [];
                    (data.roles || []).forEach(function(role){
                        roleArray.push(db.ObjectID(role._id))
                    })
                    db.roles.find({_id:{$in:roleArray},isActive:true},{title:1,_id:1}).toArray(function(err,roledata){
                        if(err){
                            res.status(401).json({ 'message': 'Username and/or Password do not match with our records.' });
                        } else {
                            console.log(roledata);
                            data.roles = roledata;
                            var tokenData = _.pick(data, ['_id','username']);
                            var token = jwt.sign(tokenData, config.vcscret,{
                                expiresIn: 86400
                            });
                            res.status(200).json({ data: data, token: token });
                        }
                    })
                   
                } else {
                    res.status(401).json({ 'message': 'Username and/or Password do not match with our records.' });
                }
            }
        })
    }

    controller.prototype.login = login;

    module.exports = new controller();

})();
