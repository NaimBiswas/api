'use strict'

var express = require('express');
var router = express.Router();
var fs = require('fs-extra');
var db = require('../../lib/db');
var _ = require('lodash');
var moment = require('moment');
var logger = require('../../logger');
var currentFileName = __filename;
var debug = require('../../lib/logger').debug('routes:upload');

//Get file
router.get('/:fetchKeyAttributeFilterSchemes', function(req, res, next) {});

//Post file
var attribCollection = [];
var collectionName = '';
var filter = {};
var etyKeyAttribRecords = [];
router.post('/', function(req, res) {
  logger.log("API Called : api/fetchKeyAttributeFilterSchemes :POST method" , 'info', currentFileName);

  try {
    // if (req.body.isMaster === 'apps') {
    //   collectionName = 'schema';
    //   filter =  {title:1, attributes:1, isActive:1, entities:1};
    // } else {
    //   collectionName = 'masterqcsettings';
    //   filter =  {title:1, attributes:1, isActive:1, schema:1};
    // }
    // var allAppPromises= [];
    // setAttrbsAll(req.body.apps, 0, res);
     if (req.body.isMaster === 'apps') {
       collectionName = 'schema';
      //  QC3-10437 by Yamuna
       var isFromWindow = !!req.body.isFromWindow;
        setAttrbsEntityAll(collectionName,req.body.apps,isFromWindow,res)
     }
     else {
      collectionName = 'masterqcsettings';
       setAttrbsEntityAll(collectionName,req.body.apps, !!req.body.isFromWindow,res)
    }

  } catch (e) {
    logger.log("catch: API Called : api/fetchKeyAttributeFilterSchemes :failed : " + e , 'error', currentFileName);
    res.status(500).json("'fetchKeyAttributeFilterSchemes' for pirticular schema/master '_id' = "+req.body._id+" :: " + e);
  }
});

function setAttrbsEntityAll(type,apps,isFrom,res) {
  logger.log("function setAttrbsEntityAll(type,apps,res) :Called" , 'info', currentFileName);

 //the function is comming from MongoStoredProcedures
 var appArray=apps.join(':');
  db.eval('db.loadServerScripts();getInitSearchData("' + type + '","' + appArray + '","' + isFrom + '")', function (er, doc) {
    if (er) {
      logger.log("if (er) : collection:db.loadServerScripts() -failed : " + er , 'error', currentFileName);
      logger.log("if (er) : collection:db.loadServerScripts();getInitSearchData(type,appArray) -failed" , 'error', currentFileName);
      return;
    }
    res.json(doc);
  });
}
// function setAttrbsAll(allAppsIds, idx, res) {
//   if (allAppsIds.length > idx) {
//     db.collection(collectionName)
//     .find({_id:db.ObjectID(allAppsIds[idx].id)},filter)
//     .toArray(function (err, data) {
//       if (err) {
//         idx++;
//         setAttrbsAll(allAppsIds, idx);
//       } else {
//         _.forEach(data[0].attributes, function(obj){
//           attribCollection.push(obj)
//         });
//         if (data[0].entities && data[0].entities.length && data[0].entities.length > 0) {
//           _.forEach(data[0].entities, function(obj){
//             var keyObj = _.filter(obj.questions, 'isKeyValue')[0];
//             attribCollection.push(keyObj);
//           })
//         }
//         setAttrbsAll(allAppsIds, ++idx, res);
//       }
//     })
//   }else {
//     attribCollection = _.uniqBy(attribCollection, 'title')
//     res.send({keyAttribsCollection: attribCollection});
//   }
// }

function getEntityRecord() {

}


module.exports = router;
