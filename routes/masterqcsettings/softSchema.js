'use strict'
/**
 * @name masterqcsettings-softSchema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var array = types.array;
var id = types.id;
var string = types.string;
var bool = types.bool;
var rSmallString = types.smallString;
var optionalArray = types.optionalArray;
var required = types.required;
var rNumber = types.rNumber;
var number = types.number;

var schema = {
  title: string.label('qc settings title'),
  module: object({
    _id: id.label('module id'),
    title: string.label('module title')
  }).label('Schedule object'),
  attributes : optionalArray(object({
    id : id.label('attribute id')
  })).label('attribute object'),
  qcform: optionalArray(object({
    id : id.label('attribute id')
  })).label('attribute object'),
  schema: optionalArray(object({
    _id: id.label('Schema id'),
    title: string.label('Schema title'),
    isActive: bool.label('Schema active?'),
    hideQCSingle: bool.label('Schema active?')
  })).label('schema object'),
  hideSites: bool.label('Hide Sites?'),
  addSites: bool.label('Sites added?'),
  allicableSites: optionalArray(
    optionalArray(object({
      _id: id.label('AllicableSites schema id'),
      title: string.label('AllicableSites schema title')
    }))
  ).label('Allicable sites array')
    .when('addSites', {is: true, then: required()}),
  isAutoFill: bool.label('QC Settings isAutoFill?'),
  isActive: bool.label('QC Settings active?'),
  qcEntriesStatus: number.label('Entries Status'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
