'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var dbConfig = require('../../lib/db/db');
var debug = require('../../lib/logger').debug('routes:schema');
var masterQCEntries = require('../masterQCEntries');
var _ = require('lodash');
var db = require('../../lib/db');
var linkedTbl = require('../../lib/linkedTbl');
var logger = require('../../logger');
var currentFileName = __filename;
//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'masterqcsettings',
    collection: 'masterqcsettings',
    schema: schema,
    softSchema: softSchema
  },
  types: {

    /**
    * @swagger
    * definition:
    *   Schema:
    *     properties:
    *       id:
    *         type: integer
    *       title:
    *         type: string
    *       isActive:
    *         type: boolean
    *       hideQCSingle:
    *         type: boolean
    */

    /**
     * @swagger
     * definition:
     *   Attribute1:
     *     properties:
     *       id:
     *         type: integer
     */

    /**
     * @swagger
     * definition:
     *   Allicablesite:
     *     properties:
     *       id:
     *         type: integer
     *       title:
     *         type: string
     */


    /**
     * @swagger
     * definition:
     *   Masterapp:
     *     properties:
     *       id:
     *         type: integer
     *       title:
     *         type: string
     *       schema:
     *         $ref: '#/definitions/Schema'
     *       isActive:
     *         type: boolean
     *       isAutoFill:
     *         type: boolean
     *       addSites:
     *         type: boolean
     *       hideSites:
     *         type: boolean
     *       attributes:
     *           type: array
     *           items:
     *              type: object
     *              allOf:
     *              - $ref: '#/definitions/Attribute1'
     *       qcform:
     *           type: array
     *           items:
     *              type: object
     *              allOf:
     *              - $ref: '#/definitions/Qcform'
     *       allicableSites:
     *           type: array
     *           items:
     *              type: object
     *              allOf:
     *              - $ref: '#/definitions/Allicablesite'
     *       module:
     *         $ref: '#/definitions/Module'
     *       modifiedDate:
     *         type: dateTime
     *       createdDate:
     *         type: dateTime
     *       createdByName:
     *         type: string
     *       modifiedByName:
     *         type: string
     *       modifiedBy:
     *         $ref: '#/definitions/ModifiedBy'
     *       createdBy:
     *         $ref: '#/definitions/CreatedBy'
     *       versions:
     *           type: array
     *           items:
     *              type: object
     *              allOf:
     *              - $ref: '#/definitions/Version'
     *       version:
     *           type: integer
     *       isMajorVersion:
     *           type: boolean
     */

    /**
     * @swagger
     * /masterqcsettings:
     *   get:
     *     tags: [Master Settings]
     *     description: Returns all master apps
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: page
     *         description: Page Number
     *         in: query
     *         required: false
     *         type: string
     *       - name: pagesize
     *         description: Page Size
     *         in: query
     *         required: false
     *         type: string
     *       - name: select
     *         description: Mention comma seperated names of properties of role that you want to see in the output (Exp Values -> [ index,title,module.title,modifiedDate,modifiedBy,isActive])
     *         in: query
     *         required: false
     *         type: string
     *       - name: sort
     *         description: Mention comma seperated names of properties of role with which you want to sort output (Exp Values -> [ -modifiedDate])
     *         in: query
     *         required: false
     *         type: string
     *     responses:
     *       200:
     *         description: An array of master apps
     *         schema:
     *           $ref: '#/definitions/Masterapp'
     */

    /**
     * @swagger
     * /masterqcsettings/{id}:
     *   get:
     *     tags: [Master Settings]
     *     description: Returns a object masterapp
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: Masterapp's id
     *         in: path
     *         required: true
     *         type: integer
     *       - name: pagesize
     *         description: Page Size
     *         in: query
     *         required: false
     *         type: string
     *     responses:
     *       200:
     *         description: An object of masterapp
     *         schema:
     *           $ref: '#/definitions/Masterapp'

     */

    /**
     * @swagger
     * /masterqcsettings:
     *   post:
     *     tags: [Master Settings]
     *     description: Creates a new Master app
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: Master app
     *         description: Masterapp object
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/Masterapp'
     *     responses:
     *       201:
     *         description: Successfully created
     */

    /**
     * @swagger
     * /masterqcsettings/{id}:
     *   patch:
     *     tags: [Master Settings]
     *     description: Updates a masterapp
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: Masterapp's id
     *         in: path
     *         required: true
     *         type: integer
     *       - name: masterapp
     *         description: masterapp object
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/Masterapp'
     *     responses:
     *       200:
     *         description: Successfully updated
     */
    POST: {
      ONE: {
        after: afterPost
      }
    },
    PATCH: {
      ONE: {
        after: afterPatch
      }
    }
  }
});

function afterPost(req, res, next) {
  logger.log("function afterPost(req, res, next) :Called" , 'info', currentFileName);

  //req.data contains the posted / patched
  //schedule object with createdBy and modifiedBy
  //And all, orignal object can be accessed from
  //req.items
  var entities = [];
  entities.push({
    collection: req.data._id.toString('hex'),
    audit: true,
    index: {
      title: ''
    }
  });

  dbConfig.configure.apply(this, entities);
  debug('New route add for :: ' + req.data._id);

  masterQCEntries.use('/' + req.data._id.toString('hex'), require('../masterQCEntries/masterQCEntries')(req.data._id.toString('hex')));

  linkedTbl.insertInLinked('masterqcsettings', req.data);
  res.json(req.data);
}

function afterPatch(req, res, next) {
  logger.log("function afterPatch(req, res, next) :Called" , 'info', currentFileName);
  linkedTbl.insertInLinked('masterqcsettings', req.data);
  res.json(req.data);
}

module.exports = route.router;
