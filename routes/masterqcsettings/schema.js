'use strict'
/**
 * @name masterqcsettings-schema
 * @author Kajal Patel <kajal.p@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var array = types.array;
var rId = types.rId;
var rString = types.rString;
var rBool = types.rBool;
var bool = types.bool;
var rSmallString = types.rSmallString;
var optionalArray = types.optionalArray;
var required = types.required;
var string = types.string;


var schema = {
  title: rString.label('QC settings title'),
  module: object({
    _id: rId.label('module id'),
    title: rString.label('module title')
  }).required().label('Schedule object'),
  attributes : optionalArray(object({
    id : rId.label('attribute id')
  })).required().label('attribute object'),
  qcform :array(object({
    id : rId.label('attribute id')
  })).required().label('attribute object'),
  schema: array(object({
    _id: rId.label('Schema id'),
    title: rString.label('Schema title'),
    isActive: rBool.label('Schema active?'),
    hideQCSingle: bool.label('Schema active?')
  })).required().label('schema object'),
  hideSites: rBool.label('Hide Sites?'),
  addSites: rBool.label('Sites added?'),
  allicableSites:optionalArray(
    array(object({
      _id: rId.label('AllicableSites schema id'),
      title: rString.label('AllicableSites schema title')
    }))
  ).label('Allicable sites array')
    .when('addSites', {is: true, then: required()}),
  isAutoFill: rBool.label('QC Settings isAutoFill?'),
  isActive: rBool.label('QC Settings active?'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
