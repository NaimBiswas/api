'use strict'
var types = require('../../lib/validator/types');
var db = require('../../lib/db');
var tdb = require('../../lib/db/tdb.js');
var restler = require('restler');
var _ = require('lodash');
var config = require('../../config/config.js');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'pluginTempRecords',
    collection: 'pluginTempRecords',
    schema: types.any,
    softSchema: types.any
  }
});

module.exports = route.router;

/**
 * @swagger
 * definition:
 *    FailReason:
 *      properties:
 *        failreason:
 *          type: string
 */

/**
 * @swagger
 * definition:
 *    CreatedByInfo:
 *      properties:
 *        id:
 *          type: string
 *        username:
 *          type: string
 *        firstname:
 *          type: string
 *        lastname:
 *          type: string
 */

/**
 * @swagger
 * definition:
 *    Levels:
 *      properties:
 *        levelId:
 *          type: integer
 *      siteId:
 *          type: string
 *      title:
 *          type: string
 */

/**
 * @swagger
 * definition:
 *    Entries:
 *      properties:
 *        attributes:
 *          type: string
 */

 /**
  * @swagger
  * definition:
  *   Values:
  *     properties:
  *       value:
  *         type: double
  *       seq:
  *         type: string
  */

/**
 * @swagger
 * definition:
 *    PluginTempRecords:
 *      properties:
 *        id:
 *          type: string
 *        reviewStatus:
 *          type: string
 *        entries:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *              - $ref: '#/definitions/Entries'
 *        levels:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *              - $ref: '#/definitions/Levels'
 *        createdDateInfo:
 *          type: dateTime
 *        createdByInfo:
 *          type: array
 *          items:
 *            type: object 
 *            allOf:
 *              - $ref: '#/definitions/CreatedByInfo'
 *        parentVersion:
 *          type: double
 *        salesforceId: 
 *          type: string
 *        createdBy:
 *          type: string
 *        modifiedBy:
 *          type: string
 *        isMajorVersion:
 *          type: string
 *        isAuditing:
 *          type: string
 *        createdByName:
 *          type: string
 *        modifiedByName:
 *          type: string
 *        createdDate:
 *          type: dateTime
 *        modifiedDate:
 *          type: dateTime
 *        failreason:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *              - $ref: '#/definitions/FailReason'
 *        values:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *              - $ref: '#/definitions/Values'
 *        syncId:
 *          type: double
 *        isDeleted:
 *          type: boolean
 *        mappingId:
 *          type: string
 *        type:
 *          type: string
 */

/**
 * @swagger
 * /pluginTempRecords:
 *  get:
 *    tags: [Administration]
 *    description: List of records
 *    produces:
 *      - application/json
 *    parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -createdDate])
 *         in: query
 *         required: false
 *         type: string
 *    responses:
 *       200:
 *         description: An array of records
 *         schema:
 *           $ref: '#/definitions/PluginTempRecords'
 */