'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'reportPermissionConfiguration',
    collection: 'reportPermissionConfiguration',
    schema: schema,
    softSchema: softSchema
  }
});
/**
 * @swagger
 * definition:
 *   ReportPermissionConfiguration:
 *     properties:
 *       id:
 *         type: integer
 *       isPermissionUpdated:
 *         type: boolean
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       createdBy:
 *         $ref: '#/definitions/CreatedBy'
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       version:
 *           type: integer
 *       isMajorVersion:
 *           type: boolean
 */
/**
 * @swagger
 * /reportPermissionConfiguration:
 *   get:
 *     tags: [Report Permission Configuration]
 *     description: Returns all reportPermissionConfiguration
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of reportPermissionConfiguration
 *         schema:
 *           $ref: '#/definitions/ReportPermissionConfiguration'

 */

 /**
  * @swagger
  * /reportPermissionConfiguration/{id}:
  *   patch:
  *     tags: [Report Permission Configuration]
  *     description: Updates a reportPermissionConfiguration
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: reportPermissionConfiguration id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: reportPermissionConfiguration
  *         description: reportPermissionConfiguration object
  *         in: body
  *         required: true
  *         schema:
  *           $ref: '#/definitions/ReportPermissionConfiguration'
  *     responses:
  *       200:
  *         description: Successfully updated
  */

module.exports = route.router;
