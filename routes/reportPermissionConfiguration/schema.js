'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var bool = types.bool;

var schema = {
  isPermissionUpdated: bool.label('isPermissionUpdated')
};

module.exports = schema;
