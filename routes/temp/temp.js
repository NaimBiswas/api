'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'temp',
    collection: 'temp',
    schema: schema,
    softSchema: softSchema
  }
});


module.exports = route.router;
