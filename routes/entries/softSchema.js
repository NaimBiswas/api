'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');
var joi = require('joi');

var object = types.object.bind(types);
var id = types.id;
var string = types.string;
var number = types.number;
var array = types.array;
var date = types.date;
var bool = types.bool;
var any = types.any;
var bigString = types.bigString;
var extraLargeString = types.extraLargeString;

var schema = object({
  entries: any,
  attrValue: any,
  batchEntries: any,
  batchFormat: any,
  draftStatus: number.label('Save status info on save as draft'),
  createdDateInfo: date.label('Created Date'),
  createdByInfo: object({
    _id: id.label('Created by user id'),
    firstname: string.label('Created By firstname'),
    lastname: string.label('Created By lastname'),
    username: string.label('Created By username')
  }).label('Created by'),
  levelIds: joi.array().label('Sites ids'),
  levels: array(object({
    levelId: number.label('Leval id'),
    siteId: id.label('Site id'),
    title: string.label('Site title'),
    parentId: string.label('Parent Id')
  })).label('Level object'),
  reviewStatus: string.label('Review status'),
  verificationCode: string.label('Verification Code'),
  verificationDate: date.label('Verification Date'),
  verifiedBy: string.label('Verified By').allow(''),
  parentVersion: number.label('parent schema version number'),
  schemaID: any,
  appId: any,
  eventSaveAndAccept: any,
  comment : bigString.label('Comment'),
  workflowreview : any,
  workflowReviewHistory: any,
  masterQCSettings: object({
    masterQCSettingsId: id.label('Master QC Settings Id'),
    title: string.label('title'),
    schemaId: id.label('Schema Id'),
    sequenceId: string.label('s# Id'),
    qcEntriesStatus: number.label('QC Entries Status')
  }),
  calendarIds : joi.array().label('calendar ids'),
  calendarverifieduser : joi.array().label('calendar ids'),
  importId : bigString.label('Import Id'),
  csv : any.label('csv'),
  parent : any,
  child: any,
  isOuterApp : bool.label("Is Auditing ?"),
  fromToken : extraLargeString.label('Form Toekn'),
  fromMail : bigString.label('Form Email').allow(''),
  parentprocInitiate : any,
  childprocInitiate : any,
  isAuditing: bool.label("Is Auditing ?"),
  //Start: QC3-6081 - Group Settings > Master Apps of blood are displayed when only Equipment module is selected
  //Changed By: Jyotil
  //Description: code to change audit trail data title.
  batchEntryId: bigString.label('Batch Entry Id'),
  //End: QC3-6081 - Group Settings > Master Apps of blood are displayed when only Equipment module is selected
  displayAssignee: any,
  assignment: any,
  timeZone: string.label('Time Zone'),
  isChild: string.label('Is child'),
  isRejected: bool.label("Is Rejected ?"),
  rejectedObj: any,
  salesforceId : any,
  lastUpdated : any,
  liteLogs : any
});

module.exports = schema;