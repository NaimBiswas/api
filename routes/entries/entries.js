// by surjeet.b@productivet.com

'use strict'
var colors = require('colors');
var batchEntryService = require('../../lib/batchEntryService/index');
var schema = require('./schema');
var softSchema = require('./softSchema');
var db = require('../../lib/db');
var entriesUtiliese = require('../../lib/utilities/entriesUtiliese');
var config = require('../../config/config.js');
var crypto = require('crypto');
var _ = require('lodash');
var mailer = require('../../lib/mailer');
var settings = require('../../lib/settings');
var moment = require('moment');
var log = require('../../logger');
var currentFileName = __filename;
var q = require('q');
var restler = require('restler');
var machineDate = new Date();
var machineoffset = machineDate.getTimezoneOffset();
var clientOffset = machineDate.getTimezoneOffset();
var mcdiff = 0;
var dbConfig = require('../../lib/db/db');
const shortid = require('shortid');


//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST

/**
* @swagger
* /entries/{id}:
*   get:
*     tags: [Advanced Search]
*     description: Returns all entries
*     produces:
*       - application/json
*     parameters:
*       - name: id
*         description: Schema Id
*         in: query
*         required: true
*         type: string
*       - name: pagesize
*         description: Page Size
*         in: query
*         required: false
*         type: string
*       - name: q[levelIds]
*         description: Level Ids
*         in: query
*         required: false
*         type: string
*       - name: q[entries.xyz]
*         description: search using attribute value where xyz will be attribute value
*         in: query
*         required: false
*         type: string
*       - name: sort
*         description: Mention comma seperated names of properties of entries with which you want to sort output (Exp Values -> [ -createdDate])
*         in: query
*         required: false
*         type: string
*     responses:
*       200:
*         description: An array of entries
*         schema:
*           type: array
*           items:
*              $ref: '#/definitions/Entry'
*/

var tempSchemaID = '';
var setArray = [];
var logData = {};

function configure(name) {
  var route = require('../../lib/routeBuilder').build({
    entity: {
      name: name,
      collection: name,
      schema: schema,
      softSchema: softSchema
    },
    types: {

      /**
      * @swagger
      * definition:
      *   Level:
      *     properties:
      *       levelId:
      *         type: integer
      *       siteId:
      *         type: string
      *       title:
      *         type: string

      */


      /**
      * @swagger
      * definition:
      *   Entry:
      *     properties:
      *       calendarIds:
      *           type: array
      *           items:
      *              type: string
      *       verificationDate:
      *         type: dateTime
      *       workflowreview:
      *         $ref: '#/definitions/Workflowreview'
      *       parentVersion:
      *         type: integer
      *       createdByInfo:
      *         $ref: '#/definitions/CreatedBy'
      *       entries:
      *         type: object
      *       levelIds:
      *           type: array
      *           items:
      *              type: string
      *       reviewStatus:
      *         type: string
      *       schemaID:
      *         type: string
      *       levels:
      *           type: array
      *           items:
      *              type: object
      *              allOf:
      *              - $ref: '#/definitions/Level'

      */

      /**
      * @swagger
      * /entries/{id}:
      *   post:
      *     tags: [Schedule]
      *     description: Creates a new entry for schedule
      *     produces:
      *       - application/json
      *     parameters:
      *       - name: id
      *         description: Schema Id
      *         in: query
      *         required: true
      *         type: string
      *       - name: entry
      *         description: entry object
      *         in: body
      *         required: true
      *         schema:
      *           $ref: '#/definitions/Entry'
      *     responses:
      *       201:
      *         description: Successfully created
      */

      GET: {
        ONE: {
          after: afterGet,
        }
      },
      POST: {
        ONE: {
          before: defaultPost,
          after: afterPost
        },
        ONESOFT: {
          before: beforePostForSaveDraft,
          after: afterPost
        }
      },
      /**
      * @swagger
      * /entries/{schemaid}/{entryid}:
      *   post:
      *     tags: [Schedule]
      *     description: Creates a new entry for schedule
      *     produces:
      *       - application/json
      *     parameters:
      *       - name: schemaid
      *         description: Schema Id
      *         in: query
      *         required: true
      *         type: string
      *       - name: entryid
      *         description: Entry Id
      *         in: query
      *         required: true
      *         type: string
      *       - name: entry
      *         description: entry object
      *         in: body
      *         required: true
      *         schema:
      *           $ref: '#/definitions/Entry'
      *     responses:
      *       201:
      *         description: Successfully created
      */
      PATCH: {
        ONE: {
          before: defaultPatch,
          after: afterPost
        },
        ONESOFT: {
          before: beforePatchForSaveDraft,
          after: afterPost
        }
      }
    }
  });

  function afterGet(req, res, next) {
    if (req.data && req.data.liteLogs && req.data.liteLogs.length) {
      //req.data.liteLogs = req.data.liteLogs.reverse();
    }
    next();
  }

  function removeIsChild(req) {
    req.isChild = req.body.isChild;
    delete req.body.isChild;
  }

  // PQT-994: Mahammad
  function removeCommentIfNull(req) {
    if (req.items.workflowreview && req.items.workflowreview.length) {
      req.items.workflowreview.forEach(function (item) {
        if (!item.comment || (item.comment && !item.comment.trim())) {
          delete item.comment;
        }

        (item.reviews || []).forEach(function (obj) {
          if (!obj.comment || (obj.comment && !obj.comment.trim())) {
            delete obj.comment;
          }
        });
      });
    }

    if (req.items.assignment && (!req.items.assignment.comments || (req.items.assignment.comments && !req.items.assignment.comments.trim()))) {
      delete req.items.assignment.comments;
    }

    // if (req.items.assignment && req.items.assignment.length) {
    //   req.items.assignment.forEach(function (item) {
    //     if (!item.comments || (item.comments && !item.comments.trim())) {
    //       delete item.comments;
    //     }
    //   });
    // }
  }

  function afterPost(req, res, next) {
    //req.data contains the posted / patched
    //And all, orignal object can be accessed from
    //req.items
    if (req.data) {
      if (req.data.isRejected) {
        var userIds = (req.data.assignment.assignee || []).reduce(function (result, val) {
          result.push(db.ObjectID(val.id));
          return result;
        }, []);
        db.users.find({ _id: { $in: userIds } }, { _id: 1, email: 1, username: 1, firstname: 1, lastname: 1 }).toArray(function (err, userdata) {
          (userdata || []).forEach(function (value) {
            if (!req.data.rejectedObj.comments) {
              req.data.rejectedObj.comments = req.data.rejectedObj.comment;
            }
            sendRejectMail(value, req.data.rejectedObj, req.data.assignment, req.data.schemaID);
          });
        });
      }
      updateschedule(req, res);
      saveAutoBatchCorrectData(req, res);
      sendDatatoWebhook(req, res);
    }
  }

  //PQT-15 Prachi - Start
  //Update reviewpendingworkflow collection, add array of review pending users
  function updatereviewPendingUsers(_id, reviewobj) {
    if (reviewobj) {
      //Create array signedUsers for getting usernames from setworkflowuser, whose review is done
      var signedUsers = _.reduce(reviewobj.reviews, function (uResut, review) {
        var getUserName = review.fullName.substring(review.fullName.lastIndexOf("(") + 1, review.fullName.lastIndexOf(")"));
        if (reviewobj.setworkflowuser && reviewobj.setworkflowuser.includes(getUserName) && review.status === 0) {
          uResut.push(getUserName);
        }
        return uResut;
      }, [])

      var reviewPendingUsers = []
      if (signedUsers.length) {
        reviewPendingUsers = reviewobj.setworkflowuser.filter(function (set) {
          return signedUsers.indexOf(set) == -1;
        });
      } else {
        reviewPendingUsers = reviewobj.setworkflowuser;
      }
      var key = 'reviewworkflow.reviewPendingUsers';
      db.reviewpendingworkflow.update({ 'entryId': db.ObjectID(_id) },
        { $set: { [key]: reviewPendingUsers } });
    }
  }
  //PQT-15 Prachi - End

  // PQT-4578 Sunny - Start

  function sendDatatoWebhook(req, res) {
    var objToBeSent = {};
    db.schema.find({ _id: db.ObjectID(req.data.appId) }).toArray(function (err, appSchema) {
      if (appSchema && appSchema[0] && appSchema[0].webhookEnabled) {
        try {
        if (appSchema[0].attributes && appSchema[0].attributes.length > 0) {
          _.forEach(appSchema[0].attributes, function (attb) {
            objToBeSent[attb.title] = modifyValueTobeSent(attb.type, req.data.entries[attb['s#']].attributes);
          });
        }

        if (appSchema[0].procedures && appSchema[0].procedures.length > 0) {
          _.forEach(appSchema[0].procedures, function (proc) {
            if (proc.questions && proc.questions.length > 0) {
              _.forEach(proc.questions, function (ques) {
                objToBeSent[ques.title] = modifyValueTobeSent(ques.type, req.data.entries[ques['s#'] + '-' + proc['s#']][proc['s#']]);
              });
            }
          });
        }

        if (appSchema[0].entities && appSchema[0].entities.length > 0) {
          _.forEach(appSchema[0].entities, function (ent) {
            if (ent.questions && ent.questions.length > 0) {
              _.forEach(ent.questions, function (quest) {
                objToBeSent[quest.title] = modifyValueTobeSent(quest.type, req.data.entries[quest['s#'] + '-' + ent['s#']][ent['s#']]);
              });
            }
          });
        }

        logData['shortId'] = objToBeSent['shortUniqueId'] = shortid.generate();
        objToBeSent['timeZoneforConversion'] = config.timeZonesP[config.currentTimeZone];
        logData['requestSentTime'] = new Date();
        logData['webhookUrl'] = appSchema[0].webhookUrl;
        logData['status'] = 'Request Sent';

        db.webhookLogs.insert(logData);
        restler.postJson(appSchema[0].webhookUrl, objToBeSent)
          .on('success', function (cmplt) {
            cmplt = JSON.parse(cmplt);
            if (cmplt && cmplt.shortUniqueId) {
              db.webhookLogs.findOneAndUpdate({'shortId': cmplt.shortUniqueId}, { $set: {'status': 'Request Received'}});
            }
          })
          .on('timeout', function () {
            console.log('timeout');
          })
          .on('error', function (err) {
            console.log('-----');
            console.log(err);
            db.webhookLogs.findOneAndUpdate({'shortId': logData['shortId']}, { $set: {'status': err}})
          });
      } catch (e) {
        var logObject = {};
        logObject['shortId'] = shortid.generate();
        logObject['webhookUrl'] = appSchema[0].webhookUrl;
        logObject['timeOfError'] = new Date();
        logObject['status'] = e.toString();
        db.webhookLogs.insert(logObject);
      }
    } 
    });
  }

  function modifyValueTobeSent(type, value) {
    switch (type.title) {
      case 'Textbox':
        return value;

      case 'Textarea':
        return value;

      case 'File Attachment':
        return modifyFileAttachmentData(type, value);

      case 'Checkbox':
        return value;

      case 'Dropdown':
        return value;

      case 'Auto Calculated':
        return value;

      case 'Auto Generate':
        return value;

      case 'Dropdown List':
        return value;

      default:
        return value;
    }

  }

  function modifyFileAttachmentData(quesType, actualValue) {
    var files = [];
    _.forEach(actualValue, function (file) {
      if (file) {
        file = ifTimeStampInFilename(file.name) ? file.name.substring(14) : file.name;
        files.push(file);
      }
    });
    return files.join(', ');
  }

  function ifTimeStampInFilename(data) {
    if (data) {
      var filename = _.split(data, '-');
      if (filename && filename.length > 1 && (parseInt(filename[0])).toString() != 'NaN' && parseInt(data).toString()) {
        return (filename[0].length == 13 && parseInt(filename[0])).toString().length === (parseInt(data)).toString().length;
      } else {
        return false;
      }
    }
  }

  function sendRejectMail(user, rejectedObj, assignment, schemaTitle) {
    var mailObj = {
      email: user.email,
      schemaTitle: schemaTitle,
      comments: rejectedObj.comments,
      assignedBy: assignment.assignedBy,
      assignee: {
        firstname: user.firstname,
        lastname: user.lastname,
        username: user.username,
      },
      appRecordUrl: assignment.appRecordUrl
    }
    mailer.send('reject-notify', mailObj, mailObj.email, function sendMailCallback(e, b) {
      if (e) {
        debug.err('Failed sending email - reject-notify');
        debug.err(mailObj);
        debug.err(e);
      } else {
        debug('reject-notify Status has sent to ' + mailObj.emai);
      }
    });
  }

  function saveAutoBatchCorrectData(req, res) {
    if (req.data && req.data.masterQCSettings && req.data.batchEntryId && req.method == 'POST') {
      var obj = {
        entryId: req.data._id,
        masterQCSettings: req.data.masterQCSettings,
        batchEntryId: req.data.batchEntryId,
        calendarIds: req.data.calendarIds || []
      };
      db.autoBatchCorrectData.insert(obj);
    }
  }

  var debug = route.debug;
  function updateSharedAppData(req, res, cb) {
    var filter = { token: req.items.fromToken };
    if (req.items.fromMail) {
      filter['emails.email'] = req.items.fromMail;
    }
    db['appAccessConfig'].findOne(filter, function (e, sharedApp) {
      if (e) {
        cb(e);
        return;
      }
      if (sharedApp) {
        if (!sharedApp.isActive) {
          cb("Invalid App, You are not allow to submit more entries.", null);
        } else {

          var mail = _.find((sharedApp.emails || []), { email: req.items.fromMail });
          var totalAllowed = _.sumBy((sharedApp.emails || []), 'allowed') || sharedApp.noOfTimeAllowed;
          var email = req.items.fromMail;
          //Validate expiration of date for less than today
          if (sharedApp.expireDate && moment().isAfter(sharedApp.expireDate, 'day')) {
            cb("App submitting date is expired, You are not allow to submit more entries.", null);
          } else {

            if (mail && (mail.linkShared == false || mail.isDeleted)) {
              cb("Invalid email, You are not allow to submit more entries.", null);
            } else if (mail && sharedApp.isForPerEmail) {
              if (mail.used >= mail.allowed) {
                //  res.status(400).json({error: 'App submitting limit is crossed, You are not allow to submit more entries.'});
                cb("App submitting limit is crossed, You are not allow to submit more entries.", null);
              }
              _.forEach((sharedApp.emails || []), function (mail) {
                if (mail.email == req.items.fromMail) {
                  mail.used = parseInt((mail.used || 0)) + 1;
                }
              });
            } else {
              if (((!email || !sharedApp.isForPerEmail) && sharedApp.noOfTimeUsed >= sharedApp.noOfTimeAllowed) || ((!email || sharedApp.isForPerEmail) && sharedApp.noOfTimeUsed >= totalAllowed)) {
                //  if ((!req.items.fromMail || !sharedApp.isForPerEmail) && sharedApp.noOfTimeUsed>=sharedApp.noOfTimeAllowed) {
                //    res.status(400).json({error: 'App submitting limit is crossed, You are not allow to submit more entries.'});
                cb("App submitting limit is crossed, You are not allow to submit more entries.", null);
              }

            }

            sharedApp.noOfTimeUsed = parseInt((sharedApp.noOfTimeUsed || 0)) + 1;
            var id = sharedApp._id;
            db.appAccessConfig.findOneAndUpdate({
              _id: id
            }, {
                $set: {
                  emails: sharedApp.emails,
                  noOfTimeUsed: sharedApp.noOfTimeUsed
                }
              }, function (err, data) {
                if (err) {
                  cb(err);
                  return;
                }
                cb(null, data);
              });
          }
        }

      } else {
        cb(null, res.items);
      }
    });

  }

  function checkSharedAppDataValidity(req, res, cb) {
    var filter = { token: req.items.fromToken };
    if (req.items.fromMail) {
      filter['emails.email'] = req.items.fromMail;
    }

    db['appAccessConfig'].findOne(filter, function (e, sharedApp) {
      if (e) {
        cb(e);
        return;
      }
      if (sharedApp) {
        if (!sharedApp.isActive) {
          cb("Invalid App, You are not allow to submit more entries.", null);
        } else {

          var mail = _.find((sharedApp.emails || []), { email: req.items.fromMail });
          var totalAllowed = _.sumBy((sharedApp.emails || []), 'allowed') || sharedApp.noOfTimeAllowed;
          var email = req.items.fromMail;
          if (sharedApp.expireDate && new Date(sharedApp.expireDate) < new Date()) {
            cb("App submitting date is expired, You are not allow to submit more entries.", null);
          } else {

            if (mail && (mail.linkShared == false || mail.isDeleted)) {
              cb("Invalid email, You are not allow to submit more entries.", null);
            } else if (mail && sharedApp.isForPerEmail) {
              if (mail.used >= mail.allowed) {
                //  res.status(400).json({error: 'App submitting limit is crossed, You are not allow to submit more entries.'});
                cb("App submitting limit is crossed, You are not allow to submit more entries.", null);
              }
              _.forEach((sharedApp.emails || []), function (mail) {
                if (mail.email == req.items.fromMail) {
                  mail.used = parseInt((mail.used || 0)) + 1;
                }
              });
            } else {
              if (((!email || !sharedApp.isForPerEmail) && sharedApp.noOfTimeUsed >= sharedApp.noOfTimeAllowed) || ((!email || sharedApp.isForPerEmail) && sharedApp.noOfTimeUsed >= totalAllowed)) {
                //  if ((!req.items.fromMail || !sharedApp.isForPerEmail) && sharedApp.noOfTimeUsed>=sharedApp.noOfTimeAllowed) {
                //    res.status(400).json({error: 'App submitting limit is crossed, You are not allow to submit more entries.'});
                cb("App submitting limit is crossed, You are not allow to submit more entries.", null);
              } else {
                req.body.createdBy = sharedApp.createdBy;
                cb(null, true);
              }

            }
          }
        }

      } else {
        cb(null, res.items);
      }
    });
  }

  function forAllSchemaVoid(masterschemaIds, masterappschema, calendarId, res, entryArg) {
    var deffer = q.defer();
    var entryIds = [];
    entryArg.voidSchemas = [];
    //var counter = 0;
    var count = 0;
    var isEntryBlank = false;

    if (masterschemaIds && masterschemaIds.length) {
      masterschemaIds.forEach(function (schemaId) {
        if (_.isUndefined(schemaId)) {
          log.log("Table is not bind with mongoskin DB");
          var entities = [];
          entities.push({
            collection: schemaId,
            audit: true,
            index: {
              title: ''
            }
          });
          log.log("Here for add collection in db object");
          dbConfig.configure.apply(this, entities);
          log.log("Adding collection to DB is Completed");
        }
        db[schemaId + ""].findOne({ 'calendarIds': { $in: [calendarId + ""] }, 'entries.status.status': 3 }, function (err, entryObj) {
          count++;
          if (entryObj) {
            entryIds.push({ schemaId: schemaId, entryId: entryObj._id });
            entryArg.voidSchemas.push({
              schemaId: schemaId,
              modifiedDate: entryObj.modifiedDate,
              modifiedBy: entryObj.modifiedBy,
              modifiedByName: entryObj.modifiedByName
            });
          }
          if (entryIds && entryIds.length && (entryIds.length == masterappschema.length)) {
            isEntryBlank = true;
            entryIds.forEach(function (id, entryIndex) {
              // QC3-9829 by Yamuna
              db[id.schemaId].findOneAndUpdate({ _id: db.ObjectID(id.entryId) }, { $set: { calendarIds: [], calendarverifieduser: [], 'entries.islinkcalendar': false } }, function (err, entryy) {
                if (err) {
                  log.log("error ::" + err)
                }
                if (entryy) {
                  log.log('success to update entry', 'info');
                }
              });
            });

          }
          if (count == masterschemaIds.length) { //QC3-9986 || QC3-9983 - Jyotil
            deffer.resolve(isEntryBlank);
          }
        });

      });
    } else {
      deffer.resolve(false);
    }

    return deffer.promise;
  }

  function updateCalendar(allscheduleIds, entry, req, res) {

    db.calendar.updateMany({ _id: { $in: allscheduleIds } }, { $set: { "entry": entry } }, { multi: true }, function (e, updateddata) {
      if (req.data.workflowreview && req.data.workflowreview.length > 0) {
        generateEntry(req, res);
      } else {
        if (req.status) {
          res.status(req.status).json(req.data);
        } else {
          res.status(200).json(req.data);
        }
      }
    });
  }

  function updateschedule(req, res) {
    if (
      (!req.isChild || (req.isChild && req.isChild != "true")) &&
      req.data.calendarIds && req.data.calendarIds.length > 0 && req.data.calendarIds[0] != null) {
      db.calendar.find({ '_id': db.ObjectID(req.data.calendarIds[0]), $or: [{ 'ismasterQcForm': false, 'schema': { $exists: true }, 'schema._id': db.ObjectID(req.originalUrl.split('/')[2]) }, { 'ismasterQcForm': true }] }) //QC3-9840: by Mahammad
        .toArray(function (err, calendar) {
          if (err) {
            log.log('Error find calendar ::' + err, 'error');
          }
          if (err) {
            log.log('Error find ReviewSchedule ::' + err, 'error');
          }
          var allscheduleIds = _.map(calendar, function (cal) {
            return cal._id;
          });
          var entry = {};
          entry.entryId = req.data._id;
          entry.masterschemaIds = [];
          entry.masterappStatus = [];
          // QC3-9462 by Yamuna
          if (req.data && req.data.entries && req.data.entries.status) {
            entry.recordStatus = req.data.entries.status.status;
          }
          entry.voidSchemas = (calendar && calendar[0] && calendar[0].entry && calendar[0].entry.voidSchemas) ? calendar[0].entry.voidSchemas : []; //QC3-9986 || QC3-9983 - Jyotil
          entry.levelIds = req.data.levelIds; //QC3-9377
          //var iscompletedAppSchedule = false;
          if (calendar[0] && calendar[0].ismasterQcForm) {
            db.masterqcsettings.find({ '_id': db.ObjectID(calendar[0].masterQCSettingsId) }).toArray(function (err, masterapp) {
              if (err) {
                log.log(err, 'error');
              } else {
                if (masterapp[0]) {
                  var masterappschema = _.filter(masterapp[0].schema, function (app) {
                    return app.isActive == true;
                  });
                  entry.workflowreview = {};
                  entry.reviewScheduleStatus = [];
                  entry.calendarverifieduser = {};
                  entry.schemaCompletedDate = {};
                  if (calendar[0].entry && calendar[0].entry.masterschemaIds) {
                    entry.masterschemaIds = calendar[0].entry.masterschemaIds;
                  }
                  if (calendar[0].entry && calendar[0].entry.calendarverifieduser) {
                    entry.calendarverifieduser = calendar[0].entry.calendarverifieduser;
                  }
                  if (calendar[0].entry && calendar[0].entry.schemaCompletedDate) {
                    entry.schemaCompletedDate = calendar[0].entry.schemaCompletedDate;
                  }

                  if (calendar[0].entry && calendar[0].entry.reviewScheduleStatus) {
                    entry.reviewScheduleStatus = calendar[0].entry.reviewScheduleStatus
                  }
                  if (calendar[0].entry && calendar[0].entry.workflowreview) {
                    entry.workflowreview = calendar[0].entry.workflowreview;
                  }
                  // QC3-8468 - overide completed by name with updated user

                  //START :: QC3-9296 - Improvement By : Yamuna.s
                  if (entry.masterschemaIds.indexOf(req.originalUrl.split('/')[2]) < 0) {
                    entry.calendarverifieduser[req.originalUrl.split('/')[2]] = [];
                    entry.schemaCompletedDate[req.originalUrl.split('/')[2]] = [];
                    entry.calendarverifieduser[req.originalUrl.split('/')[2]] = req.data.calendarverifieduser;
                    entry.schemaCompletedDate[req.originalUrl.split('/')[2]] = new Date();
                  }
                  if (entry.calendarverifieduser[req.originalUrl.split('/')[2]]) {
                    entry.calendarverifieduser[req.originalUrl.split('/')[2]] = [];
                    entry.schemaCompletedDate[req.originalUrl.split('/')[2]] = [];
                    entry.calendarverifieduser[req.originalUrl.split('/')[2]] = req.data.calendarverifieduser;
                    entry.schemaCompletedDate[req.originalUrl.split('/')[2]] = new Date();
                  }
                  if (entry.masterschemaIds.indexOf(req.originalUrl.split('/')[2]) < 0) {
                    if (entry.recordStatus != 4) {
                      entry.masterschemaIds.push(req.originalUrl.split('/')[2]);
                    }
                    //END :: QC3-9296 - Improvement By : Yamuna.s

                    entry.workflowreview[req.originalUrl.split('/')[2]] = {};
                    entry.workflowreview[req.originalUrl.split('/')[2]].workflowreview = req.data.workflowreview ? req.data.workflowreview : [];
                  }
                  if (entry.workflowreview[req.originalUrl.split('/')[2]]) {
                    entry.workflowreview[req.originalUrl.split('/')[2]].workflowreview = req.data.workflowreview ? req.data.workflowreview : [];

                  }
                  var index = '';
                  _.forEach(masterappschema, function (schema, indx) {
                    if (schema._id == req.originalUrl.split('/')[2]) {
                      index = indx;
                    }
                    if (req.data.workflowreview && req.data.workflowreview.length > 0) {
                      var ispending = _.find(req.data.workflowreview, { reviewstatus: 1 });
                      //issue solve regarding 'schedule will be not complete if user complete it one by one tab'
                      // QC3-8468 - kajal
                      if (ispending) {
                        // iscompletedAppSchedule = false;
                        entry.reviewScheduleStatus[indx] = 'Pending';
                      } else {
                        entry.reviewScheduleStatus[indx] = 'Completed';
                      }
                    } else {
                      entry.reviewScheduleStatus[indx] = 'Pending';
                    }
                    // end - QC3-8468
                  });

                  //QC3-10089
                  if (entry.masterschemaIds.length == masterappschema.length || (calendar[0] && calendar[0].skippedMasterApps && (Object.keys(calendar[0].skippedMasterApps).length + entry.masterschemaIds.length == masterappschema.length))) {

                    // if(entry.masterschemaIds.length == masterappschema.length){

                    if (entry.recordStatus == 3) {
                      // START :: QC3-9830 by Yamuna.s
                      forAllSchemaVoid(entry.masterschemaIds, masterappschema, calendar[0]._id, res, entry).then(function (data) {
                        if (data) {
                          entry = {};
                        }

                        //QC3-11003: mahammad
                        var allMasterAppIds = _.map(masterapp[0].schema, function (schema) {
                          return schema._id;
                        });

                        var pendingIds = _.difference(allMasterAppIds, entry.masterschemaIds);

                        if (calendar[0].skippedMasterApps) {
                          var allPendingAppsSkipped = !_.some(pendingIds, function (pdapp) {
                            return !calendar[0].skippedMasterApps[pdapp];
                          });

                          if (allPendingAppsSkipped) {
                            entry.schedulestatus = 'Completed';
                            entry.completedDate = new Date();
                          }
                        }
                        updateCalendar(allscheduleIds, entry, req, res);
                      }
                      );
                      // END :: QC3-9830 by Yamuna.s

                    } else {
                      entry.schedulestatus = 'Completed';
                      //Vaibhav - BI-577
                      entry.completedDate = new Date();
                      updateCalendar(allscheduleIds, entry, req, res);
                    }

                  } else {
                    forAllSchemaVoid(entry.masterschemaIds, masterappschema, calendar[0]._id, res, entry).then(function () { //QC3-9986 || QC3-9983 - Jyotil
                      entry.schedulestatus = 'Incomplete';
                      entry.reviewScheduleStatus[index] = 'Pending';
                      updateCalendar(allscheduleIds, entry, req, res);
                    });
                  }
                }
                //END :: QC3-9296 - Improvement By : Yamuna.s

              }
            });
          } else {
            entry.calendarverifieduser = req.data.calendarverifieduser;

            //START :: QC3-9296 - Improvement By : Yamuna.s
            if (entry.recordStatus != 4 && entry.recordStatus != 3) {
              entry.completedDate = new Date();
              entry.schedulestatus = 'Completed';
            } else if (entry.recordStatus == 4) {
              entry.schedulestatus = 'Incomplete';
            } else if (entry.recordStatus == 3) {
              if (_.isUndefined(calendar[0].schema._id)) {
                log.log("Table is not bind with mongoskin DB");
                var entities = [];
                entities.push({
                  collection: calendar[0].schema._id,
                  audit: true,
                  index: {
                    title: ''
                  }
                });
                log.log("Here for add collection in db object");
                dbConfig.configure.apply(this, entities);
                log.log("Adding collection to DB is Completed");
              }
              db[calendar[0].schema._id].findOneAndUpdate({ _id: db.ObjectID(req.data._id) }, { $set: { calendarIds: [], calendarverifieduser: [], 'entries.islinkcalendar': false } }, function (err, entry) {
                if (err) {
                  log.log("error ::" + err)
                }
                if (entry) {
                  res.status(200).send({ "success": true });
                }
              });
              entry = {};
            }
            if (entry) {
              entry.workflowreview = req.data.workflowreview ? req.data.workflowreview : [];
            }
            //END :: QC3-9296 - Improvement By : Yamuna.s

            db.calendar.updateMany({ _id: { $in: allscheduleIds } }, { $set: { "entry": entry } }, function (e, updateddata) {
              if (req.data.workflowreview && req.data.workflowreview.length > 0) {
                generateEntry(req, res);
              } else {
                if (req.status) {
                  res.status(req.status).json(req.data);
                } else {
                  if (req.data.workflowreview) {
                    generateEntry(req, res);
                  } else {
                    res.status(200).json(req.data);
                  }
                }
              }
            });
          }

        });
    } else {
      if (req.data.workflowreview && req.data.workflowreview.length > 0) {
        generateEntry(req, res);
      } else {
        if (req.status) {
          res.status(req.status).json(req.data);
        } else {
          res.status(200).json(req.data);
        }
      }
    }

  }

  //QC3-2767
  //Changed By:Kajal Patel
  function generateEntry(req, res) {
    if (req.data.isRejected) {
      db['reviewpendingworkflow']
        .remove({ 'entryId': db.ObjectID(req.data._id) }, function (err, a) {
          res.status(200).json(req.data)
        });
    } else {
      var qry = {};
      var entry = req.data;
      qry['entryId'] = entry._id;
      var reviewerPendingEntry;
      db['reviewpendingworkflow'].find(qry).toArray(function (err, dt) {
        if (dt.length == 0) {
          reviewerPendingEntry = {
            entryId: '',
            schemaId: '',
            parentSchemaVersion: '',
            levelIds: [],
            verificationDate: '',
            reviewworkflow: {}
          }
          reviewerPendingEntry.entryId = entry._id;
          reviewerPendingEntry.schemaId = req.originalUrl.split('/')[2];
          reviewerPendingEntry.parentSchemaVersion = entry.parentVersion;
          reviewerPendingEntry.levelIds = _.cloneDeep(entry.levelIds);
          reviewerPendingEntry.verificationDate = entry.verificationDate;
          reviewerPendingEntry.createdDate = new Date(entry.createdDate).toISOString();
          if (entry.masterQCSettings) {
            reviewerPendingEntry.masterQCSettings = entry.masterQCSettings;
          }
          var reviewobj = _.find(entry.workflowreview, { reviewstatus: 1 });
          if (reviewobj) {
            reviewerPendingEntry.reviewworkflow = _.cloneDeep(reviewobj);
          }
          if (reviewobj && entry && entry.entries && entry.entries.status && (entry.entries.status.status == 0 || entry.entries.status.status == 1)) {
            db["reviewpendingworkflow"]
              .insert(reviewerPendingEntry, function (err, a) {
                if (req.data.workflowreview && req.data.workflowreview.length) {
                  updatereviewPendingUsers(req.data._id, reviewobj);//PQT-15 Prachi
                }
                res.status(200).json(req.data);
              });
          } else {
            res.status(200).json(req.data);
          }
        } else {
          //var reviewerPendingEntry = dt[0];
          db['reviewpendingworkflow']
            .remove({ _id: db.ObjectID(dt[0]._id) }, function (err, a) {

              reviewerPendingEntry = {
                entryId: '',
                schemaId: '',
                parentSchemaVersion: '',
                levelIds: [],
                verificationDate: '',
                reviewworkflow: {}
              }
              reviewerPendingEntry.entryId = entry._id;
              reviewerPendingEntry.schemaId = req.originalUrl.split('/')[2];
              reviewerPendingEntry.parentSchemaVersion = entry.parentVersion;
              reviewerPendingEntry.levelIds = _.cloneDeep(entry.levelIds);
              reviewerPendingEntry.verificationDate = entry.verificationDate;
              reviewerPendingEntry.createdDate = new Date(entry.createdDate).toISOString();
              if (entry.masterQCSettings) {
                reviewerPendingEntry.masterQCSettings = entry.masterQCSettings;
              }
              var reviewobj = _.find(entry.workflowreview, { reviewstatus: 1 });
              if (reviewobj) {
                reviewerPendingEntry.reviewworkflow = _.cloneDeep(reviewobj);
              }
              if (reviewobj && entry && entry.entries && entry.entries.status && (entry.entries.status.status == 0 || entry.entries.status.status == 1)) {
                db["reviewpendingworkflow"]
                  .insert(reviewerPendingEntry, function (err, a) {
                    if (req.data.workflowreview && req.data.workflowreview.length) {
                      updatereviewPendingUsers(req.data._id, reviewobj);//PQT-15 Prachi
                    }
                    res.status(200).json(req.data);
                  });
              } else {
                res.status(200).json(req.data);
              }
            });
        }
      });
    }


  }
  function beforePostForSaveDraft(req, res, next) {

    removeIsChild(req);
    removeCommentIfNull(req);

    log.log("function: beforePostForSaveDraft - start", 'info', currentFileName);
    checkForUniqueBatchId(req, res, next, function (result) {
      if (result) {
        res.status(422).json({ err: 'Batch Id already exists.' });
      } else {
        if ((_.isUndefined(req.body.eventSaveAndAccept) || req.body.eventSaveAndAccept == false) && !_.isUndefined(req.body.entries) && !_.isUndefined(req.body.entries.entityModel) && Object.keys(req.body.entries.entityModel).length > 0) {
          //QC3-8120 - Prachi
          console.log('-=-=-=-=-=-=-=-=-=-Save Draft or Save Draft All Event - Add Update Entity Record From Master-=-=-=-=-=-=-=-=-=-');
          var entityCount = req.body.entries.entityModel.length;
          executeDefaultPostSuccessProcessUpdateEntity(req.body.entries.entityModel, req).then(function (data) {
            var cnt = 0;
            _.forEach(data, function (result) {
              if (result['success']) {
                cnt++;
              }
            });
            if (entityCount == cnt) {
              log.log('---------------------beforePostForSaveDraft----------------', 'info');
              if (req.body.masterQCSettings) {
                db.sets.find({}).toArray(function (err, sets) {
                  setArray = sets;
                  masterAttributeStuff(req, res, setArray, next);
                })
                sendAppFailureMail(req, res, next);
              }
              next();
            } else {
              res.status(500).json(data);
              return;
            }
          });
        } else {
          log.log('---------------------beforePostForSaveDraft----------------', 'info');
          if (req.body.masterQCSettings) {
            db.sets.find({}).toArray(function (err, sets) {
              setArray = sets;
              masterAttributeStuff(req, res, setArray, next);
            })
            sendAppFailureMail(req, res, next);
          }
          next();
        }
      }
    });
  }

  function beforePatchForSaveDraft(req, res, next) {

    removeIsChild(req);
    removeCommentIfNull(req);

    if (!_.isUndefined(req.body.entries) && !_.isUndefined(req.body.entries.entityModel) && Object.keys(req.body.entries.entityModel).length > 0) {
      var entityCount = req.body.entries.entityModel.length;

      executeDefaultPostSuccessProcessUpdateEntity(req.body.entries.entityModel, req).then(function (data) {
        var cnt = 0;
        _.forEach(data, function (result) {
          if (result['success']) {
            cnt++;
          }
        });
        if (entityCount == cnt) {
          log.log('---------------------beforePatchForSaveDraft----------------', 'info');
          if (req.body.masterQCSettings) {
            db.sets.find({}).toArray(function (err, sets) {
              setArray = sets;
              masterAttributeStuff(req, res, setArray, next);
            })
            sendAppFailureMail(req, res, next);
          }
          db.users.findOneAsync({ username: req.user.username })
            .then(function (user) {
              if (req.body.verificationCode || !(_.isUndefined(req.body.verificationCode))) {
                if (user.verificationCode === crypto.createHash('sha1').update(req.body.verificationCode).digest('hex')) {
                  next();
                }
                else {
                  res.status(200).json({ invalid: true });
                }
              }
              else {
                next();
              }
            })
        } else {
          res.status(500).json(data);
          return;
        }
      });
    } else {
      log.log('---------------------beforePatchForSaveDraft----------------', 'info');
      if (req.body.masterQCSettings) {
        db.sets.find({}).toArray(function (err, sets) {
          setArray = sets;
          masterAttributeStuff(req, res, setArray, next);
        })
        sendAppFailureMail(req, res, next);
      }
      db.users.findOneAsync({ username: req.user.username })
        .then(function (user) {
          if (req.body.verificationCode || !(_.isUndefined(req.body.verificationCode))) {
            if (user.verificationCode === crypto.createHash('sha1').update(req.body.verificationCode).digest('hex')) {
              next();
            }
            else {
              res.status(200).json({ invalid: true });
            }
          }
          else {
            next();
          }
        })
    }
  }

  function sendAppFailureMail(req, res, next) {
    //log.log('-----------------------inside sendAppFailureMail---------------------', 'info');
    log.log('req.body.masterQCSettings.schemaId: ' + req.body.masterQCSettings.schemaId, 'info');
    if (req.body.masterQCSettings && req.body.masterQCSettings.schemaId) {
      var attr = [];
      tempSchemaID = req.body.masterQCSettings.schemaId;
      var userData;
      db.users.findOneAsync({ username: req.user.username })
        .then(function (user) {
          userData = user;
          db.schema.findOneAsync({ _id: db.ObjectID(tempSchemaID) })
            .then(function (entity) {
              _.forEach(entity.attributes, function (attribute) {
                if (!_.isUndefined(req.body.entries[attribute['s#']])) {
                  var attrValue;
                  if (attribute.type.format.title === 'Date') {
                    var tempDate = new Date(req.body.entries[attribute['s#']].attributes);
                    attrValue = new Date(tempDate.getTime() - (mcdiff * 60 * 1000));
                    // attrValue = attrValue.getMonth() + 1 + '/' + attrValue.getDate() + '/' + attrValue.getFullYear();
                    attrValue = moment(moment.utc(attrValue).toDate()).format('MM/DD/YYYY');
                  }
                  if (attribute.type.format.title === 'Time') {
                    var tempTime = new Date(req.body.entries[attribute['s#']].attributes);
                    attrValue = new Date(tempTime.getTime() - (mcdiff * 60 * 1000));
                    attrValue = attrValue.getHours() + ':' + attrValue.getMinutes() + ':' + attrValue.getSeconds();
                  }
                  if (attribute.type.format.title !== 'Time' && attribute.type.format.title !== 'Date') {
                    attrValue = req.body.entries[attribute['s#']].attributes;
                  }
                  attr.push({ 'title': attribute['title'], 'value': _.isArray(attrValue) ? (attrValue).toString() : attrValue });
                }
              })
              if (req.body && req.body.entries && req.body.entries.status && req.body.entries.status.status === 0 && entity && entity.isSendEmail) {
                delete req.body.schemaID;
                if (entity) {
                  var entityModifiedDate = new Date(new Date().getTime() - (mcdiff * 60 * 1000));
                  entityModifiedDate = moment(moment.utc(entityModifiedDate).toDate()).format('MM/DD/YYYY HH:mm');
                  // + (entity.createdDate).getSeconds();
                  _.merge(entity, settings);
                  _.merge(entity, { 'tempSchemaID': entity.title });
                  if (req.body && req.body.masterQCSettings) {
                    _.merge(entity, { 'masterAppName': req.body.masterQCSettings.title });
                  }
                  _.merge(entity, { 'objAttribute': attr })
                  _.merge(entity, { 'entityModifiedDate': entityModifiedDate });
                  _.merge(entity, { 'entityModifiedBy': userData.firstname + ' ' + userData.lastname + ' (' + userData.username + ')' })
                  log.log('inside file: entries.js - function: defaultPatch - status: sending mail', 'info');
                  //log.log(JSON.stringify(req.body));

                  mailer.send('qcform-status-notify', entity, entity.emailIdForQCFormFail, function sendMailCallback(e, b) {
                    if (e) {
                      debug.err('Failed sending email - qcform-status-notify');
                      debug.err(entity);
                      debug.err(e);
                      // next();
                    } else {
                      debug('QC Form Status has sent to ' + entity.emailIdForQCFormFail);
                      // next();
                    }
                  });
                }
              }
            })
        })
    }
  }

  // by surjeet.b@productivet.com ...
  function attributeStuff(req, res, setArray, next) {
    var settitle = '';
    var updatesetid = '';
    log.log('-------------attributeStuff---------------', 'info');
    if (req.schemaID && req.body && req.body.entries && req.body.entries.status && req.body.entries.status.status === 0) {
      //log.log('form fail... hence inside');
      db.schema.findOneAsync({ title: req.schemaID })
        .then(function (schema) {
          _.forEach(schema.attributes, function (attr) {
            if (attr.markSetValueInActive) {
              //log.log('yes make set value inactive', 'info');
              if (!_.isUndefined(attr.validation)) {
                if (attr.validation.type === '1') {
                  if (attr.type.title === 'Checkbox' || attr.type.title === 'Textbox' || attr.type.title === 'Dropdown') {
                    settitle = attr.type.title === 'Textbox' ? attr.validation.validationSet.existingSet.title : attr.validation.validationSet.existingSet;

                    debug('Initiating search for set with value: ' + settitle);
                    if (attr.type.title === 'Checkbox') {
                      updatesetid = attr.validation.validationSet.existingSet._id;
                      _.forEach(req.body.entries[attr['s#']].attributes, function (a) {
                        _.filter(_.find(setArray, { _id: db.ObjectID(updatesetid) }).values, function (v) {
                          if (v.value == a) {
                            v.isActive = false;
                            v.isDefaultValue = false;
                          }
                        })
                      })
                    } else if (attr.type.title === 'Textbox' || attr.type.title === 'Dropdown') {
                      updatesetid = attr.validation.validationSet.existingSet._id;
                      _.filter(_.find(setArray, { _id: db.ObjectID(updatesetid) }).values, function (v) {
                        if (v.value == req.body.entries[attr['s#']].attributes) {
                          v.isActive = false;
                          v.isDefaultValue = false;
                        }
                      })
                    }

                    debug('Initiating search and updating for set with value : ' + updatesetid);
                    db.sets.findOneAndUpdateAsync({
                      _id: db.ObjectID(updatesetid)
                    },
                      {
                        $set: {
                          values: _.find(setArray, { _id: db.ObjectID(updatesetid) }).values
                        }
                      }).then(function updateSetFindOneAndUpdateAsyncThenCallback(value) {
                        if (value) {
                          debug('Updated set: ' + updatesetid)
                        } else {
                          debug.err('Set with following name is not find');
                        }
                      }).catch(function updateSetFindOneAndUpdateErrorCallback(err) {
                        debug.err(err);
                      });
                  }
                }
              }
            }
          })
        })
    }
  }


  //patched work at the last moment just before client release..
  //this should be not like this... masterAttributeStuff should be handled inside attributeStuff function... by surjeet.b@productivet.com ...
  function masterAttributeStuff(req, res, setArray, next) {
    var settitle = '';
    var updatesetid = '';
    log.log('-------------masterAttributeStuff---------------', 'info');
    if (req.body && req.body.entries && req.body.entries.status && req.body.entries.status.status === 0) {
      //log.log('form fail... hence inside');
      //log.log(req.body.masterQCSettings.schemaId);
      db.schema.findOneAsync({ _id: db.ObjectID(req.body.masterQCSettings.schemaId) })
        .then(function (schema) {
          _.forEach(schema.attributes, function (attr) {
            if (attr.markSetValueInActive) {
              //log.log('yes make set value inactive');
              if (!_.isUndefined(attr.validation)) {
                if (attr.validation.type === '1') {
                  if (attr.type.title === 'Checkbox' || attr.type.title === 'Textbox' || attr.type.title === 'Dropdown') {
                    settitle = attr.type.title === 'Textbox' ? attr.validation.validationSet.existingSet.title : attr.validation.validationSet.existingSet;

                    debug('Initiating search for set with value: ' + settitle);
                    if (attr.type.title === 'Checkbox') {
                      updatesetid = attr.validation.validationSet.existingSet._id;
                      _.forEach(req.body.entries[attr['s#']].attributes, function (a) {
                        _.filter(_.find(setArray, { _id: db.ObjectID(updatesetid) }).values, function (v) {
                          if (v.value == a) {
                            v.isActive = false;
                            v.isDefaultValue = false;
                          }
                        })
                      })
                    } else if (attr.type.title === 'Textbox' || attr.type.title === 'Dropdown') {
                      updatesetid = attr.validation.validationSet.existingSet._id;
                      _.filter(_.find(setArray, { _id: db.ObjectID(updatesetid) }).values, function (v) {
                        if (v.value == req.body.entries[attr['s#']].attributes) {
                          v.isActive = false;
                          v.isDefaultValue = false;
                        }
                      })
                    }

                    debug('Initiating search and updating for set with value : ' + updatesetid);
                    db.sets.findOneAndUpdateAsync({
                      _id: db.ObjectID(updatesetid)
                    },
                      {
                        $set: {
                          values: _.find(setArray, { _id: db.ObjectID(updatesetid) }).values
                        }
                      }).then(function updateSetFindOneAndUpdateAsyncThenCallback(value) {
                        if (value) {
                          debug('Updated set: ' + updatesetid)
                        } else {
                          debug.err('Set with following name is not find');
                        }
                      }).catch(function updateSetFindOneAndUpdateErrorCallback(err) {
                        debug.err(err);
                      });
                  }
                }
              }
            }
          });
        });
    }
  }

  function checkForUniqueBatchId(req, res, next, cb) {
    var isManual = !!(req.body.batchFormat && req.body.batchFormat.name == 'Manual' && req.body.batchFormat.isSelected);
    if (isManual) {
      var schemaId = _.cloneDeep(req.body.batchFormat.schemaId);
      var batchName = 'batchEntries.' + req.body.batchEntryId;
      var batchAttributesInfoKey = 'batchAttributesInfo.' + req.body.batchEntryId;
      var query = { [batchName]: { $exists: true }, schemaId: schemaId };
      delete req.body.batchFormat;

      batchEntryService.getCount(schemaId, req.body.batchEntryId, function (err, result) {
        if (err) {
          cb(false);
        } else {
          if (result == 0) {
            batchEntryService.resetBatchEntry(schemaId, req.body.batchEntryId)
              .then(function () {
                cb(false);
              });
          } else {
            cb(result > 0);
          }
        }
      });
    } else {
      cb(false);
    }
  }

  // by surjeet.b@productivet.com ...
  function defaultPost(req, res, next) {

    req.schemaID = req.body.schemaID; //PQT-4238: mahammad

    removeIsChild(req);
    removeCommentIfNull(req);

    checkForUniqueBatchId(req, res, next, function (result) {
      if (result) {
        res.status(422).json({ err: 'Batch Id already exists.' });
      } else {
        if (req.body && !req.body.schemaID && (req.body.fromMail || req.body.fromToken)) {
          checkSharedAppDataValidity(req, res, function (exceptionHandler, successHandler) {
            if (exceptionHandler) {
              res.status(400).json({ error: exceptionHandler });
            } else {
              executeDefaultPostSuccessProcess(req, res, next);
            }
          });
        } else {
          executeDefaultPostSuccessProcess(req, res, next);
        }
      }
    });
  }



  function executeDefaultPostSuccessProcessUpdateEntity(updateEntityData, req) {
    log.log("function: executeDefaultPostSuccessProcessUpdateEntity - start", 'info', currentFileName);
    var deferred = q.defer();
    if (updateEntityData) {
      getEntityRecordForUpdateEntity(updateEntityData, [], req).then(function (data) {
        deferred.resolve(data);
      });
    }
    return deferred.promise;
  }

  function getEntityRecordForUpdateEntity(updateEntityValue, updatedData, req) {
    log.log("function: getEntityRecordForUpdateEntity - start", 'info', currentFileName);
    var deferred = q.defer();
    if (updateEntityValue.length > 0) {
      if (req.body.entries.status && req.body.entries.status.status != 3) {
        var entity = updateEntityValue.shift();
        var key = "" + entity.entityKeyAttributeSeqId;
        var select = {};
        if (Number(entity.entityRecordForUpdate[key])) {
          select = { $or: [{ ['entityRecord.' + key]: entity.entityRecordForUpdate[key] }, { ['entityRecord.' + key]: parseInt(entity.entityRecordForUpdate[key]) }], isDeleted: false };
        } else {
          select = { ['entityRecord.' + key]: entity.entityRecordForUpdate[key], isDeleted: false };
        }

        db[entity['entityId']].find(select, { entityRecord: 1, isDeleted: 1 }).toArray(function (err, entityRecordData) {
          if (err) {
            log.log('Error find at the time of add/update entity record :' + err, 'error');
          }
          var returnValue = {};
          returnValue['entityRecord'] = returnValue['entityRecord'] || {};
          var postModel;
          var options;
          var getUrl;
          if (entityRecordData.length == 0 && entity.typeOfEntity == 'addEntityRecord') {
            entity.entityRecordForUpdate['isActive'] = entity.entityRecordForUpdate['isActive'] == false ? false : true;
            postModel = {
              isDeleted: false,
              isFromApp: true,
              entityRecord: entity.entityRecordForUpdate
            };
            //log.log("req.headers['x-authorization']  :: " + req.headers['x-authorization'] );
            options = { headers: { "x-authorization": req.headers['x-authorization'] } };
            getUrl = req.headers.referer ? req.headers.referer : (config.apiEnvUrl + '/');
            console.log('URL :: ' + getUrl + '/entityRecords/' + entity['entityId']);
            //restler.postJson('http://' + req.headers.host + '/entityRecords/' + entity['entityId'], postModel, options)
            restler.postJson(config.apiEnvUrl + '/entityRecords/' + entity['entityId'], postModel, options)
              .on('complete', function (cmplt) {
                db[entity['entityId']].find(select, { entityRecord: 1, isDeleted: 1 }).toArray(function (err, entR) {
                  if (err) {
                    logger.log(err);
                  }
                  console.log("Entity Record Posted Successfully.");
                  if (entR) {
                    console.log(entR);
                    if (returnValue['entityRecord']) {
                      returnValue['entityRecord'][entity['entitySeqId']] = entR[0];
                    }
                  }
                  returnValue['success'] = true;
                  updatedData.push(returnValue);
                  getEntityRecordForUpdateEntity(updateEntityValue, updatedData, req).then(function (err, d) {
                    deferred.resolve(updatedData);
                  });
                });
              })
              .on('timeout', function () {
                log.log('timeout on getEntityRecordForUpdateEntity', 'error');
              })
              .on('error', function (err) {
                log.log('error on getEntityRecordForUpdateEntity' + err, 'error');
              });
          } else {
            if (entity.typeOfEntity == 'addEntityRecord') {
              try {
                console.log('-=-=-=-=-=-=-=-=-=-=-The key attribute is already used in another entity record-=-=-=-=-=-=-=-=-=-=-');
                returnValue['errMessage'] = 'The key attribute is already used in another entity record.';
                returnValue['success'] = false;
                returnValue['entityId'] = entity['entityId'];
                returnValue['entityTitle'] = entity['entityTitle'];
                returnValue['entitySeqId'] = entity['entitySeqId'];
                returnValue['entityKeyAttributeSeqId'] = entity['entityKeyAttributeSeqId'];
                updatedData.push(returnValue);
                getEntityRecordForUpdateEntity(updateEntityValue, updatedData, req).then(function (err, d) {
                  deferred.resolve(updatedData);
                });
              }
              catch (e) {
                log.log("addEntityRecord  :: " + e, 'error');
                deferred.resolve(updatedData);
              }
            } else if (entity.typeOfEntity == 'updateEntityRecord') {
              console.log('-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=- Update Entity Record -=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-');
              entity.entityRecordForUpdate['isActive'] = entity.entityRecordForUpdate['isActive'] == false ? false : true;
              postModel = {
                isDeleted: false,
                isFromApp: true,
                entityRecord: entity.entityRecordForUpdate
              };
              options = { headers: { "x-authorization": req.headers['x-authorization'] } };
              getUrl = req.headers.referer ? req.headers.referer : (config.apiEnvUrl + '/');

              if (!_.isUndefined(entityRecordData) && !_.isUndefined(entityRecordData[0]) && _.size(entityRecordData[0]) > 0) {
                console.log('URL :: ' + getUrl + 'api/entityRecords/' + entity['entityId'] + '/' + entityRecordData[0]['_id']);
                //restler.patchJson('http://' + req.headers.host + '/entityRecords/' + entity['entityId']+'/'+entityRecordData[0]['_id'], postModel, options)
                restler.patchJson(config.apiEnvUrl + '/entityRecords/' + entity['entityId'] + '/' + entityRecordData[0]['_id'], postModel, options)
                  .on('complete', function (cmplt) {
                    //QC3-9203 Prachi
                    if (returnValue['entityRecord']) {
                      postModel['_id'] = entityRecordData[0]['_id'];
                      returnValue['entityRecord'][entity['entitySeqId']] = postModel;
                    }
                    console.log("Entity Record Patch Successfully.");
                    returnValue['success'] = true;
                    updatedData.push(returnValue);
                    getEntityRecordForUpdateEntity(updateEntityValue, updatedData, req).then(function (err, d) {
                      deferred.resolve(updatedData);
                    });
                  })
                  .on('timeout', function () {
                    log.log('timeout on updateEntityRecord', 'error');
                  })
                  .on('error', function (err) {
                    log.log('error on updateEntityRecord' + err, 'error');
                  });
              } else {

                //restler.postJson('http://' + req.headers.host + '/entityRecords/' + entity['entityId'], postModel, options)
                restler.postJson(config.apiEnvUrl + '/entityRecords/' + entity['entityId'], postModel, options)
                  .on('complete', function (cmplt) {
                    db[entity['entityId']].find(select, { entityRecord: 1, isDeleted: 1 }).toArray(function (err, entR) {
                      if (err) {
                        logger.log(err);
                      }
                      console.log("Entity Record Posted Successfully.");
                      if (entR) {
                        console.log(entR);
                        if (returnValue['entityRecord']) {
                          returnValue['entityRecord'][entity['entitySeqId']] = entR[0];
                        }
                      }
                      returnValue['success'] = true;
                      updatedData.push(returnValue);
                      getEntityRecordForUpdateEntity(updateEntityValue, updatedData, req).then(function (err, d) {
                        deferred.resolve(updatedData);
                      });
                    });
                  })
                  .on('timeout', function () {
                    log.log('timeout on getEntityRecordForUpdateEntity', 'error');
                  })
                  .on('error', function (err) {
                    log.log('error on getEntityRecordForUpdateEntity', 'error');
                  });
              }
            } else {
              returnValue['success'] = false;
              updatedData.push(returnValue);
              getEntityRecordForUpdateEntity(updateEntityValue, updatedData, req).then(function (err, d) {
                deferred.resolve(updatedData);
              });
            }
          }
        });
      } else {
        updatedData.push({ 'success': true });
        deferred.resolve(updatedData)
      }
    } else {
      deferred.resolve(updatedData);
    }
    return deferred.promise;
  }

  function executeDefaultPostSuccessProcess(req, res, next) {
    log.log("function: executeDefaultPostSuccessProcess - start", 'info', currentFileName);
    //log.log("req.body.entries.entityModel :: " + JSON.stringify(req.body.entries.entityModel));
    // if (!_.isUndefined(req.body.entries) && !_.isUndefined(req.body.entries.entityModel) && Object.keys(req.body.entries.entityModel).length > 0) {
    //   var entityCount=req.body.entries.entityModel.length;
    //   executeDefaultPostSuccessProcessUpdateEntity(req.body.entries.entityModel,req).then(function(data) {
    //     var cnt=0;
    //     _.forEach(data,function(result){
    //       if(result['success']){
    //         cnt++;
    //       }
    //     });
    //     if(entityCount==cnt){
    //       generalsaveforpost(req, res, next);
    //
    //     }else{
    //       res.status(500).json(data);
    //       return;
    //     }
    //   });
    // }else{
    generalsaveforpost(req, res, next);
    // }
  }

  function generalsaveforpost(req, res, next) {
    log.log("function: generalsaveforpost - start", 'info', currentFileName);
    clientOffset = req.headers['client-offset'] || -330;
    mcdiff = -machineoffset + parseInt(clientOffset);
    //log.log('-------default post-------');
    // log.log(req.body);
    db.sets.find({}).toArray(function (err, sets) {
      setArray = sets;
      attributeStuff(req, res, setArray, next);
    })
    if (req.body.schemaID) {
      var attr = [];
      tempSchemaID = req.body.schemaID;
      var userData;
      db.users.findOneAsync({ username: req.user.username })
        .then(function (user) {
          userData = user;
        })
      db.schema.findOneAsync({ title: tempSchemaID })
        .then(function (entity) {
          _.forEach(entity.attributes, function (attribute) {
            if (!_.isUndefined(req.body.entries[attribute['s#']])) {
              var attrValue;
              if (attribute.type.format.title === 'Date') {
                var tempDate = new Date(req.body.entries[attribute['s#']].attributes);
                attrValue = new Date(tempDate.getTime() - (mcdiff * 60 * 1000));
                // attrValue = attrValue.getMonth() + 1 + '/' + attrValue.getDate() + '/' + attrValue.getFullYear();
                attrValue = moment(moment.utc(attrValue).toDate()).format('MM/DD/YYYY');
              }
              if (attribute.type.format.title === 'Time') {
                var tempTime = new Date(req.body.entries[attribute['s#']].attributes);
                attrValue = new Date(tempTime.getTime() - (mcdiff * 60 * 1000));
                attrValue = attrValue.getHours() + ':' + attrValue.getMinutes() + ':' + attrValue.getSeconds();

              }
              if (attribute.type.format.title !== 'Time' && attribute.type.format.title !== 'Date') {
                attrValue = req.body.entries[attribute['s#']].attributes;
              }
              attr.push({ 'title': attribute['title'], 'value': _.isArray(attrValue) ? (attrValue).toString() : attrValue });
            }
          })
          if (req.body && req.body.entries && req.body.entries.status && req.body.entries.status.status === 0 && entity && entity.isSendEmail) {
            delete req.body.schemaID;
            if (entity) {
              var entityModifiedDate = new Date(new Date().getTime() - (mcdiff * 60 * 1000));
              entityModifiedDate = moment(moment.utc(entityModifiedDate).toDate()).format('MM/DD/YYYY HH:mm');
              // + (entity.createdDate).getSeconds();
              _.merge(entity, settings);
              _.merge(entity, { 'tempSchemaID': tempSchemaID });
              _.merge(entity, { 'objAttribute': attr })
              _.merge(entity, { 'entityModifiedDate': entityModifiedDate });
              _.merge(entity, { 'entityModifiedBy': userData.firstname + ' ' + userData.lastname + ' (' + userData.username + ')' })
              //log.log('inside file: entries.js - function: defaultPatch - status: sending mail');

              mailer.send('qcform-status-notify', entity, entity.emailIdForQCFormFail, function sendMailCallback(e, b) {
                if (e) {
                  debug.err('Failed sending email - qcform-status-notify');
                  debug.err(entity);
                  debug.err(e);
                  updateShareAppCount(req, res, next);
                } else {
                  debug('QC Form Status has sent to ' + entity.emailIdForQCFormFail);
                  updateShareAppCount(req, res, next);
                }
              });
            }
          } else {
            updateShareAppCount(req, res, next);
          }
        })
    } else {

      updateShareAppCount(req, res, next)
    }
  }

  function updateShareAppCount(req, res, next) {
    if (req.body.fromMail || req.body.fromToken) {
      updateSharedAppData(req, res, function (err, sharedApp) {
        if (err || !sharedApp) {
          res.status(400).json({ error: err });
          return;
        } else {
          next();
        }
      });

    } else {
      next();
    }
  }

  // by surjeet.b@productivet.com ...
  function defaultPatch(req, res, next) {

    req.schemaID = req.body.schemaID; //PQT-4238: mahammad //PQT-4353

    entriesUtiliese.manageLiteLogsHistory(db, req).then(function () {
      removeIsChild(req);
      removeCommentIfNull(req);

      log.log("function: defaultPatch - start", 'info', currentFileName);
      if (!_.isUndefined(req.body.entries) && !_.isUndefined(req.body.entries.entityModel) && Object.keys(req.body.entries.entityModel).length > 0) {
        var entityCount = req.body.entries.entityModel.length;

        executeDefaultPostSuccessProcessUpdateEntity(req.body.entries.entityModel, req).then(function (data) {
          var cnt = 0;
          _.forEach(data, function (result) {
            if (result['success']) {
              cnt++;
            }
          });
          req.body.entries.updatedEntities = data;
          if (entityCount == cnt) {
            generalsaveforpatch(req, res, next);
          } else {
            res.status(500).json(data);
            return;
          }
        });
      } else {
        generalsaveforpatch(req, res, next);
      }
    });
  }
  function generalsaveforpatch(req, res, next) {
    log.log("function: generalsaveforpatch - start", 'info', currentFileName);
    var sendAppFailureMail = true;
    if (req.query && req.query.sendAppFailureMail == 'false') {
      sendAppFailureMail = false;
      delete req.query.sendAppFailureMail;
      delete req.items.sendAppFailureMail;
    }
    clientOffset = req.headers['client-offset'] || -330;
    mcdiff = -machineoffset + parseInt(clientOffset);
    db.sets.find({}).toArray(function (err, sets) {
      setArray = sets;
      attributeStuff(req, res, setArray, next);
    })
    if (req.body.schemaID) {
      var attr = [];
      tempSchemaID = req.body.schemaID;
      db.users.findOneAsync({ username: req.user.username })
        .then(function (user) {
          // if (req.body.verificationCode || !(_.isUndefined(req.body.verificationCode))) {
          // if(user.verificationCode === crypto.createHash('sha1').update(req.body.verificationCode).digest('hex')) {
          db.schema.findOneAsync({ title: tempSchemaID })
            .then(function (entity) {
              if (entity && entity.attributes && entity.attributes.length) {
                _.forEach(entity.attributes, function (attribute) {
                  if (!_.isUndefined(req.body.entries[attribute['s#']])) {
                    var attrValue;
                    if (attribute.type.format.title === 'Date') {
                      var tempDate = new Date(req.body.entries[attribute['s#']].attributes);
                      attrValue = new Date(tempDate.getTime() - (mcdiff * 60 * 1000));
                      // attrValue = attrValue.getMonth() + 1 + '/' + attrValue.getDate() + '/' + attrValue.getFullYear();
                      // var attrValue = tempDate.getMonth() + 1 + '/' + tempDate.getDate() + '/' + tempDate.getFullYear();
                      attrValue = moment(moment.utc(attrValue).toDate()).format('MM/DD/YYYY');
                    }
                    if (attribute.type.format.title === 'Time') {
                      var tempTime = new Date(req.body.entries[attribute['s#']].attributes);
                      attrValue = new Date(tempTime.getTime() - (mcdiff * 60 * 1000));
                      attrValue = attrValue.getHours() + ':' + attrValue.getMinutes() + ':' + attrValue.getSeconds();
                      // var attrValue = tempTime.getHours() + ':' + tempTime.getMinutes() + ':' + tempTime.getSeconds();
                    }
                    if (attribute.type.format.title !== 'Time' && attribute.type.format.title !== 'Date') {
                      attrValue = req.body.entries[attribute['s#']].attributes;
                    }
                    attr.push({ 'title': attribute['title'], 'value': _.isArray(attrValue) ? (attrValue).toString() : attrValue });
                  }
                });
              }
              if (req.body && req.body.entries && req.body.entries.status && req.body.entries.status.status === 0 && entity && entity.isSendEmail && sendAppFailureMail) {
                delete req.body.schemaID;
                if (entity) {
                  var entityModifiedDate = new Date(new Date().getTime() - (mcdiff * 60 * 1000));
                  entityModifiedDate = moment(moment.utc(entityModifiedDate).toDate()).format('MM/DD/YYYY HH:mm');
                  // + (entity.createdDate).getSeconds();
                  _.merge(entity, settings);
                  _.merge(entity, { 'tempSchemaID': tempSchemaID });
                  _.merge(entity, { 'objAttribute': attr })
                  _.merge(entity, { 'entityModifiedDate': entityModifiedDate });
                  _.merge(entity, { 'entityModifiedBy': user.firstname + ' ' + user.lastname + ' (' + user.username + ')' })
                  //log.log('inside file: entries.js - function: defaultPost - status: sending mail');

                  mailer.send('qcform-status-notify', entity, entity.emailIdForQCFormFail, function sendMailCallback(e, b) {
                    if (e) {
                      debug.err('Failed sending email - qcform-status-notify');
                      debug.err(entity);
                      debug.err(e);
                      next();
                    } else {
                      debug('QC Form Status has sent to ' + entity.emailIdForQCFormFail);
                      next();
                    }
                  });
                }
              } else {
                next();
              }
            });
        })
    } else {
      next();
    }
  }

  return route.router;
}

module.exports = configure;
