'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var db = require('../../lib/db');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'pluginlist',
    collection: 'pluginlist',
    schema: schema,
    softSchema: softSchema
  }
});

/**
 * @swagger
 * definition:
 *    Fields:
 *      properties:
 *        title:
 *          type: string
 *        entity:
 *          type: string
 *        type:
 *          type: string
 *        info:
 *          type: string
 *        index:
 *          type: integer
 */

/**
 * @swagger
 * definition:
 *    Configuration:
 *      properties:
 *        fields:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *            - $ref: '#/definitions/Fields'
 */

/**
 * @swagger
 * definition:
 *    PluginList:
 *      properties:
 *        id:
 *          type: integer
 *        entity:
 *          type: string
 *        title:
 *          type: string
 *        image:
 *          type: any
 *        isActive:
 *          type: boolean
 *        isMultipleInstance:
 *          type: boolean
 *        configuration:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *            - $ref: '#/definitions/Configuration'
 */

/**
 * @swagger
 * /pluginlist:
 *   get: 
 *     tags: [Administration]
 *     description: List of plugins
 *     produces:
 *        - application/json
 *     parameters:
 *        - name: pagesize
 *          description: Page Size
 *          in: query
 *          required: false
 *          type: string
 *        - name: select
 *          description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,title,type.title,modifiedDate,modifiedBy,version,versions,isActive])
 *          in: query
 *          required: false
 *          type: string
 *     responses:
 *       200:
 *         description: An array of plugins
 *         schema:
 *           $ref: '#/definitions/PluginList'
 */

module.exports = route.router;
