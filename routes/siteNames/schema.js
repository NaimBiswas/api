'use strict'
/**
 * @name siteNames-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var rString = types.rString;
var rNumber = types.rNumber0;
var rBool = types.rBool;
var string = types.string;

var schema = {
  _id: rNumber.label('idNumber'),
  title: rString.label('SiteName title'),
  isActive: rBool.label('IsActive'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
