'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'siteNames',
    collection: 'siteNames',
    schema: schema,
    softSchema: softSchema
  }
});

/**
 * @swagger
 * definition:
 *   SiteNames:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       isActive:
 *         type: boolean
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       createdBy:
 *         $ref: '#/definitions/CreatedBy'
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       version:
 *           type: integer
 *       isMajorVersion:
 *           type: boolean
 */

/**
 * @swagger
 * /siteNames:
 *   get:
 *     tags: [Site Level Name]
 *     description: Returns all sitelevelnames
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,title,_id])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of Silelevelnames
 *         schema:
 *           $ref: '#/definitions/SiteNames'
 */

 /**
  * @swagger
  * /siteNames/{id}:
  *   get:
  *     tags: [Site Level Name]
  *     description: Returns a object sitename
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: SiteName's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: pagesize
  *         description: Page Size
  *         in: query
  *         required: false
  *         type: string
  *     responses:
  *       200:
  *         description: An object of sitename
  *         schema:
  *           $ref: '#/definitions/SiteNames'
  */

 /**
  * @swagger
  * /siteNames:
  *   post:
  *     tags: [Site Level Name]
  *     description: Creates a new sitelevelname
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: site
  *         description: Sitename object
  *         in: body
  *         required: true
  *         schema:
  *           $ref: '#/definitions/SiteNames'
  *     responses:
  *       201:
  *         description: Successfully created
  */

 /**
  * @swagger
  * /siteNames/{id}:
  *   patch:
  *     tags: [Site Level Name]
  *     description: Updates a Site Level Name
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: Sitename's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: site
  *         description: Sitename object
  *         in: body
  *         required: true
  *         schema:
  *           $ref: '#/definitions/SiteNames'
  *     responses:
  *       200:
  *         description: Successfully updated
  */

module.exports = route.router;
