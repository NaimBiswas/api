'use strict'
/**
 * @name siteNames-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');


var id = types.id;
var string = types.string;
var number = types.number;
var bool = types.bool;

var schema = {
  // _id: number.label('idNumber'),
  title: string.label('SiteName title'),
  isActive: bool.label('Is Active'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
