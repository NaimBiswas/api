//QC3-3507 -- Kajal patel
// Improvement - App Builder > Acceptance: Initiate the App on the failure of Different Procedures
'use strict'
var express = require('express');
var apiRoutes = express.Router();
var _ = require('lodash');
var db = require('../../lib/resources').db;
var dbConfig = require('../../lib/db/db');
var q = require('q');
var log = require('../../logger');
var moment = require('moment');
var logger = require('../../logger');
var defaultRoute = require('../defaultRoute/defaultRoute.js');
var currentFileName = __filename;
var allSchemaId = [];
var allSchemaEntitValWise = [];
var allKeyValueSchema = [];
var passValidate = [];
var validateerror = [];
var expandentryarray = [];
var passedapp = [];
var errorAppDataStore = [];
var currentapp = {};
var loggedinuser = {};
var allExpandSchema = [];
var allInitiatedappIds = [];
var allExpandEntityRecord = [];
var requestFrom = '';
var allKeyValueRecord =[];
var checkForSameApp = [];
var mappedentriesproc = {};
var errormessage = '';

apiRoutes.post('/', function (req, res, next) {
  logger.log("API Called : api/appInitiateentry" , 'info', currentFileName);
  allSchemaId = [];
  allSchemaEntitValWise = [];
  allKeyValueSchema = [];
  passValidate = [];
  validateerror = [];
  expandentryarray = [];
  errorAppDataStore = [];
  currentapp = {};
  loggedinuser = {};
  allExpandSchema =[];
  allInitiatedappIds = [];
  allExpandEntityRecord = [];
  allKeyValueRecord = [];
  checkForSameApp = [];
  requestFrom = req.ip;
  errormessage = '';
  var entrydetails = req.body;
  try{
    var schemaId = db.ObjectID(entrydetails.schemaId);
    db.schema.find({_id : schemaId}).toArray(function (err , data) {
      db.users.find({_id : db.ObjectID(entrydetails.loggedinUserId)}).toArray(function (err,userdata){
        currentapp = _.cloneDeep(data[0]);
        loggedinuser =  _.cloneDeep(userdata[0]);
        initiateApp(entrydetails,res);
      });
    });
  }catch(e){
    log.log('Get current app and user :: '+e,'error');
  }
});

function initiateApp(entrydetails , response) {
  logger.log("function: initiateApp - start", 'info', currentFileName);
  var entry = entrydetails.entry;
  // QC3-7766 - kajal
  var userSites = entrydetails.userSites;
  if(entrydetails.setOriginalValue){
    entry.setOriginalValue = entrydetails.setOriginalValue;
  }
  entry.sets = entrydetails.sets;
  var mappedSchema = {};
  var conditionalschema = [];
  mappedentriesproc = {}
  var i = 0;
  _.forEach(entry.entries.tempAppDataStoreProcMapping, function (app, key) {
    _.forEach(app, function (obj, inx) {
      mappedSchema[i] = {};
      mappedSchema[i] = _.clone(obj.appMapping);
      mappedSchema[i]['pSeq'] = key;
      mappedSchema[i]['procArrIdx'] = inx;
      mappedSchema[i]['appId'] = obj.appId;
      mappedSchema[i]['isAuthorized'] = obj.isAuthorizedApp;
      i++;
    });
  });
  getAllRequiredSchema(entrydetails).then(function (data) {
    allExpandSchema = data.allExpandSchema;
    allExpandEntityRecord = data.allExpandEntityRecord;
    allKeyValueRecord = data.allKeyValueRecord;
    checkForSameApp = data.checkForSameApp;

    forEachMappedSchemaRecursively(mappedSchema[0], 0);
    // QC3-9264 forEach should be recursive
    function forEachMappedSchemaRecursively(appMapping, counter) {
      if(_.size(mappedSchema) == counter){
        return afterAllAppInitiate();
      }

      passValidate = [];
      try{
        var flag = false;
        var child = {};
        if (!_.isUndefined(entry.entries.errorAppDataStoreProc) && entry.entries.errorAppDataStoreProc.length > 0) {
          var failentry = _.find(entry.entries.errorAppDataStoreProc, { setRuleSeqId : appMapping.pSeq });
          if (!_.isUndefined(failentry)) {
            flag = true;
          }
          if (!_.isUndefined(entry.childprocInitiate) && entry.childprocInitiate.length > 0) {
            child = _.find(entry.childprocInitiate, function (field) {
              return field.mappedflowseq == appMapping.pSeq;
            });
            if (!_.isUndefined(child)) {
              flag = false;
            } else {
              flag = true;
            }
          } else {
            flag = true;
          }
        } else {
          if (!_.isUndefined(entry.childprocInitiate) && entry.childprocInitiate.length > 0) {
            child = _.find(entry.childprocInitiate, function (field) {
              return field.schemaId == appMapping.appId;
            });
            if (!_.isUndefined(child)) {
              flag = false;
            } else {
              flag = true;
            }
          } else {
            flag = true;
          }

        }
      } catch(e){
        log.log(e,'error');
      }

      if(flag){
        var conditionapp =  _.find(allExpandSchema , function (schema) {
          return appMapping.appId == schema._id;
        });
        conditionalschema.push({ _id: conditionapp._id, title: conditionapp.title,workflowSeq : appMapping.pSeq, keyvalue: conditionapp.keyvalue });
        if(!conditionapp.isActive){
          passValidate = [];
          passValidate.push(false);
          errormessage =  conditionapp.title + ' is Inactive.';
          validateerror.push(errormessage);
        }
        try{
          if(conditionapp.isActive) {
            if(!appMapping.isAuthorized){
              passValidate.push(false);
              errormessage = 'You are not authorized to access linked app ' + conditionapp.title + '.';
              validateerror.push(errormessage);
            }else{
              passValidate.push(true);
            }
            var entrynewForm = {
              reviewStatus: 'Draft',
              entries: {},
              levels: [],
              levelIds: [],
            }
            entrynewForm.createdDateInfo = new Date();
            entrynewForm.createdByInfo = { _id: loggedinuser._id, username: loggedinuser.username, firstname: loggedinuser.firstname, lastname: loggedinuser.lastname };
            entrynewForm.createdDate = new Date();
            entrynewForm.createdBy = loggedinuser._id;
            entrynewForm.modifiedBy = loggedinuser._id;
            entrynewForm.modifiedDate =  new Date();
            entrynewForm.levels = [];
            entrynewForm.levelIds = [];
            var lstLvlId = entry.levelIds[entry.levelIds.length - 1];
            if (conditionapp.siteYesNo == 'no') {
              var lvlArr = getLvlLvlIds(conditionapp);
              passValidate.push(true);
              entrynewForm.levels = _.cloneDeep(lvlArr.levels);
              entrynewForm.levelIds = _.cloneDeep(lvlArr.levelIds);
            }
            if (conditionapp.siteYesNo == 'yes') {
              var found = _.find(conditionapp.allicableSites[entry.levelIds.length - 1], ['_id', lstLvlId]);
              if (!_.isUndefined(found)) {
                passValidate.push(true);
                entrynewForm.levels = _.cloneDeep(entry.levels);
                entrynewForm.levelIds = _.cloneDeep(entry.levelIds);
              } else {
                passValidate.push(false);
                errormessage = 'Sites does not match according to their level. '
                validateerror.push(errormessage);
              }
            }///QC3-8177:mahammad should end here
              _.forEach(appMapping, function (mappingobj, key) {
                var val = '';
                var proc = {};
                var question = {};
                var entityque = {};
                var expandEntityRecord = {};
                var entityobj = {};
                if(entry.entries[mappingobj.maapingfield]){
                  if(key.indexOf('-') == -1){
                    try{
                      var attr = _.find(conditionapp.attributes, {'s#' : key});
                      if(_.has(mappingobj, 'procSeqId') && attr){
                        val = entry.entries[mappingobj.maapingfield][mappingobj.procSeqId];
                        proc = _.find(currentapp.procedures, {  's#': mappingobj.procSeqId  });
                        question = _.find(proc.questions, { 's#': mappingobj.maapingfield.split('-')[0] })
                        try{
                          chekForVal(val, attr, question,entry);
                      } catch(e){
                        log.log('proc'+ e,'error');
                      }
                        entrynewForm.entries[key] = {};
                        entrynewForm.entries[key].attributes = val;
                        entrynewForm.entries[key][mappingobj.procSeqId] = val;
                        mappedentriesproc[mappingobj.maapingfield] = {};
                        mappedentriesproc[mappingobj.maapingfield].ismapped = true;
                      }else if(_.has(mappingobj,'entitySeqId') && attr){
                        val = entry.entries[mappingobj.maapingfield][mappingobj.entitySeqId];
                        var currententity = _.find(currentapp.entities, { 's#': mappingobj.entitySeqId });
                        try{
                        chekForVal(val, attr, currententity,entry);
                      } catch(e){
                        log.log('entity'+ e,'error');
                      }
                        entrynewForm.entries[key] = {};
                        entrynewForm.entries[key].attributes = val;
                        entrynewForm.entries[key][mappingobj.entitySeqId] = val;
                        mappedentriesproc[mappingobj.maapingfield] = {};
                        mappedentriesproc[mappingobj.maapingfield].ismapped = true;
                      }else if(attr){
                        val = entry.entries[mappingobj.maapingfield].attributes;
                        var currentatt = _.find(currentapp.attributes, { 's#': mappingobj.maapingfield });
                        try{
                          chekForVal(val, attr, currentatt,entry);
                      } catch(e){
                        log.log('attqq'+ e,'error');
                      }
                        entrynewForm.entries[key] = {};
                        entrynewForm.entries[key].attributes = val;
                        mappedentriesproc[mappingobj.maapingfield] = {};
                        mappedentriesproc[mappingobj.maapingfield].ismapped = true;
                      }
                    }catch(e){
                      log.log('mapping to condition app :: '+e,'error');
                    }
                  }else {
                    var entity = _.find(conditionapp.entities,{'s#' : mappingobj.initiatedEntitySeqId});
                    if(entity){
                      if(_.has(mappingobj, 'procSeqId')){
                        proc = _.find(currentapp.procedures, {  's#': mappingobj.procSeqId  });
                        question = _.find(proc.questions, { 's#': mappingobj.maapingfield.split('-')[0] })
                        if (allExpandEntityRecord.length > 0) {
                          entityque = _.find(entity.questions, { isKeyValue: true });
                          expandEntityRecord = _.find(allExpandEntityRecord, { mappingseq : mappingobj.initiatedEntitySeqId });
                          if (!_.isUndefined(expandEntityRecord) && Object.keys(expandEntityRecord.entityRecord).length > 0) {
                            _.forEach(expandEntityRecord.entityRecord.entityRecord, function (ev, ek) {
                              if (ek != 'isActive' && ek != 'entityvalueinactivereason') {
                                entrynewForm.entries[ek + '-' + entityque.parentId] = {};
                                entrynewForm.entries[ek + '-' + entityque.parentId][entityque.parentId] = ev;
                              }
                            });
                            mappedentriesproc[mappingobj.maapingfield] = {};
                            mappedentriesproc[mappingobj.maapingfield].ismapped = true;
                          }else{
                            passValidate.push(false);
                            errormessage = 'The value you are passing from ' + question.title + ' to ' + entity.title + ' is either Inactive or does not found. Please provide other value.'
                            validateerror.push(errormessage);
                          }
                        }else{
                          passValidate.push(false);
                          errormessage = 'The value you are passing from ' + question.title + ' to ' + entity.title + ' is either Inactive or does not found. Please provide other value.'
                          validateerror.push(errormessage);
                        }
                        entityque = _.find(entity.questions, { isKeyValue: true });
                      }else if(_.has(mappingobj,'entitySeqId')){
                        try{
                          entityobj = _.find(currentapp.entities, { 's#': mappingobj.entitySeqId });
                          if (allExpandEntityRecord.length > 0) {

                            entityque = _.find(entity.questions, { isKeyValue: true });
                            expandEntityRecord = _.find(allExpandEntityRecord, { mappingseq : mappingobj.initiatedEntitySeqId });
                            if (!_.isUndefined(expandEntityRecord) && Object.keys(expandEntityRecord.entityRecord).length > 0) {
                              _.forEach(expandEntityRecord.entityRecord.entityRecord, function (ev, ek) {
                                if (ek != 'isActive' && ek != 'entityvalueinactivereason') {
                                  entrynewForm.entries[ek + '-' + entityque.parentId] = {};
                                  entrynewForm.entries[ek + '-' + entityque.parentId][entityque.parentId] = ev;
                                }
                              });
                              mappedentriesproc[mappingobj.maapingfield] = {};
                              mappedentriesproc[mappingobj.maapingfield].ismapped = true;
                            }else{
                              passValidate.push(false);
                              errormessage = 'The value you are passing from ' + entityobj.title + ' to ' + entity.title + ' is either Inactive or does not found. Please provide other value.'
                              validateerror.push(errormessage);
                            }
                          }else{
                            passValidate.push(false);
                            errormessage = 'The value you are passing from ' + entityobj.title + ' to ' + entity.title + ' is either Inactive or does not found. Please provide other value.'
                            validateerror.push(errormessage);
                          }
                        }catch(e){
                          log.log('mapping to condition app ::'+e,'error');
                        }
                      }else {
                        if (allExpandEntityRecord.length > 0) {
                          entityobj = _.find(currentapp.entities, { 's#': mappingobj.entitySeqId });
                          var attributeObj = _.find(currentapp.attributes, { 's#': mappingobj.maapingfield });
                          entityque = _.find(entity.questions, { isKeyValue: true });
                          expandEntityRecord = _.find(allExpandEntityRecord, { mappingseq : mappingobj.initiatedEntitySeqId });
                          if (!_.isUndefined(expandEntityRecord) && Object.keys(expandEntityRecord.entityRecord).length > 0) {
                            _.forEach(expandEntityRecord.entityRecord.entityRecord, function (ev, ek) {
                              if (ek != 'isActive' && ek != 'entityvalueinactivereason') {
                                entrynewForm.entries[ek + '-' + entityque.parentId] = {};
                                entrynewForm.entries[ek + '-' + entityque.parentId][entityque.parentId] = ev;
                              }
                            });
                            mappedentriesproc[mappingobj.maapingfield] = {};
                            mappedentriesproc[mappingobj.maapingfield].ismapped = true;
                          }else{
                            passValidate.push(false);
                            errormessage = 'The value you are passing from ' + ((entityobj || attributeObj || {}).title) + ' to ' + entity.title + ' is either Inactive or does not found. Please provide other value.'
                            validateerror.push(errormessage);
                          }
                        }else{
                          passValidate.push(false);
                          errormessage = 'The value you are passing from ' + entityobj.title + ' to ' + entity.title + ' is either Inactive or does not found. Please provide other value.'
                          validateerror.push(errormessage);
                        }
                      }
                    }
                  }
                }
              });
              var iskeyflag = false;
              var keyatt = _.find(allKeyValueRecord, { appId: conditionapp._id });
              if (!_.isUndefined(keyatt) && !_.isUndefined(keyatt.entryrecord) && keyatt.entryrecord.length > 0) {
                passValidate.push(false);
                iskeyflag = true;
                errormessage = 'Either attribute or entity value already exist. Please provide other value. '
                validateerror.push(errormessage);
              }
              var isalreadymappded  = _.find(checkForSameApp[conditionapp._id] ,function (obj) {
                return obj.pSeq == appMapping.pSeq;
              });
              if(isalreadymappded && isalreadymappded.matched && !iskeyflag){
                passValidate.push(false);
                errormessage = 'Either attribute or entity value already exist. Please provide other value.'
                validateerror.push(errormessage);
              }
              // _.forEach(vm.checkForSameApp[ms.appId], function (arr, index) {
              //   //var obj = _.find(arr,{pSeq : ms.pSeq,pArrIdx : ms.pArrIdx});
              //   if (arr.matched && arr.pSeq == ms.pSeq && ms.pArrIdx == arr.pArrIdx) {
              //     vm.passValidate.push(false);
              //     var a = 'Either attribute or entity value already exist. Please provide other value.'
              //     vm.validateerror.push(a);
              //   }
              // })
            // }
          }
        } catch(e){
          log.log('mapping error'+e,'error');
        }
      }

      try {
        if (passValidate && passValidate.length && passValidate.indexOf(false) == -1) {
          entrynewForm.parentprocInitiate = {
            entryId: entry._id,
            schemaId: currentapp._id,
            schmeatitle: currentapp.title
          }
          entrynewForm.parentVersion = conditionapp.version;
          entrynewForm.entries.status = {};
          entrynewForm.entries.status.status = 4;
          passedapp.push({ _id: conditionapp._id, title: conditionapp.title });
          generateNextSequenceForAutoAttribute(conditionapp, entrynewForm, function (err, responseAutoGenerateAttribute) {
            if (err) {
              $log.log(err);
              return;
            }
            expandentryarray.push(postrecord(conditionapp._id, entrynewForm, entry));
            counter++;
            forEachMappedSchemaRecursively(mappedSchema[counter], counter);
          });
        } else {
          if (validateerror.length > 0) {
            errormessage = {
              appId: conditionapp._id,
              apptitle: conditionapp.title,
              errors: _.cloneDeep(validateerror),
              setRuleSeqId: appMapping.pSeq
            }
            errorAppDataStore.push(errormessage);
            validateerror = [];
            expandentryarray.push({});
          }
          counter++;
          forEachMappedSchemaRecursively(mappedSchema[counter], counter);
        }
      } catch (e) {
        log.log('app initiate error ::' + e, 'error');
      }
    }

  // QC3-9264 should be called after all app initiated
    function afterAllAppInitiate() {
      var childforms = [];
      q.all(expandentryarray).then(function (res) {
        try{
          _.forEach(conditionalschema, function (schema, index) {
            if (res[index] && res[index].ops && res[index].ops.length) {
              var initiatedForm = {
                entryId: res[index].ops[0]._id,
                entry: res[index].ops[0].entries,
                schemaId: schema._id,
                schmeatitle: schema.title,
                mappedflowseq: schema.workflowSeq
              }
              // QC3-7766 - kajal
              var entrySiteIds = res[index].ops[0].levelIds;
              var checkSitePermission = userSites.every(function(usrsite){
                return usrsite.some(function(site){
                  return entrySiteIds.some(function(appsite){return appsite == site});
                });
              });
              if (checkSitePermission) {
                initiatedForm.appsitepermission = true;
              }else{
                initiatedForm.appsitepermission = false;
              }
              //activityInfo Call
              logActivityInformation('Created', schema, res[index].ops[0]);
              childforms.push(initiatedForm);
            }
          });
          if (!_.isUndefined(childforms)) {
            if (!_.isUndefined(entry) && !_.isUndefined(entry.entries)) {
              entry.entries.mappedentriesproc = _.cloneDeep(mappedentriesproc);
            }
            if (!_.isUndefined(entry.childprocInitiate) && entry.childprocInitiate.length > 0) {
              entry.childprocInitiate = entry.childprocInitiate.concat(childforms);
              _.forEach(childforms,function (child) {
                entry.entries.childapps.push(child.mappedflowseq);
              })
            } else if(childforms.length > 0){
              entry.childprocInitiate = _.cloneDeep(childforms);
              entry.entries.childapps  =  [];
              _.forEach(childforms,function (child) {
                entry.entries.childapps.push(child.mappedflowseq);
              })
            }

          }
          entry.entries.errorAppDataStoreProc =[];
          if (!_.isUndefined(entry) && !_.isUndefined(entry.entries) ) {
            entry.entries.errorAppDataStoreProc = _.cloneDeep(errorAppDataStore);
          }
        } catch (e){
          log.log('To update entry in existing record :: '+e,'error');
        }
        delete entry.setOriginalValue;
        delete entry.sets;
        var updatedentry = _.cloneDeep(entry);
        return   response.status(200).json(updatedentry);
       });
     }
  });
}
        //Advance Search activity info
        // QC3-7705 - Loki : Save Activity Information on app initiation
        function logActivityInformation(status, schemaResource, entry) {
          var activityInfo = {};
          activityInfo.attributes = []
          activityInfo.qcformId = schemaResource._id + '';
          activityInfo.qcentryId = entry._id + '';
          activityInfo.title = schemaResource.title;
          if (schemaResource && schemaResource.keyvalue && schemaResource.keyvalue.length) {
              _.forEach(schemaResource.keyvalue, function (keyatt) {
                var obj = {
                  id: keyatt._id,
                  title: keyatt.title,
                  type: keyatt.type
                }
                if (keyatt.parentId) {
                  var attSequenceId = keyatt['s#'];
                  if (entry.entries[attSequenceId]) {
                    obj.value = entry.entries[attSequenceId].attributes
                  }
                } else {
                  var entitySequenceId = keyatt['s#'] + '-' + keyatt.parentId;
                  if (entry.entries[entitySequenceId]) {
                    obj.value = entry.entries[entitySequenceId][keyatt.parentId]
                  }
                }
                activityInfo.attributes.push(obj);
              });
          }
          activityInfo.status = status ? status : 'Edit';
          var userInfo = `${entry.createdDateInfo.firstname} ${entry.createdDateInfo.lastname}(${entry.createdDateInfo.username})`;
          activityInfo.createdByName = userInfo;
          activityInfo.modifiedByName = userInfo;
          activityInfo.createdDate = entry.createdDate;
          activityInfo.createdBy = entry.createdBy;
          activityInfo.modifiedBy = entry.modifiedBy;
          activityInfo.modifiedDate = entry.modifiedDate;
          activityInfo.isMajorVersion = true;
          activityInfo.version = 1;
          try {
            db.collection('qcentryactivity').insertOne(activityInfo, function (s, data) {
              logger.log("collection: qcentryactivity insertOne", 'info', currentFileName);
              if (data && data.ops && data.ops.length) {

                try{
                  db['qcentryactivity'].auditThis1(data.ops[0], requestFrom);
                }
                catch(e){

                }
              }
            });
          } catch (error) {
            logger.log("error on logging activity information", "error", __filename);
          }
        }

function getLvlLvlIds(schma) {
  logger.log("function: getLvlLvlIds - start", 'info', currentFileName);
  var arr = { levels: [], levelIds: [] };

  _.forEach(schma.allicableSites, function (st, idx) {
    arr.levels.push({ levelId: idx, siteId: st[0]._id, title: st[0].title });
    arr.levelIds.push(st[0]._id);
  });

  return arr;
}

function postrecord(f,entrytopost,entry){
  logger.log("function: postrecord - start", 'info', currentFileName);
  var ddd = q.defer();
  var a = f.toString();

  db.collection(a).insertOne(entrytopost , function(s,data){
    logger.log("collection: "+ a +" insertOne" , 'info', currentFileName);
    db[a].auditThis1(data.ops[0], requestFrom); //QC3-9633
    ddd.resolve(data);
  });

  return ddd.promise;
}

function chekForVal(val, attr, currentatt,entry) {
  logger.log("function: chekForVal - start", 'info', currentFileName);
  switch (attr.type.title) {
    case "Textbox":
    passValidate.push(textBoxValidation(val, attr, entry, currentatt));
    break;
    case "Dropdown":
    passValidate.push(dropDownValidation(val, attr, entry, currentatt));
    break;
    case "Textarea":
    passValidate.push(true);
    break;
    default:
    passValidate.push(false);
    break;
  }
}

function dropDownValidation(scol, mapField, entry, currentatt) {
  logger.log("function: dropDownValidation - start", 'info', currentFileName);
  var flag = false;
  var setvalues = [];
  //akashdeep.s - QC3-5445 - get the latest sets all the time.
  if (mapField.validation.validationSet.existingSet) {
    var setLetastValue = _.find(entry.sets, function (setval) {
      if (setval._id == mapField.validation.validationSet.existingSet._id) {
        return setval;
      }
    });
    if (setLetastValue) {
      setvalues = setLetastValue.values;
    }else {
      setvalues = mapField.validation.validationSet.existingSet.values;
    }
  }

  _.forEach(setvalues, function (setval) {
    if (scol.toString().trim().toLowerCase() == setval.title.toString().trim().toLowerCase()) {
      flag = true;
    }
  });
  if (!flag && currentatt) {
    errormessage = 'The value you are passing from ' + currentatt.title + ' to ' + mapField.title + " does not match with dropdown set values. Please provide other value. ";
    validateerror.push(errormessage);
  }
  return flag;
};

function textBoxValidation(scol, mapField, entry, currentatt) {
  logger.log("function: textBoxValidation - start", 'info', currentFileName);
  switch (mapField.type.format.title) {
    case "None":
    return validateField(scol, mapField, entry, false, currentatt);
    case "Number":
    return validateField(scol, mapField, entry, true, currentatt);
    case "Exponential":
    return validateField(scol, mapField, entry, true, currentatt);
    case "Percentage":
    return validateField(scol, mapField, entry, true, currentatt);
    case "Currency":
    return validateField(scol, mapField, entry, true, currentatt);
    case "Scientific":
    return validateField(scol, mapField, entry, true, currentatt);
    case "Date":
    if (!_.isUndefined(mapField) && !_.isUndefined(mapField.vali1dation) && !_.isUndefined(mapField.vali1dation.condition)) {
      return validateDate(mapField.vali1dation.condition, scol, currentatt);
    } else {
      return true;
    }
    case "Time":
    return validateTime(scol, mapField, entry, currentatt, currentatt);
    default:
    return true;
  }
}

function validateCondition(scope, value) {
  logger.log("function: validateCondition - start", 'info', currentFileName);
  if (scope.type.format.title === 'Number' || scope.type.format.title === 'Percentage' || scope.type.format.title === 'Currency' || scope.type.format.title === 'Scientific') {
    var decimals = parseInt(scope.type.format.metadata.Decimal);
    decimals = decimals > 25 ? 25 : decimals;
    if (_.isNaN(decimals) || decimals == '') {
      value = Math.floor(parseFloat(value));
    } else {
      value = _.isUndefined(value) ? '' : parseFloat(parseFloat(value).toFixed(decimals));
    }
  }
  if (!_.isUndefined(scope.type) && !_.isUndefined(scope.type.format) && scope.type.format.title == 'Exponential') {
    value = value * (Math.pow(10, !_.isUndefined(scope.type.format.metadata.power) ? scope.type.format.metadata.power : 0));
  }
  value = !isNaN(parseFloat(value)) ? parseFloat(value) : value.length;
  switch (scope.validation.condition.conditionTitle) {
    case '>':
    return value > parseFloat(scope.validation.condition.value);
    case '<':
    return value < parseFloat(scope.validation.condition.value);
    case '>=':
    return value >= parseFloat(scope.validation.condition.value);
    case '<=':
    return value <= parseFloat(scope.validation.condition.value);
    case '=':
    return value == parseFloat(scope.validation.condition.value);
    case '!=':
    return value != parseFloat(scope.validation.condition.value);
    case '>= && <=':
    return (value >= parseFloat(scope.validation.condition.minimum) && value <= parseFloat(scope.validation.condition.maximum));
    case '!(>= && <=)':
    return !(value >= parseFloat(scope.validation.condition.minimum) && value <= parseFloat(scope.validation.condition.maximum));
    default:
    return false;
  }
}

function validateField(col, question, entry, isNumber, currentatt) {
  logger.log("function: validateField - start", 'info', currentFileName);
  try{
    if (isNumber) {
      if (!(/^\d+$/.test(col) || (/^\s*[-+]?(\d*\.?\d+|\d+\.)(e[-+]?[0-9]+)?\s*$/i.test(col)))) {
        errormessage = 'The value you are passing from ' + currentatt.title + ' to ' + question.title + ' does not match. Please provide valid number.'
        validateerror.push(errormessage);
        return false;
      }
    }
    // Case for Validting regularExpression, validationSet and conditions on question
    if (!_.isUndefined(question.validation) && Object.keys(question.validation).length > 0) {
      if (!_.isUndefined(question.validation.regularExpression) && question.validation.regularExpression) {
        if (!(new RegExp(question.validation.regularExpression.regEx)).test(col)) {
          errormessage = 'The value you are passing from ' + currentatt.title + ' to ' + question.title + " does not match. Please provide valid value for regular expression.";
          validateerror.push(errormessage);
          // entry.FailReasons[question['s#']] = _.isUndefined(question.validation.regularExpression.validationMessage) ? 'Value does not match. Please provide valid value for regular expression.' : question.validation.regularExpression.validationMessage;
          return false;
        }
      }

      if (!_.isUndefined(question.validation.validationSet) && question.validation.validationSet) {
        var setTitle = _.isObject(question.validation.validationSet.existingSet) ? question.validation.validationSet.existingSet.title : question.validation.validationSet.existingSet

        var setobj = _.find(entry.sets, function (setval) {
          return setTitle == setval.title;
        });

        if (!_.isUndefined(setobj) && !_.isUndefined(setobj.values)) {
          var data = _.filter(setobj.values, function (value) {
            return value.isActive === true;
          });
        }

        var returnData = _.filter(data, function (object) {
          if (isValidNumber(object.value)) {
            if (entry.setOriginalValue && (!isNaN(parseFloat(object.value)) ? parseFloat(object.value) : object.value.toLowerCase()) === (!isNaN(parseFloat(col)) ? parseFloat(col) : col.toLowerCase())) {
              if (entry.setOriginalValue.indexOf(object.value) == -1) {
                entry.setOriginalValue.push(object.value);
              }
            }
            return true;
          } else {
            return object.value === col
          }
        }).length > 0;

        if (returnData) {
          return true;
        } else {
          if (currentatt && currentatt.title) {
            errormessage = _.isUndefined(question.validation.validationSet.validationMessage) ? 'The value you are passing from ' + currentatt.title + ' to ' + question.title + ' does not match. Please provide valid value from sets' : 'The value you are passing from ' + currentatt.title + ' to ' + question.title + '. ' + question.validation.validationSet.validationMessage;
            validateerror.push(errormessage);
          }
          return false;
        }
      }

      if (!_.isUndefined(question.validation.condition) && !_.isUndefined(question.validation.condition.conditionTitle) && question.validation.condition) {
        if (!validateCondition(question, col)) {
          errormessage = _.isUndefined(question.validation.condition.validationMessage) ? 'The value you are passing from ' + currentatt.title + ' to ' + question.title + ' does not match. Please provide valid value.' : 'The value you are passing from ' + currentatt.title + ' to ' + question.title + '. ' + question.validation.condition.validationMessage;
          validateerror.push(errormessage);
          return false;
        }
        else {
          return true;
        }
      }
    } else {
      return true;
    }
  }catch(e){
    log.log('validate field to pass initiate app :: '+e,'error');
  }
};

function isValidNumber(value) {
  logger.log("function: isValidNumber - start", 'info', currentFileName);
  // Currently we are adding patch for just checking - in the value
  // var filterInt = function (value) {
  if(/^\d+$/.test(value))
  {
    return Number(value);
  }
  else if(/^\s*[-+]?(\d*\.?\d+|\d+\.)(e[-+]?[0-9]+)?\s*$/i.test(value))
  {
    return Number(value)
  }
  else {
    return value;

  }
  // }
}

function validateDate(scope, value, currentatt) {
  logger.log("function: validateDate - start", 'info', currentFileName);
  if (scope.today === '1') {
    scope.value = new Date().setHours(0, 0, 0, 0);
  }
  switch (scope.conditionTitle) {
    case '>':
    return new Date(value).setHours(0, 0, 0, 0) > new Date(scope.value).setHours(0, 0, 0, 0);
    case '<':
    return new Date(value).setHours(0, 0, 0, 0) < new Date(scope.value).setHours(0, 0, 0, 0);
    case '>=':
    return new Date(value).setHours(0, 0, 0, 0) >= new Date(scope.value).setHours(0, 0, 0, 0);
    case '<=':
    return new Date(value).setHours(0, 0, 0, 0) <= new Date(scope.value).setHours(0, 0, 0, 0);
    case '=':
    //return value == new Date(scope.value).setHours(0,0,0);
    return moment.utc(new Date(value).setHours(0, 0, 0, 0)).isSame(new Date(scope.value).setHours(0, 0, 0, 0));
    // return moment.utc(value).isSame(new Date(scope.value).setHours(0,0,0,0));
    case '!=':
    //return value != new Date(scope.value).setHours(0,0,0);
    return !moment.utc(new Date(value).setHours(0, 0, 0, 0)).isSame(new Date(scope.value).setHours(0, 0, 0, 0));
    // return !moment.utc(value).isSame(new Date(scope.value).setHours(0,0,0,0));
    case '>= && <=':
    return (new Date(value).setHours(0, 0, 0, 0) >= new Date(scope.minimumDate).setHours(0, 0, 0, 0) && new Date(value).setHours(0, 0, 0, 0) <= new Date(scope.maximumDate).setHours(0, 0, 0, 0));
    case '!(>= && <=)':
    return !(new Date(value).setHours(0, 0, 0, 0) >= new Date(scope.minimumDate).setHours(0, 0, 0, 0) && new Date(value).setHours(0, 0, 0, 0) <= new Date(scope.maximumDate).setHours(0, 0, 0, 0));
    default:
    return false;
  }
}

function validateTime(conditiontitle, condition, entertimeValue, currenttime, currentatt) {
  logger.log("function: validateTime - start", 'info', currentFileName);
  if (currenttime === '1') {
    condition.conditiontimeValue = new Date().setHours(new Date().getHours(), new Date().getMinutes(), 0, 0);
  }

  var one = moment(moment.utc(entertimeValue).toDate()).format('h:mm:ss a');
  splitTime(one);
  entertimeValue = new Date().setHours(vm.hours, vm.mins, vm.secs, 0);

  var two = moment(moment.utc(condition.conditiontimeValue).toDate()).format('h:mm:ss a');
  splitTime(two);
  condition.conditiontimeValue = new Date().setHours(vm.hours, vm.mins, vm.secs, 0);

  var three = moment(moment.utc(condition.mintime).toDate()).format('h:mm:ss a');
  splitTime(three);
  condition.mintime = new Date().setHours(vm.hours, vm.mins, vm.secs, 0);

  var four = moment(moment.utc(condition.maxtime).toDate()).format('h:mm:ss a');
  splitTime(four);
  condition.maxtime = new Date().setHours(vm.hours, vm.mins, vm.secs, 0);

  switch (conditiontitle) {
    case '>':
    return entertimeValue > condition.conditiontimeValue;
    case '<':
    return entertimeValue < condition.conditiontimeValue;
    case '>=':
    return entertimeValue >= condition.conditiontimeValue;
    case '<=':
    return entertimeValue <= condition.conditiontimeValue;
    case '=':
    return entertimeValue == condition.conditiontimeValue;
    case '!=':
    return entertimeValue != condition.conditiontimeValue;
    case '>= && <=':
    return (entertimeValue >= condition.mintime && entertimeValue <= condition.maxtime);
    case '!(>= && <=)':
    return !(entertimeValue >= condition.mintime && entertimeValue <= condition.maxtime);
    default:
    return false;
  }
}

function getAllRequiredSchema(entrydetails) {
  logger.log("function: getAllRequiredSchema - start", 'info', currentFileName);
  try{
    var entry = entrydetails.entry;
    var defer = q.defer();
    // vm.allSchemaEntitValWise = [];
    // vm.allKeyValueSchema = [];
    // vm.allExpandSchema = [];
    // vm.allExpandEntityRecord = [];
    // vm.allKeyValueRecord = [];
    //get entity value and it's mapped schema details
    _.forEach(entry.entries.tempAppDataStoreProcMapping, function (procobj, pSeq) {
      _.forEach(procobj, function (sm, arIdx) {
        allSchemaId.push(sm.appId);
      });
    });
    var schemaIds = [];
    allInitiatedappIds = _.cloneDeep(allSchemaId);
    allSchemaId = _.uniq(allSchemaId);
    _.forEach(allSchemaId, function (id) {
      schemaIds.push(db.ObjectID(id));
    });
    getallexpandSchema(schemaIds).then(function (success) {
      var  allExpandSchema = success;
      _.forEach(entry.entries.tempAppDataStoreProcMapping, function (procobj, pSeq) {
        _.forEach(procobj, function (sm, arIdx) {
          //check for entity mapping present
          _.forEach(sm.appMapping, function (mpobj, mpk) {
            var tempKeyVal = [];
            var app = {};
            var vl = '';
            if (_.has(mpobj, 'entitySeqId')) {
              if(entry.entries[mpobj.maapingfield]){
                vl = entry.entries[mpobj.maapingfield][mpobj.entitySeqId];
              }
            } else if (_.has(mpobj, 'procSeqId')) {
              if(entry.entries[mpobj.maapingfield]){
                vl = entry.entries[mpobj.maapingfield][mpobj.procSeqId];
              }
            }
            else {
              if (entry.entries[mpobj.maapingfield] && entry.entries[mpobj.maapingfield].attributes) {
                if(entry.entries[mpobj.maapingfield]){
                  vl = entry.entries[mpobj.maapingfield].attributes;
                }
              }
            }
            app = _.find(allExpandSchema , function (schema) {
              if(schema._id == sm.appId ){
                return schema;
              }
            });
            var entity  = _.find(app.entities , {'s#' : mpobj.initiatedEntitySeqId});
            if (_.has(mpobj, 'initiatedEntitySeqId') && mpk.indexOf('-') > 0 && entity) {
              allSchemaEntitValWise.push({ pSeq: pSeq, appId: sm.appId, parentId: mpobj.entitySeqId, initiatedEntityseq: mpobj.initiatedEntitySeqId , val: vl , entityId: entity._id, questionSeqId: mpk.split('-')[0] });
            }
            //check for the keyvalue
            if(app.keyvalue.length > 0){
              var kv = {};
              if(mpk.indexOf('-') > 0){
                var entityseqId = mpk.split('-')[1];
                if(entityseqId){
                  kv  = _.find(app.keyvalue, {parentId : entityseqId});
                }
              }else{
                kv  = _.find(app.keyvalue, {'s#' : mpk});
              }
              if(kv){
                // var filterAttributeKey = _.filter(allKeyValueSchema , function (akey) {
                //   return akey.appId == app._id;
                // });
                var obj = {
                  seq: kv['s#'],
                  val: vl
                }
                if (_.has(kv, 'parentId')) {
                  obj['parentId'] = kv.parentId;
                }
                tempKeyVal.push(obj);
              }

            }
            var checkForKeyValue = _.find(allKeyValueSchema , {pSeq : pSeq});
            if(!checkForKeyValue){
              allKeyValueSchema.push({ appId: app._id, keyval: tempKeyVal, pSeq: pSeq, pArrIdx: arIdx });
            }
          });
        });
      });
      var alldbobj = [];
      var checkForSameApp = {};
      _.forEach(allSchemaEntitValWise, function (ev) {
        var qry = {};
        qry["entityRecord." + ev.questionSeqId ] = ev.val;
        var obj = {
          entityId : ev.entityId,
          initiatedEntityseq : ev.initiatedEntityseq,
          qry : qry
        }
        alldbobj.push(obj)
      });
      var qry = {};
      allKeyValueSchema = _.uniq(allKeyValueSchema);

      _.forEach(allKeyValueSchema, function (aks) {
        //create promise to get data from Record
        _.forEach(aks.keyval, function (obj) {
          if (_.has(obj, 'parentId')) {
            qry['entries.' + obj.seq + '-' + obj.parentId + '.' + obj.parentId] = obj.val;
          } else {
            qry['entries.' + obj.seq + '.attributes'] = obj.val;
          }
        });
        if (_.isUndefined(checkForSameApp[aks.appId])) {
          checkForSameApp[aks.appId] = [];
        }
        checkForSameApp[aks.appId].push({ qry: qry, pSeq: aks.pSeq, pArrIdx: aks.pArrIdx, matched: false });
        if (aks.keyval.length > 0) {
          var obj = {
            entryappId : aks.appId,
            initiatedEntityseq : aks.pSeq,
            qry : qry
          }
          alldbobj.push(obj);
        }
      });

      // _.forEach(checkForSameApp, function (val, key) {
      //   for (var i = 0; i < val.length - 1; i++) {
      //     for (var j = 1; j < val.length; j++) {
      //         if (_.isEqual(val[i].qry, val[j].qry)) {
      //         if (!val[j].matched) {
      //           console.log('11111111111111111111');
      //           val[j].matched = true;
      //         }
      //       }
      //     }
      //   }
      // });

    _.forEach(checkForSameApp,function (val,key) {
      var flag = false;
      var allseq = [];
      var allquery = [];
      _.forEach(val,function (obj) {
        if(Object.keys(obj.qry).length > 0){
        if(allseq.indexOf(obj.pSeq) == -1){
          allseq.push(obj.pSeq)
          if(allquery.indexOf(obj.qry) > -1){
            _.forEach(allquery , function (query,index) {
              if (_.isEqual(query, obj.qry)) {
                flag =true;
                obj.matched = true;
              }
            })
          }else{
            allquery.push(obj.qry)
          }
        }else{
          if(flag){
            obj.matched = true;
          }
        }
      }
      })
    })
      var array = [];
      _.forEach(alldbobj,function (obj) {
        array.push(getallresponse((obj.entityId ? obj.entityId : obj.entryappId),obj.qry));
      })
      var allExpandEntityRecord = [];
      var allKeyValueRecord = [];
      q.all(array).then(function(data){
        _.forEach(alldbobj,function (obj,index) {
          if(obj.entityId){
            if(data[index].length > 0 ){
              allExpandEntityRecord.push({ entityId: obj.entityId,mappingseq : obj.initiatedEntityseq,entityRecord: data[index][0] });
            }else{
              allExpandEntityRecord.push({ entityId: obj.entityId,mappingseq : obj.initiatedEntityseq,entityRecord: {} });
            }
          }else if(obj.entryappId){
            allKeyValueRecord.push({ appId: obj.entryappId,mappingseq : obj.initiatedEntityseq, entryrecord: data[index]});
          }
        })
        var obj = {
          allExpandSchema : allExpandSchema,
          allExpandEntityRecord : allExpandEntityRecord,
          allKeyValueRecord : allKeyValueRecord,
          checkForSameApp : checkForSameApp
        }
        defer.resolve(obj);
      });
    });
    return defer.promise;
  }catch(e){
    log.log('db check for initiate app ::'+e,'error')
  }
}

function getallresponse(f,qry){
  logger.log("function: getallresponse - start", 'info', currentFileName);
  try{
    var ddd = q.defer();
    var a = f.toString();
    db.collection(a).find(qry).toArray(function(s,data){
      logger.log("collection: " + a + " find" , 'info', currentFileName);
      ddd.resolve(data);
    })
    return ddd.promise;
  } catch(e){
    log.log('check all response :: '+e,'error');
  }
}

function getallexpandSchema(schemaIds) {
  logger.log("function: getallexpandSchema - start", 'info', currentFileName);
  var deferred = q.defer();
  db.schema.find({_id : {$in : schemaIds}}).toArray(function (err , data) {
    deferred.resolve(data);
  });
  return deferred.promise;
}

//QC3-3079 - surjeet: generate auto number for Auto Generate Number's Attibute
function generateNextSequenceForAutoAttribute(schema, entry, cb) {
  var autoGenerateAttribute;
  if (schema && schema.attributes.length) {
    autoGenerateAttribute = _.find(schema.attributes, function (attribute) {
      return attribute.type.title == 'Auto Generate';
    });
  }

  if (!autoGenerateAttribute || entry._id) {
    cb(null, 'success'); //no auto generate attribute found.
    return;
  }
  console.log(autoGenerateAttribute);
  defaultRoute.getNextAutoNumber(autoGenerateAttribute._id).then(function(response){
    console.log('-----------------------------');
    console.log(response);
      if (response && response.autoGeneratedAttributeCounter) {
        entry.entries[autoGenerateAttribute['s#']] = entry.entries[autoGenerateAttribute['s#']] || {};
        entry.entries[autoGenerateAttribute['s#']]['attributes'] =
        autoGenerateAttribute.autoGenerateNumberPrefix + ' ' + response.autoGeneratedAttributeCounter + ' ' + autoGenerateAttribute.autoGenerateNumberSuffix;
        cb(null, 'success');
      }else {
        cb('No Data Found', null);
      }
  })
  .catch(function (ex) {
    cb(ex, null);
  });

  // Restangular
  // .all('defaultRoute/getAndGenerateNextSequence')
  // .post({attributeId: vm.autoGenerateAttribute._id})
  // .then(function (response) {
  //   if (response.status != '422' && response.data && entry && entry.entries) {
  //     entry.entries[vm.autoGenerateAttribute['s#']] = entry.entries[vm.autoGenerateAttribute['s#']] || {};
  //     entry.entries[vm.autoGenerateAttribute['s#']]['attributes'] =
  //     vm.autoGenerateAttribute.autoGenerateNumberPrefix + ' ' + response.data.autoGeneratedAttributeCounter + ' ' + vm.autoGenerateAttribute.autoGenerateNumberSuffix;
  //     cb(null, 'success');
  //   }else {
  //     cb('No Data Found', null);
  //   }
  // })
  // .catch(function (ex) {
  //   $log.log(ex);
  //   cb(ex, null);
  // })
}


module.exports = apiRoutes;
