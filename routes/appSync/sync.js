var express = require('express');
var db = require('../../lib/db');
var log = require('../../logger');
var _ = require('lodash');
var restler = require('restler');
var config = require('../../config/config.js');
var amqp = require('amqplib');
var q = require('q');
// var cache = require('./cache');
var valid = require('./validation');
var connect = require('../pluginCommon/salesforce.js')

var j = 0;
var i = 0;
var iteratation = 0;
var jsonObject;
var failed_records = [];
var successful_records = [];
var entityValues = [];
var entries = {};
var failreason = {};
var conne = {};
var syncId = +new Date();
var con = {
  double: 'Number',
  checkbox: 'string',
  currency: 'Currency',
  date: 'Date',
  datetime: 'string',
  number: 'Number',
  percent: 'Percentage',
  text: 'Text',
  string: 'None',
  reference: 'Number'
}
module.exports = function (app) {
  var apiRoutes = express.Router();

  //get apprecord,mapping,schema,pluginrecord's Id

  function getData(req, res, next) {
    failed_records = [];
    successful_records = [];

    var finalObject = {
          mappingObject: [],
          pluginAppRecordIds: [],
          perfeqtaAppRecordIds: [],
          processSchema: [],
          configDetail: {},
          sites: [],
          header: req.headers['x-authorization'],
          entityValues: []
      }

    q.all([
      getPlugin(req.query.id),
      getAppRecord(req.query.appId),
      getUserData(req.query.user),
      getSchema(req.query.appId),
      getPluginConfig(req.query.configId),
      getSites()
    ]).then(function (result) {

      finalObject.mappingObject = result[0];
      finalObject.perfeqtaAppRecordIds = result[1];
      finalObject.userdata = result[2];
      finalObject.processSchema = result[3];
      finalObject.configDetail = result[4];
      finalObject.sites = result[5];

      getEntityValue(finalObject.processSchema[0].entities);

      connect.getConected(finalObject.configDetail[0]).then(function (data) {
        console.log("------------------------data----------------------------");
        conne = data;
        conne.query("SELECT Id, Name FROM " + req.query.sapp, function (err, result) {
          if (err) { return console.error(err); }
          else {
            _.forEach(result.records, function (obj) {
              finalObject.pluginAppRecordIds.push(obj.Id);
              req.pluginAppRecordIds = finalObject.pluginAppRecordIds;
            });

            if (finalObject.perfeqtaAppRecordIds.length) {

              _.forEach(finalObject.perfeqtaAppRecordIds, function (obj) {

                if (!_.isUndefined(obj.salesforceId)) {
                  if (finalObject.pluginAppRecordIds.indexOf(obj.salesforceId) != -1) {
                    finalObject.pluginAppRecordIds.splice(finalObject.pluginAppRecordIds.indexOf(obj.salesforceId), 1);
                  }
                }
              });
            }

            if (finalObject.pluginAppRecordIds.length == 0) {
              res.send("There are no unique records to sync.");
            }

            cache.createCache(req.url.substr(1, (req.url.indexOf('?') - 1)), finalObject, 10 * 60);

            next();

          }
        });

      }, function (err) {

      })
    });
  }


  // mapp records with perfeqta app,get one by one plugin records

  function performSync(req, res) {

      var newobject = [];

      cache.getCache(req.url.substr(1, (req.url.indexOf('?') - 1))).then(function (resp) {
      newobject = resp;
      getDataWithValidation(req, res, newobject, i);
        }, function (err) {
               console.log(err);
         });
    }

  // function checkDataInCache(req,res,next) {

  //   cache.getCache('Mapping').then(function (resp) {
  //             finalObject = resp;
  //           }, function (err) {
  //               console.log(err);
  //               next();
  //           });

  // }

  apiRoutes.get('/manualsync', getData, performSync);

  app.use('/appsync', apiRoutes);
}


function getDataWithValidation(req, res, newobject, i) {
  console.log("---------------------------Recursion-------------------------------------------", i);

  var jsonObjects = [];

  if (!_.isUndefined(newobject) && !_.isUndefined(newobject.pluginAppRecordIds) && newobject.pluginAppRecordIds.length) {

    if (i < newobject.pluginAppRecordIds.length) {

      conne.sobject(req.query.sapp).retrieve(newobject.pluginAppRecordIds[i], function (err, account) {
        if (err) { return console.error(err); }
        jsonObjects = account;
        validateData(newobject, jsonObjects, i);
        i = i + 1;
        getDataWithValidation(req, res, newobject, i);
      });
    } else {
      console.log("all record inserted.");
      if (failed_records.length) {
        insertFailedRecords(failed_records).then(function (data) {
          console.log(data);
          res.send("Sync completed");
        }, function (err) {
          console.log(err);
        });
      }
      else {
        console.log("fpne");
         res.send("Sync completed");
      }
    }
  }
}

function validateData(newobject, jsonObject1, i) {
  entries = {};
  var levels = [];
  var levelIds = [];
  failreason = {};
  var sfData = [];
  var tempSites = [];

  _.forEach(newobject.mappingObject[0].psqueDetail, function (obj, index) {
    if (obj.paap.quetype == 'attributes') {

      sfData.push({ 'value': jsonObject1[obj.sapp.title], 'seq': obj.paap.comboId });

      if (!checkValidation(obj.paap, jsonObject1[obj.sapp.title], obj.sapp.stype)) {
        entries[obj.paap.comboId] = {
          'attributes': jsonObject1[obj.sapp.title]
        };
      }

    } else if (obj.paap.quetype == 'procedure') {

      var x = obj.paap.comboId.substring(obj.paap.comboId.indexOf('-') + 1, obj.paap.comboId.length);
      var y = obj.paap.comboId.substring(0, obj.paap.comboId.indexOf('-'));
      sfData.push({ 'value': jsonObject1[obj.sapp.title], 'seq': y });

      if (!checkValidation(obj.paap, jsonObject1[obj.sapp.title], obj.sapp.stype)) {
        entries[obj.paap.comboId] = {
          [x]: jsonObject1[obj.sapp.title]
        };
      }
    } else if (obj.paap.quetype == 'entity') {

      var x = obj.paap.comboId.substring(obj.paap.comboId.indexOf('-') + 1, obj.paap.comboId.length);
      var y = obj.paap.comboId.substring(0, obj.paap.comboId.indexOf('-'));

      sfData.push({ 'value': jsonObject1[obj.sapp.title], 'seq': y });

      if (!entityValidation(obj.paap, jsonObject1[obj.sapp.title], obj.sapp.stype, newobject.processSchema[0], entityValues)) {

        entries[obj.paap.comboId] = {
          [x]: jsonObject1[obj.sapp.title]
        };
      }
    } else if (obj.paap.quetype == 'site') {
      sfData.push({ 'value': jsonObject1[obj.sapp.title], 'seq': obj.paap.comboId });
      tempSites.push(jsonObject1[obj.sapp.title]);
      _.forEach(newobject.processSchema[0].allicableSites, function (s2, ix) {
        _.forEach(s2, function (s1) {
          if (s1.title == jsonObject1[obj.sapp.title]) {
            var sitobj = {
              levelId: ix,
              siteId: s1._id,
              title: s1.title
            };
            levels.push(sitobj);
            levelIds.push(s1._id);
          }
        });
      });
    }
  });


  if (newobject.mappingObject[0].isDefaultSites) {
    if (!_.isUndefined(newobject.mappingObject[0].levels)) {
      _.forEach(newobject.mappingObject[0].levels, function (obj) {
        var a = _.find(newobject.sites, function (o) {
          return o._id == obj._id;
        })
        var siteObj = {
          levelId: a.level._id, siteId: a._id, title: a.title
        }
        levels.push(siteObj);
        levelIds.push(a._id);
      })
    }
  }



  if (!_.isUndefined(levels)) {
    if (levels.length != newobject.processSchema[0].allicableSites.length) {
      _.forEach(tempSites, function (obj, ix) {
        failreason[ix] = 'Sites not filled according to their level.';
      });
    }
  }


  // console.log(failreason);
  entries.status = {};
  entries.status.status = 4;
  var dumpDataObj = {
    'reviewStatus': 'Draft',
    'entries': entries,
    'levels': levels,
    'levelIds': levelIds,
    'createdDateInfo': new Date(),
    'createdByInfo': { _id: newobject.userdata[0]._id, username: newobject.userdata[0].username, firstname: newobject.userdata[0].firstname, lastname: newobject.userdata[0].lastname },
    'parentVersion': newobject.mappingObject[0].appVersion,
    'salesforceId': jsonObject1.Id,
    'createdBy': newobject.userdata[0]._id,
    'modifiedBy' : newobject.userdata[0]._id,
    'isMajorVersion': true,
    'isAuditing' : false,
    'createdByName' : newobject.userdata[0].firstname + " " + newobject.userdata[0].lastname + " (" + newobject.userdata[0].username + ")",
    'modifiedByName' :  newobject.userdata[0].firstname + " " + newobject.userdata[0].lastname + " (" + newobject.userdata[0].username + ")",
    'createdDate' : new Date(),
    'modifiedDate' : new Date(),
    'failreason': failreason,
    'values': sfData,
    'mappingId': newobject.mappingObject[0]._id,

  }
  if(Object.keys(dumpDataObj.failreason).length == 0){
    delete dumpDataObj.failreason;
  }

  // console.log(failreason);
  // if (Object.keys(failreason).length) {
    // temp = {
    //   'failreason': failreason,
    //   'values': sfData,
    //   'mappingId': newobject.mappingObject[0]._id
    // }
    // var fails = Object.assign(dumpDataObj, temp);
    failed_records.push(dumpDataObj);
    i = i + 1;
  // }
  // else {
  //   console.log("-----------------restlar-----------------------");
  //    successful_records.push(dumpDataObj);

    // restler.postJson('http://localhost:3000/entries/' + newobject.mappingObject[0].appId, dumpDataObj, options2)
    //   .on('complete', function (cmplt) {
    //     console.log("enterde by restler");
    //     console.log(cmplt);
    //     i = i + 1;
    //   })
    //   .on('timeout', function () {
    //     console.log("enterde by timeout");
    //   })
    //   .on('error', function (err) {
    //     console.log("not enterde by restler");
    //     console.log(err);
    //   });
  // }
}





// db call for plugin record

function getPlugin(id) {
  var deferred = q.defer();
  db.plugin.find({ _id: db.ObjectID(id) }).toArray(function (err, data1) {
    if (err) {
      deferred.reject(err);
    }
    else {
      deferred.resolve(data1);
    }
  });
  return deferred.promise;
}

// db call for app records

function getAppRecord(appId, version) {
  var deferred = q.defer();
  db.collection(appId).find({}, { _id: 1, salesforceId: 1 }).toArray(function (err, data) {
    if (err) {
      deferred.reject(err);
    }
    else {
      deferred.resolve(data);
    }
  });
  return deferred.promise;
}

// db call for user data

function getUserData(user) {
  var deferred = q.defer();
  db.users.find({ _id: db.ObjectID(user) }).toArray(function (err, usr) {
    if (err) {
      deferred.reject(err);
    }
    else {
      deferred.resolve(usr);
    }
  });
  return deferred.promise;
}

// get perticular schema

function getSchema(id) {
  var deferred = q.defer();
  db.schema.find({ _id: db.ObjectID(id) }).toArray(function (err, data) {
    if (err) {
      deferred.reject(err);
    }
    else {
      deferred.resolve(data);
    }
  });
  return deferred.promise;
}

function getSites() {
  var deferred = q.defer();
  db.sites.find({}).toArray(function (err, data) {
    if (err) {
      deferred.reject(err);
    }
    else {
      deferred.resolve(data);
    }
  });
  return deferred.promise;
}


function getPluginConfig(id) {

  var deferred = q.defer();
  db.pluginConfig.find({ _id: db.ObjectID(id) }).toArray(function (err, data) {
    if (err) {
      deferred.reject(err);
    }
    else {
      deferred.resolve(data);
    }
  });
  return deferred.promise;

}

function insertFailedRecords(failed) {
  var deferred = q.defer();
  db.collection('pluginTempRecords').insertMany(failed, function (err, inserted) {
    if (err) {
      deferred.reject(err);
    }
    else {
      deferred.resolve(inserted);
    }
  })
  return deferred.promise;
}



//entityValues

function getEntityValue(entities) {
  if (entities.length) {
    if (iteratation !== entities.length) {
      db[entities[iteratation]._id].find({ isDeleted: false }).toArray(function (err, data) {
        console.log(entityValues);
        entityValues[entities[iteratation]._id] = [];
        entityValues[entities[iteratation]._id].push(data);
        iteratation++;
        getEntityValue(entities);
      });
    }
  }
}


// entity validation

function entityValidation(paap, value, stype, processSchema, entityValue) {
  if (con[stype]) {
    if (paap.queformat.format.title == con[stype]) {
      var x = paap.comboId.substring(paap.comboId.indexOf('-') + 1, paap.comboId.length);
      var y = paap.comboId.substring(0, paap.comboId.indexOf('-'));
      var entity = _.filter(processSchema.entities, { 's#': x });
      if (!_.isUndefined(entity)) {

        if (paap.queformat.format.title == 'Date') {

          values = _.filter(entityValues[entity[0]._id][0], function (entityValue) {
            console.log("---");
            console.log("--||--");
            value = new Date(value).toISOString();
            return new Date(entityValue.entityRecord[y]).setHours(0, 0, 0, 0) == new Date(value).setHours(0, 0, 0, 0);
          });
        }

        else {
          var values = _.filter(entityValue[entity[0]._id][0], function (entityValue) {
            return entityValue.entityRecord[y] == value;
          });
        }
      }
      if (values && values.length > 0) {
        console.log("true");
        //rather than keyvalue questions
        var questions = _.filter(entity[0].questions, { 'isKeyValue': false });
        _.forEach(questions, function (q) {
          entries[q['s#'] + '-' + entity[0]['s#']] = {};
          entries[q['s#'] + '-' + entity[0]['s#']][entity[0]['s#']] = values[0].entityRecord[q['s#']];
        });
      } else {
        failreason[x] = "Key attribute is not matched."
        return true;
      }
    } else {
      failreason[x] = "Data type mismatched."
      return true;
    }
  } else {
    return true;
  }
}


function checkValidation(paap, value, stype) {
  var x;
  if (con[stype]) {
    if (paap.queformat.format.title == con[stype]) {

      if (Object.keys(paap.quevalidation).length) {
        if (paap.queformat.format.title == 'Date' && paap.quevalidation.length) {
          if (valid.performedDateValidation(paap.quevalidation, value) && value !== '') {
            return false;
          } else {
            if (paap.comboId.indexOf('-') != -1) {

              x = paap.comboId.substring(0, paap.comboId.indexOf('-'));

              failreason[x] = !_.isUndefined(paap.quevalidation.condition.validationMessage) ? paap.quevalidation.condition.validationMessage : "Datatype mismatch";

            } else {

              failreason[paap.comboId] = !_.isUndefined(paap.quevalidation.condition.validationMessage) ? paap.quevalidation.condition.validationMessage : "Datatype mismatch";

            }
            return true;
          }
        } else {
          if (!_.isUndefined(paap.quevalidation.regularExpression) && paap.quevalidation.regularExpression) {
            if ((new RegExp(paap.quevalidation.regularExpression.regEx)).test(value) && value !== '') {
              return false;
            } else {
              if (paap.comboId.indexOf('-') != -1) {

                x = paap.comboId.substring(0, paap.comboId.indexOf('-'));

                failreason[x] = !_.isUndefined(paap.quevalidation.regularExpression.validationMessage) ? paap.quevalidation.regularExpression.validationMessage : "value does not match with regular expression";

              } else {

                failreason[paap.comboId] = !_.isUndefined(paap.quevalidation.condition.validationMessage) ? paap.quevalidation.condition.validationMessage : "value does not match with regular expression";

              }
              return true;
            }
          }
          if (!_.isUndefined(paap.quevalidation.validationSet) && paap.quevalidation.validationSet && !_.isUndefined(paap.quevalidation.validationSet.existingSet)) {

            if ((_.findIndex(paap.quevalidation.validationSet.existingSet.values, function (o) { return o.value == value; }) != -1) && value !== '') {
              return false;
            }
            else {
              if (paap.comboId.indexOf('-') != -1) {
                x = paap.comboId.substring(0, paap.comboId.indexOf('-'));
                failreason[x] = !_.isUndefined(paap.quevalidation.validationSet.existingSet.validationMessage) ? paap.quevalidation.validationSet.existingSet.validationMessage : "Value does not match with set value.";

              } else {

                failreason[paap.comboId] = !_.isUndefined(paap.quevalidation.condition.validationMessage) ? paap.quevalidation.condition.validationMessage : "Value does not match with set value.";

              }
              return true;
            }
          }
          if (!_.isUndefined(paap.quevalidation.condition) && !_.isUndefined(paap.quevalidation.condition.conditionTitle) && paap.quevalidation.condition) {
            if (valid.validateCondition(paap.quevalidation, value) && value !== '') {
              return false;
            } else {
              if (paap.comboId.indexOf('-') != -1) {
                x = paap.comboId.substring(0, paap.comboId.indexOf('-'));

                failreason[x] = !_.isUndefined(paap.quevalidation.condition.validationMessage) ? paap.quevalidation.condition.validationMessage : "Value does not satisfy the condition";
              } else {

                failreason[paap.comboId] = !_.isUndefined(paap.quevalidation.condition.validationMessage) ? paap.quevalidation.condition.validationMessage : "Value does not satisfy the condition";

              }
              return true;
            }
          }
        }
      } else {
        return false;
      }
    }
    else {
      if (paap.comboId.indexOf('-') != -1) {
        x = paap.comboId.substring(0, paap.comboId.indexOf('-'));
        failreason[x] = "Datatype mismatch";
      } else {

        failreason[paap.comboId] = "Datatype mismatch";

      }
      return true;
    }
  } else {
    return true;
  }
}
