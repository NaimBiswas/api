(function () {
    'use strict';
    var config = require('../../config/config.js');
    var q = require('q');
    var cache = require('express-redis-cache')();
    // var cache;
    // if (config.isRedisOn) {
    //     cache = require('express-redis-cache')();
    // }

    var service = function () { };

    function createCache(name, data, expire) {
        try {
            var sData = JSON.stringify(data);
            cache.add(name, sData, { expire: (expire || config.cacheSetting.expire) }, function (err, data) {
                if (err) {
                    console.log(err);
                }
            });
        }
        catch (e) {
            console.log(e);
        }

    }
    function getCache(name) {
        var defer = q.defer();
        try {
            cache.get(name, function (error, entries) {
                if (error) {
                    next();
                    defer.reject(error);
                }
                if (entries && entries.length && entries[0].body) {
                    var jData = JSON.parse(entries[0].body);
                    defer.resolve(jData);
                } else {
                    defer.reject({ message: 'No Data' });
                }

            });
        }
        catch (e) {
            setTimeout(function () {
                defer.reject(e);
            }, 0);

        }

        return defer.promise;
    }
    function clearCache(name) {

    }

    service.prototype.createCache = createCache;
    service.prototype.getCache = getCache;
    service.prototype.clearCache = clearCache;

    module.exports = new service();
})();
