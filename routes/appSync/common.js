// db call for plugin record
'use strict'
var db = require('../../lib/db');
var log = require('../../logger');
var _ = require('lodash');
var restler = require('restler');
var config = require('../../config/config.js');
var q = require('q');
// var cache = require('../appSync/cache');
var valid = require('../appSync/validation');
var connect = require('../pluginCommon/salesforce.js')
var amqp = require('amqplib/callback_api');
var common = require('../appSync/common');

function getPlugin(id) {
    console.log('----------getPlugin---------');

    var deferred = q.defer();
    db.plugin.find({ _id: db.ObjectID(id), isActive: true  }).toArray(function(err, data1) {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve(data1);
        }
    });
    return deferred.promise;
}

// db call for plugin temp records

function getTempRecord(mappingId) {
    console.log('----------getTempRecord---------');
    var deferred = q.defer();
    db.collection('pluginTempRecords').find({ 'mappingId': mappingId, 'failreason': { $exists: false } }, { _id: 1, salesforceId: 1 }).toArray(function(err, datas) {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve(datas);
        }
    });
    return deferred.promise;
}

// db call for user data

function getUserData(user) {
    console.log('----------getUserData---------');

    var deferred = q.defer();
    db.users.find({ _id: db.ObjectID(user) }).toArray(function(err, usr) {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve(usr);
        }
    });
    return deferred.promise;
}

// get perticular schema

function getSchema(id, appversion, type) {
    console.log('----------getSchema---------');
    // console.log(id);
    // console.log(appversion);
    appversion = parseFloat(appversion);
    // console.log(type);
    var deferred = q.defer();
    if (type == 'app') {
        db.schemaAuditLogs.find({ identity: db.ObjectID(id), version: appversion }).toArray(function(err, data) {
            if (err) {
                // console.log(err);
                deferred.reject(err);
            } else {
                // console.log(data);
                deferred.resolve(data);
            }
        });

    } else {
        // console.log("99999999999999999999");
        db.entitiesAuditLogs.find({ identity: db.ObjectID(id), version: appversion }).toArray(function(err, data12) {
            if (err) {
                console.log(err);
                deferred.reject(err);
            } else {
                // console.log(data12);
                deferred.resolve(data12);
            }
        });
    }
    return deferred.promise;
}

function getAppRecord(id, type) {
    console.log('----------getSchema---------');
    // console.log(id);
    // console.log(appversion);
    // appversion = parseFloat(appversion);
    var deferred = q.defer();
    if(type == 'app'){
        db.collection(id).find({'entries.status.status' : {$ne:3}}).toArray(function(err, records) {
            if (err) {
                // console.log(err);
                deferred.reject(err);
            } else {
                // console.log(records);
                deferred.resolve(records);
            }
        });
    }
    else{
        db.collection(id).find({isDeleted:false, 'entries.status.status' : {$ne:3}}).toArray(function(err, records) {
            if (err) {
                // console.log(err);
                deferred.reject(err);
            } else {
                // console.log(records);
                deferred.resolve(records);
            }
        });
    }
    return deferred.promise;
}

function getSites() {
    var deferred = q.defer();
    db.sites.find({}).toArray(function(err, data) {
        if (err) {
            deferred.reject(err);
        } else {
            // console.log(data);
            deferred.resolve(data);
        }
    });
    return deferred.promise;
}


function getPluginConfig(id) {
    if (id) {
        var deferred = q.defer();
        db.pluginConfig.find({ _id: db.ObjectID(id) }).toArray(function(err, data) {
            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve(data);
            }
        });
        return deferred.promise;
    } else {
        return 0;
    }
}

function sets() {

    var deferred = q.defer();
    db.sets.find({}).toArray(function(err, data) {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve(data);
        }
    });
    return deferred.promise;

}


function getTypes() {

    var deferred = q.defer();
    db.types.find({ title: 'Textbox' }).toArray(function(err, data) {
        if (err) {
            deferred.reject(err);
        } else {
            // console.log("--------------vijeta-------------------");
            // console.log(data);
            deferred.resolve(data);
        }
    });
    return deferred.promise;

}

function getSchemacurrentversion(id, type) {
    var deferred = q.defer();
    if (type == 'app') {
        db.schema.find({ _id: db.ObjectID(id) }).toArray(function(err, data) {
            if (err) {
                // console.log(err);
                deferred.reject(err);
            } else {
                // console.log(data);
                deferred.resolve(data);
            }
        });
        return deferred.promise;
    } else {
        db.entitiesAuditLogs.find({ _id: db.ObjectID(id), isActive: true }).toArray(function(err, data12) {
            if (err) {
                // console.log(err);
                deferred.reject(err);
            } else {
                // console.log(data12);
                deferred.resolve(data12);
            }
        });
        return deferred.promise;
    }

}

module.exports = {
    getPluginConfig: getPluginConfig,
    getSites: getSites,
    getSchema: getSchema,
    getUserData: getUserData,
    getTempRecord: getTempRecord,
    getPlugin: getPlugin,
    sets: sets,
    getAppRecord: getAppRecord,
    getTypes: getTypes,
    getSchemacurrentversion: getSchemacurrentversion
}
