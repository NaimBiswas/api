var express = require('express');
var db = require('../../lib/db');
var q = require('q');
var _ = require('lodash');
var config = require('../../config/config.js');
var amqp = require('amqplib/callback_api');

module.exports = function (app) {
  var apiRoutes = express.Router();
  function syncProcess(req, res) {

    amqp.connect('amqp://' + config.mq.connection, function (err, conn1) {
      if (err) {
        res.status(501).send("CNE");// CNE:Connection Not Established
      }

      else {
        conn1.createChannel(function (err, ch) {
          if (err) {
            res.status(501).send("CNC");// CNE:Channel Not Created
          }

          else {
            db.collection('plugin').update({ _id: db.ObjectID(req.query.id) }, { $set: { isSyncing: false } }, function (err, res) {
              if (err) {
                res.status(501).send("Data not updated");
              }
            });

            var q = config.mq.pluginMq.queue;
            ch.assertQueue(q, { durable: false });
            ch.sendToQueue(q, new Buffer(JSON.stringify(req.query)), { persistent: true });
            res.status(200).send("OK");
          }
        });

        setTimeout(function () {
          conn1.close();
        }, 500);
        
      }
    });
  }

  apiRoutes.get('/manualsync', syncProcess);
  app.use('/appsync', apiRoutes);
}
