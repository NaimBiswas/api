(function() {
    'use strict';
    var _ = require('lodash');
    var moment = require('moment')
    var logger = require('../../logger');
    var service = function() {};

    /* var con = {
        double: 'Number',
        checkbox: 'string',
        currency: 'Currency',
        date: 'Date',
        datetime: 'string',
        number: 'Number',
        percent: 'Percentage',
        text: 'Text',
        string: 'None',
        reference: 'Number'
    } */

    /* function checkValidation(paap, value, stype) {

        if (con[stype]) {
            if (paap.queformat.format.title == con[stype]) {

                if (Object.keys(paap.quevalidation).length) {
                    if (paap.queformat.format.title == 'Date' && paap.quevalidation.length) {
                        if (performedDateValidation(paap.quevalidation, value)) {
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        if (!_.isUndefined(paap.quevalidation.regularExpression) && paap.quevalidation.regularExpression) {
                            if ((new RegExp(paap.quevalidation.regularExpression.regEx)).test(value)) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                        if (!_.isUndefined(paap.quevalidation.validationSet) && paap.quevalidation.validationSet && !_.isUndefined(paap.quevalidation.validationSet.existingSet)) {

                            if (_.findIndex(paap.quevalidation.validationSet.existingSet.values, function(o) { return o.value == value; }) != -1) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                        if (!_.isUndefined(paap.quevalidation.condition) && !_.isUndefined(paap.quevalidation.condition.conditionTitle) && paap.quevalidation.condition) {
                            if (validateCondition(paap.quevalidation, value)) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                    }
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    } */

    function validateCondition(validation, value, queformat) {
        if(queformat && queformat.format && queformat.format.title == 'None'){
            value = value.length;
        }

        switch (validation.condition.conditionTitle) {
            case '>':
                return value > parseFloat(validation.condition.value);
            case '<':
                return value < parseFloat(validation.condition.value);
            case '>=':
                return value >= parseFloat(validation.condition.value);
            case '<=':
                return value <= parseFloat(validation.condition.value);
            case '=':
                return value == parseFloat(validation.condition.value);
            case '!=':
                return value != parseFloat(validation.condition.value);
            case '>= && <=':
                return (value >= parseFloat(validation.condition.minimum) && value <= parseFloat(validation.condition.maximum));
            case '!(>= && <=)':
                return !(value >= parseFloat(validation.condition.minimum) && value <= parseFloat(validation.condition.maximum));
            default:
                return false;
        }
    }

    function performedTimeValidation(validation, value) {
        if (_.isUndefined(validation) || (!_.isUndefined(validation) && _.isUndefined(validation.condition))) {
            return true
        }
        var scope = {};
        scope = _.cloneDeep(validation.condition);
        var h;
        var m;
        var mdt;
        if (value.indexOf(':') !== -1) {
            var timeValue = value.split(':');
            if ((!isNumber(timeValue[0]) || parseInt(timeValue[0]) > 24) && (!isNumber(timeValue[1]) || parseInt(timeValue[1]) > 60)) {
                value = new Date();
            } else {
                h = isNumber(timeValue[0]) ? parseInt(timeValue[0]) : 0;
                m = isNumber(timeValue[1]) ? parseInt(timeValue[1]) : 0;
                value = moment(new Date().setHours(h, m, 0, 0)).set('year', 1970).set('month', 0).set('date', 1);
            }
        } else {
            value = new Date();
        }

        if (!_.isUndefined(scope.value)) {
            if (scope.value.indexOf(':') !== -1) {
                var timeValue1 = scope.value.split(':');
                if ((!isNumber(timeValue1[0]) || parseInt(timeValue1[0]) > 24) && (!isNumber(timeValue1[1]) || parseInt(timeValue1[1]) > 60)) {
                    scope.value = new Date();
                } else {
                    scope.value = moment(new Date().setHours((isNumber(timeValue1[0]) ? parseInt(timeValue1[0]) : 0, m, 0, 0), (isNumber(timeValue1[1]) ? parseInt(timeValue1[1]) : 0))).set('year', 1970).set('month', 0).set('date', 1);
                }
            } else {
                scope.value = new Date();
            }
        }

        if (!_.isUndefined(scope.minimum)) {
            if (scope.minimum.indexOf(':') !== -1) {
                mdt = new Date(scope.minimum);
                mdt = new Date(mdt.getTime() + (mcdiff * 60 * 1000));
                h = mdt.getHours();
                m = mdt.getMinutes();
                scope.minimum = moment(new Date().setHours(h, m, 0, 0)).set('year', 1970).set('month', 0).set('date', 1);
            } else {
                scope.minimum = new Date();
            }
        }

        if (!_.isUndefined(scope.maximum)) {
            if (scope.maximum.indexOf(':') !== -1) {
                mdt = new Date(scope.maximum);
                mdt = new Date(mdt.getTime() + (mcdiff * 60 * 1000));
                h = mdt.getHours();
                m = mdt.getMinutes();
                scope.maximum = moment(new Date().setHours(h, m, 0, 0)).set('year', 1970).set('month', 0).set('date', 1);

            } else {
                scope.maximum = new Date();
            }
        }


        switch (validation.condition.conditionTitle) {
            case '>':
                if (value > scope.value) {
                    return true;
                } else {
                    return false;
                }

            case '<':
                if (value < scope.value) {
                    return true;
                } else {
                    return false;
                }
            case '>=':
                if (value >= scope.value) {
                    return true;
                } else {
                    return false;
                }
            case '<=':
                if (value <= scope.value) {
                    return true;
                } else {
                    return false;
                }
            case '=':
                if (value == scope.value) {
                    return true;
                } else {
                    return false;
                }
            case '!=':
                if (value != scope.value) {
                    return true;
                } else {
                    return false;
                }
            case '>= && <=':
                if (value >= scope.mimimum && value <= scope.maximum) {
                    return true;
                } else {
                    return false;
                }
            case '!(>= && <=)':
                if (!(value >= scope.mimimum && value <= scope.maximum)) {
                    return true;
                } else {
                    return false;
                }
            default:
                return false;
        }
    }

    function performedDateValidation(validation, value) {
        logger.log("-----------------indate validation------------------------");
        if (validation.condition.today === '1') {
            validation.condition.value = new Date().setHours(0, 0, 0, 0);
        }
        switch (validation.condition.conditionTitle) {
            case '>':
                if (new Date(value).setHours(0, 0, 0, 0) > new Date(validation.condition.value).setHours(0, 0, 0, 0)) {
                    return true;
                } else {
                    return false;
                }
            case '<':
                if (new Date(value).setHours(0, 0, 0, 0) < new Date(validation.condition.value).setHours(0, 0, 0, 0)) {
                    return true;
                } else {
                    return false;
                }
            case '>=':
                if (new Date(value).setHours(0, 0, 0, 0) >= new Date(validation.condition.value).setHours(0, 0, 0, 0)) {
                    return true;
                } else {
                    return false;
                }
            case '<=':
                if (new Date(value).setHours(0, 0, 0, 0) <= new Date(validation.condition.value).setHours(0, 0, 0, 0)) {
                    return true;
                } else {
                    return false;
                }
            case '=':
                if (moment.utc(new Date(value).setHours(0, 0, 0, 0)).isSame(new Date(validation.condition.value).setHours(0, 0, 0, 0))) {
                    return true;
                } else {
                    return false;
                }
            case '!=':
                if (!moment.utc(new Date(value).setHours(0, 0, 0, 0)).isSame(new Date(validation.condition.value).setHours(0, 0, 0, 0))) {
                    return true;
                } else {
                    return false;
                }
            case '>= && <=':
                if ((new Date(value).setHours(0, 0, 0, 0) >= new Date(validation.condition.minimumDate).setHours(0, 0, 0, 0) && new Date(value).setHours(0, 0, 0, 0) <= new Date(validation.condition.maximumDate).setHours(0, 0, 0, 0))) {
                    return true;
                } else {
                    return false;
                }
            case '!(>= && <=)':
                if (!(new Date(value).setHours(0, 0, 0, 0) >= new Date(validation.condition.minimumDate).setHours(0, 0, 0, 0) && new Date(value).setHours(0, 0, 0, 0) <= new Date(validation.condition.maximumDate).setHours(0, 0, 0, 0))) {
                    return true;
                } else {
                    return false;
                }
            default:
                return false;
        }
    };

    function isNumber(value) {
        return (/^\d+$/.test(value) || (/^\s*[-+]?(\d*\.?\d+|\d+\.)(e[-+]?[0-9]+)?\s*$/i.test(value)))
    }
    service.prototype.validateCondition = validateCondition;
    service.prototype.performedDateValidation = performedDateValidation;
    service.prototype.performedTimeValidation = performedTimeValidation;
    module.exports = new service();

})();