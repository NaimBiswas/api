; (function () {
    'use strict';
    var db = require('../../lib/db');

    var SearchFavorites = function SearchFavorites(req, res, next) {
        db["searchFavorites"].removeAsync({ _id:  db.ObjectID(req.params.id)})
            .then(function (data) {
                res.json(data);
            });
    }

    var LinkedTbl = function LinkedTbl(req, res, next) {
        db["linkedTbl"].removeAsync({ linkedToId: req.params.id})
            .then(function (data) {
                res.json(data);
            });
    }

    module.exports = {
        SearchFavorites: SearchFavorites,
        LinkedTbl: LinkedTbl
    };

})();
