/*
author - mahammad.c - mahammad.c@productivet.com
all delete api are here
*/
(function () {
  'use strict';
  var express = require('express');
  var Delete = require('./delete.js');
  module.exports = function (app) {
    var apiRoutes = express.Router();
    apiRoutes.delete('/searchFavorites/:id', Delete.SearchFavorites);
    apiRoutes.delete('/linkedTbl/:id', Delete.LinkedTbl);
    app.use('/' , apiRoutes);
  }
})();
