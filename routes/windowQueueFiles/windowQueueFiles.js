'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var validate = require('../../lib/validator');
var db = require('../../lib/db');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'windowQueueFiles',
    collection: 'windowQueueFiles',
    schema: schema,
    softSchema: softSchema
  }
});

/**
 * @swagger
 * definition:
 *   Useractivity:
 *     properties:
 *       id:
 *         type: integer
 *       username:
 *         type: string
 *       date:
 *         type: dateTime
 *       access:
 *         type: string
 *       ip:
 *         type: integer
 *       hostName:
 *         type: string
 *       roles:
 *         type: string
 */

/**
 * @swagger
 * /loginattempts:
 *   get:
 *     tags: [User Activity]
 *     description: Returns all user activities
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of role that you want to see in the output (Exp Values -> [ username,ip,hostName,date,roles,access])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of role with which you want to sort output (Exp Values -> [ -date])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of user activities
 *         schema:
 *           $ref: '#/definitions/Useractivity'
 */


module.exports = route.router;
