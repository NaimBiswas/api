'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var rId = types.rId;
var object = types.object.bind(types);
var rString = types.rString;
var rBool = types.rBool;
var rNumber = types.rNumber;
var bool = types.bool;
var any = types.any;
var string = types.string;

var schema = {
};

module.exports = schema;
