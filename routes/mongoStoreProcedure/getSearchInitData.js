(function () {
  'use strict';

  var getInitSearchData = function(collectionName, idsStr, isFrom) {
    var ids = idsStr.split(':');
    var idss=[];
    ids.forEach(function(id){
      idss.push(ObjectId(id))
    });
    ids=idss;
    var returnObj = {
      data: [],
      type: ''
    };
    var entityAaya;
    var attributeAaya;
    if (collectionName == 'schema') {
      returnObj.type = "schema";
      returnObj.data = [];
      entityAaya = {};
      attributeAaya = {};
      parseAllSchema(ids, entityAaya, attributeAaya,1);
    }
    else if (collectionName === 'masterqcsettings') {
      returnObj.type = 'master';
      entityAaya = {};
      attributeAaya = {};
      db[collectionName].find({ _id: { $in: ids } }).forEach(function (ms) {
        //get all schema,push in a array and run upper list
        if (ms && ms.schema && ms.schema.length) {
          var objIds = [];
          ms.schema.forEach(function (sch) {
            objIds.push(ObjectId(sch._id));
          });
          parseAllSchema(objIds, entityAaya, attributeAaya,2);
        }
      });
    }
    function getAttributes(schema, attributeAaya) {
      if (schema && schema.attributes && schema.attributes.length) {
        schema.attributes.forEach(function (attrib) {
          if (attrib && !attributeAaya[attrib._id]) {
            attributeAaya[attrib._id] = true;
            // QC3-10437 by Yamuna
            if(isFrom){
              attrib.keys=[];
              attrib.keys.push(attrib['s#']);
              attrib.isEntity=false;
              returnObj.data.push(attrib);
            }else{
              //fetch the latest attribute
              var newAttrib = db.attributes.findOne(ObjectId(attrib._id));
              if (newAttrib) {
                newAttrib.keys=[];
                newAttrib.keys.push(attrib['s#']);
                newAttrib.isEntity=false;
                returnObj.data.push(newAttrib);
              }
            }
          }else{
            returnObj.data.forEach(function (attayi) {
              if(attayi._id==attrib._id){
                attayi.keys.push(attrib['s#']);
              }
            });
          }
        });
      }
    }
    function getEntity(schema, entityAaya) {
      if (schema && schema.entities && schema.entities.length) {
        schema.entities.forEach(function (oldEntity) {
          var entity =db.entities.findOne(ObjectId(oldEntity._id));
          if (entity && entity.questions && entity.questions.length) {
            entity.questions.forEach(function (question) {
              if (question && question.isKeyValue && !entityAaya[question.parentId]) {
                entityAaya[entity._id] = true;
                //get Entity
                question = getEntityRecord(entity._id.valueOf(), question['s#'], question);
                question.keys=[];
                question.keys.push(question['s#']+'-'+oldEntity['s#']);
                question.isEntity=true;
                question.title = entity.title + ' - ' + question.title;
                returnObj.data.push(question);
              }else{
                returnObj.data.forEach(function (entayi) {
                  if(question._id==entayi._id){
                    entayi.keys.push(question['s#']+'-'+oldEntity['s#']);
                  }
                });
              }
            });
          }
        });
      }
    }
    function getEntityRecord(id, seq, question) {
      var project = {};
      var tempKeyList = [];
      project['entityRecord.' + seq] = 1;
      project['entityRecord.isActive'] = 1; 
      //By Miral
      project['isDeleted'] = 1;
      if(isFrom === 'true' || isFrom === true){
        db[id].find({}, project).forEach(function (record) {
          //QC3-8018: mahammad - ADD CONDITION 'if(record && record.entityRecord && record.entityRecord[seq])'
          if(record && record.entityRecord && (record.entityRecord[seq] == 0 || record.entityRecord[seq]) && (record.entityRecord[seq]).toString()){//QC3-8373: mahammad :- toString() for 0
            var tmpVal = {
              'value': record.entityRecord[seq],
              'isActive': true
            };
            if (!record.entityRecord.isActive) {
              tmpVal.isActive = false
            }
            tmpVal.isDeleted = record.isDeleted;
            tempKeyList.push(tmpVal);
          }
        });
      }
      
      question['_id'] = id;
      if(isFrom === 'true' || isFrom === true){
        question['entityRecordKeyValues'] = tempKeyList;
      }
      return question;
    }
    function parseAllSchema(ids, entityAaya, attributeAaya,type) {
      entityAaya = entityAaya || {};
      attributeAaya = attributeAaya || {};
      if(type==2){
        collectionName='schema'
      }
      db[collectionName].find({ _id: { $in: ids } }).forEach(function (schema) {
        //1 : get Attributes.
        getAttributes(schema, attributeAaya);
        //2 : get Entity.
        getEntity(schema, entityAaya);
      });
    }
    return returnObj;
  }

  module.exports = {
    getInitSearchData: getInitSearchData
  }
})();
