(function () {
    'use strict';
    var getScheduleSchemaWise = function (page, pageSize, schemaId) {
        if (!schemaId) {
            return "Provide Schema Id";
        }
        if (!pageSize) {
            pageSize = 10;
        }
        if (!page) {
            page = 1;
        }
        var skip = ((page - 1) * pageSize);

        return db.calendar.aggregate([
            { $match: { 'schema._id': ObjectId(schemaId) } },
            {
                $lookup: {
                    from: schemaId,
                    as: 'status',
                    foreignField: 'calendarIds',
                    localField: '_id'
                }
            },
            {
                $project: {
                    'date': 1, 'time': 1, 'schema': 1, 'title': 1, type: 1, notified: 1, status: {
                        $cond: { if: { $gte: [{ '$size': "$status" }, 1] }, then: 'Complete', else: 'Pending' }
                    }
                }
            },
            { $skip: skip },
            { $limit: pageSize }

        ]);
    }

    var getAllSchedule = function (page, pageSize, token) {
        if (!pageSize) {
            pageSize = 10;
        }
        if (!page) {
            page = 1;
        }
        var skip = ((page - 1) * pageSize);
        var calArray = [];

        var obj = db.apiAccessConfig.findOne({ apitoken: token });
        var apps = [];
        if (obj && obj.apps && obj.apps.length) {
            //convert string to ObjectId
            obj.apps.forEach(function (app) {
                if (typeof app == 'string' && app.length == 24) {
                    app = ObjectId(app);
                }
                apps.push(app);
            });
        }
        //printjson(apps);
        db.calendar.find({ 'schema._id': { $in: apps } }).skip(skip).limit(pageSize).forEach(function (cal) {
            if (cal && cal.schema && cal.schema._id) {
                var strSchemaId = cal.schema._id.valueOf();
                var strCalId = cal._id.valueOf();
                var cnt = db[strSchemaId].find({ calendarIds: strCalId }).count();
                calArray.push({ _id: cal._id, AppName: cal.schema.title,Title:cal.title,Type:cal.type,Time:cal.time,Date:cal.date,Nototified:cal.notified, status: ((cnt > 0) ? 'Complete' : 'Pending') });
            }
        });
        return calArray;

    }
    module.exports = {
        getScheduleSchemaWise: getScheduleSchemaWise,
        getAllSchedule: getAllSchedule
    }
})();
