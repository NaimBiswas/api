/**
 * author : akashdeep.s - akashdeep.s@productivet.com
 * desc : initialize all DB Stored Procedure and load Scripts
 */
(function () {
    'use strict';

    var db = require('../../lib/db');
    var Code = require('mongodb').Code;
    var fs = require('fs');
    var _ = require('lodash');
    var promiseArray = [];

    fs
        .readdirSync(__dirname)
        .filter(function (file) {
            return (file.indexOf(".") !== 0) && (file !== "index.js");
        })
        .forEach(function (file) {
            var context = require('./' + file);
            _.forEach(context, function (fle,key) {
                promiseArray.push(db.collection("system.js").save(
                    {
                        _id: key,
                        value: new Code(context[key].toString())
                    }
                    , { upsert: true }));
            });
        });
    Promise.all(promiseArray).then(function (data) {
        db.eval('db.loadServerScripts();');
    }, function (error) {
        //console.log();
    });
})();
