'use strict'
/**
 * @name dmsperfeqtamapping schema
 * @author Lalit Godiya <lalit.g@productivet.com>
 *
 * @version 1.0
 */

var types = require('../../lib/validator/types');

var rBigString = types.rBigString;
var rString = types.rString;
var number = types.number;
var string = types.string;

var schema = {
  fileName : rString.label('fileName'),
  version: number.label('version'),
  appName: rBigString.label('appName'),
  timeZone: string.label('Time Zone')
 };

module.exports = schema;
