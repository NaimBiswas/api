'use strict'

var express = require('express');
var router = express.Router();
var fs = require('fs-extra');
var db = require('../../lib/db');
var _ = require('lodash');
var moment = require('moment');
var logger = require('../../logger');
var currentFileName = __filename;
var debug = require('../../lib/logger').debug('routes:upload');

//Get file
router.get('/:fetchEntityRecords', function(req, res, next) {});

//Post file
router.post('/', function(req, res, next) {
  logger.log("API Called : api/fetchEntityRecords: POST method" , 'info', currentFileName);

  db
  .collection(db.ObjectID(req.body._id).toString())
  //QC3-6941 Yamuna
  .find({'entityRecord.isActive':true})
  .toArray(function (err, entities){
    try {
      logger.log("collection:entityRecord - success" , 'info', currentFileName);

      var entitySelection = [];
      if (!_.isUndefined(req.body) && !_.isUndefined(req.body.entityFilters) && !_.isUndefined(req.body.entityFilters[0]) && !_.isUndefined(req.body.entityFilters[0].conditions)) {
        _.forEach(entities, function (entity) {
          var transformedModel = _.reduce(entity.entityRecord,function (a, value, question) {
            var procIds = question.split('-');
            if(procIds.length > 1){
              if(value[procIds[1]] || value[procIds[1]] === 0){
                a[question] = value[procIds[1]];
              }
            }else {
              if (procIds[0] && procIds[0] !== undefined && procIds[0] !== 'isActive' && procIds[0] !== 'entityvalueinactivereason') {
                question += '-'+req.body['s#'];
                a[question] = value;
              }
            }
            return a;
          }, {});
          var format = 'Number';
          var proc = '';
          var lamda = '(function(model){ return ' +   parseCriteria(proc, req.body.entityFilters[0].conditions, format, entity.entityRecord) + '})('+ JSON.stringify(transformedModel) + ');';

          //QC3-6931 Prachi (add round bracket in parseCriteria function)
          //Old result which is wrong [true || truw && false = true] - provided only *Add Condition*
          //New result [(((true) || true) && false) = false]
          var firstCondition = lamda.substring(0,25);
          var lastCondition = lamda.substring(25);

          if (!_.isUndefined(req.body.entityFilters[0]) && !_.isUndefined(req.body.entityFilters[0].conditions) && req.body.entityFilters[0].conditions.length) {
            _.times(req.body.entityFilters[0].conditions.length, function(c) {
              lastCondition = '(' + lastCondition;
            });
          }

          lamda = firstCondition + lastCondition;
          var res = eval(lamda);
          if (res) {
            entitySelection.push(entity);
          }
        });
        res.send({entitySelection: entitySelection});
      }else if (!_.isUndefined(req.body) && !_.isUndefined(req.body.entityFilters) && req.body.entityFilters.length == 0) {
        res.send({entitySelection: entities});
      } else if (!_.isUndefined(req.body) && _.isUndefined(req.body.entityFilters)) {
        res.send({entitySelection: entities});
      }
    } catch (e) {
      logger.log("catch: collection:entityRecord - failed : " + e , 'error', currentFileName);
      //console.log(e);
      res.status(500).json("'fetchEntityRecords' for pirticular entity '_id' = "+req.body._id+" :: " + e);
    }
  });
});

var operators = {};
var boolMeaning = {
  "AND": '&&',
  "OR": '||',
  "*": '*',
  "/": '/',
  "+": '+',
  "-": '-',
  "%": '%'
}

operators['format'] = function (format, op) {
  if (format == 'None') {
    return '\'' + op + '\'';
  }else {
    return op;
  }
}
operators['>'] = function (op1, op2, format) {
  if (op1.parentId) {
    if(format == 'Date'){
      return  '(moment.utc(new Date(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'])).isAfter(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))))';
    } else {
      return  'model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  }else {
    if(format == 'Date'){
      return  '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).isAfter(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))) )';
    } else {
      return  'model[\'' + op1['s#'] + '\'] > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['>='] = function (op1, op2, format) {
  if (op1.parentId) {
    if(format == 'Date'){
      return  '(moment.utc(new Date(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'])).isSameOrAfter(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))))';
    } else {
      return  'model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  }else {
    if(format == 'Date'){
      return  '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).isSameOrAfter(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))) )';
    } else {
      return  'model[\'' + op1['s#'] + '\'] >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['<'] = function (op1, op2, format) {
  if (op1.parentId) {
    if(format == 'Date'){
      return  '(moment.utc(new Date(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'])).isBefore(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))))';
    } else {
      return  'model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  }else {
    if(format == 'Date'){
      return  '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).isBefore(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))) )';
    } else {
      return  'model[\'' + op1['s#'] + '\'] < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['<='] = function (op1, op2, format) {
  if (op1.parentId) {
    if(format == 'Date'){
      return  '(moment.utc(new Date(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'])).isSameOrBefore(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))))';
    } else {
      return  'model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  }else {
    if(format == 'Date'){
      return  '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).isSameOrBefore(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))) )';
    } else {
      return  'model[\'' + op1['s#'] + '\'] <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}
operators['contains'] = function (op1, op2, format) {
  if (op1.parentId) {
    return  '(\'\' + model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ') !== -1';
  }else {
    return  '(\'\' + model[\'' + op1['s#'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ') !== -1';
  }
}
operators['does not contain'] = function (op1, op2, format) {
  if (op1.parentId) {
    return  '(\'\' + model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ') === -1';
  }else {
    return  '(\'\' + model[\'' + op1['s#'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ') === -1';
  }
}
operators['starts with'] = function (op1, op2, format) {
  if (op1.parentId) {
    return  '(\'\' + model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).startsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
  }else {
    return  '(\'\' + model[\'' + op1['s#'] + '\']).startsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
  }
}
operators['ends with'] = function (op1, op2, format) {
  if (op1.parentId) {
    return  '(\'\' + model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).endsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
  }else {
    return  '(\'\' + model[\'' + op1['s#'] + '\']).endsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
  }
}
//QC3-5072
//Bug: Home > Smart Alert : Operator Drop down Values should be in Camel Notation as we have throughout the system.
//Start -- Prachi
operators['Contains'] = function (op1, op2, format) {
  if (op1.parentId) {
    return  '(\'\' + model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ') !== -1';
  }else {
    return  '(\'\' + model[\'' + op1['s#'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ') !== -1';
  }
}
operators['Does Not Contain'] = function (op1, op2, format) {
  if (op1.parentId) {
    return  '(\'\' + model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ') === -1';
  }else {
    return  '(\'\' + model[\'' + op1['s#'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ') === -1';
  }
}

operators['Starts With'] = function (op1, op2, format) {
  if (op1.parentId) {
    return  '(\'\' + model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).startsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
  }else {
    return  '(\'\' + model[\'' + op1['s#'] + '\']).startsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
  }
}
operators['Ends With'] = function (op1, op2, format) {
  if (op1.parentId) {
    return  '(\'\' + model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).endsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
  }else {
    return  '(\'\' + model[\'' + op1['s#'] + '\']).endsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
  }
}
//End -- Prachi
operators['='] = function (op1, op2, format) {
  if (op1.parentId) {
    if(format == 'Date'){
      return  '(moment.utc(new Date(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'])).isSame(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))))';
    } else {
      return  '(_.isArray(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']) ? (_.includes(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'],' +   (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2))     + ')) : (model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '))';
    }
  }else {
    if(format == 'Date'){
      return  '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).isSame(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))) )';
    } else {
      return  '(_.isArray(model[\'' + op1['s#'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '\'],' +   (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2))     + ')) : (model[\'' + op1['s#'] + '\'] == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '))';
    }
  }
}

operators['+'] = function (op1, op2, format) {
  return  '(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] + ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
}

operators['*'] = function (op1, op2, format) {
  return  '(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] * ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
}

operators['/'] = function (op1, op2, format) {
  return  '(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] / ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
}

operators['%'] = function (op1, op2, format) {
  return  '(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] % ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
}

operators['-'] = function (op1, op2, format) {
  return  '(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] - ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
}


operators['!='] = function (op1, op2, format) {
  if (op1.parentId) {
    if(format == 'Date'){
      // PQT-4232
      return  '!(moment.utc(new Date(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).setHours(0,0,0,0)).isSame(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))))';
    } else {
      return  '(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
    }
  }else {
    if(format == 'Date'){
      return  '!(moment.utc(new Date(model[\'' + op1['s#'] + '\']).setHours(0,0,0,0)).isSame(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))) )';
    } else {
      return  '(model[\'' + op1['s#'] + '\'] != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
    }
  }
}

// Parse Criteria Strat - Prachi

operators['Greater than'] = function (op1, op2, format) {
  if (op1.parentId) {
    if(format == 'Date'){
      // PQT-4232
      return  '(moment.utc(new Date(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).setHours(0,0,0,0)).isAfter(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))))';
    } else {
      return  'model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  }else {
    if(format == 'Date'){
      // PQT-4232
      return  '(moment.utc(new Date(model[\'' + op1['s#'] + '\']).setHours(0,0,0,0)).isAfter(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))) )';
    } else {
      return  'model[\'' + op1['s#'] + '\'] > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['Greater than or equal to'] = function (op1, op2, format) {
  if (op1.parentId) {
    if(format == 'Date'){
      // PQT-4232
      return  '(moment.utc(new Date(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).setHours(0,0,0,0)).isSameOrAfter(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))))';
    } else {
      return  'model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  }else {
    if(format == 'Date'){
      // PQT-4232
      return  '(moment.utc(new Date(model[\'' + op1['s#'] + '\']).setHours(0,0,0,0)).isSameOrAfter(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))) )';
    } else {
      return  'model[\'' + op1['s#'] + '\'] >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['Less than'] = function (op1, op2, format) {
  if (op1.parentId) {
    if(format == 'Date'){
      // PQT-4232
      return  '(moment.utc(new Date(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).setHours(0,0,0,0)).isBefore(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))))';
    } else {
      return  'model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  }else {
    if(format == 'Date'){
      // PQT-4232
      return  '(moment.utc(new Date(model[\'' + op1['s#'] + '\']).setHours(0,0,0,0)).isBefore(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))) )';
    } else {
      return  'model[\'' + op1['s#'] + '\'] < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['Less than or equal to'] = function (op1, op2, format) {
  if (op1.parentId) {
    if(format == 'Date'){
      // PQT-4232
      return  '(moment.utc(new Date(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).setHours(0,0,0,0)).isSameOrBefore(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))))';
    } else {
      return  'model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  }else {
    if(format == 'Date'){
      // PQT-4232
      return  '(moment.utc(new Date(model[\'' + op1['s#'] + '\']).setHours(0,0,0,0)).isSameOrBefore(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))) )';
    } else {
      return  'model[\'' + op1['s#'] + '\'] <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['Equal to'] = function (op1, op2, format) {
  if (op1.parentId) {
    if(format == 'Date'){
      // PQT-4232
      return  '(moment.utc(new Date(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).setHours(0,0,0,0)).isSame(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))))';
    } else {
      return  '(_.isArray(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']) ? (_.includes(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'],' +   (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2))     + ')) : (model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '))';
    }
  }else {
    if(format == 'Date'){
      // PQT-4232
      return  '(moment.utc(new Date(model[\'' + op1['s#'] + '\']).setHours(0,0,0,0)).isSame(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))) )';
    } else {
      return  '(_.isArray(model[\'' + op1['s#'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '\'],' +   (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2))     + ')) : (model[\'' + op1['s#'] + '\'] == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '))';
    }
  }
}

operators['Not equal to'] = function (op1, op2, format) {
  if (op1.parentId) {
    if(format == 'Date'){
      // PQT-4232
      return  '!(moment.utc(new Date(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\']).setHours(0,0,0,0)).isSame(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))))';
    } else {
      return  '(model[\'' + op1['s#'] +'-'+ op1['parentId'] + '\'] != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] +'-'+ op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
    }
  }else {
    if(format == 'Date'){
      // PQT-4232
      return  '!(moment.utc(new Date(model[\'' + op1['s#'] + '\']).setHours(0,0,0,0)).isSame(moment.utc(new Date(\''+(_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0))) )';
    } else {
      return  '(model[\'' + op1['s#'] + '\'] != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
    }
  }
}

// Parse Criteria End


function parseCriteria(procId, criteria, format, model) {
  logger.log("function parseCriteria(procId, criteria, format, model) - called" , 'info', currentFileName);

  var val = '';
  _.forEach(criteria, function (c) {
    if(c.conditions){
      val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + '(' + parseCriteria(procId, c.conditions, format, model) + ')' + ')';
      if (c.conditions.length) { //QC3-9060 Prachi
        _.times(c.conditions.length, function(c) {
          val = '(' + val;
        });
      }
    } else {
      if(!_.isUndefined(c.comparison) && c.comparison !== null) {
        val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + operators[c.comparison](c.leftOperand, c.rightOperand, _.isObject(c.leftOperand) ? ((!_.isUndefined(c.leftOperand.type) && !_.isUndefined(c.leftOperand.type.format) && c.leftOperand.type.format.title) ? c.leftOperand.type.format.title : 'None') : format)  + ')';
      } else {
        if(!_.isUndefined(c.leftOperand) && c.leftOperand !== null) {
          if(c.leftOperand['type']['format']['title'] === 'Exponential'){
            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + '(model[\'' + c.leftOperand['s#'] +'-'+ c.leftOperand['parentId'] + '\'] * (Math.pow(10, ' + c.leftOperand['type']['format']['metadata']['power'] + ')))' + ')';
          } else {
            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + '(model[\'' + c.leftOperand['s#'] +'-'+ c.leftOperand['parentId'] + '\'])' + ')';
          }
        } else if(!_.isUndefined(c.rightOperand) && c.rightOperand !== null) {
          if(_.isObject(c.rightOperand) && c.rightOperand['type']['format']['title'] === 'Exponential'){
            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + (_.isObject(c.rightOperand) ? '(model[\'' + c.rightOperand['s#'] +'-'+ c.rightOperand['parentId'] + '\'] * (Math.pow(10, ' + c.rightOperand['type']['format']['metadata']['power'] + ')))' : parseFloat(c.rightOperand) ? parseFloat(c.rightOperand) : 0)  + ')';
          }else if(_.isObject(c.rightOperand) && c.rightOperand['type']['format']['title'] === 'Date'){
            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + (_.isObject(c.rightOperand) ? '(model[\'' + c.rightOperand['s#'] +'-'+ c.rightOperand['parentId'] + '\'])' : new Date(c.rightOperand))  + ')';
          } else {
            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + (_.isObject(c.rightOperand) ? '(model[\'' + c.rightOperand['s#'] +'-'+ c.rightOperand['parentId'] + '\'])' : new Date(c.rightOperand))  + ')';
          }
        }
      }
    }
  });
  return val;
}

module.exports = router;
