'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var db = require('../../lib/resources').db;

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'searchFavorites',
    collection: 'searchFavorites',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    POST: {
      ONE: {
        before: beforePost
        //after: afterPostLinked
      },
      ONESOFT: {
        before: beforePost
        //after: afterPostLinked
      }
    }
  }
});

// QC3-8867: mahammad
function beforePost(req, res, next) {
  if (req.body) {
    db.searchFavorites.find({ title: req.body.title, systemSearch: req.body.systemSearch, isEntity: req.body.isEntity, isWindow: req.body.isWindow, createdBy: {$in: [req.user._id, db.ObjectID(req.user._id)]} }).toArray(function (error, response) {
      if (response && response.length) {
        return res.json({ 'alreadyExist': true });
      } else {
        next();
      }
    });
  } else {
    next();
  }
}
// QC3-8867: mahammad

/**
 * @swagger
 * definition:
 *  search:
 *    properties:
 *      qcform:
 *        type: any
 *      searchByRadio:
 *        type: any
 *      month:
 *        type: dateTime
 *      year:
 *        type: dateTime
 *      date:
 *        type: dateTime
 *      attribute:
 *        type: array
 *        items:
 *          type: object
 *          allOf:
 *           - $ref: '#/definitions/attribute/'
 *      attributevalue:
 *         type: array
 *         items:
 *           type: object
 *           allOf:
 *            - $ref: '#/definitions/attributeValue'
 *      attributedetails:
 *         type: array
 *         items: {}
 *      batchEntryId:
 *         type: integer
 *      startDate:
 *         type: dateTime
 *      endDate:
 *          type: dateTime
 *      sitelevel:
 *         type: array
 *         items: {}
 *      status:
 *         type: array
 *         items: {}
 *      searchCriteria:
 *         type: string
 *      apps:
 *         type: array
 *         items:
 *           type: object
 *           allOf:
 *            - $ref: '#/definitions/apps'
 *      module:
 *          type: array
 *          items:
 *           type: object
 *           allOf:
 *           - $ref: '#/definitions/module'
 */
 /**
 * @swagger
 * definition:
 *   module:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       route:
 *         type: string
 *       reqParams:
 *         type: integer
 *       restangularized:
 *         type: boolean
 *       fromServer:
 *         type: boolean
 *       parentResource:
 *         type: integer
 *       restangularCollection:
 *         type: boolean
 */

 /**
  * @swagger
  * definition:
  *  apps:
  *   properties:
  *     id:
  *       type: integer
  */

  /**
  * @swagger
  * definition:
  *  attributeValue:
  *    properties:
  *      id:
  *        type: integer
  */

  /**
 * @swagger
 * definition:
 *   attribute:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       type:
 *         type: array
 *         items:
 *           type: object
 *           allOf:
 *            - $ref: '#/definitions/Type'
 *       version:
 *           type: integer
 *       isActive:
 *         type: boolean
 *       module:
 *         type: array
 *         items:
 *           type: object
 *           allOf:
 *            - $ref: '#/definitions/Module'
 *       createdBy:
 *         type : integer
 *       modifiedBy:
 *         type : integer
 *       createdDate:
 *         type : dateTime
 *       modifiedDate:
 *         type : dateTime
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *               - $ref: '#/definitions/Version'
 *       isMajorVersion:
 *           type: boolean
 *       isEntity:
 *           type: boolean
 */

 /**
 * @swagger
 * definition:
 *   Version:
 *     properties:
 *       version:
 *         type: integer
 *       record:
 *         type: integer
 */

 /**
 * @swagger
 * definition:
 *   Module:
 *     properties:
 *       id:
 *         type: integer
 */

 /**
 * @swagger
 * definition:
 *   Metadata:
 *     properties:
 *        Decimal:
 *          type: string
 *        MeasurementUnit:
 *          type: string
 */

 /**
 * @swagger
 * definition:
 *   Format:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       metadata:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *               - $ref: '#/definitions/Metadata'
 */

/**
* @swagger
* definition:
*   Type:
*     properties:
*       id:
*         type: integer
*       title:
*         type: string
*       formats:
*           type: array
*           items:
*              type: object
*              allOf:
*               - $ref: '#/definitions/Format'
*/

/**
 * @swagger
 * definition:
 *   SearchFavorites:
 *     properties:
 *       id:
 *         type: integer
 *       search:
 *         type: array
 *         items:
 *            type: object
 *            allOf:
 *             - $ref: '#/definitions/search'
 *       searchCriteria:
 *          type: string
 *       systemSearch:
 *          type: boolean
 *       title:
 *          type: string
 *       createdBy:
 *          type: integer
 *       modifiedBy:
 *          type: integer
 *       createdByName:
 *          type: string
 *       modifiedByName:
 *          type: string
 *       createdDate:
 *          type: dateTime
 *       modifiedDate:
 *          type: dateTime
 *       versions:
 *          type: array
 *          items:
 *             type: object
 *             allOf:
 *              - $ref: '#/definitions/Version'
 *       version:
 *          type: integer
 *       isMajorVersion:
 *          type: boolean
 *       isEntity:
 *          type: boolean
 */

 /**
 * @swagger
 * /searchFavorites:
 *   get:
 *     tags: [Entity Records]
 *     description: Searches entity records from 'My favorites'
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of records
 *         schema:
 *           $ref: '#/definitions/SearchFavorites'
 */

module.exports = route.router;
