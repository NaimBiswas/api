'use strict'
/**
 * @name sites-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var string = types.string;
var bool = types.bool;
var object = types.object.bind(types);
var any = types.any;
var schema = {
  search: any,
  type: any.label('system search type (isAppQuickAccess/isEntityMyFavorite/isWindowSystemFavorite'),
  systemSearch: bool.label('system search or not'),
  isEntity: bool.label('is entity or not'),
  isWindow: bool.label('is window or not'),
  title: string.label('search name'),
  searchCriteria: string.label('store app type master or apss'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
