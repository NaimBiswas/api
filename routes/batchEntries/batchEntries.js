'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var batchEntryService = require('../../lib/batchEntryService/index');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'batchEntries',
    collection: 'batchEntries',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    GET: {
      ALL: {
        after: afterGet,
        before: beforeGet
      }
    },
  }
});

function beforeGet(req, res, next) {
  req.query.y = {};

  if (req.query.q) {
    req.query.y.sequenceId = req.query.q.sequenceId;
    req.query.y.formId = req.query.q.formId;
    req.query.y.batchNo = req.query.q.batchNo;
    req.query.y.counter = req.query.q.counter;

    delete req.query.q.sequenceId;
    delete req.query.q.formId;
    delete req.query.q.batchNo;
    delete req.query.q.counter;
  }

  next();
}

function afterGet(req, res, next) {

  if (req.query.q && req.query.q.schemaId) {

  var childQuery = {};

  if (req.query.y && req.query.y.sequenceId) {
    childQuery.record = {
      'masterQCSettings.sequenceId': req.query.y.sequenceId
    };
  }

  if (req.query.y.formId) {
    childQuery.record = {
      'formId': req.query.y.formId
    };
  }

  if (req.query.y.batchNo) {
    childQuery.record = {
      'batchNo': req.query.y.batchNo
    };
    childQuery.attribute = {
      'batchNo': req.query.y.batchNo
    };
  }

  if (req.query.y.counter) {
    childQuery.record = {
      'counter': req.query.y.counter
    };
  }

  if (req.query.y.formId && req.query.orBatchNo) {
    childQuery.record = {
      $or: [{
        'batchNo': req.query.orBatchNo
      }, {
        'formId': req.query.y.formId
      }]
    };

    childQuery.attribute = {
      'batchNo': req.query.orBatchNo
    };
  }

  if (!req.query.y.formId && req.query.orBatchNo) {
    childQuery.record = {
      'batchNo': req.query.orBatchNo
    };
    childQuery.attribute = {
      'batchNo': req.query.orBatchNo
    };
  }

  if (req.query.recordSelect) {
    childQuery.recordSelect = req.query.recordSelect;
  }

  batchEntryService.getBatchEntries(req.query.q.schemaId, req.data, null, childQuery)
    .then(function (batchEntries) {
      res.json(batchEntries);
    });

  } else {
    res.json(req.data);
  }
}

module.exports = route.router;