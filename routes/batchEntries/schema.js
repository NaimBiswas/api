'use strict'
/**
 * @name batchEntries-schema
 * @author Surjeet Bhadauriya <surjeet.b@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var array = types.array;
var rString = types.rString;
var rBool = types.rBool;
var rSmallString = types.rSmallString;
var any = types.any;
var string = types.string;

var schema = {
  schemaId: rString.label('Schema ID'),
  schemaTitle: rString.label('Schema Title'),
  batchEntries: any,
  batchAttributesInfo: any,
  timeZone: string.label('Time Zone')
};

module.exports = schema;
