'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');
var joi = require('joi');

var object = types.object.bind(types);
var rId = types.rId;
var id = types.id;
var rString = types.rString;
var array = types.array;
var rBool = types.rBool;
var rNumber = types.rNumber;
var string = types.string;
var required = types.required;
var any = types.any;



var childMetadata = array(object({
  _id: id,
  name: rString.label('Validator title'),
  title: rString.label('Validator title'),
  isOptional: rBool.label('isOptional'),
  group: {
    _id: rString.label('Group Name / ID'),
    title: joi.string().min(0).max(50).label('Group Title')
  },
  class : string.label('Optonal css class, default is col-md-6'),
  dataType : rString.label('Data / Control type'),
  allowItems: array(object({
    _id: rId.label('Validator id'),
    title: joi.string().max(50).label('Validator title')
  }))
}));
var schema = object({
  formatId: rId.label('Format id'),
  name: rString.label('Validator title'),
  title: rString.label('Validator title'),
  isOptional: rBool.label('isOptional'),
  group: {
    _id: rString.label('Group Name / ID'),
    title: joi.string().min(0).max(50).label('Group Title')
  },
  dataType : rString.label('Data / Control type'),
  allowItems: array(object({
    _id: rId.label('Validator id'),
    title: joi.string().max(50).label('Validator title')
  })),
  metadata: array(object({
    _id: id,
    name: rString.label('Validator title'),
    title: rString.label('Validator title'),
    isOptional: rBool.label('isOptional'),
    group: {
      _id: rString.label('Group Name / ID'),
      title: joi.string().min(0).max(50).label('Group Title')
    },
    class : string.label('Optonal css class, default is col-md-6'),
    dataType : rString.label('Data / Control type'),
    metadata: childMetadata,
    allowItems: array(object({
      _id: rId.label('Validator id'),
      title: joi.string().max(50).label('Validator title')
    }))
  }))
});

module.exports = schema;
