'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var id = types.id;
var string = types.string;
var array = types.array;
var bool = types.bool;
var number = types.number;
var required = types.required;
var any = types.any;


var schema = object({
  _id: id.label('Attribute id'),
  title: string.label('Attribute title'),
  type: object({
    _id: id.label('Type id'),
    title: string.label('Type title'),
    format: object({
      _id: id.label('Format id'),
      title: string.label('Format title'),
      metadata: any.label('Metadata required for the format')
    })
  }).label('Attribute type'),
  isActive: bool.label('Active'),
  module: object({
    _id: id.label('Module id'),
    title: string.label('Module title')
  }).label('Modules of module object'),
  validations: any
});

module.exports = schema;
