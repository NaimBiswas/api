'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var _ =require('lodash');
var db = require('../../lib/db');
//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'controlChartParameters',
    collection: 'controlChartParameters',
    schema: schema,
    softSchema: softSchema
  },
  types:{
    POST:{
      ONE: {
        before : beforePost,
        after : afterPost,
      }
    },
  }
});

function beforePost(req,res,next){
  _.forEach(req.items.spParameteres.columnInfo,function(col){
    if(col.extraInformation && col.extraInformation.info){
      _.forEach(col.extraInformation.info,function(colinfo){
        colinfo.appId = db.ObjectID(colinfo.appId);
      })
    }
  })
  next();
}

function afterPost(req, res, next){
  console.log("-------------------------------------------------------------------------------")
  console.log("---------------------------------controlchartparametrs-----------------------------------")

  console.log(req.data)
  console.log("-------------------------------------------------------------------------------")

  res.status(200).json(req.data)
}

/**
 * @swagger
 * definition:
 *   ParamXSeries:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       type:
 *         $ref: '#/definitions/Type'
 *       version:
 *           type: integer
 *       isActive:
 *           type: boolean
 *       validation:
 *         $ref: '#/definitions/Validation'
 *       module:
 *         $ref: '#/definitions/Module'
 *       s#:
 *         type: integer
 */

 /**
  * @swagger
  * definition:
  *   ParamYSeries:
  *     properties:
  *       id:
  *         type: integer
  *       title:
  *         type: string
  *       type:
  *         $ref: '#/definitions/Type'
  *       version:
  *           type: integer
  *       s#:
  *         type: integer
  *       comboId:
  *         type: integer
  *       parentId:
  *         type: integer
  *       criteria:
  *         $ref: '#/definitions/Criteria'
  */


/**
 * @swagger
 * definition:
 *   Controlcharts:
 *     properties:
 *       id:
 *         type: integer
 *       module:
 *         $ref: '#/definitions/Module'
 *       title:
 *         type: string
 *       paramFormName:
 *         type: string
 *       paramXSeries:
 *         $ref: '#/definitions/ParamXSeries'
 *       paramYSeries:
 *         $ref: '#/definitions/ParamYSeries'
 *       paramDateForm:
 *         type: dateTime
 *       paramDateTo:
 *         type: dateTime
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       createdBy:
 *         $ref: '#/definitions/CreatedBy'
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       version:
 *           type: integer
 *       isMajorVersion:
 *           type: boolean
 */


/**
 * @swagger
 * /controlchart:
 *   get:
 *     tags: [Reports]
 *     description: Returns all controlcharts
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of controlcharts
 *         schema:
 *           $ref: '#/definitions/Controlcharts'

 */


module.exports = route.router;
