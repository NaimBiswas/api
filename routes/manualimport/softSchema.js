'use strict'
/**
 * @name manualimport-schema
 * @author Kajal Patel <kajal.p@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var id = types.id;
var rString = types.rString;
var fileString = types.fileString;
var requiredStringForImport = types.requiredStringForImport;
var array = types.array;
var optionalArray = types.optionalArray;
var rBool = types.rBool;
var bool = types.bool;
var number = types.number;
var any = types.any;
var required = types.required;
var anyArray = types.anyArray;
var string = types.string;
var smallStringNotrequired = types.smallStringNotrequired

var schema = {
  module:object({
    _id: id.label('Module id'),
    title: string.label('Module title')
  }).label('Module object'),
  title: string.label('Mapping Name'),
  fileindex: anyArray.label('fileindex array'),
  schema: object({
    _id: id.label('QcForm id'),
    title: string.label('QcForm title'),
    mappingName : string.label('Mapping Name'),
    mapping: optionalArray(object({
        csvField: any,
            //mappingField: rString.label('Mapping Field title'),
        mappingField:any
    }))
  }).label('Form array of object'),
  file : any.label('CSV File'),
    isAllowHeader: bool.label('Active?'),
  type : string.label('Type'),
  timeZone: string.label('Time Zone'),
  lastSubmitedCount : smallStringNotrequired.label('lastSubmitedCount'),
  status:any
};

module.exports = schema;
