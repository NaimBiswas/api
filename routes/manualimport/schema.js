'use strict'
/**
 * @name manualimport-schema
 * @author Kajal Patel <kajal.p@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var requiredStringForImport = types.requiredStringForImport;
var array = types.array;
var optionalArray = types.optionalArray;
var rBool = types.rBool;
var number = types.number;
var any = types.any;
var required = types.required;
var anyArray = types.anyArray;
var string = types.string;
var smallStringNotrequired = types.smallStringNotrequired

var schema = {
  module:object({
    _id: rId.label('Module id'),
    title: rString.label('Module title')
  }).required().label('Module object'),
  title: rString.label('Mapping Name'),
  fileindex: anyArray.label('fileindex array'),
  schema: object({
    _id: rId.label('QcForm id'),
    title: rString.label('QcForm title'),
    mappingName : rString.label('Mapping Name'),
    mapping: optionalArray(object({
        csvField: any,
            //mappingField: rString.label('Mapping Field title'),
        mappingField:any
    }))
  }).required().label('Form array of object'),
  file : any.label('CSV File'),
    isAllowHeader: rBool.label('Active?'),
  type : rString.label('Type'),
  timeZone: string.label('Time Zone'),
  lastSubmitedCount : smallStringNotrequired.label('lastSubmitedCount'),
  status:any
};

module.exports = schema;
