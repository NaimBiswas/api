'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var validation = require('./validation');
var entityValidation = require('./entityvalidation');

var MICRO = require("../../microservices/MICRO");

var manualImportPublisherInstance = MICRO.getPublisherInstance("manualImport");

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST

/**
 * @swagger
 * definition:
 *   MappingField:
 *     properties:
 *       _id:
 *         type: string
 *       title:
 *         type: string
 *       validation:
 *         type: object
 *       isKeyValue:
 *         type: boolean
 *       require:
 *         type: boolean
 *       isActive:
 *         type: boolean
 *       s#:
 *         type: string
 *       comboId:
 *         type: string
 *       mapField:
 *         type: string
 *       mapindex:
 *         type: integer
 *       version:
 *         type: integer
 *       module:
 *         $ref: '#/definitions/Module'
 *       type:
 *         $ref: '#/definitions/Type'
 */

/**
 * @swagger
 * definition:
 *   Mapping:
 *     properties:
 *       csvField:
 *         type: string
 *       mappingField:
 *         $ref: '#/definitions/MappingField'
 */

/**
 * @swagger
 * definition:
 *   ManualSchema:
 *     properties:
 *       _id:
 *         type: string
 *       title:
 *         type: string
 *       mappingName:
 *         type: string
 *       mapping:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Mapping'
 */

/**
 * @swagger
 * definition:
 *   ManualImport:
 *     properties:
 *       _id:
 *         type: string
 *       module:
 *         $ref: '#/definitions/Module'
 *       schema:
 *         $ref: '#/definitions/ManualSchema'
 *       type:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 */

/**
 * @swagger
 * /manualimport:
 *   get:
 *     tags: [Import Data]
 *     description: Returns all imported data
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of manual import that you want to see in the output (Exp Values -> [ index,schema,schema,type,modifiedDate,modifiedBy])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of manual import with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of manual import
 *         schema:
 *           $ref: '#/definitions/ManualImport'
 */

 /**
  * @swagger
  * /manualimport/{id}:
  *   get:
  *     tags: [Import Data]
  *     description: Returns all imported data
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: Manual Import's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: pagesize
  *         description: Page Size
  *         in: query
  *         required: false
  *         type: string
  *     responses:
  *       200:
  *         description: An object of user
  *         schema:
  *           $ref: '#/definitions/ManualImport'
  */

  /**
   * @swagger
   * /manualimport:
   *   post:
   *     tags: [Import Data]
   *     description: Creates a new mapping
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: fileName
   *         description: File Name
   *         in: query
   *         required: true
   *         type: string
   *       - name: allowHeader
   *         description: Allow Header flag
   *         in: query
   *         required: true
   *         type: boolean
   *       - name: isimportview
   *         description: Is Import View
   *         in: query
   *         required: true
   *         type: boolean
   *       - name: manuaimport
   *         description: Manual Import object
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/ManualImport'
   *     responses:
   *       201:
   *         description: Successfully created
   */

   /**
    * @swagger
    * /manualimport/{id}:
    *   patch:
    *     tags: [Import Data]
    *     description: Updates a mapping
    *     produces:
    *       - application/json
    *     parameters:
    *       - name: id
    *         description: Mapping's id
    *         in: path
    *         required: true
    *         type: integer
    *       - name: fileName
    *         description: File Name
    *         in: query
    *         required: true
    *         type: string
    *       - name: allowHeader
    *         description: Allow Header flag
    *         in: query
    *         required: true
    *         type: boolean
    *       - name: schemaId
    *         description: Schema Id
    *         in: query
    *         required: true
    *         type: integer
    *       - name: isimportview
    *         description: Is Import View
    *         in: query
    *         required: true
    *         type: boolean
    *       - name: manuaimport
    *         description: Manual Import object
    *         in: body
    *         required: true
    *         schema:
    *           $ref: '#/definitions/ManualImport'
    *     responses:
    *       200:
    *         description: Successfully updated
    */

  //Chandni : Code end for Swagger

var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'manualimport',
    collection: 'manualimport',
    schema: schema,
    softSchema: softSchema
    },
    types: {
        POST: {
            ONE: {
                after : afterPost
            }
        },
        PATCH:{
          ONE: {
            after : afterPost
          }
        }
    }
});


function afterPost(req, res, next) {

    var reqBody = {
        headers : req.headers,
        query : req.query,
        data : req.data,
        user : req.user,
        body : req.body
    }
    manualImportPublisherInstance.call("import",reqBody,function (err,data){
        (req.status ? res.status(req.status) : res).json(data);
    });

    // if (req.query.isimportview == 'true') {
    //     if (req.body.type == 'entity') {
    //         entityValidation.validate(req, res, next);
    //     }
    //     else {
    //         validation.validate(req, res, next);
    //     }
    // }
    // else{
    //   (req.status ? res.status(req.status) : res).json(req.data);
    // }
}


module.exports = route.router;
