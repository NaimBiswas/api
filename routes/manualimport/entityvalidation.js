var fs = require('fs');
var _ = require('lodash');
var csv = require('csv-array');
var moment = require('moment');
var db = require('../../lib/resources').db;
var dbConfig = require('../../lib/db/db');
var entries = require('../entries');
var csventries = require('../csventries');
var uuid = require('node-uuid');
var config = require('../../config/config.js');
var request = require('request');
var restler = require('restler');
var regexUtilty = require('../../lib/utilities/regexUtilities.js');


var mapping = {};
var reqHeaders = {};
var processSchema = {}; // Jaydipsinh : for holding schema of current processing entry
var allsites = [];
var user = {};
var parentVersion = '';
var mappingId = '';
var levelLength;
var processedData = [];
var passDataCol = [];
var failDataCol = [];
var setOriginalValue = [];
var oldSchemas = [];
var setsarray = [];
var attrPass1 = [];
var attrPass2 = [];
var allTypes = {};
var linkEntityData = {};
var importId = '';
///by surjeet
var keyValueAttribute;
var userdata = {};
var tempSites = [];
var options = {};
var machineDate = new Date();
var machineoffset = machineDate.getTimezoneOffset();
var clientOffset = machineDate.getTimezoneOffset();
var mcdiff = 0;

var entityValidationController = function () { };

//getting csv file
function readFile(req, res, next, sites) {
  clientOffset = req.headers['client-offset'] || -330;
  mcdiff = -machineoffset + parseInt(clientOffset);
  allsites = sites;

  if (!req.query.fileName) {

    
    handleResponse(res,422,{ 'message': 'file not provided' })
    // res.status(422).json({ 'message': 'file not provided' })
    return;
  }


  if (config && config.fileServer &&
    config.fileServer.enabled &&
    config.fileServer.url &&
    config.fileServer.url.get) {
    request(`${config.fileServer.url.get}${req.query.fileName}`,{strictSSL:false})
      .on('response',  function (resApi) {

        console.log("dfd");

        resApi.pipe(fs.createWriteStream(process.env.FILE_STORE + req.query.fileName));
        resApi.on( 'end', function(){
          // go on with processing

          handleAfterFileDownload(req,res)

        });

      });
  }
  else{
    handleAfterFileDownload(req,res)
  }
}


function handleAfterFileDownload(req,res){

  var filename = process.env.FILE_STORE + req.query.fileName;
  // var filename =

  if (!fs.existsSync(filename)) {
    handleResponse(res,404,{ 'message': 'file not found' })
    // res.status(404).json({ 'message': 'file not found' })
    return;
  }

  var items = [];
  csv.parseCSV(filename, function (data) {
    //akashdeep.s - QC3-5527 - filtering for those field who is not created by new line.
    items = _.filter(data, function (dt) {
      //check for blank data
      return !(dt && dt.length == 1 && dt[0].trim() == "")
    });
    //items = data;
    processOldData(items, req, res);
    //res.status(200).json(data);
  }, false);

}

function processOldData(items, req, res) {
  //var rData = req.data;
  mappingId = req.data._id.toString();
  var allowHeader = req.query.allowHeader;
  //levelLength = req.query.sitelevels;
  parentVersion = req.query.parentVersion;
  importId = uuid.v1();
  _.forEach(items, function (data, index) {
    //var attrProcess = false;
    if (allowHeader == 'true') {
      if (index > 0) {
        allValidation(data, index);
      }
    }
    else {
      //attrProcess is either true or false
      //true - key attr is passed
      //false - key attr is failed
      allValidation(data, index);
    }
  });
  //check for key attribute validation from existing passdatacol
  _.forEach(passDataCol , function (pdata, indx) {
    if (pdata) {
      validateAttribute(pdata, indx);
    }
  })
  // _.forEach(passDataCol, function (pdata, idz) {
  //   if (pdata){
  //     validateAttribute(pdata, idz);
  //   }
  // });

  //check for key attribute from database data
  _.forEach(attrPass1, function (pdata, idz) {
    if (pdata){
      validateAttributeDb(pdata, idz);      
    }
  });

  /**
  *
  * **/
  processedData = attrPass2.concat(failDataCol);
  //create a new collection
  //var entities = [];
  db["csventries"].insertManyAsync(processedData).then(function (data) {
    handleResponse(res,200,req.data)
    // (req.status ? res.status(req.status) : res).json(req.data);
    // res.status(req.status).json(req.data);
  }).catch(function (err) {
    console.log(err);
  });
}

//the whole code is modify by surjeet.b@productivet.com for isKeyAttributeAndEntity check in imported file data
function validateAttribute(val, inx) {
  var optchk = {
    'entries': {}
  };
  _.forEach(processSchema.questions, function (keyvalue) {
    if (keyvalue.isKeyValue) {
      optchk.entries[keyvalue['s#']] = {
        'attributes': val.entries[keyvalue['s#']].attributes
      }
    }
  })
  if (Object.getOwnPropertyNames(optchk.entries).length == 0) {
    attrPass1.push(val);
  }else {
    var fltr = _.filter(passDataCol, optchk);
    if (fltr.length > 1) {
       if(!_.find(attrPass1,optchk) && !_.find(failDataCol,optchk) ){
          attrPass1.push(val);
       }else{
        val.status = 'Fail';
        _.forEach(processSchema.questions, function (keyvalue) {
          if (keyvalue.isKeyValue) {
            val.FailReasons[keyvalue['s#']] = 'Key attribute value already exists. Please provide other value.';
          }
        })
        failDataCol.push(val);
      }
    }
    else {
      attrPass1.push(val);
    }
  }
}

//the whole code is modify by surjeet.b@productivet.com for isKeyAttributeAndEntity check from database
function validateAttributeDb(val, inx) {
  var optchk = {
    'entityRecord': {}
  };
  _.forEach(processSchema.questions, function (keyvalue) {
    if (keyvalue.isKeyValue) {
      if (keyvalue.type.format.title === 'Date') {
        optchk.entityRecord[keyvalue['s#']] = new Date(val.entries[keyvalue['s#']].attributes);
      } else {
        optchk.entityRecord[keyvalue['s#']] = val.entries[keyvalue['s#']].attributes;
      }
    }
  });
  if (Object.getOwnPropertyNames(optchk.entityRecord).length == 0) {
    attrPass2.push(val);
  } else {
    var keyArr = Object.getOwnPropertyNames(optchk.entityRecord)
    var fltr = _.filter(oldSchemas, function(enSchema){
      return keyArr.some(function(val){
        return optchk.entityRecord[val] == enSchema.entityRecord[val]
      })
    });
    if (fltr.length) {
      val.status = 'Fail';
      _.forEach(processSchema.questions, function (keyvalue) {
        if (keyvalue.isKeyValue) {
          val.FailReasons[keyvalue['s#']] = 'Key attribute value already exists. Please provide other value.';
        }
      })
      failDataCol.push(val);
    } else {
      attrPass2.push(val);
    }
  }
}
function allValidation(val, inx) {
  var entry = {
    reviewStatus: 'Draft',
    entries: {},
    csvdata: [],
    FailReasons: {},
    type : 'entity'
  }
  entry.importId = importId;
  entry.createdDateInfo = new Date();
  entry.createdByInfo = { _id: userdata._id, username: userdata.username, firstname: userdata.firstname, lastname: userdata.lastname };

  var passValidate = [];

  _.forEach(val, function (col, colidx) {
    col = col.trim();
    var selectedData = {};
    _.forEach(mapping.fileindex , function (value) {
      if (value == colidx) {
        _.forEach(mapping.schema.mapping, function (data, idx) {
          if (colidx == data.mappingField.mapindex) {
            if(data.mappingField.addBarcodeRule){
              col = regexUtilty.removeWithIdentifier(col,data.mappingField.addBarcodeRulePrefix || '','startWith');
              col = regexUtilty.removeWithIdentifier(col,data.mappingField.addBarcodeRuleSuffix || '','endWith');
            }
            if (data.mappingField.type && _.size(data.mappingField.type) > 0) {
              switch (data.mappingField.type.title) {
                case "Textbox":
                passValidate.push(textBoxValidation(col, data.mappingField, entry));
                break;
                case "Dropdown":
                passValidate.push(dropDownValidation(col, data.mappingField, entry));
                break;
                case "Dropdown List":
                passValidate.push(textBoxValidation(col, data.mappingField, entry));
                break;
                case "Auto Calculated":
                passValidate.push(textBoxValidation(col, data.mappingField, entry));
                break;
                case "Textarea":
                passValidate.push(true);
                break;
                case "File Attachment":
                passValidate.push(true);
                break;
                default:
                passValidate.push(false);
                break;
              }
            }else if (data.mappingField.typeSelector == '2' && _.size(data.mappingField.linkToEntity) > 0) {
              passValidate.push(linkEntityValidation(""+col, data.mappingField, entry));
            }
            if(_.isNull(col) || col == ''){
              entry.FailReasons[data.mappingField['s#']] = data.mappingField.mapField.replace(/\*/g,'') + " is required.";
              passValidate.push(false);
            }
            selectedData = data;
          }
        });
        entry.entries.status = {};
        entry.entries.status.status = 4;

        if (!_.isUndefined(selectedData.mappingField)) {
          if (selectedData.mappingField.optionalProcedure == true) {
            entry.entries.settings = {};
            entry.entries.settings[selectedData.mappingField.procedureseqId] = true;
          }



          //if attribute and sites
          if (selectedData.mappingField['s#']) {
            entry.csvdata.push({ 'value': col, 'seq': selectedData.mappingField['s#'] });
            if (_.find(processSchema.questions, { 'title': selectedData.mappingField.title })) {
              keyValueAttribute = _.find(processSchema.keyvalue, { 'title': selectedData.mappingField.title })
            }
            // parseInt(col) &&
            if (!_.isUndefined(selectedData.mappingField) && !_.isUndefined(selectedData.mappingField.type) && selectedData.mappingField.type.title != 'Dropdown') {
              var setValue;
              if (!_.isUndefined(selectedData.mappingField.validation) && !_.isUndefined(selectedData.mappingField.validation.validationSet)) {
                //if (keyValueAttribute && keyValueAttribute !== undefined && keyValueAttribute.isUniqueKeyValue == true) {
                  _.forEach(setOriginalValue, function (setData) {
                    if (col.toLowerCase() == setData.toLowerCase()) {
                      setValue = setData;
                    }
                  });
                  entry.entries[selectedData.mappingField['s#']] = {
                    attributes: setValue,
                    //removed intentionally bcz it is causing problem
                    // iskey : true
                  };
                //} else {

                  //var setValue;

                  // _.forEach(setOriginalValue, function (setData) {
                  //   if (col.toLowerCase() == setData.toLowerCase()) {
                  //     setValue = setData;
                  //   }
                  // });

                  // entry.entries[selectedData.mappingField['s#']] = {
                  //   attributes: setValue,
                  //   // iskey : false
                  // };
                //}
              } else {
                if (keyValueAttribute && keyValueAttribute !== undefined && keyValueAttribute.isUniqueKeyValue == true) {
                  if (selectedData.mappingField.typeSelector == '2' && _.size(selectedData.mappingField.linkToEntity) > 0) {
                    entry.entries[selectedData.mappingField['s#']] = {
                      attributes: col
                    };
                  }else {
                    // QC3-7346 -- Kajal Patel
                    entry.entries[selectedData.mappingField['s#']] = {
                      attributes: (isNumber(col) ? parseFloat(col) : col)
                    };
                  }
                } else {
                  if (selectedData.mappingField.typeSelector == '2' && _.size(selectedData.mappingField.linkToEntity) > 0) {
                    entry.entries[selectedData.mappingField['s#']] = {
                      attributes: col
                    };
                  }else {
                    // QC3-7346 -- Kajal Patel
                    entry.entries[selectedData.mappingField['s#']] = {
                      attributes: (selectedData.mappingField.type.format.title == 'None' ? col : (isNumber(col) ? parseFloat(col) : col))
                    };
                  }
                }
              }

              if (!_.isUndefined(selectedData.mappingField) && !_.isUndefined(selectedData.mappingField.type) && !_.isUndefined(selectedData.mappingField.type.format) && selectedData.mappingField.type.format.title == 'Date') {
                //akashdeep.s - QC3-6246 - if format is not there then default format is MM/dd/yyyy
                var format = "MM/dd/yyyy";
                if (selectedData.mappingField.type.format.metadata.format) {
                  format = selectedData.mappingField.type.format.metadata.format;
                }
                col = moment(col, format.toUpperCase())._d;
                //col = new Date(col);
                col = new Date(col.getTime() + (mcdiff * 60 * 1000));
                  entry.entries[selectedData.mappingField['s#']] = {
                    attributes: col,
                  };
              }
              if (!_.isUndefined(selectedData.mappingField) && !_.isUndefined(selectedData.mappingField.type) && selectedData.mappingField.type.title == 'Textbox' && !_.isUndefined(selectedData.mappingField.type.format) && selectedData.mappingField.type.format.title == 'Time') {
                if (getTimeFormat(selectedData.mappingField.type.format.metadata.datetime)) {
                  if (col.indexOf(':') !== -1) {
                    var value = col.split(':');
                    if ((!isNumber(value[0]) || parseInt(value[0]) > 24) && (!isNumber(value[1]) || parseInt(value[1]) > 60)) {
                      col = new Date();
                    } else {
                      col = new Date();
                      var h = isNumber(value[0]) ? parseInt(value[0]) : 0;
                      var m = isNumber(value[1]) ? parseInt(value[1]) : 0;
                      // QC3-2806 - Jyotil - Audit Trail Info Time
                      col = moment(new Date().setHours(h, m, 0, 0)).set('year', 1970).set('month', 0).set('date', 1);
                      // QC3-2806 - Jyotil - Audit Trail Info Time
                    }
                  } else {
                    col = new Date();
                  }
                } else {
                  if (col.indexOf(':') !== -1) {
                    var value = col.split(':');
                    if ((!isNumber(value[0]) || parseInt(value[0]) > 12) && (!isNumber(value[1]) || parseInt(value[1]) > 60)) {
                      col = new Date();
                    } else {
                      var h = isNumber(value[0]) ? parseInt(value[0]) : 0;
                      var m = isNumber(value[1]) ? parseInt(value[1]) : 0;
                      // QC3-2806 - Jyotil - Audit Trail Info Time
                      col = moment(new Date().setHours(h, m, 0, 0)).set('year', 1970).set('month', 0).set('date', 1);;
                      // QC3-2806 - Jyotil - Audit Trail Info Time
                    }
                  } else {
                    col = new Date();
                  }
                }
                col = new Date(col);
                col = new Date(col.getTime() + (mcdiff * 60 * 1000));
                entry.entries[selectedData.mappingField['s#'] ] = {
                  attributes: col
                };
              }
            } else {
                //var setValue;
                // console.log(setOriginalValue);
                _.forEach(setOriginalValue, function (setData) {
                  // console.log(setData);
                  if (col.toLowerCase() == setData.toLowerCase()) {
                    setValue = setData;
                  }
                });
                // console.log(setValue);
                // entry.entries[selectedData.mappingField['s#']].attributes = setValue;
                entry.entries[selectedData.mappingField['s#']] = {
                  attributes: setValue,
                  // iskey : true
                };
                // console.log(entry.entries[selectedData.mappingField['s#']]);

            }
            if (!_.isUndefined(selectedData.mappingField) && !_.isUndefined(selectedData.mappingField.type) && selectedData.mappingField.type.title == 'Checkbox') {
              var chkdata = col.split('|');
              setValue = [];

              _.forEach(setOriginalValue, function (setData) {
                _.forEach(chkdata, function (csvData) {
                  if (csvData.toLowerCase() == setData.toLowerCase()) {
                    setValue.push(setData);
                  }
                })
              })
                entry.entries[selectedData.mappingField['s#']] = {
                  attributes: setValue,
                  // iskey : true
                };
            }
          }

        }
      }
    })
  });

  //console.log('--------------------------');
  //console.log(passValidate);
  entry.parentVersion = parentVersion;
  entry.mappingId = mappingId;
  if (passValidate.indexOf(false) == -1) {
    entry.status = "Pass";
    passDataCol.push(entry);
  } else {
    entry.status = "Fail";
    failDataCol.push(entry);
  }
  return;

}
function createqcentries(argument) {
  //if procedure

}
function textBoxValidation(scol, mapField, entry) {
  switch (mapField.type.format.title) {
    case "None":
    return validateField(scol, mapField, entry, false);
    break;
    case "Number":
    return validateField(scol, mapField, entry, true);
    break;
    case "Exponential":
    return validateField(scol, mapField, entry, true);
    break;
    case "Percentage":
    return validateField(scol, mapField, entry, true);
    break;
    case "Currency":
    return validateField(scol, mapField, entry, true);
    break;
    case "Scientific":
    return validateField(scol, mapField, entry, true);
    break;
    case "Date":
    return validateDate(scol, mapField, entry);
    break;
    case "Time":
    return validateTime(scol, mapField, entry);
    break;
    default:
    return true;
    break;
  }
}

function dropDownValidation(scol, mapField, entry) {
  var flag = false;
  var setvalues = [];
  var setobj;
  if (_.isUndefined(mapField.validation.validationSet.existingSet.values)) {
    setobj = _.find(setsarray, function (setval) {
      return _.isObject(mapField.validation.validationSet.existingSet) ? mapField.validation.validationSet.existingSet.title : mapField.validation.validationSet.existingSet == setval.title;
    });
    setvalues = setobj.values;
  } else {
    //var setobj;
    setobj = _.find(setsarray, function (setval) {
      return setval._id.toString() === mapField.validation.validationSet.existingSet._id;
    });
    setvalues = setobj.values;

    //setvalues = mapField.validation.validationSet.existingSet.values;
  }
  _.forEach(setvalues, function (setval) {
    if (scol.toString().trim().toLowerCase() == setval.title.toString().trim().toLowerCase()) {
      if (setOriginalValue.indexOf(setval.title) == -1) {
        setOriginalValue.push(setval.title);
      }
      flag = true;
    }
  });
  if (!flag) {
    entry.FailReasons[mapField['s#']] = "Value does not match with dropdown set values. Please provide other value.";
  }
  return flag;
}

function linkEntityValidation(scol, mapField, entry) {
  var flag = false;
  _.forIn(linkEntityData, function(value,key ) {
    if (key == mapField.linkToEntity.entityId) {
      _.forIn(value, function(val) {
        if (val == scol) {
          flag = true;
        }
      });
    }
  });
  if (!flag) {
    entry.FailReasons[mapField['s#']] = "Value does not match. Please provide other value.";
  }
  return flag;
}

// Code for validate condition provided in questions.
function validateField(col, question, entry, isNumber) {
  if (isNumber) {
    if (!(/^\d+$/.test(col) || (/^\s*[-+]?(\d*\.?\d+|\d+\.)(e[-+]?[0-9]+)?\s*$/i.test(col)))) {
      entry.FailReasons[question['s#']] = "Value does not match. Please provide valid number.";
      return false;
    }
  }
  // Case for Validting regularExpression, validationSet and conditions on question
  if (!_.isUndefined(question.validation)) {

    if (!_.isUndefined(question.validation.regularExpression) && question.validation.regularExpression) {
      if (!(new RegExp(question.validation.regularExpression.regEx)).test(col)) {
        entry.FailReasons[question['s#']] = _.isUndefined(question.validation.regularExpression.validationMessage) ? 'Value does not match. Please provide valid value for regular expression.' : question.validation.regularExpression.validationMessage;
        return false;
      }
      else{
        return true;
      }
    } else if (!_.isUndefined(question.validation.validationSet) && question.validation.validationSet) {
      var setTitle = _.isObject(question.validation.validationSet.existingSet) ? question.validation.validationSet.existingSet.title : question.validation.validationSet.existingSet

      var setobj = _.find(setsarray, function (setval) {
        return setTitle == setval.title;
      });

      var data = _.filter(setobj.values, function (value) {
        return value.isActive === true;
      });


      var returnData = _.filter(data, function (object) {
        if (isValidNumber(object.value)) {
          if ((!isNaN(parseFloat(object.value)) ? parseFloat(object.value) : object.value.toLowerCase()) === (!isNaN(parseFloat(col)) ? parseFloat(col) : col.toLowerCase())) {
            if (setOriginalValue.indexOf(object.value) == -1) {
              setOriginalValue.push(object.value);
            }
            return true;
          }
        } else {
          return object.value === col
        }
      }).length > 0;

      if (returnData) {
        return true;
      } else {
        entry.FailReasons[question['s#']] = _.isUndefined(question.validation.validationSet.validationMessage) ? 'Value does not match. Please provide valid value from sets' : question.validation.validationSet.validationMessage;
        return false;
      }
    } else if (!_.isUndefined(question.validation.condition) && !_.isUndefined(question.validation.condition.conditionTitle) && question.validation.condition) {
      if (!validateCondition(question, col)) {
        entry.FailReasons[question['s#']] = _.isUndefined(question.validation.condition.validationMessage) ? 'Value does not match. Please provide valid value.' : question.validation.condition.validationMessage;
        return false;
      }
      else{
        return true;
      }
    }
  } else {
    return true;
  }
}

// function splitTime(dispalyTime) {
//   var timestring = dispalyTime.replace(' ', ':');
//   var timearray = timestring.split(':');
//   var hour = '';
//   var min = '';
//   if (timearray[3] == 'am') {
//     if (parseInt(timearray[0]) === 12) {
//       timearray[0] = 0;
//     }
//     hour = parseInt(timearray[0]);
//     min = parseInt(timearray[1]);
//   }
//   if (timearray[3] == 'pm') {
//     if (parseInt(timearray[0]) === 12) {
//       timearray[0] = 0;
//     }
//     hour = parseInt(timearray[0]) + 12;
//     min = parseInt(timearray[1]);
//   }
// }

function getTimeFormat(formatId) {

  var allowItems = _.reduce(allTypes[0].formats, function (o, obj) {

    if (obj.title === 'Time') {
      o.push(obj.metadata[0].allowItems)
    }
    return o;
  }, [])[0];
  var timeFormt = _.reduce(allowItems, function (o, item) {
    if (item._id === formatId) {
      o.push(item);
    }
    return o;
  }, [])[0];

  return (timeFormt && (timeFormt['title'] === 'HH:MM' || timeFormt['title'] === 'HH:MM:SS'));

}

// validate time
function validateTime(time, mapField, entry) {
  // Check whether date is number or not if date is number then consider invalid date

  if (time.indexOf(':') !== -1 && time.split(':').length <= 3) {
    var value = time.split(':');
    if (!isNumber(value[0]) || parseInt(value[0]) > 24) {
      entry.FailReasons[mapField['s#']] = "Please provide valid time.";
      return false;
    }
    if (!isNumber(value[1]) || parseInt(value[1]) > 60) {
      entry.FailReasons[mapField['s#']] = "Please provide valid time.";
      return false;
    }
    // need to write code for time validation

    if (!_.isUndefined(mapField.validation) && !_.isUndefined(mapField.validation.condition)) {

      //value new date && scope time from question

      return performedTimeValidation(mapField.validation.condition, time, entry, mapField['s#']);
    }



    // ...
  } else {
    entry.FailReasons[mapField['s#']] = "Please provide valid time.";
    return false;
  }
  return true;
}

//validate date
function validateDate(date, mapField, entry) {
  // Check whether date is number or not if date is number then consider invalid date
  if (getDateFormat(mapField.type.format.metadata.datetime, date, mapField['s#'])) {
    var timestamp = Date.parse(isNumber(date) ? '' : date);
    if (_.isNaN(timestamp)) {
      var datearray = date.split('/');
      var onlyDate = datearray[0];
      var month = datearray[1];
      var year = datearray[2];
      date = new Date(year, month, onlyDate);
    }

    if (!_.isUndefined(mapField.validation) && !_.isUndefined(mapField.validation.condition)) {
      return performedDateValidation(mapField.validation.condition, new Date(date), entry, mapField['s#']);
    } else {
      return true;
    }
  }
  else {
    entry.FailReasons[mapField['s#']] = "Please provide valid date format.";
    return false;
  }
}

function getDateFormat(formatId, date, sequenceId) {

  var allowItems = _.reduce(allTypes[0].formats, function (o, obj) {
    if (obj.title === 'Date') {
      o.push(obj.metadata[0].allowItems)
    }
    return o;
  }, [])[0];
  var dateFormt = _.reduce(allowItems, function (o, item) {
    if (item._id === formatId) {
      o.push(item);
    }
    return o;
  }, [])[0];
  var checkedDate = '';
  if (formatId == "") {
    dateFormt = {};
    dateFormt['title'] = 'MM/DD/YYYY';
  }
  if (dateFormt) {
    if (dateFormt['title'] === 'DD/MM/YYYY') {
      checkedDate = moment(date, 'DD/MM/YYYY').format('DD/MM/YYYY');
      if (checkedDate == date) {
        return true
      }
    } else if (dateFormt['title'] === 'MM/DD/YYYY') {
      checkedDate = moment(date, 'MM/DD/YYYY').format('MM/DD/YYYY');
      if (checkedDate == date) {
        return true
      }
    } else if (dateFormt['title'] === 'YYYY/MM/DD') {
      checkedDate = moment(date, 'YYYY/MM/DD').format('YYYY/MM/DD');
      if (checkedDate == date) {
        return true
      }
    } else if (dateFormt['title'] === 'MM/DD/YY') {
      checkedDate = moment(date, 'MM/DD/YY').format('MM/DD/YY');
      if (checkedDate == date) {
        return true
      }
    } else if (dateFormt['title'] === 'DD/MMM/YY') {
      checkedDate = moment(date, 'DD/MMM/YY').format('DD/MMM/YY');
      if (checkedDate == date) {
        return true
      }
    } else if (dateFormt['title'] === 'MONTH/DD/YYYY') {
      checkedDate = moment(date, 'MMMM/DD/YYYY').format('MMMM/DD/YYYY');
      if (checkedDate == date) {
        return true
      }
    } else if (dateFormt['title'] === 'DD/MMM/YYYY') {
      checkedDate = moment(date, 'DD/MMM/YYYY').format('DD/MMM/YYYY');
      if (checkedDate == date) {
        return true
      }
    }
    return false;
  } else {
    return false;
  }
}
// Common code for validate field value is valid as per given on question validation
// Start by Jaydipsinh

function isNumber(value) {
  return (/^\d+$/.test(value) || (/^\s*[-+]?(\d*\.?\d+|\d+\.)(e[-+]?[0-9]+)?\s*$/i.test(value)))
}

function performedTimeValidation(scope1, value, entry, sequenceId) {
  var scope = {};
  scope = _.cloneDeep(scope1);
  // var momentValue = moment(moment.utc(value).toDate()).format('h:mm:ss a');
  // splitTime(momentValue);
  // value = new Date().setHours(h, m, 0, 0);

  if (value.indexOf(':') !== -1) {
    var timeValue = value.split(':');
    if ((!isNumber(timeValue[0]) || parseInt(timeValue[0]) > 24) && (!isNumber(timeValue[1]) || parseInt(timeValue[1]) > 60)) {
      value = new Date();
    } else {
      var h = isNumber(timeValue[0]) ? parseInt(timeValue[0]) : 0;
      var m = isNumber(timeValue[1]) ? parseInt(timeValue[1]) : 0;
      // QC3-2806 - Jyotil - Audit Trail Info Time
      value = moment(new Date().setHours(h, m, 0, 0)).set('year', 1970).set('month', 0).set('date', 1);
      // QC3-2806 - Jyotil - Audit Trail Info Time
    }
  } else {
    value = new Date();
  }

  if (!_.isUndefined(scope.value)) {
    if (scope.value.indexOf(':') !== -1) {
      var timeValue1 = scope.value.split(':');
      if ((!isNumber(timeValue1[0]) || parseInt(timeValue1[0]) > 24) && (!isNumber(timeValue1[1]) || parseInt(timeValue1[1]) > 60)) {
        scope.value = new Date();
      } else {
        // QC3-2806 - Jyotil - Audit Trail Info Time
        scope.value = moment(new Date().setHours((isNumber(timeValue1[0]) ? parseInt(timeValue1[0]) : 0, m, 0, 0), (isNumber(timeValue1[1]) ? parseInt(timeValue1[1]) : 0))).set('year', 1970).set('month', 0).set('date', 1);
        // QC3-2806 - Jyotil - Audit Trail Info Time
      }
    } else {
      scope.value = new Date();
    }
  }

  if (!_.isUndefined(scope.minimum)) {
    if (scope.minimum.indexOf(':') !== -1) {
      var mdt = new Date(scope.minimum);
      mdt = new Date(mdt.getTime() +(mcdiff * 60 * 1000));
      var h = mdt.getHours();
      var m = mdt.getMinutes();
      // QC3-2806 - Jyotil - Audit Trail Info Time
      scope.minimum = moment(new Date().setHours(h, m, 0, 0)).set('year', 1970).set('month', 0).set('date', 1);
      // QC3-2806 - Jyotil - Audit Trail Info Time
      //var timeValue1 = scope.minimum.split(':');
      //if ((!isNumber(timeValue1[0]) || parseInt(timeValue1[0]) > 24) && (!isNumber(timeValue1[1]) || parseInt(timeValue1[1]) > 60)) {
      //    scope.value = new Date();
      //} else {
      //    scope.minimum = new Date().setHours((isNumber(timeValue1[0]) ? parseInt(timeValue1[0]) : 0, m, 0, 0), (isNumber(timeValue1[1]) ? parseInt(timeValue1[1]) : 0));
      //}
    } else {
      scope.minimum = new Date();
    }
  }

  if (!_.isUndefined(scope.maximum)) {
    if (scope.maximum.indexOf(':') !== -1) {
      var mdt = new Date(scope.maximum);
      mdt = new Date(mdt.getTime() + (mcdiff * 60 * 1000));
      var h = mdt.getHours();
      var m = mdt.getMinutes();
      // QC3-2806 - Jyotil - Audit Trail Info Time
      scope.maximum = moment(new Date().setHours(h, m, 0, 0)).set('year', 1970).set('month', 0).set('date', 1);
      // QC3-2806 - Jyotil - Audit Trail Info Time
      //var timeValue1 = scope.maximum.split(':');
      //if ((!isNumber(timeValue1[0]) || parseInt(timeValue1[0]) > 24) && (!isNumber(timeValue1[1]) || parseInt(timeValue1[1]) > 60)) {
      //    scope.maximum = new Date();
      //} else {
      //    scope.maximum = new Date().setHours((isNumber(timeValue1[0]) ? parseInt(timeValue1[0]) : 0, m, 0, 0), (isNumber(timeValue1[1]) ? parseInt(timeValue1[1]) : 0));
      //}
    } else {
      scope.maximum = new Date();
    }
  }

  // var momentScopeValue = moment(moment.utc(scope.value).toDate()).format('h:mm:ss a');
  // splitTime(momentScopeValue);
  // scope.value = new Date().setHours(hour, min, 0, 0);

  // var momentScopeMinimum = moment(moment.utc(scope.minimum).toDate()).format('h:mm:ss a');
  // splitTime(momentScopeMinimum);
  // scope.minimum = new Date().setHours(hour, min, 0, 0);

  // var momentScopMeximum = moment(moment.utc(scope.maximum).toDate()).format('h:mm:ss a');
  // splitTime(momentScopMeximum);
  // scope.maximum = new Date().setHours(hour, min, 0, 0);

  switch (scope.conditionTitle) {
    case '>':
    if (value > scope.value) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }

    case '<':
    if (value < scope.value) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    case '>=':
    if (value >= scope.value) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    case '<=':
    if (value <= scope.value) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    case '=':
    if (value == scope.value) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    case '!=':
    if (value != scope.value) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    case '>= && <=':
    if (value >= scope.minimum && value <= scope.maximum) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    case '!(>= && <=)':
    if (!(value >= scope.minimum && value <= scope.maximum)) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    default:
    return false;
  }
}

function performedDateValidation(scope, value, entry, sequenceId) {
  if (scope.today === '1') {
    scope.value = new Date().setHours(0, 0, 0);
  }
  switch (scope.conditionTitle) {
    case '>':
    if (new Date(value).setHours(0, 0, 0, 0) > new Date(scope.value).setHours(0, 0, 0, 0)) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    case '<':
    if (new Date(value).setHours(0, 0, 0, 0) < new Date(scope.value).setHours(0, 0, 0, 0)) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    case '>=':
    if (new Date(value).setHours(0, 0, 0, 0) >= new Date(scope.value).setHours(0, 0, 0, 0)) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    case '<=':
    if (new Date(value).setHours(0, 0, 0, 0) <= new Date(scope.value).setHours(0, 0, 0, 0)) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    case '=':
    if (moment.utc(new Date(value).setHours(0, 0, 0, 0)).isSame(new Date(scope.value).setHours(0, 0, 0, 0))) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    case '!=':
    if (!moment.utc(new Date(value).setHours(0, 0, 0, 0)).isSame(new Date(scope.value).setHours(0, 0, 0, 0))) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    case '>= && <=':
    if ((new Date(value).setHours(0, 0, 0, 0) >= new Date(scope.minimumDate).setHours(0, 0, 0, 0) && new Date(value).setHours(0, 0, 0, 0) <= new Date(scope.maximumDate).setHours(0, 0, 0, 0))) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date for import.' : scope.validationMessage;
      return false;
    }
    case '!(>= && <=)':
    if (!(new Date(value).setHours(0, 0, 0, 0) >= new Date(scope.minimumDate).setHours(0, 0, 0, 0) && new Date(value).setHours(0, 0, 0, 0) <= new Date(scope.maximumDate).setHours(0, 0, 0, 0))) {
      return true;
    } else {
      entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date.' : scope.validationMessage;
      return false;
    }
    default:
    entry.FailReasons[sequenceId] = _.isUndefined(scope.validationMessage) ? 'Please provide valid Date.' : scope.validationMessage;
    return false;
  }
}


function validateCondition(scope, value) {
  if (scope.type.format.title === 'Number' || scope.type.format.title === 'Percentage' || scope.type.format.title === 'Currency' || scope.type.format.title === 'Scientific') {
    var decimals = parseInt(scope.type.format.metadata.Decimal);
    decimals = decimals > 25 ? 25 : decimals;
    if (_.isNaN(decimals) || decimals == '') {
      value = Math.floor(parseFloat(value));
    } else {
      value = _.isUndefined(value) ? '' : parseFloat(parseFloat(value).toFixed(decimals));
    }
  }

  if(scope.type.title == 'Textbox' && scope.type.format.title == 'None'){
    value = value.length;
  }else{
    value = !isNaN(parseFloat(value)) ? parseFloat(value) : value.length;
  }

  switch (scope.validation.condition.conditionTitle) {
    case '>':
    return value > parseFloat(scope.validation.condition.value);
    case '<':
    return value < parseFloat(scope.validation.condition.value);
    case '>=':
    return value >= parseFloat(scope.validation.condition.value);
    case '<=':
    return value <= parseFloat(scope.validation.condition.value);
    case '=':
    return value == parseFloat(scope.validation.condition.value);
    case '!=':
    return value != parseFloat(scope.validation.condition.value);
    case '>= && <=':
    return (value >= parseFloat(scope.validation.condition.minimum) && value <= parseFloat(scope.validation.condition.maximum));
    case '!(>= && <=)':
    return !(value >= parseFloat(scope.validation.condition.minimum) && value <= parseFloat(scope.validation.condition.maximum));
    default:
    return false;
  }
}

function validateSets(value, scope, cb) {

  var setTitle = _.isObject(scope.validation.validationSet.existingSet) ? scope.validation.validationSet.existingSet.title : scope.validation.validationSet.existingSet

  var setobj = _.find(setsarray, function (setval) {
    return setTitle == setval.title;
  });

  var data = _.filter(setobj.values, function (value) {
    return value.isActive === true;
  });

  cb(_.filter(data, function (object) {
    if (isValidNumber(object.value)) {
      return (!isNaN(parseFloat(object.value)) ? parseFloat(object.value) : object.value) === (!isNaN(parseFloat(value)) ? parseFloat(value) : value);
    } else {
      return object.value === value
    }
  }).length > 0);
}

// Code for check if given value is valid number or string
function isValidNumber(value) {
  // Currently we are adding patch for just checking - in the value
  return value.indexOf('-') === -1 || value.indexOf('-') === 0;
}
// End by Jaydipsinh
//** text box validatiuons *//


function handleResponse(res,status,resData){
  if(res.respond){
    resData.status = status;
    res.respond(resData)
  }
  else if(res.status){
    res.status(status).json({ 'message': 'file not provided' })
  }
  
}

entityValidationController.prototype.validate = function (req, res, next) {
  mapping = {};
  allsites = [];
  user = {};
  allTypes = {};
  linkEntityData = {};
  parentVersion = '';
  processedData = [];
  passDataCol = [];
  failDataCol = [];
  oldSchemas = [];
  setsarray = [];
  attrPass1 = [];
  attrPass2 = [];
  mapping = req.data;
  reqHeaders = req.headers;
  user = req.user;
  options = { headers : { "x-authorization" : reqHeaders['x-authorization'] } };
  db.sets.find({}).toArray(function (err, sets) {
    db.types.find({ title: 'Textbox' }).toArray(function (err, types) {
      db.collection(req.query.schemaId).find({isDeleted:false}).toArray(function (err, oldscs) {
        db.entities.find({ _id: db.ObjectID(req.query.schemaId) }).toArray(function (err, schema) {
          db.users.find({ _id: db.ObjectID(req.user._id) }).toArray(function (err, usr) {
            restler.get(config.apiEnvUrl + '/getLinkedEntityRecords/' + req.query.schemaId, options).on('complete', function (entityData) {
              linkEntityData = entityData;
              userdata = usr[0];
              setsarray = sets;
              oldSchemas = oldscs;
              allTypes = types;
              processSchema = schema[0];
              readFile(req, res, next);
            });
          });
        });
      });
    });
  });
}

module.exports = new entityValidationController();
