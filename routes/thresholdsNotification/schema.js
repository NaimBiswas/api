'use strict'
/**
 * @name sets-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var array = types.array;
var rString = types.rString;
var rDate = types.rDate;
var rBool = types.rBool;
var rSmallString = types.rSmallString;
var rId = types.rId;
var string = types.string;

var schema = {
  thresholdId : rString.label('thresholdsNotification thresholdId'),
  title: rString.label('thresholdsNotification title'),
  email: rBool.label('thresholdsNotification email'),
  logdate: rDate.label('thresholdsNotification logdate'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
