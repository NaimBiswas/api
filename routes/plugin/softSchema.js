
'use strict'
/**
 * @name questions-schema
 * @author vijeta  <vijeta.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var anyArray = types.anyArray;
var rBool = types.rBool;
var bool = types.bool;
var number = types.number;
var rNumber = types.number;
var string = types.string;
var required = types.required;
var stringArray = types.stringArray;
var any = types.any;
var rBigString = types.bigString;
var BigString = types.bigString;
var optionaldate = types.optionaldate;
var smallString = types.smallString;


var schema = {

  title : rBigString.label('Mapping title'),
  module : rBigString.label('Module'),
  moduleName : rBigString.label('ModuleName'),
  mappingTool : rBigString.label('MapppingTool'),
  appId : BigString.label('App Id'),
  appTitle : any,//BigString.label('App Title'),//optionaldate.label('Expiry Dtae'),
  sappTitle : any,//BigString.label('Plugin App Title'),
  instance : any,
  psqueDetail : anyArray.label('psqueDetail'),
  appVersion : rNumber.label('App Version'),
  levels : anyArray.label('Levels Array'),
  levelIds : anyArray.label('Level Ids'),
  isDefaultSites : bool.label('isDefaultSites'),
  isSyncing : bool.label('isSyncing'),
  timeZone: string.label('Time Zone'),
  type : any,
  isActive : bool.label('isSyncing'),
  file: any,
  UploadSampleCSV : any,
  isAllowHeader : any,
  finalFile : any,
  isDeleted :  bool.label('isDeleted'),
  instanceName : any,
  pluginName : any,
  pluginUniqueIdentifier : any,
  qcAppId : any
  // infusionsoftId : any
  //  lastModifiedTime : string.label('Last MOdified Time')



};

module.exports = schema;
