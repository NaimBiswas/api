'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var validate = require('../../lib/validator');
var db = require('../../lib/db');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'plugin',
    collection: 'plugin',
    schema: schema,
    softSchema: softSchema
  }
});

module.exports = route.router;

/**
 * @swagger
 * definition:
 *    metadata:
 *     properties:
 *      format:
 *        type: string
 */

/**
 * @swagger
 * definition:
 *    Format:
 *      properties:
 *        id:
 *          type: string
 *        title: 
 *          type: string
 *        metadata:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *              - $ref: '#/definitions/metadata'
 */

/**
 * @swagger
 * definition:
 *    QueFormat: 
 *      properties:
 *       id:
 *         type: string
 *       format:
 *         type: array
 *         items:
 *           type: object
 *           allOf:
 *             - $ref: '#/definitions/Format'
 *       title:
 *         type: string     
 */

/**
 * @swagger
 * definition:
 *    Sapp:
 *      properties:
 *        title: 
 *          type: integer
 *        label:
 *          type: string
 *        stype:
 *          type: string
 */

/**
 * @swagger
 * definition:
 *    Paap:
 *      properties:
 *        quetype:
 *          type: string
 *        quetitle: 
 *          type: string
 *        comboId:
 *          type: string
 *        queformat:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *              - $ref: '#/definitions/QueFormat'
 *        quevalidation: 
 *          type: string
 */

/**
 * @swagger
 * definition:
 *    Details:
 *      properties:
 *        paap:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *              - $ref: '#/definitions/Paap'
 *        sapp:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *              - $ref: '#/definitions/Sapp'  
 */

/**
 * @swagger
 * definition:
 *    InstanceName:
 *      properties:
 *        id:
 *          type: string
 *        title:
 *          type: string
 */

/**
 * @swagger
 * definition:
 *   Version:
 *     properties:
 *       version:
 *         type: integer
 *       record:
 *         type: string
 */

/**
 * @swagger
 * definition:
 *  Plugin:
 *    properties:
 *      id:
 *        type: string
 *      module:
 *        type: string
 *      moduleName:
 *        type: string
 *      mappingTool:
 *        type: string
 *      instance:
 *        type: string
 *      appId: 
 *        type: string
 *      appTitle: 
 *        type: string
 *      title:
 *        type: string
 *      sappTitle:
 *        type: string
 *      psqueDetail:
 *        type: array
 *        items:
 *          type: object
 *          allOf:
 *            - $ref: '#/definitions/Details'
 *      appVersion:
 *         type: integer
 *      levels:
 *         type: string
 *      levelIds:
 *         type: string
 *      type: 
 *         type: string
 *      isActive:
 *         type: boolean
 *      isAllowHeader:
 *         type: boolean
 *      isDeleted: 
 *         type: boolean 
 *      file:
 *         type: string
 *      UploadSampleCSV:
 *         type: boolean
 *      isDefaultSites:
 *         type: boolean
 *      isSyncing:
 *         type: boolean
 *      instanceName: 
 *         type: array
 *         items:
 *            type: object
 *            allOf:
 *              - $ref: '#/definitions/InstanceName'
 *      pluginName:
 *         type: string
 *      createdBy: 
 *         type: string
 *      modifiedBy:
 *         type: string
 *      timeZone: 
 *         type: string
 *      createdByName:
 *         type: string
 *      modifiedByName: 
 *         type: string
 *      createdDate:
 *         type: dateTime
 *      modifiedDate:
 *         type: dateTime
 *      versions:  
 *         type: array
 *         items:
 *            type: object
 *            allOf:
 *             - $ref: '#/definitions/Version'
 *      version:
 *         type: integer
 *      isMajorVersion: 
 *         type: boolean
 *      finalFile:
 *         type: string
 */

/**
 * @swagger
 * /plugin:
 *  get:
 *    tags: [Administration]
 *    description: List of mapping name and plugins
 *    produces: 
 *       - application/json
 *    parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,title,appTitle,type,mappingTool,pluginName,sappTitle,modifiedDate,modifiedBy,isActive,isSyncing,isActive])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *    responses:
 *       200:
 *         description: An array of plugins
 *         schema:
 *           $ref: '#/definitions/Plugin'
 */