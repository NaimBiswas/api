'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var string = types.string;
var bool = types.bool;

var schema = {
  title: string.label('title'),
  internalName: string.label('Internal Name'),
  isActive: bool.label('is Active'),
  isPermissionUpdated: bool.label('isPermissionUpdated')
};

module.exports = schema;
