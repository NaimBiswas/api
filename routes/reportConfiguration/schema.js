'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var rString = types.rString;
var rBool = types.rBool;
var string =types.string;
var schema = {
  title: rString.label('title'),
  internalName: rString.label('Internal Name'),
  isActive: rBool.label('is Active'),
  isPermissionUpdated: rBool.label('isPermissionUpdated'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
