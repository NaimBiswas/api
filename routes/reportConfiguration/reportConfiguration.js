'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'reportConfiguration',
    collection: 'reportConfiguration',
    schema: schema,
    softSchema: softSchema
  }
});

/**
 * @swagger
 * definition:
 *   Reports:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       internalName:
 *         type: string
 *       isActive:
 *         type: boolean
 *       isPermissionUpdated:
 *         type: boolean
 */


/**
 * @swagger
 * /reportConfiguration:
 *   get:
 *     tags: [Reports]
 *     description: Returns all reports
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of reports
 *         schema:
 *           $ref: '#/definitions/Reports'

 */

module.exports = route.router;
