'use strict'
/**
 * @name sites-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var rNumber = types.rNumber;
var number = types.number;
var rBool = types.rBool;
var optionalArray = types.optionalArray;
var string = types.string;

var schema = {
  title: rString.label('Sites title'),
  level: object({
    _id: number.label('Leval id'),
    title: rString.label('Leval title')
  }).required().label('Level object'),
  children: optionalArray(object({
    _id: rId.label('Children id'),
    title: rString.label('Children title')
  })),
  parents: array(object({
    _id: rId.label('Parents id'),
    title: rString.label('Parents title')
  })),
  isActive: rBool.label('Is Active'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
