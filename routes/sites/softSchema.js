'use strict'
/**
 * @name sites-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var id = types.id;
var string = types.string;
var array = types.array;
var number = types.number;
var bool = types.bool;
var optionalArray = types.optionalArray;

var schema = {
  title: string.label('Sites title'),
  level: object({
    _id: number.label('Leval id'),
    title: string.label('Leval title')
  }).label('Level object'),
  children: optionalArray(object({
    _id: id.label('Children id'),
    title: string.label('Children title')
  })),
  parents: array(object({
    _id: id.label('Parents id'),
    title: string.label('Parents title')
  })),
  isActive: bool.label('Is Active'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
