'use strict'
/**
 * @name window-schema
 * @author Prachi Thakkar <prachi.t@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var id = types.id;
var rString = types.rString;
var array = types.array;
var optionalArray = types.optionalArray;
var anyArray = types.anyArray;
var rBool = types.rBool;
var bool = types.bool;
var number = types.number;
var string = types.string;
var required = types.required;
var stringArray = types.stringArray;
var any = types.any;
var rBigString = types.rBigString;
var bigString = types.bigString;
var rMediumBigString = types.rMediumBigString;
var mediumBigString = types.mediumBigString;
var rDate = types.rDate;
var date = types.date;

var schema = {
  createdBy: string.label('Window Created By'),
  windowId: any.label('Window Id'),
  windowStatus: object({
    id: number.label('Window Status Id'),
    title: string.label('Window Status Title')
  }).label('Window Status Object'),
  createdDate: date.label('Window Created Date'),
  windowStartDate: date.label('Window Start Date'),
  title: bigString.label('Window Title'),
  description: mediumBigString.label('Window Description').allow(''),
  module: object({
    _id: id.label('Module Id'),
    title: string.label('Module Title')
  }).label('Window Module Object'),
  app: object({
    _id: id.label('App Id'),
    title: string.label('App Name')
  }).label('App Object'),
  filteredAttributes: optionalArray(object({
      attribute: object({
        _id: id.label('Attribute/Entity Id'),
        title: mediumBigString.label('Attribute/Key Attribute Title'),
        's#': string.label('Attribute/Entity Sequence Id'),
        keyAttributeSeqId: string.label('If Entity - Entity Key Attribute Sequence Id'),
        isEntity: bool.label('If Entity assign true else false')
      }).label('Attribute Object'),
      value: any,
      type: string.label('Attribute/Key Attribute Format')
  })),
  testTypeOption: string.label('Test type'),
  testType: object({
    _id: id.label('Question Id'),
    title: mediumBigString.label('Question Title'),
    's#': string.label('Question Sequence Number'),
    type: any,
    parentId: string.label('Question Parent Id - Procedure Sequence Id'),
    comboId: string.label('Question ComboId Id - Procedure/Question Combo Sequence Id')
  }).label('Question Object'),
  sampleSize: number.label('Sample Size'),
  maximumAllowedProcessFailure: number.label('Maximum Allowed Process Failure'),
  allowSecondStage: bool.label('Allow Second Stage'),
  additionalTestSecondStage: number.label('Additional Test For Second Stage').allow(''),
  windowType: any.label('Window Type'),
  windowPlan: any.label('Window Plan'),
  populationSize: number.label('Population Size').allow(''),
  attributeDate: object({
    _id: id.label('Attribute Id'),
    title: string.label('Attribute Title'),
    's#': string.label('Attrubute Sequence Number'),
  }).label('Attribute Object'),
  windowPeriod: object({
   month: number.label('Month'),
   year: number.label('Year')
 }).label('Window Period'),
 siteSelection: string.label('site selection'),
  repeatWindow: bool.label('Repeat Window'),
  isGeneratedRepeatWindow: bool.label('Is Generated Repeat Window'),
  allicableSites:array(
    array(object({
      _id: id.label('Site Id'),
      title: string.label('Site Title')
    }))
  ).label('Window Site Array'),
  windowFrequencyStatusAlert: any.label('Window Frequency Status Alert'),
  emailAddresses: any.label('Email'),
  stopWindowDate: date.label('Stop Window Date'),
  stoppedBy: string.label('Window Stopped By'),
  stopWindowDescription: mediumBigString.label('Stop Window Description').allow(''),
  timeZone: string.label('Time Zone'),
  firstStageInformation: any,
  secondStageStartWithCount: any,
  secondStageInformation: any
};

module.exports = schema;
