'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var linkedTbl = require('../../lib/linkedTbl');
var db = require('../../lib/db');
var q = require('q');
var logger = require('../../logger');
var config = require('../../config/config.js');
var moment = require('moment');
var currentFileName = __filename;

//For generate Frequency
function generateEntries(frequencyObject){
  var windowCalandarEntries = [];
  var currentDate;
  var startDate;
  if (new Date().getMonth() == frequencyObject.windowPeriod.month && new Date().getFullYear() == frequencyObject.windowPeriod.year) {
    startDate = new Date();
  }else {
    startDate = new Date(frequencyObject.windowPeriod.year, frequencyObject.windowPeriod.month, 2);
  }
  var endDate = new Date(frequencyObject.windowPeriod.year, frequencyObject.windowPeriod.month + 1, 0);

  var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
  var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
  var date;
  if(frequencyObject && frequencyObject.windowFrequencyStatusAlert && frequencyObject.windowFrequencyStatusAlert.type === 'Hourly'){
    var hours = frequencyObject.windowFrequencyStatusAlert.hourly.repeats.every;
    var totalhours = (diffDays+1) * 24;
    var schedulehr = totalhours / hours;
    var scheduleendDate = new Date(endDate.setDate(endDate.getDate() + 1));
    for(var hour = 0 ; hour < schedulehr  ; hour ++){
      var calendarDateDaily;
      if(hour == 0){
        calendarDateDaily = startDate.setHours(startDate.getHours());
      }else{
        calendarDateDaily = startDate.setHours(startDate.getHours() + parseInt(hours));
      }
      calendarDateDaily = new Date(calendarDateDaily);
      if(new Date(startDate) < new Date(scheduleendDate)){
        calendardetails(calendarDateDaily,frequencyObject);
      }
    }
  }else if(frequencyObject && frequencyObject.windowFrequencyStatusAlert && frequencyObject.windowFrequencyStatusAlert.type === 'Weekly'){
    currentDate = new Date(startDate);
    var skipDate = new Date(startDate.setDate(startDate.getDate()));
    //var addincalendarDate = new Date(endDate);
    for (var weekly = 0 ; weekly < diffDays + 1 ; weekly ++){
      var weekday = new Date(currentDate.getTime()).getUTCDay();
      var a = new Date(skipDate);
      var addincalendarDate = new Date(a.setDate(a.getDate() + parseInt(7)));
      var currCompareDate=new Date(currentDate).setHours(0,0,0,0);
      var endCompareDate=new Date(endDate).setHours(0,0,0,0);
      if(currentDate >= skipDate && currentDate < addincalendarDate  && currCompareDate<=endCompareDate ){
        for(var i = 0 ; i < frequencyObject.windowFrequencyStatusAlert.weekly.repeats.days.length ; i++){
          if(frequencyObject.windowFrequencyStatusAlert.weekly.repeats.days[i] == "Mon"){
            if(weekday == 1){
              date = new Date(currentDate);
              calendardetails(date,frequencyObject);
            }
          }
          if(frequencyObject.windowFrequencyStatusAlert.weekly.repeats.days[i] == "Tue"){
            if(weekday == 2){
              date = new Date(currentDate);
              calendardetails(date,frequencyObject);
            }
          }
          if(frequencyObject.windowFrequencyStatusAlert.weekly.repeats.days[i] == "Wed"){
            if(weekday == 3){
              date = new Date(currentDate);
              calendardetails(date,frequencyObject);
            }
          }
          if(frequencyObject.windowFrequencyStatusAlert.weekly.repeats.days[i] == "Thu"){
            if(weekday == 4){
              date = new Date(currentDate);
              calendardetails(date,frequencyObject);
            }
          }
          if(frequencyObject.windowFrequencyStatusAlert.weekly.repeats.days[i] == "Fri"){
            if(weekday == 5){
              date = new Date(currentDate);
              calendardetails(date,frequencyObject);
            }
          }
          if(frequencyObject.windowFrequencyStatusAlert.weekly.repeats.days[i] == "Sat"){
            if(weekday == 6){
              date = new Date(currentDate);
              calendardetails(date,frequencyObject);
            }
          }
          if(frequencyObject.windowFrequencyStatusAlert.weekly.repeats.days[i] == "Sun"){
            if(weekday == 0){
              date = new Date(currentDate);
              calendardetails(date,frequencyObject);
            }
          }
        }
      }
      var date1 = new Date(addincalendarDate).setHours(0,0,0,0);
      var currentdate = new Date(currentDate).setHours(0,0,0,0);
      if(date1 === currentdate){
        skipDate = new Date(skipDate.setDate(skipDate.getDate() + parseInt(7)));
        diffDays++;
        continue;
      }
      currentDate = new Date(currentDate.getTime() + 86400000);
    }
  } else if(frequencyObject && frequencyObject.windowFrequencyStatusAlert && frequencyObject.windowFrequencyStatusAlert.type === 'Monthly'){
    date = new Date(startDate.getTime());
    calendardetails(date,frequencyObject);
  }

  //To create calendar window frequency Object
  function calendardetails(calendarDate,frequencyObject){
    var calendarobj = {
      schema:{
        _id:'',
        title:''
      },
      testType:''
    };
    var newscheduleDate = '';
    var a = new Date(calendarDate);
    if (frequencyObject && frequencyObject.windowFrequencyStatusAlert && frequencyObject.windowFrequencyStatusAlert.type !== 'Hourly') {
      var timeTemp = new Date(calendarDate.getTime());
      a.setHours(a.getHours() +timeTemp.getHours());
      a.setMinutes(a.getMinutes() +timeTemp.getMinutes());
      a.setSeconds(a.getSeconds() +timeTemp.getSeconds());
      newscheduleDate=a;
    }
    else {
      newscheduleDate = a;
    }
    calendarobj.title = frequencyObject.title;
    calendarobj.schema._id = db.ObjectID(frequencyObject.app._id);
    calendarobj.schema.title = frequencyObject.app.title;
    calendarobj.testType = frequencyObject.testType;
    calendarobj.scheduledBy = frequencyObject.createdBy;
    calendarobj.type = frequencyObject.windowFrequencyStatusAlert.type;
    calendarobj.date = newscheduleDate;
    calendarobj.email = frequencyObject.emailAddresses;
    calendarobj.notified = false;
    calendarobj.windowId = frequencyObject._id;
    if(frequencyObject.timeZone){
      calendarobj.timeZone = frequencyObject.timeZone;
    }else{
      calendarobj.timeZone = frequencyObject.headers['client-tz'] ? frequencyObject.headers['client-tz'] : config.timeZonesP[config.currentTimeZoneP];
    }
    windowCalandarEntries.push(calendarobj);
  }
  return windowCalandarEntries;
}

//For insert generated window frequency in calendarWindows collection
function storeEntries(frequencyObject, afterPatch, callback){
  try{
    if(frequencyObject.windowStatus == "Current"){
      //For update the window frequency
      if(afterPatch){
        db
        .calendarWindows
        .removeManyAsync({windowId : frequencyObject._id})
        .then(function (data) {
          var windowCalandarEntries = generateEntries(frequencyObject);
          if(windowCalandarEntries){
            db.calendarWindows.insertManyAsync(windowCalandarEntries).then(function (data) {
              //Successfully
              callback(null, data);
            }).catch(function (err) {
              logger.log("function: storeEntries - catch : " + err, 'error', currentFileName);
              callback(err);
            });
          }else{
            callback(null, true);
          }
        }).catch(function (err) {
          logger.log("function: storeEntries - catch : " + err, 'error', currentFileName);
          callback(err);
        });
      }else{
        var windowCalandarEntries = generateEntries(frequencyObject);
        if(windowCalandarEntries.length > 0){
          db.calendarWindows.insertManyAsync(windowCalandarEntries).then(function (data) {
            // QC3-10995 by Yamuna Removed the callback
            callback(null, data);
          }).catch(function (err) {
            callback(err);
          });
        } else {
          callback(null, true);
        }
      }
    }else {
      callback(null, true);
    }
  }catch(e){
    logger.log("function: storeEntries - catch : " + e, 'error', currentFileName);
    callback(e);
  }
}

module.exports = {
  storeEntries: storeEntries
}
