'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var linkedTbl = require('../../lib/linkedTbl');
var db = require('../../lib/db');
var q = require('q');
var logger = require('../../logger');
var config = require('../../config/config.js');
var mailer = require('../../lib/mailer');
var getRecords = require("../qulificationperformance/searchRecords.js");
var windowFrequency = require('./windowFrequencyAlert.js');
var moment = require('moment-timezone');
var handlebars = require('handlebars');
var _ = require('lodash');
var currentFileName = __filename;

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'windows',
    collection: 'windows',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    POST: {
      ONE: {
        before: beforePost,
        after: afterPost
      }
    },
    PATCH: {
      ONE: {
        after: afterPatch
      }
    }
  }
});


var formattedModelForEmail = require('../qulificationperformance/formattedModelForEmail.js');

handlebars.registerHelper('ifCondition', function (v1, operator, v2, options) {
    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
          }
});

function getWindowCount() {
  var deferred = q.defer();
  db.windows.count({}, function(err, getAllWindowCount) {
    if (err) {
      logger.log(err);
    }
    deferred.resolve(getAllWindowCount)
  });
  return deferred.promise;
}

function beforePost(req, res, next) {
  if (req.items) {
    getWindowCount().then(function(count) {
      if (!req.items.windowId) {
        req.items.windowId = count + 1;
      }
      if (!req.items.windowStatus) {
        req.items.windowStatus = {
          'id':1,
          'title':'Current'
        }
      }
      req.items.isGeneratedRepeatWindow = false;
      next();
    });
  }
}

function afterPost(req, res, next) {
  var monthArray = [{value:0,title:'January'},{value:1,title:'February'},{value:2,title:'March'},{value:3,title:'April'},{value:4,title:'May'},{value:5,title:'June'},
  {value:6,title:'July'},{value:7,title:'August'},{value:8,title:'September'},{value:9,title:'October'},{value:10,title:'November'},{value:11,title:'December'}];
  var windowTypes = [ {id: '1', title: "Binomial Distribution"}, {id: '2', title: "Hypergeometric Distribution"}, {id: '3', title: "Customized Windows"} ];
  var frequencyObject = {
    _id: req.data._id,
    title: req.data.title,
    app:{
      _id:req.data.app._id,
      title:req.data.app.title
    },
    windowStartDate: req.data.windowStartDate,
    windowPeriod:{
      month:req.data.windowPeriod.month,
      year:req.data.windowPeriod.year
    },
    testType:req.data.testType,
    createdBy: req.data.createdBy ,
    emailAddresses: req.data.emailAddresses,
    windowStatus: req.data.windowStatus.title,
    windowFrequencyStatusAlert: req.data.windowFrequencyStatusAlert,
    headers:req.headers,
  }
  windowFrequency.storeEntries(frequencyObject, '', function(err,data){
    if(err){
      //something goes wrong
    }
    if(data){
      logger.log('success to update window', 'info');
      var params = {
        windowId: req.data.windowId,
        id: req.data._id
      }
      getRecords.fetchAppRecords(params, function(error, data) {
        if(err){
          //something goes wrong
        }
        if(data == "No Records Found."){
          req.data.countDetails = '0';

        }else if(_.size(data)){
          req.data.countDetails = data;
        }
        formattedModelForEmail.getFormattedModel(req.data).then(function(modelForEmail){
          modelForEmail.templateType = "createWindow";
          notify(modelForEmail,function(err,result){
            if(err){
              logger.log("call:notify error : " + err , 'error', currentFileName);
              return;
            }
            logger.log("call:notify res" , 'info', currentFileName);
          });
        });
      });
    }
    //success
  });
  res.status(201).json(req.data);
}

function afterPatch(req, res, next) {
  var afterPatch = true;
  var windowTypes = [ {id: '1', title: "Binomial Distribution"}, {id: '2', title: "Hypergeometric Distribution"}, {id: '3', title: "Customized Windows"} ];
  if(req.data.windowStatus && req.data.windowStatus.title == "Stopped"){
    var params = {
      windowId: req.data.windowId,
      id: req.data._id
    }
    getRecords.fetchAppRecords(params, function(error, data) {
      // req.data.stopWindowDate = new Date(new Date().setHours(0, 0, 0, 0));
      var query = {};
      if (data.secondStage && data.secondStageInfo) {//QC3-10299 Prachi
        query = {
          firstStageInformation: data.firstStage,
          secondStageInformation: data.secondStageInfo,
          stopWindowDate: new Date(new Date().setHours(0, 0, 0, 0))
        };
      }else {
        query = {
          firstStageInformation: data.firstStage,
          stopWindowDate: new Date(new Date().setHours(0, 0, 0, 0))
        };
      }

      db.windows.findAndModify({_id: db.ObjectID(req.data._id)}, {},{ $set: query},
      {new: true}, function(err, windowData) {
        if(err) {
          logger.log("error ::"+err)
        }
        if(windowData){
          db.windows.auditThis1(windowData, req.ip);
          logger.log('success to update window', 'info');
          formattedModelForEmail.getFormattedModel(windowData.value).then(function(modelForEmail){
            modelForEmail.templateType = "stopWindow";
            notify(modelForEmail,function(err,result){
              if(err){
                logger.log("call:notify error : " + err , 'error', currentFileName);
                return;
              }
              logger.log("call:notify res" , 'info', currentFileName);
            });
          });
        }
      });
    });
  }else{
    var frequencyObject = {
      _id: req.data._id,
      title: req.data.title,
      app:{
        _id:req.data.app._id,
        title:req.data.app.title
      },
      windowPeriod:{
        month:req.data.windowPeriod.month,
        year:req.data.windowPeriod.year
      },
      testType:req.data.testType,
      createdBy: req.data.createdBy ,
      emailAddresses: req.data.emailAddresses,
      windowFrequencyStatusAlert: req.data.windowFrequencyStatusAlert,
      windowStatus: req.data.windowStatus.title,
      headers:req.headers
    }
    windowFrequency.storeEntries(frequencyObject,afterPatch, function(err, res){
      if(err){
        logger.log("call:storeEntries error : " + err , 'error', currentFileName);
      }
    });
  }
  res.status(200).json(req.data);
}

function notify(entity,cb){
  var template = (entity.windowStatus.title != 'Stopped') ? 'qp-notify-common' : 'qp-notify-common';
  mailer.send(template, entity, entity.emailAddresses, function sendMailCallback(err, value) {
    if (err) {
      cb(err, entity);
    } else {
      logger.log('Email has been sent to ' + entity.emailAddresses);
      entity.fileInfo = [];
      delete entity.fileInfo;
      cb(null, entity);
    }
  });
}


/**
* @swagger
* definition:
*   Question:
*     properties:
*       id:
*         type: integer
*       title:
*         type: string
*       isActive:
*         type: boolean
*       type:
*         $ref: '#/definitions/Type'
*       validation:
*         $ref: '#/definitions/Validation'
*       modifiedDate:
*         type: dateTime
*       createdDate:
*         type: dateTime
*       createdByName:
*         type: string
*       modifiedByName:
*         type: string
*       modifiedBy:
*         $ref: '#/definitions/ModifiedBy'
*       createdBy:
*         $ref: '#/definitions/CreatedBy'
*       versions:
*           type: array
*           items:
*              type: object
*              allOf:
*              - $ref: '#/definitions/Version'
*       version:
*           type: integer
*       isMajorVersion:
*           type: boolean
*/

/**
* @swagger
* /questions:
*   get:
*     tags: [Question]
*     description: Returns all questions
*     produces:
*       - application/json
*     parameters:
*       - name: page
*         description: Page Number
*         in: query
*         required: false
*         type: string
*       - name: pagesize
*         description: Page Size
*         in: query
*         required: false
*         type: string
*       - name: select
*         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,title,type.title,modifiedDate,modifiedBy,version,versions,isActive])
*         in: query
*         required: false
*         type: string
*       - name: sort
*         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
*         in: query
*         required: false
*         type: string
*     responses:
*       200:
*         description: An array of questions
*         schema:
*           $ref: '#/definitions/Question'

*/

/**
* @swagger
* /questions/{id}:
*   get:
*     tags: [Question]
*     description: Returns a object question
*     produces:
*       - application/json
*     parameters:
*       - name: id
*         description: Questtion's id
*         in: path
*         required: true
*         type: integer
*       - name: pagesize
*         description: Page Size
*         in: query
*         required: false
*         type: string
*     responses:
*       200:
*         description: An object of question
*         schema:
*           $ref: '#/definitions/Question'

*/

/**
* @swagger
* /questions/{id}:
*   patch:
*     tags: [Question]
*     description: Updates a question
*     produces:
*       - application/json
*     parameters:
*       - name: id
*         description: Question's id
*         in: path
*         required: true
*         type: integer
*       - name: question
*         description: question object
*         in: body
*         required: true
*         schema:
*           $ref: '#/definitions/Question'
*     responses:
*       200:
*         description: Successfully updated
*/
/**
* @swagger
* /questions:
*   post:
*     tags: [Question]
*     description: Creates a new question
*     produces:
*       - application/json
*     parameters:
*       - name: question
*         description: Question object
*         in: body
*         required: true
*         schema:
*           $ref: '#/definitions/Question'
*     responses:
*       201:
*         description: Successfully created
*/

module.exports = route.router;
