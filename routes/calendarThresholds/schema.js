'use strict';
/**
 * @name calendarthresholds-schema
 * @author kajal Patel <kajal.p@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var string = types.string;
var rDate = types.rDate;
var any = types.any;
var array = types.array;

var schema = {
  schema: array(object({
    _id: rId.label('Schema id'),
    title: rString.label('Schema title')
  })).required().label('schema object'),
  title: rString.label('calendarThresholds title'),
  type: rString.label('calendarThresholds type'),
  scheduledBy: rString.label('calendarThresholds scheduled by user'),
  date: rDate.label('calendarThresholds date'),
  endDate: any.label('End Date'),
  startTime: any.label('Start Time'),
  endTime: any.label('End Time'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
