'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'calendarThresholds',
    collection: 'calendarThresholds',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    GET: {},
    PUT: false,
    PATCH: false,
    POST: false
  }
});

module.exports = route.router;
