/**
* @author Jyotil Raval <jyotil.r@productivet.com>
* @description Export Schema with Whole Schema detail, Attributes, Entities, EntityRecord, Procedure, Questions, Sets
* @param {String} FileName
* @return {String} OK
* @version 1.0
*/

(function () {
  'use strict';
  var express = require('express');
  var db = require('../../lib/db');
  var _ = require('lodash');
  var fs = require('fs');
  var decode = require('../../lib/oauth/model').getAccessToken;
  var lzstring = require('lz-string');
  var logger = require('../../logger');
  var currentFileName = __filename;
  var importSchemaCore = {
    app: require('./importSchemaCore.js')
  };


  module.exports = function (app) {
    var apiRoutes = express.Router();

    apiRoutes.post('/app', function (req, res, next) {
      authenticRequest(req, res, next, 'app');
    });

    function authenticRequest (req, res, next, type) {
      logger.log('API Called : api/importSchema', 'info', currentFileName);
      var xToken = req.headers['x-authorization'] || req.query['x-authorization'];
      if (xToken) {
        logger.log('if(xToken) : Inside', 'info', currentFileName);
        db['allAuthToken'].findOne({
          token: xToken
        }, function (err, token) {
          if (err || !token) {
            logger.log('Collection: allAuthToken - Failed : ' + err, 'error', currentFileName);
            res.status(403).json({
              message: 'You are not authorized.'
            });
            return;
          }
          logger.log('Collection: allAuthToken- success', 'info', currentFileName);
          renderPost(req, res, next, (type || 'app'));
        });
      } else if (req.headers['authorization'] || req.query['authorization']) {
        logger.log("else if (req.headers['authorization'] || req.query['authorization'])- Inside authorization brear token", 'info', currentFileName);
        var decompressToken = lzstring.decompressFromEncodedURIComponent(req.headers['authorization']);
        var token = decompressToken;
        if (token && _.startsWith(token, 'Bearer ')) {
          logger.log("if(token && _.startsWith(token,'Bearer ')) : Inside decompressed authorization brear token ", 'info', currentFileName);
          decode(token.substring(7), function (err, data) {
            if (err) {
              logger.log('Error: decoding authorization brear token failed : '+ err, 'error', currentFileName);
              res.status(403).json({
                message: 'You are not authorized.'
              });
              return;
            }
            logger.log('decode: Success - Inside decoded authorization brear token Info', 'info', currentFileName);
            renderPost(req, res, next, (type || 'app'));
          });
        } else {
          logger.log('else: authorization brear token decompression failed', 'info', currentFileName);
          res.status(403).json({
            message: 'You are not authorized.'
          });
        }
      } else {
        logger.log('else: No token found', 'info', currentFileName);
        res.status(403).json({
          message: 'You are not authorized.'
        });
      }
    }

    function renderPost (req, res, next, type) {
      logger.log('Function: RenderPost - Start', 'info', currentFileName);
      // var fileName = req.body || {};
      fs.readFile((process.env.FILE_STORE+'1517497991911-Complicated.pqt'), function read(err, data) {
        if (err) {
          res.status(400).json({
            message: err
          });
        } else {
          var fileContent = data.toString();
          importSchemaCore[type || 'app'].generateSchemaDetail(fileContent).then(function (respons) {
            res.status(200).json(JSON.parse(fileContent));
          }).catch(function (err) {
            res.status(400).json({
              message: err
            });
          })
        }
      });
    }

    app.use('/importSchema/', apiRoutes);
  }

})();
