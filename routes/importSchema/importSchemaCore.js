/**
* @author Jyotil Raval <jyotil.r@productivet.com>
* @description import Schema with Whole Schema detail, Attributes, Entities, EntityRecord, Procedure, Questions, Sets
* @param {Object} schemaDetail
* @return {String} OK
* @version 1.0
*/

(function () {
  'use strict';

  var q = require('q');
  var logger = require('../../logger');
  var currentFileName = __filename;

  function generateSchemaDetail(options) {
    logger.log("function: generateSchemaDetail - start", 'info', currentFileName);
    try {
      var deferred = q.defer();
      deferred.resolve(options);
    } catch (err) {
      deferred.reject(err);
    }
    return deferred.promise;
  }


  module.exports = {
    generateSchemaDetail: generateSchemaDetail
  }
})();
