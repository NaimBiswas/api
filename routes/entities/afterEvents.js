var entityRecords = require('../entityRecords');
var dbConfig = require('../../lib/db/db');
var db = require('../../lib/db');
var log = require('../../logger');
var _ = require('lodash');
var logger = require('../../logger');
var currentFileName = __filename;


var afterEvents = {
    afterPost,
    afterPatch
}

function afterPost(req, res, next) {
    logger.log("function: afterPost - start", 'info', currentFileName);
    //req.data contains the posted / patched
    //schedule object with createdBy and modifiedBy
    //And all, orignal object can be accessed from
    //req.items
    try {
        var entities = [];
        entities.push({
            collection: req.data._id.toString('hex'),
            audit: true,
            index: {
                title: ''
            }
        });

        dbConfig.configure.apply(this, entities);
        entityRecords.use('/' + req.data._id.toString('hex'), require('../entityRecords/entityRecords')(req.data._id.toString('hex')));
    } catch (err) {
        log.log('Error while generating route :: ' + err, 'error');
    }

    var linkedEntityArray = [];

    _.forEach(req.data.questions, function (que) {
        if (que.linkToEntity && !_.isUndefined(que.linkToEntity.entityId) && !_.isNull(que.linkToEntity.entityId)) {
            var tempLinkToEntity = {
                entityId: req.data._id,
                entityAttributeSeqId: que['s#'],
                linkedEntityId: db.ObjectID(que.linkToEntity.entityId),
                linkedEntityKeyAttributeSeqId: que.linkToEntity.entityKeyAttributeId
            };
            linkedEntityArray.push(tempLinkToEntity);
        }
    });

    if (linkedEntityArray && linkedEntityArray.length > 0) {
        db['mappedLinkToEntity'].insertMany(linkedEntityArray);
    }
}

function afterPatch(req, res, next) {
    _.forEach(req.data.questions, function (que) {
        if (que.linkToEntity && !_.isUndefined(que.linkToEntity.entityId) && !_.isNull(que.linkToEntity.entityId)) {
            var tempLinkToEntity = {
                entityId: db.ObjectID(req.data._id),
                entityAttributeSeqId: que['s#'],
                linkedEntityId: db.ObjectID(que.linkToEntity.entityId),
                linkedEntityKeyAttributeSeqId: que.linkToEntity.entityKeyAttributeId,
                linkedEntityKeyAttributeFormat: que.linkToEntity.entityKeyAttributeDateFormat
            };

            //If entity non-key attribute linked with another entites key attribute
            db['mappedLinkToEntity'].findAndModify({
                entityId: db.ObjectID(tempLinkToEntity.entityId),
                entityAttributeSeqId: tempLinkToEntity.entityAttributeSeqId
            }, [], {
                $set: tempLinkToEntity
            }, {
                new: true,
                upsert: true
            }, function (err, data) {
                if (err) {
                    console.log(err);
                }
            });
        } else {
            //If remove link to entity need to remove from mappedLinkToEntity
            //console.log('-------------Inside Remove holdRemovedAttributeSeqIds-------------------');
            //need to pass removed attribute's seq id

            if (req.data && req.data.holdRemovedAttributeSeqIds && req.data.holdRemovedAttributeSeqIds.length) {
                _.forIn(req.data.holdRemovedAttributeSeqIds, function (quesSeq) {
                    db['mappedLinkToEntity'].remove({
                        entityId: db.ObjectID(req.data._id),
                        entityAttributeSeqId: quesSeq
                    });
                });
            }
        }
    });
}

module.exports = afterEvents;