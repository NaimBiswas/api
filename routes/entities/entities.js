'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var log = require('../../logger');
var linkedTbl= require('../../lib/linkedTbl');
var logger = require('../../logger');
var currentFileName = __filename;
var afterEvents = require('./afterEvents');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'entities',
    collection: 'entities',
    schema: schema,
    softSchema: softSchema
  },
  types:{
         /**
         * @swagger
         * definition:
         *   Question_entity:
         *     properties:
         *       title:
         *         type: string
         *       type:
         *         $ref: '#/definitions/Type'
         *       isKeyValue:
         *         type: boolean
         *       comboId:
         *         type: integer
         *       validation:
         *         $ref: '#/definitions/Validation'
         */

    /**
     * @swagger
     * definition:
     *   Entity:
     *     properties:
     *       id:
     *         type: integer
     *       title:
     *         type: string
     *       isActive:
     *         type: boolean
     *       isInstructionForUser:
     *         type: boolean
     *       tag:
     *           type: array
     *           items:
     *              type: string
     *       module:
     *           type: array
     *           items:
     *              type: object
     *              allOf:
     *              - $ref: '#/definitions/Module'
     *       questions:
     *           type: array
     *           items:
     *              type: object
     *              allOf:
     *              - $ref: '#/definitions/Question_entity'
     *       description:
     *         type: string
     *       modulesTitle:
     *         type: string
     *       instructionURL:
     *         type: string
     *       createdDate:
     *         type: dateTime
     *       modifiedDate:
     *         type: dateTime
     *       createdByName:
     *         type: string
     *       modifiedByName:
     *         type: string
     *       modifiedBy:
     *         $ref: '#/definitions/ModifiedBy'
     *       createdBy:
     *         $ref: '#/definitions/CreatedBy'
     *       versions:
     *           type: array
     *           items:
     *              type: object
     *              allOf:
     *              - $ref: '#/definitions/Version'
     *       values:
     *           type: array
     *           items:
     *              type: object
     *              allOf:
     *              - $ref: '#/definitions/Value'

     */

    /**
     * @swagger
     * /entities:
     *   get:
     *     tags: [Entity]
     *     description: Returns all entities
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: page
     *         description: Page Number
     *         in: query
     *         required: false
     *         type: string
     *       - name: q[_id]
     *         description: Entitie's Id
     *         in: query
     *         required: false
     *         type: string
     *       - name: pagesize
     *         description: Page Size
     *         in: query
     *         required: false
     *         type: string
     *       - name: select
     *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,title,modulesTitle,modifiedDate,modifiedBy,version,versions,description,isActive,isAuditing])
     *         in: query
     *         required: false
     *         type: string
     *       - name: sort
     *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
     *         in: query
     *         required: false
     *         type: string
     *     responses:
     *       200:
     *         description: An array of entities
     *         schema:
     *           $ref: '#/definitions/Entity'

     */

     /**
      * @swagger
      * /entities/{id}:
      *   get:
      *     tags: [Entity]
      *     description: Returns a object entity
      *     produces:
      *       - application/json
      *     parameters:
      *       - name: id
      *         description: Entity's id
      *         in: path
      *         required: true
      *         type: integer
      *       - name: pagesize
      *         description: Page Size
      *         in: query
      *         required: false
      *         type: string
      *     responses:
      *       200:
      *         description: An object of entity
      *         schema:
      *           $ref: '#/definitions/Entity'
      */


     /**
     * @swagger
     * /entities:
     *   post:
     *     tags: [Entity]
     *     description: Creates a new entity
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: entity
     *         description: Entity object
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/Entity'
     *     responses:
     *       201:
     *         description: Successfully created
     */
     /**
      * @swagger
      * /entities/{id}:
      *   patch:
      *     tags: [Entity]
      *     description: Updates an Entity
      *     produces:
      *       - application/json
      *     parameters:
      *       - name: id
      *         description: Entity's id
      *         in: path
      *         required: true
      *         type: integer
      *       - name: entity
      *         description: Entities's object
      *         in: body
      *         required: true
      *         schema:
      *           $ref: '#/definitions/Entity'
      *     responses:
      *       200:
      *         description: Successfully updated
      */

    POST:{
      ONE: {
        after : afterPost
      },
      ONESOFT: {
        after: afterPost
      }
    },
    PATCH:{
      ONE:{
        after : afterPatch
      },
      ONESOFT:{
        after : afterPatch
      }
    }
  }
});

function afterPatch(req, res, next) {
  logger.log("function: afterPatch - start", 'info', currentFileName);
  afterEvents.afterPatch(req, res, next);
  linkedTbl.insertInLinked('entities', req.data);
  res.json(req.data);
}

function afterPost(req, res, next) {
  logger.log("function: afterPost - start", 'info', currentFileName);
  afterEvents.afterPatch(req, res, next);
  linkedTbl.insertInLinked('entities', req.data);
  res.json(req.data);
}

module.exports = route.router;
