'use strict'
/**
 * @name procedures-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var rBool = types.rBool;
var number = types.number;
var bool = types.bool;
var string = types.string;
var required = types.required;
var stringArray = types.stringArray;
var any = types.any;
var rBigString = types.bigString;
var bigString = types.bigString;
var id = types.id;
var extraLargeString = types.extraLargeString;
var rExtraLargeString = types.rExtraLargeString;

var schema = {
  module: array(object({
    _id: id.label('Module schema id'),
    title: string.label('Schema title')
  })).label('Schema module'),
  holdRemovedAttributeSeqIds : any.label('Removed Attribute Sequence Id(s)'),
  modulesTitle: rBigString.label('Modules Title'),
  title: rBigString.label('Entities title'),
  tags: stringArray.label('Tags string array'),
  discoverable: bool.label('Discoverable'),
  isInstructionForUser: bool.label('is Instrcution For Users'),
  instructionURL: any.label('Schema instructionURL'),
  file: any.label('Schema upload file'),
  dmsfile: any.label('DMS file'),
  instructionForUser: any.label('Instruction for user').allow(''),
  description: any.label('Description').allow(''),
    isActive: bool.label('Active?'),
    version : number.label('Entity version'),
    values : any,
  highlightEntityRecord: bool.label('HighlightEntityRecord'),
  questions: array(object({
    //_id: rId.label('Question id'),
    title: rExtraLargeString.label('Questions title'),
    comboId: string.label('Question Combo Id'),
    's#': string.label('Sequence number'),
    isKeyValue: bool.label('is Key Value?'),
    addBarcodeRule: bool.label('Add Barcode Rule'),
    addBarcodeRulePrefix: any.label('Add Barcode Rule Prefix').allow(''),
    addBarcodeRuleSuffix: any.label('Add Barcode Rule Suffix').allow(''),
    //version : number.label('Question version'),
    isOptional: bool.label('is Optional'),
    type: object({
      _id: id.label('Type id'),
      title: string.label('Type title'),
      format: object({
        _id: id.label('Format id'),
        title: string.label('Format title'),
        metadata: any.label('Metadata required for the format')
      })
    }).label('Type object'),
    validation: any.label('Validation array'),
    typeSelector: any.label('Type Selector'),
    linkToEntity: any.label('Link To Entity'),
    criteria: any
    })).label('Questions array of object'),
  isAuditing: bool.label("Is Auditing ?"),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
