'use strict'
/**
 * @name questions-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var anyArray = types.anyArray;
var rBool = types.rBool;
var bool = types.bool;
var number = types.number;
var string = types.string;
var required = types.required;
var stringArray = types.stringArray;
var any = types.any;
var rBigString = types.bigString;
var bigString = types.bigString;
var optionaldate = types.optionaldate;
var smallString = types.smallString;



var schema = {
    title : rBigString.label('Friendly Token Name'),
    expirydate : any,//optionaldate.label('Expiry Dtae'),
    isActive : rBool.label('Active?'),
    apps : anyArray,
    entities : anyArray,
    apitoken : any,
    hash:any, //crypto sha256 encryption
    ipaddress:any,
    timeZone: string.label('Time Zone')

};

module.exports = schema;
