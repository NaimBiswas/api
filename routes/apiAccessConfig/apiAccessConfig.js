'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var crypto = require('crypto');
var config = require('../../config/config');
//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'apiAccessConfig',
    collection: 'apiAccessConfig',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    POST: {
      ONE: {
        before: generateToken,
        // after  : afterPost
      }
    }
  }
});

function generateToken(req, res, next) {
 // logger.log("function: generateToken - start", 'info', currentFileName);
  var date = new Date();
  req.items.apitoken = crypto.randomBytes(10).toString('hex') + date.getTime().toString();
  req.items.hash = crypto.createHmac('sha256', config.apiProject.secretKey).update(req.items.apitoken).digest('hex');
  next();
}
/**
 * @swagger
 * definition:
 *   Question:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       isActive:
 *         type: boolean
 *       type:
 *         $ref: '#/definitions/Type'
 *       validation:
 *         $ref: '#/definitions/Validation'
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       createdBy:
 *         $ref: '#/definitions/CreatedBy'
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       version:
 *           type: integer
 *       isMajorVersion:
 *           type: boolean
 */

/**
 * @swagger
 * /questions:
 *   get:
 *     tags: [Question]
 *     description: Returns all questions
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,title,type.title,modifiedDate,modifiedBy,version,versions,isActive])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of questions
 *         schema:
 *           $ref: '#/definitions/Question'

 */

/**
 * @swagger
 * /questions/{id}:
 *   get:
 *     tags: [Question]
 *     description: Returns a object question
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Questtion's id
 *         in: path
 *         required: true
 *         type: integer
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An object of question
 *         schema:
 *           $ref: '#/definitions/Question'

 */

/**
 * @swagger
 * /questions/{id}:
 *   patch:
 *     tags: [Question]
 *     description: Updates a question
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Question's id
 *         in: path
 *         required: true
 *         type: integer
 *       - name: question
 *         description: question object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/Question'
 *     responses:
 *       200:
 *         description: Successfully updated
 */
/**
 * @swagger
 * /questions:
 *   post:
 *     tags: [Question]
 *     description: Creates a new question
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: question
 *         description: Question object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/Question'
 *     responses:
 *       201:
 *         description: Successfully created
 */

module.exports = route.router;
