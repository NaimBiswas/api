'use strict'
/**
 * @name resources-schema
 * @author shoaib ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var rString = types.rString;
var rBool = types.rBool;
var string = types.string;

var schema = {
  title: rString.label('Title'),
  entity: string.label('Entity'),
  isActive: rBool.label('Active?'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
