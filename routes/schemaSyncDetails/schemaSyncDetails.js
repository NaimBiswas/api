'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var validate = require('../../lib/validator');
var db = require('../../lib/db');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'schemaSyncDetails',
    collection: 'schemaSyncDetails',
    schema: schema,
    softSchema: softSchema
  }
});

/**
 * @swagger
 * definition:
 *   SchemaSyncDetails:
 *     properties:
 *       id:
 *         type: integer
 *       formName:
 *         type: string
 *       formId:
 *         type: integer
 *       version:
 *           type: integer
 *       toSync:
 *           type: boolean
 *       lastSyncDateTime:
 *           type: integer
 *       moduleId:
 *           type: dateTime
 */


/**
 * @swagger
 * /schemaSyncDetails:
 *   get:
 *     tags: [Reports]
 *     description: Returns all schemaSyncDetails
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of schemaSyncDetails
 *         schema:
 *           $ref: '#/definitions/SchemaSyncDetails'
 */

module.exports = route.router;
