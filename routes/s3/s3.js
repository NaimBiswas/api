(function () {

  'use strict'
  var crypto = require('crypto');
  // var access_Token = "EwA4A8l6BAAU7p9QDpi/D7xJLwsTgCg3TskyTaQAAdQ9f5iPBu37dNZRBKYsL3b+4Z0rpWCjOCF7x2GB/lbjbgXToL0ATzowj1+D/rRTcE/mkh/CuYC2p3AkOgHksXOgrO3Wx1/HCN/axgcsNZbVMu1N+U1vuXQBD9V8AmqAAQedGsg35veyb5n4sX7PYcGVOhAcs6iS+boBFSs49dUgsrpTO0V5K4XYJfyBP3zkZQiQ7C4PyVft1mqCaNgiMZK3F0dDGspLf0I+Kwjl7EnK8OGZxdq88Zpo4vuaNf4zdNekq91MBFAeN2jzH0OKPB9Dzqy6zLsl7AsXHkKwb7+aM6GjRf42ZPjQ7fB7J+dFvt5tqwLQ5fbsX2BksLYDEpADZgAACJiOc9m0P7tSCALvol/uGhP5qDzONs/KC2BVfKwayms/59ZJGi0Asq4rXG6XVJDmTa4ZkbHTyQTFMiEfcovE7m5VP6QympP5H0rSdl41fnNwyPzdEgs4E7eyFHluTzPIoDpwmK/v2AWHrHlPqLDGkLst3k7WIsEOWkLxE0rQ0ufWObDwpeJMJBymYCypIvVe9ECsQ+XCgKzcuAVBMSU8awEkruMkLBVzJwqvWg8SnG8du5STMTNof4todE6sCO5jiJK6Vf1UHiQOObJRjXXRtgmb1VAqfAMraRTo3PJSQdp6lyWn/1VeKXjSq8a/kL5zLiU3ejLFoxva8BEGLLIvYi1QAnkgOGHWERL26GEA8q8oXst5RQdKl0CzXEN/F+mTNQKZZMs1mceiX/PEHVAfGCWx2baFwM/TV19bDU7tWG93liAQODrxjKuVeSM+wieY1IV4xZRMATEMSE1KWYuPoU/obEr0aGqAzTVxcXfAOUghRD18UP5L4Wo/+fOWnqggFJc19AO4SmVtzHWN8EpwTHcqA0P+zesOPOU9cTgGZFWGqf+zuQ/HOb1oxUi/nTlgsltw+3qoQNGK9yEK6Zw3dMCeKQgzX+VvqYriZPv+7z6isVk5P96qCKb6YVMD+iOAlRxeiFnziSqmt78oxRv9inV2L1mo6O3ZJNzsK+0KT6X0f/NdT7PjXiTLJhXmJV3XMWp6SgI="
  var service = function () { };

  // This is the entry function that produces data for the frontend
  // config is hash of S3 configuration:
  // * bucket
  // * region
  // * accessKey
  // * secretKey
  function s3Credentials(config, params) {
    return {
      endpoint_url: "https://" + config.bucket + ".s3.amazonaws.com",
      params: s3Params(config, params)
    }
  }

  // Returns the parameters that must be passed to the API call
  function s3Params(config, params) {
    var credential = amzCredential(config);
    var policy = s3UploadPolicy(config, params, credential);
    var policyBase64 = new Buffer(JSON.stringify(policy)).toString('base64');
    return {
      key: params.filename,
      acl: 'private',
      success_action_status: '201',
      policy: policyBase64,
      "content-type": params.contentType,
      'x-amz-algorithm': 'AWS4-HMAC-SHA256',
      'x-amz-credential': credential,
      'x-amz-date': dateString() + 'T000000Z',
      'x-amz-signature': s3UploadSignature(config, policyBase64, credential)
    }
  }

  function dateString() {
    var date = new Date().toISOString();
    return date.substr(0, 4) + date.substr(5, 2) + date.substr(8, 2);
  }

  function amzCredential(config) {
    return [config.accessKey, dateString(), config.region, 's3/aws4_request'].join('/')
  }

  // Constructs the policy
  function s3UploadPolicy(config, params, credential) {
    return {
      // 5 minutes into the future
      expiration: new Date((new Date).getTime() + (5 * 60 * 1000)).toISOString(),
      conditions: [
        { bucket: config.bucket },
        { key: params.filename },
        { acl: 'private' },
        { success_action_status: "201" },
        // Optionally control content type and file size
        // A content-type clause is required (even if it's all-permissive)
        // so that the uploader can specify a content-type for the file
        ['starts-with', '$Content-Type', ''],
        ['content-length-range', 0, 5368709120],
        { 'x-amz-algorithm': 'AWS4-HMAC-SHA256' },
        { 'x-amz-credential': credential },
        { 'x-amz-date': dateString() + 'T000000Z' }
      ],
    }
  }

  function hmac(key, string) {
    var hmac = crypto.createHmac('sha256', key);
    hmac.end(string);
    return hmac.read();
  }

  // Signs the policy with the credential
  function s3UploadSignature(config, policyBase64, credential) {
    var dateKey = hmac('AWS4' + config.secretKey, dateString());
    var dateRegionKey = hmac(dateKey, config.region);
    var dateRegionServiceKey = hmac(dateRegionKey, 's3');
    var signingKey = hmac(dateRegionServiceKey, 'aws4_request');
    return hmac(signingKey, policyBase64).toString('hex');
  }
  service.prototype.s3Credentials = s3Credentials;
  module.exports = new service();

})();