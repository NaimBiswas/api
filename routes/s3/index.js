var crypto = require('crypto');
var path = require('path');
var express = require('express');
var http = require('http');
var db = require('../../lib/db');
var apiRoutes = express.Router();
var _ = require('lodash');
var s3 = require('./s3');
var fs = require('fs');
var AWS = require('aws-sdk');
var ObjectId = require('mongodb').ObjectID;

module.exports = function (app) {
    var s3Config;
    var s3bucket;

    function getCreds(callback) {
        db.collection('pluginConfig').findOne({ entity: "s3" }, function (err, success) {

            if (err) {
                callback();
                logger.log(err);
            }

            if(success == null){
                var nullValue = true;
                callback(nullValue);
            } else {
                var nullValue = false;
                s3Config = {
                    accessKey: success.config.accesskeyid,
                    secretKey: success.config.secretaccesskey,
                    bucket: success.config.bucketname,
                    region: success.config.region
                };

                s3bucket = new AWS.S3({
                    params: { Bucket: s3Config.bucket },
                    signatureVersion: 'v4',
                    accessKeyId: s3Config.accessKey,
                    region: s3Config.region,
                    secretAccessKey: s3Config.secretKey
                });
                callback(nullValue);
            }
        })
    }

    function getVersionCreds(version, amazonID, callback) {
        var createAmazoID = ObjectId(amazonID);
        db.collection('pluginConfigAuditLogs').findOne({ identity : createAmazoID, version : +(version) }, function (err, success) {

            if (err) {
                callback();
                logger.log(err);
            }
            else{
                s3Config = {
                    accessKey: success.config.accesskeyid,
                    secretKey: success.config.secretaccesskey,
                    bucket: success.config.bucketname,
                    region: success.config.region
                };
                s3bucket = new AWS.S3({
                    params: { Bucket: s3Config.bucket },
                    signatureVersion: 'v4',
                    accessKeyId: s3Config.accessKey,
                    region: s3Config.region,
                    secretAccessKey: s3Config.secretKey
                });
                callback();
            }
        })
    }

    function getConfigData(request, response) {
        var decodeFileName = decodeURIComponent(request.params.name);
        var decodeFileType = decodeURIComponent(request.params.type);
            
        getCreds(function (getData) {
            if(getData){
                response.json(!getData)
            } else {
                if (request.params.name) {
                    response.json(s3.s3Credentials(s3Config, { filename: decodeFileName, contentType: decodeFileType }));
                } else {
                    response.status(400).send('filename is required');
                }
            }
        });

    }

    function getList(request, response) {
        getCreds(function (getData) {
            if(getData){
                response.json(getData);
            } else {
                s3bucket.listObjects(function (err, data) {
                    if (!data || err) {
                        response.json({ config: "Please check your Amazon S3 configuration." });
                    } else{
                        response.json(data.Contents);
                    }
                })
            }
        });
    }

    function getS3Url(request, response) {
        var version = request.params.getS3Version;
        var amazonID = request.params.getAmazonID;
        var decodedName = decodeURIComponent(request.params.getFileName);
        var params = {
            Key: decodedName,
            Expires: 600
        }
        
        getVersionCreds(version, amazonID, function () {
            var url = s3bucket.getSignedUrl('getObject', params);
            response.send(url);
        })
        
    }

    function getUploadedData(request, response) {
        var fileName = _.cloneDeep(request.params.getUploaded);
        getCreds(function () {
            s3bucket = new AWS.S3({
                params: {
                    Bucket: s3Config.bucket,
                    Prefix: fileName
                    // Prefix: 's/5469b2f5b4292d22522e84e0/ms.files/'
                },
                accessKeyId: s3Config.accessKey,
                secretAccessKey: s3Config.secretKey
            });
            s3bucket.listObjects(function (err, data) {
                var bucketList = data.Contents;
                response.json(bucketList);
            })
        });
    }

    apiRoutes.get('/s3_credentials/:name/:type', getConfigData);
    apiRoutes.get('/s3_BucketList/', getList);
    apiRoutes.get('/s3_BucketList/:getUploaded', getUploadedData);
    apiRoutes.get('/s3_BucketList/getUrl/:getFileName/:getS3Version/:getAmazonID', getS3Url);
    app.use('/s3', apiRoutes);
}