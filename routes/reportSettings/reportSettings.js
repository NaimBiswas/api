'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var db = require('../../lib/resources').db;
//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'joins',
    collection: 'joins',
    schema: schema,
    softSchema: softSchema
  },
  types:{
    POST:{
      ONE: {
        before : doAction
      }
    },
    PATCH:{
      ONE: {
        before : doAction
      }
    }
  }
});


function doAction(req, res, next){
  //   console.log('------------------------here in do action------------------------------');
  //   joinName = req.body.joinName;
  //   objectId = req.body.objId;
  //   joinArr = req.body.array;

  //  // var deferred = q.defer();
  //  console.log(objectId);
  //   if(objectId=='undefined'){ 
  //        db.collection('joins').insert({
  //       'joinName': joinName,
  //       'join': joinArr,
  //   }, function (err, result) {
  //       if (err) {
  //           deferred.reject(err)
  //           db.close()
  //       }
  
  //           deferred.resolve(result);

  //   });
  //      }
  //   else{
  //       db.collection('joins').update({ _id: ObjectId(objectId)}, {
  //       $set: {
  //           'joinName': joinName,
  //           'join': joinArr,
  //       }
  //   }, function () {
  //       deferred.resolve();
  //   })
  //   }
   
  //   return deferred.promise;
//   console.log('-----------------------------------------------');
//   console.log(req.params);
//    console.log(req.body);
//    console.log(req.query);
  
//   if(req.params._id && req.params._id.toString().substring(25) == 'remove'){
//     console.log('-------------------------');
//      console.log(req.params._id.toString().substring(25));
//     var id = req.params._id.toString().substring(0,24);
//       db.collection('joins').remove({ _id: db.ObjectID(id)},function (err, data) {
//        res.send(200, {data: 'success'});
// });
//   }
//   else{
  next();
//  }
}

module.exports = route.router;
