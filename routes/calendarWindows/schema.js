'use strict'
/**
 * @name calendarWindows-schema
 * @author Yamuna Sardhara <yamuna.s@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var id = types.id;
var rString = types.rString;
var string = types.string;
var rDate = types.rDate;
var array = types.array;

var schema = {
  schema: array(object({
    _id: rId.label('Schema id'),
    title: rString.label('Schema title')
  })).required().label('schema object'),
  title: rString.label('calendarWindows title'),
  type: rString.label('calendarWindows type'),
  testType: object({
    _id: id.label('Question Id'),
    title: string.label('Question Title'),
    's#': string.label('Question Sequence Number'),
    parentId: string.label('Question Parent Id - Procedure Sequence Id'),
  }).label('Test Type Object'),
  scheduledBy: rString.label('calendarWindows scheduled by user'),
  date: rDate.label('calendarWindows date'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
