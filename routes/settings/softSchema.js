'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');
var joi = require('joi');

var object = types.object.bind(types);
var string = types.string;
var bool = types.bool;


var schema = object({
  _id: joi.string().equal('default').label('Settings Id'),
  title: string.label('Settings title'),
  company: string.label('Company name'),
  product: string.label('Product name'),
  web: string.label('Web portal url'),
  support: string.label('Web portal url'),
  buildVersion: string.label('buildVersion'),
  truncateDecimal: bool.label('Truncate Decimal')
}).or('title','company','product','web');

module.exports = schema;
