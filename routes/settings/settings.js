'use strict'
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'settings',
    collection: 'settings',
    softSchema: softSchema
  },
  types: {
    GET: {},
    PUT: false,
    PATCH: {},
    POST: false
  }
});


module.exports = route.router;
