/*
author - lokesh.b - lokesh.b@productive.com
here we are bypasig this url for the advanced search
*/
;
(function () {
  'use strict';
  var express = require('express');
  var db = require('../../lib/db');
  var _ = require('lodash');
  var decode = require('../../lib/oauth/model').getAccessToken;
  var lzstring = require('lz-string');
  var logger = require('../../logger');
  var currentFileName = __filename;
  var advancedSearchCore = {
    app: require('./advancedSearchByAppCore.js'),
    entity: require('./advancedSearchByEntityCore.js')
  }; //require('./advancedSearchCore.js');

  // var advancedSearchByAppCore = require('./advancedSearchByAppCore.js');
  // var advancedSearchByEntityCore = require('./advancedSearchByEntityCore.js');

  /**
  * @swagger
  * /advancedsearch:
  *   post:
  *     tags: [Attribute]
  *     description: Filter app entries on behalf of master qc settings and apps
  *     produces:
  *       - application/json
  *     parameters:
  *        - name: apps
  *         description: App/masterqcsettings id`s
  *         in: body
  *         required: false
  *         type: array of string
  *       - name: includeInActiveRecords
  *         description: fetch inActive records
  *         in: body
  *         required: true(default false)
  *         type: boolean
  *       - name: pageNumber
  *         description: Page Number
  *         in: body
  *         required: false
  *         type: number
  *       - name: pageSize
  *         description: Page Size
  *         in: body
  *         required: false
  *         type: number
  *       - name: sort
  *         description: object to apply sorting default ({createdDate : -1})
  *         in: body
  *         required: false
  *         type: object
  *       - name: isForMasterAppOnly
  *         description: For masterqcsettings apps
  *         in: body
  *         required: false
  *         type: boolean
  *       - name: dates
  *         description: object to apply dates filter ({start : datestring, end: datestring})
  *         in: body
  *         required: false
  *         type: object
  *       - name: status
  *         description: Number Array of status (Fail: 0,Pass: 1,Pending: 2,void: 'info',Draft: 4) eg. [1,2,0]
  *         in: body
  *         required: false
  *         type: Array of Numbers
  *       - name: levelIds
  *         description: String Array of levelIds
  *         in: body
  *         required: false
  *         type: Array of String
  *       - name: batchEntryId
  *         description: Batch Entry string for searching
  *         in: body
  *         required: false
  *         type: string

  *     responses:
  *       200:
  *         description: Json Array Object
  */

  module.exports = function (app) {
    var apiRoutes = express.Router();

    apiRoutes.post('/', function (req, res, next) {
      executeAPIOperation(req, res, next, 'app');
    });

    apiRoutes.post('/entity', function (req, res, next) {
      executeAPIOperation(req, res, next, 'entity');
    });

    function executeAPIOperation (req, res, next, type) {

      logger.log('API Called : api/advancedsearch', 'info', currentFileName);

      var xToken = req.headers['x-authorization'] || req.query['x-authorization'];
      if (xToken) {
        logger.log('if(xToken) : Inside', 'info', currentFileName);
        db['allAuthToken'].findOne({
          token: xToken
        }, function (e, token) {
          if (e || !token) {

            logger.log('Collection: allAuthToken - Failed : ' + e, 'error', currentFileName);

            res.status(403).json({
              message: 'We regret to inform you that your session has been ended.'
            });
            return;
          }

          logger.log('Collection: allAuthToken- success', 'info', currentFileName);

          db['users'].findOne({
            _id: db.ObjectID(token.userId || token.user._id)
          }, function (err, user) {
            if (err) {
              logger.log('Collection:  call -failed : ' + err, 'error', currentFileName);
              res.status(403).json({
                message: 'You are not authorized.'
              });
              return;
            }

            logger.log('Collection:  users - success', 'info', currentFileName);
            req.user = user || {};

            if (req.user['applicableSites']) {
              logger.log("if (req.user['applicableSites']): Inside", 'info', currentFileName);
              var accessiableSites = req.user['applicableSites'];
              req.user['applicableSitesList'] = _.map(_.cloneDeep(req.user['applicableSites']), function (sites) {
                return _.map(sites, function(subSites){ return subSites._id});
              });
              req.user['applicableSites'] = _.map([].concat.apply([], accessiableSites), function (site) {
                return site._id;
              });
            }
            var rolesToValidate = _.map((user.roles || []), function (role) {
              return db.ObjectID(role._id);
            });
            db['roles'].find({
              _id: {
                '$in': rolesToValidate
              },
              modules: {
                $exists: true
              }
            }, {
              modules: 1
            }).toArray(function (err, roles) {

              logger.log('Collection: roles- success', 'info', currentFileName);
              var schema = [];
              _.forEach((roles || []), function (role) {
                _.forEach((role.modules || []), function (module) {
                  _.forEach((module.permissions || []), function (permission) {
                    if (permission.view && permission.entityType === 'schemas') {
                      schema.push(permission._id + '');
                      // schema.push(typeof permission._id == 'string'? db.ObjectID(permission._id):permission._id);
                    }
                  });
                });
              });

              req.user.authorizedSchemas = _.uniq(schema);
              renderPost(req, res, next, (type || 'app'));

            });

          });

        });
      } else if (req.headers['authorization'] || req.query['authorization']) {
        logger.log("else if (req.headers['authorization'] || req.query['authorization'])- Inside authorization brear token", 'info', currentFileName);
        var decompressToken = lzstring.decompressFromEncodedURIComponent(req.headers['authorization']);
        var token = decompressToken;
        if (token && _.startsWith(token, 'Bearer ')) {
          logger.log("if(token && _.startsWith(token,'Bearer ')) : Inside decompressed authorization brear token ", 'info', currentFileName);
          decode(token.substring(7), function (err, data) {
            if (err) {
              logger.log('Error: decoding authorization brear token failed : '+ err, 'error', currentFileName);
              res.status(403).json({
                message: 'You are not authorized.'
              });
              return;
            }
            logger.log('decode: Success - Inside decoded authorization brear token Info', 'info', currentFileName);
            req.user = data.user || {};
            renderPost(req, res, next, (type || 'app'));
          });
        } else {
          logger.log('else: authorization brear token decompression failed', 'info', currentFileName);
          res.status(403).json({
            message: 'You are not authorized.'
          });
        }
      } else {
        logger.log('else: No token found', 'info', currentFileName);
        res.status(403).json({
          message: 'We regret to inform you that your session has been ended.'
        });
      }
    }

    function renderPost (req, res, next, type) {

      logger.log('Function: RenderPost - Start', 'info', currentFileName);
      var applicableSites = advancedSearchCore[type || 'app'].getApplicableSites(req.user['applicableSites']);
      var options = req.body || {};
      options.requestFrom = 'api';
      options.user = req.user;
      options.clientOffSet = req.headers['client-offset'] || 0;
      options.clientTZ = req.headers['client-tz'] || 'America/Chicago';
      // PQT-1358 by Yamuna
      if (options.createdByName) {
        options.createdBy = typeof req.user._id == 'string' ? db.ObjectID(req.user._id) : req.user._id;
        delete options.createdByName;
      }
      options.permissions = req.user['permissions'] || [];

      if (options.levelIds && options.levelIds.length) {
        options.levelIds = _.filter((options.levelIds||[]), function(level){
          return !!level;
        });

        var siteLength = (req.user['applicableSitesList']||[]).length || req.user['applicableSites'].length;
        if(options.levelIds.length != siteLength){
          var length = siteLength-1;
          var objKey = {};
          if (req.user['applicableSitesList']) {
           objKey["levelIds."+length] = {'$in':req.user['applicableSitesList'][length]};
          } else{
           objKey["levelIds."+length] = {'$in':req.user['applicableSites'][length]};
          }
          options.customQuery = objKey;
        }

        logger.log('if (options.levelIds && options.levelIds.length): Inside levelIds filter', 'info', currentFileName);
        if (!advancedSearchCore[type || 'app'].applicableSitesContainsSites(applicableSites, options.levelIds)) {
          res.status(403).json({
            message: 'You are not authorized.'
          });
          return;
        }
      } else if (applicableSites && applicableSites.length) {
        options.applicableSites = applicableSites;
      }
      // req.permissionFromToken = data;

      logger.log('Function : fetchRecords: Before Calling ...', 'info', currentFileName);
      advancedSearchCore[type || 'app'].fetchRecords(options, function (err, response) {
        if (err) {
          logger.log('Function : fetchRecords- Error : failed : '+ err, 'error', currentFileName);
          res.status(400).json({
            message: err.message
          });
        }
        logger.log('Function : fetchRecords - success', 'info', currentFileName);
        res.status(200).json(response);
      });
    }

    app.use('/advancedsearch/', apiRoutes);
  }

})();
