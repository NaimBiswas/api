/**
 * 
 * @author Lokesh Boran and Jyotil Raval
 * @name advancedSearchByAppCore.js
 * @version 0.0.0 Made this API for sending flat data
 * @version 0.0.1 Export file related change
 * @version 0.0.2 Schema version wise data <surjeet.b@productivet.com>
 */

var db = require('../../lib/db');
var _ = require('lodash');
var moment = require('moment-timezone');
var q = require('q');
var logger = require('../../logger');
var currentFileName = __filename;
var batchEntriesService = require('../../lib/batchEntryService/index');

function applicableSitesContainsSites(applicableSites, sites) {
  logger.log("function: applicableSitesContainsSites - start", 'info', currentFileName);
  return sites.every(function (value) {
    return (applicableSites.indexOf(value) >= 0);
  });
}

function getApplicableSites(applicableSites) {
  logger.log("function: getApplicableSites - start", 'info', currentFileName);
  return [].concat.apply([], applicableSites);
}

function schemaAttributeFilter(attributes) {
  logger.log("function: schemaAttributeFilter - start", 'info', currentFileName);
  var filter = [];
  _.forEach((attributes || []), function (attribute) {
    if (!attribute.type) {
      filter.push({
        'attributes.s#': {
          '$in': attribute.keys
        }
      });
    } else if (attribute.type) {
      var keys = attribute.keys.map(function (key) {
        var entityQuestion = key.split('-') || [];
        return entityQuestion[1];
      });
      filter.push({
        'entities.s#': {
          '$in': keys
        }
      });
    }
  });
  return filter;
}

function fetchRecords(options, callback) {
  logger.log("function: fetchRecords - start", 'info', currentFileName);
  var filter = {};
  if (options && options.apps && options.apps.length) {
    filter['_id'] = {
      '$in': options.apps.map(function (app) {
        return db.ObjectID(app);
      })
    };
  }
  if (options.attributes && options.attributes.length) {
    filter['$and'] = schemaAttributeFilter(options.attributes);
    if (filter['$and'] && filter['$and'].length) {
      delete filter['$and'];
    }
  }
  if (!options.includeInActiveRecords) {
    filter.isActive = true;
  }
  try {
    var entryFilter = createFilters(options);
    // if(options.isReviewPending){
    //   entryFilter.isReviewPending = options.isReviewPending;
    // }
    fetchAllUsers(function (err, users) {
      if (err) {
        callback(err);
      } else {
        entryFilter.user = options.user;
        entryFilter.users = users;
        if(options.accessedEntryIdObj){
          entryFilter.accessedEntryIdObj = options.accessedEntryIdObj;
        }
        // PQT-1358 by Yamuna
        if(options.createdBy){
          entryFilter.createdBy = options.createdBy;
          entryFilter.query.createdBy = options.createdBy;
        }
        //PQT-4385 Start Hiren
        if(options.createdByName){
          entryFilter.createdByName =options.createdByName;
        }//PQT-4385 end
        if (options.isForMasterAppOnly && options.isForMasterAppOnly == 'masterapps') {
          getMasterSettingApps(filter, entryFilter, function (err, result) {
            if (err) {
              callback(err);
            }
            if (result) {
              callback(null, result);
            }
          });
        } else {
          getAllAppData(filter, entryFilter, function (err, result) {
            if (err) {
              callback(err);
            }
            if (result) {
              callback(null, result);
            }
          });
        }
      }
    });
  } catch (e) {
    logger.log("function: fetchRecords - catch : " + e, 'error', currentFileName);
    callback(e);
  }
}

function getMasterSettingApps(masterAppFilter, entryFilters, callback) {
  logger.log("function: getMasterSettingApps - start", 'info', currentFileName);
  var filteredMasterAppEntryList = [];
  var columnFilter = {
    'title': 1,
    'schema': 1,
    'isActive': 1,
    'module': 1
  };
  db.masterqcsettings.find(masterAppFilter, columnFilter, function (err, schema) {
    logger.log("collection:users before fetch", 'info', currentFileName);

    function processItem(err, item) {
      if (err) {
        callback(err);
      }
      if (item === null) {
        callback(null, filteredMasterAppEntryList);
        return; // All done!
      } else {
        var filter = {};
        if (item && item.schema && item.schema.length) {
          if (entryFilters.user && entryFilters.user.authorizedSchemas && entryFilters.user.authorizedSchemas.length) {

            item.schema = _.filter((item.schema || []), function (sch) {
              return _.some((entryFilters.user.authorizedSchemas), function (asch) {
                return asch == sch._id;
              });
            });
          }
          var schemaIds = item.schema.map(function (app) {
            return db.ObjectID(app._id);
          });

          filter['_id'] = {
            '$in': schemaIds
          };
        }
        if (masterAppFilter.isActive) {
          filter.isActive = true;
        }
        getAllAppData(filter, entryFilters, function (error, result) {
          if (error) {
            callback(error);
            return;
          } else {
            item.apps = result;
            filteredMasterAppEntryList.push(item);
          }
          schema.nextObject(processItem);
        }, item._id.toString());
      }
    }
    schema.nextObject(processItem);
  });
}

function makeAttributeEntityFilter(options, schema){
  if (typeof options['attributes'] === 'object' && options['attributes'].length && schema) {
    options.query['$and'] = [];
    var attributesToFilter = options['attributes'].filter(function (a) {
      return a.isEntity == false;
    });
    var entitiesToFilter = options['attributes'].filter(function (a) {
      return a.isEntity == true;
    });
    if (entitiesToFilter.length) {
      var filteredEntities = generateEntitiesFilterSequenceWiseScript((schema.entities || []), entitiesToFilter);
      if (filteredEntities && filteredEntities.length) {
        filteredEntities.forEach(function (ent) {
          options.query['$and'].push(ent);
        });
      }
    }
    if (attributesToFilter.length) {
      var filteredAttributes = generateAttributeFilterSequenceWiseScript((schema.attributes || []), attributesToFilter);
      if (filteredAttributes && filteredAttributes.length) {
        filteredAttributes.forEach(function (attrib) {
          options.query['$and'].push(attrib);
        });
      }
    }
    if (!options.query['$and'].length) {
      delete options.query['$and'];
    }
  }
}

function getEntryQuery(options){
  if(options.isReviewPending){
    options.query["workflowreview"]= {
      "$elemMatch": {
        "$or": [
          {
            "userSelectionType": "0",
            "roles": {
              "$in": options.isReviewPending.roles
            }
          },
          {
            "userSelectionType": {
              "$ne": "0"
            },
            "user": {
              "$elemMatch": {
                "id" : {"$in":[options.isReviewPending.userId.toString(),options.isReviewPending.userId]}
              }
            }
          }
        ]
      }
    };

  if(options.entryId){
    options.query['_id'] = db.ObjectID(options.entryId);
  }
  }
  return options.query;
}

function getAllAppData(schemaFilter, options, callback, parentSchemaId) {
  logger.log("function: getAllAppData - start", 'info', currentFileName);
  var filteredAppEntryList = [];
  var columnFilter = {
    'title': 1,
    'attributes': 1,
    'isActive': 1,
    'module': 1,
    'description': 1,
    'createdBy': 1,
    'modifiedBy': 1,
    'createdByName': 1,
    'modifiedByName': 1,
    'createdDate': 1,
    'modifiedDate': 1,
    'version': 1,
    'entities': 1,
    'masterQCSettings': 1,
    'keyvalue': 1,
    'fromMail': 1
  };
  db.schema.find(schemaFilter, columnFilter, function (err, schema) {
    logger.log("collection:users before fetch", 'info', currentFileName);

    function processItem(err, item) {
      if (err) {
        callback(err);
      }
      if (item === null) {
        callback(null, filteredAppEntryList);
        return;
      } else {
        makeAttributeEntityFilter(options, item);
        // getEntryQuery(options);
        var collectionName = (item._id + '');
        if (options.appFilter['masterQCSettings'] && options.masterApp) {
          options.query['masterQCSettings'] = options.appFilter['masterQCSettings'];
        }
        if(options.createdByName){
          options.query['createdByName'] = options.createdByName;
        }
        //--By MIRAL - 7769
        /**
         * #options.query is query which we will use to fire in respective collection
         * #parentSchemaId comes from argument of this function
         *
         * we are cloning #options.query in #optionsQueryClone so it can not reflect other code
         * hear we are replacing "masterQCSettings.masterQCSettingsId" of #optionsQueryClone
         * because when we want count then we should search with actual parent of child app rather tnan searched parent of masterapp
         */
        var optionsQueryClone = _.cloneDeep(options.query);
        if (parentSchemaId) {
          optionsQueryClone["masterQCSettings.masterQCSettingsId"] = parentSchemaId;
        }
        //START :: PQT-1266 by Yamuna
        if (options.accessedEntryIdObj && options.accessedEntryIdObj[item._id]) {
          optionsQueryClone['_id'] = {
            '$in': options.accessedEntryIdObj[item._id].map(function (entry) {
              return db.ObjectID(entry);
            })
          };
        }   
        
        //END :: PQT-1266 by Yamuna
        db.collection(collectionName).count(optionsQueryClone, function (fail, count) {
          // -|| changed above by miral
          // db.collection(collectionName).count(options.query, function(fail, count) {
          logger.log("collection:users before fetch", 'info', currentFileName);
          if (fail) {
            callback(fail);
            return;
          } else {
            item.totalFilteredRecords = count;
            //--By MIRAL - 7769
            /**
             * hear we will talk about only below condition
             * #parentSchemaId comes from argument of this function
             * if #parentSchemaId is available then we will think that flow comes from Master app not from APP
             * and we will check if current master id is equal to expeceted master id whose record we want
             *
             * if #parentSchemaId is not available then we will compare current app id with expected id of first element of options.app array
             *
             * in the case of options.isForExport we will want all record because we need it for export
             */
            if (count > 0 &&
              ((parentSchemaId && options.masterApp && options.masterApp.length && !!_.find(options.masterApp, function (v) {
                  return v == parentSchemaId;
                })) ||
                (!parentSchemaId && options.apps[0] == item._id.toString()) ||
                options.isForExport)) {
              //START :: PQT-1266 by Yamuna
              if (!_.isEmpty(options.accessedEntryIdObj) && options.accessedEntryIdObj[item._id] && options.accessedEntryIdObj[item._id].length) {
                options.query['_id'] = {
                  '$in': (options.accessedEntryIdObj[item._id] || []).map(function (entry) {
                    return db.ObjectID(entry);
                  })
                };
              }
             //END :: PQT-1266 by Yamuna
              // -|| exchange above and below By MIRAL
              // if (count>0) {
              // if (count>0 && ((options.apps[0]== item._id) || options.isForExport)) {
              getFilteredAppRecords(options, collectionName, function (error, result) {
                if (error) {
                  callback(error);
                  return;
                } else {
                  getAllSchemaVersion(item._id, columnFilter, options, function(err, versionWiseSchema){
                  item.entries = [];                
                  fetchFormBatchEntry(result, item, options, function () {
                    if (!options.isForExport) {
                      _.forEach((result || []), function (entry) {
                        var passVerionsWiseSchema = versionWiseSchema[entry.parentVersion];
                        var projectedEntriesData = projectedListForAdvancedSearch(passVerionsWiseSchema || item, entry, options);
                        item.entries.push(projectedEntriesData.projectedEntry);                                                
                      });
                      delete item.attributes;
                      delete item.entities;
                      if (item.entries && item.entries.length) {
                        item.headerTitles = getHeaderTitles(versionWiseSchema);
                      }
                    } else {
                      item.entries = result;
                    }
                    filteredAppEntryList[_.findIndex(schemaFilter._id['$in'], item._id)] = item;
                    schema.nextObject(processItem);
                  });
                  });                  
                }
              });
            } else {
              filteredAppEntryList[_.findIndex(schemaFilter._id['$in'], item._id)] = item;
              schema.nextObject(processItem);
            }
          }
        });
      }
    }
    schema.nextObject(processItem);
  });
}

function getHeaderTitles(versionWiseAllSchema) {
  var attributes = {};
  var entities = {};

  _.forIn(versionWiseAllSchema, function (schema) {
    (schema.attributes || []).forEach(function (attribute) {
      attributes[attribute.title] = {
        title: attribute.title,
        seq: 'entries.' + attribute['s#'] + '.attributes'
      };
    });

    (schema.entities || []).forEach(function (entity) {
      entities[entity.title] = {
        title: entity.title,
        seq: 'entries.' + entity.questions[0]['s#'] + '-' + entity['s#'] + '.' + entity['s#']
      };
    });
  });

  return [].concat(_.sortBy(_.values(attributes), 'title'), _.sortBy(_.values(entities), 'title'));
}

/**
 * 
 * @author Surjeet Bhadauriya <surjeet.b@productivet.com>
 * @param {string} itemId 
 * @param {object} columnFilter 
 * @param {function} cb 
 * @desc PQT-171 - Get schema data for all entry version
 * @returns object
 */
function getAllSchemaVersion(itemId, columnFilter, options, cb) {
  if (options && options.query && options.query.hasOwnProperty('fromToken')) {
    return cb(null, {});
  }
  db.schemaAuditLogs
    .find({ identity: itemId }, columnFilter)
    .toArray(function(err, data) {
      if (err) {
        return cb(err, null);
      }
      //grouping it version wise.. will return an object which contains array with version wise schema..
      data = data.reduce(function(obj, value){
        obj[value.version] = value;
        return obj;
      }, {});
      
      cb(null, data);      
    });

}

function fetchAllUsers(cb) {
  logger.log("function: fetchAllUsers - start", 'info', currentFileName);
  try {
    db.collection('users').find({}, {
        'username': 1,
        'firstname': 1,
        'lastname': 1,
        '_id': 1
      })
      .toArray(function (err, result) {
        if (err) {
          cb(err);
        } else {
          cb(null, result);
        }
      });
  } catch (e) {
    cb(e);
  }
}

function projectedListForAdvancedSearch(schema, entry, options) {
  logger.log("function: projectedListForAdvancedSearch - start", 'info', currentFileName);
  var projectedEntry = {
    "_id": entry._id,
    "App": schema.title
  };

  if (options.query.fromToken) {
    projectedEntry['Email'] = {
      's#': 'fromMail',
      value: (entry['fromMail'] || '')
    };
  }

  _.forEach((schema.attributes || []), function (attr) {
    if (!entry.entries[attr['s#']]) {
      //projectedEntry[attr.title] = '';
      //Hot Fix - Surjeet - QC3-11340
      projectedEntry[attr.title] = {
        's#' : 'entries.'+attr['s#']+'.attributes',
        value : ''
      };
      return;
    } else if (!_.isUndefined(attr)) {

      projectedEntry[attr.title] = displayValueByFormat(attr, entry.entries[attr['s#']]['attributes'], options.clientTZ) || '';

      if (typeof projectedEntry[attr.title] === 'object' && projectedEntry[attr.title].length) {
        projectedEntry[attr.title] = projectedEntry[attr.title].join(', ');
      }
    } else {
      projectedEntry[attr.title] = !_.isUndefined(entry.entries[attr['s#']]) ? entry.entries[attr['s#']]['attributes'] : '';
      if (typeof projectedEntry[attr.title] === 'object' && projectedEntry[attr.title].length) {
        projectedEntry[attr.title] = projectedEntry[attr.title].join(', ');
      }
    }

    projectedEntry[attr.title] = {
      's#': 'entries.' + attr['s#'] + '.attributes',
      value: projectedEntry[attr.title]
    };
  });
  _.forEach((schema.entities || []), function (ent) {
    if (!entry.entries[ent.questions[0]['s#'] + '-' + ent['s#']]) {
      // projectedEntry[ent.title] = '';
        //Hot Fix - Surjeet - QC3-11340
        projectedEntry[ent.title] = {
            's#' : 'entries.'+ent.questions[0]['s#']+'-'+ent['s#']+'.'+ent['s#'],
            value : ''
        };
      return;
    } else if (!_.isUndefined(ent.questions[0])) { //!_.isUndefined(ent.questions[0].type) && !_.isUndefined(ent.questions[0].type.format) && ent.questions[0].type.format.title == 'Date'
      projectedEntry[ent.title] = displayValueByFormat(ent.questions[0], entry.entries[ent.questions[0]['s#'] + '-' + ent['s#']][ent['s#']], options.clientTZ) || '';
      if (typeof projectedEntry[ent.title] === 'object' && projectedEntry[ent.title].length) {
        projectedEntry[ent.title] = projectedEntry[ent.title].join(', ');
      }
    } else {
      projectedEntry[ent.title] = !_.isUndefined(entry.entries[ent.questions[0]['s#'] + '-' + ent['s#']]) ? entry.entries[ent.questions[0]['s#'] + '-' + ent['s#']][ent['s#']] : '';
      if (typeof projectedEntry[ent.title] === 'object' && projectedEntry[ent.title].length) {
        projectedEntry[ent.title] = projectedEntry[ent.title].join(', ');
      }
    }

    projectedEntry[ent.title] = {
      's#': 'entries.' + ent.questions[0]['s#'] + '-' + ent['s#'] + '.' + ent['s#'],
      value: projectedEntry[ent.title]
    };
  });

  projectedEntry["Created Date"] = moment.utc(new Date(entry.createdDate)).tz(options.clientTZ).format('MM/DD/YYYY HH:mm:ss');
  projectedEntry["Modified Date"] = moment.utc(new Date(entry.modifiedDate)).tz(options.clientTZ).format('MM/DD/YYYY HH:mm:ss');

  if (!entry.modifiedByName && entry.modifiedBy && options && options.users && options.users.length) {
    var userDetails = _.find((options.users || []), function (user) {
      return user._id.toString() == entry.modifiedBy.toString();
    });
    if (userDetails) {
      entry.modifiedByName = (userDetails.firstname || '').concat(' ').concat(userDetails.lastname || '').concat(' (').concat(userDetails.username || 'N/A').concat(')');
    }
  }

  projectedEntry["Modified By"] = entry.modifiedByName;

  //by surjeet.b@productivet.com.../ for improvement no QC3-1639
  if (entry && entry.assignment && entry.assignment.assignee && entry.assignment.assignee.length) {
    projectedEntry["Assignee"] = entry.assignment.assignee.map(function (user) {
      return '' + user.username;
    }).join(', ');
    //PQT-96 by Yamuna
    projectedEntry["AssigneeHover"] = entry.assignment.assignee.map(function (user) {
      return user.firstname + ' ' + user.lastname + ' ('+ user.username + ')';
    }).join(', ');
  } else {
    projectedEntry["Assignee"] = 'N/A';
  }

  projectedEntry["Record Status"] = getStausString(entry);
  projectedEntry["Rejected"] = entry.isRejected;
  projectedEntry["masterQCSettings"] = entry['masterQCSettings'];
  //QC3-8959 Prachi - Backlog: Implement mechanism for storing app record version which can be utilized for Activity information report
  if (entry['version']) {
    projectedEntry["version"] = entry['version'];
  }

  if (entry['BatchEntry']) {
    projectedEntry["BatchEntry"] = entry['BatchEntry'];
  }
  if (!options.query.fromToken) {
    projectedEntry["Batch Entry ID"] = entry['BatchEntry'] ? entry['BatchEntry'].title : 'N/A';
  }

  return {
    'projectedEntry': projectedEntry
  };
}

function fetchFormBatchEntry(entries, schema, options, cb) {
  logger.log("function: fetchFormBatchEntry - start", 'info', currentFileName);
  try {
    var schemaId = schema._id.toString();
    var query = {};
    if (options.isForMasterAppOnly == 'apps') {
      query = {
        'schemaId': {
          '$in': [schemaId]
        }
      };
      (entries || []).forEach(function (entry) {
        if (!query['schemaId']['$in']) {
          query['schemaId']['$in'] = [];
        }
        if (entry.masterQCSettings) {
          query['schemaId']['$in'].push(entry.masterQCSettings.masterQCSettingsId);
        }
      });
    } else {
      if (options && options.masterApp && options.masterApp.length) {
        schemaId = options.masterApp[0];
        query = {
          'schemaId': schemaId
        };
      }
    }

    batchEntriesService.getBatchEntries(query['schemaId'])
      .then(function (result) {
        (result || []).forEach(function (batchEntry) {
          _.forIn((batchEntry.batchEntries || {}), function (value, key) {
            (value || []).forEach(function (item) {
              entries.forEach(function (ent) {
                if (item.formId === ent._id.toString()) {
                  ent['BatchEntry'] = {
                    _id: batchEntry._id,
                    title: key
                  };
                  return;
                }
              });
            });
          });
        });
        cb();
      });
  } catch (e) {
    logger.log("function: fetchFormBatchEntry - catch : " + e, 'error', currentFileName);
    cb();
  }
}

function displayValueByFormat(c, value, clientTZ) {
  logger.log("function: displayValueByFormat - start", 'info', currentFileName);
  var filterdDate = '';
  if (c && c.type && c.type.format && c.type.format.title === "Date" && value) { //If value is there than only convert //QC3-9355 Prachi
    if (c.type.format.metadata.format === '' || !c.type.format.metadata.format || _.isUndefined(c.type.format.metadata.format)) {
      return moment.utc(new Date(value)).tz(clientTZ).format('MM/DD/YYYY');
    } else {
      switch (c.type.format.metadata.format) {
        case 'dd/MM/yyyy':
          filterdDate = 'DD/MM/YYYY';
          break;

        case 'MM/dd/yyyy':
          filterdDate = 'MM/DD/YYYY';
          break;

        case 'yyyy/MM/dd':
          filterdDate = 'YYYY/MM/DD';
          break;

        case 'MM/dd/yy':
          filterdDate = 'MM/DD/YY';
          break;

        case 'dd/MMM/yy':
          filterdDate = 'DD/MMM/YY';
          break;

        case 'MONTH/dd/yyyy':
          filterdDate = 'MMMM/DD/YYYY';
          break;

        case 'dd/MMM/yyyy':
          filterdDate = 'DD/MMM/YYYY';
          break;

        case 'MMMM/dd/yyyy':
          filterdDate = 'MMMM/DD/YYYY';
          break;

        default:
          filterdDate = 'MM/DD/YYYY';
      }
      return moment.utc(new Date(value)).tz(clientTZ).format(filterdDate);

    }
  } else if (!_.isUndefined(c) && !_.isUndefined(c.type) && !_.isUndefined(c.type.format) && c.type.format.title === "Time") {
    return moment.utc(new Date(value)).tz(clientTZ).format('HH:mm a');

  } else {
    return value === 0 ? '0' : value; //QC3-8373: mahammad :- return '0' instead of return false;
  }
}

function getStausString(app) {
  logger.log("function: getStausString - start", 'info', currentFileName);
  if (!(app.entries && app.entries.status)) {
    return {
      's#': 'statusString', // QC3-7197 - Jyotil
      'value': 'Draft'
    };
  }
  var status = app.entries.status.status;
  var hasPendingWorkflowReview = false;
  if (app.workflowreview && app.workflowreview.length) {
    hasPendingWorkflowReview = _.some((app.workflowreview || []), function (review) {
      return review.reviewstatus == 1;
    });
  }
  if (status == 3) {
    return {
      's#': 'statusString', // QC3-7197 - Jyotil
      'value': 'Void'
    };
  } else if (status == 4) {
    return {
      's#': 'statusString', // QC3-7197 - Jyotil
      'value': 'Draft'
    };
  } else if (hasPendingWorkflowReview && status == 1) {
    return {
      's#': 'statusString', // QC3-7197 - Jyotil
      'value': 'Review Pending - Passed'
    };
  } else if (hasPendingWorkflowReview && status == 0) {
    return {
      's#': 'statusString', // QC3-7197 - Jyotil
      'value': 'Review Pending - Failed'
    };
  } else if (status == 1) {
    return {
      's#': 'statusString', // QC3-7197 - Jyotil
      'value': 'Passed'
    };
  } else {
    return {
      's#': 'statusString', // QC3-7197 - Jyotil
      'value': 'Failed'
    };
  }
}

function createFilters(filters) {
  logger.log("function: createFilters - start", 'info', currentFileName);
  var pageSize = parseInt(filters.pageSize || 10);
  var pageNumber = parseInt(filters.pageNumber || 1);
  // if(!filters.clientTZ){
  //   filters.clientTZ= 'America/Chicago';
  // }
  var options = {
    pageSize: pageSize,
    pageNumber: pageNumber,
    skip: pageSize * (pageNumber - 1),
    appFilter: {},
    apps: filters.apps, //--By MIRAL - 7769
    limit: pageSize,
    isForExport: filters.isForExport || false,
    clientOffSet: filters.clientOffSet,
    clientTZ: filters.clientTZ,
    sort: filters.sort || {
      'createdDate': -1
    },
    query: {},
    isForMasterAppOnly: filters.isForMasterAppOnly,
    attributes: filters.attributes || []
  };
  if (filters.sort) {
    options.sort = filters.sort;
  }
  if (filters.dates && (filters.dates.start || filters.dates.end)) {
    options.query['createdDate'] = generateDateFilterScript(filters.dates.start, filters.dates.end);
  }
  if (filters.status && filters.status.length) {
    options.query['$or'] = generateStatusFilter(filters['status']);
  }
  if (filters.customQuery) {
    options.query = Object.assign(options.query, filters.customQuery);
  }
  if (filters.batchEntryId) {
    options.query['batchEntryId'] = parse('string', filters.batchEntryId);
  }
  if (filters.levelIds && filters.levelIds.length) {
    options.query['levelIds'] = {
      $all: filters.levelIds
    };
  } else if (filters.applicableSites && filters.applicableSites.length) {
    options.query['$where'] = JSON.stringify(filters.applicableSites).replace(/\s/g, '').concat('.indexOf(this.levelIds[this.levelIds.length - 1])>=0');
    options.applicableSitesOfWhere = filters.applicableSites;
  }
  if (filters.isForMasterAppOnly == 'masterapps') {
    options.query['masterQCSettings'] = {
      $exists: true
    };
  } else if (filters.isForMasterAppOnly == 'singleapps') {
    options.query['masterQCSettings'] = {
      $exists: false
    };
  }
  //START:: PQT - 20 - Yamuna
  if(filters.isRejected){
    options.query['$or'] = options.query['$or'] || [];
    options.query['$or'].push({'isRejected' : filters.isRejected});
  }
  //END:: PQT - 20 - Yamuna
  if (filters.hasMasterAppEntry && filters.hasMasterAppEntry.length) {
    options.appFilter['masterQCSettings'] = {
      $exists: true
    };
    options.query['masterQCSettings.masterQCSettingsId'] = filters.hasMasterAppEntry[0];
    options.masterApp = filters.hasMasterAppEntry;
  }
  if (filters.assignee && filters.assignee.length) {
    options.query['assignment.assignee.id'] = {
      '$in' : filters.assignee
    };
  }
  if (filters.token) {
    options.query['fromToken'] = filters.token;
  }
  return options;
}

function getAggrigateQuery(options) {
  var optionsQueryCopy = _.cloneDeep(options.query);
  delete optionsQueryCopy["$where"];

  var agrigateQuery = [];

  agrigateQuery.push({
    "$match": optionsQueryCopy
  })
  agrigateQuery.push({
    "$addFields": {
      // entries: 1,
      // workflowreview : 1,
      lastLevelId: {
        $arrayElemAt: ["$levelIds", -1]
      }
    }
  });

  agrigateQuery.push({
    "$match": {
      lastLevelId: {
        "$in": options.applicableSitesOfWhere
      }
    }
  });
  agrigateQuery.push({
    "$addFields": {
      // entries:1,
      workflowReviewPenfing: {
        $filter: {
          input: "$workflowreview",
          as: "item",
          cond: {
            $eq: ["$$item.reviewstatus", 1]
          }
        }
      }
    }
  });
  agrigateQuery.push({
    "$addFields": {
      statusString: {
        $switch: {
          branches: [{
              case: {
                $and: [{
                    $not: {
                      $eq: ["$workflowReviewPenfing", null]
                    }
                  },
                  {
                    $eq: [{
                      $size: "$workflowReviewPenfing"
                    }, 1]
                  },
                  {
                    $eq: ["$entries.status.status", 1]
                  }
                ]
              },
              then: "Review Pending - Passed"
            },
            {
              case: {
                $and: [{
                    $not: {
                      $eq: ["$workflowReviewPenfing", null]
                    }
                  },
                  {
                    $eq: [{
                      $size: "$workflowReviewPenfing"
                    }, 1]
                  },
                  {
                    $eq: ["$entries.status.status", 0]
                  }
                ]
              },
              then: "Review Pending - Failed"
            },
            {
              case: {
                $eq: ["$entries.status.status", 0]
              },
              then: "Failed"
            },
            {
              case: {
                $eq: ["$entries.status.status", 1]
              },
              then: "Passed"
            },
            {
              case: {
                $eq: ["$entries.status.status", 2]
              },
              then: "Review Pending"
            },
            {
              case: {
                $eq: ["$entries.status.status", 3]
              },
              then: "Void"
            },
            {
              case: {
                $eq: ["$entries.status.status", 4]
              },
              then: "Draft"
            }
          ],
          default: "No Status"
        }
      }
    }
  });

  agrigateQuery.push({
    $sort: options.sort
  })
  agrigateQuery.push({
    $skip: options.skip
  })
  agrigateQuery.push({
    $limit: options.limit
  })


  return agrigateQuery;
}


function getFilteredAppRecords(options, collectionName, callback) {
  logger.log("function: getFilteredAppRecords - start", 'info', currentFileName);
  try {
    if (options.appFilter['masterQCSettings'] && !options.query['masterQCSettings']) {
      options.query['masterQCSettings'] = options.appFilter['masterQCSettings'];
    }
    
    if (options.sort.statusString) {
      db.collection(collectionName)
        .aggregateAsync(getAggrigateQuery(options))
        .then(function (response) {

          callback(null, response);

          // if (err) {
          //   callback(err);
          // } else {
          //   callback(null, response);
          // }
        });
    } else {
      db.collection(collectionName)
        .find(options.query, {
          'fromToken': 0
        })
        .sort(options.sort)
        .skip(options.skip)
        .limit(options.limit)
        .toArray(function (err, response) {
          if (err) {
            callback(err);
          } else {
            callback(null, response);
          }
        });
    }




  } catch (exception) {
    logger.log("function: getFilteredAppRecords - catch : " + exception, 'error', currentFileName);
    callback(exception);
  }
}

function generateStatusFilter(statuses) {
  logger.log("function: generateStatusFilter - start", 'info', currentFileName);
  if (!statuses || !statuses.length) {
    return;
  }
  var query = [];
  _.forEach(statuses, function (status) {
    if (status == 3 || status === 4) {
      query.push({
        'entries.status.status': status
      });
    } else if (status === 2) {
      query.push({
        'workflowreview.reviewstatus': {
          "$in": [1]
        },
        'entries.status.status': {
          "$nin": [3, 4]
        }
      });
    } else if (status === 1 || status === 0) {
      var subquery = {
        '$and': []
      };
      subquery['$and'].push({
        'workflowreview.reviewstatus': {
          $ne: 1
        }
      });
      subquery['$and'].push({
        'entries.status.status': status
      });
      query.push(subquery);
    }
  });
  if (query.length) {
    return query;
  } else {
    return {};
  }
}

function generateDateFilterScript(start, end) {
  logger.log("function: generateDateFilterScript - start", 'info', currentFileName);
  if (!start && !end) {
    return;
  }
  if (start && end) {
    return {
      '$gte': parse('date', start),
      "$lte": parse('date', end)
    };
  } else if (start) {
    return {
      '$gte': parse('date', start)
    };
  } else {
    return {
      '$lte': parse('date', end)
    }; //will execute if there is only end defined
  }
}

function generateEntitiesFilterSequenceWiseScript(entities, filters) {
  logger.log("function: generateEntitiesFilterSequenceWiseScript - start", 'info', currentFileName);
  var entitiesArray = (entities.filter(function (entity) {
    entity.questions = _.filter(entity.questions, function (que) {
      return que.isKeyValue
    }) || [];
    return entity.questions.length > 0
  }) || []).map(function (entity) {
    return {
      'question': entity.questions[0],
      's#': entity['s#'],
      'combineds#': entity.questions[0]['s#'] + "-" + entity['s#']
    };
  });
  var entitiesList = filters;
  var filteredEntities = [];
  var groupedEntities = _.groupBy(entitiesList, 'keys') || {};
  _.forIn(groupedEntities, function (group) {
    var matchedList = {
      '$or': []
    };
    group.forEach(function (filter) {
      _.map(entitiesArray, function (entity) {
        var exists = _.some(filter.keys, function (key) {
          return key == entity['combineds#'];
        });
        if (exists) {
          var parsedValue = parse(entity.question.type.format.title, filter.value);
          var entitiesKey = 'entries.' + entity['combineds#'] + '.' + entity['s#'];
          var returningRelationship = {};
          returningRelationship[entitiesKey] = parsedValue;
          matchedList['$or'].push(returningRelationship);
          if (filter.value && new Date(filter.value) != 'Invalid Date' && isNaN(Number(filter.value))) {
            var dateFilter = {};
            dateFilter[entitiesKey] = filter.value;
            matchedList['$or'].push(dateFilter);
            matchedList['$or'].push({
              [entitiesKey]: (new Date(filter.value)).toISOString()
            }); //mahammad
          } else if (typeof filter.value !== "number" && parseFloat(filter.value)) {
            var numberFilter = {};
            numberFilter[entitiesKey] = parseFloat(filter.value);
            matchedList['$or'].push(numberFilter);
          } else {
            var stringFilter = {};
            stringFilter[entitiesKey] = filter.value.toString();
            matchedList['$or'].push(stringFilter);
          }
        }
      });
    });
    if (matchedList['$or'].length) {
      filteredEntities.push(matchedList);
    }
  });
  return filteredEntities;
}

function generateAttributeFilterSequenceWiseScript(attributes, filters) {
  logger.log("function: generateAttributeFilterSequenceWiseScript - start", 'info', currentFileName);
  var attributesList = filters;
  var filteredAttributes = [];
  var groupedAttributes = _.groupBy(attributesList, 'keys') || {};
  _.forIn(groupedAttributes, function (group) {
    var matchedList = {
      '$or': []
    };
    group.forEach(function (filter) {
      _.map(attributes, function (attribute) {
        var exists = _.some(filter.keys, function (key) {
          return key == attribute['s#'];
        });
        if (exists) {
          var parsedValue = parse(attribute.type.format.title, filter.value);
          var attributesKey = 'entries.' + attribute['s#'] + '.attributes';
          var returningRelationship = {};
          returningRelationship[attributesKey] = parsedValue;
          matchedList['$or'].push(returningRelationship);
          if (filter.value && new Date(filter.value) != 'Invalid Date' && isNaN(Number(filter.value))) {
            // if ((typeof filter.value=='object' || (typeof filter.value == 'string' && filter.value.length>0 && filter.value.lastIndexOf('Z')==filter.value.length-1)) && new Date(filter.value)) {
            var dateFilter = {};
            dateFilter[attributesKey] = filter.value;
            matchedList['$or'].push(dateFilter);
            matchedList['$or'].push({
              [attributesKey]: (new Date(filter.value)).toISOString()
            }); //mahammad
          } else if (typeof filter.value !== "number" && parseFloat(filter.value)) {
            var numberFilter = {};
            numberFilter[attributesKey] = parseFloat(filter.value);
            matchedList['$or'].push(numberFilter);
          } else {
            var stringFilter = {};
            stringFilter[attributesKey] = filter.value.toString(); //mahammad - entitiesKey to attributesKey
            matchedList['$or'].push(stringFilter);
          }
        }
      });
    });
    if (matchedList['$or'].length) {
      filteredAttributes.push(matchedList);
    }
  });

  return filteredAttributes;
}

function generateAttributeFilterScript(attributes, relation) {
  logger.log("function: generateAttributeFilterScript - start", 'info', currentFileName);
  var filters = [];
  relation = relation || '$or'
  var groupedAttributes = _.mapValues(_.groupBy(attributes, 'key'),
    clist => clist.map(attribute => _.omit(attribute, 'key'))) || {};
  _.forIn(groupedAttributes, function (attribute, key) {
    var tempAttribute = {};
    tempAttribute[relation] = (_.map(groupedAttributes[key] || [], function (object) {
      var originalValue = object['value'];
      var parsedValue = parse(object['type'], originalValue);
      var operator = object['operator'];
      var returningRelationship = {};
      var attributesKey = 'entries.' + key + '.attributes';
      if (operator) {
        var internalQuery = {};
        internalQuery[operator] = parsedValue;
        returningRelationship[attributesKey] = internalQuery;
      } else {
        returningRelationship[attributesKey] = originalValue;
      }
      return returningRelationship;
    }) || []);
    filters.push(tempAttribute);
  });
  return filters;
}

function parse(type, value) {
  logger.log("function: parse - start", 'info', currentFileName);
  var valArray = [];
  var keyArray = [];
  type = (type || '').toLowerCase();
  switch (type) {
    case 'date':
      return new Date(value);
    case 'regex':
      return {
        $regex: new RegExp(value.toLowerCase(), 'i')
      };
    case 'number':
    case 'exponential':
    case 'percentage':
    case 'currency':
    case 'scientific':
      if (/^[0-9\-\+]*$/.test(value) == true) {
        value = parseInt(value);
      }
      if (/^[0-9 \.\-\+]*$/.test(value) == true) {
        value = parseFloat(value);
      }
      return value;
    case 'array':
      keyArray = [];
      if (typeof value == 'string' && value.length) {
        valArray = value.split(',');
        _.forEach(valArray, function (chkvalue) {
          var isNotANumber = false;
          //Either for string or int
          keyArray.push(chkvalue);
          if (/^[0-9\-\+]*$/.test(chkvalue) == true) {
            chkvalue = parseInt(chkvalue);
            isNotANumber = true;
          }
          if (/^[0-9 \.\-\+]*$/.test(chkvalue) == true) {
            chkvalue = parseFloat(chkvalue);
            isNotANumber = false;
          }
          if (/^[0-9e\.\-\+]*$/.test(chkvalue) == true) {
            chkvalue = Number(chkvalue);
            isNotANumber = false;
          }
          if (chkvalue.length == 24 && db.ObjectID.isValid(chkvalue)) {
            chkvalue = db.ObjectID(chkvalue);
            isNotANumber = false;
          }
          if (isNotANumber) {
            keyArray.push(new RegExp('.*' + chkvalue + '.*', 'i'));
          } else {
            keyArray.push(chkvalue);
          }
        });
      }
      return keyArray;
    case 'dstring':
      valArray = value.split(',');
      keyArray = [];
      _.forEach(valArray, function (chkvalue) {
        keyArray.push(chkvalue);
      });
      return keyArray;
    default:
      return value;
  }
}

module.exports = {
  fetchRecords: fetchRecords,
  getApplicableSites: getApplicableSites,
  generateStatusFilter: generateStatusFilter,
  generateDateFilterScript: generateDateFilterScript,
  makeAttributeEntityFilter: makeAttributeEntityFilter,
  applicableSitesContainsSites: applicableSitesContainsSites,
}
