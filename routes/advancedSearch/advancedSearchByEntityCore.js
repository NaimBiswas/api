/*
To call advancedsearch/entity API
Pass Model Input data in like this format.

{
"apps":["5938f7c8b8a0da2c7cb9ff63","59809c1739705a49e896217f"],
"sort": {
"createdDate": -1
},
"attributes": [
{
"isEntity": true,
"keys": [
"1497948942455-1501590363922"
],
"value": 1
},
{
"isEntity": false,
"keys": [
"1496905557474"
],
"value": "Asia"
}
],
"batchEntryId": "",
"status": [],
"levelIds": [],
"includeInActiveRecords": true,
"pageSize": 10,
"pageNumber": 1,
"searchByDrop": "2",
"isForExport": true // Not complusary. If you want to manipulate data your-self than send it to true.
}


{
"apps":["594ef54ab90f154da82a82dd"],
  "sort": {
    "createdDate": -1
  },
  "attributes": [
    {
      "isEntity": true,
      "keys": [
        "1497948942455"
      ],
      "_id":"",
      "value": 1
    }
  ],
  "includeInActiveRecords": true,
  "pageSize": 10,
  "pageNumber": 1,
  "isForExport":false
}
*/

var db = require('../../lib/db');
var _ = require('lodash');
var moment = require('moment-timezone');
var q = require('q');
var logger = require('../../logger');
var currentFileName = __filename;
var batchEntriesService = require('../../lib/batchEntryService/index');

function applicableSitesContainsSites(applicableSites, sites) {
  logger.log("function: applicableSitesContainsSites - start", 'info', currentFileName);
  return sites.every(function (value) {
    return (applicableSites.indexOf(value) >= 0);
  });
}

function getApplicableSites(applicableSites) {
  logger.log("function: getApplicableSites - start", 'info', currentFileName);
  return  [].concat.apply([], applicableSites);
}

function schemaAttributeFilter(attributes) {
  logger.log("function: schemaAttributeFilter - start", 'info', currentFileName);
  var filter = [];
  _.forEach((attributes || []),function (attribute) {
    if (!attribute.isEntity) {
      filter.push({'attributes.s#': {'$in': attribute.keys}})
    } else if(attribute.type || attribute.isEntity){
      var keys = attribute.keys.map(function (key) {
        var entityQuestion = key.split('-') ||[];
        return entityQuestion[1]||key;
      });
      filter.push({'entities.questions.s#': {'$in': keys}})
    }
  });

  return filter;
}

function fetchRecords(options, callback) {
  logger.log("function: fetchRecords - start", 'info', currentFileName);
  var filter = {};
  if (options && options.apps && options.apps.length) {
    filter['_id'] = { '$in': options.apps.map(function (app) { return db.ObjectID(app) }) };
  }
  if (options.attributes && options.attributes.length) {
    filter['$and'] = schemaAttributeFilter(options.attributes);
  }
  if (!options.includeInActiveRecords) {
    filter.isActive = true;
  }
  try {

    var entryFilter = createFilters(options);
    fetchAllUsers(function (err, users) {
      if (err) {
        callback(err);
      } else {
        entryFilter.user = options.user;
        entryFilter.apps =  options.apps;
        entryFilter.users = users;
        getAllAppData(filter, entryFilter, function (err, result) {
          if (err) {
            callback(err);
          }
          if (result) {
            callback(null, result);
          }
        });
      }
    });
  } catch (e) {
    logger.log("function: fetchRecords - catch : " + e, 'error', currentFileName);
    callback(e);
  }
}

function getAllAppData(schemaFilter, options, callback) {
  logger.log("function: getAllAppData - start", 'info', currentFileName);
  var filteredAppEntryList = [];
  var columnFilter = {'title':1, 'attributes':1, 'isActive': 1, 'module': 1,
  'description': 1, 'createdBy': 1, 'modifiedBy': 1, 'createdByName': 1, 'modifiedByName': 1,
  'createdDate': 1, 'modifiedDate': 1, 'version': 1, 'entities': 1, 'masterQCSettings': 1, 'keyvalue':1};
  db.schema.find(schemaFilter,columnFilter, function (err, schema) {
    logger.log("collection:users before fetch" , 'info', currentFileName);
    function processItem(err, item) {
      if (err) {
        callback(err);
      }
      if (item === null) {
        callback(null, filteredAppEntryList);
        return;
      } else {
        if (typeof options['attributes'] === 'object' && options['attributes'].length && item) {
          options.query['$and'] = [];
          var attributesToFilter = options['attributes'].filter(function(a){
            return a.isEntity == false;
          });
          var entitiesToFilter = options['attributes'].filter(function(a){
            return a.isEntity == true;
          });
          if (entitiesToFilter.length) {
            var filteredEntities = generateEntitiesFilterSequenceWiseScript((item.entities||[]), entitiesToFilter);
            if (filteredEntities && filteredEntities.length) {
              filteredEntities.forEach(function(ent){
                options.query['$and'].push(ent);
              });
            }
          }
          if (attributesToFilter && attributesToFilter.length) {
            var filteredAttributes = generateAttributeFilterSequenceWiseScript((item.attributes||[]),attributesToFilter);
            if (filteredAttributes && filteredAttributes.length) {
              filteredAttributes.forEach(function(attrib){
                options.query['$and'].push(attrib);
              });
            }
          }
          if (!options.query['$and'].length) {
            delete options.query['$and'];
          }
        }

        var collectionName =(item._id + '');

        db.collection(collectionName).count(options.query, function(fail, count) {
          logger.log("collection:users before fetch" , 'info', currentFileName);
          if (fail) {
            callback(fail)
            return;
          } else {
            item.totalFilteredRecords = count;
            item.pageSize = options.pageSize;
            item.pageNumber = options.pageNumber;
            if (count>0 && (!filteredAppEntryList.length || options.isForExport)) {
              getFilteredAppRecords(options, collectionName, function (error, result) {
                if (error) {
                  callback(error)
                  return;
                } else {
                  item.entries = [];
                  fetchFormBatchEntry(result, item,options, function () {
                    if (!options.isForExport) {
                      _.forEach((result||[]), function (entry) {
                        item.entries.push(projectedListForAdvancedSearch(item, entry, options));
                      });
                      delete item.attributes;
                      delete item.attributes;
                      delete item.entities;
                    } else {
                      item.entries = result;
                    }

                    if (!options.apps || !options.apps.length) {
                      filteredAppEntryList.push(item);
                    } else {
                      filteredAppEntryList[_.findIndex(schemaFilter._id['$in'],item._id)] = item;
                    }
                    schema.nextObject(processItem);
                  });
                }
              });
            } else {
              if (!options.apps || !options.apps.length) {
                filteredAppEntryList.push(item);
              } else {
                filteredAppEntryList[_.findIndex(schemaFilter._id['$in'],item._id)] = item;
              }
              schema.nextObject(processItem);
            }
          }
        });
      }
    }
    schema.nextObject(processItem);

  });
}

function fetchAllUsers(cb) {
  logger.log("function: fetchAllUsers - start", 'info', currentFileName);
  try {
    db.collection('users').find({},{'username':1, 'firstname': 1, 'lastname': 1, '_id': 1})
    .toArray(function (err, result) {
      if (err) {
        cb(err);
      }else {
        cb(null, result);
      }
    });
  } catch (e) {
    cb(e);
  }
}

function projectedListForAdvancedSearch(schema, entry, options) {
  logger.log("function: projectedListForAdvancedSearch - start", 'info', currentFileName);
  var projectedEntry =  {
    "_id" : entry._id,
    "App" : {
      key: schema._id,
      value: schema.title
    }
  };
  _.forEach((schema.attributes||[]), function(attr) {
    if (!entry.entries[attr['s#']]) {
      projectedEntry[attr.title] = '';
      return;
    }else if (!_.isUndefined(attr)) {
      projectedEntry[attr.title] = displayValueByFormat(attr, entry.entries[attr['s#']]['attributes'], options.clientTZ) || '';
      if (typeof projectedEntry[attr.title] === 'object' && projectedEntry[attr.title].length) {
        projectedEntry[attr.title] = projectedEntry[attr.title].join(', ');
      }
    }else {
      projectedEntry[attr.title] = !_.isUndefined(entry.entries[attr['s#']]) ? entry.entries[attr['s#']]['attributes'] : '';
      if (typeof projectedEntry[attr.title] === 'object' && projectedEntry[attr.title].length) {
        projectedEntry[attr.title] = projectedEntry[attr.title].join(', ');
      }
    }
    projectedEntry[attr.title] = {
      's#' : 'entries.'+attr['s#']+'.attributes',
      value : projectedEntry[attr.title],
      key : projectedEntry[attr._id]
    };
  });
  _.forEach((schema.entities||[]), function(ent) {
    if (!entry.entries[ent.questions[0]['s#']+'-'+ent['s#']]) {
      projectedEntry[ent.title] = '';
      return;
    } else if (!_.isUndefined(ent.questions[0])) {//!_.isUndefined(ent.questions[0].type) && !_.isUndefined(ent.questions[0].type.format) && ent.questions[0].type.format.title == 'Date'
    projectedEntry[ent.title] = displayValueByFormat(ent.questions[0], entry.entries[ent.questions[0]['s#']+'-'+ent['s#']][ent['s#']], options.clientTZ) || '';
    if (typeof projectedEntry[ent.title] === 'object' && projectedEntry[ent.title].length) {
      projectedEntry[ent.title] = projectedEntry[ent.title].join(', ');
    }
  }
  else {
    projectedEntry[ent.title] = !_.isUndefined(entry.entries[ent.questions[0]['s#']+'-'+ent['s#']]) ? entry.entries[ent.questions[0]['s#']+'-'+ent['s#']][ent['s#']] : '';
    if (typeof projectedEntry[ent.title] === 'object' && projectedEntry[ent.title].length) {
      projectedEntry[ent.title] = projectedEntry[ent.title].join(', ');
    }
  }
  projectedEntry[ent.title] = {
    's#' : 'entries.'+ent.questions[0]['s#']+'-'+ent['s#']+'.'+ent['s#'],
    value : projectedEntry[ent.title],
    key : projectedEntry[ent._id]
  };
});

projectedEntry["Created Date"] = {
  value : moment.utc(new Date(entry.createdDate)).tz(options.clientTZ).format('MM/DD/YYYY HH:mm:ss'),
  key : 'createdDate'
};
projectedEntry["Modified Date"] = {
  value : moment.utc(new Date(entry.modifiedDate)).tz(options.clientTZ).format('MM/DD/YYYY HH:mm:ss'),
  key : 'modifiedDate'
};

if (!entry.modifiedByName && entry.modifiedBy && options && options.users && options.users.length) {
  var userDetails = _.find((options.users||[]), function (user) {
    return user._id.toString() == entry.modifiedBy.toString();
  });
  if (userDetails) {
    entry.modifiedByName = (userDetails.firstname||'').concat(' ').concat(userDetails.lastname||'').concat(' (').concat(userDetails.username||'N/A').concat(')');
  }
}

projectedEntry["Modified By"] = {
  key: 'modifiedByName',
  value: entry.modifiedByName
};

//by surjeet.b@productivet.com.../ for improvement no QC3-1639
if (entry && entry.assignment && entry.assignment.assignee && entry.assignment.assignee.length) {
  projectedEntry["Assignee"] = entry.assignment.assignee.map(function (user) {
    return '' + user.username;
  }).join(', ');
  projectedEntry["AssigneeHover"] = entry.assignment.assignee.map(function (user) {
    return user.firstname + ' ' + user.lastname + ' ('+ user.username + ')';
  }).join(', ');
} else {
  projectedEntry["Assignee"] = 'N/A';
}

projectedEntry["Record Status"] = {value: getStausString(entry)};
projectedEntry["Rejected"] = entry.isRejected;
projectedEntry["masterQCSettings"] = entry['masterQCSettings'];


if (entry['BatchEntry']) {
  projectedEntry["BatchEntry"] = entry['BatchEntry'];
}
projectedEntry["Batch Entry ID"] = {value: (entry['BatchEntry'] ? entry['BatchEntry'].title : 'N/A')};

return projectedEntry;
}

function fetchFormBatchEntry(entries, schema,options, cb) {
  logger.log("function: fetchFormBatchEntry - start", 'info', currentFileName);
  try {
    var schemaId = schema._id.toString();
    if (options && options.masterApp &&  options.masterApp.length) {
      schemaId = options.masterApp[0];
    }
    var query = {'schemaId': {'$in':[schemaId]}};

    (entries||[]).forEach(function (entry) {
      if (!query['schemaId']['$in']) {
        query['schemaId']['$in'] = [];
      }
      if (entry.masterQCSettings) {
        query['schemaId']['$in'].push(entry.masterQCSettings.masterQCSettingsId);
      }
    });

    batchEntryService.getBatchEntries(query['schemaId']).then(function (result) {
      (result||[]).forEach(function(batchEntry){
        _.forIn((batchEntry.batchEntries||{}), function (value, key) {
          (value||[]).forEach(function(item){
            entries.forEach(function (ent) {
              if (item.formId === ent._id.toString()) {
                ent['BatchEntry']= {
                  _id : batchEntry._id,
                  title : key
                };
                return;
              }
            });
          });
        });
      });
      cb();
    });
  } catch (e) {
    logger.log("function: fetchFormBatchEntry - catch : "+e , 'error', currentFileName);
    cb();
  }
}

function displayValueByFormat(c, value, clientTZ) {
  logger.log("function: displayValueByFormat - start", 'info', currentFileName);
  //var returnValue = '';
  var filterdDate = '';
  if (!_.isUndefined(c) && !_.isUndefined(c.type) && !_.isUndefined(c.type.format) && c.type.format.title==="Date" && value) {
    if (c.type.format.metadata.format === '' || !c.type.format.metadata.format ||  _.isUndefined(c.type.format.metadata.format)) {
      return moment.utc(new Date(value)).tz(clientTZ).format('MM/DD/YYYY HH:mm:ss');
    }else {
      switch(c.type.format.metadata.format){
        case 'dd/MM/yyyy':
        filterdDate ='DD/MM/YYYY';
        break;

        case 'MM/dd/yyyy':
        filterdDate = 'MM/DD/YYYY';
        break;

        case 'yyyy/MM/dd':
        filterdDate = 'YYYY/MM/DD';
        break;

        case 'MM/dd/yy':
        filterdDate = 'MM/DD/YY';
        break;

        case 'dd/MMM/yy':
        filterdDate = 'DD/MMM/YY';
        break;

        case 'MONTH/dd/yyyy':
        filterdDate = 'MMMM/DD/YYYY';
        break;

        case 'dd/MMM/yyyy':
        filterdDate = 'DD/MMM/YYYY';
        break;
        
        case 'MMMM/dd/yyyy':
        filterdDate = 'MMMM/DD/YYYY';
        break;

        default:
        filterdDate = 'MM/DD/YYYY';
      }
      return moment.utc(new Date(value)).tz(clientTZ).format(filterdDate);

    }
  }else if (!_.isUndefined(c) && !_.isUndefined(c.type) && !_.isUndefined(c.type.format) && c.type.format.title==="Time") {
    return moment.utc(new Date(value)).tz(clientTZ).format('HH:mm a');

  } else {
    return value;
  }
}

function getStausString(app) {
  logger.log("function: getStausString - start", 'info', currentFileName);
  if (!(app.entries && app.entries.status)) {
    return 'Draft';
  }
  var status = app.entries.status.status;
  var hasPendingWorkflowReview = false;
  if (app.workflowreview && app.workflowreview.length) {
    hasPendingWorkflowReview = _.some((app.workflowreview||[]), function (review) {
      return review.reviewstatus == 1
    });
  }
  if (status==3) {
    return 'Void';
  } else if (status == 4) {
    return 'Draft';
  } else if (hasPendingWorkflowReview && status == 1) {
    return 'Review Pending - Passed';
  } else if (hasPendingWorkflowReview && status == 0) {
    return 'Review Pending - Failed';
  } else if (status == 1) {
    return 'Passed';
  }  if (hasPendingWorkflowReview && status == 2) { //QC3-7516 by bhavin..
    return 'Review Pending';
  } else {
    return 'Failed'
  }
}

function createFilters(filters) {
  logger.log("function: createFilters - start", 'info', currentFileName);
  var pageSize = parseInt(filters.pageSize || 10);
  var pageNumber = parseInt(filters.pageNumber || 1);
  var options = {
    pageSize: pageSize,
    pageNumber: pageNumber,
    skip: pageSize * (pageNumber - 1),
    appFilter : {},
    limit: pageSize,
    isForExport: filters.isForExport||false,
    clientOffSet: filters.clientOffSet,
    clientTZ: filters.clientTZ,
    sort: filters.sort||{ 'createdDate': -1 },
    query: {},
    attributes: filters.attributes||[]
  };
  if (filters.sort) {
    options.sort = filters.sort;
  }
  if (filters.isForEntity) {
    options.isForEntity = filters.isForEntity;
  }
  if (filters.status && filters.status.length) {
    options.query['$or'] = generateStatusFilter(filters['status']);
  }

  if (filters.levelIds && filters.levelIds.length) {
    options.query['levelIds'] = { $all: filters.levelIds };
  }
  if(filters.customQuery){
    options.query = Object.assign(options.query, filters.customQuery);
  }
  else if (filters.applicableSites && filters.applicableSites.length && !filters.isForEntity) {
    options.query['$where'] = JSON.stringify(filters.applicableSites).replace(/\s/g,'').concat('.indexOf(this.levelIds[this.levelIds.length - 1])>=0');
  }
  if (filters.assignee && filters.assignee.length) {
    options.query['assignment.assignee.id'] = {
      '$in' : filters.assignee
    };
  }
  return options;
}

function getFilteredAppRecords(options, collectionName, callback) {
  logger.log("function: getFilteredAppRecords - start", 'info', currentFileName);
  try {
    db.collection(collectionName)
    .find(options.query, { 'fromToken': 0 })
    .sort(options.sort)
    .skip(options.skip)
    .limit(options.limit)
    .toArray(function (err, response) {
      if (err) {
        callback(err);
      } else {
        callback(null, response);
      }
    });
  } catch (exception) {
    logger.log("function: getFilteredAppRecords - catch : " + exception, 'error', currentFileName);
    callback(exception);
  }
}

function generateStatusFilter(statuses) {
  logger.log("function: generateStatusFilter - start" , 'info', currentFileName);
  if (!statuses || !statuses.length) {
    return;
  }
  var query = [];
  _.forEach(statuses, function (status) {
    if (status == 3 || status === 4) {
      query.push({ 'entries.status.status': status });
    } else if (status === 2) {
      query.push({ 'workflowreview.reviewstatus': { "$in": [1] }, 'entries.status.status': { "$nin": [3, 4] } });
    } else if (status === 1 || status === 0) {
      var subquery = { '$and': [] };
      subquery['$and'].push({ 'workflowreview.reviewstatus': { $ne: 1 } });
      subquery['$and'].push({ 'entries.status.status': status });
      query.push(subquery);
    }
  });
  if (query.length) {
    return query;
  } else {
    return {};
  }
}

function generateDateFilterScript(start, end) {
  logger.log("function: generateDateFilterScript - start" , 'info', currentFileName);
  if (!start && !end) {
    return;
  }
  if (start && end) {
    return { '$gt': parse('date', start), "$lt": parse('date', end) };
  } else if (start && !end) {
    return { '$gt': parse('date', start) };
  } else if (!start && end) {
    return { '$lt': parse('date', end) };
  }
}

function generateEntitiesFilterSequenceWiseScript(entities, filters) {
  logger.log("function: generateEntitiesFilterSequenceWiseScript - start" , 'info', currentFileName);
  var entitiesArray = (entities.filter(function(entity){
    entity.questions = _.filter(entity.questions, function(que){
      return que.isKeyValue})||[];
      return entity.questions.length>0
    })||[]).map(function(entity){
      return {
        'question' : entity.questions[0],
        's#': entity['s#'],
        '_id': entity['_id'],
        'combineds#':entity.questions[0]['s#']+"-"+entity['s#']
      };
    });
    var entitiesList =   filters;
    var filteredEntities = [];
    var filterEntity = {};
    var groupedEntities = _.groupBy(entitiesList, 'keys') || {};
    _.forIn(groupedEntities, function (group) {
      var matchedList = {'$or':[]};
      group.forEach(function (filter) {
        _.map(entitiesArray, function (entity) {
          var exists = _.some(filter.keys, function (key) {
            return key == entity.question['s#'];
          });
          if (exists) {
            var parsedValue = parse(entity.question.type.format.title, filter.value);
            var entitiesKey = 'entries.' + entity['combineds#'] + '.'+entity['s#'];
            var returningRelationship = {};
            returningRelationship[entitiesKey] = parsedValue;
            matchedList['$or'].push(returningRelationship);
            if (filter.value && filter.value.length && new Date(filter.value)) {
              filterEntity = {};
              filterEntity[entitiesKey] = filter.value;
              matchedList['$or'].push(filterEntity);
            } else if(typeof filter.value !== "number" && parseFloat(filter.value)){
              filterEntity = {};
              filterEntity[entitiesKey] = parseFloat(filter.value);
              matchedList['$or'].push(filterEntity);
            } else if (filter.value || filter.value == '0') {
              filterEntity = {};
              filterEntity[entitiesKey] = filter.value.toString();
              matchedList['$or'].push(filterEntity);
            }
          }
        });
      });
      if (matchedList['$or'].length) {
        filteredEntities.push(matchedList);
      }
    });
    return filteredEntities;
  }

  function generateAttributeFilterSequenceWiseScript(attributes, filters) {
    logger.log("function: generateAttributeFilterSequenceWiseScript - start" , 'info', currentFileName);
    var attributesList =   filters;
    var filteredAttributes = [];
    var groupedAttributes = _.groupBy(attributesList, 'keys') || {};
    _.forIn(groupedAttributes, function (group) {
      var matchedList = {'$or':[]};
      group.forEach(function (filter) {
        _.map(attributes, function (attribute) {
          var exists = _.some(filter.keys, function (key) {
            return key == attribute['s#'];
          });
          if (exists) {
            var parsedValue = parse(attribute.type.format.title, filter.value);
            var attributesKey = 'entries.' + attribute['s#'] + '.attributes';
            var returningRelationship = {};
            returningRelationship[attributesKey] = parsedValue;
            matchedList['$or'].push(returningRelationship);
            if (filter.value) {
              var dateFilter = {};
              dateFilter[attributesKey] = filter.value;
              matchedList['$or'].push(dateFilter);
            }else if(typeof filter.value !== "number" && parseFloat(filter.value)){
              var numberFilter = {};
              numberFilter[attributesKey] = parseFloat(filter.value);
              matchedList['$or'].push(numberFilter);
            } else{
              var stringFilter = {};
              stringFilter[entitiesKey] = filter.value.toString();
              matchedList['$or'].push(stringFilter);
            }
          }
        });
      });
      if (matchedList['$or'].length) {
        filteredAttributes.push(matchedList);
      }
    });

    return filteredAttributes;
  }

  function generateAttributeFilterScript(attributes, relation) {
    logger.log("function: generateAttributeFilterScript - start" , 'info', currentFileName);
    var filters = [];
    relation = relation || '$or'
    var groupedAttributes = _.mapValues(_.groupBy(attributes, 'key'),
    clist => clist.map(attribute => _.omit(attribute, 'key'))) || {};
    _.forIn(groupedAttributes, function (attribute, key) {
      var tempAttribute = {};
      tempAttribute[relation] = (_.map(groupedAttributes[key] || [], function (object) {
        var originalValue = object['value'];
        var parsedValue = parse(object['type'], originalValue);
        var operator = object['operator'];
        var returningRelationship = {};
        var attributesKey = 'entries.' + key + '.attributes';
        if (operator) {
          var internalQuery = {};
          internalQuery[operator] = parsedValue;
          returningRelationship[attributesKey] = internalQuery;
        } else {
          returningRelationship[attributesKey] = originalValue;
        }
        return returningRelationship;
      }) || []);
      filters.push(tempAttribute);
    });
    return filters;
  }

  function parse(type, value) {
    logger.log("function: parse - start" , 'info', currentFileName);
    type = (type||'').toLowerCase();
    switch (type) {
      case 'date':
      return new Date(value);
      case 'regex':
      return { $regex: new RegExp(value.toLowerCase(), 'i') };
      case 'number':
      case 'exponential':
      case 'percentage':
      case 'currency':
      case 'scientific':
      if (/^[0-9\-\+]*$/.test(value) == true) {
        value = parseInt(value);
      }
      if (/^[0-9 \.\-\+]*$/.test(value) == true) {
        value = parseFloat(value);
      }
      return value;
      case 'array':
      var keyArray = [];
      if (typeof value == 'string' && value.length) {
        var valArray = value.split(',');
        _.forEach(valArray, function (chkvalue) {
          var isNotANumber = false;
          //Either for string or int
          keyArray.push(chkvalue);
          if (/^[0-9\-\+]*$/.test(chkvalue) == true) {
            chkvalue = parseInt(chkvalue);
            isNotANumber = true;
          }
          if (/^[0-9 \.\-\+]*$/.test(chkvalue) == true) {
            chkvalue = parseFloat(chkvalue);
            isNotANumber = false;
          }
          if (/^[0-9e\.\-\+]*$/.test(chkvalue) == true) {
            chkvalue = Number(chkvalue);
            isNotANumber = false;
          }
          if (chkvalue.length == 24 && db.ObjectID.isValid(chkvalue)) {
            chkvalue = db.ObjectID(chkvalue);
            isNotANumber = false;
          }
          if (isNotANumber) {
            keyArray.push(new RegExp('.*' + chkvalue + '.*', 'i'));
          } else {
            keyArray.push(chkvalue);
          }
        });
      }
      return keyArray;
      case 'dstring':
      valArray = value.split(',');
      keyArray = [];
      _.forEach(valArray , function(chkvalue){
        //var a = new Date(chkvalue);
        keyArray.push(chkvalue);
      });
      return keyArray ;
      default:
      return value;
    }
  }

  module.exports = {
    fetchRecords: fetchRecords,
    getApplicableSites: getApplicableSites,
    applicableSitesContainsSites: applicableSitesContainsSites
  }
