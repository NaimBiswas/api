'use strict';
/**
 * @name questions-schema
 * @author Surjeet Bhadauriya <surjeet.b@productivet.com>
 *
 * @version 2.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var anyArray = types.anyArray;
var rBool = types.rBool;
var rDate = types.rDate;
var bool = types.bool;
var number = types.number;
var rNumber = types.rNumber;
var string = types.string;
var required = types.required;
var stringArray = types.stringArray;
var any = types.any;
var rBigString = types.bigString;
var bigString = types.bigString;
var mediumBigString = types.mediumBigString;

var schema = {
  schemaId: rString.label('schemaOrMasterId'),
  schemaTitle: bigString.label('schemaTitle'),
  masterQCSettings: any,
  entryObj: object({
    entryId: rId.label('entryId'),
    entries: any,
    status: any,
    createdDate: rDate.label('createdDate'),
    modifiedDate: rDate.label('modifiedDate'),
    createdBy: any,
    modifiedBy:any,
  }).required().label('entryObj'),
  // createdDate: rDate.label('createdDate'),
  // modifiedDate: rDate.label('modifiedDate'),
  assignment: array(object({
    assignee: array(object({  // PQT-4 - Jyotil - Making array of object
      id: rId.label('Assignee User Id'),
      firstname: rString.label('Assignee Firstname'),
      lastname: rString.label('Assignee Lastname'),
      username: rString.label('Assignee Username')
    })).required().label('Assignee'),
    roles: array(Object({  // PQT-4 - Jyotil - Making array of object
      id: rId.label('Role Id'),
      title: string.label('Role Title')
    })).required().label('Role'),
    assignedBy: object({
      id: rId.label('Assigned By User Id'),
      firstname: rString.label('Assigned By Firstname'),
      lastname: rString.label('Assigned By Lastname'),
      username: rString.label('Assigned By Username')
    }).required().label('Assigned By'),
    assignedDate: rDate.label('Assigned Date'),
    comments: mediumBigString.label('comments').allow(''),
    isRejected : bool.label('Is Rejected'), // PQT-1 - Jyotil - added boolean for rejection.
    appRecordUrl: mediumBigString.label('App Record Url'),
    recordVersion: any
  })).required().label('Assignment'),
  sendAssignmentEmail: bool.label('sendAssignmentEmail'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
