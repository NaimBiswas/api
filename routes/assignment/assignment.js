'use strict'
var colors = require('colors');
var schema = require('./schema');
var softSchema = require('./softSchema');
var db = require('../../lib/db');
var mailer = require('../../lib/mailer');
var _ = require('lodash');
var settings = require('../../lib/settings');
var log = require('../../logger');
var logger = require('../../logger');
var currentFileName = __filename;

var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'assignment',
    collection: 'assignment',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    POST: {
      ONE: {
        before: beforePostorPatch
      }
    },
    PATCH: {
      ONE: {
        before: beforePostorPatch
      }
    }
  }
});

/**
* @swagger
* definition:
*   Question:
*     properties:
*       id:
*         type: integer
*       title:
*         type: string
*       isActive:
*         type: boolean
*       type:
*         $ref: '#/definitions/Type'
*       validation:
*         $ref: '#/definitions/Validation'
*       modifiedDate:
*         type: dateTime
*       createdDate:
*         type: dateTime
*       createdByName:
*         type: string
*       modifiedByName:
*         type: string
*       modifiedBy:
*         $ref: '#/definitions/ModifiedBy'
*       createdBy:
*         $ref: '#/definitions/CreatedBy'
*       versions:
*           type: array
*           items:
*              type: object
*              allOf:
*              - $ref: '#/definitions/Version'
*       version:
*           type: integer
*       isMajorVersion:
*           type: boolean
*/

/**
* @swagger
* /questions:
*   get:
*     tags: [Question]
*     description: Returns all questions
*     produces:
*       - application/json
*     parameters:
*       - name: page
*         description: Page Number
*         in: query
*         required: false
*         type: string
*       - name: pagesize
*         description: Page Size
*         in: query
*         required: false
*         type: string
*       - name: select
*         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,title,type.title,modifiedDate,modifiedBy,version,versions,isActive])
*         in: query
*         required: false
*         type: string
*       - name: sort
*         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
*         in: query
*         required: false
*         type: string
*     responses:
*       200:
*         description: An array of questions
*         schema:
*           $ref: '#/definitions/Question'

*/

/**
* @swagger
* /questions/{id}:
*   get:
*     tags: [Question]
*     description: Returns a object question
*     produces:
*       - application/json
*     parameters:
*       - name: id
*         description: Questtion's id
*         in: path
*         required: true
*         type: integer
*       - name: pagesize
*         description: Page Size
*         in: query
*         required: false
*         type: string
*     responses:
*       200:
*         description: An object of question
*         schema:
*           $ref: '#/definitions/Question'

*/

/**
* @swagger
* /questions/{id}:
*   patch:
*     tags: [Question]
*     description: Updates a question
*     produces:
*       - application/json
*     parameters:
*       - name: id
*         description: Question's id
*         in: path
*         required: true
*         type: integer
*       - name: question
*         description: question object
*         in: body
*         required: true
*         schema:
*           $ref: '#/definitions/Question'
*     responses:
*       200:
*         description: Successfully updated
*/
/**
* @swagger
* /questions:
*   post:
*     tags: [Question]
*     description: Creates a new question
*     produces:
*       - application/json
*     parameters:
*       - name: question
*         description: Question object
*         in: body
*         required: true
*         schema:
*           $ref: '#/definitions/Question'
*     responses:
*       201:
*         description: Successfully created
*/

function sendEmailAlertForAssignment(entity, next) {
  logger.log("function: sendEmailAlertForAssignment - start", 'info', currentFileName);
  if (entity && entity.assignment && entity.assignment.length && entity.sendAssignmentEmail) {
    var assignmentData = entity.assignment[entity.assignment.length - 1];
    if (assignmentData) {

      var assigneeIds = assignmentData.assignee.map(function (asign) {
        return db.ObjectID(asign.id);
      })

      db.users.find({ _id: { $in: assigneeIds }, assignmentEmailNotification: true }).toArray(function (err, users) {
        if (err || !users) {
          return;
        }
        //PQT-141 - Surjeet Bhadauriya.. surjeet.b@productivet.com
        sendEmail(users, assignmentData, entity);
      });
    }
  }
}

/**
 * 
 * @author Surjeet Bhadauriya <surjeet.b@productivet.com>
 * @param {array} users 
 * @param {array} assignmentData 
 * @param {object} entity 
 */
function sendEmail(users, assignmentData, entity) {
  if (users.length) {
    var sendMailToUser = users.shift();
    var clonedAssignmentData = _.cloneDeep(assignmentData);
    // PQT-1045 by Yamuna
    if(sendMailToUser && clonedAssignmentData){
      clonedAssignmentData.assignee = {
        "username" : sendMailToUser.username,
        "firstname" : sendMailToUser.firstname,
        "lastname" : sendMailToUser.lastname
    }
    }
    _.merge(clonedAssignmentData, { assigneeEmail: sendMailToUser.email, schemaTitle: entity.schemaTitle, settings });
    mailer.send('assignment-notify', clonedAssignmentData, clonedAssignmentData.assigneeEmail, function sendMailCallback(e, b) {
      if (e) {
        log.log('Failed sending email - qcform-status-notify', 'error');
        log.log(entity, 'error');
        log.log(e, 'error');
      } else {
        log.log("Assignee change info has sent to " + sendMailToUser.email, "info");
      }
      sendEmail(users, assignmentData, entity);
    });
  }
}

/**
 * 
 * @author Surjeet Bhadauriya <surjeet.b@productivet.com>
 * @param {object} obj 
 * @param {function} cb 
 */
function getEntryVersion(obj, cb) {
  //getting entry data
  if (!obj) {
    cb({ err: 'obj is not found' }, null);
  }
  db[obj.schemaId].findOne(
    { _id: db.ObjectID(obj.entryObj.entryId) },
    { 'version': 1 },
    function (err, entryData) {
      if (err) {
        cb(err, null);
      }
      cb(null, entryData);
    }
  );
}

function beforePostorPatch(req, res, next) {
  logger.log("function: beforePostorPatch - start", 'info', currentFileName);
  if (req.body.assignment && req.body.assignment.length) {
    // var obj = req.body.assignment[req.body.assignment.length - 1];
    getEntryVersion(req.body, function (err, entryData) {
      if (err) {
        next();
      }
      req.body.assignment[req.body.assignment.length - 1].recordVersion = entryData.version;
      next();
    });
  }
  // second condition check if reject record has been change assignee
  if (!req.body.assignment[req.body.assignment.length - 1].isRejected) {
    sendEmailAlertForAssignment(req.body, next);
  }
}

module.exports = route.router;
