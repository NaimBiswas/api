'use strict'
/**
 * @name calendar-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var string = types.string;
var rDate = types.rDate;

var schema = {
  title: rString.label('Calendar title'),
  type: rString.label('Calendar type'),
  scheduledBy: rString.label('Calendar scheduled by user'),
  time: string.label('Calendar time'),
  date: rDate.label('Calendar date'),
  scheduleId: rString.label('Calendar scheduled by id'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
