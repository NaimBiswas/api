'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'drilldownRequestParameter',
    collection: 'drilldownRequestParameter',
    schema: schema,
    softSchema: softSchema
  }
});



/**
 * @swagger
 * definition:
 *   DrilldownRequestParameter:
 *     properties:
 *       formIdlist:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/FormIdlist'
 *       procedureId:
 *         type: integer
 *       startDate:
 *         type: dateTime
 *       endDate:
 *         type: dateTime
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         type: integer
 *       createdBy:
 *         type: integer
 *       id:
 *         type: integer
 */


/**
 * @swagger
 * /drilldownRequestParameter:
 *   post:
 *     tags: [Reports]
 *     description: Creates a new drilldownRequestParameter
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: drilldownRequestParameter
 *         description: DrilldownRequestParameter object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/DrilldownRequestParameter'
 *     responses:
 *       201:
 *         description: Successfully created
 */


module.exports = route.router;
