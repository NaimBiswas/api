'use strict'
/**
* @name Schemas-schema
* @author Shoaib Ganja <shoaib.g@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;

var rBool = types.rBool;
var bool = types.bool;
var rNumber = types.rNumber;
var number = types.number;
var string = types.string;
var id = types.id;
var required = types.required;
var stringArray = types.stringArray;
var optionalArray = types.optionalArray;
var any = types.any;
var bigString = types.bigString;
var extraLargeString = types.extraLargeString;
var rExtraLargeString = types.rExtraLargeString;
var smallString = types.smallString;


var schema = {
  title: rString.label('Schema title'),
  toSync: bool.label('Sync for SSRM'),
  loader: bool.label('loader flag'),
  batchEntries: any,
  batchAttributesInfo: any,
  module:object({
    _id: rId.label('Module schema id'),
    title: rString.label('Schema title')
  }).required().label('Schema module'),
  description: any.label('Description').allow(''),
  dmsfile: any.label('Schema upload DMS file'),
  deleteProcedure: any.label("Delete Procedure"),
  helpDocumentPath: string.label('Schema HelpDocumentPath'),
  instructionURL: any.label('Schema instructionURL'),
  file: any.label('Schema upload file'),
  siteYesNo: rString.label('siteYesNo Sites?'),
  addSites: rBool.label('Sites added?'),
  allowcreater: bool.label('Allow Creater'),
  isLockRecord : bool.label('is Lock Record'),
  isLockReviewWorkflow: bool.label('is Lock Review Workflow'),
  allowCreaterObj: object({
    id: rId.label('Allow Creater id'),
    username: rString.label('Allow Creater username')
  }).label('allowCreaterObj'),
  setreviewworkflow: bool.label('Set Review Workflow'),
  webhookEnabled: bool.label('Enable Webhook'),
  webhookUrl: bigString.label('URL for webhook'),
  webhookAuthentication: any.label('Authentication for Webhook URL'),
  workflowreview: optionalArray(object({
    anyOne: any.label('Required To Sign'),//PQT-15 Prachi
    userSelectionType: any,
    reviewstatus: rNumber.label('Review Status'),
    roles: any,
    user: optionalArray(object({
      id: rId.label('user id'),
      username: rString.label('username'),
      firstname: rString.label('First Name'),
      lastname: rString.label('Last Name')
    }))
  })).label('Set Workflow Review array')
  .when('setreviewworkflow', {is: true, then: required()}),
  reviewRequired: bool.label('ReviewRequired?'),
  allicableSites:optionalArray(
    array(object({
      _id: rId.label('AllicableSites schema id'),
      title: rString.label('AllicableSites schema title')
    }))
  ).label('Allicable sites array')
  .when('addSites', {is: true, then: required()}),
  // reviewRequired: rBool.label('ReviewRequired?'),

// by miral QC3-9174
  attributes: optionalArray(object({
    _id: id.label('Attribute id'),
    's#': string.label('Sequence number'),
    title: string.label('Attribute title'),
    isSetDefaultValue: bool.label('Is Set Default Value'),
    setDefaultValue: any,
    addBarcodeRule: bool.label('Add Barcode Rule'),
    addBarcodeRulePrefix: any.label('Add Barcode Rule Prefix').allow(''),
    addBarcodeRuleSuffix: any.label('Add Barcode Rule Suffix').allow(''),
    autoGenerateNumberPrefix: smallString.label('Auto Generate Number Prefix').allow(''),
    autoGenerateNumberSuffix: smallString.label('Auto Generate Number Suffix').allow(''),
    version : number.label('Attribute version'),
    markSetValueInActive: bool.label('Mark Set Value InActive'),
    type: object({
      _id: rId.label('Type id'),
      title: rString.label('Type title'),
      format: object({
        _id: rId.label('Format id'),
        title: rString.label('Format title'),
        metadata: any
        //formatString: string.label('Format string')
      })
    }).required().label('Type object'),
    measurementUnit: object({
      enabled: rBool.label('Enabled?'),
      title: string.label('Measurement unit title')
      .when('enabled', {is: true, then: required()})
    }),
    isActive: rBool.label('isStatus?'),
    module: object({
      _id: rId.label('Module id'),
      title: rString.label('Module title')
    }).required().label('Module object'),
    validation: any

// by miral QC3-9174
})).required().label('attributes array of schema'),
// })).label('attributes array of schema'),
  keyvalue : any,
  requirefordraft : any,
  entities: optionalArray(object({
    _id: rId.label('Entity id'),
    refrencedQuestion: any.label("referenced Questions"),
    's#': string.label('Sequence number'),
    title: extraLargeString.label('Entity title'),
    version: number.label('Entity version'),
    isShowQuestionAdded: bool.label('isShowQuestionAdded'),
    isExpBuild: bool.label('isExpBuild'),
    isInstructionForUser: bool.label('is Instrcution For Users'),
    instructionURL: any.label('Schema instructionURL'),
    file: any.label('Schema upload file'),
    dmsfile: any.label('Schema upload DMS file'),
    instructionForUser: any.label('Instruction for user').allow(''),
    markEntityValueInActive: bool.label('Mark Entity Value InActive'),
    getInactiveRecord: bool.label('Display Inactive Record'),
    enableEntityRecord : bool.label('Enable Entity Record in Update Mode'),
    isDisplayQuestion : bool.label('IsDisplayQuestion'),
    isSetLabel : bool.label('IsSetLabel?'),
    entityLabelTitle : any,
    filteredEntQuesSelected: any,
    filteredEntQues: any,
    entityUsedInProcedureCondition: any,
    questions: array(object({
      parentId: string.label('Parent Entity Id'),
      comboId: string.label('Entity and Question Combo Id'),
      isOptional: bool.label('isOptional'),
      addBarcodeRule: bool.label('Add Barcode Rule'),
      addBarcodeRulePrefix: any.label('Add Barcode Rule Prefix').allow(''),
      addBarcodeRuleSuffix: any.label('Add Barcode Rule Suffix').allow(''),
      // _id: rId.label('Question id'),
      's#': string.label('Sequence number'),
      version: number.label('Question version'),
      title: extraLargeString.label('Questions title'),
      isKeyValue:bool.label('IsKeyValue'),
      isAppearance:bool.label('IsAppearance'),
      isDisplayQuestion : bool.label('isDisplayQuestion'),
      type: object({
        _id: id.label('Type id'),
        title: string.label('Type title'),
        format: object({
          _id: id.label('Type id'),
          title: string.label('Type title'),
          metadata: any
        })
      }).label('Type object'),
      measurementUnit: object({
        enabled: rBool.label('Measurement enabled?'),
        title: string.label('Measurement title')
        .when('enabled', { is: true, then: required() })
      }),
      validation: any,
      typeSelector: any.label('Type Selector'),
      linkToEntity: any.label('Link To Entity'),
      criteria: any,
      formula: any,
    })).required().label('Questions array'),
    values: any,
    conditionalWorkflows: any,
    filterEntityQuestionEvents: any,
    entityFilters: any,
    questionUsedInProcedureCondition: any,
    questionUsedInEntityFilter : any,
    questionUsedInAcceptanceConditionProcedure: any,
    questionUsedInFormula : any,
    questionUsedInAcceptance : any
  })).required().label('Entity array'),
  procedures:optionalArray(object({
    _id: rId.label('Procedure id'),
    linkToOtherProcedure: any.label('Procedure Link'),
    referencedQuestions: any.label("referenced Questions"),
    refrencedQuestion: any.label("referenced Questions"),
    's#': string.label('Sequence number'),
    instructionURL: any.label('Schema instructionURL'),
    file: any.label('Schema upload file'),
    dmsfile: any.label('Schema upload file'),
    title: extraLargeString.label('Procedure title'),
    version : number.label('Procedure version'),
    conditional : bool.label('Conditional Procedure'),
    questions: array(object({
      parentId: string.label('Parent Procedure Id'),
      comboId: string.label('Procedure and Question Combo Id'),
      _id: rId.label('Question id'),
      's#': string.label('Sequence number'),
      version : number.label('Question version'),
      isHiddenQuestion: bool.label('isHiddenQuestion'),
      checkValidCriteria: bool.label('Check Validity For Acceptance Criteria'),
      addBarcodeRule: bool.label('Add Barcode Rule'),
      addBarcodeRulePrefix: any.label('Add Barcode Rule Prefix').allow(''),
      addBarcodeRuleSuffix: any.label('Add Barcode Rule Suffix').allow(''),
      title: extraLargeString.label('Questions title'),
      isoptionalquestion: bool.label('isoptionalquestion'),
      type: object({
        _id: rId.label('Type id'),
        title: rString.label('Type title'),
        format: object({
          _id: rId.label('Type id'),
          title: rString.label('Type title'),
          metadata: any
          //formatString: string.label('Format string of type')
        })
      }).required().label('Type object'),
      measurementUnit: object({
        enabled: rBool.label('Measurement enabled?'),
        title: string.label('Measurement title')
        .when('enabled', {is: true,then: required()})
      }),
      isInstructionForUser: bool.label('Is Instruction for user'),
      instructionForUser: any.label('Instruction for user').allow(''),
      validation: any,
      criteria: any,
      formula: any,
    })).required().label('Questions array'),
    isOptionalProcedure: bool.label('IsOptionalProcedure?'),
    optionalProcedureTitle: any,
    isMultipleProcedure: bool.label('Is Multiple Procedure'),
    multipleProcedureTitle: any,
    isInstructionForUser: bool.label('Is Instruction for user'),
    instructionForUser: any.label('Instruction for user').allow(''),
    conditionalWorkflows: any,
    questionUsedInAcceptance: any,
    questionUsedInFormula: any,
    questionUsedInProcedureCondition: any,
    questionUsedInAcceptanceConditionProcedure: any
  })).required().label('Procedure array'),
  acceptanceCriteria: object({
    // _id: rId.label('AcceptanceCriteria id'),
    criteria: array(object({
      procedures: optionalArray(object({
        _id: rId.label('Procedures id'),
        's#': string.label('Sequence number'),
        title: extraLargeString.label('Procedure title')
      })).required().label('Procedures array')
    })).required().label('Criteria array')
  }).required().label('AcceptanceCriteria object'),
  appWorkflows :any,
  isActive: rBool.label('IsActive?'),
  isSendEmail: bool.label('IsSendEmail?'),
  isShared: bool.label('isShared?'),
  emailIdForQCFormFail: extraLargeString.label('Email Collection').allow(''),
  isDeleted: bool.label('Is Deleted ?'),
  isPrintLinkedApp: bool.label('Is PrintLinkedApp'),
  headMsgPrint: any,
  headMsgPrintForPass: any,
  headMsgPrintForFail: any,
  isPrintFailMsg: bool.label('Is isPrintFailMsg'),
  restoreStableVersion: bool.label('Is Restore Stable Version'),
  restoreStableVersionAudit: object({
    previousVersion: number.label('Previous Version'),
    stableVersion: number.label('Stable Version')
  }),
  isDraft: bool.default(false).label('Is Draft Version'),
  isAuditing: bool.label("Is Updating ?"),
  timeZone: string.label('Time Zone')
};

/**
* @swagger
* definition:
*   Type:
*     properties:
*        id:
*          type: integer
*        format:
*           $ref: '#/definitions/Format'
*        title:
*           type: string
*/

/**
* @swagger
* definition:
*   Condition:
*     properties:
*        type:
*          type: boolean
*        currenttime:
*           type: integer
*        conditionTitle:
*           type: string
*        value:
*           type: dateTime
*        validationMessage:
*           type: string
*        maximumDate:
*           type: dateTime
*        minimumDate:
*           type: dateTime
*/


/**
* @swagger
* definition:
*   Validation:
*     properties:
*        type:
*          type: integer
*        condition:
*           $ref: '#/definitions/Condition'
*/

/**
 * @swagger
 * definition:
 *   Version:
 *     properties:
 *       version:
 *         type: integer
 *       record:
 *         type: integer
 */

 /**
  * @swagger
  * definition:
  *   ModifiedBy:
  *     properties:
  *       id:
  *         type: integer
  *       username:
  *         type: string
  *       firstname:
  *         type: string
  *       lastname:
  *         type: string
  */

  /**
   * @swagger
   * definition:
   *   CreatedBy:
   *     properties:
   *       id:
   *         type: integer
   *       username:
   *         type: string
   *       firstname:
   *         type: string
   *       lastname:
   *         type: string
   */

/**
 * @swagger
 * definition:
 *   Module:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 */

 /**
  * @swagger
  * definition:
  *   AllowCreaterObj:
  *     properties:
  *       id:
  *         type: integer
  *       username:
  *         type: string
  */

  /**
   * @swagger
   * definition:
   *   Workflowreview_schema:
   *     properties:
   *       user:
   *           type: array
   *           items:
   *              type: object
   *              allOf:
   *              - $ref: '#/definitions/User_workflowreview'
   *       roles:
   *           type: array
   *           items:
   *              type: string
   *       reviewstatus:
   *           type: integer
   */


/**
 * @swagger
 * definition:
 *   Attribute_schema:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       type:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Type'
 *       version:
 *           type: integer
 *       isActive:
 *         type: boolean
 *       module:
 *         $ref: '#/definitions/Module'
 *       s#:
 *         type : integer
 */

 /**
  * @swagger
  * definition:
  *   procedures_app:
  *     properties:
  *       id:
  *         type: integer
  *       title:
  *         type: string
  *       s#:
  *         type: integer
  */


 /**
  * @swagger
  * definition:
  *   criteria_app:
  *     properties:
  *       criteria:
  *           type: array
  *           items:
  *              type: object
  *              allOf:
  *              - $ref: '#/definitions/procedures_app'

  */

 /**
  * @swagger
  * definition:
  *   AcceptanceCriteria_app:
  *     properties:
  *       criteria:
  *           type: array
  *           items:
  *              type: object
  *              allOf:
  *              - $ref: '#/definitions/criteria_app'

  */

  /**
   * @swagger
   * definition:
   *   Keyvalue:
   *     properties:
   *       id:
   *         type: integer
   *       title:
   *         type: string
   *       type:
   *           type: array
   *           items:
   *              type: object
   *              allOf:
   *              - $ref: '#/definitions/Type'
   *       version:
   *           type: integer
   *       isActive:
   *         type: boolean
   *       s#:
   *         type: integer
   *       isUniqueKeyValue:
   *         type: boolean
   */

   /**
    * @swagger
    * definition:
    *   Requirefordraft:
    *     properties:
    *       id:
    *         type: integer
    *       title:
    *         type: string
    *       type:
    *           type: array
    *           items:
    *              type: object
    *              allOf:
    *              - $ref: '#/definitions/Type'
    *       version:
    *           type: integer
    *       isActive:
    *         type: boolean
    *       s#:
    *         type: integer
    *       isRequireForDraft:
    *         type: boolean
    */

    /**
        * @swagger
        * definition:
        *   Innercriteria:
        *     properties:
        *        criteriaType:
        *          type: integer
        *        condition:
        *           type: string
        *        minimum:
        *           type: dateTime
        *        maximum:
        *           type: dateTime
        */

        /**
        * @swagger
        * definition:
        *   Innercondition:
        *     properties:
        *        leftOperand:
        *          type: integer
        *        rightOperand:
        *           type: int
        *        comparison:
        *           type: string
        *        bool:
        *           type: boolean
        */

        /**
        * @swagger
        * definition:
        *   Criteria:
        *     properties:
        *        condition:
        *           type: array
        *           items:
        *              type: object
        *              allOf:
        *              - $ref: '#/definitions/Innercondition'
        *        criteria:
        *           $ref: '#/definitions/Innercriteria'
        */

/**
 * @swagger
 * definition:
 *   Appbuilder:
 *     properties:
 *       id:
 *         type: integer
 *       loader:
 *         type: boolean
 *       title:
 *         type: string
 *       addSites:
 *         type: boolean
 *       attributes:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Attribute_schema'
 *       procedures:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Procedure'
 *       entities:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Entity'
 *       keyvalue:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Keyvalue'
 *       requirefordraft:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Requirefordraft'
 *       acceptanceCriteria:
 *         $ref: '#/definitions/AcceptanceCriteria_app'
 *       isActive:
 *         type: boolean
 *       setreviewworkflow:
 *         type: boolean
 *       isDeleted:
 *         type: boolean
 *       siteYesNo:
 *         type: boolean
 *       isLockRecord:
 *         type: boolean
 *       allicableSites:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Allicablesite'
 *       module:
 *         $ref: '#/definitions/Module'
 *       allowCreaterObj:
 *         $ref: '#/definitions/AllowCreaterObj'
 *       file:
 *         type: string
 *       workflowreview:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Workflowreview_schema'
 *       isAuditing:
 *         type: boolean
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       createdBy:
 *         $ref: '#/definitions/CreatedBy'
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       version:
 *           type: integer
 *       isMajorVersion:
 *           type: boolean
 */


/**
 * @swagger
 * /schema:
 *   get:
 *     tags: [App Builder]
 *     description: Returns all apps
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,title,module.title,modifiedDate,modifiedBy,version,isActive,isAuditing])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of apps
 *         schema:
 *           $ref: '#/definitions/Appbuilder'
 */

 /**
  * @swagger
  * /schema/{id}:
  *   get:
  *     tags: [App Builder]
  *     description: Returns a object appbuilder
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: App's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: pagesize
  *         description: Page Size
  *         in: query
  *         required: false
  *         type: string
  *     responses:
  *       200:
  *         description: An object of appbuilder
  *         schema:
  *           $ref: '#/definitions/Appbuilder'

  */

 /**
  * @swagger
  * /schema:
  *   post:
  *     tags: [App Builder]
  *     description: Creates a new App
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Appbuilder
  *         description: appbuilder object
  *         in: body
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Appbuilder'
  *     responses:
  *       201:
  *         description: Successfully created
  */

 /**
  * @swagger
  * /schema/{id}:
  *   patch:
  *     tags: [App Builder]
  *     description: Updates a app
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: app's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: app
  *         description: appbuilder object
  *         in: body
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Appbuilder'
  *     responses:
  *       200:
  *         description: Successfully updated
  */

module.exports = schema;
