'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var dbConfig = require('../../lib/db/db');
var log = require('../../logger');
var debug = require('../../lib/logger').debug('routes:schema');
var entries = require('../entries');
var _ = require('lodash');
var db = require('../../lib/db');
var linkedTbl = require('../../lib/linkedTbl');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'schema',
    collection: 'schema',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    POST: {
      ONE: {
        after: afterPost
      },
      ONESOFT: {
        after: afterPost
      }
    },
    PATCH: {
      ONE: {
        after: afterPatch
      },
      ONESOFT: {
        after: afterPatch
      }
    }
  }
});

function afterPost(req, res, next) {
  //req.data contains the posted / patched
  //schedule object with createdBy and modifiedBy
  //And all, orignal object can be accessed from
  //req.items
  //log.log(req.data._id);
  //log.log('Username :: ' + req.user.username);
  try {
    log.log('Start creating dynamic route for schema.','info');
    var entities = [];
    entities.push({
      collection: req.data._id.toString('hex'),
      audit: true,
      index: {
        title: ''
      }
    });
    log.log('Go for add new dynamic table in dbConfig ' + req.data._id,'debug');
    dbConfig.configure.apply(this, entities);
    log.log('New dynamic table added for :: ' + req.data._id,'info');

    entries.use('/' + req.data._id.toString('hex'), require('../entries/entries')(req.data._id.toString('hex')));
    log.log('New dynamic route created successfully ' + req.data._id,'info');
  } catch (err) {
    log.log('Error while generating route :: ' + err,'error');
  }

  linkedTbl.insertInLinked('schema', req.data);
  res.json(req.data);
}

function afterPatch(req, res, next) {
  linkedTbl.insertInLinked('schema', req.data);
  res.json(req.data);
}

module.exports = route.router;
