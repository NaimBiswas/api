//module.exports = require('./schema');
'use strict'
/**
* @name Schemas-schema
* @author Shoaib Ganja <shoaib.g@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var array = types.array;
var id = types.id;
var string = types.string;
var bool = types.bool;
var number = types.number;
var required = types.required;
var stringArray = types.stringArray;
var optionalArray = types.optionalArray;
var date = types.date;
var any = types.any;
var bigString = types.bigString;
var extraLargeString = types.extraLargeString;
var rExtraLargeString = types.rExtraLargeString;
var smallString = types.smallString;
var schema = {
  _id: id.label('Schema ID'),
  title: string.label('Schema title'),
  toSync: bool.label('Sync for SSRM'),
  loader: bool.label('loader flag'),
  batchEntries: any,
  batchAttributesInfo: any,
  module:object({
    _id: id.label('Module schema id'),
    title: string.label('Schema title')
  }).label('Schema module'),
  description: any.label('Description').allow(''),
  helpDocumentPath: string.label('Schema HelpDocumentPath'),
  instructionURL: any.label('Schema instructionURL'),
  file: any.label('Schema upload file'),

  dmsfile: any.label('Schema upload DMS file'),
  deleteProcedure: any.label("Delete Procedure"),
  siteYesNo: string.label('siteYesNo Sites?'),
  addSites: bool.label('Sites added?'),
  allowcreater: bool.label('Allow Creater'),
  isLockRecord : bool.label('is Lock Record'),
  allowCreaterObj: object({
    id: id.label('Allow Creater id'),
    username: string.label('Allow Creater username')
  }).label('allowCreaterObj'),
  isLockReviewWorkflow: bool.label('is Lock Review Workflow'),
  setreviewworkflow: bool.label('Set Review Workflow'),
  webhookEnabled: bool.label('Enable Webhook'),
  webhookUrl: bigString.label('URL for webhook'),
  webhookAuthentication: any.label('Authentication for Webhook URL'),
  reviewRequired: bool.label('Review Required'),
  workflowreview: optionalArray(object({
    anyOne: any.label('Required To Sign'),//PQT-15 Prachi
    userSelectionType: any,
    reviewstatus: number.label('Review Status'),
    roles: any,
    user: optionalArray(object({
      id: id.label('user id'),
      username: string.label('username'),
      firstname: string.label('First Name'),
      lastname: string.label('Last Name')
    })).label('user')
  })
).label('Set Workflow Review array')
.when('setreviewworkflow', {is: true, then: required()}),
allicableSites:optionalArray(
  optionalArray(object({
    _id: id.label('AllicableSites schema id'),
    title: string.label('AllicableSites schema title')
  }))
).label('Allicable sites array')
.when('addSites', {is: true, then: required()}),
// reviewRequired: bool.label('ReviewRequired?'),
attributes: optionalArray(object({
  _id: id.label('Attribute id'),
  title: string.label('Attribute title'),
  isSetDefaultValue: bool.label('Is Set Default Value'),
  setDefaultValue: any,
  addBarcodeRule: bool.label('Add Barcode Rule'),
  addBarcodeRulePrefix: any.label('Add Barcode Rule Prefix').allow(''),
  addBarcodeRuleSuffix: any.label('Add Barcode Rule Suffix').allow(''),
  autoGenerateNumberPrefix: smallString.label('Auto Generate Number Prefix').allow(''),
  autoGenerateNumberSuffix: smallString.label('Auto Generate Number Suffix').allow(''),
  's#': string.label('Sequence number'),
  version : number.label('Attribute version'),
  markSetValueInActive: bool.label('Mark Set Value InActive'),
  type: object({
    _id: id.label('Type id'),
    title: string.label('Type title'),
    format: object({
      _id: id.label('Format id'),
      title: string.label('Format title'),
      metadata: any
      //formatString: string.label('Format string')
    })
  }).label('Type object'),
  measurementUnit: object({
    enabled: bool.label('Enabled?'),
    title: string.label('Measurement unit title')
    .when('enabled', {is: true, then: required()})
  }),
  isKeyAttribute: bool.label('IsKeyAttribute?'),
  isMandatory: bool.label('IsMandatory?'),
  isSaveWithoutAccept: bool.label('IsSaveWithoutAccept?'),
  isActive: bool.label('IsActive?'),
  module: object({
    _id: id.label('Module id'),
    title: string.label('Module title')
  }).label('Module object'),
  validation: any.label('Validation')
})).label('attributes array of schema'),
keyvalue : any,
requirefordraft : any,
entities: optionalArray(object({
  _id: id.label('Entity id'),
  's#': string.label('Sequence number'),
  refrencedQuestion: any.label("referenced Questions"),
  title: extraLargeString.label('Entity title'),
  version: number.label('Entity version'),
  isExpBuild: bool.label('isExpBuild'),
  isShowQuestionAdded: bool.label('isShowQuestionAdded'),
  isInstructionForUser: bool.label('is Instrcution For Users'),
  instructionForUser: any.label('Instruction for user').allow(''),
  instructionURL: any.label('Schema instructionURL'),
  file: any.label('Schema upload file'),
  dmsfile: any.label('Schema upload file'),
  markEntityValueInActive: bool.label('Mark Entity Value InActive'),
  getInactiveRecord: bool.label('Display Inactive Record'),
  enableEntityRecord : bool.label('Enable Entity Record in Update Mode'),
  isDisplayQuestion : bool.label('IsDisplayQuestion'),
  isSetLabel : bool.label('IsSetLabel?'),
  entityLabelTitle : any,
  entityUsedInProcedureCondition: any,
  questions: array(object({
    parentId: string.label('Parent Entity Id'),
    comboId: string.label('Entity and Question Combo Id'),
    isOptional: bool.label('isOptional'),
    addBarcodeRule: bool.label('Add Barcode Rule'),
    addBarcodeRulePrefix: any.label('Add Barcode Rule Prefix').allow(''),
    addBarcodeRuleSuffix: any.label('Add Barcode Rule Suffix').allow(''),
    // _id: id.label('Question id'),
    's#': string.label('Sequence number'),
    version: number.label('Question version'),
    title: extraLargeString.label('Questions title'),
    isKeyValue: bool.label('IsKeyValue'),
    isAppearance:bool.label('IsAppearance'),
    isDisplayQuestion : bool.label('isDisplayQuestion'),
    type: object({
      _id: id.label('Type id'),
      title: string.label('Type title'),
      format: object({
        _id: id.label('Type id'),
        title: string.label('Type title'),
        metadata: any
      })
    }).label('Type object'),
    measurementUnit: object({
      enabled: bool.label('Measurement enabled?'),
      title: string.label('Measurement title')
      .when('enabled', { is: true, then: required() })
    }),
    validation: any,
    typeSelector: any.label('Type Selector'),
    linkToEntity: any.label('Link To Entity'),
    criteria: any,
    formula: any,
  })).required().label('Questions array'),
  values: any,
  conditionalWorkflows: any,
  entityFilters: any,
  filteredEntQuesSelected: any,
  filterEntityQuestionEvents : any,
  filteredEntQues: any,
  questionUsedInProcedureCondition: any,
  questionUsedInEntityFilter : any,
  questionUsedInAcceptanceConditionProcedure: any,
  questionUsedInFormula : any,
  questionUsedInAcceptance : any
})).label('Entity array'),
procedures:optionalArray(object({
  _id: id.label('Procedure id'),
  linkToOtherProcedures: any.label('Procedure Link'),
  's#': string.label('Sequence number'),
  instructionURL: any.label('Schema instructionURL'),
  file: any.label('Schema upload file'),
  dmsfile: any.label('Schema upload file'),
  referencedQuestions: any.label("referenced Questions"),
  refrencedQuestion: any.label("referenced Questions"),
  title: extraLargeString.label('Procedure title'),
  version : number.label('Procedure version'),
  conditional : bool.label('Conditional Procedure'),
  questions: array(object({
    parentId: string.label('Parent Procedure Id'),
    comboId: string.label('Procedure and Question Combo Id'),
    _id: id.label('Question id'),
    isHiddenQuestion: bool.label('isHiddenQuestion'),
    checkValidCriteria: bool.label('Check Validity For Acceptance Criteria'),
    addBarcodeRule: bool.label('Add Barcode Rule'),
    addBarcodeRulePrefix: any.label('Add Barcode Rule Prefix').allow(''),
    addBarcodeRuleSuffix: any.label('Add Barcode Rule Suffix').allow(''),
    's#': string.label('Sequence number'),
    version : number.label('Question version'),
    title: extraLargeString.label('Questions title'),
    isoptionalquestion: bool.label('isoptionalquestion'),
    type: object({
      _id: id.label('Type id'),
      title: string.label('Type title'),
      format: object({
        _id: id.label('Type id'),
        title: string.label('Type title'),
        metadata: any
        //formatString: string.label('Format string of type')
      })
    }).label('Type object'),
    measurementUnit: object({
      enabled: bool.label('Measurement enabled?'),
      title: string.label('Measurement title')
      .when('enabled', {is: true,then: required()})
    }),
    isInstructionForUser: bool.label('Is Instruction for user'),
    instructionForUser: any.label('Instruction for user').allow(''),
    validation: any,
    criteria: any,
    formula: any,
    // validations: object({
    //   type: object({
    //     _id: rString.label('Type id of validations'),
    //     value: rString.label('Value of validations')
    //   }).required().label('Type object'),
    //   field1: rString.label('Field1 of validations'),
    //   field2: string.label('Field2 of validations'),
    //   message: rString.label('Message of validations')
    // })).required().label('Validation array'),
    // formula: object({
    //    expression: stringArray.label('Expression string array'),
    //    computedExpression: rString.label('Computed expression')
    // }).required().label('Formula object'),
    // acceptanceCriteria: array(object({
    //   expressionStatements: array(object({
    //     field: rString.label('Field of ExpressionStatements'),
    //     operator: rString.label('Operator of ExpressionStatements'),
    //     value: rString.label('value of ExpressionStatements'),
    //     inclusion: rString.label('Inclusion of ExpressionStatements')
    //   })).required().label('Expression statements array'),
    //   expression: rString.label('Expression of Acceptance Criteria'),
    //   displayExpression: rString.label('DisplayExpression of Acceptance Criteria'),
    //   type: object({
    //     _id: rString.label('Type Id of Acceptance Criteria'),
    //     operator1: rString.label('Operator1 of Acceptance Criteria'),
    //     value1: rString.label('Value1 of Acceptance Criteria'),
    //     exponential1: rString.label('Exponential1 of Acceptance Criteria'),
    //     actualValue1: rString.label('ActualValue1 of Acceptance Criteria'),
    //     operator2: rString.label('Operator2 of Acceptance Criteria')
    //       .when('_id', {is:'multiple', then:required()}),
    //     value2: rString.label('Value2 of Acceptance Criteria')
    //       .when('_id', {is:'multiple', then:required()}),
    //     exponential2: rString.label('Exponential2 of Acceptance Criteria')
    //       .when('_id', {is:'multiple', then:required()}),
    //     actualValue2: rString.label('ActualValue2 of Acceptance Criteria')
    //       .when('_id', {is:'multiple', then:required()})
    //   }).required().label('Type object')
    // }).required().label('AcceptanceCriteria array')
  })).required().label('Questions array'),
  isOptionalProcedure: bool.label('IsOptionalProcedure?'),
  optionalProcedureTitle: any,
  isMultipleProcedure: bool.label('Is Multiple Procedure'),
  multipleProcedureTitle: any,
  isInstructionForUser: bool.label('Is Instruction for user'),
  instructionForUser: any.label('Instruction for user').allow(''),
  // conditionalWorkflows: array(object({
  //   conditions: array(object({
  //     question: object({
  //       _id: rId.label('Question id'),
  //       title: rString.label('Question title')
  //     }).required().label('Question object'),
  //     state: bool.label('state pass or fail')
  //   })).required().label('Conditional array'),
  //   conditionsDisplay: rString.label('Conditions display'),
  //   procedureToExpand: rId.label('ProcedureToExpand id')
  // }))
  //   conditionalWorkflows: array(object({
  //     conditions: array(object({
  //       question: object({
  //         _id: rId.label('Question id'),
  //         title: rString.label('Question title'),
  //     	  's#': string.label('Sequence number'),
  //         expectedResult: bool.label('state pass or fail')
  //       }).required().label('Question object')
  //     })).required().label('Conditional array'),
  //     procedureToExpand: object({
  // _id: rId.label('Question id'),
  //        title: rString.label('Question title'),
  //     	 's#': string.label('Sequence number')
  //     }).required().label('procedureToExpand object')
  //   }))
  conditionalWorkflows: any,
  questionUsedInAcceptance: any,
  questionUsedInFormula : any,
  questionUsedInProcedureCondition: any,
  questionUsedInAcceptanceConditionProcedure: any
})).label('Procedure array'),
acceptanceCriteria: object({
  // _id: id.label('AcceptanceCriteria id'),
  criteria: array(object({
    procedures: optionalArray(object({
      _id: id.label('Procedures id'),
      's#': string.label('Sequence number'),
      title: extraLargeString.label('Procedure title')
    })).label('Procedures array')
  })).label('Criteria array')
}).label('AcceptanceCriteria object'),
appWorkflows :any,
// acceptanceCriteria: array(object({
//   _id: rId.label('AcceptanceCriteria id'),
//   title: rString.label('AcceptanceCriteria title')
// })).required().label('AcceptanceCriteria array'),
// acceptanceCriteriaExpression: string.label('AcceptanceCriteriaExpression Title'),
createdBy: string.label('Created By'),
modifiedBy: string.label('Modified By'),
createdDate: date.label('Created Date'),
modifiedDate: date.label('Modified Date'),
isActive: bool.label('IsActive?'),
isSendEmail: bool.label('IsSendEmail?'),
isShared: bool.label('isShared?'),
emailIdForQCFormFail: extraLargeString.label('Email Collection').allow(''),
isDeleted: bool.label('Is Deleted ?'),
isPrintLinkedApp: bool.label('Is PrintLinkedApp'),
headMsgPrint: any,
headMsgPrintForPass: any,
headMsgPrintForFail: any,
isPrintFailMsg: bool.label('Is isPrintFailMsg'),
restoreStableVersion: bool.label('Is Restore Stable Version'),
restoreStableVersionAudit: object({
  previousVersion: number.label('Previous Version'),
  stableVersion: number.label('Stable Version')
}),
isDraft: bool.default(false).label('Is Draft Version'),
isAuditing: bool.label("Is Updating ?"),
timeZone: string.label('Time Zone'),
};

module.exports = schema;
