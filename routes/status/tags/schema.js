'use strict'
/**
 * @name tags-schema
 * @author shoaib ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');
//Change by amit
var rString = types.rString;

var schema = {
  _id: rString.label('Tag name string')
};

module.exports = schema;
