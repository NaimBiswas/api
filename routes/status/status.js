'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'status',
    collection: 'status',
    schema: schema,
    softSchema: softSchema
  }
});

/**
 * @swagger
 * definition:
 *   Status:
 *     properties:
 *      id:
 *        type: integer
 *      title:
 *        type: string
 *      isActive:
 *        type: boolean
 *      createdBy:
 *        type: integer
 *      modifiedBy:
 *        type: integer
 *      createdDate:
 *        type: dateTime
 *      modifiedDate:
 *        type: dateTime
 */

/**
 * @swagger
 * /status:
 *   get:
 *     tags: [Reports]
 *     description: Gives the status
 *     produces:
 *        - application/json
 *     parameters:
 *        - name: pagesize
 *          description: Page Size
 *          in: query
 *          required: false
 *          type: string
 *     responses:
 *       200:
 *         description: An array of statuses
 *         schema:
 *           $ref: '#/definitions/Status'
 */

module.exports = route.router;
