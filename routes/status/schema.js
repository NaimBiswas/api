'use strict'
/**
 * @name status-schema
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rString = types.rString;
var rBool = types.rBool;
var string = types.string;

var schema = {
  title: rString.label('Status title'),
  isActive: rBool.label('Status active?'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
