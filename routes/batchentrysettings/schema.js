'use strict'
/**
 * @name batchentrysettings - schema
 * @author surjeet bhadauriya <surjeet.b@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var array = types.array;
var rBigString = types.rBigString;
var rBool = types.rBool;
var rNumber = types.rNumber;
var number=types.number;
var string= types.string;
var bool=types.bool;
var any = types.any;

var schema = {
  batchentry: array(object({
    seqId: rNumber.label('seqId'),
    formId: rBigString.label('formId'),
    formTitle: rBigString.label('formtitle'),
    copyNonKeyAttributes: rBool.label('Copy Non Key Attributes'),
    copyNonKeyEntities: bool.label('Copy Non Key Entities Attribute'),
    copySiteSelections: rBool.label('Copy Site Selections'),
    isDefaultBatchMode: bool.label('Start App in Default Batch Mode')
  })).required().label('Batch Entry'),
  batchEntryFormat:array(object({
    name:string.label('name'),
    entryType: number.label('batchEntryFormat'),
    batchEntryFormatValue: any,
    batchCurrentValue:number.label('batchCurrentValue'),
    isSelected:bool.label('isSelected')
  })).label('batchEntryFormat'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
