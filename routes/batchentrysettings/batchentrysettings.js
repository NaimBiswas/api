'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'batchentrysettings',
    collection: 'batchentrysettings',
    schema: schema,
    softSchema: softSchema
  }
});

/**
 * @swagger
 * definition:
 *   Batchentry:
 *     properties:
 *       seqId:
 *         type: integer
 *       formId:
 *         type: integer
 *       formTitle:
 *         type: string
 *       copyNonKeyAttributes:
 *         type: boolean
 *       copySiteSelections:
 *         type: boolean
 */

/**
 * @swagger
 * definition:
 *   Batchentrysettings:
 *     properties:
 *       id:
 *         type: integer
 *       batchentry:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Batchentry'
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       createdBy:
 *         $ref: '#/definitions/CreatedBy'
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       version:
 *           type: integer
 *       isMajorVersion:
 *           type: boolean
 */

/**
 * @swagger
 * /batchentrysettings/{id}:
 *   get:
 *     tags: [Batch Entry Setting]
 *     description: Returns all batch entries
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: batchentry's id
 *         in: path
 *         required: true
 *         type: integer
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of batchentrysettings
 *         schema:
 *           $ref: '#/definitions/Batchentrysettings'
 */

 /**
  * @swagger
  * /batchentrysettings/{id}:
  *   get:
  *     tags: [Batch Entry Setting]
  *     description: Returns a object batch Entry
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: Batch Entry's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: pagesize
  *         description: Page Size
  *         in: query
  *         required: false
  *         type: string
  *     responses:
  *       200:
  *         description: An object of batch Entry
  *         schema:
  *           $ref: '#/definitions/Batchentrysettings'

  */

 /**
  * @swagger
  * /batchentrysettings/{id}:
  *   patch:
  *     tags: [Batch Entry Setting]
  *     description: Updates a Batch Entry
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: batchentry's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: batchentrysettings
  *         description: batchentrysettings object
  *         in: body
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Batchentrysettings'
  *     responses:
  *       200:
  *         description: Successfully updated
  */


module.exports = route.router;
