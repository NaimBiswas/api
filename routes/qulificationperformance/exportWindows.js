  /**
* @name Qulification Performance - get Windows for Reports
* @author Yamuna Sardhara <yamuna.s@productivet.com>
*
* @version 1.0
*/
var db = require('../../lib/db');
var config = require('../../config/config.js');
var _ = require('lodash');
var moment = require('moment-timezone');
var q = require('q');
var logger = require('../../logger');
var currentFileName = __filename;
var amqp = require('amqplib/callback_api');
var getReportWindows = require('./getReportWindows.js');
var exportfileForWindowReports = require('../exportfile/exportfileForWindowReports.js');

var MICRO = require("../../microservices/MICRO");
var qpExportPublisherInstance = MICRO.getPublisherInstance("qpExport");

function getAppName(appId, callback) {
  db.schema.findOne({_id: db.ObjectID(appId)}, {title: 1}, function(err, app) {
    if (err) {
      callback(err);
      return;
    }
    callback(null, app);
  });
}

function exportWindows(options){
  var deferred = q.defer();
  try{
    getAppName(options.apps[0], function(err, app) {
      // QC3-10904 by Yamuna
      var filename = '';
      if(options.apps && options.apps.length > 1 && (!options || (options && !options.title))){ //Email error resolve from qa
        filename = new Date().getTime() + "-QP_" + new Date().getTime() + '.' + options.exportAs;
      }else{
        filename = new Date().getTime() + "-" + (options.title ? ("QP_" + options.title) : ("QP_" + app.title)) + "_" + new Date().getTime() + '.' + options.exportAs;
      }
      db.collection('windowQueueFiles').insert({
        'title': options.title ? options.title : ((options.apps && options.apps.length > 1) ? "QP" : ("QP_" + app.title)),
        'filter': options,
        'filename': filename,
        'status': 'Pending',
        'createdDate': new Date(),
        'modifiedDate': new Date(),
        'createdByName': options.userName,
        'isGenerate': false
      }, function (error, result) {
        if (error) {
          deferred.reject(error);
          return;
        } else {

          options.savequeueId = result.ops[0]._id;
          options.filename = filename;
          options.isFrom = "Export";
          options.pageSize = 10000;
          options.pageNumber = 1;

          var msg = {data:options};
          
          qpExportPublisherInstance.call("export",msg,function patch(err,data){
            console.log("patched--------------------",err,data);
          });
          deferred.resolve(options);

          // var dd = {};
          // dd.aa.dd = "dfd";

          // amqp.connect('amqp://localhost', function (errorToConnect, conn) {
          //   if (errorToConnect) {
          //     deferred.reject(errorToConnect);
          //   } else {
          //     conn.createChannel(function (errors, ch) {
          //       if (errors) {
          //         deferred.reject(errors);
          //         return;
          //       } else {
          //         var q = config.serverName;
          //         //var msg = options.userName;
          //         options.savequeueId = result.ops[0]._id;
          //         options.filename = filename;
          //         options.isFrom = "Export";
          //         options.pageSize = 10000;
          //         options.pageNumber = 1;
          //         // cache.quesaveCache(options);
          //         ch.assertQueue(q, { durable: true });
          //         var msg = JSON.stringify({data:options});
          //         //miralhear
          //         ch.sendToQueue(q, new Buffer(msg));
          //         deferred.resolve(options);
          //       }
          //     });
          //   }
          // });
        }
      });
    });
  }catch(e){
    deferred.reject(e);
  }
  return deferred.promise;
}

function exportWindowManipulation(req) {
  var deferred = q.defer();
  if(req && req.data){
    getReportWindows.fetchReportWindows(req.data).then(function(windowRecords){
      exportfileForWindowReports.fetchExportedData(windowRecords,req.data).then(function(response){
        deferred.resolve(req.data);
      });
    })
  }else{
    deferred.reject('req not found');
  }

  return deferred.promise;
}

module.exports = {
  exportWindows: exportWindows,
  exportWindowManipulation: exportWindowManipulation
}
