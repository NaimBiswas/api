/**
* @name Qulification Performance - Search App Records From Window Id
* @author Jyotil Raval <jyotil.r@productivet.com>
* @version 1.0
*/

(function () {
  var _ = require("lodash");
  var moment = require("moment");
  var decode = require("../../lib/oauth/model").getAccessToken;
  var lzstring = require("lz-string");
  var dbConfig = require("../../lib/db/db");

  var options = {};
  options.clientTZ = "America/Chicago";

  function projectedListForAdvancedSearch(schema, entry, options) {
    var projectedEntry = {
      _id: entry._id,
      App: schema.title
    };

    if (
      !entry.modifiedByName &&
      entry.modifiedBy &&
      options &&
      options.users &&
      options.users.length
    ) {
      var userDetails = _.find(options.users || [], function(user) {
        return user._id.toString() == entry.modifiedBy;
      });
      if (userDetails) {
        entry.modifiedByName = (userDetails.firstname || "")
        .concat(" ")
        .concat(userDetails.lastname || "")
        .concat(" (")
        .concat(userDetails.username || "N/A")
        .concat(")");
      }
    }

    if (entry["BatchEntry"]) {
      projectedEntry["BatchEntry"] = entry["BatchEntry"];
    }
    projectedEntry["Batch Entry ID"] = entry["BatchEntry"]
    ? entry["BatchEntry"].title
    : "N/A";
    return projectedEntry;
  }

  /* function fetchFormBatchEntry(entries, schema, options, cb) {
    try {
      var schemaId = schema._id.toString();
      if (options && options.masterApp && options.masterApp.length) {
        schemaId = options.masterApp[0];
      }
      db
      .collection("batchEntries")
      .find({ schemaId: schemaId }, { batchEntries: 1 })
      .toArray(function(err, result) {
        (result || []).forEach(function(batchEntry) {
          _.forIn(batchEntry.batchEntries || {}, function(value, key) {
            if (value && value.length) {
              value.forEach(function(item) {
                entries.forEach(function(ent) {
                  if (item.formId === ent._id.toString()) {
                    ent["BatchEntry"] = {
                      _id: batchEntry._id,
                      title: key
                    };
                    return;
                  }
                });
              });
            }
          });
        });
        cb();
      });
    } catch (e) {
      cb();
    }
  } */

  var getPirticularFormateTitle = function (queType, queFormate, formatId, types) {
    return (_.find((((_.find(((_.find((types || []), function(type){
      return type.title == queType;
    }) || {}).formats || []), function(format){
      return format.title == queFormate;
    }) || {}).metadata || [{}])[0].allowItems || []), function(allowedItem){
      return allowedItem._id == formatId;
    }) || {}).title || '';
  }

  function displayValueByFormat(c, value, clientTZ,options) {
    var filterdDate = "";
    if (c && c.type && c.type.format && c.type.format.title === "Date") {
      filterdDate = (c.type.format.metadata.format || "MM/DD/YYYY")
      .toLocaleUpperCase()
      .replace("MONTH", "MMMM");

      if (value) {
        return moment
        .utc(new Date(value))
        .tz(clientTZ)
        .format(filterdDate);
      } else {
        return value;
      }
    } else if (value && c && c.type && c.type.format && c.type.format.title === "Time") {
      var timeFormate = getPirticularFormateTitle(c.type.title, c.type.format.title, c.type.format.metadata.datetime, options.types);
      return moment
      .utc(new Date(value))
      .tz(clientTZ)
      .format(timeFormate || "HH:mm");
    } else if (value && c && c.type && c.type.format && c.type.title === "File Attachment") {
      if(value && value.length && value.forEach){
        value.forEach(function (val) {
          val.type = c.type.title;
        });
      }
      return value;
    } else {
      return value === 0 ? "0" : value;
    }
  }

  function getStausString(app) {
    if (!(app.entries && app.entries.status)) {
      return "Draft";
    }
    var status = app.entries.status.status;
    var hasPendingWorkflowReview = false;
    if (app.workflowreview && app.workflowreview.length) {
      hasPendingWorkflowReview = _.some(app.workflowreview || [], function(
        review
      ) {
        return review.reviewstatus == 1;
      });
    }
    if (status == 3) {
      return "Void";
    } else if (status == 4) {
      return "Draft";
    } else if (hasPendingWorkflowReview && status == 1) {
      return "Review Pending - Passed";
    } else if (hasPendingWorkflowReview && status == 0) {
      return "Review Pending - Failed";
    } else if (status == 1) {
      return "Passed";
    }
    if (hasPendingWorkflowReview && status == 2) {
      return "Review Pending";
    } else {
      return "Failed";
    }
  }

  function getEntities(entities, entry, entryObject,options) {
    if (!entities || !entry) {
      return [];
    }

    return _.reduce(entities, function entityCallback(returnArray, ent) {
        var fakeEntity = {
          _id: ent._id,
          title: ent.title,
          "s#": ent["s#"],
          instructionForUser: ent.isInstructionForUser ? ent.instructionForUser : "",
          version: ent.version,
          questions: []
        };

        (ent.questions || []).forEach(function(_question) {
          var question = {
            "s#": _question["_s#"],
            title: _question.title,
            comboId: _question.comboId,
            parentId: _question.parentId,
            fullQuestion: _question,
            isKeyValue: _question.isKeyValue,
            type: _question.type?_question.type.title?_question.type.title:"":"",
            key: "entries." + _question["s#"] + "-" + ent["s#"] + "." + ent["s#"],
          };

          if (!entry.entries[_question["s#"] + "-" + ent["s#"]]) {
            question.value = "";
            return;
          } else if (_question) {
            question.value =
            displayValueByFormat(
              _question,
              entry.entries[_question["s#"] + "-" + ent["s#"]][ent["s#"]],
              options.clientTZ,
              options
            ) || "";

            if (typeof question.value === "object" && question.value.length) {
              if (question.value && question.value.length && question.value[0].type =="File Attachment") {
                question.value = question.value;
                question.type = "File Attachment";
              } else {
                question.value = question.value.join(", ");
              }
            }
          } else {
            question.value = entry.entries[_question["s#"] + "-" + ent["s#"]]
            ? entry.entries[_question["s#"] + "-" + ent["s#"]][ent["s#"]]
            : "";
            if (typeof question.value === "object" && question.value.length) {
              if (question.value && question.value.length && question.value[0].type =="File Attachment") {
                question.value = question.value[0];
                question.type = "File Attachment";
              } else {
                question.value = question.value.join(", ");
              }
            }
          }
          fakeEntity.questions.push(question);
        });
        returnArray.push(fakeEntity);
        return returnArray;
      },
      []
    );
  }

  function getAttributes(attributes, entry, entryObject, options) {
    if (!attributes || !entry) {
      return [];
    }
    return _.reduce(attributes, function entityCallback(returnArray, attribute) {
        var fakeEntity = {
          "_id": attribute._id,
          "title": attribute.title,
          "s#": attribute["s#"],
          "version": attribute.version,
          "fullQuestion": attribute,
          "parentId":attribute.parentId,
          "type": attribute.type?attribute.type.title?attribute.type.title:"":"",
          "key": "entries." + attribute["s#"] + ".attributes",
        };

        if (!entry.entries[attribute["s#"]]) {
          fakeEntity["value"] = "";
          return;
        } else if (!_.isUndefined(attribute)) {
          fakeEntity.value =
          displayValueByFormat(
            attribute,
            entry.entries[attribute["s#"]]["attributes"],
            options.clientTZ,
            options
          ) || "";
          if (typeof fakeEntity.value === "object" && fakeEntity.value.length) {
            if (fakeEntity.value && fakeEntity.value.length && fakeEntity.value[0].type =="File Attachment") {
              fakeEntity.value = fakeEntity.value[0];
              fakeEntity.type = "File Attachment";
            } else {
              fakeEntity.value = fakeEntity.value.join(", ");
            }
          }
        } else {
          fakeEntity.value = !_.isUndefined(entry.entries[attribute["s#"]])
          ? entry.entries[attribute["s#"]]["attributes"]
          : "";
          if (typeof fakeEntity.value === "object" && fakeEntity.value.length) {
            if (fakeEntity.value && fakeEntity.value.length && fakeEntity.value[0].type =="File Attachment") {
              fakeEntity.value = fakeEntity.value[0];
              fakeEntity.type = "File Attachment";
            } else {
              fakeEntity.value = fakeEntity.value.join(", ");
            }
          }
        }
        returnArray.push(fakeEntity);
        return returnArray;
      },
      []
    );
  }

  function applicableSitesContainsSites(applicableSites, sites) {
    return sites.every(function(value) {
      return applicableSites.indexOf(value) >= 0;
    });
  }

  function getApplicableSites(applicableSites) {
    return [].concat.apply([], applicableSites);
  }

  function getAuthorizedSites(req, cb) {
    var decompressToken = lzstring.decompressFromEncodedURIComponent(
      req.headers["authorization"]
    );
    var token = decompressToken;
    if (token && _.startsWith(token, "Bearer ")) {
      decode(token.substring(7), function(err, data) {
        if (err) {
          cb({ status: 403, message: "You are not authorized." }, null);
          return;
        }
        req.user = data.user || {};
        var applicableSites = getApplicableSites(req.user["applicableSites"]);
        var options = req.body || {};
        options.user = data.user;
        options.permissions = req.user["permissions"] || [];
        if (options.levelIds && options.levelIds.length) {
          options.levelIds = _.filter(options.levelIds || [], function(level) {
            return !!level;
          });

          if (options.levelIds.length != req.user["applicableSites"].length) {
            var length = req.user["applicableSites"].length - 1;
            var objKey = {};
            objKey["levelIds." + length] = {
              $in: req.user["applicableSites"][length]
            };
            options.query = objKey;
          }
          if (!applicableSitesContainsSites(applicableSites, options.levelIds)) {
            cb({ status: 403, message: "You are not authorized." }, null);
            return;
          }
        } else if (applicableSites && applicableSites.length) {
          options.applicableSites = applicableSites;
        }
        options.clientOffSet = req.headers["client-offset"] || 0;
        options.clientTZ = req.headers["client-tz"] || "America/Chicago";
        cb(null, options);
      });
    } else {
      cb({ status: 403, message: "You are not authorized." }, null);
    }
  }
  function getDistinctVersionsEntries(entries) {
    return _.uniq(
      (entries || []).map(function(entry) {
        return entry.parentVersion;
      })
    );
  }
  function dbConfigApply(collectionName){
    var entities = [];
    entities.push({
      collection: collectionName,
      audit: true,
      index: {
        title: ''
      }
    });
    dbConfig.configure.apply(this, entities);
  }

  function getSchemaVersionWise(entriesDistinctVersionList,schemaVersionArray,cb) {
    var auditQuery = {
      _id: {
        $in: getAuditlogIDs(entriesDistinctVersionList, schemaVersionArray)
      }
    };
    db["schemaAuditLogs"].find(auditQuery).toArray(function schemaAuditLogCallback(err, auditLogs) {
      if(err){
        return cb(err,null)
      }
      var schemaVersionWise = (auditLogs || []).reduce(function versionWiseLogs(returnObject, currentValue) {
        returnObject[currentValue.version] = currentValue;
        return returnObject;
      },{});
      cb(null,schemaVersionWise)
    });
  }

  function getAuditlogIDs(entriesDistinctVersionList, schemaVersionArray) {
    return (schemaVersionArray || []).reduce(function versionsCallback(idArray, currentValue) {
      if (entriesDistinctVersionList.indexOf(currentValue.version) != -1) {
        idArray.push(currentValue.record);
      }
      return idArray;
    },[]);
  }

  function getFormatedEntryObject(entry, parentSchema, options) {
    options.shortObjectSequance = 1;
    var entryObject = {
      shortInfos: {},
      _id: entry._id,
      child:entry.child,
      parentVersion: entry.parentVersion,
      latestSchemaVersion: options.latestSchemaVersion,
      createdByName: entry.createdByName?entry.createdByName:options.usersObject[entry.createdBy],
      modifiedByName: entry.modifiedByName?entry.modifiedByName:options.usersObject[entry.modifiedBy],
      createdDate: moment
      .utc(new Date(entry.createdDate))
      .tz(options.clientTZ)
      .format("MM/DD/YYYY HH:mm:ss"),
      modifiedDate: moment
      .utc(new Date(entry.modifiedDate))
      .tz(options.clientTZ)
      .format("MM/DD/YYYY HH:mm:ss"),
      levels: entry.levels,
      workflowreview: entry.workflowreview,
      verificationDate: entry.verificationDate,
      reviewStatus: entry.reviewStatus,
      batchEntryId: entry.batchEntryId,
      masterQCSettings: entry.masterQCSettings,
      schema: parentSchema
      ? {
        _id: parentSchema.identity,
        title: parentSchema.title,
        isActive: parentSchema.isActive,
        workflowreview: parentSchema.workflowreview,
        module: parentSchema.module,
        description: parentSchema.description,
        allowcreater:parentSchema.allowcreater,
        file: parentSchema.file,
        headMsgPrint: parentSchema.headMsgPrint,
        isLockRecord: parentSchema.isLockRecord,
        headMsgPrintForPass: parentSchema.headMsgPrintForPass,
        headMsgPrintForFail: parentSchema.headMsgPrintForFail,
        instructionURL: parentSchema.instructionURL,
        siteYesNo: parentSchema.siteYesNo,
      }
      : undefined
    };
    if (parentSchema) {
      addConditionalProcedures(parentSchema);
      // For solving QC3-10283 - Remove blank procedures + entities from display list - JD
      filterProceduresWithBlankValues(parentSchema, entry);

      entryObject["attributes"] = Utils.getAttributes(
        parentSchema.attributes,
        entry,
        entryObject,
        options
      );
      options.shortObjectSequance++;
      entryObject["entities"] = Utils.getEntities(
        parentSchema.entities,
        entry,
        entryObject,
        options
      );

      entryObject["procedures"] = Utils.getEntities(
        parentSchema.procedures,
        entry,
        entryObject,
        options
      );
      entryObject["keyvalues"] = Utils.getAttributes(
        parentSchema.keyvalue,
        entry,
        undefined, // QC3-10196 - Jyotil
        options // QC3-10196 - Jyotil
      );
    }
    entryObject["status"] = Utils.getStausString(entry);
    entryObject["FailMesssages"] = [];
    if (entry.entries.FailMesssageForQCForm) {
      _.forEach(entry.entries.FailMesssageForQCForm, function(value, key){
        entryObject["FailMesssages"].push(value)
      });
    }
    entryObject.shortInfos["Review Status"] = entryObject["status"];
    entryObject.shortInfos["Created Date"] = entryObject.createdDate;
    entryObject.shortInfos["Modified Date"] = entryObject.modifiedDate;
    entryObject.shortInfos["Modified By"] = entryObject.modifiedByName;
    entryObject.shortInfos["Record Status"] = entryObject.status;
    return entryObject;
  }

  module.exports = {
    dbConfigApply:dbConfigApply,
    getStausString: getStausString,
    getAttributes: getAttributes,
    getEntities: getEntities,
    projectedListForAdvancedSearch: projectedListForAdvancedSearch,
    displayValueByFormat: displayValueByFormat,
    getAuthorizedSites: getAuthorizedSites,
    getDistinctVersionsEntries: getDistinctVersionsEntries,
    getSchemaVersionWise: getSchemaVersionWise,
    getFormatedEntryObject: getFormatedEntryObject
  };

})()
