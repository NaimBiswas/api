/**
* @name Qulification Performance - get Windows for Reports
* @author Yamuna Sardhara <yamuna.s@productivet.com>
*
* @version 1.0
*/
var db = require('../../lib/db');
var config = require('../../config/config.js');
var _ = require('lodash');
var moment = require('moment-timezone');
var q = require('q');
var logger = require('../../logger');
var currentFileName = __filename;

var getData = {
  windows: require('./searchWindow.js'),
  records: require('./searchRecords.js'),
  selectedRecords: require('./searchSelectedRecords.js')
}

function fetchReportWindows(options){
  try{
    var deferred = q.defer();
    getWindows(options).then(function(windows){
      if(windows && !windows.length){
        deferred.reject("something wents wrong");
      }
      getRecords(windows, [], options).then(function(entries){
        deferred.resolve(entries);
      });
    });
    return deferred.promise;
  }catch(e){
    //something goes wrong
  }
}

function getWindows(options){
  var deferred = q.defer();
  getData.windows.fetchRecords(options, function(error, data) {
    if(error){
      deferred.reject(error);
    }
    if(data && data.length){
      deferred.resolve(data);
    }else{
      deferred.resolve([]);
    }
  });
  return deferred.promise;
}

function getRecordsCountOnly(params){
  var deferred = q.defer();
  getData.records.fetchAppRecords(params, function(error, recordsCount) {
    if(error){
      deferred.reject(error);
      return;
    }
    if(recordsCount){
      deferred.resolve(recordsCount);
    }
  });
  return deferred.promise;
}

function getRecordsCount(windows,windowsWithCounts){
  var deferred = q.defer();
  if(windows && windows.length){
    var window = windows.shift();
    if(window && window._id){
      var params = {
        id : (window._id).toString(),
        windowId: (window.ID).toString()
      }
      getRecordsCountOnly(params).then(function(recordsCountResponse){
        window.counts = recordsCountResponse;
        windowsWithCounts.push(window);
        getRecordsCount(windows, windowsWithCounts).then(function(data){
          deferred.resolve(windowsWithCounts);
        }, function(){
          deferred.resolve(windowsWithCounts);
        });
      });
    }else{
      deferred.resolve(windowsWithCounts);
    }
  }else{
    deferred.resolve(windowsWithCounts);
  }
  return deferred.promise;
}

function getWindowsWithCounts(options){
  var deferred = q.defer();
  getWindows(options).then(function(windows){
    var windowCountWithQuery = windows[windows.length - 1];
    getRecordsCount(windows,[]).then(function(windowsWithCountsResult){
      if(windowsWithCountsResult && windowsWithCountsResult.length){
        windowsWithCountsResult[windowsWithCountsResult.length] = windowCountWithQuery;
      }
      deferred.resolve(windowsWithCountsResult);
    });
  });
  return deferred.promise;
}

function getRecords(windows, windowWithRecords, optionsForSort, callback){
  var deferred = q.defer();
  if(windows && windows.length){
    var window = windows.shift();
    if(window && window._id){
      var params = {
        id : (window._id).toString(),
        windowId: (window.ID).toString()
      }
      getRecordsCountOnly(params).then(function(options){
        if (!options || (options == "No Records Found.") || (options && options.firstStage && (options.firstStage.getUnitTestedEntriesCount === 0 && (!options.secondStageInfo || (options.secondStageInfo && options.secondStageInfo.getUnitTestedEntriesCount))))) {
          if(optionsForSort.isFrom == "Export"){
            window.records = options;
            window.counts = options;
            windowWithRecords.push(window);
          }else{
            windowWithRecords.push(options);
          }
          getRecords(windows, windowWithRecords,optionsForSort).then(function(data){
            deferred.resolve(windowWithRecords);
          }, function(){
            deferred.resolve(windowWithRecords);
          });
        }else{
          var getAllRecordsForWindow = options.firstStage.getUnitTestedEntries;
          if (options.firstStage && options.firstStage.nonProcessFailureEntries && options.firstStage.nonProcessFailureEntries.length) {
            getAllRecordsForWindow = getAllRecordsForWindow.concat(options.firstStage.nonProcessFailureEntries);
          }
          if(options.secondStageInfo && options.secondStageInfo.getUnitTestedEntries && options.secondStageInfo.getUnitTestedEntries.length){
            getAllRecordsForWindow = getAllRecordsForWindow.concat(options.secondStageInfo.getUnitTestedEntries);
          }
          if (options.secondStageInfo && options.secondStageInfo.nonProcessFailureEntries && options.secondStageInfo.nonProcessFailureEntries.length) {
            getAllRecordsForWindow = getAllRecordsForWindow.concat(options.secondStageInfo.nonProcessFailureEntries);
          }
          var optionsFilter = {
            type: 'Unit Tested Records',
            from: 'Reports',
            appId: options.appId,
            recordIds: getAllRecordsForWindow,
            sort: optionsForSort.sort,
            pageSize: optionsForSort.pageSize ? optionsForSort.pageSize :10,
            pageNumber:  optionsForSort.pageNumber ? optionsForSort.pageNumber :1,
            clientOffSet: config.timeZones[config.currentTimeZoneP] ? config.timeZones[config.currentTimeZoneP] : options.timeZone,
            clientTZ: config.timeZonesP[config.currentTimeZoneP] ? config.timeZonesP[config.currentTimeZoneP] : options.timeZone
          };
          getData.selectedRecords.fetchSelectedAppRecords(params, optionsFilter, function(error, data) {
            if(error){
              deferred.reject(error);
            }else{
              if(optionsForSort.isFrom == "Export"){
                window.records = data;
                window.counts = options;
                windowWithRecords.push(window);
              }else{
                windowWithRecords.push(data);
              }
  
              getRecords(windows, windowWithRecords,optionsForSort).then(function(data){
                deferred.resolve(windowWithRecords);
              }, function(){
                deferred.resolve(windowWithRecords);
              });
            }
          });
        }
      });
    }else{
      deferred.resolve(windowWithRecords);
    }
  }else{
    deferred.resolve(windowWithRecords);
  }
  return deferred.promise;
}

module.exports = {
  fetchReportWindows: fetchReportWindows,
  getRecords: getRecords,
  getWindowsWithCounts: getWindowsWithCounts
}
