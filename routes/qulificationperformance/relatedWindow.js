/**
* @name Qulification Performance - Search App Records From Window Id
* @author Prachi Thakkar <prachi.t@productivet.com>
*
* @version 1.0
*/
var db = require('../../lib/db');
var _ = require('lodash');
var moment = require('moment-timezone');
var q = require('q');
var logger = require('../../logger');
var currentFileName = __filename;

//Return unit tested app records
function fetchRelatedWindows(params, options, callback) {
  logger.log("function: fetchRelatedWindows - start", 'info', currentFileName);
  try {
    getWindowData(params, options, function (resultWindow) {
      if (resultWindow) {
        callback(null, resultWindow);
      }else {
        callback('No Records Found.');
      }
    });
  } catch (e) {
    logger.log("function: fetchRelatedWindows - catch : " + e, 'error', currentFileName);
    callback(e);
  }
}

//Find window from specific window id
function getWindowData(params, options, callback) {
  db.windows.findOne({windowId: parseInt(params.windowId), _id: db.ObjectID(params.id)}, function(err, thisWindow) {
    if (err) {
      callback(err);
    }
    if (thisWindow) {
      createFilterForRelatedWindow(thisWindow, params, options, function(relatedWindows) {
        if (relatedWindows) {
          callback(relatedWindows);
        }else {
          callback('No Records Found.');
        }
      });
    }else {
      callback('No Records Found.');
    }
  });
}

function createFilterForRelatedWindow(thisWindow, params, options, callback) {
  try {
    var filter = createFilters(thisWindow, options);
    var query = _.cloneDeep(filter.query);
    delete filter.query;
    getAllRelatedWindow(filter, query, function (resultWindow) {
      if (resultWindow) {
        callback(resultWindow);
      }else {
        callback('No Records Found.');
      }
    });
  } catch (e) {
    logger.log("function: fetchRecords - catch : " + e, 'error', currentFileName);
    callback(e);
  }
}

function getAllRelatedWindow(windowFilter, query, callback) {
  logger.log("function: getWindowData - start", 'info', currentFileName);
  var columnFilter = {'title':1, 'windowId':1, 'app':1, 'sampleSize':1, 'createdDate':1, 'filteredAttributes':1,
  'windowStartDate':1, 'maximumAllowedProcessFailure':1, 'allowSecondStage':1, 'additionalTestSecondStage':1,
  'windowStatus':1, 'testType':1, 'testTypeOption':1, 'windowType':1, 'windowPlan':1, 'populationSize':1, 'timeZone':1};
  db.windows.count(windowFilter, function(fail, count) {
    if (fail) {
      callback(fail);
      return;
    } else {
      if (count > 0) {
        db.windows.find(windowFilter, columnFilter).sort(query.sort).skip(query.skip).limit(query.limit)
        .toArray(function (err, windows) {
          if (err) {
            logger.log(err + ' : Error');
            callback(err);
          }
          if (windows && windows.length) {
            getProjection(windows, function(updatedWindow) {
              updatedWindow[updatedWindow.length] = {
                allWindows: count,
                query: query
              };
              callback(updatedWindow);
            });
          }else {
            callback({'data': 'No Records Found.'});
          }
        });
      }else {
        callback({'data': 'No Records Found.'});
      }
    }
  });
}
//Get window using folloeing conventions
function getProjection(windows, callback) {
  getLatestAppTitle(windows, function(filteredApp) {
    var getUpdatedWindows = windows.reduce(function(c, v) {
      if (v.filteredAttributes) {
        var attributeIds = [];
        var timeZone = v.timeZone;
        var filteredAttributes = v.filteredAttributes.reduce(function(c, v){
          v.value = (v.type === 'Date') ? moment(v.value).tz(timeZone).format('MM/DD/YYYY') : v.value; //QC3-10962 Prachi
          if(_.indexOf(attributeIds,v.attribute['s#']) == -1){
            c.push({[v.attribute.title]:[v.value]});
            attributeIds.push(v.attribute['s#']);
          }else{
            // QC3-10953 by Yamuna
            var objectToPush = c.filter(function(a){
              return (Object.keys(a) && Object.keys(a)[0]) == v.attribute.title;
            });
            objectToPush[0][v.attribute.title].push(v.value);
          }
          return c;
        }, []);
      }
      //QC3-10438 Prachi
      if (v.testType && (v.testType['s#'] || v.testType['comboId']) && (filteredApp[v.testType['comboId']] || filteredApp[v.testType['s#']])) {
        v.testType.title = v.testType['comboId'] ? filteredApp[v.testType['comboId']] : filteredApp[v.testType['s#']];
      }
      c.push({
        '_id': v._id,
        'ID': v.windowId,
        'title': v.title,
        'App': filteredApp[v.app._id],
        'Test Type': v.testType,
        'Sample Size': v.sampleSize,
        'Window Status': v.windowStatus.title,
        'Window Created Date': new Date(v.createdDate),
        'Window Start Date': new Date(v.windowStartDate),
        'Filter': filteredAttributes ? filteredAttributes : [],
        'maximumAllowedProcessFailure' : v.maximumAllowedProcessFailure,
        'additionalTestSecondStage' : v.allowSecondStage ? v.additionalTestSecondStage : '0',
        'allowSecondStage': v.allowSecondStage,
        'windowType': v.windowType ? v.windowType : '',
        'windowPlan': v.windowPlan ? v.windowPlan : '',
        'populationSize': v.populationSize ? v.populationSize : ''
      });
      return c;
    }, []);
    callback(getUpdatedWindows);
  });
}

//QC3-10384 Prachi
function getLatestAppTitle(windows, callback) {
  //For now we have single apps multiple window, if we have multiple apps multiple window then we need to do such process
  var getAllAppId = windows.map(function(w) {
    return w.app._id.toString();
  });
  //_.uniq all appId, when multiple app selection comes in picture need to use $in query in app query
  if (getAllAppId && getAllAppId.length) {
    var filter = {};
    filter['_id'] = { '$in': (_.uniqWith(getAllAppId, _.isEqual)).map(function (appId) { return db.ObjectID(appId); }) };
    db.schema.find(filter, {'title':1, 'procedures': 1}).toArray(function(err, latestApp) {
      if (err) {
        callback(err);
      }
      var filteredApp = {};
      windows.forEach(function(oneWindow) {
        latestApp.forEach(function(app) {
          if (oneWindow.app._id == app._id) {
            filteredApp[oneWindow.app._id] = app.title;
            //QC3-10438 Prachi
            if (oneWindow.testTypeOption == 'Procedure') {
              var getProc = _.find(app.procedures, {'s#' : oneWindow.testType['s#']});
              filteredApp[oneWindow.testType['s#']] = getProc.title;
            }else if (oneWindow.testTypeOption == 'Question') {
              var getProcForQuestion = _.find(app.procedures, {'s#' : oneWindow.testType['parentId']});
              if (getProcForQuestion && getProcForQuestion.questions) {
                if (oneWindow.testType['comboId']) {
                  var getQuestion = _.find(getProcForQuestion.questions, {'comboId' : oneWindow.testType['comboId']});
                  filteredApp[oneWindow.testType['comboId']] = (getQuestion && getQuestion.title) ? getQuestion.title : oneWindow.testType.title;
                }else {
                  var getQuestionWithoutComboId = _.find(getProcForQuestion.questions, {'s#' : oneWindow.testType['s#']});
                  filteredApp[oneWindow.testType['s#']] = (getQuestionWithoutComboId && getQuestionWithoutComboId.title) ? getQuestionWithoutComboId.title : oneWindow.testType.title;
                }
              }else {
                filteredApp[oneWindow.testType['comboId']] = oneWindow.testType.title;
              }
            }
          }
        });
      });
      callback(filteredApp);
    });
  }else {
    callback(windows[0].app.title);
  }
}

//Create query for array of attributes
function windowAttributeFilter(attributes) {
  logger.log("function: windowAttributeFilter - start", 'info', currentFileName);
  var getFilteredAttributes = [];
  var groupedAttributes = _.groupBy(attributes, 'keys') || {};
  _.forIn(groupedAttributes, function (group) {
    var matchedList = {'$or':[]};
    group.forEach(function (filter) {
      var attributeFilter = {};
      attributeFilter['filteredAttributes.value'] = parse(filter.type, filter.value);
      matchedList['$or'].push(attributeFilter);
      if (filter.value && new Date(filter.value) != 'Invalid Date' && isNaN(Number(filter.value))) {
        var dateFilter = {};
        dateFilter['filteredAttributes.value'] = filter.value;
        matchedList['$or'].push(dateFilter);
        matchedList['$or'].push({['filteredAttributes.value']: (new Date(filter.value)).toISOString()}); //mahammad
      }else if(typeof filter.value !== "number" && parseFloat(filter.value)){
        var numberFilter = {};
        numberFilter['filteredAttributes.value'] = parseFloat(filter.value);
        matchedList['$or'].push(numberFilter);
      } else{
        var stringFilter = {};
        stringFilter['filteredAttributes.value'] = filter.value.toString();
        matchedList['$or'].push(stringFilter);
      }
    });
    if (matchedList['$or'].length) {
      getFilteredAttributes.push(matchedList);
    }
  });
  return getFilteredAttributes;
}

//Test Type: App contains question and that quaestion contains acceptance criteria - filter for test type question
function windowTestTypeFilter(type, testType) {
  var filter = {};
  if (type == 'Question') {
    filter['$or'] = [];
    if (testType && testType['s#'] && testType['parentId']) {
      filter['$or'].push({'testType.s#': testType['s#'], 'testType.parentId': testType['parentId']});
      filter['$or'].push({'testType.s#': testType['parentId']});
    }
  }else if (type == 'Procedure') {
    if (testType && testType['s#']) {
      filter = ({'testType.s#': testType['s#']});
    }
  }
  return filter;
}

//Create filter query for search window
function createFilters(thisWindow, options) {
  var filter = {};
  if (thisWindow && thisWindow.app) {
    filter['app._id'] = thisWindow.app._id.toString();
  }

  filter['$and'] = [];

  if (thisWindow.windowId) {
    filter['$and'].push({'windowId': {$ne: thisWindow.windowId}})
  }

  if (thisWindow.filteredAttributes && thisWindow.filteredAttributes.length) {
    var filteredAttributes = windowAttributeFilter(thisWindow.filteredAttributes);
    if (filteredAttributes && filteredAttributes.length) {
      filteredAttributes.forEach(function(attrib){
        filter['$and'].push(attrib);
      });
    }
  }
  if (thisWindow.testType && thisWindow.testType['s#']) { //QC3-10956	Prachi (Wrong related window display)
    filter['$and'].push(windowTestTypeFilter(thisWindow.testTypeOption, thisWindow.testType));
  }
  if (thisWindow.windowStatus) {
    filter['$and'].push({'windowStatus.title': thisWindow.windowStatus.title});
  }
  if (thisWindow.windowPeriod.month && thisWindow.windowPeriod.year) {
    filter['$and'].push(generateMonthYearFilterScript(thisWindow.windowPeriod.month, thisWindow.windowPeriod.year));
  }
  if (thisWindow.applicableSites && thisWindow.applicableSites.length) {
    filter['$where'] = JSON.stringify(thisWindow.applicableSites).replace(/\s/g,'').concat('.some(function(x) {return this.allicableSites[this.allicableSites.length-1].find(function(site){return site._id == x})}, this)');
  }

  filter.query = {
    sort : options.sort ? options.sort : { '_id': -1 },
    pageSize : parseInt(options.pageSize || 10),
    pageNumber : parseInt(options.pageNumber || 1),
    skip : options.pageSize * (options.pageNumber - 1),
    limit : parseInt(options.pageSize),
    clientOffSet : options.clientOffSet,
    clientTZ : options.clientTZ
  };
  return filter;
}

//Create date filter for search window (whare date should be window start date)
function generateMonthYearFilterScript(month, year) {
  logger.log("function: generateMonthYearFilterScript - start" , 'info', currentFileName);
  if (month && year) {
    return ({'windowPeriod.month': month, 'windowPeriod.year': year});
  }
}

//Parse date, number, flot for sppropriate search data
function parse(type, value) {
  logger.log("function: parse - start" , 'info', currentFileName);
  type = (type||'').toLowerCase();
  if (type == 'array' || type == 'dstring') {
    var keyArray = [];
    var valArray = value.split(',');
  }
  switch (type) {
    case 'date':
    return new Date(value);
    case 'regex':
    return { $regex: new RegExp(value.toLowerCase(), 'i') };
    case 'number':
    case 'exponential':
    case 'percentage':
    case 'currency':
    case 'scientific':
    if (/^[0-9\-\+]*$/.test(value) == true) {
      value = parseInt(value);
    }
    if (/^[0-9 \.\-\+]*$/.test(value) == true) {
      value = parseFloat(value);
    }
    return value;
    case 'array':
    if (typeof value == 'string' && value.length) {
      _.forEach(valArray, function (chkvalue) {
        var isNotANumber = false;
        //Either for string or int
        keyArray.push(chkvalue);
        if (/^[0-9\-\+]*$/.test(chkvalue) == true) {
          chkvalue = parseInt(chkvalue);
          isNotANumber = true;
        }
        if (/^[0-9 \.\-\+]*$/.test(chkvalue) == true) {
          chkvalue = parseFloat(chkvalue);
          isNotANumber = false;
        }
        if (/^[0-9e\.\-\+]*$/.test(chkvalue) == true) {
          chkvalue = Number(chkvalue);
          isNotANumber = false;
        }
        if (chkvalue.length == 24 && db.ObjectID.isValid(chkvalue)) {
          chkvalue = db.ObjectID(chkvalue);
          isNotANumber = false;
        }
        if (isNotANumber) {
          keyArray.push(new RegExp('.*' + chkvalue + '.*', 'i'));
        } else {
          keyArray.push(chkvalue);
        }
      });
    }
    return keyArray;
    case 'dstring':
    _.forEach(valArray , function(chkvalue){
      keyArray.push(chkvalue);
    });
    return keyArray ;
    default:
    return value;
  }
}

module.exports = {
  fetchRelatedWindows: fetchRelatedWindows
}
