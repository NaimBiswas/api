/**
* @name Qulification Performance - Assign Failure For Window Records
* @author Prachi Thakkar <prachi.t@productivet.com>
*
* @version 1.0
*/
var db = require('../../lib/db');
var _ = require('lodash');
var moment = require('moment-timezone');
var q = require('q');
var logger = require('../../logger');
var currentFileName = __filename;
var getRecords = require("./searchRecords.js");
var mailer = require('../../lib/mailer');
var formattedModelForEmail = require("./formattedModelForEmail.js");

//Assign Failure for app entry
function fetchAppRecordForFailureAssignment(options, callback) {
  logger.log("function: fetchAppRecordForFailureAssignment - start", 'info', currentFileName);
  try {
    //Update entry object: failure assign to entry (process/non-process)
    updateRecordFailureAssignment(options, function (returnUpdatedRecord) {
      if (returnUpdatedRecord) {
        //After entry update: If condition satisfied than FAIL window
        updateWindowStatusForAllWindows(options).then(function(result) {
          callback(null, returnUpdatedRecord);
        });
      }else {
        callback('Something went wrong, please try again or contact administrator.');
      }
    });
  } catch (e) {
    logger.log("function: fetchAppRecordForFailureAssignment - catch : " + e, 'error', currentFileName);
    callback(e);
  }
}

//Update entry object: failure assign to entry (process/non-process)
function updateRecordFailureAssignment(dataForRecordUpdate, callback) {
  try {
    if (dataForRecordUpdate.windowDetails && dataForRecordUpdate.windowDetails.length && dataForRecordUpdate.failureType && dataForRecordUpdate.recordId && dataForRecordUpdate.appId) {
      var appId = dataForRecordUpdate.appId + '';
      db.collection(appId).findOne({"_id" : db.ObjectID(dataForRecordUpdate.recordId)}, function(err, appEntry) {
        var updateModel = appEntry.failure || {};
        //updateModel['windowIdForMessageInEntryForm'] = dataForRecordUpdate.windowId;
        dataForRecordUpdate.windowDetails.forEach(function(windowDetail) {
          if (windowDetail['_id']) {
            updateModel[windowDetail['_id']] = {
              isSecondStage: windowDetail.allowSecondStage ? true : false,
              type: (dataForRecordUpdate.failureType == 1) ? 'Process' : 'Non Process',
              comment: dataForRecordUpdate.failureComment ? dataForRecordUpdate.failureComment : '',
              app: {
                _id: (windowDetail.app && windowDetail.app._id) ? windowDetail.app._id : '',
                title: (windowDetail.app && windowDetail.app.title) ? windowDetail.app.title : windowDetail['App']
              },
              window: {
                _id: windowDetail._id,
                windowId: windowDetail.windowId ? windowDetail.windowId : windowDetail['ID'],
                title: windowDetail.title,
                windowTestType: (windowDetail.testType && windowDetail.testType.title) ? windowDetail.testType.title : windowDetail['Test Type']
              }
            };
          }
        });
        //Update entry
        if (updateModel && Object.keys(updateModel).length) {
          db.collection(appId).updateOne({ "_id" : db.ObjectID(dataForRecordUpdate.recordId) }, { $set: { failure: updateModel } }, function(err, update) {
            if (err) {
              callback(err);
            }
            callback({success: 'Record updated successfully'});
          });
        }else {
          callback('Something went wrong, please try again or contact administrator.');
        }
      });
    }else {
      callback('Something went wrong, please try again or contact administrator.');
    }
  } catch (e) {
    callback(e);
  }
}

//After entry update: If condition satisfied than FAIL window (Recursive q service)
function updateWindowStatusForAllWindows(dataForRecordUpdate) {
  var deferred = q.defer();
  if(dataForRecordUpdate.windowDetails.length > 0){
    var window = dataForRecordUpdate.windowDetails.shift();
    //QC3-10389 : Add status condition - Prachi
    if (!window.allWindows && ((window['Window Status'] || (window.windowStatus && window.windowStatus.title)) && ((window['Window Status'] ? window['Window Status'] : window.windowStatus.title) == 'Current'))) {
      var params = {
        windowId: window['ID'] ? window['ID'] : window.windowId,
        id: window._id
      }
      //Parameter created for accessing ./searchRecords.js
      accessSearchRecordForGettingCount(params, window, dataForRecordUpdate).then(function(filter) {
        //After entry update: If condition satisfied than FAIL window (Recursive q service)
        updateWindowStatusForAllWindows(dataForRecordUpdate).then(function(data){
          deferred.resolve(true);
        }, function(e){
          deferred.resolve(true);
        });
      });
    }else {
      //After entry update: If condition satisfied than FAIL window (Recursive q service)
      updateWindowStatusForAllWindows(dataForRecordUpdate).then(function(data){
        deferred.resolve(true);
      }, function(e){
        deferred.resolve(true);
      });
    }
  }else {
    deferred.resolve(true);
  }
  return deferred.promise;
}

//Parameter created for accessing ./searchRecords.js
//Get process fail count compare with maximul failure limit and proceed
function accessSearchRecordForGettingCount(params, window, dataForRecordUpdate) {
  var deferred = q.defer();
  getRecords.fetchAppRecords(params, function(error, data) {
    if (error) {
      deferred.resolve(error);
    }
    if (data == "No Records Found.") {//QC3-10406 Prachi
      deferred.resolve('No Updates');
    }else {
      if (data.firstStage && data.firstStage.processFailure) {
        data.firstStage.processFailure = _.uniqWith(data.firstStage.processFailure, _.isEqual);
      }
      if (((!data.secondStage && (data.firstStage.processFailure == (window.maximumAllowedProcessFailure + 1))) ||
      (window.allowSecondStage && !data.secondStage && (data.firstStage.processFailure == (window.maximumAllowedProcessFailure + 2))))
      && (window.windowStatus && window.windowStatus.title == "Current")) {
        //Update window status FAIL or update window with First Stage Information
        windowFailureAction(window, data.firstStage, dataForRecordUpdate).then(function(success) {
          deferred.resolve(true);
        }, function(e){
          deferred.resolve(true);
        });
      }else if (data.secondStage  && (window.windowStatus && window.windowStatus.title == "Current")) {
        //Update window status FAIL or update window with First Stage Information
        windowFailureAction(window, data, dataForRecordUpdate).then(function(success) {
          deferred.resolve(true);
        }, function(e){
          deferred.resolve(true);
        });
      }else {
        deferred.resolve('No Updates');
      }
    }
  });
  return deferred.promise;
}

//Update window status FAIL or update window with First Stage Information
function windowFailureAction(window, unitTestCount, dataForRecordUpdate) {
  var deferred = q.defer();
  if ((unitTestCount.processFailure || unitTestCount.processFailure == '0') && (unitTestCount.nonProcessFailure || unitTestCount.nonProcessFailure == '0') && (unitTestCount.getUnitTestedEntriesCount || unitTestCount.getUnitTestedEntriesCount == '0')) {
    var countOfUnitTestedRecords = unitTestCount.nonProcessFailure + unitTestCount.getUnitTestedEntriesCount;
  }
  if (unitTestCount.secondStage && unitTestCount.secondStageInfo && unitTestCount.secondStageInfo.processFailure && unitTestCount.secondStageInfo.processFailure == 1) {
    db.windows.findAndModify({_id: db.ObjectID(window._id)}, {},{$set: { "windowStatus": {"id" : 4,"title" : "Failed"} ,
     "secondStageInformation": unitTestCount.secondStageInfo,"firstStageInformation": unitTestCount.firstStage,
     "modifiedDate": new Date(), "modifiedByName": dataForRecordUpdate.username, "modifiedBy": dataForRecordUpdate.userId}},
    {new: true}, function(err, updatedWindow) {
      if (err) {
        deferred.resolve(err);
      } 
      db.windows.auditThis1(updatedWindow.value, dataForRecordUpdate.ip);
      formattedModelForEmail.getFormattedModel(updatedWindow.value).then(function(modelForEmail){
        modelForEmail.templateType = "failWindow";
        notify(modelForEmail,function(err,result){
          if(err){
            logger.log("call:notify error : " + err , 'error', currentFileName);
            return;
          }
          logger.log("call:notify res" , 'info', currentFileName);
        });
      });
      deferred.resolve('Window Fail Successfully.');
     });
    } else if ((!window.allowSecondStage) || (window.allowSecondStage && (unitTestCount.processFailure == (window.maximumAllowedProcessFailure + 2)))) {
      db.windows.findAndModify({_id: db.ObjectID(window._id)}, {},{ $set: { "windowStatus": {"id" : 4,"title" : "Failed" },
      "firstStageInformation": unitTestCount ,
      "modifiedDate": new Date(), "modifiedByName": dataForRecordUpdate.username, "modifiedBy": dataForRecordUpdate.userId }},
      {new: true}, function(err, updatedWindow) {
        if (err) {
          deferred.resolve(err);
        }
        db.windows.auditThis1(updatedWindow.value, dataForRecordUpdate.ip);
        formattedModelForEmail.getFormattedModel(updatedWindow.value).then(function(modelForEmail){
          modelForEmail.templateType = "failWindow";
          notify(modelForEmail,function(err,result){
            if(err){
              logger.log("call:notify error : " + err , 'error', currentFileName);
              return;
            }
            logger.log("call:notify res" , 'info', currentFileName);
          });
        });
        deferred.resolve('Window Fail Successfully.');
      });
    }else if(unitTestCount && unitTestCount.pendingFailure == 0){
      db.windows.updateOne({ "_id" : db.ObjectID(window._id) }, { $set: { "secondStageStartWithCount": countOfUnitTestedRecords } }, function(err, update) {
        if (err) {
          deferred.resolve(err);
        }
        deferred.resolve('Window Updated with second stage information Successfully.');
      });
    }else {
      deferred.resolve(true);
    }
    return deferred.promise;
  }

    function notify(entity,cb){
      mailer.send('qp-notify-common', entity, entity.emailAddresses, function sendMailCallback(err, value) {
        if (err) {
          cb(err, entity);
        } else {
          logger.log('Email has been sent to ' + entity.emailAddresses);
          entity.fileInfo = [];
          delete entity.fileInfo;
          cb(null, entity);
        }
      });
    }

    module.exports = {
      fetchAppRecordForFailureAssignment: fetchAppRecordForFailureAssignment
    }
