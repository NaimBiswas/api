/**
* @name Qulification Performance - Search App Records From Window Id
* @author Prachi Thakkar <prachi.t@productivet.com>
*
* @version 1.0
*/
var db = require('../../lib/db');
var _ = require('lodash');
var moment = require('moment-timezone');
var q = require('q');
var logger = require('../../logger');
var currentFileName = __filename;
var mailer = require('../../lib/mailer');
var formattedModelForEmail = require("./formattedModelForEmail.js");
var ip = require('ip');

//Return unit tested app records
function fetchAppRecords(params, callback) {
  logger.log("function: fetchAppRecords - start", 'info', currentFileName);
  try {
    getWindowData(params, function (resultWindow) {
      if (resultWindow) {
        callback(null, resultWindow);
      }else {
        callback('No Records Found.');
      }
    });
  } catch (e) {
    logger.log("function: fetchAppRecords - catch : " + e, 'error', currentFileName);
    callback(e);
  }
}

//Find window from specific window id
function getWindowData(params, callback) {
  db.windows.findOne({windowId: parseInt(params.windowId), _id: db.ObjectID(params.id)}, function(err, thisWindow) {
    if (err) {
      callback(err);
      return;
    }
    if (thisWindow) {
      getAppRecordsFromThisWindow(thisWindow, function functionName(appRecords) {
        callback(appRecords);
      });
    }else {
      callback('No Records Found.');
    }
  });
}

function goAndUpdateWindowStatus(response, thisWindow, isWindowReadyForUpdate, isWindowReadyForFail, callback) {
  if (isWindowReadyForUpdate && !isWindowReadyForFail) {//QC3-10971 Prachi
    var countOfUnitTestedRecords = response.firstStage.nonProcessFailure + response.firstStage.getUnitTestedEntriesCount;
    db.windows.updateOne({ "_id" : db.ObjectID(thisWindow._id) },
     { $set: { "secondStageStartWithCount": countOfUnitTestedRecords } }, function(err, update) {
      if (err) {
        callback(err);
        return;
      }
      callback(null, 'Success');
    });
  }else {
    //Working as it was before, only for Fail Status
    db.windows.findAndModify({_id: db.ObjectID(thisWindow._id)}, {},{ $set: { "windowStatus": {"id" : 4,"title" : "Failed" },
    "firstStageInformation": response.firstStage}},
    {new: true}, function(err, updatedWindow) {
      if (err) {
        callback(err);
        return;
      }
      callback(null, updatedWindow);
    });
  }
}

//Get appid from window and get all the app records using app id
function getAppRecordsFromThisWindow(thisWindow, callback) {
  var appId = thisWindow.app._id + '';
  if (appId) {
    db.schema.findOne({_id: db.ObjectID(appId)}, function(err, schema) {
      if (err) {
        callback(err);
        return;
      }
      var recordFilter = createFilterForAppRecords(thisWindow, schema);
      db.collection(appId).find(recordFilter).toArray(function (err, appEntries) {
        if (err) {
          callback(err);
          return;
        }
        if (thisWindow && schema && appEntries) {
          var getAppEntries = [];
          if (thisWindow.testTypeOption == 'Question') {
            getAppEntries = getEntryHavingTestTypeValue(thisWindow, schema, appEntries);
          }else {
            getAppEntries = getEntryHavingTestTypeValueForProcedure(thisWindow, schema, appEntries);
          }
          if (getAppEntries && getAppEntries.length) {
            getEntriesAsTestTypeCriteria(getAppEntries, schema, thisWindow, function functionName(response) {
              if (response && _.size(response) > 0) {
                //QC3-10751  Prachi - Start (Update Window Status)
                if (response.firstStage && response.firstStage.voidQC) {
                  checkVoidRecordForExistingWindow(response.firstStage.voidQC, schema, function(err, previousEntryDetails) {
                    if (err) {
                      callback(err);
                      return;
                    }
                    response.firstStage.voidQC = previousEntryDetails;
                    response.firstStage.voidQCEntries = previousEntryDetails.length;
                    //QC3-10960 Prachi - remove check for maximum define (maximumAllowedProcessFailure = 0 Problem causes)
                    var isWindowReadyForFail = false;
                    var isWindowReadyForUpdate = false;
                    //QC3-10971 Prachi
                    if ((!thisWindow.allowSecondStage && (response.firstStage.processFailure > thisWindow.maximumAllowedProcessFailure)) ||
                    (thisWindow.allowSecondStage && !response.secondStage && (response.firstStage.processFailure > thisWindow.maximumAllowedProcessFailure + 1)) ||
                    (thisWindow.allowSecondStage && response.secondStage && (response.secondStageInfo.processFailure > 0))) {
                      isWindowReadyForFail = true;
                    }
                    if (thisWindow.allowSecondStage && ((response.firstStage.processFailure > thisWindow.maximumAllowedProcessFailure) && (thisWindow.allowSecondStage && response.firstStage.pendingFailure == 0))) { //QC3-11069 Prachi (pendingFailure condition added)
                      isWindowReadyForUpdate = true;
                    }
                    if (response.firstStage && response.firstStage.processFailure && (isWindowReadyForFail || isWindowReadyForUpdate) && thisWindow.windowStatus.title == "Current") {
                      goAndUpdateWindowStatus(response, thisWindow, isWindowReadyForUpdate, isWindowReadyForFail, function(err, updatedWindow) {
                        if (err) {
                          callback(err);
                          return;
                        }
                        if (updatedWindow && updatedWindow.value && !isWindowReadyForUpdate) {
                          db.windows.auditThis1(updatedWindow.value, ip.address());
                          sendEmailForFailWindow(updatedWindow);
                          callback(response);
                        }else {
                          callback(response);
                        }
                      });
                      //Update Window Status - End(QC3-10751)
                    }else {
                      callback(response);
                    }
                  });
                }else {
                  var isWindowReadyForFail = false;
                  var isWindowReadyForUpdate = false;
                  //QC3-10971 Prachi
                  if ((!thisWindow.allowSecondStage && (response.firstStage.processFailure > thisWindow.maximumAllowedProcessFailure)) ||
                  (thisWindow.allowSecondStage && !response.secondStage && (response.firstStage.processFailure > thisWindow.maximumAllowedProcessFailure + 1)) ||
                  (thisWindow.allowSecondStage && response.secondStage && (response.secondStageInfo.processFailure > 0))) {
                    isWindowReadyForFail = true;
                  }
                  if (thisWindow.allowSecondStage && ((response.firstStage.processFailure > thisWindow.maximumAllowedProcessFailure) && (thisWindow.allowSecondStage && response.firstStage.pendingFailure == 0))) { //QC3-11069 Prachi (pendingFailure condition added)
                    isWindowReadyForUpdate = true;
                  }
                  if (response.firstStage && response.firstStage.processFailure && (isWindowReadyForFail || isWindowReadyForUpdate) && thisWindow.windowStatus.title == "Current") {
                    goAndUpdateWindowStatus(response, thisWindow, isWindowReadyForUpdate, isWindowReadyForFail, function(err, updatedWindow) {
                      if (err) {
                        callback(err);
                        return;
                      }
                      if (updatedWindow && updatedWindow.value  && !isWindowReadyForUpdate) {
                        db.windows.auditThis1(updatedWindow.value, ip.address());
                        sendEmailForFailWindow(updatedWindow);
                        callback(response);
                      }else {
                        callback(response);
                      }
                    });
                    //Update Window Status - End(QC3-10751)
                  }else {
                    callback(response);
                  }
                }
              }else {
                callback('No Records Found.');
              }
            });
          }else {
            callback('No Records Found.');
          }
        }else {
          callback('No Records Found.');
        }
      });
    });
  }else {
    callback('No Records Found.');
  }
}

//QC3-10898 Prachi - Void Records are display in Void QC section even that records is s already added to the window
function checkVoidRecordForExistingWindow(voidQcEntries, app, callback) {
  var getDataForUpdate = voidQcEntries.reduce(function(returnData, entry) {
    if ((entry.versions.length > 1) && (entry.versions[entry.versions.length - 2])) {
      entry.versions[entry.versions.length - 2]['entryId'] = entry._id;
      returnData.push(entry.versions[entry.versions.length - 2]);
    }
    return returnData;
  }, []);
  if (getDataForUpdate && getDataForUpdate.length) {
    var auditCollection = (app._id).toString() + 'AuditLogs';
    var queryForVoid = {['$or']: []};
    getDataForUpdate.forEach(function(data) {
      queryForVoid['$or'].push({'_id': db.ObjectID(data.record), 'version': data.version, 'identity': db.ObjectID(data.entryId)});
    });
    db.collection(auditCollection).find(queryForVoid).toArray(function (err, entries) {
      if (err) {
        callback(err);
        return;
      }
      entries.forEach(function(e) {
        var recordStatus = getAppRecordStatus(e);
        if (recordStatus != "Passed" && recordStatus != "Failed") { //QC3-10952 Prachi
          var removeIndex = _.findIndex(voidQcEntries, {'_id': e.identity});
          if (removeIndex || removeIndex == 0) {
            return voidQcEntries.splice(removeIndex, 1);
          }
        }
      });
      callback(null, voidQcEntries); //QC3-10900 Prachi
    });
  }else {
    callback(null, voidQcEntries); // QC3-10928 / QC3-10939 Prachi
  }
}

function sendEmailForFailWindow(updatedWindow) {
  formattedModelForEmail.getFormattedModel(updatedWindow.value).then(function(modelForEmail){
    modelForEmail.templateType = "failWindow";
    notify(modelForEmail,function(err,result){
      if(err){
        logger.log("call:notify error : " + err , 'error', currentFileName);
        return;
      }
      logger.log("call:notify res" , 'info', currentFileName);
    });
  });
}

function notify(entity,cb){
  mailer.send('qp-notify-common', entity, entity.emailAddresses, function sendMailCallback(err, value) {
    if (err) {
      cb(err, entity);
    } else {
      logger.log('Email has been sent to ' + entity.emailAddresses);
      entity.fileInfo = [];
      delete entity.fileInfo;
      cb(null, entity);
    }
  });
}

//Get actual app record status
function getAppRecordStatus(entry) {
  logger.log("function getAppRecordStatus(entry) :Called" , 'info', currentFileName);

  if (!(entry.entries && entry.entries.status)) {
    return "Draft";
  }
  var status = (entry.entries && entry.entries.status) ? entry.entries.status.status : 4;
  var hasPendingWorkflowReview = false;
  if (entry.workflowreview && entry.workflowreview.length) {
    hasPendingWorkflowReview = _.some((entry.workflowreview||[]), function (review) {
      return review.reviewstatus == 1
    });
  }
  if (status==3) {
    return 'Void';
  } else if (status == 4) {
    return 'Draft';
  } else if (hasPendingWorkflowReview && status == 1) {
    return 'Review Pending - Passed';
  } else if (hasPendingWorkflowReview && status == 0) {
    return 'Review Pending - Failed';
  } else if (status == 1) {
    return 'Passed';
  } else {
    return 'Failed'
  }
}

//Selected window test type contains Pass or Fail result
function getEntriesAsTestTypeCriteria(allEntry, schema, windowData, callback) {
  var countForStartingSecondStage = 0;
  var getTestTypeBasedEntries = [];
  var getFailEntries = [];
  var getProcessFailEntries = [];
  var getNonProcessFailEntries = [];
  var getVoidRecord = [];

  var getSecondStageUnitTestedRecord = [];
  var getSecondStagePendingFailEntries = [];
  var getSecondStageProcessFailEntries = [];
  var getSecondStageNonProcessFailEntries = [];
  var getSecondStageVoidRecord = [];
  var countDetails = {};

  //QC3-10677 Prachi - Saved First & Second Stage Information - Refector for saved First Stage / Second Stage Saved Information
  if (windowData.firstStageInformation && windowData.secondStageInformation && windowData.windowStatus.title != "Current") {
    getTestTypeBasedEntries = windowData.firstStageInformation.getUnitTestedEntries;
    getFailEntries = windowData.firstStageInformation.pendingFailureEntries;
    getNonProcessFailEntries = windowData.firstStageInformation.nonProcessFailureEntries;
    getProcessFailEntries = windowData.firstStageInformation.processFailureEntries;
    getVoidRecord = windowData.firstStageInformation.voidQC;

    getSecondStageUnitTestedRecord = windowData.secondStageInformation.getUnitTestedEntries;
    getSecondStagePendingFailEntries = windowData.secondStageInformation.pendingFailureEntries;
    getSecondStageNonProcessFailEntries = windowData.secondStageInformation.nonProcessFailureEntries;
    getSecondStageProcessFailEntries = windowData.secondStageInformation.processFailureEntries;
    getSecondStageVoidRecord = windowData.secondStageInformation.voidQC;
  }else if (windowData.firstStageInformation && !windowData.secondStageStartWithCount && !windowData.secondStageInformation && windowData.windowStatus.title != "Current") {
    // QC3-11030 - Prachi - (!windowData.secondStageStartWithCount - Added)
    var getWindowEntries = [];
    if (windowData.firstStageInformation.getUnitTestedEntries && windowData.firstStageInformation.getUnitTestedEntries.length) {
      getWindowEntries = windowData.firstStageInformation.getUnitTestedEntries;
    }
    if (windowData.firstStageInformation.nonProcessFailureEntries && windowData.firstStageInformation.nonProcessFailureEntries.length) {
      getWindowEntries = (getWindowEntries.length) ? (getWindowEntries.concat(windowData.firstStageInformation.nonProcessFailureEntries)) : windowData.firstStageInformation.nonProcessFailureEntries;
    }
    var getEntriesFromWindowEntryId = getWindowEntries.reduce(function(c, v) {
      var getEntry = _.find(allEntry, {'_id':v});
      if (getEntry) {
        c.push(getEntry);
      }
      return c;
    }, []);
    var checkFeasibility = false;
    var checkStatus = {};
    getEntriesFromWindowEntryId.forEach(function(entry) {
      _.forEach(entry.failure, function(value, key) {
        if(((value.app._id ? value.app._id : value.app.title) == (value.app._id ? windowData.app._id : windowData.app.title))
        && windowData.testType && value.window && value.window.windowTestType &&
        (value.window.windowTestType['s#'] ? (windowData.testType['s#'] == value.window.windowTestType['s#']) :
        (windowData.testType['title'] == value.window.windowTestType)) && !checkFeasibility){
          checkFeasibility = true;
          if (value.type == "Process" ) {
            getProcessFailEntries.push(entry._id);
            checkStatus[entry._id] = value.type; //QC3-11044
          }else if (value.type == "Non Process") {
            getNonProcessFailEntries.push(entry._id);
            checkStatus[entry._id] = value.type; //QC3-11044
          }
        }
      });
      var getRecordStatus = getAppRecordStatus(entry);
      if (windowData.testTypeOption == 'Procedure') {
        if (entry.entries && entry.entries.status && windowData.testType && windowData.testType['s#'] && entry.entries.status[windowData.testType['s#']]) {
          if (entry.failure && entry.failure[windowData['_id']] && entry.failure[windowData['_id']].type) {
            if (entry.failure[windowData['_id']].type == "Non Process") {
              getNonProcessFailEntries.push(entry._id);
            }
            if (entry.failure[windowData['_id']].type == "Process") {
              getProcessFailEntries.push(entry._id);
              getTestTypeBasedEntries.push(entry._id);
            }
          }
          //QC3-11044 Prachi - Add Condition - ((checkStatus && !checkStatus[entry._id]) ||(checkStatus && checkStatus[entry._id] && checkStatus[entry._id] != 'Non Process'))
          if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && ((checkStatus && !checkStatus[entry._id]) ||(checkStatus && checkStatus[entry._id] && checkStatus[entry._id] != 'Non Process')) && (getRecordStatus == 'Passed' || getRecordStatus == 'Failed') && (entry.entries.status[windowData.testType['s#']].status === 0 || entry.entries.status[windowData.testType['s#']].status === 1)) {
            getTestTypeBasedEntries.push(entry._id);
            if (entry.entries.status[windowData.testType['s#']].status === 0  && !checkFeasibility) {
              getFailEntries.push(entry._id);
            }
          }
          if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && getRecordStatus == 'Void' && (entry.entries.status[windowData.testType['s#']].status === 0 || entry.entries.status[windowData.testType['s#']].status === 1)) {
            getVoidRecord.push(entry);
          }
        }
      }else if (windowData.testTypeOption == 'Question') {
        if (windowData.testType.parentId && windowData.testType['s#'] &&
        entry.entries.status[windowData.testType.parentId] &&
        entry.entries[windowData.testType['s#'] +'-'+ windowData.testType.parentId] &&
        (entry.entries[windowData.testType['s#'] +'-'+ windowData.testType.parentId][windowData.testType.parentId] || entry.entries[windowData.testType['s#'] +'-'+ windowData.testType.parentId][windowData.testType.parentId] == '0')) { //QC3-10372 Prachi
          if (entry.failure && entry.failure[windowData['_id']] && entry.failure[windowData['_id']].type) {
            if (entry.failure[windowData['_id']].type == "Non Process") {
              getNonProcessFailEntries.push(entry._id); //QC3-10373 Prachi / QC3-10379 Prachi
            }
            if (entry.failure[windowData['_id']].type == "Process") {
              getProcessFailEntries.push(entry._id);//QC3-10373
              getTestTypeBasedEntries.push(entry._id);
            }
          }
          if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && (getRecordStatus == 'Passed' || getRecordStatus == 'Failed') && (entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 0 || entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 1)) {
            getTestTypeBasedEntries.push(entry._id);//QC3-10373
            if (entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 0  && !checkFeasibility) {
              getFailEntries.push(entry._id);
            }
          }
          if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && getRecordStatus == 'Void' && (entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 0 || entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 1)) {
            getVoidRecord.push(entry);
          }
        }
      }
    });
  }else {
    allEntry.forEach(function(entry) {
      var getRecordStatus = getAppRecordStatus(entry);
      if (windowData.testTypeOption == 'Procedure') {
        if (entry.entries && entry.entries.status && windowData.testType && windowData.testType['s#'] && entry.entries.status[windowData.testType['s#']]) {
          if (getRecordStatus != "Void") { //UT change
            countForStartingSecondStage ++;
          }
          if (windowData && windowData.secondStageStartWithCount && (countForStartingSecondStage > windowData.secondStageStartWithCount)) {
            if (entry.failure && entry.failure[windowData['_id']] && entry.failure[windowData['_id']].type) {
              if (entry.failure[windowData['_id']].type == "Non Process") {
                getSecondStageNonProcessFailEntries.push(entry._id);
              }
              if (entry.failure[windowData['_id']].type == "Process") {
                getSecondStageProcessFailEntries.push(entry._id);
                getSecondStageUnitTestedRecord.push(entry._id);
              }
            }
            if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && (getRecordStatus == 'Passed' || getRecordStatus == 'Failed') && (entry.entries.status[windowData.testType['s#']].status === 0 || entry.entries.status[windowData.testType['s#']].status === 1)) {
              getSecondStageUnitTestedRecord.push(entry._id);
              if (entry.entries.status[windowData.testType['s#']].status === 0) {
                getSecondStagePendingFailEntries.push(entry._id);
              }
            }
            if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && getRecordStatus == 'Void' && (entry.entries.status[windowData.testType['s#']].status === 0 || entry.entries.status[windowData.testType['s#']].status === 1)) {
              getSecondStageVoidRecord.push(entry);
            }
          }else {
            var checkFeasibility = false;
            var checkStatus = {};
            _.forEach(entry.failure, function(value, key) {
              if(((value.app._id ? value.app._id : value.app.title) == (value.app._id ? windowData.app._id : windowData.app.title))
              && windowData.testType && value.window && value.window.windowTestType &&
              (value.window.windowTestType['s#'] ? (windowData.testType['s#'] == value.window.windowTestType['s#']) :
              (windowData.testType['title'] == value.window.windowTestType)) && !checkFeasibility){
                checkFeasibility = true;
                if (value.type == "Process") { //Remove status === 0 condition QC3-11068 Prachi
                  getProcessFailEntries.push(entry._id);
                  checkStatus[entry._id] = value.type; //QC3-11044
                }else if (value.type == "Non Process") {
                  getNonProcessFailEntries.push(entry._id);
                  checkStatus[entry._id] = value.type; //QC3-11044
                }
              }
            });
            if (entry.failure && entry.failure[windowData['_id']] && entry.failure[windowData['_id']].type) {
              if (entry.failure[windowData['_id']].type == "Non Process") {
                getNonProcessFailEntries.push(entry._id);
              }
              if (entry.failure[windowData['_id']].type == "Process") {
                getProcessFailEntries.push(entry._id);
                getTestTypeBasedEntries.push(entry._id);
              }
            }
            //QC3-11044 Prachi - Add Condition - ((checkStatus && !checkStatus[entry._id]) ||(checkStatus && checkStatus[entry._id] && checkStatus[entry._id] != 'Non Process'))
            if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && ((checkStatus && !checkStatus[entry._id]) ||(checkStatus && checkStatus[entry._id] && checkStatus[entry._id] != 'Non Process')) && (getRecordStatus == 'Passed' || getRecordStatus == 'Failed') && (entry.entries.status[windowData.testType['s#']].status === 0 || entry.entries.status[windowData.testType['s#']].status === 1)) {
              getTestTypeBasedEntries.push(entry._id);
              if (entry.entries.status[windowData.testType['s#']].status === 0 && !checkFeasibility) {
                getFailEntries.push(entry._id);
              }
            }
            if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && getRecordStatus == 'Void' && (entry.entries.status[windowData.testType['s#']].status === 0 || entry.entries.status[windowData.testType['s#']].status === 1)) {
              getVoidRecord.push(entry);
            }
          }
        }
      }else if (windowData.testTypeOption == 'Question') {
        if (windowData.testType.parentId && windowData.testType['s#'] &&
        entry.entries.status[windowData.testType.parentId] &&
        entry.entries[windowData.testType['s#'] +'-'+ windowData.testType.parentId] &&
        (entry.entries[windowData.testType['s#'] +'-'+ windowData.testType.parentId][windowData.testType.parentId] || entry.entries[windowData.testType['s#'] +'-'+ windowData.testType.parentId][windowData.testType.parentId] == '0')) { //QC3-10372 Prachi
          countForStartingSecondStage ++;
          if (windowData && windowData.secondStageStartWithCount) {
            //if following condition is setisfied then need to add record in Second Stage Information
            if (countForStartingSecondStage > windowData.secondStageStartWithCount) {
              if (entry.failure && entry.failure[windowData['_id']] && entry.failure[windowData['_id']].type) {
                if (entry.failure[windowData['_id']].type == "Non Process") {
                  getSecondStageNonProcessFailEntries.push(entry._id);
                }
                if (entry.failure[windowData['_id']].type == "Process") {
                  getSecondStageProcessFailEntries.push(entry._id);
                  getSecondStageUnitTestedRecord.push(entry._id);
                }
              }
              if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && (getRecordStatus == 'Passed' || getRecordStatus == 'Failed') && (entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 0 || entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 1)) {
                getSecondStageUnitTestedRecord.push(entry._id);
                if (entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 0) {
                  getSecondStagePendingFailEntries.push(entry._id);
                }
              }
              if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && getRecordStatus == 'Void' && (entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 0 || entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 1)) {
                getSecondStageVoidRecord.push(entry);
              }
            }else {
              if (entry.failure && entry.failure[windowData['_id']] && entry.failure[windowData['_id']].type) {
                if (entry.failure[windowData['_id']].type == "Non Process") {
                  getNonProcessFailEntries.push(entry._id);
                }
                if (entry.failure[windowData['_id']].type == "Process") {
                  getProcessFailEntries.push(entry._id);
                  getTestTypeBasedEntries.push(entry._id);
                }
              }
              if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && (getRecordStatus == 'Passed' || getRecordStatus == 'Failed') && (entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 0 || entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 1)) {
                getTestTypeBasedEntries.push(entry._id);
                if (entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 0) {
                  getFailEntries.push(entry._id);
                }
              }
              if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && getRecordStatus == 'Void' && (entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 0 || entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 1)) {
                getVoidRecord.push(entry);
              }
            }
          }else {
            var checkFeasibility = false;
            var checkStatus = {};
            _.forEach(entry.failure, function(value, key) {
              if(((value.app._id ? value.app._id : value.app.title) == (value.app._id ? windowData.app._id : windowData.app.title))
              && windowData.testType && value.window && value.window.windowTestType &&
              (value.window.windowTestType['s#'] ? (windowData.testType['s#'] == value.window.windowTestType['s#']) :
              (windowData.testType['title'] == value.window.windowTestType)) && !checkFeasibility){
                checkFeasibility = true;
                if (value.type == "Process") { //Remove status === 0 condition QC3-11068 Prachi
                  getProcessFailEntries.push(entry._id);
                  checkStatus[entry._id] = value.type; //QC3-11044
                }else if (value.type == "Non Process") {
                  getNonProcessFailEntries.push(entry._id);
                  checkStatus[entry._id] = value.type; //QC3-11044
                }
              }
            });
            if (entry.failure && entry.failure[windowData['_id']] && entry.failure[windowData['_id']].type) {
              if (entry.failure[windowData['_id']].type == "Non Process") {
                getNonProcessFailEntries.push(entry._id);
              }
              if (entry.failure[windowData['_id']].type == "Process") {
                getProcessFailEntries.push(entry._id);
                getTestTypeBasedEntries.push(entry._id);
              }
            }
            //QC3-11044 Prachi - Add Condition - ((checkStatus && !checkStatus[entry._id]) ||(checkStatus && checkStatus[entry._id] && checkStatus[entry._id] != 'Non Process'))
            if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && ((checkStatus && !checkStatus[entry._id]) ||(checkStatus && checkStatus[entry._id] && checkStatus[entry._id] != 'Non Process')) && (getRecordStatus == 'Passed' || getRecordStatus == 'Failed') && (entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 0 || entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 1)) {
              getTestTypeBasedEntries.push(entry._id);
              if (entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 0 && !checkFeasibility) {
                getFailEntries.push(entry._id);
              }
            }if (((entry.failure && !entry.failure[windowData['_id']]) || !entry.failure) && getRecordStatus == 'Void' && (entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 0 || entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 1)) {
              getVoidRecord.push(entry);
            }
          }
        }
      }
    });
  }
  //QC3-10949 / QC3-10956 Prachi
  getProcessFailEntries = _.uniqWith(getProcessFailEntries, _.isEqual);
  getNonProcessFailEntries = _.uniqWith(getNonProcessFailEntries, _.isEqual);
  getFailEntries = _.uniqWith(getFailEntries, _.isEqual);
  getTestTypeBasedEntries = _.uniqWith(getTestTypeBasedEntries, _.isEqual);

  countDetails = {
    appId: schema._id,
    secondStage: false,
    firstStage: {
      pendingFailure: getFailEntries ? getFailEntries.length : 0,
      pendingFailureEntries: getFailEntries,
      getUnitTestedEntriesCount: getTestTypeBasedEntries ? getTestTypeBasedEntries.length : 0,
      getUnitTestedEntries: getTestTypeBasedEntries,
      processFailureEntries: getProcessFailEntries,
      processFailure: getProcessFailEntries ? getProcessFailEntries.length : 0,
      nonProcessFailureEntries: getNonProcessFailEntries,
      nonProcessFailure: getNonProcessFailEntries ? getNonProcessFailEntries.length : 0,
      voidQC: getVoidRecord,
      voidQCEntries: getVoidRecord ? getVoidRecord.length : 0
    }
  }
  if (windowData.secondStageStartWithCount && windowData.allowSecondStage && windowData.additionalTestSecondStage) {
    getSecondStageUnitTestedRecord = _.uniqWith(getSecondStageUnitTestedRecord, _.isEqual);
    getSecondStagePendingFailEntries = _.uniqWith(getSecondStagePendingFailEntries, _.isEqual);
    getSecondStageProcessFailEntries = _.uniqWith(getSecondStageProcessFailEntries, _.isEqual);
    getSecondStageNonProcessFailEntries = _.uniqWith(getSecondStageNonProcessFailEntries, _.isEqual);
    countDetails.secondStage = true;
    countDetails.secondStageStartWithCount = windowData.secondStageStartWithCount;
    countDetails.secondStageInfo = {
      pendingFailure: getSecondStagePendingFailEntries ? getSecondStagePendingFailEntries.length : 0,
      pendingFailureEntries: getSecondStagePendingFailEntries,
      getUnitTestedEntriesCount: getSecondStageUnitTestedRecord ? getSecondStageUnitTestedRecord.length : 0,
      getUnitTestedEntries: getSecondStageUnitTestedRecord,
      processFailureEntries: getSecondStageProcessFailEntries,
      processFailure: getSecondStageProcessFailEntries ? getSecondStageProcessFailEntries.length : 0,
      nonProcessFailureEntries: getSecondStageNonProcessFailEntries,
      nonProcessFailure: getSecondStageNonProcessFailEntries ? getSecondStageNonProcessFailEntries.length : 0,
      voidQC: getSecondStageVoidRecord,
      voidQCEntries: getSecondStageVoidRecord ? getSecondStageVoidRecord.length : 0
    };
  }
  if (countDetails && Object.keys(countDetails).length) {
    callback(countDetails);
  }else {
    callback('No Records Found.');
  }
}

//Create attribute filter as per window id selection
function getFilteredAttributes(attributes, timeZone) {
  var createdFilter = [];
  var groupedAttributes = _.groupBy(attributes, 'attribute.s#') || {};
  _.forIn(groupedAttributes, function (group) {
    var matchedList = {'$or':[]};
    group.forEach(function (filter) {
      var attributeFilter = {};
      var attrkey = '';
      var key = '';
      if(filter && filter.attribute && filter.value){
        if (filter && filter.attribute && filter.attribute.isEntity && filter.attribute.keyAttributeSeqId) {
          attrkey = '' + filter.attribute.keyAttributeSeqId;
          var entkey = '' + filter.attribute.keyAttributeSeqId.substr(filter.attribute.keyAttributeSeqId.indexOf('-') + 1);
          key = 'entries.' + attrkey + '.' + entkey;
        }else {
          attrkey = '' + filter.attribute['s#'];
          key = 'entries.' + attrkey + '.attributes';
        }
        attributeFilter[key] = parse(filter.type, filter.value);
        matchedList['$or'].push(attributeFilter);
        if (filter.value && new Date(filter.value) != 'Invalid Date' && isNaN(Number(filter.value))) {
          var dateFilter = {};
          dateFilter[key] = filter.value;
          matchedList['$or'].push(dateFilter);
          matchedList['$or'].push({[key]: (new Date(filter.value)).toISOString()});
          matchedList['$or'].push({[key]: moment(filter.value).tz(timeZone).format('MM/DD/YYYY')});
        }else if(typeof filter.value !== "number" && parseFloat(filter.value)){
          var numberFilter = {};
          numberFilter[key] = parseFloat(filter.value);
          matchedList['$or'].push(numberFilter);
        } else{
          var stringFilter = {};
          stringFilter[key] = filter.value.toString();
          matchedList['$or'].push(stringFilter);
        }
      }
    });
    if (matchedList['$or'].length) {
      createdFilter.push(matchedList);
    }
  });
  return createdFilter;
}

//Create object for app record filter as provided at the time of window creation
function createFilterForAppRecords(windowDetail, schema) {
  //Open Created Record using Today(06-02-2018), Window start date Today(07-02-2018) - Prachi
  var recordFilter = {};
  recordFilter['$and'] = [];
  var lastDay = new Date(windowDetail.windowPeriod.year, windowDetail.windowPeriod.month + 1, 1).toISOString(); //QC3-11188 by Yamuna
  var firstDay = new Date(windowDetail.windowStartDate).toISOString();
  if (lastDay && firstDay) {
    var key = '' + windowDetail.attributeDate['s#'];
    recordFilter['$and'].push({
      ['entries.' + key + '.attributes']: { $gte: firstDay, $lte: lastDay}
    });
  }
  if (windowDetail.filteredAttributes) {
    var filteredAttributes = getFilteredAttributes(windowDetail.filteredAttributes, windowDetail.timeZone);
    filteredAttributes.forEach(function(attr) {
      recordFilter['$and'].push(attr);
    });
  }
  if (windowDetail.allicableSites) {
    windowDetail.allicableSites.forEach(function(sites, level) {
      if ((windowDetail.allicableSites.length - 1) == level) {
        var getLastLevelIds = sites.map(function(s) {
          return s._id;
        });
        if (getLastLevelIds && getLastLevelIds.length) {
          var levelkey = '' + level;
          recordFilter['$and'].push({
            ['levelIds.' + levelkey]: { $in: getLastLevelIds}
          });
        }
      }
    });
  }
  return recordFilter;
}

//Create filter for getting app records as per provided window filters
function getEntryHavingTestTypeValue(windowDetail, app, appEntries) {
  return appEntries.filter(function(entry) {
    if (windowDetail && windowDetail.testType && entry.entries[windowDetail.testType['s#'] +'-'+ windowDetail.testType.parentId] &&
    (entry.entries[windowDetail.testType['s#'] +'-'+ windowDetail.testType.parentId][windowDetail.testType.parentId] ||
    entry.entries[windowDetail.testType['s#'] +'-'+ windowDetail.testType.parentId][windowDetail.testType.parentId] == '0')) {
      return entry;
    }
  });
}

//QC3-10742/QC3-10745 Prachi
function getEntryHavingTestTypeValueForProcedure(windowDetail, app, appEntries) {
  if (app.procedures && app.procedures.length) {
    var getWindowTestTypeProcedureFromApp = _.find(app.procedures, {'s#' : windowDetail.testType['s#']});
  }
  return appEntries.filter(function(entry) {
    var criteriaQuestionHaveValue = false;
    if (getWindowTestTypeProcedureFromApp && getWindowTestTypeProcedureFromApp.questions) {
      getWindowTestTypeProcedureFromApp.questions.forEach(function(que) {
        if (que.criteria && que.criteria.length && !criteriaQuestionHaveValue && entry.entries[que['s#'] +'-'+ que.parentId] &&
        (entry.entries[que['s#'] +'-'+ que.parentId][que.parentId] || entry.entries[que['s#'] +'-'+ que.parentId][que.parentId] == '0')) {
          criteriaQuestionHaveValue = true;
        }
      });
    }else {
      criteriaQuestionHaveValue = true;
    }
    if (criteriaQuestionHaveValue) {
      return entry;
    }
  });
}

//Parse date, number, flot for sppropriate search data
function parse(type, value) {
  logger.log("function: parse - start" , 'info', currentFileName);
  type = (type||'').toLowerCase();
  if (type == 'array' || type == 'dstring') {
    var keyArray = [];
    var valArray = value.split(',');
  }
  switch (type) {
    case 'date':
    return new Date(value);
    case 'regex':
    return { $regex: new RegExp(value.toLowerCase(), 'i') };
    case 'number':
    case 'exponential':
    case 'percentage':
    case 'currency':
    case 'scientific':
    if (/^[0-9\-\+]*$/.test(value) == true) {
      value = parseInt(value);
    }
    if (/^[0-9 \.\-\+]*$/.test(value) == true) {
      value = parseFloat(value);
    }
    return value;
    case 'array':
    if (typeof value == 'string' && value.length) {
      _.forEach(valArray, function (chkvalue) {
        var isNotANumber = false;
        //Either for string or int
        keyArray.push(chkvalue);
        if (/^[0-9\-\+]*$/.test(chkvalue) == true) {
          chkvalue = parseInt(chkvalue);
          isNotANumber = true;
        }
        if (/^[0-9 \.\-\+]*$/.test(chkvalue) == true) {
          chkvalue = parseFloat(chkvalue);
          isNotANumber = false;
        }
        if (/^[0-9e\.\-\+]*$/.test(chkvalue) == true) {
          chkvalue = Number(chkvalue);
          isNotANumber = false;
        }
        if (chkvalue.length == 24 && db.ObjectID.isValid(chkvalue)) {
          chkvalue = db.ObjectID(chkvalue);
          isNotANumber = false;
        }
        if (isNotANumber) {
          keyArray.push(new RegExp('.*' + chkvalue + '.*', 'i'));
        } else {
          keyArray.push(chkvalue);
        }
      });
    }
    return keyArray;
    case 'dstring':
    _.forEach(valArray , function(chkvalue){
      keyArray.push(chkvalue);
    });
    return keyArray ;
    default:
    return value;
  }
}

module.exports = {
  fetchAppRecords: fetchAppRecords
}
