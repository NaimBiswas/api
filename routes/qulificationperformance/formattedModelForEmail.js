/**
* @name Qulification Performance - To get ready model for mail
* @author Yamuna Sardhara <yamuna.s@productivet.com>
*
* @version 1.0
*/

var _ = require('lodash');
var moment = require('moment-timezone');
var q = require('q');
var logger = require('../../logger');
var currentFileName = __filename;

function getFormattedModel(windowData){
  var deferred = q.defer();
  if(windowData){
    var monthArray = [{value:0,title:'January'},{value:1,title:'February'},{value:2,title:'March'},{value:3,title:'April'},{value:4,title:'May'},{value:5,title:'June'},
    {value:6,title:'July'},{value:7,title:'August'},{value:8,title:'September'},{value:9,title:'October'},{value:10,title:'November'},{value:11,title:'December'}];
    var windowTypes = [ {id: '1', title: "Binomial Distribution"}, {id: '2', title: "Hypergeometric Distribution"}, {id: '3', title: "Customized Windows"} ];
    if (!windowData.filteredAttributes || !windowData.filteredAttributes.length){
      windowData.attributes = 'N/A';
      windowData.filteredAttributes = [];
    }else{
      windowData.attributes = '';
      windowData.filteredAttributes.forEach(function(v, c){
        v.value = (v.type == 'Date') ? moment(v.value).format('MM/DD/YYYY') : v.value;
      });
    }
    //mail should be fired of stop window
    windowData.windowStartDate = moment(windowData.windowStartDate).tz(windowData.timeZone).format('MM/DD/YYYY hh:mm:ss A'); //QC3-10891	Prachi (IST time resolve)
    windowData.windowEndDate = moment(new Date(windowData.windowPeriod.year, windowData.windowPeriod.month+1, 0)).format('MM/DD/YYYY hh:mm:ss A');
    windowData.stopWindowDate = moment(windowData.stopWindowDate).format('MM/DD/YYYY HH:MM');
    windowData.countDetails = windowData.firstStageInformation ? windowData.firstStageInformation : '0';
    windowData.pendingFailure = (windowData.countDetails != 0) ? windowData.countDetails.pendingFailure : 0;
    windowData.processFailure = (windowData.countDetails != 0) ? windowData.countDetails.processFailure : 0;
    windowData.unitTestedUnits = (windowData.countDetails != 0) ? windowData.countDetails.getUnitTestedEntriesCount : 0;
    windowData.voidQC = (windowData.countDetails != 0) ? windowData.countDetails.voidQCEntries : 0;
    windowData.remainingUnits = (windowData.countDetails != 0) ? (((windowData.sampleSize - windowData.countDetails.getUnitTestedEntriesCount) < '0') ? 0 : (windowData.sampleSize - windowData.countDetails.getUnitTestedEntriesCount)) : (windowData.sampleSize ? windowData.sampleSize :0);
    windowData.remainingFailures = (windowData.countDetails != 0) ? (((windowData.maximumAllowedProcessFailure - windowData.countDetails.processFailure) < '0') ? 0 : (windowData.maximumAllowedProcessFailure - windowData.countDetails.processFailure)) : (windowData.maximumAllowedProcessFailure ? windowData.maximumAllowedProcessFailure : 0);
    windowData.windowTypeEmail = _.find(windowTypes,{id: windowData.windowType}).title; //QC3-11093 by Yamuna

    //QC3-10694 Email Change Improvement
    if (windowData && windowData.allowSecondStage) {
      windowData.hasSecondStage = 'YES';
      windowData.isSecondStageActivated = 'NO';
      windowData.sampleDetailsForSecondStage = windowData.additionalTestSecondStage;
      //QC3-10299 Prachi
      if (windowData && windowData.secondStageInformation) {
        windowData.isSecondStageActivated = 'YES';
        windowData.displaySecondStage = true;
        windowData.secondStageCountDetails = windowData.secondStageInformation ? windowData.secondStageInformation : '0';
        windowData.seondStagePendingFailure = (windowData.secondStageCountDetails != 0) ? windowData.secondStageCountDetails.pendingFailure : 0;
        windowData.seondStageProcessFailure = (windowData.secondStageCountDetails != 0) ? windowData.secondStageCountDetails.processFailure : 0;
        windowData.seondStageRemainingFailure = (windowData.secondStageCountDetails != 0) ? (windowData.seondStageProcessFailure > 0 ? 0 : 1) : 0;
      }
    }
    if(monthArray[windowData.windowPeriod.month]){
      windowData.windowPeriod.month = monthArray[windowData.windowPeriod.month].title;
    }
    deferred.resolve(windowData);
  }else{
    deferred.reject("something wents wrong");
    return;
  }
return deferred.promise;
}

module.exports = {
  getFormattedModel: getFormattedModel
}
