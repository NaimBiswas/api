/**
* @name Qulification Performance - Search App Records From Window Id
* @author Jyotil Raval <jyotil.r@productivet.com>
* @param {Array} Entry._id
* @param {string} Schema._id
* @return {Object} Rwo Formated Data of Entry of pirticlar schema of that version
* @version 1.0
*/
var db = require('../../lib/db');
var _ = require('lodash');
var moment = require("moment");
var q = require('q');
var logger = require('../../logger');
var currentFileName = __filename;
var Utils = require("./utils.js");

//Return unit tested app records
function fetchAppRecords(params, options) {
  logger.log('Function: fetchAppRecords - Start', 'info', currentFileName);
  var deferred = q.defer();
  try {
    var filter = options;
    filter.types = [];
    getTypeFormate().then(function (types) {
      filter.types = types;
      getLatestSchema(filter).then(function (instance) {
        deferred.resolve(instance);
      }).catch(function (err) {
        deferred.reject(err);
      });
    });
  } catch (err) {
    deferred.reject(err);
  }
  return deferred.promise;
}

function getTypeFormate() {
  var deferred = q.defer();
  try {
    db.types.find().toArray(function (err, instance) {
      if (err) {
        deferred.reject(err);
      } else {
        deferred.resolve(instance);
      }
    })
  } catch (err) {
    deferred.reject(err);
  }
  return deferred.promise;
}

function getSiteIdsWithNames(siteIds, cb) {
  db.sites.aggregate(
    [
      { $match: { _id: { $in: siteIds } } },
      {
        $lookup: {
          from: "siteNames",
          localField: "level._id",
          foreignField: "_id",
          as: "siteName"
        }
      },
      {
        $project: { _id: 1, siteName: 1 }
      }
    ],
    function(err, sitesWithName) {
      if (err) {
        return cb(err,null);
      }
      sitesWithName = sitesWithName.reduce(function(returnObject, siteObject) {
        returnObject[siteObject._id] = siteObject.siteName[0].title;
        return returnObject;
      }, {});
      cb(null, sitesWithName);
    }
  );
}

function getCommonSiteIdsinAllEntries(entries) {
  var siteIds = [];
  _.forEach(entries, function(singleEntry) {
    _.forEach(singleEntry.levels, function(level) {
      if (siteIds.indexOf(level.siteId) == -1) {
        siteIds.push(level.siteId);
      }
    });
  });
  siteIds = _.reduce(siteIds, function(siteIdsResult, siteIdString) {
    siteIdsResult.push(db.ObjectID(siteIdString));
    return siteIdsResult;
  },[]);
  return siteIds;
}

function idWiseDateFormat(cb){
  db.types.find({
    title:"Textbox"
  }).toArray(function(err,typedata){
    var idWiseDateFormat = {};
    _.forEach(typedata[0].formats,function(format){
      if(format.title == "Date"){
        _.forEach((format.metadata[0] || {}).allowItems,function(item){
          idWiseDateFormat[item._id] = item.title;
        })
      }
    });
    cb(null,idWiseDateFormat);
  });
}

var getEntry = function (filter, schemaDetail) {
  logger.log('Function: getEntry - Start', 'info', currentFileName);
  var deferred = q.defer();
  try {
    var collection = filter.schemaId;
    var entryFilter = {
      '_id' : db.ObjectID(filter.entryId)
    };
    db[collection].findOne(entryFilter, function (err, entryDetail) {
      if (err) {
        deferred.reject(err);
      } else {
        filter.parentVersion = entryDetail.parentVersion || 1;
        filter.parentSchema = schemaDetail.versions.find(function (version) {
          return version.version  == filter.parentVersion
        }).record || '';
        getVersionSchema(filter, entryDetail).then(function (formatedSchema) {
          deferred.resolve(formatedSchema);
        }).catch(function (err) {
          deferred.reject(err);
        });
      }
    });
  } catch (err) {
    deferred.reject(err);
  }
  return deferred.promise;
}

function attachSiteNameTitleToEntryObject(entry, siteIdWithNames) {
  entry.levels.forEach(function(level) {
    level.siteName = siteIdWithNames[level.siteId];
  });
}

var getVersionSchema = function (filter, entryDetail) {
  logger.log('Function: getSchema - Start', 'info', currentFileName);
  var deferred = q.defer();
  try {
    var collection = 'schemaAuditLogs';
    var parentSchemaFilter = {
      '_id' : db.ObjectID(filter.parentSchema)
    };
    db[collection].findOne(parentSchemaFilter, function (err, parentSchemaDetail) {
      if (err) {
        deferred.reject(err);
      } else {
        getSiteIdsWithNames(getCommonSiteIdsinAllEntries([entryDetail]), function (err, siteIdWithNames) {
          if (err) {
            deferred.reject(err);
          } else {
            attachSiteNameTitleToEntryObject(entryDetail,siteIdWithNames);
            generateSchemaDetail(parentSchemaDetail, entryDetail, filter).then(function (responce) {
              deferred.resolve(responce);
            }).catch(function (err) {
              deferred.reject(err);
            });
          }
        });
      }
    });
  } catch (err) {
    deferred.reject(err);
  }
  return deferred.promise;
}

var getLatestSchema = function (filter) {
  logger.log('Function: getSchema - Start', 'info', currentFileName);
  var deferred = q.defer();
  try {
    var collection = 'schema';
    var scheamFilter = {
      '_id' : db.ObjectID(filter.schemaId)
    };
    db[collection].findOne(scheamFilter, function (err, schemaDetail) {
      if (err) {
        deferred.reject(err);
      } else {
        getEntry(filter, schemaDetail).then(function (responce) {
          deferred.resolve(responce);
        }).catch(function (err) {
          deferred.reject(err);
        });
      }
    });
  } catch (err) {
    deferred.reject(err);
  }
  return deferred.promise;
}

function getFormatedEntryObject(entry, parentSchema, options) {
  logger.log('Function: getFormatedEntryObject - Start', 'info', currentFileName);
  var deferred = q.defer();
  try {
    options.shortObjectSequance = 1;
    var entryObject = {
      _id: entry._id,
      child:entry.child,
      parentVersion: entry.parentVersion,
      createdByName: entry.createdByName,
      createdByInfo: entry.createdByInfo,
      modifiedByName: entry.modifiedByName,
      createdDate: moment
      .utc(new Date(entry.createdDate))
      .tz(options.clientTZ)
      .format("MM/DD/YYYY HH:mm:ss"),
      modifiedDate: moment
      .utc(new Date(entry.modifiedDate))
      .tz(options.clientTZ)
      .format("MM/DD/YYYY HH:mm:ss"),
      levels: entry.levels,
      // createdByInfo: entry.createdByInfo,
      workflowreview: entry.workflowreview,
      verificationDate: entry.verificationDate,
      reviewStatus: entry.reviewStatus,
      batchEntryId: entry.batchEntryId,
      masterQCSettings: entry.masterQCSettings,
      schema: parentSchema
      ? {
        _id: parentSchema.identity,
        title: parentSchema.title,
        // attributes: parentSchema.attributes,
        // procedures: parentSchema.procedures,
        isActive: parentSchema.isActive,
        workflowreview: parentSchema.workflowreview,
        // allicableSites: parentSchema.allicableSites,
        module: parentSchema.module,
        description: parentSchema.description,
        allowcreater:parentSchema.allowcreater,
        // allowCreaterObj: parentSchema.allowCreaterObj,
        file: parentSchema.file,
        // entities: parentSchema.entities,
        // keyvalue: parentSchema.keyvalue,
        headMsgPrint: parentSchema.headMsgPrint,
        isLockRecord: parentSchema.isLockRecord,
        headMsgPrintForPass: parentSchema.headMsgPrintForPass,
        headMsgPrintForFail: parentSchema.headMsgPrintForFail,
        instructionURL: parentSchema.instructionURL,
        siteYesNo: parentSchema.siteYesNo,
      }: undefined
    };
    if (parentSchema) {
      addConditionalProcedures(parentSchema);
      // For solving QC3-10283 - Remove blank procedures + entities from display list - JD
      filterProceduresWithBlankValues(parentSchema, entry);
      entryObject["attributes"] = Utils.getAttributes(parentSchema.attributes, entry, entryObject, options);
      options.shortObjectSequance++;
      entryObject["entities"] = Utils.getEntities(parentSchema.entities, entry, entryObject, options);
      entryObject["procedures"] = Utils.getEntities(parentSchema.procedures, entry, entryObject, options);
    }
    entryObject["status"] = Utils.getStausString(entry);
    entryObject["FailMesssages"] = [];
    if (entry.entries.FailMesssageForQCForm) {
      _.forEach(entry.entries.FailMesssageForQCForm, function(value, key){
        entryObject["FailMesssages"].push(value)
      });
    }
    deferred.resolve(entryObject);
  } catch (err) {
    deferred.reject(err);
  }
  return deferred.promise;
}

function addConditionalProcedures(parentSchema){
  _.forEach(parentSchema.entities,function(entity, index) {
    _.forEach(entity.conditionalWorkflows || [],function(workflow) {
      if(workflow.procedureToExpand && _.isUndefined(workflow.procedureToExpand['isProcedure'])) {
        // For solving QC3-10283 - for differentiate conditional procedure Vs Entity - JD
        // Adding into entities rather then procedure. Due to this sequence is maintain as display in workfow
        workflow.procedureToExpand['isProcedure'] = true;
        // Use splice for insert procedure at specific index
        parentSchema.entities.splice((index+1), 0, workflow.procedureToExpand);
      }
    });
  })
}

// For solving QC3-10283 - Return true or false based on any questions contains value - JD
function needToDisplay(proc, entries) {
  var count = 0;
  _.forEach(proc.questions, function(question) {
    if(entries[question['s#']+'-'+proc['s#']] &&
    entries[question['s#']+'-'+proc['s#']][proc['s#']] &&
    !_.isNull(entries[question['s#']+'-'+proc['s#']][proc['s#']]) &&
    !_.isUndefined(entries[question['s#']+'-'+proc['s#']][proc['s#']])) {
      count++;
    }
  });
  return count > 0 ? true : false;
}

// For solving QC3-10283 - Remove blank procedure + entities from display list - JD
function filterProceduresWithBlankValues(parentSchema, entry) {
  if(entry && entry.entries) {
    // Filtering Procedures
    parentSchema.procedures = _.filter(parentSchema.procedures, function(procedure) {
      // condition written for optional procedures
      if(procedure.isOptionalProcedure) {
        return entry.entries.settings && entry.entries.settings[procedure['s#']] ? needToDisplay(procedure, entry.entries) : false;
      } else {
        return needToDisplay(procedure, entry.entries);
      }
    });

    // Filtering Entities
    parentSchema.entities = _.filter(parentSchema.entities, function(entity) {
      return needToDisplay(entity, entry.entries);
    });
  }
}

var generateSchemaDetail = function (parentSchemaDetail, entryDetail, filter) {
  logger.log('Function: getSchema - Start', 'info', currentFileName);
  var deferred = q.defer();
  try {
    getFormatedEntryObject(entryDetail, parentSchemaDetail, filter).then(function (FormatedData) {
      deferred.resolve(FormatedData);
    }).catch(function (err) {
      deferred.reject(err);
    })
  } catch (err) {
    deferred.reject(err);
  }
  return deferred.promise;
}

var createFilters = function (options) {
  var quary = {
    'entryId' : options.entryId,
    'schemaId' : options.schemaId
  };
  return quary;
}

module.exports = {
  fetchAppRecords: fetchAppRecords
}
