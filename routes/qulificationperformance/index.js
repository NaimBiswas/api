/**
* @name Qulification Performance
* @author Prachi Thakkar <prachi.t@productivet.com>
*
* @version 1.0
*/
(function () {
  'use strict';
  var express = require('express');
  var db = require('../../lib/db');
  var config = require('../../config/config.js');
  var _ = require('lodash');
  var decode = require('../../lib/oauth/model').getAccessToken;
  var lzstring = require('lz-string');
  var logger = require('../../logger');
  var currentFileName = __filename;
  var Utils = require("./utils.js");
  var Middlewares = require("../review/middlewares");
  var Review = require("../review/review");

  //Create utiliti js file for create search window filter
  var searchWindow = {
    window: require('./searchWindow.js'),
    relatedWindow: require('./relatedWindow.js'),
    records: require('./searchRecords.js'),
    selectedRecords: require('./searchSelectedRecords.js'),
    failureAssignment: require('./failureAssignment.js'),
    versionWiseEntry: require('./versionWiseEntry.js'),
    getReportWindows: require('./getReportWindows.js'),
    exportWindows: require('./exportWindows')
  };

  module.exports = function (app) {
    var apiRoutes = express.Router();
    //Get apps for creating or search windows - Filter app which contains date attribute & questions having acceptance criteria
    function getWindowApp(req, res) {
      var params = ['criteria', '1'];
      db['schema'].find({ $and: [{$where: "getCriteriaQuestions.apply(this, " + JSON.stringify(params) + ");"},
      {'attributes.type.format.title': 'Date'}, {'module._id': req.params.moduleId}, {'isActive': true} ] },
      {title: 1, module: 1}).toArray(function (err,apps) {
        if (err) {
          log.log(err + ' : Error');
        }
        if (apps && apps.length) {
          res.status(200).send(apps);
        }else {
          res.status(204).send('No Apps Found.');
        }
      });
    }

    function getWindowPlan(req, res) {
      if (req.params.type && req.params.plan && req.params.populationSize && req.params.type == '2') {
        db.windowSamplingPlan.find({windowType: req.params.type, planId: req.params.plan, populationSize: req.params.populationSize.toString()}).toArray(function(err, samplingPlan) {
          if (err) {
            log.log(err + ' : Error');
            res.status(400).send('Bad Request');
          }
          if (samplingPlan && samplingPlan.length) {
            res.status(200).send(samplingPlan);
          }else {
            res.status(204).send('No Plan Found.');
          }
        });
      }else if (req.params.type && req.params.plan && req.params.type == '1') {
        db.windowSamplingPlan.find({windowType: req.params.type, planId: req.params.plan}).toArray(function(err, samplingPlan) {
          if (err) {
            log.log(err + ' : Error');
            res.status(400).send('Bad Request');
          }
          if (samplingPlan && samplingPlan.length) {
            res.status(200).send(samplingPlan);
          }else {
            res.status(204).send('No Plan Found.');
          }
        });
      }else {
        res.status(400).send('Bad Request');
      }
    }

    function getAllAppId(getAppFromWindows, callback) {
      var getAllAppId = getAppFromWindows.map(function(w) {
        return db.ObjectID(w.app._id);
      });
      if (getAllAppId && getAllAppId.length) {
        getAllAppId = _.uniqWith(getAllAppId, _.isEqual);
        callback(null, getAllAppId);
      }else {
        callback(false);
      }
    }

    function getAppConfiguredInWindow(req, res) {
      if (req.params.moduleId) {
        db.windows.find({'module._id': req.params.moduleId}, {'app': 1}).toArray(function(err, getAppFromWindows) {
          if (err) {
            res.status(500).json({message: err});
          }
          if (getAppFromWindows && getAppFromWindows.length) {
            getAllAppId(getAppFromWindows, function(err, getAllAppId) {
              if (getAllAppId && getAllAppId.length) {
                db.schema.find({'_id': {'$in': getAllAppId}}, {'title': 1, 'module':1, 'isActive': 1}).sort({'title': 1}).toArray(function(err, apps) {
                  if (err) {
                    res.status(500).json({message: err});
                    return;
                  }else {
                    res.status(200).send(apps);
                  }
                });
              }else {
                res.status(204).send('No Apps Found.');
              }
            });
          }else {
            res.status(204).send('No Apps Found.'); //QC3-10792 Prachi
          }
        });
      }else {
        res.status(204).send('No Apps Found.');
      }
    }
    //Get apps for creating or search windows
    //Method: GET API
    apiRoutes.get('/getWindowApp/:moduleId',getWindowApp);

    apiRoutes.get('/getAppConfiguredInWindow/:moduleId',getAppConfiguredInWindow);

    apiRoutes.get('/getWindowPlan/:type/:plan/:populationSize',getWindowPlan);

    apiRoutes.post("/entryDetails",function(req, res, next) {
      var options = {};
      options.clientTZ = req.headers["client-tz"] || "America/Chicago";
      var body = req.body;
      var schemaId = body.schemaId;
      var entryId = body.entryId;
      db.collection(schemaId).find({_id:db.ObjectID(entryId)}).toArray(function (err, entries) {
        db.collection("schema").find({_id:db.ObjectID(schemaId)}).toArray(function (err, schema) {
          var entriesDistinctVersionList = Utils.getDistinctVersionsEntries(entries);
          Utils.getSchemaVersionWise(entriesDistinctVersionList,schema[0].versions).then(function(schemaVersionWise){
            var parentSchema = schemaVersionWise[entries[0].parentVersion];
            var entryData = Utils.getFormatedEntryObject(entries[0],parentSchema,options)
            res.status(200).send(entryData);
          });
        });
      });
    });

    //API for Search Window - Check for token
    apiRoutes.post('/searchWindow', function (req, res, next) {
      executeAPIOperation(req, res, next, 'window');
    });

    //Get record count for unit tested records/ pending failure records/ remaining units
    apiRoutes.post('/getAppRecordCount/:windowId/:id', function (req, res, next) {
      executeAPIOperation(req, res, next, 'records');
    });

    //Get record of scheam of version
    apiRoutes.post('/versionwiseentry/:windowId/:id', function (req, res, next) {

      // req.body.entryId
      // req.body.schemaId

      req.body.appId = req.body.schemaId;
      req.body.sort = {"createdDate": -1}
      req.body.includeInActiveRecords = true;
      req.body.pageSize = 100;
      req.body.pageNumber = 1;
      req.body.apps = [];
      req.body.fetchMaster = false;
      req.body.noPermissionCheck = true;

      next();
      // executeAPIOperation(req, res, next, 'versionWiseEntry');
    },Middlewares.attachOptions,
    Middlewares.handleMasterApp,
    Review.getReviewData);

    //Get related windows at the time of Process Non-process Failure Assignment
    apiRoutes.post('/getRelatedWindows/:windowId/:id', function (req, res, next) {
      executeAPIOperation(req, res, next, 'relatedWindow');
    });

    //Get records as per above apis responce data (get all the records ids from api /getAppRecordCount/:windowId/:id)
    apiRoutes.post('/getSelectedRecords/:windowId/:id', function (req, res, next) {
      executeAPIOperation(req, res, next, 'selectedRecords');
    });

    //Assign record is process failure or non process failure
    apiRoutes.post('/failureAssignment', function (req, res, next) {
      executeAPIOperation(req, res, next, 'failureAssignment');
    })

    //Get windows for reports
    apiRoutes.post('/getReportWindows', function (req, res, next) {
      executeAPIOperation(req, res, next, 'getReportWindows');
    })

    //Get window records for reports
    apiRoutes.post('/getReportWindows/:windowId/:id', function (req, res, next) {
      executeAPIOperation(req, res, next, 'getReportWindows');
    })

    apiRoutes.post('/exportWindows', function (req, res, next) {
      executeAPIOperation(req, res, next, 'exportWindows');
    })

    //Token verification (User is authorized or not)
    function executeAPIOperation (req, res, next, type) {

      logger.log('API Called : api/qulificationperformance', 'info', currentFileName);

      var xToken = req.headers['x-authorization'] || req.query['x-authorization'];
      if (xToken) {
        logger.log('if(xToken) : Inside', 'info', currentFileName);
        db['allAuthToken'].findOne({
          token: xToken
        }, function (e, token) {
          if (e || !token) {

            logger.log('Collection: allAuthToken - Failed : ' + e, 'error', currentFileName);

            res.status(403).json({
              message: 'You are not authorized.'
            });
            return;
          }

          logger.log('Collection: allAuthToken- success', 'info', currentFileName);
          db['users'].findOne({
            _id: db.ObjectID(token.userId || token.user._id)
          }, function (err, user) {
            if (err) {
              logger.log('Collection:  call -failed : ' + err, 'error', currentFileName);
              res.status(403).json({
                message: 'You are not authorized.'
              });
              return;
            }

            logger.log('Collection:  users - success', 'info', currentFileName);
            req.user = user || {};

            if (req.user['applicableSites']) {
              logger.log("if (req.user['applicableSites']): Inside", 'info', currentFileName);
              var accessiableSites = req.user['applicableSites'];
              req.user['applicableSitesList'] = _.map(_.cloneDeep(req.user['applicableSites']), function (sites) {
                return _.map(sites, function(subSites){ return subSites._id});
              });
              req.user['applicableSites'] = _.map([].concat.apply([], accessiableSites), function (site) {
                return site._id;
              });
            }

            var rolesToValidate = _.map((user.roles || []), function (role) {
              return db.ObjectID(role._id);
            });
            db['roles'].find({
              _id: {
                '$in': rolesToValidate
              },
              modules: {
                $exists: true
              }
            }, {
              modules: 1
            }).toArray(function (err, roles) {

              logger.log('Collection: roles- success', 'info', currentFileName);
              var schema = [];
              _.forEach((roles || []), function (role) {
                _.forEach((role.modules || []), function (module) {
                  _.forEach((module.permissions || []), function (permission) {
                    if (permission.view && permission.entityType === 'schemas') {
                      schema.push(permission._id + '');}
                    });
                  });
                });

                req.user.authorizedSchemas = _.uniq(schema);
                if (req.params.windowId) {
                  if (type == 'records') {
                    //If window Id is there in request parameter, get app records from window id
                    renderAppRecords(req, res, next, (type || 'records'));
                  }
                  if (type == 'selectedRecords') {
                    //using window Id get unit tested records or many more
                    renderSelectedTypeRecords(req, res, next, (type || 'selectedRecords'));
                  }
                  if (type == 'relatedWindow') {
                    //Get related windows at the time of Process Non-process Failure Assignment
                    renderRelatedWindows(req, res, next, (type || 'relatedWindow'));
                  }
                  if (type == 'versionWiseEntry') {
                    //Get Version Wise Entry
                    renderVersionWiseEntry(req, res, next, (type || 'versionWiseEntry'));
                  }
                  if(type == 'getReportWindows'){
                    //for get records of window for report
                    renderGetReportWindows(req, res, next, (type || 'getReportWindows'));
                  }
                }else {
                  if (type == 'failureAssignment') {
                    //Assign record is process failure or non process failure
                    renderAppRecordForFailureAssignment(req, res, next, (type || 'failureAssignment'));
                  }else if(type == 'window') {
                    //For Search Window - Render Data for Searched Window
                    renderPost(req, res, next, (type || 'window'));
                  }else if(type == 'getReportWindows'){
                    //for get windows for report
                    renderGetReportWindows(req, res, next, (type || 'getReportWindows'));
                  }else if(type == 'exportWindows') {
                    //For export Window - pdf or excel
                    renderExportWindows(req, res, next, (type || 'exportWindows'));
                  }else {
                    res.status(400).json({
                      message: 'Invalid Request'
                    });
                  }
                }
              });

            });

          });
        } else if (req.headers['authorization'] || req.query['authorization']) {
          logger.log("else if (req.headers['authorization'] || req.query['authorization'])- Inside authorization brear token", 'info', currentFileName);
          var decompressToken = lzstring.decompressFromEncodedURIComponent(req.headers['authorization']);
          var token = decompressToken;
          if (token && _.startsWith(token, 'Bearer ')) {
            logger.log("if(token && _.startsWith(token,'Bearer ')) : Inside decompressed authorization brear token ", 'info', currentFileName);
            decode(token.substring(7), function (err, data) {
              if (err) {
                logger.log('Error: decoding authorization brear token failed : '+ err, 'error', currentFileName);
                res.status(403).json({
                  message: 'You are not authorized.'
                });
                return;
              }
              logger.log('decode: Success - Inside decoded authorization brear token Info', 'info', currentFileName);
              req.user = data.user || {};
              if (req.params.windowId) {
                if (type == 'records') {
                  //If window Id is there in request parameter, get app records from window id
                  renderAppRecords(req, res, next, (type || 'records'));
                }
                if (type == 'selectedRecords') {
                  //using window Id get unit tested records or many more
                  renderSelectedTypeRecords(req, res, next, (type || 'selectedRecords'));
                }
                if (type == 'relatedWindow') {
                  //Get related windows at the time of Process Non-process Failure Assignment
                  renderRelatedWindows(req, res, next, (type || 'relatedWindow'));
                }
                if (type == 'versionWiseEntry') {
                  //Get Version Wise Entry
                  renderVersionWiseEntry(req, res, next, (type || 'versionWiseEntry'));
                }
              }else {
                if (type == 'failureAssignment') {
                  //Assign record is process failure or non process failure
                  renderAppRecordForFailureAssignment(req, res, next, (type || 'failureAssignment'));
                }else if(type == 'window') {
                  //For Search Window - Render Data for Searched Window
                  renderPost(req, res, next, (type || 'window'));
                }else if(type == 'getReportWindows'){
                  //for get windows for report
                  renderGetReportWindows(req, res, next, (type || 'getReportWindows'));
                }else if(type == 'exportWindows') {
                  //For export Window - pdf or excel
                  renderExportWindows(req, res, next, (type || 'exportWindows'));
                }else {
                  res.status(400).json({
                    message: 'Invalid Request'
                  });
                }
              }
            });
          } else {
            logger.log('else: authorization brear token decompression failed', 'info', currentFileName);
            res.status(403).json({
              message: 'You are not authorized.'
            });
          }
        } else {
          logger.log('else: No token found', 'info', currentFileName);
          res.status(403).json({
            message: 'You are not authorized.'
          });
        }
      }

      //Assign record is process failure or non process failure
      function renderAppRecordForFailureAssignment(req, res, next, type){
        logger.log('Function: renderAppRecordForFailureAssignment - Start', 'info', currentFileName);
        var options = req.body || {};
        options.ip = req.ip;
        searchWindow[type || 'failureAssignment'].fetchAppRecordForFailureAssignment(options, function (err, response) {
          if (err) {
            logger.log('Function : fetchAppRecordForFailureAssignment- Error : failed : '+ err, 'error', currentFileName);
            res.status(500).json({
              message: err.message
            });
          }else {
            logger.log('Function : fetchAppRecordForFailureAssignment - success', 'info', currentFileName);
            res.status(200).json(response);
          }
        });
      }

      //For get windows for report
      function renderGetReportWindows(req, res, next, type){
        logger.log('Function: renderGetReportWindows - Start', 'info', currentFileName);
        var options = req.body || {};
        options.clientOffSet = config.timeZones[config.currentTimeZoneP] ? config.timeZones[config.currentTimeZoneP] : options.timeZone;
        options.clientTZ  = config.timeZonesP[config.currentTimeZoneP] ? config.timeZonesP[config.currentTimeZoneP] : options.timeZone;
        if(req.params && _.size(req.params) > 0){
          var windows = [];
          windows.push({
            _id: req.params.id,
            ID: req.params.windowId
          });
          try{
            searchWindow[type || 'getReportWindows'].getRecords(windows, [], options).then(function(response){
              logger.log('Function : getRecords - success', 'info', currentFileName);
              res.status(200).json(response);
            }).catch(function (err) {
              if (err == "No Records Found.") {
                res.status(200).json([err]);
              }else{
                res.status(500).json({
                  message: err
                });
              }
            });
          }catch(err){
            logger.log('Function : getRecords- Error : failed : '+ err, 'error', currentFileName);
            res.status(500).json({
              message: err
            });
          }
        }else if(options.isFrom && options.isFrom == 'Reports'){
          try{
            searchWindow[type || 'getReportWindows'].getWindowsWithCounts(options).then(function(response){
              logger.log('Function : getWindowsWithCounts - success', 'info', currentFileName);
              res.status(200).json(response);
            })
          }catch(err){
            logger.log('Function : getWindowsWithCounts- Error : failed : '+ err, 'error', currentFileName);
            res.status(500).json({
              message: err
            });
          }
        }else {
          searchWindow[type || 'getReportWindows'].fetchReportWindows(options, function (err, response) {
            if (err) {
              logger.log('Function : fetchReportWindows- Error : failed : '+ err, 'error', currentFileName);
              res.status(500).json({
                message: err.message
              });
            }else {
              logger.log('Function : fetchReportWindows - success', 'info', currentFileName);
              res.status(200).json(response);
            }
          });
        }

      }

      //export Windows

      function renderExportWindows(req, res, next, type){
        logger.log('Function: renderExportWindows - Start', 'info', currentFileName);
        var options = req.body || {};
        options.clientTZ = req.headers['client-tz'] ? req.headers['client-tz'] : config.timeZonesP[config.currentTimeZoneP];
        options.clientOffSet = req.headers['client-offset'] ? req.headers['client-offset'] : config.timeZones[config.currentTimeZoneP];
        try{
          searchWindow[type || 'exportWindows'].exportWindows(options).then(function(response){
            logger.log('Function : exportWindows - success', 'info', currentFileName);
            res.status(200).json(response);
          });
        }catch(err){
          logger.log('Function : exportWindows- Error : failed : '+ err, 'error', currentFileName);
          res.status(500).json({
            message: err
          });
        }

      }

      //Get related windows at the time of Process Non-process Failure Assignment
      function renderRelatedWindows(req, res, next, type){
        logger.log('Function: renderRelatedWindows - Start', 'info', currentFileName);
        var options = req.body || {};
        options.user = req.user;
        options.clientOffSet = req.headers['client-offset'] || 0;
        options.clientTZ = req.headers['client-tz'] || 'America/Chicago';

        options = {
          sort : options.sort ? options.sort : { '_id': -1 },
          pageSize : parseInt(options.pageSize || 10),
          pageNumber : parseInt(options.pageNumber || 1),
          skip : options.pageSize * (options.pageNumber - 1),
          limit : parseInt(options.pageSize),
          clientOffSet : options.clientOffSet,
          clientTZ : options.clientTZ
        };

        searchWindow[type || 'relatedWindow'].fetchRelatedWindows(req.params, options, function (err, response) {
          if (err) {
            logger.log('Function : fetchRelatedWindows- Error : failed : '+ err, 'error', currentFileName);
            res.status(500).json({
              message: err.message
            });
          }else {
            logger.log('Function : fetchRelatedWindows - success', 'info', currentFileName);
            res.status(200).json(response);
          }
        });
      }

      function renderVersionWiseEntry(req, res, next, type) {
        logger.log('Function: renderVersionWiseEntry - Start', 'info', currentFileName);
        var options = req.body || {};
        options.user = req.user;
        options.clientOffSet = req.headers['client-offset'] || 0;
        options.clientTZ = req.headers['client-tz'] || 'America/Chicago';
        options = {
          entryId : options.entryId,
          schemaId : options.schemaId,
          sort : options.sort ? options.sort : { '_id': -1 },
          pageSize : parseInt(options.pageSize || 10),
          pageNumber : parseInt(options.pageNumber || 1),
          skip : options.pageSize * (options.pageNumber - 1) || 0,
          limit : parseInt(options.pageSize) || 10,
          clientOffSet : options.clientOffSet,
          clientTZ : options.clientTZ
        };

        searchWindow[type || 'versionWiseEntry'].fetchAppRecords(req.params, options).then(function (response) {
          logger.log('Function : renderVersionWiseEntry - success', 'info', currentFileName);
          res.status(200).json(response);
        }).catch(function (err) {
          res.status(500).json({
            message: err
          });
        });
      }

      //If window Id is there in request parameter, get app records from window id
      function renderAppRecords(req, res, next, type) {
        logger.log('Function: renderAppRecords - Start', 'info', currentFileName);
        if (req.params.windowId && req.params.id) {
          searchWindow[type || 'records'].fetchAppRecords(req.params, function (err, response) {
            if (err) {
              logger.log('Function : renderAppRecords- Error : failed : '+ err, 'error', currentFileName);
              res.status(500).json({
                message: err.message
              });
            }else {
              logger.log('Function : renderAppRecords - success', 'info', currentFileName);
              res.status(200).json(response);
            }
          });
        }else {
          console.log("else renderAppRecords");
          logger.log('Function : renderAppRecords- Error : failed', 'error', currentFileName);
          res.status(500).json({
            message: 'Invalid Request.'
          });
        }
      }

      //using window Id get unit tested records or many more
      function renderSelectedTypeRecords(req, res, next, type) {
        logger.log('Function: renderSelectedTypeRecords - Start', 'info', currentFileName);
        if (req.params.windowId && req.params.id) {
          var options = req.body;
          options.user = req.user;
          options.clientOffSet = req.headers['client-offset'] || 0;
          options.clientTZ = req.headers['client-tz'] || 'America/Chicago';
          searchWindow[type || 'selectedRecords'].fetchSelectedAppRecords(req.params, options, function (err, response) {
            if (err) {
              logger.log('Function : renderSelectedTypeRecords- Error : failed : '+ err, 'error', currentFileName);
              res.status(500).json({
                message: err.message
              });
            }else {
              logger.log('Function : renderSelectedTypeRecords - success', 'info', currentFileName);
              res.status(200).json(response);
            }
          });
        }else {
          logger.log('Function : renderSelectedTypeRecords- Error : failed', 'error', currentFileName);
          res.status(500).json({
            message: 'Invalid Request.'
          });
        }
      }

      //Function for search window
      function renderPost (req, res, next, type) {
        logger.log('Function: RenderPost - Start', 'info', currentFileName);
        var applicableSites = searchWindow[type || 'window'].getApplicableSites(req.user['applicableSites']);
        var options = req.body || {};
        options.requestFrom = 'api';
        options.user = req.use;
        options.clientOffSet = req.headers['client-offset'] || 0;
        options.clientTZ = req.headers['client-tz'] || 'America/Chicago';

        options.permissions = req.user['permissions'] || [];

        //Logic For Sites - Start
        if (options.levelIds && options.levelIds.length) {
          options.levelIds = _.filter((options.levelIds||[]), function(level){
            return !!level;
          });

          var siteLength = (req.user['applicableSitesList']||[]).length || req.user['applicableSites'].length;

          if(options.levelIds.length != siteLength){
            var length = siteLength-1;
            var objKey = {};
            if (req.user['applicableSitesList']) {
              objKey["levelIds."+length] = {'$in':req.user['applicableSitesList'][length]};
            } else{
              objKey["levelIds."+length] = {'$in':req.user['applicableSites'][length]};
            }
            options.customQuery = objKey;
          }

          logger.log('if (options.levelIds && options.levelIds.length): Inside levelIds filter', 'info', currentFileName);
          if (!searchWindow[type || 'window'].applicableSitesContainsSites(applicableSites, options.levelIds)) {
            res.status(403).json({
              message: 'You are not authorized.'
            });
            return;
          }
        } else if (applicableSites && applicableSites.length) {
          options.applicableSites = applicableSites;
        }
        //Logic For Sites - End
        logger.log('Function : fetchRecords: Before Calling ...', 'info', currentFileName);
        searchWindow[type || 'window'].fetchRecords(options, function (err, response) {
          if (err) {
            logger.log('Function : fetchRecords- Error : failed : '+ err, 'error', currentFileName);
            res.status(500).json({
              message: err.message
            });
          }else {
            logger.log('Function : fetchRecords - success', 'info', currentFileName);
            res.status(200).json(response);
          }
        });
      }

      //Initialize Qulification Performance
      app.use('/qulificationperformance/', apiRoutes);
    }

  })();
