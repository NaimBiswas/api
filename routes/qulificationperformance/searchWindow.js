/**
* @name Qulification Performance - Search Window Service
* @author Prachi Thakkar <prachi.t@productivet.com>
*
* @version 1.0
*/
var db = require('../../lib/db');
var _ = require('lodash');
var moment = require('moment-timezone');
var q = require('q');
var logger = require('../../logger');
var currentFileName = __filename;

function applicableSitesContainsSites(applicableSites, sites) {
  logger.log("function: applicableSitesContainsSites - start", 'info', currentFileName);
  return sites.every(function (value) {
    return (applicableSites.indexOf(value) >= 0);
  });
}

function getApplicableSites(applicableSites) {
  logger.log("function: getApplicableSites - start", 'info', currentFileName);
  return  [].concat.apply([], applicableSites);
}

//Create query for array of attributes
function windowAttributeFilter(attributes, clientTZ) {
  logger.log("function: windowAttributeFilter - start", 'info', currentFileName);
  var getFilteredAttributes = [];
  var groupedAttributes = _.groupBy(attributes, 'keys[0]') || {};

  _.forIn(groupedAttributes, function (group) {
    var matchedList = {'$or':[]};
    group.forEach(function (filter) {
      var attributeFilter = {};
      var key;
      if (filter.isEntity) {
        //For Entity
        var entityKey = filter.keys[0].substr(0, filter.keys[0].indexOf('-')) + '';
        key = 'filteredAttributes.attribute.' + entityKey;
      }else {
        //For Attribute
        var attrkey = '' + filter.keys[0];
        key = 'filteredAttributes.attribute.' + attrkey ;
      }
      attributeFilter[key] = filter.isEntity ? (filter.keys[0].substr(0, filter.keys[0].indexOf('-')) + '') : ('' + filter.keys[0]);
      matchedList['$or'].push(attributeFilter);
      if (filter.value && new Date(filter.value) != 'Invalid Date' && isNaN(Number(filter.value))) {
        var dateFilter = {};
        dateFilter['filteredAttributes.value'] = filter.value;
        matchedList['$or'].push(dateFilter);
        matchedList['$or'].push({['filteredAttributes.value']: (new Date(filter.value)).toISOString()});
        matchedList['$or'].push({['filteredAttributes.value']: moment(filter.value).tz(clientTZ).format('MM/DD/YYYY')});
      }else if(typeof filter.value !== "number" && parseFloat(filter.value) && filter.type !== 'None'){
        var numberFilter = {};
        numberFilter['filteredAttributes.value'] = parseFloat(filter.value);
        matchedList['$or'].push(numberFilter);
      } else{
        var stringFilter = {};
        stringFilter['filteredAttributes.value'] = filter.value.toString();
        matchedList['$or'].push(stringFilter);
      }
    });
    if (matchedList['$or'].length) {
      getFilteredAttributes.push(matchedList);
    }
  });
  return getFilteredAttributes;
}

//Test Type: App contains question and that quaestion contains acceptance criteria - filter for test type question
function windowTestTypeFilter(type, question) {
  var filter = {};
  if (type == 'Question') {
    filter['$or'] = [];
    if (question[0] && question[0]['qSeq'] && question[0]['pSeq']) {
      //filter['$or'].push({'testType.s#': question[0]['qSeq'], 'testType.parentId': question[0]['pSeq']});
      //filter['$or'].push({'testType.s#': question[0]['pSeq']});
      filter = ({'testType.s#': question[0]['qSeq'], 'testType.parentId': question[0]['pSeq']}); //QC3-10758 Prachi
    }
  }else if (type == 'Procedure') {
    if (question[0] && question[0]['qSeq']) {
      filter = ({'testType.s#': question[0]['qSeq']});
    }
  }
  return filter;
}

//Filter for site levels
function windowLevelIdFilter(levelIds) {
  logger.log("function: windowLevelIdFilter - start", 'info', currentFileName);
  var filter = {};
  filter['$and'] = [];
  levelIds.forEach(function(levelId, key) {
    var query = 'allicableSites.' + key + '._id'
    filter['$and'].push({[query]: levelId});
  });
  return filter;
}

//Create filter query for search window
function createFilters(options) {
  var filter = {};
  if (options && options.apps && options.apps.length) {
    filter['app._id'] = { '$in': options.apps.map(function (app) { return app.toString(); }) };
  }
  filter['$and'] = [];
  if (options.attributes && options.attributes.length) {
    var filteredAttributes = windowAttributeFilter(options.attributes,options.clientTZ);
    if (filteredAttributes && filteredAttributes.length) {
      filteredAttributes.forEach(function(attrib){
        filter['$and'].push(attrib);
      });
    }
  }
  if (options.testType && options.testType.length && options.testType[0] && Object.keys(options.testType[0]).length) {
    filter['$and'].push(windowTestTypeFilter(options.testTypeOption, options.testType));
  }
  if (options.status && options.status.length) {
    filter['$and'].push(generateStatusFilter(options.status));
  }

  if(options.windowType && options.windowType.length){
    filter['$and'].push({['windowType'] : {'$in': options.windowType.map(function (type) { return type.toString(); }) }});
  }
  if (options.dates && (options.dates.start || options.dates.end)) {
    var orArray = {['$or']: []};
    orArray['$or'].push({['windowStartDate'] :{'$gte': new Date(options.dates.start),'$lt': new Date(options.dates.end)}});
    orArray['$or'].push({['windowStartDate'] :{'$gte': options.dates.start,'$lt': options.dates.end}});
    filter['$and'].push(orArray);
  }

  if (options.levelIds && options.levelIds.length) {
    filter['$and'].push(windowLevelIdFilter(options.levelIds));
  } else if (options.applicableSites && options.applicableSites.length) {
    filter['$where'] = JSON.stringify(options.applicableSites).replace(/\s/g,'').concat('.some(function(x) {return this.allicableSites[this.allicableSites.length-1].find(function(site){return site._id == x})}, this)');
  }

  if (filter['$and'] && !filter['$and'].length) {
    delete filter['$and'];
  }

  filter.query = {
    sort : options.sort ? options.sort : { 'windowId': -1 },
    pageSize : parseInt(options.pageSize || 10),
    pageNumber : parseInt(options.pageNumber || 1),
    skip : options.pageSize * (options.pageNumber - 1),
    limit : parseInt(options.pageSize),
    clientOffSet : options.clientOffSet,
    clientTZ : options.clientTZ
  };
  return filter;
}

//Function calls from qualificationperformance/index.js for fetch searched record from created filter
function fetchRecords(options, callback) {
  logger.log("function: fetchRecords - start", 'info', currentFileName);
  try {
    var filter = createFilters(options);
    var query = _.cloneDeep(filter.query);
    delete filter.query;
    getWindowData(filter, query, function (resultWindow) {
      if (resultWindow) {
        callback(null, resultWindow);
      }
    });
  } catch (e) {
    logger.log("function: fetchRecords - catch : " + e, 'error', currentFileName);
    callback(e);
  }
}

//Get window using folloeing conventions
function getProjection(windows, callback) {
  getLatestAppTitle(windows, function(filteredApp) {
    var getUpdatedWindows = windows.reduce(function(c, v) {
      if (v.filteredAttributes) {
        var attributeIds = [];
        var timeZone = v.timeZone;
        var filteredAttributes = v.filteredAttributes.reduce(function(c, v){
          v.value = (v.type === 'Date') ? moment(v.value).tz(timeZone).format('MM/DD/YYYY') : v.value; //QC3-10962 Prachi
          // QC3-10415 by Yamuna
          if(v.attribute){
            if(_.indexOf(attributeIds,v.attribute['s#']) == -1){
              c.push({[v.attribute.title]:[v.value]});
              attributeIds.push(v.attribute['s#']);
            }else{
              // QC3-10953 by Yamuna
              var objectToPush = c.filter(function(a){
                return (Object.keys(a) && Object.keys(a)[0]) == v.attribute.title;
              });
              objectToPush[0][v.attribute.title].push(v.value);
            }
          }
          return c;
        }, []);
      }
      //QC3-10438 Prachi
      if (v.testType && (v.testType['s#'] || v.testType['comboId']) && (filteredApp[v.testType['comboId']] || filteredApp[v.testType['s#']])) {
        v.testType.title = v.testType['comboId'] ? filteredApp[v.testType['comboId']] : filteredApp[v.testType['s#']];
      }
      c.push({
        '_id': v._id,
        'ID': v.windowId,
        'title': v.title,
        'App': filteredApp[v.app._id],
        'Test Type': v.testType,
        'Sample Size': v.sampleSize,
        'Window Status': v.windowStatus.title,
        'Window Created Date': new Date(v.createdDate),
        'Window Start Date': new Date(v.windowStartDate),
        'Window End Date' : moment(new Date(v.windowPeriod.year, v.windowPeriod.month+1, 0)).format('MM/DD/YYYY'),
        'Filter': filteredAttributes ? filteredAttributes : [],
        'maximumAllowedProcessFailure' : v.maximumAllowedProcessFailure,
        'additionalTestSecondStage' : v.allowSecondStage ? v.additionalTestSecondStage : '0',
        'allowSecondStage': v.allowSecondStage,
        'windowType': v.windowType ? v.windowType : '',
        'windowPlan': v.windowPlan ? v.windowPlan : '',
        'populationSize': v.populationSize ? v.populationSize : ''
      });
      return c;
    }, []);
    callback(getUpdatedWindows);
  });
}

//QC3-10384 Prachi
function getLatestAppTitle(windows, callback) {
  //For now we have single apps multiple window, if we have multiple apps multiple window then we need to do such process
  var getAllAppId = windows.map(function(w) {
    return w.app._id.toString();
  });
  //_.uniq all appId, when multiple app selection comes in picture need to use $in query in app query
  if (getAllAppId && getAllAppId.length) {
    var filter = {};
    filter['_id'] = { '$in': (_.uniqWith(getAllAppId, _.isEqual)).map(function (appId) { return db.ObjectID(appId); }) };
    db.schema.find(filter, {'title':1, 'procedures': 1}).toArray(function(err, latestApp) {
      if (err) {
        callback(err);
      }
      var filteredApp = {};
      windows.forEach(function(oneWindow) {
        latestApp.forEach(function(app) {
          if (oneWindow.app._id == app._id) {
            filteredApp[oneWindow.app._id] = app.title;
            //QC3-10438 Prachi
            if (oneWindow.testTypeOption == 'Procedure') {
              var getProc = _.find(app.procedures, {'s#' : oneWindow.testType['s#']});
              filteredApp[oneWindow.testType['s#']] = (getProc &&getProc.title) ? getProc.title : oneWindow.testType.title;
            }else if (oneWindow.testTypeOption == 'Question') {
              var getProcForQuestion = _.find(app.procedures, {'s#' : oneWindow.testType['parentId']});
              if (getProcForQuestion && getProcForQuestion.questions) {
                if (oneWindow.testType['comboId']) {
                  var getQuestion = _.find(getProcForQuestion.questions, {'comboId' : oneWindow.testType['comboId']});
                  filteredApp[oneWindow.testType['comboId']] = (getQuestion && getQuestion.title) ? getQuestion.title : oneWindow.testType.title;
                }else {
                  var getQuestionWithoutComboId = _.find(getProcForQuestion.questions, {'s#' : oneWindow.testType['s#']});
                  filteredApp[oneWindow.testType['s#']] = (getQuestionWithoutComboId && getQuestionWithoutComboId.title) ? getQuestionWithoutComboId.title : oneWindow.testType.title;
                }
              }else {
                filteredApp[oneWindow.testType['comboId']] = oneWindow.testType.title; //QC3-11185 Prachi
              }
            }
          }
        });
      });
      callback(filteredApp);
    });
  }else {
    callback(windows[0].app.title);
  }
}


//Get window as per provided filter
function getWindowData(windowFilter, query, callback) {
  logger.log("function: getWindowData - start", 'info', currentFileName);
  var columnFilter = {'title':1, 'windowId':1, 'app':1, 'sampleSize':1, 'createdDate':1, 'filteredAttributes':1,
  'windowStartDate':1, 'maximumAllowedProcessFailure':1, 'allowSecondStage':1, 'additionalTestSecondStage':1,
  'windowStatus':1, 'testType':1, 'testTypeOption':1, 'windowType':1, 'windowPlan':1, 'populationSize':1, 'windowPeriod':1, 'timeZone': 1};
  db.windows.count(windowFilter, function(fail, count) {
    if (fail) {
      callback(fail);
      return;
    } else {
      if (count > 0) {
        db.windows.find(windowFilter, columnFilter).sort(query.sort).skip(query.skip).limit(query.limit)
        .toArray(function (err, windows) {
          if (err) {
            logger.log(err + ' : Error');
            callback(err);
          }
          if (windows && windows.length) {
            getProjection(windows, function(updatedWindow) {
              updatedWindow[updatedWindow.length] = {
                allWindows: count,
                query: query
              };
              callback(updatedWindow);
            });
          }else {
            callback({'data': 'No Records Found.'});
          }
        });
      }else {
        callback({'data': 'No Records Found.'});
      }
    }
  });
}

//Create status filter for serach window
function generateStatusFilter(allStatus) {
  logger.log("function: generateStatusFilter - start" , 'info', currentFileName);
  if (!allStatus || !allStatus.length) {
    return;
  }
  var query = {};
  query['$or'] = [];
  _.forEach(allStatus, function (status) {
    if (status) {
      query['$or'].push({'windowStatus.title': status})
    }
  });
  if (query && query['$or'].length) {
    return query;
  } else {
    return {};
  }
}

module.exports = {
  fetchRecords: fetchRecords,
  getApplicableSites: getApplicableSites,
  applicableSitesContainsSites: applicableSitesContainsSites
}
