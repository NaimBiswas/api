/**
* @name Qulification Performance - Search App Records From Window Id
* @author Prachi Thakkar <prachi.t@productivet.com>
*
* @version 1.0
*/
var db = require('../../lib/db');
var _ = require('lodash');
var moment = require('moment-timezone');
var q = require('q');
var logger = require('../../logger');
var currentFileName = __filename;

//Return unit tested app records/ pending failure app records
function fetchSelectedAppRecords(params, options, callback) {
  logger.log("function: fetchRecords - start", 'info', currentFileName);
  try {
    var appId = options.appId + '';
    if (appId) {
      db.schema.findOne({_id: db.ObjectID(appId)}, function(err, schema) {
        if (err) {
          logger.log(err + ' : Error');
          callback(err);
        }
        var recordFilter = {};
        if (options && options.recordIds && options.recordIds.length) {
          recordFilter['_id'] = { '$in': options.recordIds.map(function (rId) { return db.ObjectID(rId); }) };

          var query = {
            sort : options.sort ? options.sort : { 'createdDate': -1 },
            pageSize : parseInt(options.pageSize || 10),
            pageNumber : parseInt(options.pageNumber || 1),
            skip : options.pageSize * (options.pageNumber - 1),
            limit : parseInt(options.pageSize),
            clientOffSet : options.clientOffSet,
            clientTZ : options.clientTZ
          };
          db.collection(appId).count(recordFilter, function(fail, count) {
            if (fail) {
              callback(fail);
              return;
            } else {
              if (count > 0) {
                db.collection(appId).find(recordFilter).sort(query.sort).skip(query.skip).limit(query.limit)
                .toArray(function (err, appRecords) {
                  if (err) {
                    callback(err);
                  }
                  getWindow(params, function(windowData) {
                    if (!windowData) {
                      callback('No Records Found.');
                    }
                    getProjectedAppEntries(appRecords, schema, options, windowData, function(response) {
                      if (response) {
                        response[response.length] = {
                          allRecords: count,
                          query: query
                        }
                        callback(null, response);
                      }else {
                        callback('No Records Found.');
                      }
                    });
                  });
                });
              }else {
                callback('No Records Found.');
              }
            }
          });
        }else {
          callback('No Records Found.');
        }
      });
    }else {
      callback('No Records Found.');
    }
  } catch (e) {
    logger.log("function: fetchSelectedAppRecords - catch : " + e, 'error', currentFileName);
    callback(e);
  }
}

//Get window using speacific window id: where window id is come from req.prams(qualificationperformance/index.js)
function getWindow(params, callback) {
  db.windows.findOne({windowId: parseInt(params.windowId), _id: db.ObjectID(params.id)}, function(err, thisWindow) {
    if (err) {
      logger.log(err + ' : Error');
      callback(err);
    }
    if (thisWindow) {
      callback(thisWindow);
    }else {
      callback('No Records Found.');
    }
  });
}

//Fromated app entries, for display in table
function getProjectedAppEntries(allEntry, schema, options, windowData, callback) {
  var allProjectedEntry = [];
  allEntry.forEach(function(entry) {
    var projectedEntry =  {
      "_id": entry._id,
       "App": (entry.masterQCSettings ? (schema.title + '(Master App)') : (schema.title + '(App)'))
    };
    //Get attribute data
    _.forEach((schema.attributes||[]), function(attr) {
      if (!entry.entries[attr['s#']]) {
        projectedEntry[attr.title] = '';
        return;
      }else if (attr) {
        projectedEntry[attr.title] = displayValueByFormat(attr, entry.entries[attr['s#']]['attributes'], options.clientTZ) || '';

        if (typeof projectedEntry[attr.title] === 'object' && projectedEntry[attr.title].length) {
          projectedEntry[attr.title] = projectedEntry[attr.title].join(', ');
        }
      }else {
        projectedEntry[attr.title] = entry.entries[attr['s#']] ? entry.entries[attr['s#']]['attributes'] : '';
        if (typeof projectedEntry[attr.title] === 'object' && projectedEntry[attr.title].length) {
          projectedEntry[attr.title] = projectedEntry[attr.title].join(', ');
        }
      }
      projectedEntry[attr.title] = {
        's#' : 'entries.'+attr['s#']+'.attributes',
        value : projectedEntry[attr.title]
      };
    });

    //Get entity data
    _.forEach((schema.entities||[]), function(ent) {
      if (!entry.entries[ent.questions[0]['s#']+'-'+ent['s#']]) {
        projectedEntry[ent.title] = '';
        return;
      } else if (ent.questions[0]) {
        projectedEntry[ent.title] = displayValueByFormat(ent.questions[0], entry.entries[ent.questions[0]['s#']+'-'+ent['s#']][ent['s#']], options.clientTZ) || '';
        if (typeof projectedEntry[ent.title] === 'object' && projectedEntry[ent.title].length) {
          projectedEntry[ent.title] = projectedEntry[ent.title].join(', ');
        }
      }
      else {
        projectedEntry[ent.title] = entry.entries[ent.questions[0]['s#']+'-'+ent['s#']] ? entry.entries[ent.questions[0]['s#']+'-'+ent['s#']][ent['s#']] : '';
        if (typeof projectedEntry[ent.title] === 'object' && projectedEntry[ent.title].length) {
          projectedEntry[ent.title] = projectedEntry[ent.title].join(', ');
        }
      }
      projectedEntry[ent.title] = {
        's#' : 'entries.'+ent.questions[0]['s#']+'-'+ent['s#']+'.'+ent['s#'],
        value : projectedEntry[ent.title]
      };
    });
    projectedEntry[ windowData.testType.title + ((windowData.testTypeOption == 'Procedure') ? ' (Procedure)' : ' (Question)') +" Status"] = getStausStringAsPerSelectedTestType(entry, schema, windowData, options);
    projectedEntry["Record Status"] = getStausString(entry);

    if (options.type == 'Pending Failure Records') {
      projectedEntry["Disposition"] = 'Assign';
    }
    if (options.type == 'Non Process Failure Records') {
      projectedEntry["Failure Type"] = 'Non Process';
    }
    if (options.type == 'Process Failure Records') {
      projectedEntry["Failure Type"] = 'Process';
    }

    if(options.from == 'Reports'){
      projectedEntry["Created Date"] = moment.utc(new Date(entry.createdDate)).tz(options.clientTZ).format('MM/DD/YYYY HH:mm:ss');
      projectedEntry["Modified Date"] = moment.utc(new Date(entry.modifiedDate)).tz(options.clientTZ).format('MM/DD/YYYY HH:mm:ss');
      projectedEntry["Modified By"] = entry.modifiedByName;
    }
    // projectedEntry["Batch Entry Id"] = entry['batchEntryId'] ? entry['batchEntryId'] : 'N/A';

    // if (!entry.modifiedByName && entry.modifiedBy && options && options.users && options.users.length) {
    //   var userDetails = _.find((options.users||[]), function (user) {
    //     return user._id.toString() == entry.modifiedBy.toString();
    //   });
    //   if (userDetails) {
    //     entry.modifiedByName = (userDetails.firstname||'').concat(' ').concat(userDetails.lastname||'').concat(' (').concat(userDetails.username||'N/A').concat(')');
    //   }
    // }
    // if (entry && entry.assignment && entry.assignment.assignee) {
    //   projectedEntry["Assignee"] = entry.assignment.assignee.firstname + ' ' + entry.assignment.assignee.lastname + ' (' + entry.assignment.assignee.username + ')';
    // }else {
    //   projectedEntry["Assignee"] = 'N/A'
    // }

    allProjectedEntry.push(projectedEntry);
  });
  if (allEntry.length == allProjectedEntry.length) {
    callback(allProjectedEntry);
  }
}


//Date/ Time Format
function displayValueByFormat(c, value, clientTZ) {
  logger.log("function: displayValueByFormat - start", 'info', currentFileName);  
  var filterdDate = '';
  if (c && c.type && c.type.format && c.type.format.title==="Date" && value) {
    if (c.type.format.metadata.format === '' || !c.type.format.metadata.format ||  _.isUndefined(c.type.format.metadata.format)) {
      return moment.utc(new Date(value)).tz(clientTZ).format('MM/DD/YYYY');
    }else {
      switch(c.type.format.metadata.format){
        case 'dd/MM/yyyy':
        filterdDate ='DD/MM/YYYY';
        break;

        case 'MM/dd/yyyy':
        filterdDate = 'MM/DD/YYYY';
        break;

        case 'yyyy/MM/dd':
        filterdDate = 'YYYY/MM/DD';
        break;

        case 'MMMM/dd/yyyy':
        filterdDate = 'MMMM/DD/YYYY';
        break;

        case 'MM/dd/yy':
        filterdDate = 'MM/DD/YY';
        break;

        case 'MONTH/dd/yyyy':
        filterdDate = 'MMMM/DD/YYYY';
        break;

        case 'dd/MMM/yy':
        filterdDate = 'DD/MMM/YY';
        break;

        case 'dd/MMM/yyyy':
        filterdDate = 'DD/MMM/YYYY';
        break;

        default:
        filterdDate = 'MM/DD/YYYY';
      }
      return moment.utc(new Date(value)).tz(clientTZ).format(filterdDate);
    }
  }else if (c && c.type && c.type.format && c.type.format.title==="Time") {
    return moment.utc(new Date(value)).tz(clientTZ).format('HH:mm a');
  } else {
    return value === 0 ? '0' : value;
  }
}

//Return test type pass/fail for deciding pending failure records (if specific test type is fail than for that specific window that app record is fail)
function getStausStringAsPerSelectedTestType(entry, schema, windowData, options) {
  if (windowData.testTypeOption == 'Procedure') {
    if (windowData.testType['s#'] && entry.entries.status[windowData.testType['s#']]) {
      if (entry.entries.status[windowData.testType['s#']].status === 0) {
        if (options.from == "Reports") {
          if (entry.failure && entry.failure[windowData._id] && entry.failure[windowData._id].type) {
            return 'Failed (' + entry.failure[windowData._id].type + ')' ;
          }else {
            return 'Failed (Pending)';
          }
        }else {
          return 'Failed';
        }
      }else {
        return 'Passed';
      }
    }
  }else if (windowData.testTypeOption == 'Question') {
    if (windowData.testType.parentId && windowData.testType['s#'] &&
    entry.entries.status[windowData.testType.parentId] &&
    entry.entries[windowData.testType['s#'] +'-'+ windowData.testType.parentId] &&
    (entry.entries[windowData.testType['s#'] +'-'+ windowData.testType.parentId][windowData.testType.parentId] ||
    entry.entries[windowData.testType['s#'] +'-'+ windowData.testType.parentId][windowData.testType.parentId] == '0')) { //QC3-11012 Prachi
      if (entry.entries.status[windowData.testType.parentId][windowData.testType['s#'] +'-'+ windowData.testType.parentId] === 0) {
        return 'Failed';
      }else {
        return 'Passed';
      }
    }
  }
}

//Return actual app record status
function getStausString(app) {
  logger.log("function: getStausString - start", 'info', currentFileName);
  if (!(app.entries && app.entries.status)) {
    return 'Draft';
  }
  var status = app.entries.status.status;
  var hasPendingWorkflowReview = false;
  if (app.workflowreview && app.workflowreview.length) {
    hasPendingWorkflowReview = _.some((app.workflowreview||[]), function (review) {
      return review.reviewstatus == 1
    });
  }
  if (status==3) {
    return 'Void';
  } else if (status == 4) {
    return 'Draft';
  } else if (hasPendingWorkflowReview && status == 1) {
    return 'Review Pending - Passed';
  } else if (hasPendingWorkflowReview && status == 0) {
    return 'Review Pending - Failed';
  } else if (status == 1) {
    return 'Passed';
  }  if (hasPendingWorkflowReview && status == 2) { //QC3-7516 by bhavin..
    return 'Review Pending';
  } else {
    return 'Failed'
  }
}

module.exports = {
  fetchSelectedAppRecords: fetchSelectedAppRecords
}
