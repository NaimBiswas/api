'use strict'
/**
 * @name Groupsettings-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var array = types.array;
var rString = types.rString;
var bigString = types.bigString;
var rBool = types.rBool;
var bool = types.bool;
var rSmallString = types.rSmallString;
var rId = types.rId;
var any = types.any;
var string = types.string;

var schema = {
  title: rString.label('Groupsettings title'),
  isActive: rBool.label('Groupsettings active?'),
  displayModules: any.label('Display Modules'),
  modules:array(object({
    id: rId.label('Module schema id')
  })).required().label('Schema module'),
  forms: array(object({
    id: rId.label('Form id'),
    title: bigString.label('Form title'),
    module:rString.label('Form module'),
    isMasterForm: bool.label('isMasterForm')
  })).required().label('form object'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
