'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'groupsettings',
    collection: 'groupsettings',
    schema: schema,
    softSchema: softSchema
  }
});
/**
 * @swagger
 * definition:
 *   Forms_gs:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       module:
 *         type: string
 */

 /**
  * @swagger
  * definition:
  *   Module1:
  *     properties:
  *       id:
  *         type: integer
  */

/**
 * @swagger
 * definition:
 *   Groupsetting:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       forms:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Forms_gs'
 *       modules:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Module1'
 *       isActive:
 *         type: boolean
 *       displayModules:
 *         type: string
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       createdBy:
 *         $ref: '#/definitions/CreatedBy'
 */

/**
 * @swagger
 * /groupsettings:
 *   get:
 *     tags: [Group Settings]
 *     description: Returns all groups
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of role that you want to see in the output (Exp Values -> [ index,title,displayModules,modifiedDate,modifiedBy,isActive])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of role with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of groupsettings
 *         schema:
 *           $ref: '#/definitions/Groupsetting'
 */

 /**
  * @swagger
  * /groupsettings/{id}:
  *   get:
  *     tags: [Group Settings]
  *     description: Returns a object groupsettings
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: Group's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: pagesize
  *         description: Page Size
  *         in: query
  *         required: false
  *         type: string
  *     responses:
  *       200:
  *         description: An object of groupsettings
  *         schema:
  *           $ref: '#/definitions/Groupsetting'

  */

 /**
  * @swagger
  * /groupsettings:
  *   post:
  *     tags: [Group Settings]
  *     description: Creates a new Group
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: groupsettings
  *         description: groupsettings object
  *         in: body
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Groupsetting'
  *     responses:
  *       201:
  *         description: Successfully created
  */

 /**
  * @swagger
  * /groupsettings/{id}:
  *   patch:
  *     tags: [Group Settings]
  *     description: Updates a group
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: group's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: groupsettings
  *         description: groupsetting object
  *         in: body
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Groupsetting'
  *     responses:
  *       200:
  *         description: Successfully updated
  */

module.exports = route.router;
