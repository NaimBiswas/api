module.exports = require('./schema');
'use strict'
/**
* @name Groupsettings-schema
* @author Shoaib Ganja <shoaib.g@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var array = types.array;
var string = types.string;
var bool = types.bool;
var smallString = types.smallString;
var rBigString = types.rBigString;
var id = types.id;
var any = types.any;

var schema = {
  title: string.label('Groupsettings title'),
  isActive: bool.label('Groupsettings active?'),
  displayModules: any.label('Display Modules'),
  modules:array(object({
    id: id.label('Module schema id')
  })).label('Schema module'),
  forms: array(object({
    id: id.label('Form id'),
    title: rBigString.label('Form title'),
    module:string.label('Form module'),
    isMasterForm: bool.label('isMasterForm')
  })).label('form object'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
