'use strict'
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function homeRouteCallback(req, res, next) {
  res.render('index', { title: 'QC 3.0' });
});


module.exports = router;
