/**
 *
 * author - akshdeep.s
 * referece - chandni.p
 * Implementation of swagger for project -api
 */


(function () {
    'use strict';
    var config = require('../../config/config');
    var swaggerJSDoc = require('swagger-jsdoc');
    var swagger = require("swagger-node-express");
    var express = require('express');
    var db = require('../../lib/db');
    var crypto = require('crypto');
    var session = require('express-session');

    module.exports = function (app) {
        app.use(session({
            saveUninitialized: true,
            resave: true,
            secret: "mySecret"
        }));
        app.set('view engine', 'ejs');
        app.use(express.static('views'));
        var apiRoutes = express.Router();
        //configure the

        //1 : projectSwagger --
        var projectSwaggerOptions = {
            swaggerDefinition: {
                info: {
                    title: 'PERFEQTA - Project Api Documentation', // Title (required)
                    version: '1.0.0', // Version (required)
                },
                host: process.env.NODE_ENV === 'development' ? '' : config.host,
                basePath: process.env.NODE_ENV === 'development' ? '' : config.basePath,
                securityDefinitions: {
                    "apiKey": {
                        "type": "apiKey",
                        "description": "Authorization Token",
                        "name": "apiKey",
                        "in": "header"
                    }
                }
            },
            apis: ['./routes/apiProject/appController.js', './routes/apiProject/entityController.js', './routes/apiProject/scheduleControlller.js'
            ] // Path to the API docs
        };
        var projectSwaggerSpec = swaggerJSDoc(projectSwaggerOptions);
        app.get('/project-api.json', function (req, res) {
            res.setHeader('Content-Type', 'application/json');
            res.send(projectSwaggerSpec);
        });

        //2 : AppSwagger --
        var appSwaggerOptions = {
            swaggerDefinition: {
                info: {
                    title: 'PERFEQTA - Project Api Documentation', // Title (required)
                    version: '1.0.0', // Version (required)
                },
                host: process.env.NODE_ENV === 'development' ? '' : config.host,
                basePath: process.env.NODE_ENV === 'development' ? '' : config.basePath,
                securityDefinitions: {
                    "apiKey": {
                        "type": "apiKey",
                        "description": "Authorization Token",
                        "name": "apiKey",
                        "in": "header"
                    }
                }
            },
            apis: ['./routes/qcentryactivity/qcentryactivity.js', './routes/index.js', './routes/users/users.js', './routes/thresholds/thresholds.js', './routes/attributes/attributes.js', './routes/sets/sets.js', './routes/entities/entities.js', './routes/questions/questions.js', './routes/procedures/procedures.js', './routes/roles/roles.js', './routes/schedules/schedules.js', './routes/masterqcsettings/masterqcsettings.js', './routes/loginattempts/loginattempts.js', './routes/batchentrysettings/batchentrysettings.js', './routes/groupsettings/groupsettings.js', './routes/reportPermissionConfiguration/reportPermissionConfiguration.js', './routes/schema/schema.js', './routes/sites/sites.js', './routes/modules/modules.js', './routes/siteNames/siteNames.js', './routes/generalSettings/generalSettings.js', './routes/entityRecords/entityRecords.js', './routes/reportConfiguration/reportConfiguration.js', './routes/types/types.js', './routes/drilldown/drilldown.js', './routes/controlchart/controlchart.js', './routes/schemaSyncDetails/schemaSyncDetails.js', './routes/calendar/calendar.js', './routes/entries/entries.js', './routes/manualimport/manualimport.js', './routes/importfile/importfile.js', './routes/csventries/csventries.js', './routes/versions/versions.js', './routes/apiProject/appController.js', './routes/searchFavorites/searchFavorites.js', './routes/status/status.js', './routes/appAccessConfig/appAccessConfig.js', './routes/pluginlist/pluginlist.js', './routes/linkedTbl/linkedTbl.js', './routes/reportTemplates/reportTemplates.js', './routes/autoSync/autoSync.js', './routes/plugin/plugin.js', './routes/pluginConfig/pluginConfig.js', './routes/ReportSchemaColumns/ReportSchemaColumns.js', './routes/pluginTempRecords/pluginTempRecords.js', './routes/mappedLinkToEntity/mappedLinkToEntity.js'] // Path to the API docs/          
             // Path to the API docs
        };
        var appSwaggerSpec = swaggerJSDoc(appSwaggerOptions);
        app.get('/app-api.json', function (req, res) {
            res.setHeader('Content-Type', 'application/json');
            res.send(appSwaggerSpec);
        });
        //------------------------------------------------------------------------------------------------//
        swagger.setApiInfo({
            title: "PERFEQTA PROJECT API",
            description: "API to do something, manage something...",
            termsOfServiceUrl: "",
            contact: "akashdeep.s@productivet.com",
            license: "",
            licenseUrl: ""
        });

        apiRoutes.get('/:type', function (req, res, next) {
            //check for the token here, if user has toke for api, then send them api, else app.
            //type = project and app
            console.log("get in doc");
            var type = req.params.type;
            if (type == 'app') {
                //check for the authentication
                if (req.session.user) {
                    res.render('doc/index.ejs', { url: type + "-api.json", auth: true, type: type, token: req.session.user.token });
                } else {
                    res.render('doc/index.ejs', {
                        url: '',
                        auth: false,
                        type: type,
                        token: ''
                    });
                }
            }
            else if (type == 'project') {
                res.render('doc/index.ejs', { url: type + "-api.json", auth: true, type: type, token: '' });
            }
            else {
                res.render('doc/index.ejs', { url: '', auth: false, type: type, token: '' });
            }

        });
        apiRoutes.post('/:type', function (req, res, next) {
            //check for the user and just return him token, tha's it/.
            var type = req.params.type;
            var loginCredsObj = {
                username: req.body.username.toLowerCase(),
                $or: [{ temporaryPassword: req.body.password }, { password: crypto.createHash('sha1').update(req.body.password).digest('hex') }],
                isActive: true,
                status: { $in: ['active', 'signingup', 'recovering-password', 'reseting-password'] }
            }
            db.users.findOneAsync(loginCredsObj).then(function (user) {
                if (!user) {
                    res.render('doc/index.ejs', { url: '', auth: false, type: type, message: 'username password is wrong.', token: '' });
                }
                else {
                    //get token
                    db.allAuthToken.findOneAsync({ userId : user._id }).then(function (token) {
                       if (token) {
                            req.session.user = user;
                            req.session.user.token = token.token;
                            res.render('doc/index.ejs', { url: type + "-api.json", auth: true, type: type, token: user.token });
                        }else{
                             res.render('doc/index.ejs', { url: '', auth: false, type: type, message: 'username password is wrong.', token: '' });
                        }
                    });

                }
            }).catch(function (err) {
                res.json(err);
            });

        });
        apiRoutes.get('/app/logout', function (req, res, next) {
            req.session.user = undefined;
            res.redirect('/doc/app');
        });

        app.use("/doc", apiRoutes);
        swagger.setAppHandler(apiRoutes);
        swagger.configureSwaggerPaths('', 'api-docs', '');
    }
})();
