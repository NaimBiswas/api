'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var validate = require('../../lib/validator');
var db = require('../../lib/db');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'modules',
    collection: 'modules',
    schema: schema.schema,
    softSchema: softSchema
  }
});
/**
 * @swagger
 * definition:
 *   Modules_module:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       isActive:
 *         type: boolean
 *       allowSpecialSettings:
 *         type: boolean
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       version:
 *           type: integer
 *       isMajorVersion:
 *           type: boolean
 */

/**
 * @swagger
 * /modules:
 *   get:
 *     tags: [General]
 *     description: Returns all modules
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ title,id])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of modules
 *         schema:
 *           $ref: '#/definitions/Modules_module'
 */

/**
 * @swagger
 * definition:
 *   ModuleModel:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       isActive:
 *         type: boolean
 *       allowSpecialSettings:
 *         type: boolean
 *       noOfConCurrentUsers:
 *         type: string
 */

/**
 * @swagger
 * /modules:
 *   get:
 *     tags: [Module]
 *     description: Returns all modules
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,username,displayroles,firstname,lastname,Reset,modifiedDate,modifiedBy,isActive])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of modules
 *         schema:
 *           $ref: '#/definitions/ModuleModel'

 */

var router = route.router;

router.get('/noOfConCurrentUsers/:noOfConCurrentUsers', validate(schema.noOfConCurrentUsersSchema), function (req, res, next) {
  logger.log("API Called : api/modules :GET method" , 'info', currentFileName);

  db.modules.updateMany({}, {$set: {"noOfConCurrentUsers" : req.items.noOfConCurrentUsers}}, function(err, value){
    if(err){
      logger.log("collection :modules -updateMany- Failed : " + err , 'error', currentFileName);
      return res.status(422).json({'message': 'Failed to execute step. The Code is invalid or expired.'});
    }
    res.status(200).json([]);
  });
});



module.exports = router;
