'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var rString = types.rString;
var string = types.string;
var rBool = types.rBool;
var rNumber = types.rNumber;

var schema = {
  title: rString.label('Module title'),
  isActive: rBool.label('Module active?'),
  allowSpecialSettings:rBool.label('Allow Special Settings'),
  timeZone:string.label('Time Zone'),
  noOfConCurrentUsers: rString.label('Number of Concurrent Users') // Needed at module publised time.
};

var noOfConCurrentUsersSchema = {
  noOfConCurrentUsers: rNumber.label('noOfConCurrentUsers')
};

module.exports = {
  schema: schema,
  noOfConCurrentUsersSchema: noOfConCurrentUsersSchema
}
