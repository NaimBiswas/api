'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var string = types.string;
var bool = types.bool;

var schema = {
  title: string.label('Module title'),
  isActive: bool.label('Module active?'),
  allowSpecialSettings:bool.label('Allow Special Settings'),
  timeZone:string.label('Time Zone'),
  noOfConCurrentUsers: string.label('Number of Concurrent Users') // Needed at module publised time.
};

module.exports = schema;
