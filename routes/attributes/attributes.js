'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var _ = require('lodash');
var linkedTbl = require('../../lib/linkedTbl');
var logger = require('../../logger'); //QC3-8939 Prachi(already resolved)
var currentFileName = __filename;

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'attributes',
    collection: 'attributes',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    PATCH: {
      ONE: {
        after: afterPatch
      },
      ONESOFT: {
        after: afterPatch
      }
    }
  }
});
function afterPatch(req, res, next) {
  logger.log("function: afterPatch - start", 'info', currentFileName);
  linkedTbl.insertInLinked('attributes', req.data);
  res.status(200).json(req.data);
}

//Chandni : Code start for Swagger

/**
 * @swagger
 * definition:
 *   ModifiedBy:
 *     properties:
 *       id:
 *         type: integer
 *       username:
 *         type: string
 *       firstname:
 *         type: string
 *       lastname:
 *         type: string
 */



/**
 * @swagger
 * definition:
 *   Version:
 *     properties:
 *       version:
 *         type: integer
 *       record:
 *         type: string
 */


/**
 * @swagger
 * definition:
 *   Attribute:
 *     properties:
 *       title:
 *         type: string
 *       version:
 *         type: string
 *       modifiedDate:
 *         type: dateTime
 *       isActive:
 *         type: boolean
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       type:
 *         $ref: '#/definitions/Type'
 *       module:
 *         $ref: '#/definitions/Module'
 *       validation:
 *         $ref: '#/definitions/Validation'
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 */


/**
 * @swagger
 * /attributes:
 *   get:
 *     tags: [Attribute]
 *     description: Returns all attributes
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: q[module._id]
 *         description: Module Id
 *         in: query
 *         required: false
 *         type: string
 *       - name: q[isActive]
 *         description: Is Active
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [title,type.title,modifiedDate,modifiedBy,version,versions])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of users
 *         schema:
 *           $ref: '#/definitions/Attribute'

 */

/**
 * @swagger
 * /attributes/{id}:
 *   get:
 *     tags: [Attribute]
 *     description: Returns all attributes
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Attribute's id
 *         in: path
 *         required: true
 *         type: integer
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An object of attribute
 *         schema:
 *           $ref: '#/definitions/Attribute'

 */

/**
 * @swagger
 * /attributes/{id}/draft:
 *   patch:
 *     tags: [Attribute]
 *     description: Updates a Attribute
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Attribute's id
 *         in: path
 *         required: true
 *         type: integer
 *       - name: attribute
 *         description: Attribute object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/Attribute'
 *     responses:
 *       200:
 *         description: Successfully updated
 */
//Chandni : Code end for Swagger

module.exports = route.router;
