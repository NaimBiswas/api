'use strict'
/**
 * @name modules-schema
 * @author Amit Dave <dave.a@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var rBool = types.rBool;
var rNumber = types.rNumber;
var string = types.string;
var smallString = types.smallString // QC3-10659 - Jyotil;
var required = types.required;
var any = types.any;
var bool = types.bool;


var schema = object({
  title: rString.label('Attribute title'),
  type: object({
    _id: rId.label('Type id'),
    title: rString.label('Type title'),
    format: object({
      _id: rId.label('Format id'),
      title: rString.label('Format title'),
      metadata: any.label('Metadata required for the format')
    })
  }).required().label('Attribute type'),
  isActive: rBool.label('Active'),
  module: object({
    _id: rId.label('Module id'),
  }).required().label('Modules of module object'),
  addBarcodeRule: bool.label('Add Barcode Rule'),
  addBarcodeRulePrefix: any.label('Add Barcode Rule Prefix').allow(''),
  addBarcodeRuleSuffix: any.label('Add Barcode Rule Suffix').allow(''),
  autoGenerateNumberPrefix: smallString.label('Auto Generate Number Prefix').allow(''), // QC3-10659 - Jyotil
  autoGenerateNumberSuffix: smallString.label('Auto Generate Number Suffix').allow(''), // QC3-10659 - Jyotil
  validation: any,
  timeZone: string.label('Time Zone')
});

module.exports = schema;
