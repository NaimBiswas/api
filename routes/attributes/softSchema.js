'use strict'
/**
* @name modules-schema
* @author Amit Dave <dave.a@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var id = types.id;
var string = types.string;
var array = types.array;
var bool = types.bool;
var number = types.number;
var smallString = types.smallString; // QC3-10659 - Jyotil
var required = types.required;
var any = types.any;


var schema = object({
  title: string.label('Attribute title'),
  type: object({
    _id: id.label('Type id'),
    title: string.label('Type title'),
    format: object({
      _id: id.label('Format id'),
      title: string.label('Format title'),
      metadata: any.label('Metadata required for the format')
    })
  }).label('Attribute type'),
  isActive: bool.label('Active'),
  module: object({
    _id: id.label('Module id'),
    title: string.label('Module title')
  }).label('Modules of module object'),
  addBarcodeRule: bool.label('Add Barcode Rule'),
  addBarcodeRulePrefix: any.label('Add Barcode Rule Prefix').allow(''),
  addBarcodeRuleSuffix: any.label('Add Barcode Rule Suffix').allow(''),
  autoGenerateNumberPrefix: smallString.label('Auto Generate Number Prefix').allow(''), // QC3-10659 - Jyotil
  autoGenerateNumberSuffix: smallString.label('Auto Generate Number Suffix').allow(''), // QC3-10659 - Jyotil
  validation: any,
  timeZone: string.label('Time Zone')
});

module.exports = schema;
