'use strict'
/**
 * @name roles-softSchema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var array = types.array;
var id = types.id;
var string = types.string;
var bool = types.bool;
var bigString = types.bigString;
var any = types.any;

var schema = {
  title: string.label('Title'),
  isActive: bool.label('Active?'),
  description: any.label('Description').allow(''),
  modules: array(object({
    _id: id.label('id'),
    permissions: array(object({
      _id: id.label('From id'),
      title: string.label('Title'),
      entity: string.label('Entity'),
      entityType: string.label('Entity Type'),
      view: bool.label('Permissions View'),
      create: bool.label('Permissions Create'),
      edit: bool.label('Permissions Edit'),
      review: bool.label('Permissions Review'),
      editSchema: bool.label('Permissions editSchema'),
      assignment: bool.label('Permissions assignment')
    })).label('Permissions Array')
  })).label('modules Array'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
