'use strict'
/**
 * @name roles-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */

var types = require('../../lib/validator/types');

var rString = types.rString;
var rBool = types.rBool;
var bigString = types.bigString;
var any = types.any;
var string = types.string;

var schema = {
  title: rString.label('Roles Title'),
  isActive: rBool.label('Active?'),
  description: any.label('Description').allow(''),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
