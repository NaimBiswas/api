'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var db = require('../../lib/resources').db;
var model = require('../../lib/oauth/model');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'roles',
    collection: 'roles',
    schema: schema,
    softSchema: softSchema
  }
  // ,
  // types:{
  //   POST:{
  //     ONE: {
  //       after : afterPatch
  //     }
  //   },
  //   PATCH:{
  //     ONE: {
  //       after : afterPatch
  //     }
  //   }
  // }
});

  /**
   * @swagger
   * definition:
   *   Permission:
   *     properties:
   *       id:
   *         type: integer
   *       title:
   *           type: string
   *       entity:
   *           type: string
   *       view:
   *           type: boolean
   *       create:
   *           type: boolean
   *       edit:
   *           type: boolean
   */

  /**
   * @swagger
   * definition:
   *   Module_roles:
   *     properties:
   *       id:
   *         type: integer
   *       permissions:
   *           type: array
   *           items:
   *              type: object
   *              allOf:
   *              - $ref: '#/definitions/Permission'
   */

/**
 * @swagger
 * definition:
 *   Role:
 *     properties:
 *       id:
 *         type: integer
 *       isActive:
 *         type: boolean
 *       title:
 *         type: string
 *       description:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       version:
 *           type: integer
 *       isMajorVersion:
 *           type: boolean
 *       modules:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Module_roles'
 */
/**
 * @swagger
 * /roles:
 *   get:
 *     tags: [Role]
 *     description: Returns all roles
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of role that you want to see in the output (Exp Values -> [ title,modifiedDate,modifiedBy,isActive])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of role with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of role
 *         schema:
 *           $ref: '#/definitions/Role'

 */

 /**
  * @swagger
  * /roles/{id}:
  *   get:
  *     tags: [Role]
  *     description: Returns a object role
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: Role's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: pagesize
  *         description: Page Size
  *         in: query
  *         required: false
  *         type: string
  *     responses:
  *       200:
  *         description: An object of role
  *         schema:
  *           $ref: '#/definitions/Role'

  */

 /**
  * @swagger
  * /roles:
  *   post:
  *     tags: [Role]
  *     description: Creates a new role
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: role
  *         description: Role object
  *         in: body
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Role'
  *     responses:
  *       201:
  *         description: Successfully created
  */
  /**
   * @swagger
   * /roles/{id}:
   *   patch:
   *     tags: [Role]
   *     description: Updates a role
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: id
   *         description: Role's id
   *         in: path
   *         required: true
   *         type: integer
   *       - name: role
   *         description: role object
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/Role'
   *     responses:
   *       200:
   *         description: Successfully updated
   */
function afterPatch(req, res, next) {
  db['users'].findOne({_id: db.ObjectID(req.user._id)}, function(e,u) {
    if(e) { next; }
    if(u){
      model.generateToken('roles', {'user': u}, function (error, token) {
        if(error) { next; };
        var response = {
          token_type: 'bearer',
          access_token: token
        };
        res.set({'Cache-Control': 'no-store', 'Pragma': 'no-cache'});
        res.jsonp(response);
        next;
      });
    }
  });
}

module.exports = route.router;
