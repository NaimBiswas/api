'use strict'
var types = require('../../lib/validator/types');
var db = require('../../lib/db');
var tdb = require('../../lib/db/tdb.js');
//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'autoSyncHistory',
    collection: 'autoSyncHistory',
    schema: types.any,
    softSchema: types.any
  }
});

module.exports = route.router;
