'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var settings = require('../../lib/settings');
var config = require('../../lib/config');
var securityProtocols = require('../../lib/securityProtocols/oAppAccessToken');
var db = require('../../lib/db');
var crypto = require('crypto')
  , shasum = crypto.createHash('sha1');
var _ = require('lodash');
var mailer = require('../../lib/mailer');
var moment = require('moment');
var logger = require('../../logger');
var currentFileName = __filename;

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'appAccessConfig',
    collection: 'appAccessConfig',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    POST: {
      ONE: {
        before: beforePostForGenerateToken,
        after: afterPost
      }
    },
    PATCH: {
      ONE: {
        before: beforePut,
        after: afterPost
      }
    }
  }
});

var debug = route.debug;
function afterPost(req, res, next) {
  logger.log("function: afterPost - start", 'info', currentFileName);
  //req.data contains the posted / patched
  //And all, orignal object can be accessed from
  //req.items
  var appAccessConfig = req.data;

  if (appAccessConfig.isActive) {
    sendLink(appAccessConfig);
  }

  db.allAuthToken.findOneAsync({ token: appAccessConfig.token }).then(function (result) {
    logger.log("collection: allAuthToken findOneAsync", 'info', currentFileName);
    if (!result) {
      db.allAuthToken.insertAsync({
        token: appAccessConfig.token ? appAccessConfig.token : generateToken(appAccessConfig.body),
        user: appAccessConfig.user,
        expireDate: '',
        usersUUID: req.user ? req.user.usersUUID : '',
        lastUsed: new Date(),
        tokenType: 'App'
      });
    }

    res.status(req.status || 201).json(req.data);

  });


}

function sendLink(appAccessConfig) {
  logger.log("function: sendLink - start", 'info', currentFileName);
  if (appAccessConfig.emails && appAccessConfig.emails.length) {
    Promise.all(appAccessConfig.emails.map(function (item) {
      if (item.hasSelected && !item.isDeleted) {
        mailer.send('share-app-link-email', _.cloneDeep(appAccessConfig), item.email, function sendMailCallback(e, b) {
          if (e) {
            logger.log("function: mailer send share-app-link-email " + e, 'error', currentFileName);
            //console.log(e);
          } else {
            console.log('App link has sent to ' + item.email);
          }
        });
      }
    })).then(function (res) {
      debug('App link has sent successfully ');
    });
  }

}

function beforePostForGenerateToken(req, res, next) {
  logger.log("function: beforePostForGenerateToken - start", 'info', currentFileName);

  var allAuthToken = {
    token: req.body.token ? req.body.token : generateToken(req.body),
    user: req.body.user,
    expireDate: '',
    usersUUID: req.user ? req.user.usersUUID : '',
    lastUsed: new Date(),
    tokenType: 'App'
  };
  req.items.token = allAuthToken.token;
  generateLinks(req);
  // req.items.link = '/oapp?token='+allAuthToken.token;
  // _.forEach((req.items.emails||[]), function (mail) {
  //   mail.link = '/oapp?token='+allAuthToken.token +'&email='+mail.email;
  // });

  db.allAuthToken.insertAsync(allAuthToken).then(function (data) {
    logger.log("collection: allAuthToken insertAsync", 'info', currentFileName);
    next();
  });

}

function beforePut(req, res, next) {
  generateLinks(req);
  next();
}

function generateLinks(req) {
  logger.log("function: generateLinks - start", 'info', currentFileName);
  // req.items.link = '/oapp?token='+req.items.token;
  if (req.items.isForPerEmail) {
    _.forEach((req.items.emails || []), function (mail) {
      mail.allowed = req.items.noOfTimeAllowed;
      // mail.link = '/oapp?token='+req.items.token +'&email='+mail.email;
    });
  }

}

function generateToken(model) {
  logger.log("function: generateToken - start", 'info', currentFileName);
  if (!model) {
    return;
  }
  if (model.token) {
    return model.token
  }
  var input = {
    appId: model.app._id,
    userId: model.user._id
  }

  var token = securityProtocols.generateOAppToken(input);
  model.token = token;

  return token;
}

/**
 * @swagger
 * definition:
 *   User:
 *     properties:
 *        id:
 *          type: integer
 *        username:
 *          type: string
 *        firstname:
 *          type: string
 *        lastname:
 *          type: string
 */

/**
 * @swagger
 * definition:
 *    Emails:
 *      properties:
 *        email:
 *          type: string
 *        isDeleted:
 *          type: boolean
 *        hasSelected:
 *          type: boolean
 *        linkShared:
 *          type: boolean
 */

 /**
  * @swagger
  * definition:
  *    Module:
  *      properties:
  *         id:
  *           type: integer
  *         title:
  *           type: string
  */

/**
  * @swagger
  * definition:
  *    App:
  *      properties:
  *         id:
  *           type: integer
  *         title:
  *           type: string
  */

/**
 * @swagger
 * definition:
 *   Version:
 *     properties:
 *       version:
 *         type: integer
 *       record:
 *         type: string
 */

/**
 * @swagger
 * definition:
 *   Tried:
 *      properties:
 *        xAgent:
 *          type: integer
 */

/**
 * @swagger
 * definition:
 *   AppAccess:
 *      properties:
 *        id:
 *          type: string
 *        user:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *             - $ref: '#/definitions/User'
 *        isActive:
 *           type: boolean
 *        allowMultiple:
 *           type: boolean
 *        token:
 *           type: any
 *        message: 
 *           type: string
 *        emails:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *               - $ref: '#/definitions/Emails'
 *        noOfTimeUsed:
 *           type: integer
 *        module:
 *           type: array
 *           items: 
 *             type: object
 *             allOf:
 *             - $ref: '#/definitions/Module'
 *        app:
 *           type: array
 *           items:
 *             type: object
 *             allOf:
 *             - $ref: '#/definitions/App'
 *        createdBy:
 *           type: integer
 *        modifiedBy:
 *           type: integer
 *        createdByName:
 *           type: string
 *        modifiedByName:
 *           type: string
 *        createdDate:
 *           type: dateTime
 *        modifiedDate:
 *           type: dateTime
 *        versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *        version:
 *           type: integer
 *        isMajorVersion:
 *           type: boolean
 *        tried:
 *           type: array
 *           items: 
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Tried'
 *    
 */

/**
 * @swagger
 * /appAccessConfig:
 *   get:
 *     tags: [Administration]
 *     description: Gives details regarding the one accessing apps
 *     produces:
 *        - application/json
 *     parameters:
 *        - name: page
 *          description: Page Number
 *          in: query
 *          required: false
 *          type: string
 *        - name: pagesize
 *          description: Page Size
 *          in: query
 *          required: false
 *          type: string
 *        - name: select
 *          description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [title,type.title,modifiedDate,modifiedBy,version,versions])
 *          in: query
 *          required: false
 *          type: string
 *        - name: sort
 *          description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
 *          in: query
 *          required: false
 *          type: string
 *     responses:
 *        200:
 *          description: An array of details regarding the one accessing apps 
 *          schema:
 *             $ref: '#/definitions/AppAccess'
 */         
module.exports = route.router;
