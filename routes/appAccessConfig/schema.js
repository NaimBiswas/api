'use strict'
/**
 * @name appAccessConfig-schema
 * @author Lokesh Boran <lokesh.b@productivet.com>
 *
 * @version 1.0
 */

var types = require('../../lib/validator/types');

var mediumBigString = types.mediumBigString;
var rBigString = types.rBigString;
var bigString = types.bigString;
var string = types.string;
var array = types.array;
var date = types.date;
var number = types.number;
var any = types.any;
var optionalArray = types.optionalArray;
var rId = types.rId;
var rString = types.rString;
var extraLargeString = types.extraLargeString;
var rExtraLargeString = types.rExtraLargeString;
var rBool = types.rBool;
var bool = types.bool;
var object = types.object.bind(types);

var schema = {
  app: object({
    _id: rId.label('App Id'),
    title: rString.label('App Title'),
    version: number.label('Version')
  }),
  module: object({
    _id: rId.label('Module Id'),
    title: string.label('Module Title')
  }),
  noOfTimeAllowed : number.label('No Of Time Allowed'),
  noOfTimeUsed : number.label('No Of Time Used'),
  isActive: rBool.label('Active'),
  allowMultiple: rBool.label('Allow Multiple'),
  isForPerEmail: bool.label('Is For Per Email'),
  token: extraLargeString.label('Token'),
  // link: extraLargeString.label('Link').allow(''),
  message: mediumBigString.label('Special Message').allow(''),//QC3-6925: by mahammad
  emails: optionalArray(object({
    email: rExtraLargeString.label('Emails'),
    // link: extraLargeString.label('link').allow(''),
    allowed: number.label('Total Allowed'),
    used: number.label('Total Used'),
    hasSelected: bool.label('Select For Link Sharing').default(false),
    isDeleted: rBool.label('Deleted'),
    linkShared: bool.label('Link Shared')
  })),
  user: object({
    _id: rId.label('User Id'),
    username: rString.label('Username'),
    firstname: string.label('First Name'),
    lastname: string.label('Last Name')
  }),
  expireDate: date.label('Expire Date').allow(''),
  lastUsed : date.label('Last Used Date'),
  tried: optionalArray({
    xAgent: string.label('host')
  }).label('Tried'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
