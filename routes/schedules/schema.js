'use strict'
/**
 * @name schedules-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var rBool = types.rBool;
var rDate = types.rDate;
var date = types.date;
var string = types.string;
var bool = types.bool;
var bigString = types.bigString;
var required = types.required;
var forbidden = types.forbidden;
var stringArray = types.stringArray;
var rNumber = types.rNumber;
var number = types.number;
var optionaldate = types.optionaldate;
var anyArray = types.anyArray;
var any = types.any;
var extraLargeString = types.extraLargeString;
var id = types.id;


var schema = {

  title: rString.label('Schedule title'),
  schema: object({
    _id: rId.label('Schema id'),
    title: rString.label('Schema title'),
    isActive :bool.label('is Active?')
  }).label('Schema object'),
  master: object({
    _id: rId.label('Master id'),
    title: rString.label('Master title'),
    isActive :bool.label('is Active?')
  }).label('Master object'),
  frequency: object({
    type: rString.label('Frequency type'),
    subType: rString.label('SubType type'),
    daily: object({
      repeats: object({
        every: number.label('Daily Every')
        .when('type', {is:'Daily', then:required()})
        .when('subType', {is:'EveryBusinessDay', then:forbidden()}),
        everyBusinessDay: string.label('Every Business Day')
        .when('subType', {is:'EveryBusinessDay', then:required()})
        .when('subType', {is:'Every', then:forbidden()})
      })
    }).when('type', {is:'Daily', then:required(), otherwise: forbidden()}).label('Daily object'),
    hourly: object({
      repeats: object({
        every: number.label('hourly Every')
        .when('type', {is:'Hourly', then:required()}),
      })
    }).when('type', {is:'Hourly', then:required(), otherwise: forbidden()}).label('hourly object'),
    weekly: object({
      repeats: object({
        every: rNumber.label('Weekly Every'),
        days: stringArray.label('Weekly days')
      })
    }).when('type', {is:'Weekly', then:required(), otherwise: forbidden()}).label('Weekly object'),
    monthly: object({
      repeats: object({
        dayOfMonth: object({
          day: rNumber.label('DayOfMonth Day'),
          month: rNumber.label('DayOfMonth Month'),
        }).when('subType', {is:'EveryMonthly', then:required()}).label('DayOfMonth object'),
        dayOfWeek: object({
          every: rString.label('DayOfWeek Every'),
          day: rString.label('DayOfWeek Day'),
          month: rNumber.label('DayOfWeek Month'),
        }).when('subType', {is:'WeeklyMonthly', then:required()}).label('DayOfWeek object')
      })
    }).when('type', {is:'Monthly', then:required(), otherwise: forbidden()}).label('Monthly object')
  }).required().label('Frequency object'),
  startDate: rDate.label('Start date'),
  occurenceState : rBool.label('occurenceState'),
  endDate: optionaldate.label('End date')
  .when('occurenceState',  {is:'false',then: required()}),
  occurence: number.label('Occurence')
  .when('occurenceState',  {is:'true',then: required()}),
  time: string.label('Schedule time'),
  reminders: object({
    enabled: rBool.label('Reminders enabled'),
    participants: stringArray.label('Participants reminder')
      .when('enabled', {is:true, then:required()})
  }),
  requiredTimeFrequency:any.label('Required Time To Perform Frequency'),
  requiredTimeFrequencytype:any.label('Required Time Type To Perform Frequency'),
  email: any.label('Email'),
  parentSchemaVersion: number.label('parent schema version number'),
  comments: any.label('Comments').allow(''),
  attribute: any,
  //QC3-8815-Backlog - Schedule - Remove the 'Review Schedule' tab and rename other tabs
  // isSetReviewFrequency : rBool.label('is Set Review Frequency?'),
  // reviewdays: number.label('review Days')
  //             .when('subType', {is:'isSetReviewFrequency', then:required()}).label('Review days'),
  // reviewtype : string.label('Review type')
  //               .when('subType', {is:'isSetReviewFrequency', then:required()}).label('Review type'),
  // // reviewtime : string.label('Review time'),
  // reviewemail: any.label('Review Email'),
  workflowreview : any,
  ismasterQcForm :rBool.label('is master QcForm?'),
  masterQCSettingsId : id.label('master qcsettings id').allow(null,""),
  isActive :rBool.label('is Active?'),
  isdst:rBool.label('Is DST?'),
  timeZone: string.label('Time Zone'),
  scheduleInProcess: bool.label('Schedule in process'),
  allowToSkip: rBool.label('Allow to skip')
};

module.exports = schema;
