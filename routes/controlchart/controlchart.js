'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var request = require('request');
var config = require('../../config/config');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'controlchart',
    collection: 'controlchart',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    GET: {
      ONE: {
        after: doAction
      }
    },
    PATCH: {
      ONE: {
        after: doActionRemoved
      }
    }
  }
});

function doAction(req, res, next) {
  console.log(req.ip);
  console.log("---------------------------------------------------------------------------------");
  console.log("controlchart opened");
  console.log("---------------------------------------------------------------------------------");
  var dataObj = {
    user: req.user,
    data: req.data,
    ip: req.ip,
    type: "Control Charts"
  }
  var options = {
    url: config.reportUrl + "/bilogs",
    method: "GET",
    body: dataObj,
    json: true
  }
  request(options, function (error, response, body) {
    if (error) {
      console.log(error);
    } else {
      console.log("dump data");
    }
  });
  res.status(200).json(req.data);
}


function doActionRemoved(req, res, next) {
 
  console.log("---------------------------------------------------------------------------------");
  console.log("Controlchart removed");
  console.log("---------------------------------------------------------------------------------");
  req.data.action = "Removed";
  var dataObj = {
    user: req.user,
    data: req.data,
    ip: req.ip,
    type: "Control Charts",
  }
  var options = {
    url: config.reportUrl + "/bilogs",
    method: "GET",
    body: dataObj,
    json: true
  }
  request(options, function (error, response, body) {
    if (error) {
      console.log(error);
    } else {
      console.log("dump data");
    }
  });
  res.status(200).json(req.data);
}

/**
 * @swagger
 * definition:
 *   ParamXSeries:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       type:
 *         $ref: '#/definitions/Type'
 *       version:
 *           type: integer
 *       isActive:
 *           type: boolean
 *       validation:
 *         $ref: '#/definitions/Validation'
 *       module:
 *         $ref: '#/definitions/Module'
 *       s#:
 *         type: integer
 */

 /**
  * @swagger
  * definition:
  *   ParamYSeries:
  *     properties:
  *       id:
  *         type: integer
  *       title:
  *         type: string
  *       type:
  *         $ref: '#/definitions/Type'
  *       version:
  *           type: integer
  *       s#:
  *         type: integer
  *       comboId:
  *         type: integer
  *       parentId:
  *         type: integer
  *       criteria:
  *         $ref: '#/definitions/Criteria'
  */


/**
 * @swagger
 * definition:
 *   Controlcharts:
 *     properties:
 *       id:
 *         type: integer
 *       module:
 *         $ref: '#/definitions/Module'
 *       title:
 *         type: string
 *       paramFormName:
 *         type: string
 *       paramXSeries:
 *         $ref: '#/definitions/ParamXSeries'
 *       paramYSeries:
 *         $ref: '#/definitions/ParamYSeries'
 *       paramDateForm:
 *         type: dateTime
 *       paramDateTo:
 *         type: dateTime
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       createdBy:
 *         $ref: '#/definitions/CreatedBy'
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       version:
 *           type: integer
 *       isMajorVersion:
 *           type: boolean
 */


/**
 * @swagger
 * /controlchart:
 *   get:
 *     tags: [Reports]
 *     description: Returns all controlcharts
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of controlcharts
 *         schema:
 *           $ref: '#/definitions/Controlcharts'

 */


module.exports = route.router;
