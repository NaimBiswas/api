'use strict'
/**
* @name resources-schema
* @author shoaib ganja <shoaib.g@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var rString = types.rString;
var bool = types.bool;
var string = types.string;
var object = types.object.bind(types);
var rId = types.rId;
var any = types.any;

var schema = {
  module: object({
    _id: rId.label('Module id'),
    title: rString.label('Module title'),
  }).label('Module'),
  title: string.label('Title'),
  appList: any,
  paramXAxis: any,
  paramYAxis: any,
  startDate: any ,
  endDate: any,
  status: any,
  selectedEntity: any,
  selectedEntityValue: any,
  siteLevel: any,
  allApps: any,
  numberCommon: any,
  dateCommon: any,
  entityCommon: any,
  appWiseCommonQuestion: any,
  isActive: bool.label('Active?'), 
  title: string,

};

module.exports = schema;
