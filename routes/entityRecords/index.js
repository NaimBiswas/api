// module.exports = require('./entityRecords');

var app = require('express')();
var dbConfig = require('../../lib/db/db');
var _ = require('lodash');
var debug = require('../../lib/logger').debug('routes:entityRecords');
var db = require('../../lib/resources').db;

db.entities.find({},{title:1}).toArray(function(err, result) {
  var entities = [];

  _.forEach(result, function(schema, key) {
    entities.push({
      collection: schema._id.toString('hex'),
      audit: true,
      index: {
        title: ''
      }
    });
  });

  debug('Registering dynamic records routes :: Length :: ' + entities.length);

  dbConfig.configure.apply(this, entities);

  _.forEach(result, function(schema, key) {
    debug("Registering dynamic entries routes " + ('/' + schema._id.toString('hex')).underline.bold);
    app.use('/' + schema._id.toString('hex'), require('./entityRecords')(schema._id.toString('hex')));
  });

});

module.exports = app;
