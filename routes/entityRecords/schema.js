'use strict'
/**
 * @name questions-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var anyArray = types.anyArray;
var rBool = types.rBool;
var bool = types.bool;
var number = types.number;
var string = types.string;
var required = types.required;
var stringArray = types.stringArray;
var any = types.any;
var rBigString = types.bigString;
var bigString = types.bigString;


var schema = {
    entityRecord : any,
    isDeleted: bool.label('Is Deleted?'),
	isFromApp : bool.label("Is From App"),
    csv : any.label('csv'),
    isAuditing: bool.label("Is Updating ?"),
    timeZone: string.label('Time Zone'),
    salesforceId: any,
    createdDateInfo: any

};

module.exports = schema;
