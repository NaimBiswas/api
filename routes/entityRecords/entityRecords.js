'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var db = require('../../lib/resources').db;
var logger = require('../../logger');
var currentFileName = __filename;
var config = require('../../config/config.js');
var exportFileEntity = require('../exportfile/exportFileEntity.js');

var request = require("request");
var http = require('https');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
function configure(name) {
  var route = require('../../lib/routeBuilder').build({
    entity: {
      name: name,
      collection: name,
      schema: schema,
      softSchema: softSchema
    },
    types:{

      /**
       * @swagger
       * definition:
       *   entityrecord:
       *     properties:
       *       entityRecord:
       *         $ref: '#/definitions/E_record'
       *       isAuditing:
       *           type: boolean
       */
      /**
       * @swagger
       * definition:
       *   E_record:
       *     properties:
       *       isActive:
       *           type: boolean
       */
    /**
     * @swagger
     * definition:
     *   EntityRecords:
     *     properties:
     *       id:
     *         type: integer
     *       entityRecord:
     *         $ref: '#/definitions/E_record'
     *       modifiedDate:
     *         type: dateTime
     *       createdDate:
     *         type: dateTime
     *       createdByName:
     *         type: string
     *       modifiedByName:
     *         type: string
     *       modifiedBy:
     *         $ref: '#/definitions/ModifiedBy'
     *       createdBy:
     *         $ref: '#/definitions/CreatedBy'
     *       versions:
     *           type: array
     *           items:
     *              type: object
     *              allOf:
     *              - $ref: '#/definitions/Version'
     *       version:
     *           type: integer
     *       isMajorVersion:
     *           type: boolean
     */

    /**
     * @swagger
     * /entityRecords/{id}:
     *   get:
     *     tags: [Entity Records]
     *     description: Returns all entityRecords
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: entityrecord id
     *         in: path
     *         required: true
     *         type: string
     *       - name: allowedPageSizes
     *         description: Page Number
     *         in: query
     *         required: false
     *         type: string
     *       - name: page
     *         description: Page Number
     *         in: query
     *         required: false
     *         type: string
     *       - name: pagesize
     *         description: Page Size
     *         in: query
     *         required: false
     *         type: string
     *       - name: q[csv]
     *         description: CSV Id
     *         in: query
     *         required: false
     *         type: string
     *       - name: sort
     *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
     *         in: query
     *         required: false
     *         type: string
     *       - name: totalItems
     *         description: totalItems
     *         in: query
     *         required: false
     *         type: string
     *     responses:
     *       200:
     *         description: An array of entities
     *         schema:
     *           $ref: '#/definitions/EntityRecords'
     */
/**
 * @swagger
 * /entityRecords/{id}:
 *   post:
 *     tags: [Entity Records]
 *     description: Creates a new entity record
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: entityrecord id
 *         in: path
 *         required: true
 *         type: string
 *       - name: entityrecord
 *         description: entityrecord object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/EntityRecords'
 *     responses:
 *       201:
 *         description: Successfully created
 */
 /**
  * @swagger
  * /entityRecords/{id}/{id}:
  *   patch:
  *     tags: [Entity Records]
  *     description: Updates a entityrecord
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: entityrecords's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: id
  *         description: record's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: entityrecord
  *         description: entityrecord object
  *         in: body
  *         required: true
  *         schema:
  *           $ref: '#/definitions/entityrecord'
  *     responses:
  *       200:
  *         description: Successfully updated
  */
  GET:{
    ALL: {
      after : afterGet,
      // after  : afterPost
    }
  },
      POST:{
        ONE: {
          before : beforePost,
          // after  : afterPost
        }
      },
      PATCH:{
        ONE: {
          before : beforePatch,
          // after  : afterPatch
        }
      }
    }
  });

  function afterGet(req, res, next) {
    if (req.query.entityId && req.data && req.data.length) {
      db.entities.find({ _id: db.ObjectID(req.query.entityId) }).toArray(function (err, entity) {
        logger.log('DB call: enity  (used in API : /exportfile/entity) : - Success', 'info', currentFileName);
        if (err) throw err;
        if (entity && entity[0]) {      
          
          let entityData = [{
            entityRecords: req.data,
            entity: entity[0],
            appliedFilter: req.query.appliedFilter ? JSON.parse(req.query.appliedFilter) : []
          }];
          
          let parsedData = exportFileEntity.parseData(entityData);
          
          if (parsedData) {

            // exportFileEntity.generateDocuments(parsedData, req.query.exportDataType).then(function (response) {
            //   logger.log('Callback: inside callback of generateDocuments (used in API : /exportfile/entity) : - Success', 'info', currentFileName);
            //   return res.json({ filePath: response.filePath, fileName: response.fileName });
            // });


            req.body.parsedData = parsedData;
            req.body.exportDataType = req.query.exportDataType;
            exportFile(req,res,next);

          }
        }
        else{
          next();
        }
      });
    } else {
      next();
    }
  }


  function exportFile(req,res,next){

    if(config && config.exportFileServer &&
      config.exportFileServer.enabled &&
      config.exportFileServer.url &&
      config.exportFileServer.url.post
    ){
        var bodyString = JSON.stringify(req.body);

          const options = {
            // url:config.exportFileServer.url.post,
            hostname: config.exportFileServer.url.postExpanded.hostname,
            port: config.exportFileServer.url.postExpanded.port,
            path: config.exportFileServer.url.postExpanded.generateDocuments,
            method: 'POST',
            // strictSSL:false,
            "rejectUnauthorized": false,
            headers: {
              // 'x-authorization':req.headers['x-authorization'] || req.query['x-authorization'],
              // authorization:req.headers.authorization,
              // 'client-tz':req.headers['client-tz'],
              'Content-Type': 'application/json',
              'Content-Length': bodyString.length
            },
            json: true,
          };
          
        
        http.request(options, function(hRes){
            
            var rawData = '';
            hRes.on('data', function(chunk) { rawData += chunk; });
            hRes.on('end', function(){
              res.json(JSON.parse(rawData));
            });


          },function(err){
          }).write(bodyString);

      }
        else{

          exportFileEntity.generateDocuments(req.body.parsedData, req.query.exportDataType).then(function (response) {
            logger.log('Callback: inside callback of generateDocuments (used in API : /exportfile/entity) : - Success', 'info', currentFileName);
            return res.json({ filePath: response.filePath, fileName: response.fileName });
        });

        // handlePost(req,res,next);
      }
  }
  
  

  function beforePost(req, res, next) {
    next();
  }

  //by surjeet.b@productivet.com
  function beforePatch(req, res, next) {
    logger.log("function: beforePatch - start", 'info', currentFileName);
    if (req.params && req.params._id) {
      var entityTableId = req.params._id.toString().substring(req.params._id.toString().indexOf(':') + 1, req.params._id.toString().indexOf('valId'));
      var entityValueId = req.params._id.toString().substring(req.params._id.toString().lastIndexOf(':') + 1, req.params._id.toString().length);
      if (req.params._id.toString().indexOf('entityId') !== -1 && req.params._id.toString().indexOf('valId') !== -1) {
        let objectToUpdate = {
          isDeleted: true,
          "entityRecord.isActive": false,
          modifiedDate: new Date()
        }

        if (req.user) {
          objectToUpdate.modifiedByName = req.user.firstname + ' ' + req.user.lastname + ' (' + req.user.username + ')';
          objectToUpdate.modifiedBy = req.user._id;
        }
        db[entityTableId.toString()]
        .findOneAndUpdateAsync({
          _id: db.ObjectID(entityValueId)
        }, {
          $set: objectToUpdate
        })
        .then(function (response) {
          logger.log("collection: " + entityTableId.toString() + " findOneAndUpdateAsync" , 'info', currentFileName);
          return res.json({'entityTableId': entityTableId, 'entityValueId': entityValueId});
        });
      }else {
        next();
      }
    }else {
      next();
    }
  }

  return route.router;
}
module.exports = configure;
