'use strict'
/**
 * @name documentsettings schema
 * @author Lalit Godiya <lalit.g@productivet.com>
 *
 * @version 1.0
 */

var types = require('../../lib/validator/types');

var rBigString = types.rBigString;
var rString = types.rString;
var bigString = types.bigString;
var string = types.string;

var schema = {
  perfeqtauserId : rString.label('perfeqtauserId'),
  userName: rBigString.label('userName'),
  userPassword: rBigString.label('userPassword'),
  vaultId: bigString.label('vaultId'),
  timeZone: string.label('Time Zone')

};

module.exports = schema;
