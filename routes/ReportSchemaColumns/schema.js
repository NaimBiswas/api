'use strict'
/**
 * @name reportsettingsconfigSettings-schema
 * @author
 *
 * @version 1.0
 */

var types = require('../../lib/validator/types');


var rBigString = types.rBigString;
var bigString = types.bigString;
var rNumber = types.rNumber;
var number = types.number;
var any = types.any;
var string = types.string;

var schema = {

  title : rBigString.label('App title'),
  appId : bigString.label('App Id'),
  columnInfo: any,
  controlchartColumn : any
};

module.exports = schema;
