'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var settings = require('../../lib/settings');
var config = require('../../lib/config');
var db = require('../../lib/resources').db;
var _ = require('lodash');

var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'ReportSchemaColumns',
    collection: 'ReportSchemaColumns',
    schema: schema,
    softSchema: softSchema
  },
   types: {
    GET: {
      ALL: {
        after: afterPost
      }
    },
  }
});

function afterPost(req, res, next) {
  if (req.data && req.data[0] && req.data[0].appId) {
    db.customReportColumnDetail.findOne({ 'appId': req.data[0].appId }, function (err, data) {
      if (err) {
        res.json(err);
      } else {
        if (data) {
          _.forEach(req.data[0].columnInfo, function (column) {
            if (column.questionid) {
              _.forEach(data.columns, function (app) {
                if (column.questionid == app.questionid) {
                  column.displayName = app.newname || app.originalname;
                }
              })
            }

          })
          res.json(req.data);
        } else {
          res.json(req.data);
        }
      }
    })
  } else {
    res.json(req.data);
  }
}

module.exports = route.router;

/**
 * @swagger
 * definition:
 *    ExtraInformation:
 *      appId:
 *        type: string
 *      maintype:
 *        type: string
 *      subtype:
 *        type: string
 *      label:
 *        type: string
 */

/**
 * @swagger
 * definition:
 *    ColumnInfo:
 *      title:
 *        type: string
 *      displayName:
 *        type: string
 *      questionid:
 *        type: string
 *      dataType:
 *        type: string
 *      extraInformation:
 *        type: array
 *        items:
 *          type: object
 *          allOf:
 *          - $ref: '#/definitions/ExtraInformation'
 */

/**
 * @swagger
 * definition:
 *    SchemaColumns:
 *      properties:
 *        id:
 *          type: string
 *        title:
 *          type: string
 *        appId:
 *          type: string
 *        columnInfo:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *            - $ref: '#/definitions/ColumnInfo'
 */

/**
 * @swagger
 * /reportSchemaColumns:
 *  get:
 *    tags: [Reports]
 *    description: Add/edit custom report
 *    produces:
 *      - application/json
 *    responses:
 *       200:
 *         description: Add/edit custom reports
 *         schema:
 *            $ref: '#/definitions/SchemaColumns'
 */