/*
    author - akashdeep.s - akashdeep.s@productive
    here we are bypasig this url to check for the token app and user
*/
(function () {
    'use strict';
    var express = require('express');
    var db = require('../../lib/db');
    var crypto = require('crypto');
    var config = require('../../config/config');
    var appController = require('./appController.js');
    var entityControlller = require('./entityController.js');
    var scheduleControlller = require('./scheduleControlller.js');
    var queryBuilder = require('./queryBuilder.js');

    module.exports = function (app) {
        var apiRoutes = express.Router();

        //restricting it to post so, that this can not be directly hit by url.

        function isValidObjectID(str) {
            // A valid Object Id must be 24 hex characters
            return (/^[0-9a-fA-F]{24}$/).test(str);
        }

        function authMid(req, res, next) {
            var token = req.query.token || req.headers["token"] || '';
            if (token) {
                var accesstoken = crypto.createHmac('sha256', config.apiProject.secretKey).update(token).digest('hex');
                //check for valid token, here are we going to fetch data from apiAccessToken;
                db.apiAccessConfig.findOne({ hash: accesstoken }, function (err, data) {
                    if (err) {
                        res.status(422).json({ 'message': err.message });
                    } else if (data) {
                        if (data.isActive) {
                            if (data.expirydate) {
                                if (new Date(data.expirydate).setHours(0, 0, 0, 0) > new Date().setHours(0, 0, 0, 0)) {
                                    req.userdata = data;
                                    next();
                                } else {
                                    next({ message: "Your token is expired." });
                                }
                            } else {
                                req.userdata = data;
                                next();
                            }
                        } else {
                            next({ message: "Token is not active." });
                        }

                    } else {
                        next({ message: "Please provide a valid Token" });
                    }
                })

            }
            else {
                next({ message: "Please provide a Token" });
            }
        }

        function checkTitle(req, res, next) {
            var id = req.params.appId || req.params.entityId;
            if (isValidObjectID(id)) {
                req.searchId = id;
                next();
            } else {
                if (req.params.appId) {
                    db.schema.findOne({ title: req.params.appId }, { _id: 1 }, function (err, data) {
                        if (err) {
                            res.status(422).json({ 'message': err.message });
                        } else {
                            if (data) {
                                req.searchId = data._id;
                                next();
                            } else {
                                res.status(422).json({ 'message': 'No data available' });
                            }

                        }
                    })
                } else if (req.params.entityId) {
                    db.entities.findOne({ title: req.params.entityId }, { _id: 1 }, function (err, data) {
                        if (err) {

                            res.status(422).json({ 'message': err.message });
                        } else {
                            if (data) {
                                req.searchId = data._id;
                                next();
                            } else {
                                res.status(422).json({ 'message': 'No data available' });
                            }

                        }
                    })
                }
            }
        }
        /* function getCallerIP(request) {
            var ip = request.headers['x-forwarded-for'] ||
                request.connection.remoteAddress ||
                request.socket.remoteAddress ||
                request.connection.socket.remoteAddress;
            ip = ip.split(',')[0];
            ip = ip.split(':').slice(-1); //in case the ip returned in a format: "::ffff:146.xxx.xxx.xxx"
            return ip;
        } */
        function checkIP(req, res, next) {
            // var ip  = req.userdata.ipaddress.split(',');
            // console.log(ip);
            // var searchip = ''+ip.indexOf(req.ip);
            // console.log(req.ip);
            if (req.userdata && req.userdata.ipaddress) {
                if (req.userdata.ipaddress.indexOf(req.ip) > -1 || req.ip == '127.0.0.1') {
                    next();
                } else {
                    res.status(422).json({ 'message': 'IP address does not match' });
                }
            }
            else {
                next();
            }
        }

        apiRoutes.get('/app', checkIP, queryBuilder.query, appController.getAllApp);
        apiRoutes.get('/app/:appId', checkIP, queryBuilder.query, checkTitle, appController.getOneApp);
        apiRoutes.get('/entity', checkIP, queryBuilder.query, entityControlller.getAllEntity);
        apiRoutes.get('/entity/:entityId', checkIP, queryBuilder.query, checkTitle, entityControlller.getOneEntity);
        apiRoutes.get('/schedule', checkIP, queryBuilder.query, scheduleControlller.getAllSchedule);

        apiRoutes.get('/schedule/:schemaId', checkIP, scheduleControlller.getSchemaWiseSchedule);

        //apiRoutes.get('/schedule', queryBuilder.query, scheduleControlller.getAllSchedule);
        //apiRoutes.get('/schedule/:scheduleId', queryBuilder.query,checkTitle, scheduleControlller.checkAppId, scheduleControlller.getOneSchedule);
        app.use('/oapi/' + config.apiProject.version, authMid, apiRoutes);
        //app.get('/testfun',scheduleControlller.testfun);

        apiRoutes.get('/entity/:entityId/getFields', checkIP, queryBuilder.query, checkTitle, entityControlller.getOneEntityField);
        apiRoutes.get('/app/:appId/getFields', checkIP, queryBuilder.query, checkTitle, appController.getOneAppField);
    }

})();

