
var config = require('../../config/config');
var db = require('../../lib/db');
module.exports.query = function (req, res, next) {
    var pageSize = parseInt(req.query.pagesize || config.apiProject.options.pageSize);
    var page = parseInt(req.query.page || config.apiProject.options.page);
    req.options = {
        page: parseInt(req.query.page) || 1,
        pageSize: parseInt(req.query.pageSize) || 10000,
        skip: pageSize * (page - 1),
        limit: pageSize,
        where: {},
        select: {
            title: 1
        }
    }

    if (req.query.apptitle) {
        db.schema.findOne({ title: req.query.apptitle }, { _id: 1 }, function (err, data) {
            if (err) {
                //res.status(422).json({ 'message': err.message });
            } else {
                var appId = req.userdata.apps.indexOf(data._id);
                if (appId && appId != -1) {
                    req.options.where['schema._id'] = db.ObjectID(data._id);
                    console.log("in");
                } else {
                    res.status(304).json({ 'message': "you don't have this app permission" });
                }
            }
        })
    }

    if (req.query.datebetween) {
        var date = req.query.datebetween.split(',');
        var date1 = new Date(date[1]);
        var date2 = new Date(date[0]);
        req.options.where['modifiedDate'] = {
            $gte: date2,
            $lte: date1
        };
    }

    next();
}