
var db = require('../../lib/db');
var _ = require('lodash');
var Code = require('mongodb').Code;

module.exports.getAllSchedule1 = function (req, res, next) {
  var arr = [];
  _.forEach(req.userdata.apps, function (data) {
    arr.push(data);
  });
  req.options.where['schema._id'] = { $in: arr };
  req.options.select = {
    title: 1,
    schema: 1
  }
  db.schedules.find(req.options.where, req.options.select).skip(req.options.skip).limit(req.options.limit).toArray(function (err, data) {
    if (err) {
      res.status(422).json({ message: err.message });
    } else {
      res.status(200).json(data);
    }
  });
}


module.exports.getAllScheduleCalender = function (req, res, next) {
  var arr = [];
  _.forEach(req.userdata.apps, function (data) {
    arr.push(db.ObjectID(data));
  })
  var date = new Date();
  if (!req.query.datebetween) {
    req.options.where['date'] = { "$gte": date };
  } else {
    date = req.query.datebetween.split(',');
    delete req.options.where['modifiedDate'];
    req.options.where['date'] = {
      $gte: new Date(date[0]),
      $lte: new Date(date[1])
    };
  }
  if (!req.query.apptitle) {
    req.options.where['schema._id'] = { $in: arr };
  }
  console.log(req.options.where);
  db.calendar.find(req.options.where).skip(req.options.skip).limit(req.options.limit).toArray(function (err, data) {
    if (err) {
      res.status(422).json({ message: err.message });
    } else {
      res.status(200).json(data);
    }
  })
}

module.exports.checkAppId = function (req, res, next) {
  var id = req.searchId;
  var appId = req.userdata.apps.indexOf(id);
  if (appId && appId != -1) {
    next();
  } else {
    res.status(304).json({ 'message': "you don't have this app permission" });
  }
}


module.exports.getOneSchedule = function (req, res, next) {
  var date = new Date();
  if (!req.query.datebetween) {
    req.options.where['schema._id'] = db.ObjectID(req.searchId);
    req.options.where['date'] = { "$gte": date };
  } else {
    date = req.query.datebetween.split(',');
    delete req.options.where['modifiedDate'];
    req.options.where['schema._id'] = db.ObjectID(req.searchId);
    req.options.where['date'] = {
      $gte: new Date(date[1]),
      $lte: new Date(date[0])
    };
  }
  console.log(req.options.where);
  db.calendar.find(req.options.where).skip(req.options.skip).limit(req.options.limit).toArray(function (err, data) {
    if (err) {
      res.status(422).json({ message: err.message });
    } else {
      res.status(200).json(data);
    }
  })
}

module.exports.getAllSchedule = function (req, res, next) {
    var token = req.query.token || req.headers["token"] || '';
    console.log(req.userdata.apps);
    db.eval('getAllSchedule(' + req.query.page + ',' + req.query.pagesize + ',"' + token+'")', function (er, doc) {
    if (er) {
      next(er);
      return;
    }
    res.json(doc);
  });
}

module.exports.getSchemaWiseSchedule = function (req, res, next) {

  //the function is comming from MongoStoredProcedures
  db.eval('getScheduleSchemaWise(' + req.query.page + ',' + req.query.pagesize + ',"' + req.params.schemaId + '")', function (er, doc) {
    if (er) {
      next(er);
      return;
    }
    res.json(doc._batch);
  });
}

/**
 * @swagger
 * definition:
 *   Accessibleschedules:
 *     properties:
 *       id:
 *         type: integer
 *       AppName:
 *         type: string
 *       Title:
 *         type: string
 *       Type:
 *         type: string
 *       Time:
 *         type: dateTime
 *       Date:
 *         type: dateTime
 *       Nototified:
 *         type: boolean
 *       status:
 *         type: string
 */

/**
 * @swagger
 * /oapi/v1/schedule:
 *   get:
 *     tags: [Schedules from Api]
 *     description: Returns all accessible schedules
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pageSize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: datebetween
 *         description: date between
 *         in: query
 *         required: false
 *         type: string
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: An array of Accessible Schedules
 *         schema:
 *           $ref: '#/definitions/Accessibleschedules'
 */
