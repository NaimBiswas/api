
var db = require('../../lib/db');
var _ = require('lodash');
module.exports.getAllEntity = function (req, res, next) {
  var arr = [];
  _.forEach(req.userdata.entities, function (data) {
    arr.push(db.ObjectID(data));
  })
  req.options.where['_id'] = { $in: arr };
  db.entities.find(req.options.where, req.options.select).skip(req.options.skip).limit(req.options.limit).toArray(function (err, data) {
    if (err) {
      res.status(422).json({ 'message': err.message });
    } else {
      res.status(200).json(data);
    }
  })
}

module.exports.getOneEntity1 = function (req, res, next) {
  var id = req.searchId;
  db.entitiesAuditLogs.find({ identity: db.ObjectID(id) }, { questions: 1, title: 1 }).toArray(function (err, data) {
    if (err) {
      res.status(422).json({ 'message': err.message });
    } else {
      var mainarray = {
        title: "",
        entries: []
      }
      var allquestion = [];
      function checkque(id) {
        var entryobj = _.find(allquestion, { comboId: id });
        return entryobj;
      }
      _.forEach(data, function (que, key) {
        mainarray['title'] = que.title;
        _.forEach(que.questions, function (q) {
          if (!checkque(q.comboId)) {
            allquestion.push(q);
          }
        })
      })
      db.collection(db.ObjectID(id).toString()).find({}).toArray(function (err, entries) {
        if (err) {
          res.status(422).json({ 'message': err.message });
        } else {
          _.forEach(entries, function (entry, key) {
            var entryarr = [];
            _.forEach(entry.entityRecord, function (e, key1) {
              entry = {
                key: "",
                value: ""
              }
              var data = checkque(key1);
              if (data) {
                entry['key'] = data.title;
                entry['value'] = e;
                entryarr.push(entry);
              }
            })
            setTimeout(function () { mainarray['entries'].push(entryarr); }, 0);
            //mainarray['entries'].push(entryarr);
          })
        }
      })
      setTimeout(function () { res.status(200).json(mainarray); }, 10);
      //res.status(200).json(mainarray);
    }
  })
}

module.exports.getOneEntity = function (req, res, next) {
  var id = req.searchId;
  db.collection(db.ObjectID(id).toString()).find(req.options.where).skip(req.options.skip).limit(req.options.limit).toArray(function (err, entries) {
    if (err) {
      res.status(422).json({ 'message': err.message });
    } else {
      if (entries.length > 0) {
        var mainarray = {
          title: "",
          entries: []
        }
        db.entities.findOne({ _id: db.ObjectID(id) }, { questions: 1, title: 1 }, function (err, data) {
          if (err) {
            res.status(422).json({ 'message': err.message });
          } else {
            mainarray['title'] = data.title;
            function checkque(id) {
              if (id == 'isActive') {
                return { title: 'Status' };
              }
              var entryobj = _.find(data.questions, { comboId: id });
              return entryobj;
            }

            _.forEach(entries, function (entry, key) {
              var entryarr = [];
              _.forEach(entry.entityRecord, function (e, key1) {
                entry = {
                  key: "",
                  value: ""
                }
                var data = checkque(key1);
                if (data) {
                  entry['key'] = data.title;
                  entry['value'] = e;
                  entryarr.push(entry);
                }
              })
              setTimeout(function () { mainarray['entries'].push(entryarr); }, 0);
              //mainarray['entries'].push(entryarr);
            })
            //res.status(200).json(data);
          }
          setTimeout(function () { res.status(200).json(mainarray); }, 10);
        })

      } else {
        res.status(200).json({ message: 'No entries available' });
      }
    }
  })
}

module.exports.getOneEntityField = function (req, res, next) {
  console.log("In getField");
  var id = req.searchId;
  db.collection('entities').find({ _id: db.ObjectID(id) }).toArray(function (err, entries) {
    if (err) {
      res.status(422).json({ 'message': err.message });
    } 
    else {
      if (entries.length) {
        var mainarray = {
          entityData: []
        }
        _.forEach(entries[0], function (value, key) {
          if (key == 'questions') {
            _.forEach(value, function (questionValue) {
              mainarray.entityData.push({
                'title': questionValue.title,
                'label': questionValue.title 
              });
            })
          }
          else {
            mainarray.entityData.push({ 'title': key, 'label': key });
          }
        })
        setTimeout(function () { res.status(200).json(mainarray); }, 10);
      } 
      else {
        res.status(200).json({ message: 'No entries available' });
      }
    }
  })
}

/**
 * @swagger
 * definition:
 *   Apientity_entries:
 *     properties:
 *       key:
 *         type: string
 *       value:
 *         type: string
 */


/**
 * @swagger
 * definition:
 *   Entityentries:
 *     properties:
 *       title:
 *         type: string
 *       entries:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Apientity_entries'
 */

/**
 * @swagger
 * /oapi/v1/entity:
 *   get:
 *     tags: [Entities from Api]
 *     description: Returns all accessible entities
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pageSize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: datebetween
 *         description: date between
 *         in: query
 *         required: false
 *         type: string
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: An array of Accessible Entities
 *         schema:
 *           $ref: '#/definitions/Accessibleapps'
 */

 /**
  * @swagger
  * /oapi/v1/entity/{id}:
  *   get:
  *     tags: [Entities from Api]
  *     description: Returns a object entity entries
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: Entity's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: pagesize
  *         description: Page Size
  *         in: query
  *         required: false
  *         type: string
  *       - name: token
  *         description: token
  *         in: header
  *         required: true
  *         type: string
  *     responses:
  *       200:
  *         description: An object of entity entries
  *         schema:
  *           $ref: '#/definitions/Entityentries'
  */

