var db = require('../../lib/db');
var _ = require('lodash');
var logger = require('../../logger');
var currentFileName = __filename;

module.exports.getAllApp = function (req, res, next) {
    logger.log("function: getAllApp - start", 'info', currentFileName);
    var arr = [];
    _.forEach(req.userdata.apps, function (data) {
        arr.push(db.ObjectID(data));
    });
    
    req.options.where['_id'] = { $in: arr };
    db.schema.find(req.options.where, req.options.select).skip(req.options.skip).limit(req.options.limit).toArray(function (err, data) {
        if (err) {
            logger.log("collection: schema : " + err, 'error', currentFileName);
            res.status(422).json({ 'message': err.message });
        } else {
            logger.log("collection: schema", 'info', currentFileName);
            res.status(200).json(data);
        }
    });
}

module.exports.getOneApp1 = function (req, res, next) {
    logger.log("function: getOneApp1 - start", 'info', currentFileName);
    var id = req.searchId;
    var allappdata = [];
    db.collection(db.ObjectID(id).toString()).aggregate({ $group: { _id: "$parentVersion" } }, function (err, data) {
        if (err) {
            logger.log("collection: " + db.ObjectID(id).toString() + "aggregate : " + err, 'error', currentFileName);
            res.status(422).json({ 'message': err.message });
        } else {
            var versi = [];
            _.forEach(data, function (a) {
                versi.push(a._id);
            });

            db.schemaAuditLogs.find({ identity: db.ObjectID(id), version: { $in: versi } }).toArray(function (err, data1) {
                if (err) {
                    logger.log("collection: schemaAuditLogs find : " + err, 'error', currentFileName);
                    res.status(422).json({ 'message': err.message });
                } else {
                    db.collection(db.ObjectID(id).toString()).find({}).toArray(function (err, entries) {
                        logger.log("collection: " + db.ObjectID(id).toString() + " find", 'info', currentFileName);
                        _.forEach(data1, function (appentry, key) {
                            var arr2 = {
                                title: "",
                                version: "",
                                entries: [
                                ]
                            }
                            arr2['title'] = appentry.title;
                            arr2['version'] = appentry.version;
                            _.forEach(entries, function (entry, key6) {
                                if (appentry.version == entry.parentVersion) {
                                    var arr1 = [];
                                    if (appentry.attributes) {
                                        _.forEach(appentry.attributes, function (e) {
                                            arr1.push(e);
                                        })
                                    }
                                    if (appentry.entities) {
                                        _.forEach(appentry.entities, function (e) {
                                            arr1.push(e);
                                        })
                                    }
                                    if (appentry.procedures) {
                                        _.forEach(appentry.procedures, function (e) {
                                            arr1.push(e);
                                        })
                                    }
                                    function procedurecheck(parentId) {
                                        var rvalue = false;
                                        _.forEach(appentry.procedures, function (data, key) {
                                            if (appentry.procedures[key]['s#'] == parentId) {
                                                rvalue = true;
                                            }
                                        });
                                        return rvalue;
                                    }
                                    function entitycheck(parentId) {
                                        var rvalue = false;
                                        _.forEach(appentry.entities, function (data, key) {
                                            if (appentry.entities[key]['s#'] == parentId) {
                                                rvalue = true;
                                            }
                                        })
                                        return rvalue
                                    }
                                    var arr3 = {
                                        attributes: [],
                                        entity: [],
                                        procedure: []
                                    }
                                    _.forEach(arr1, function (attribute, key1) {
                                        if (attribute.questions) {
                                            var mainarr = {
                                                title: "",
                                                values: []
                                            }
                                            var proceflag = false;
                                            var entityflag = false;
                                            _.forEach(attribute.questions, function (ap, key3) {
                                                if (appentry.procedures && procedurecheck(attribute.questions[key3].parentId)) {
                                                    var attr = {
                                                        title: "",
                                                        value: "",
                                                    };
                                                    attr['title'] = attribute.questions[key3]['title'];
                                                    attr['value'] = entry.entries[attribute.questions[key3]['s#'] + '-' + attribute.questions[key3].parentId][attribute.questions[key3].parentId];
                                                    mainarr['values'].push(attr);
                                                    proceflag = true;

                                                } else if (appentry.entities && entitycheck(attribute.questions[key3].parentId)) {
                                                    var attr = {
                                                        title: "",
                                                        value: "",
                                                    };
                                                    mainarr['title'] = attribute.title;
                                                    attr['title'] = attribute.questions[key3]['title'];
                                                    attr['value'] = entry.entries[attribute.questions[key3]['s#'] + '-' + attribute.questions[key3].parentId][attribute.questions[key3].parentId];
                                                    mainarr['values'].push(attr);
                                                    entityflag = true;
                                                }
                                            })
                                            if (proceflag) {
                                                mainarr['title'] = attribute.title;
                                                arr3['procedure'].push(mainarr);
                                            } else if (entityflag) {
                                                mainarr['title'] = attribute.title;
                                                arr3['entity'].push(mainarr);
                                            }
                                        } else {
                                            var attr = {
                                                title: "",
                                                value: "",
                                            };
                                            attr['title'] = attribute.title;
                                            attr['value'] = entry.entries[attribute['s#']].attributes;
                                            arr3['attributes'].push(attr);
                                            /* var attr = {
                                                title: "",
                                                value: "",
                                            }; */
                                        }
                                    })

                                    arr2['entries'].push(arr3);
                                    //allappdata.push(arr2)
                                }
                            })
                            allappdata.push(arr2);
                            //setTimeout(function () { allappdata.push(arr2); }, 0);
                        })
                    })
                }
                res.status(200).json(allappdata);
                //setTimeout(function () { res.status(200).json(allappdata); }, 10);
            })
        }
    })
}

module.exports.getOneApp = function (req, res, next) {
    logger.log("function: getOneApp - start", 'info', currentFileName);
    var id = req.searchId;
    var allappdata = [];

    db.collection(db.ObjectID(id).toString()).find(req.options.where).skip(req.options.skip).limit(req.options.limit).toArray(function (err, entries) {
        if (err) {
            logger.log("collection: " + db.ObjectID(id).toString() + " find : " + err, 'error', currentFileName);
            res.status(422).json({ 'message': err.message });
        } else {
            if (entries.length > 0) {
                var grouparr = _.countBy(entries, 'parentVersion');
                var versi = [];
                _.forEach(grouparr, function (data, key) {
                    versi.push(parseFloat(key));
                });
                db.schemaAuditLogs.find({ identity: db.ObjectID(id), version: { $in: versi } }).toArray(function (err, data1) {
                    if (err) {
                        logger.log("collection: schemaAuditLogs find : " + err, 'error', currentFileName);
                        res.status(422).json({ 'message': err.message });
                    } else {
                        _.forEach(data1, function (appentry, key) {
                            var arr2 = {
                                title: "",
                                version: "",
                                entries: [
                                ]
                            }
                            arr2['title'] = appentry.title;
                            arr2['version'] = appentry.version;
                            _.forEach(entries, function (entry, key6) {
                                if (appentry.version == entry.parentVersion) {
                                    var arr1 = [];
                                    if (appentry.attributes) {
                                        _.forEach(appentry.attributes, function (e) {
                                            arr1.push(e);
                                        })
                                    }
                                    if (appentry.entities) {
                                        _.forEach(appentry.entities, function (e) {
                                            arr1.push(e);
                                        })
                                    }
                                    if (appentry.procedures) {
                                        _.forEach(appentry.procedures, function (e) {
                                            arr1.push(e);
                                        })
                                    }
                                    function procedurecheck(parentId) {
                                        var rvalue = false;
                                        _.forEach(appentry.procedures, function (data, key) {
                                            if (appentry.procedures[key]['s#'] == parentId) {
                                                rvalue = true;
                                            }
                                        });
                                        return rvalue;
                                    }
                                    function entitycheck(parentId) {
                                        var rvalue = false;
                                        _.forEach(appentry.entities, function (data, key) {
                                            if (appentry.entities[key]['s#'] == parentId) {
                                                rvalue = true;
                                            }
                                        })
                                        return rvalue
                                    }
                                    var arr3 = {
                                        attributes: [],
                                        entity: [],
                                        procedure: []
                                    }
                                    _.forEach(arr1, function (attribute, key1) {
                                        if (attribute.questions) {
                                            var mainarr = {
                                                title: "",
                                                values: []
                                            }
                                            var proceflag = false;
                                            var entityflag = false;
                                            _.forEach(attribute.questions, function (ap, key3) {
                                                if (appentry.procedures && procedurecheck(attribute.questions[key3].parentId)) {
                                                    var attr = {
                                                        title: "",
                                                        value: "",
                                                    };
                                                    console.log(attribute.questions)
                                                    attr['title'] = attribute.questions[key3]['title'];
                                                    attr['value'] = entry.entries[attribute.questions[key3]['s#'] + '-' + attribute.questions[key3].parentId][attribute.questions[key3].parentId];
                                                    mainarr['values'].push(attr);
                                                    proceflag = true;

                                                } else if (appentry.entities && entitycheck(attribute.questions[key3].parentId)) {
                                                    var attr = {
                                                        title: "",
                                                        value: "",
                                                    };
                                                    mainarr['title'] = attribute.title;
                                                    attr['title'] = attribute.questions[key3]['title'];
                                                    attr['value'] = entry.entries[attribute.questions[key3]['s#'] + '-' + attribute.questions[key3].parentId][attribute.questions[key3].parentId];
                                                    mainarr['values'].push(attr);
                                                    entityflag = true;
                                                }
                                            })
                                            if (proceflag) {
                                                mainarr['title'] = attribute.title;
                                                arr3['procedure'].push(mainarr);
                                            } else if (entityflag) {
                                                mainarr['title'] = attribute.title;
                                                arr3['entity'].push(mainarr);
                                            }
                                        } else {
                                            var attr = {
                                                title: "",
                                                value: "",
                                            };
                                            attr['title'] = attribute.title;
                                            attr['value'] = entry.entries[attribute['s#']].attributes;
                                            arr3['attributes'].push(attr);
                                            /* var attr = {
                                                title: "",
                                                value: "",
                                            }; */
                                        }
                                    })

                                    arr2['entries'].push(arr3);
                                    //allappdata.push(arr2)
                                }
                            });
                            //setTimeout(function () { allappdata.push(arr2); }, 0);
                            allappdata.push(arr2);
                        });
                        res.status(200).json(allappdata);
                    }
                    //setTimeout(function () { res.status(200).json(allappdata); }, 10);
                });
            } else {
                res.status(200).json({ message: 'No entries available' })
            }
        }

    })

}

module.exports.getOneAppField = function (req, res, next) {
    console.log("In getField");
    var id = req.searchId;
    db.collection('schema').find({ _id: db.ObjectID(id) }).toArray(function (err, entries) {
      if (err) {
        res.status(422).json({ 'message': err.message });
      } 
      else {
        if (entries.length) {
          var mainarray = {
            appData: []
          }
          _.forEach(entries[0], function (value, key) {
            if (key == 'entities') {
              _.forEach(value, function (entityValue, key) {
                mainarray.appData.push({ 'title': entityValue.title, 'label': entityValue.title });
              })
            }
            else if (key == 'attributes') {
                _.forEach(value, function (attributesValue, key) {
                    mainarray.appData.push({ 'title': attributesValue.title, 'label': attributesValue.title });                  
                })
              }
            // else if (key == 'allicableSites') {
            //     _.forEach(value, function (sitesValue, key) {
            //         _.forEach(value, function (sitesValue, key) {
            //             mainarray.appData.push({ 'title': sitesValue.title, 'label': sitesValue.title });                  
            //         })
            //     })
            //   }
            else {
              mainarray.appData.push({ 'title': key, 'label': key });
            }
          })
          setTimeout(function () { res.status(200).json(mainarray); }, 10);
        } 
        else {
          res.status(200).json({ message: 'No entries available' });
        }
      }
    })
  }



/**
 * @swagger
 * definition:
 *   Accessibleapps:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 */

/**
 * @swagger
 * /oapi/v1/app:
 *   get:
 *     tags: [Apps from Api]
 *     description: Returns all accessible apps
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pageSize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: datebetween
 *         description: date between
 *         in: query
 *         required: false
 *         type: string
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: An array of Accessible Apps
 *         schema:
 *           $ref: '#/definitions/Accessibleapps'
 */

 /**
  * @swagger
  * definition:
  *   Apiapp_attributes:
  *     properties:
  *       title:
  *         type: string
  *       value:
  *         type: string
  */

  /**
   * @swagger
   * definition:
   *   Apiapp_entity:
   *     properties:
   *       title:
   *         type: string
   *       values:
   *           type: array
   *           items:
   *              type: object
   *              allOf:
   *              - $ref: '#/definitions/Apiapp_attributes'
   */

 /**
  * @swagger
  * definition:
  *   Apiapp_Entries:
  *     properties:
  *       attributes:
  *           type: array
  *           items:
  *              type: object
  *              allOf:
  *              - $ref: '#/definitions/Apiapp_attributes'
  *       entity:
  *           type: array
  *           items:
  *              type: object
  *              allOf:
  *              - $ref: '#/definitions/Apiapp_entity'
  *       procedure:
  *           type: array
  *           items:
  *              type: object
  *              allOf:
  *              - $ref: '#/definitions/Apiapp_entity'
  */

 /**
  * @swagger
  * definition:
  *   Appentries:
  *     properties:
  *       title:
  *         type: string
  *       version:
  *         type: integer
  *       entries:
  *           type: array
  *           items:
  *              type: object
  *              allOf:
  *              - $ref: '#/definitions/Apiapp_Entries'
  */

 /**
  * @swagger
  * /oapi/v1/app/{id}:
  *   get:
  *     tags: [Apps from Api]
  *     description: Returns a object app entries
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: App's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: pagesize
  *         description: Page Size
  *         in: query
  *         required: false
  *         type: string
  *       - name: token
  *         description: token
  *         in: header
  *         required: true
  *         type: string
  *     responses:
  *       200:
  *         description: An object of app entries
  *         schema:
  *           $ref: '#/definitions/Appentries'
  */


