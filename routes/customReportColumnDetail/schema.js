'use strict'
/**
* @name resources-schema
* @author shoaib ganja <shoaib.g@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var rString = types.rString;
var rBool = types.rBool;
var string = types.string;
var object = types.object.bind(types);
var rId = types.rId;
var any = types.any;

var schema = {
  appId:any,
  appName:any,
  columns:any,
  timeZone: string.label('Time Zone')
};

module.exports = schema;
