(function() {

    'use strict'
    var request = require('request');
    var fs = require('fs');
    var access_Token = "Bearer TGgDCWdC5XAAAAAAAAAAcXPvmotlbpuGPSKqQ16lhiICdU9mj9kRmuBU4UTyuQEF"
    var service = function() {};
    var Dropbox = require('dropbox');
    // var access_Token = "TGgDCWdC5XAAAAAAAAAAcXPvmotlbpuGPSKqQ16lhiICdU9mj9kRmuBU4UTyuQEF"
    var dbx = new Dropbox({ accessToken: access_Token });

    function listFiles(req, response) {
        dbx.filesListFolder({ path: '/myFolder' })
            .then(function(resp) {
                console.log(resp);
                response.send(resp.entries);
            })
            .catch(function(error) {
                console.log(error);
            });
    }
    // uploadFile()

    function uploadFiles(req, res) {
        var readStream = fs.createReadStream(process.env.FILE_STORE + req.params.fileName);
        var str = req.params.fileName;
        var file_name = str.slice(14);
        readStream.on('data', function(data) {
            dbx.filesUpload({ path: '/myFolder/' + file_name, contents: data })
                .then(function(response) {
                    // var results = document.getElementById('results');
                    // results.appendChild(document.createTextNode('File uploaded!'));
                    //   console.log(response);
                })
                .catch(function(error) {
                    //   console.error(error);
                });
        }).on('end', function() {
            console.log('end');
        });

    }
    // downloadfile()
    // authorization=Bearer TGgDCWdC5XAAAAAAAAAAcXPvmotlbpuGPSKqQ16lhiICdU9mj9kRmuBU4UTyuQEF&arg={"path":"/myfolder/textarea.xlsx"}
    function downloadFiles(req, res) {
        var options = {
            method: 'POST',
            url: 'https://api.dropboxapi.com/2/files/get_temporary_link',
            'Content-Type': 'application / json',
            headers: {
                'Authorization': access_Token
            },
            body: {
                "path": "/myFolder/textarea.xlsx"
            },
            json: true

        }
        request(options,
            function(error, response1, body) {
                if (error) {
                    response.status(501).json(error);
                }
                // response.send(body.entries);
                console.log(body);
            });

    }

    service.prototype.listFiles = listFiles;
    service.prototype.uploadFiles = uploadFiles;
    service.prototype.downloadFiles = downloadFiles;
    module.exports = new service();

})();
