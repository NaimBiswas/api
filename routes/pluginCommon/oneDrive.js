(function () {

    'use strict'
    var oneDriveAPI = require('onedrive-api');
    var fs = require('fs');
    var access_Token = "EwA4A8l6BAAU7p9QDpi/D7xJLwsTgCg3TskyTaQAAdQ9f5iPBu37dNZRBKYsL3b+4Z0rpWCjOCF7x2GB/lbjbgXToL0ATzowj1+D/rRTcE/mkh/CuYC2p3AkOgHksXOgrO3Wx1/HCN/axgcsNZbVMu1N+U1vuXQBD9V8AmqAAQedGsg35veyb5n4sX7PYcGVOhAcs6iS+boBFSs49dUgsrpTO0V5K4XYJfyBP3zkZQiQ7C4PyVft1mqCaNgiMZK3F0dDGspLf0I+Kwjl7EnK8OGZxdq88Zpo4vuaNf4zdNekq91MBFAeN2jzH0OKPB9Dzqy6zLsl7AsXHkKwb7+aM6GjRf42ZPjQ7fB7J+dFvt5tqwLQ5fbsX2BksLYDEpADZgAACJiOc9m0P7tSCALvol/uGhP5qDzONs/KC2BVfKwayms/59ZJGi0Asq4rXG6XVJDmTa4ZkbHTyQTFMiEfcovE7m5VP6QympP5H0rSdl41fnNwyPzdEgs4E7eyFHluTzPIoDpwmK/v2AWHrHlPqLDGkLst3k7WIsEOWkLxE0rQ0ufWObDwpeJMJBymYCypIvVe9ECsQ+XCgKzcuAVBMSU8awEkruMkLBVzJwqvWg8SnG8du5STMTNof4todE6sCO5jiJK6Vf1UHiQOObJRjXXRtgmb1VAqfAMraRTo3PJSQdp6lyWn/1VeKXjSq8a/kL5zLiU3ejLFoxva8BEGLLIvYi1QAnkgOGHWERL26GEA8q8oXst5RQdKl0CzXEN/F+mTNQKZZMs1mceiX/PEHVAfGCWx2baFwM/TV19bDU7tWG93liAQODrxjKuVeSM+wieY1IV4xZRMATEMSE1KWYuPoU/obEr0aGqAzTVxcXfAOUghRD18UP5L4Wo/+fOWnqggFJc19AO4SmVtzHWN8EpwTHcqA0P+zesOPOU9cTgGZFWGqf+zuQ/HOb1oxUi/nTlgsltw+3qoQNGK9yEK6Zw3dMCeKQgzX+VvqYriZPv+7z6isVk5P96qCKb6YVMD+iOAlRxeiFnziSqmt78oxRv9inV2L1mo6O3ZJNzsK+0KT6X0f/NdT7PjXiTLJhXmJV3XMWp6SgI="
    var service = function () { };

    function listFiles(req, response) {
        oneDriveAPI.items.listChildren({
            accessToken: access_Token,
            itemId: "root"
        }).then((childrens) => {
            console.log(childrens);
            response.send(childrens)
        })
    }

    function uploadFiles(req, response) {
        var str = req.params.fileName; 
        var file_name = str.slice(14);
        var readable_Stream = fs.createReadStream( process.env.FILE_STORE + req.params.fileName);

        oneDriveAPI.items.uploadSimple({
            accessToken: access_Token,
            filename: file_name,
            readableStream: readable_Stream
        }).then((item) => {
            console.log(item);
            response.send("OK")
        })
    }

    service.prototype.listFiles = listFiles;
    service.prototype.uploadFiles = uploadFiles;
    module.exports = new service();

})();
