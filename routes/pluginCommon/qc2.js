(function () {
    'use strict'
    var _ = require('lodash');
    var q = require('q');
    var moduleNames = [];
    var moduleFields = [];
    var service = function () {};
    var Sequelize = require('sequelize');

    function getSequelize(database) {
        var sequelize = new Sequelize(database[0].config.dbname, database[0].config.username, database[0].config.password, {
            host: database[0].config.host,
            dialect: 'mssql',
            pool: {
                max: 5,
                min: 0,
                acquire: 30000,
                idle: 10000
            }
        });
        return sequelize;
    }

    function getApp(req, response) {
        var sequelize = getSequelize(req.instanceData);
        moduleNames = [];
        sequelize.query('select * from QCTestBaseType').spread(function (data) {
            _.forEach(data, function (obj) {
                moduleNames.push({
                    id: obj.QCTestBaseTypeId,
                    title: obj.Name
                });
            });
            response.send(moduleNames);
        }, function (err) {
            response.send(err);
        });

    }

    function getAppField(req, response) {
        var sequelize = getSequelize(req.instanceData);
        moduleFields = [];
        sequelize.query('exec OpenDataSearch @intSearchType=2,@strDIN=NULL,@dtStartDate=\'2000-01-01 00:00:00\', @dtEndDate=\'2018-01-31 00:00:00\', @Region=0,@ProductionSite=0,@CollectionSite=0,@ProductType=' + '\'' + req.query.appid + '\'' + ',@ProductCode=\'\',@IsPassed=1,@IsFailed=1,@IsPending=1,@IsRoutineTesting=1,@IsMiscellaneousTesting=0,@intUserId=1,@IsVoid=1;').spread(function (data) {
            _.forEach(Object.keys(data[0]), function (item) {
                moduleFields.push({
                    'label': item,
                    'title': item,
                    'stype': ''
                });
            });
            response.send(moduleFields);
        }, function (err) {
            response.send(err);
        });
    }

    function getRecords(datas, data, sapp, mapping, req) {
        var tempArr = [];
        tempArr.push(data);
        var sequelize = getSequelize(tempArr);
        var defered = q.defer();
        var startPosition;
        var endPosition
        if (req.pluginUniqueIdentifier) {
            startPosition = req.pluginUniqueIdentifier;
            endPosition = +(startPosition) + 100;
        } else {
            startPosition = 0;
            endPosition = 100;
        }
        sequelize.query('exec OpenDataSearch1 @intSearchType=2,@strDIN=NULL,@dtStartDate=\'2000-01-01 00:00:00\', @dtEndDate=\'2018-01-31 00:00:00\', @Region=0,@ProductionSite=0,@CollectionSite=0,@ProductType=' + '\'' + req.qcAppId + '\'' + ',@ProductCode=\'\',@IsPassed=1,@IsFailed=1,@IsPending=1,@IsRoutineTesting=1,@IsMiscellaneousTesting=0,@intUserId=1,@IsVoid=1,@startPosition=' + '\'' + startPosition + '\'' + ',@endPosition=' + '\'' + endPosition + '\'' + ';').spread(function (appRecords) {
            defered.resolve(appRecords);
        });
        return defered.promise;
    }

    service.prototype.getApp = getApp;
    service.prototype.getAppField = getAppField;
    service.prototype.getRecords = getRecords;
    module.exports = new service();

})();