(function() {

   'use strict'
   var _ = require('lodash');
   var q = require('q');
   var sf = require('node-salesforce');
   var conn;
   var appNames = [];
   var appFields = [];
   var appRecordIds = [];
   var service = function() {};


   function getApp(req, response) {
       appNames = [];
       getConected(req.instanceData[0]).then(function(data) {
           conn.describeGlobal(function(err, res) {
               if (err) { return console.error(err); }
               _.forEach(res.sobjects, function(data) {
                   appNames.push({ title: data.name });
               });
               response.send(appNames);
           });
       }, function(err) {
           response.status(501).json(err);
       });
   }


   function getAppField(req, res) {
       appFields = [];
       getConected(req.instanceData[0]).then(function(data) {

           conn.sobject(req.query.title).describe$(function(err, meta) {
               if (err) { return console.error(err); }
               _.forEach(meta.fields, function(item) {
                   appFields.push({ 'label': item.label, 'title': item.name, 'stype': item.type });
               });
               appFields = _.orderBy(appFields, function(oj) {
                   return oj.label;
               });
               res.send(appFields);
           });
       }, function(err) {
           response.status(501).json(err);
       });
   }

   function getRecords(datas, data, sapp, mapping, req) {
       var defered = q.defer();
       var appRecords = [];
       getConected(data).then(function(data) {
           conn.sobject(sapp).retrieve(datas, function(err, accounts) {
               if (err) { defered.reject(err) } else {
                   appRecords = accounts;
                   defered.resolve(appRecords)
               }
           });
       }, function(err) {
           return err;
       });
       return defered.promise;
   }

   function getRecordIds(data, sapp) {
       var deffred = q.defer();
       appRecordIds = [];
       getConected(data).then(function(data) {
           conn.query("SELECT Id, Name FROM " + sapp, function(err, result) {
               if (err) {
                   deffred.reject(err);
                   return;
               } else {
                   _.forEach(result.records, function(obj) {
                       appRecordIds.push(obj.Id);
                   });
                   deffred.resolve(appRecordIds);
               }
           })
       }, function(err) {
           deffred.reject(err);
       });
       return deffred.promise
   }


   function getConected(data) {
       var deferred = q.defer();
       conn = new sf.Connection({
           clientId: data.config.consumerkey,
           clientSecret: data.config.consumersecret
       });
       conn.login(data.config.username, data.config.password + data.config.securitytoken, function(err, userInfo) {
           if (err) {
               deferred.reject(err);
           } else {
               deferred.resolve(conn);
           }
       });
       return deferred.promise;
   }




   service.prototype.getAppField = getAppField;
   service.prototype.getApp = getApp;
   service.prototype.getConected = getConected;
   service.prototype.getRecordIds = getRecordIds;
   service.prototype.getRecords = getRecords;
   module.exports = new service();

})();
