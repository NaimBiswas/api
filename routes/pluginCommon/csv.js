(function() {

    'use strict'
    var _ = require('lodash');
    var moduleNames = [];
    var moduleFields = [];
    var service = function() {};
    var dirname = 'allCSV';
    var csv = require('csv-array');
    var q = require('q');
    var fs = require('fs-extra');
    var db = require('../../lib/db');
    var json2csv = require('json2csv');
    var clientSftp = require('ssh2-sftp-client');
    var sftp = new clientSftp();
    var clientFtp = require('ftp');
    var ftp = new clientFtp();
    var configure = require('../../config/config');
    var logger = require('../../logger');


    // var config = require(.)

    function getApp(req, response) {
        moduleNames = [];
        fs.readdir(dirname, (err, files) => {
            files.forEach(file => {
                logger.log(file);
                moduleNames.push({ title: file });
            });
            response.send(moduleNames);
        })
    }

    function getAppField(req, response) {
        moduleFields = [];
        csv.parseCSV('allCSV' + '/' + req.query.title, function(data) {
            _.forEach(data[0], function(obj, index) {
                moduleFields.push({ 'label': obj, 'title': index, 'stype': '' });
            })
            response.send(moduleFields);
        }, false);
    }

    function getRecords(datas, data, sapp, mapping, req) {
        var defered = q.defer();
        var matchedName = [];
        if (sapp && sapp != 'N/A' && fs.existsSync(configure.staticpath + sapp)) {
            csv.parseCSV(configure.staticpath + sapp, function(data) {
              if(mapping.isAllowHeader){
                data.shift();
              }
                defered.resolve(data);
                fs.removeSync(configure.staticpath + sapp);
            }, false);
        } else {
            if (data.config.servetype == 'sftp') {
                sftp.connect({
                    host: data.config.host,
                    port: data.config.port,
                    username: data.config.username,
                    password: data.config.password
                }).then(function() {
                    return sftp.list(data.config.path);
                }).then(function(datacsv) {
                    var matchedName = _.filter(datacsv, function(obj) {
                        return obj.name == mapping.title + '.csv';
                    });
                    logger.log(matchedName[0].name);
                    if (matchedName && matchedName.length && matchedName[0]) {

                        sftp.get(data.config.path + "/" + matchedName[0].name).then(function(stream) {
                            var strm = stream.pipe(fs.createWriteStream(configure.staticpath + matchedName[0].name));
                            strm.on('finish', function() {
                                csv.parseCSV(configure.staticpath + matchedName[0].name, function(datafromsftp) {
                                    if(mapping.isAllowHeader){
                                    datafromsftp.shift();
                                    }
                                    defered.resolve(datafromsftp);
                                    fs.removeSync(configure.staticpath + matchedName[0].name);
                                }, false);

                                // sftp.mkdir(data.config.path + "/passed/", true);
                            });
                            strm.on('error', function(err) {
                                logger.log(err);
                                defered.reject(err);
                            })

                        }).catch(function(err) {
                            defered.reject(err);
                        });
                    } else {
                        defered.reject('');
                    }
                }).catch(function(err) {
                    defered.reject(err);
                });
            }


            if (data.config.servetype == 'ftp') {
                var comparename = mapping.title + '.csv';
                var listing;
                ftp.on('ready', function() {
                    ftp.list(data.config.path, function(err, list) {
                        if (err) {
                            throw err
                        };
                        listing = list;
                        matchedName = _.filter(listing, function(obj) {
                            return obj.name == comparename;
                        });
                        if (matchedName && matchedName.length && matchedName[0]) {
                            ftp.get(data.config.path + '/' + matchedName[0].name, function(err, stream) {
                                if (err) throw err;
                                stream.once('close', function() { ftp.end(); });
                                var strm = stream.pipe(fs.createWriteStream(configure.staticpath + matchedName[0].name));
                                strm.on('finish', function() {
                                    csv.parseCSV(configure.staticpath + matchedName[0].name, function(datafromftp) {
                                        if(mapping.isAllowHeader){
                                        datafromftp.shift();
                                        }
                                        defered.resolve(datafromftp);
                                        fs.removeSync(configure.staticpath + matchedName[0].name);
                                    }, false);
                                });
                            });
                        } else {
                            defered.reject('Null');
                        }
                        ftp.end();
                    });

                });


                ftp.connect({
                    host: data.config.host,
                    port: data.config.portnuber,
                    user: data.config.username,
                    password: data.config.password
                });
            }
        }
        return defered.promise;
    }

    function downloadFile(req, res) {
        var fields = [];
        db.collection('plugin').findOne({ _id: db.ObjectID(req.query.data) }, function(err, data) {
            if (err) {
                logger.log(err, 'debug', 'receive.js');
            }
            _.forEach(data.psqueDetail, function(obj) {
                if(!_.isNull(obj.sapp.title)){
                    logger.log(obj.sapp.label)
                fields.push(obj.sapp.label.toString().replace(/'/g, ""));
              }
            })
            var result = json2csv({ fields });
            var fileName = (+new Date() + '-' + data.title + '.csv').replace(/\/|\\+/g, '_');
            fs.writeFile(configure.staticpath + fileName, result, function(err, csv) {
                if (err) {
                    logger.log(err);
                } else {
                    res.json({ fileName: fileName });
                }
            })
        })
    }

    function uploadFile(req, res) {
        req.pipe(req.busboy);
        req.busboy.on('file', function(fieldname, file, filename) {
            // debug("Uploading: " + filename);
            filename = (new Date()).getTime() + '-' + filename;

            //Path where file will be uploaded
            var fstream = fs.createWriteStream(process.env.FILE_STORE + filename);

            file.pipe(fstream);

            fstream.on('close', function() {
                //   debug("Upload Finished of " + filename);
                var newFileName = process.env.FILE_STORE + filename;
                if (!fs.existsSync(newFileName)) {

                    res.status(404).json({ 'message': 'file not found' })
                    return;
                }
                res.status(200).json({ file: filename });
            })
        })

    }

    function downloadAfterUpload(req, res) {
        var filename = process.env.FILE_STORE + req.query.file;
        if (!fs.existsSync(filename)) {
            res.status(404).json({ 'message': 'file not found' })
            return;
        }

        csv.parseCSV(filename, function(data) {
            res.status(200).json(data);
        }, false);
    }


    service.prototype.getApp = getApp;
    service.prototype.getAppField = getAppField;
    service.prototype.getRecords = getRecords;
    service.prototype.downloadFile = downloadFile;
    service.prototype.uploadFile = uploadFile;
    module.exports = {
        getApp: getApp,
        getAppField: getAppField,
        getRecords: getRecords,
        downloadFile: downloadFile,
        uploadFile: uploadFile,
        downloadAfterUpload: downloadAfterUpload
    }

})();
