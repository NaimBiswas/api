(function () {

    'use strict';
    var flatten = require('flat');
    var flattenService = function () { };

    function init(forFlatten) {
        var startFlatten = flatten(forFlatten);
        console.log(startFlatten);
        return startFlatten;
    }

    flattenService.prototype.init = init;
    module.exports = new flattenService();
    
})();
