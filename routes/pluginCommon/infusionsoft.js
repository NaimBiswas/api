(function () {

    'use strict'
    var api = require('infusionsoft-api');
    var _ = require('lodash');
    var q = require('q');
    var service = function () { };

    var moduleNames = [];
    var moduleFields = [];
    var infusionsoft;

    function getApp(req, response) {
        moduleNames = [];
        infusionsoft = new api.DataContext(req.instanceData[0].config.appname, req.instanceData[0].config.accesstoken);
        _.forEach(Object.keys(infusionsoft), function (item) {
            moduleNames.push({ 'label': item, 'title': item, 'stype': '' });
        });
        response.send(moduleNames);
    }

    function getAppField(req, response) {
        moduleFields = [];
        var selectedApp = req.query.title;
        infusionsoft[selectedApp]
            .take(100)
            .toArray()
            .done(function (fetchedField) {
                _.forEach(Object.keys(fetchedField[0]), function (item) {
                    moduleFields.push({ 'label': item, 'title': item, 'stype': '' });
                });
                response.send(moduleFields);
            }); 
    }

    function getRecords(datas, data, sapp, mapping, req) {
        var defered = q.defer();
        var lastId = mapping.pluginUniqueIdentifier;
        infusionsoft = new api.DataContext(data.config.appname, data.config.accesstoken);
        infusionsoft[sapp]
            .where(Contact.Id, '~>~' + lastId)
            .take(100)
            .toArray()
            .done(function (fetchedField) {
                defered.resolve(fetchedField);
            });
        return defered.promise;
    }

    service.prototype.getApp = getApp;
    service.prototype.getAppField = getAppField;
    service.prototype.getRecords = getRecords;
    module.exports = new service();

})();
