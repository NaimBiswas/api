(function() {

    'use strict'
    var request = require('request');
    var pluginConfig = require('./config')
    var _ = require('lodash');
    var q = require('q');
    var flatten = require('flat');
    var service = function() {};

    function getApp(req, response) {
        var appNames = [];
        _.forEach(pluginConfig.quickbooks.apps, function(data) {
            appNames.push({ title: data });
        });
        response.send(appNames);
    }

    function getAppField(req, response) {
        var moduleFields = [];
        var options = {
            method: 'GET',
            url: pluginConfig.quickbooks.getUrl + req.instanceData[0].config.companyId + '/query?query=select * from ' + req.query.title + ' startposition 1 maxresults 1000',
            headers: {
                'Authorization': 'Bearer ' + req.instanceData[0].config.accesstoken,
                'Accept': 'application/json'
            }
        }

        request(options,
            function(error, response2, body) {
                var bodyToJson = JSON.parse(body);
                var moduleTitle = req.query.title;
                console.log(bodyToJson);
                if (bodyToJson && bodyToJson.QueryResponse && bodyToJson.QueryResponse[moduleTitle] && bodyToJson.QueryResponse[moduleTitle][0]) {
                    var flat = flatten(bodyToJson.QueryResponse[moduleTitle][0]);
                    _.forEach(Object.keys(flat), function(item) {
                        moduleFields.push({ 'label': item, 'title': item, 'stype': '' });
                    });
                    moduleFields = _.orderBy(moduleFields, function(oj) {
                        return oj.label;
                    });
                    response.send(moduleFields);
                } else {
                    response.status(501).json(err);
                }

            });
    }

    function getRecords(datas, data, sapp, mapping, req) {

        var defered = q.defer();
        var appRecords = [];
        var date;
        if (req.pluginUniqueIdentifier) {
            date = req.pluginUniqueIdentifier
        } else {
            date = '2000-01-01T08:09:36.058Z';
        }
        // var date = '2000-01-01T08:09:36.058Z';
        console.log("getrecords", req.pluginUniqueIdentifier);
        console.log(date);
        //var date = mapping.pluginUniqueIdentifier || '2000-01-01T20:12:37.00%2B0000';
        var options = {
            method: 'GET',
            url: pluginConfig.quickbooks.getUrl + data.config.companyId + '/query?query=select * from ' + sapp + ' WHERE MetaData.CreateTime > ' + '\'' + date + '\'' + ' startposition 1 maxresults 100',
            headers: {
                'Authorization': 'Bearer ' + data.config.accesstoken,
                'Accept': 'application/json'
            }
        }
        console.log(options);
        request(options,
            function(error, response2, body) {
                if (error) {
                    defered.reject(error)
                }
                // console.log(response2);
                // var bodyToJson = JSON.parse(body);
                // var moduleTitle = sapp;
                // var flat = flatten(bodyToJson.QueryResponse[moduleTitle][0]);
                if (body) {
                    // console.log(body);
                    var bodyToJson = JSON.parse(body);
                    var moduleTitle = sapp;
                    appRecords = bodyToJson.QueryResponse[moduleTitle];
                    var temp = {};
                    var records = [];
                    var obj2 = ''
                    _.forEach(appRecords, function(object) {
                        temp = {};
                        _.forEach(mapping.psqueDetail || [], function(mapp) {
                            if (mapp.sapp && mapp.sapp.title) {
                                obj2 = '';
                                var splitKey = mapp.sapp.title.split('.');
                                var obj = object;
                                _.forEach(splitKey, function(k, i) {
                                    //if it is array
                                    if (obj && obj instanceof Array) {
                                        _.forEach(obj, function(item) {
                                            if (item[k]) {
                                                obj2 = obj2 + item[k];
                                            }
                                        })
                                    } else { //object
                                        if (obj[k]) {
                                            obj = obj[k];
                                        } else {
                                            obj = '';
                                        }
                                    }
                                });
                                temp[mapp.sapp.title] = obj2 ? obj2 : obj;
                            }
                        })
                        temp.CreateTime = object.MetaData.CreateTime;
                        records.push(temp);
                    })
                    defered.resolve(records);
                } else {
                    defered.reject(body)
                }
            });
        return defered.promise;
    }

    service.prototype.getApp = getApp;
    service.prototype.getAppField = getAppField;
    service.prototype.getRecords = getRecords;
    module.exports = new service();

})();



// moduleFields = [];
//         var selectedApp = req.query.title.toLowerCase();
//         request(pluginConfig.infusionsoft.getUrl + selectedApp + '?access_token=' + req.instanceData[0].config.accesstoken,
//             function(error, response2, body) {
//                 console.log(body);
//                 if (error) {
//                     response.status(501).json(error);
//                 }
//                 var x = req.query.title.toLowerCase();
//                 if (body.indexOf('<') == -1) {
//                     var bodyToJson = JSON.parse(body);
//                     var flat = flatten(bodyToJson[x][0]);
//                     // item.slice(item.lastIndexOf('.')+1)
//                     _.forEach(Object.keys(flat), function(item) {
//                         moduleFields.push({ 'label': item, 'title': item, 'stype': '' });
//                     });
//                     moduleFields = _.orderBy(moduleFields, function(oj) {
//                         return oj.label;
//                     });
//                     response.send(moduleFields);
//                 } else {
//                     response.status(501).json(body);
//                 }
//             });
