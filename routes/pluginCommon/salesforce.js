(function() {

    'use strict'
    var pluginConfig = require('./config.js');
    var _ = require('lodash');
    var request = require('request');
    var q = require('q');
    var service = function() {};


    function getApp(req, response) {
        var appNames = [];
        getConected(req.instanceData[0]).then(function(data) {
            var options = {
                method: 'GET',
                url: 'https://' + req.instanceData[0].config.url + '/services/data/v40.0/sobjects/',
                headers: {
                    'Authorization': 'Bearer ' + JSON.parse(data).access_token
                }
            }
            request(options,
                function(error, response1, body) {
                    if (error) {
                        response.status(501).json(error);
                    }

                    _.forEach(JSON.parse(body).sobjects, function(data) {
                        appNames.push({ title: data.name });
                    });
                    response.send(appNames);
                });
        }, function(err) {
            response.status(501).json(err);
        });
    }


    function getAppField(req, res) {
      var appFields = [];
        getConected(req.instanceData[0]).then(function(data) {
            var options = {
                method: 'GET',
                url: 'https://' + req.instanceData[0].config.url + '/services/data/v40.0/sobjects/' + req.query.title + '/describe',
                headers: {
                    'Authorization': 'Bearer ' + JSON.parse(data).access_token
                }
            }
            request(options,
                function(error, response1, body) {
                    if (error) {
                        res.status(501).json(error);
                    }
                    _.forEach(JSON.parse(body).fields, function(item) {
                        appFields.push({ 'label': item.label, 'title': item.name, 'stype': item.type });
                    });
                    appFields = _.orderBy(appFields, function(oj) {
                        return oj.label;
                    });
                    res.send(appFields);
                });
        }, function(err) {
            res.status(501).json(err);
        });
    }

    function getRecords(datas, data, sapp, mapping, req) {
        var defered = q.defer();
       
        var sfDataPromise = [];
        getConected(data).then(function(result) {
            _.forEach(datas, function(entity) {
                sfDataPromise.push(getsfRecords(JSON.parse(result).access_token, sapp, entity, data));
            });
            q.all(sfDataPromise).then(function(entDatas) {
                return defered.resolve(entDatas);
            }, function(err) {
                return defered.reject(err);
            });
        }, function(err) {
            return err;
        });
        return defered.promise;
    }

    function getsfRecords(token, sapp, entity, data) {
        var defered = q.defer();
        var options = {
            method: 'GET',
            url: 'https://' + data.config.url + '/services/data/v40.0/sobjects/' + sapp + '/' + entity,
            headers: {
                'Authorization': 'Bearer ' + token
            }
        }
        request(options,
            function(error, response1, body) {
                if (error) {
                    defered.reject(error)
                }
                defered.resolve(JSON.parse(body))
            });
        return defered.promise;
    }


    function getRecordIds(data, sapp, mapping) {
        var deffred = q.defer();
        var appRecordIds = [];
        getConected(data).then(function(result) {
            var createdDate;
            if (mapping.pluginUniqueIdentifier) {
                createdDate = mapping.pluginUniqueIdentifier;
            } else {
                createdDate = '2000-01-01T20:12:37.00%2B0000';
            }
            var options = {
                method: 'GET',
                url: 'https://' + data.config.url + '/services/data/v40.0/queryAll/?q=SELECT+Id+from+' + sapp + '+WHERE+CreatedDate+>+' + createdDate + '+limit+100',
                headers: {
                    'Authorization': 'Bearer ' + JSON.parse(result).access_token
                }
            }
            console.log(options.url)
            request(options,
                function(error, response1, body) {
                    if (error) {
                        deffred.reject(error)
                    }
                    // console.log(JSON.parse(body));
                    _.forEach(JSON.parse(body).records, function(obj) {
                        appRecordIds.push(obj.Id);
                    });
                    deffred.resolve(appRecordIds)

                });
        }, function(err) {
            deffred.reject(err)
        });
        return deffred.promise
    }


    function getConected(result) {
        var deferred = q.defer();
        var url = pluginConfig.salesForce.login + result.config.consumerkey + '&client_secret=' + result.config.consumersecret + '&username=' + result.config.username + '&password=' + result.config.password + result.config.securitytoken;
        request({ uri: url, method: 'POST' },
            function(error, response1, body) {
                if (error) {
                    deferred.reject(error);
                }
                if(body){
                    var checkError = JSON.parse(body).error;
                    if(checkError){
                        deferred.reject(error);
                    } else {
                        deferred.resolve(body);                
                    }
                }
            });
        return deferred.promise;
    }

    service.prototype.getAppField = getAppField;
    service.prototype.getApp = getApp;
    service.prototype.getConected = getConected;
    service.prototype.getRecordIds = getRecordIds;
    service.prototype.getRecords = getRecords;
    module.exports = new service();

})();
