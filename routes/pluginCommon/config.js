module.exports = {

    salesForce : {
      login : 'https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id='
    },
    infusionsoft : {
      getUrl : 'https://api.infusionsoft.com/crm/rest/v1/'
    },
    zoho : {
      getModule : 'https://crm.zoho.com/crm/private/json/Info/getModules?authtoken=',
      getUrl : 'https://crm.zoho.com/crm/private/json/'
    },
    quickbooks : {
      getUrl : 'https://sandbox-quickbooks.api.intuit.com/v3/company/',
      apps : ['Account','Attachable','Bill','BillPayment','Budget',
      'Class','CompanyInfo','CreditMemo','Customer','Department',
      'Deposit','Employee','Estimate','Invoice','Item'
    ,'JournalEntry','Payment','PaymentMethod','Preferences','Purchase'
  ,'PurchaseOrder','RefundReceipt','SalesReceipt','TaxAgency','TaxCode'
,'TaxRate','Term','TimeActivity','Transfer','Vendor','VendorCredit']
    }
}
