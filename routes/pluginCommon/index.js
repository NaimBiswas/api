'use strict'
var express = require('express');
var http = require('http');
var db = require('../../lib/db');
var apiRoutes = express.Router();
var infusionsoft = require('./infusionsoft.js');
var salesforce = require('./salesforce.js');
// var dummy = require('./dummy.js');
var zoho = require('./zoho.js');
var csv = require('./csv.js');
var qc2 = require('./qc2.js');
var quickbooks = require('./quickbooks.js');
var _ = require('lodash');
var request = require('request');

module.exports = function(app) {

    function getConfigData(req, res, next) {
        db.pluginConfig.find({ _id: db.ObjectID(req.query.id) }).toArray(function(err, data) {
            if (err) {
                return;
            } else {
                req.instanceData = data;
            }
            next();
        });
    }

    function getPluginData(req, res, next) {

        switch (req.params.pluginType) {
            case 'salesforce':
                switch (req.params.function) {
                    case 'getApp':
                        salesforce.getApp(req, res)
                        break;
                    case 'getAppField':
                        salesforce.getAppField(req, res)
                        break;
                }

                break;
            case 'dummy':
                switch (req.params.function) {
                    case 'getApp':
                        dummy.getApp(req, res)
                        break;
                    case 'getApp':
                        dummy.getApp(req, res)
                        break;
                }
                break;
            case 'zoho':
                switch (req.params.function) {
                    case 'getApp':
                        zoho.getApp(req, res)
                        break;
                    case 'getAppField':
                        zoho.getAppField(req, res)
                        break;
                }
                break;
            case 'csv':
                switch (req.params.function) {
                    case 'getApp':
                        csv.getApp(req, res)
                        break;
                    case 'getAppField':
                        csv.getAppField(req, res)
                        break;
                    case 'downloadFile':
                        csv.downloadFile(req, res)
                        break;
                    case 'uploadFile':
                        csv.uploadFile(req, res)
                        break;
                    case 'downloadAfterUpload':
                        csv.downloadAfterUpload(req, res)
                        break;
                }
                break;
            case 'infusionsoft':
                switch (req.params.function) {
                    case 'getApp':
                        infusionsoft.getApp(req, res)
                        break;
                    case 'getAppField':
                        infusionsoft.getAppField(req, res)
                        break;
                }
                break;
           case 'qc2':
                switch (req.params.function) {
                    case 'getApp':
                        qc2.getApp(req, res)
                        break;
                    case 'getAppField':
                        qc2.getAppField(req, res)
                        break;
                }
                break;
                case 'quickbooks':
                    switch (req.params.function) {
                        case 'getApp':
                            quickbooks.getApp(req, res)
                            break;
                        case 'getAppField':
                            quickbooks.getAppField(req, res)
                            break;
                    }
                    break;
        }
    }

    apiRoutes.get('/:pluginType/:function', getConfigData, getPluginData);
    apiRoutes.post('/csv/uploadFile', csv.uploadFile)
    app.use('/pluginCommon', apiRoutes);

}
