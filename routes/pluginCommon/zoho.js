(function() {

    'use strict'
    var request = require('request');
    var pluginConfig = require('./config')
    var _ = require('lodash');
    var q = require('q');
    var moment = require('moment');
    var moduleNames = [];
    var moduleFields = [];
    var service = function() {};

    function getApp(req, response) {
        moduleNames = [];
        request(pluginConfig.zoho.getModule + req.instanceData[0].config.authenticationToken + '&scope=crmapi',
            function(error, response1, body) {
                if(error){
                    return;
                }
                else{
                    var bodyToJson;
                    try{
                        bodyToJson = JSON.parse(body);
                        _.forEach(bodyToJson.response.result.row, function(data) {
                            moduleNames.push({ title: data.content });
                        });
                        response.send(moduleNames);                    
                    }   
                    catch(error){
                        return;
                    }
                }
            });
    }

    function getAppField(req, response) {
       moduleFields = [];
       request(pluginConfig.zoho.getUrl + req.query.title + '/' + 'getFields?authtoken=' + req.instanceData[0].config.authenticationToken + '&scope=crmapi',
           function(error, response2, body) {
               var bodyToJson = JSON.parse(body);
               var moduleTitle = req.query.title;
               if(moduleTitle.startsWith('CustomModule')){
                   _.forEach(bodyToJson[moduleTitle].section.FL, function(item) {
                       moduleFields.push({ 'label': item.label, 'title': item.dv, 'stype': item.type });
                   });
               }
               else{
                   _.forEach(bodyToJson[moduleTitle].section, function(item) {
                       var moduleName = item.name;
                       if(Array.isArray(item.FL)){
                           _.forEach(item.FL, function(item2) {
                               moduleFields.push({
                               'label': moduleName + '-' + item2.label,
                               'title': item2.label,
                               'stype': item.type });
                           });
                       }
                       else{
                               moduleFields.push({ 'label': item.name, 'title': item.dv, 'stype': item.type });
                       }                        
                   });
               }
               moduleFields = _.orderBy(moduleFields, function(oj) {
                   return oj.label;
               });
               response.send(moduleFields);
           });
   }

    function getRecords(datas, data, sapp, mapping, req) {

        var defered = q.defer();
        var appRecords = [];
        var imp = []
        var temp = {};
        var jsonObjects = [];
        var modifiedDate;
     if (req.pluginUniqueIdentifier) {
                modifiedDate= moment(req.pluginUniqueIdentifier).format("YYYY-MM-DD 00:00:00");
           } else {
                modifiedDate = '2000-01-07 00:00:00';
           } 
        //    + '/' + 'getRecords?newFormat=1&authtoken=' + data.config.authenticationToken + '&scope=crmapi&fromIndex=' + req.fromIndex + '&toIndex=' + req.toIndex + '&sortColumnString=Created Time&sortOrderString=asc',
         request(pluginConfig.zoho.getUrl + sapp + '/' + 'getRecords?newFormat=1&authtoken=' + data.config.authenticationToken + '&scope=crmapi&lastModifiedTime='+ modifiedDate, 
        
            function(error, response2, body) {
                var bodyToJson = JSON.parse(body);
                var moduleTitle = sapp;
                if(bodyToJson && bodyToJson.response && bodyToJson.response.result && bodyToJson.response.result[moduleTitle] && bodyToJson.response.result[moduleTitle].row){
                    appRecords = bodyToJson.response.result[moduleTitle].row;
                    console.log(appRecords);
                    if (appRecords instanceof Array) {
                        _.forEach(appRecords, function(obj) {
                            temp = {};
                            _.forEach(obj.FL, function(item) {
                                temp[item.val] = item.content
                            })
                            imp.push(temp);
                        })
                    } else {
                        temp = {};
                        _.forEach(appRecords.FL, function(item) {
                            temp[item.val] = item.content
                        })
                        imp.push(temp);
                    }
                    defered.resolve(imp);
                }
                else{
                    defered.reject("ok");
                }
            });
        return defered.promise;
    }

    service.prototype.getApp = getApp;
    service.prototype.getAppField = getAppField;
    service.prototype.getRecords = getRecords;
    module.exports = new service();

})();