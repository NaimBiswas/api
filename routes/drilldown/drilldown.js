'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var request = require('request');
var config = require('../../config/config');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'drilldown',
    collection: 'drilldown',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    GET: {
      ONE: {
        after: doAction
      }
    },
    PATCH: {
      ONE: {
        after: doActionRemoved
      }
    }
  }
});

function doAction(req, res, next) {
  console.log(req.ip);
  console.log("---------------------------------------------------------------------------------");
  console.log("drilldown opened");
  console.log("---------------------------------------------------------------------------------");
  var dataObj = {
    user: req.user,
    data: req.data,
    ip: req.ip,
    type: "Drill Down Charts"
  }
  var options = {
    url: config.reportUrl + "/bilogs",
    method: "GET",
    body: dataObj,
    json: true
  }
  request(options, function (error, response, body) {
    if (error) {
      console.log(error);
    } else {
      console.log("dump data");
    }
  });
  res.status(200).json(req.data);
}

function doActionRemoved(req, res, next) {
 
  console.log("---------------------------------------------------------------------------------");
  console.log("Drilldown removed");
  console.log("---------------------------------------------------------------------------------");
  req.data.action = "Removed";
  var dataObj = {
    user: req.user,
    data: req.data,
    ip: req.ip,
    type: "Drill Down Charts",
  }
  var options = {
    url: config.reportUrl + "/bilogs",
    method: "GET",
    body: dataObj,
    json: true
  }
  request(options, function (error, response, body) {
    if (error) {
      console.log(error);
    } else {
      console.log("dump data");
    }
  });
  res.status(200).json(req.data);
}

/**
 * @swagger
 * definition:
 *   FormList:
 *     properties:
 *       id:
 *         type: integer
 */

/**
 * @swagger
 * definition:
 *   Drilldown:
 *     properties:
 *       id:
 *         type: integer
 *       module:
 *         $ref: '#/definitions/Module'
 *       title:
 *         type: string
 *       formList:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/FormList'
 *       procedure:
 *         $ref: '#/definitions/Procedure'
 *       startDate:
 *         type: dateTime
 *       endDate:
 *         type: dateTime
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       createdBy:
 *         $ref: '#/definitions/CreatedBy'
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       version:
 *           type: integer
 *       isMajorVersion:
 *           type: boolean
 */


/**
 * @swagger
 * /drilldown:
 *   get:
 *     tags: [Reports]
 *     description: Returns all drilldown charts
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of drilldown charts
 *         schema:
 *           $ref: '#/definitions/Drilldown'
 */

module.exports = route.router;
