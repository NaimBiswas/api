'use strict'
/**
* @name resources-schema
* @author shoaib ganja <shoaib.g@productivet.com>
*
* @version 1.0
*/
var types = require('../../lib/validator/types');

var rString = types.rString;
var rBool = types.rBool;
var string = types.string;
var object = types.object.bind(types);
var rId = types.rId;
var any = types.any;
var id = types.id;

var schema = {
  module:object({
    _id: rId.label('Module id'),
    title: rString.label('Module title')
  }).label('Module'),
  title: string.label('Title'),
  isActive: rBool,
  procedure:any,
  startDate:any,
  endDate:any,
  appList: any,
  allSelectedProcedure:any,
  commonProcedure:any,
  selectVersionProcedure:any,
  sitelevel:any,
  status:any,
  allApps:any

};

module.exports = schema;
