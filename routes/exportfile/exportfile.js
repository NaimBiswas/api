/**
* @name exportfile
* @author Surjeet Bhadauriya <surjeet.b@productivet.com>
*
* @version 0.0.0
*/

var express = require('express');
var router = express.Router();
var fs = require('fs-extra');
var path = require('path');
var json2csv = require('json2csv');
var _ = require('lodash');
var q = require('q');
// var moment = require('moment');
var moment = require('moment-timezone');
var db = require('../../lib/db');
var excel = require('node-excel-export');
var config = require('../../config/config.js');
var debug = require('../../lib/logger').debug('routes:upload');
var exportFileEntity = require('./exportFileEntity.js');
var decode = require('../../lib/oauth/model').getAccessToken;
var lzstring = require('lz-string');
var pdf = require('html-pdf');
var handlebars = require('handlebars');
var advancedSearchByAppCore = require('../advancedSearch/advancedSearchByAppCore.js');
var advancedSearchByEntityCore = require('../advancedSearch/advancedSearchByEntityCore.js');
var FolderZip = require('folder-zip');
var exportUtilty = require('../../lib/utilities/exportUtilities.js');

var count = 0;
var exportAllCount = 0;
var appHavingEntriesCount =  0;
var noOfApps = 0;
var dirForMasterApp = config.staticpath;
var timeStamp = new Date().getTime();
var keepMasterAppTitle = '';
var singleTitle = '';
var getAllDateForExcel = {};
var multiMasterPath;
var masterTitle = '';
var storeAllSites = [];
var zipName = 'PERFEQTA_' + new Date().getTime() + '_' + moment(moment.utc(new Date()).toDate()).format('MM-DD-YYYY');
var clientTZ = 'America/Chicago';
var keepMasterAppsIds = {};
var logger = require('../../logger');
var currentFileName = __filename;

var request = require("request");
var http = require('https');
var storeAllSiteNames;

var regexStr = /[`~!@#$%^&*()_|+\=÷¿?;:'"<>\{\}\[\]\\\/]/;
var regexIns = new RegExp(regexStr.source, "g");
var zip = new FolderZip();
var options = {
  excludeParentFolder: true, //Default : false. if true, the content will be zipped excluding parent folder.
  parentFolderName: 'v1.0' //if specified, the content will be zipped, within the 'v1.0' folder
};

db.sites.find({}).toArray(function (err , data) {
  storeAllSites = data;
});

//Start: QC3-8458 - Bug.
//Changed By: Mahammad
//Description: display lettest site name to Applied Filter section.

db.siteNames.find({}).toArray(function (err , res) {
  storeAllSiteNames = res;
});

//End: QC3-8458 - Bug.

var dirForExportAll = config.staticpath;
var reqBodyFilters  = {};

router.get('/:file', function(req, res, next) {

  if (config && config.exportFileServer && config.exportFileServer.enabled && config.exportFileServer.url && config.exportFileServer.url.get) {
    req.body.strictSSL =false;
    req.pipe(request.get(`${config.exportFileServer.url.get}${req.params.file}`, req.body)).pipe(res);
  } else {
    handleGet(req, res);
  }

  // logger.log("API Called : api/exportfile :GET method" , 'info', currentFileName);
  // if(!req.params.file){
  //   logger.log("if(!req.params.file) :file not provided" , 'error', currentFileName);
  //   res.status(422).json({'message' : 'file not provided'})
  //   return;
  // }

  // var filename = process.env.FILE_STORE + req.params.file;
  // filename = decodeURI(filename);

  // if (!fs.existsSync(filename)){
  //   logger.log("if(!req.params.file) :file not found" , 'error', currentFileName);
  //   res.status(404).json({'message' : 'file not found'})
  //   return;
  // }

  // res.download(filename, filename);
  // res.on('finish', function () {
  //   fs.removeSync(filename);
  // })

});

function handleGet(req, res) {

  logger.log("API Called : api/exportfile :GET method" , 'info', currentFileName);
  if(!req.params.file){
    logger.log("if(!req.params.file) :file not provided" , 'error', currentFileName);
    res.status(422).json({'message' : 'file not provided'})
    return;
  }

  var filename = process.env.FILE_STORE + req.params.file;
  filename = decodeURI(filename);

  if (!fs.existsSync(filename)){
    logger.log("if(!req.params.file) :file not found" , 'error', currentFileName);
    res.status(404).json({'message' : 'file not found'})
    return;
  }

  res.download(filename, filename);
  res.on('finish', function () {
    fs.removeSync(filename);
  })

}

router.post('/', function(req, res, next) {
    if(config && config.exportFileServer &&
      config.exportFileServer.enabled &&
      config.exportFileServer.url &&
      config.exportFileServer.url.post
    ){
        var bodyString = JSON.stringify(req.body);

        const options = {
          // url:config.exportFileServer.url.post,
          hostname: config.exportFileServer.url.postExpanded.hostname,
          port: config.exportFileServer.url.postExpanded.port,
          path: config.exportFileServer.url.postExpanded.path,
          method: 'POST',
          // strictSSL:false,
          "rejectUnauthorized": false,
          headers: {
            'x-authorization':req.headers['x-authorization'] || req.query['x-authorization'],
            // authorization:req.headers.authorization,
            'client-tz':req.headers['client-tz'],
            'Content-Type': 'application/json',
            'Content-Length': bodyString.length
          },
          json: true,
        };

      http.request(options, function(hRes){

          var rawData = '';
          hRes.on('data', function(chunk) { rawData += chunk; });
          hRes.on('end', function(){
            res.json(JSON.parse(rawData));
          });


        },function(err){
        }).write(bodyString);

    }
    else{
      handlePost(req,res,next);
    }



  // logger.log("API Called : api/exportfile :POST method" , 'info', currentFileName);
  // isAuthorizationAndRender(req, res, next);
});


function handlePost(req, res,next){
  logger.log("API Called : api/exportFileEntity :POST method" , 'info', currentFileName);
  // req.isEntityPost = true;
  isAuthorizationAndRender(req, res, next);
}


router.post('/entity', function(req, res, next) {
  logger.log("API Called : api/exportFileEntity :POST method" , 'info', currentFileName);
  req.isEntityPost = true;



  if(config && config.exportFileServer &&
    config.exportFileServer.enabled &&
    config.exportFileServer.url &&
    config.exportFileServer.url.post
  ){

        var bodyString = JSON.stringify(req.body);

        const options = {
          // url:config.exportFileServer.url.post,
          hostname: config.exportFileServer.url.postExpanded.hostname,
          port: config.exportFileServer.url.postExpanded.port,
          path: config.exportFileServer.url.postExpanded.exportEntity,
          method: 'POST',
          // strictSSL:false,
          "rejectUnauthorized": false,
          headers: {
            'x-authorization':req.headers['x-authorization'] || req.query['x-authorization'],
            // authorization:req.headers.authorization,
            'client-tz':req.headers['client-tz'],
            'Content-Type': 'application/json',
            'Content-Length': bodyString.length
          },
          json: true,
        };

      http.request(options, function(hRes){

          var rawData = '';
          hRes.on('data', function(chunk) { rawData += chunk; });
          hRes.on('end', function(){
            res.json(JSON.parse(rawData));
          });


        },function(err){
        }).write(bodyString);

    }
    else{
      handlePost(req,res,next);
    }


  // isAuthorizationAndRender(req, res, next);
});

function isAuthorizationAndRender(req, res, next) {
  try {
    var xToken = req.headers['x-authorization'] || req.query['x-authorization'];
    clientTZ = req.headers['client-tz']||'America/Chicago';
    if(xToken){
      logger.log("if(xToken) : Inside xToken" , 'info', currentFileName);
        db['allAuthToken'].findOne({token : xToken},function(e,token){
          if (e) {
            logger.log("collection :allAuthToken- allAuthToken call failed : " + e , 'error', currentFileName);
            res.status(403).json({ message: 'You are not authorized.'});
            return;
          }

          //fix by surjeet.b@productivet.com - issue of token.userId => null
          var userId;
          if (token && token.userId) {
            userId = token.userId;
          }else if(token && token.user && token.user._id) {
            userId = token.user._id;
          }else {
            userId = xToken.split('_')[1];
          }
          //end fix by surjeet.b@productivet.com - issue of token.userId => nul

          db['users'].findOne({_id : db.ObjectID(userId)},function(err,user){
            if (err) {
              logger.log("collection :users- users call failed : " + err , 'error', currentFileName);
              res.status(403).json({ message: 'You are not authorized.'});
              return;
            }
            req.user = user||{};

            if (req.user['applicableSites']) {
              logger.log("if (req.user['applicableSites']): Inside", 'info', currentFileName);
              var accessiableSites = req.user['applicableSites'];
              req.user['applicableSitesList'] = _.map(_.cloneDeep(req.user['applicableSites']), function (sites) {
                return _.map(sites, function(subSites){ return subSites._id});
              });
              req.user['applicableSites'] = _.map([].concat.apply([], accessiableSites), function (site) {
                return site._id;
              });
            }
            renderPost(req, res, next);
          });

        });
    } else if (req.headers['authorization'] || req.query['authorization']) {
      logger.log("else if (req.headers['authorization'] || req.query['authorization']) : Inside authorization brear token" , 'info', currentFileName);

      var decompressToken = lzstring.decompressFromEncodedURIComponent(req.headers['authorization']);
      var token = decompressToken;
      if(token && _.startsWith(token,'Bearer ')) {
        decode(token.substring(7), function (err, data) {
          if (err) {
            logger.log("decode(token.substring(7), function (err, data)) : Inside authorization brear token - err -failed : " + err , 'error', currentFileName);
            res.status(403).json({ message: 'You are not authorized.'});
            return;
          }
          req.user = data.user||{};
          renderPost(req, res, next);
        });
      } else {
        logger.log("else: authorization brear token" , 'info', currentFileName);
        return res.status(403).json({ message: 'You are not authorized.'});
      }
    } else {
      logger.log("else: authorization brear token and xtoken not found" , 'info', currentFileName);
      return res.status(403).json({ message: 'You are not authorized.'});
    }
  } catch (err) {
    logger.log("catch: export.js - POST method : " + err , 'error', currentFileName);
    return res.status(403).json({ message: 'You are not authorized. '});
  }
}

function parseJsonForExportFile(getData) {
  logger.log("function parseJsonForExportFile(getData) :Called" , 'info', currentFileName);
  var deferred = q.defer();
  dirForMasterApp = config.staticpath;
  getAllDateForExcel[getData.id] = getAllDateForExcel[getData.id] || [];
  //Get user for display modifiedBy name from id
  getUser().then(function(users) {
    //get all schema for app data
    getSchema(getData).then(function (allSchema) {
      var getEntryDate = {};
      count = 0;
      appHavingEntriesCount = 0;
      //This is temporary function for getting entries
      //Need to change here        parseJsonForExportFile(reqBodyFilters.exportData).then(function (response) {

      getFilterDataFromAppEntries(allSchema, getEntryDate, getData).then(function(responseEntry){
        _.forEach(responseEntry, function(entries, key) {
          if (entries && entries.length > 0) {
            logger.log("getFilterDataFromAppEntries :success - entries.length found" , 'info', currentFileName);
            var getOneSchema = _.find(allSchema, function(s) {
              if (s._id == key) {
                return s;
              }
            });
            getCsvData(getOneSchema, entries, users, reqBodyFilters).then(function (generateDataForExxport) {
              generateDocuments(getData, getOneSchema, generateDataForExxport).then(function (response) {
                if (response && response.filePath) {
                  logger.log("generateDocuments :success - response.filePath found" , 'info', currentFileName);
                  deferred.resolve(response);
                }
              });
            });
          }
        });
      });
    });
  });
  return deferred.promise;
}

function getSchemaSuccess(success) {
  logger.log("function getSchemaSuccess(success) :Called" , 'info', currentFileName);
  var getIds = [];
  getIds = _.map(success, '_id');
  var schemaObjectIds = [];
  _.forEach(getIds, function (id) {
    schemaObjectIds.push(db.ObjectID(id));
  });
  return schemaObjectIds;
}

function getSchemaForMaster(appIds ,actualPassedData) {
  logger.log("function getSchemaForMaster(appIds ,actualPassedData) :Called" , 'info', currentFileName);
  var deferred = q.defer();
  if (!actualPassedData.isMaster) {
    logger.log("if (!actualPassedData.isMaster) - not master app", 'info', currentFileName);
    db.schema.find({_id : db.ObjectID(appIds)}).toArray(function (err , data) {
      deferred.resolve(data);
    });
  }else {
    db.schema.find({_id : {$in : appIds}}).toArray(function (err , data) {
      deferred.resolve(data);
    });
  }
  return deferred.promise;
}

function getUser() {
  logger.log("function getUser :Called" , 'info', currentFileName);
  var deferred = q.defer();
  db.users.find({},{username:1,firstname:1,lastname:1}).toArray(function (err , data) {
    deferred.resolve(data);
  });
  return deferred.promise;
}

function getFilterDataFromAppEntries(allSchema, getEntryDate, getData) {
  logger.log("function getFilterDataFromAppEntries(allSchema, getEntryDate, getData) :Called" , 'info', currentFileName);
  var deferred = q.defer();
  if (reqBodyFilters.isEntity) {
      _.forEach(allSchema, function(schema, index){
        reqBodyFilters.appFilter.apps = [schema._id.toString()];
        reqBodyFilters.appFilter.clientTZ = clientTZ;
        advancedSearchByEntityCore.fetchRecords(reqBodyFilters.appFilter, function (err, response) {
          if (err) {
            logger.log("advancedSearchByEntityCore :fetchRecords : not master app - err -failed : " + err , 'error', currentFileName);
            // console.log(err);
          }

          if (response) {
            logger.log("advancedSearchByEntityCore :fetchRecords : not master app -success" , 'info', currentFileName);
            _.forEach(response, function (data) {
              if (data) {
                getEntryDate[data._id] = data.entries;
              }
            });
            if (index == (allSchema.length - 1)) {
              _.forEach(getEntryDate, function (entries) {
                if (entries && entries.length) {
                  appHavingEntriesCount++;
                }
              });
              deferred.resolve(getEntryDate);
            }
          }
        });
      });
  } else {
    if (reqBodyFilters.appFilter.isForMasterAppOnly == 'masterapps') {
      logger.log("if (reqBodyFilters.appFilter.isForMasterAppOnly) :for master app" , 'info', currentFileName);
      reqBodyFilters.appFilter.apps = [getData.id];
      reqBodyFilters.appFilter.hasMasterAppEntry = [getData.id];
      reqBodyFilters.appFilter.clientTZ = clientTZ;
      // delete reqBodyFilters.appFilter.hasMasterAppEntry;
      advancedSearchByAppCore.fetchRecords(reqBodyFilters.appFilter, function (err, response) {
        if (err) {
          logger.log("advancedSearchByAppCore :fetchRecords : master app - err -failed : " + err, 'error', currentFileName);
          // console.log(err);
        }
        if (response) {
          logger.log("advancedSearchByAppCore :fetchRecords : master app -success" , 'info', currentFileName);
          getEntryDate = {};
          _.forEach(response, function (masterAppData) {
            keepMasterAppsIds[getData.id] = _.filter(masterAppData.apps, function (app) {
              return app.entries && app.entries.length > 0;
            }).length;
            _.forEach(masterAppData.apps, function (appData, index) {
              if (appData) {
                getEntryDate[appData._id] = appData.entries || [];
              }
              if (index == masterAppData.apps.length - 1) {
                _.forEach(getEntryDate, function (entries) {
                  if (entries && entries.length) {
                    appHavingEntriesCount++;
                  }
                });
                deferred.resolve(getEntryDate);
              }
            })
          })
        }
      });
    }else {
      logger.log("if (reqBodyFilters.appFilter.isForMasterAppOnly) :not master app" , 'info', currentFileName);
      _.forEach(allSchema, function(schema, index){
        reqBodyFilters.appFilter.apps = [schema._id.toString()];
        reqBodyFilters.appFilter.clientTZ = clientTZ;
        advancedSearchByAppCore.fetchRecords(reqBodyFilters.appFilter, function (err, response) {
          if (err) {
            logger.log("advancedSearchByAppCore :fetchRecords : not master app - err -failed : " + err , 'error', currentFileName);
            // console.log(err);
          }
          if (response) {
            logger.log("advancedSearchByAppCore :fetchRecords : not master app -success" , 'info', currentFileName);
            _.forEach(response, function (data) {
              if (data) {
                getEntryDate[data._id] = data.entries;
              }
            });
            if (index == (allSchema.length - 1)) {
              _.forEach(getEntryDate, function (entries) {
                if (entries && entries.length) {
                  appHavingEntriesCount++;
                }
              });
              deferred.resolve(getEntryDate);
            }
          }
        });
      });
    }
  }
  return deferred.promise;
}

function getCsvData(schema, entries, users, getAppFilter){
  var deferred = q.defer();
  logger.log("function getCsvData(schema, entries, users, getAppFilter) :Called" , 'info', currentFileName);
  getResourceData('schemaAuditLogs', {identity: schema._id}, function (err, versionWiseSchema) {

  var isExistMasterAppRecord = false;
  var getAllDateForGenerateCSV = [];
  var headers = [];
  var entityHeaders = [];
  var attributeHeaders = [];
  var attrEntityHeaders = [];
  var formattedFilterForPdf = {
    apps: [],
    sites: [],
    attributes: [],
    entities: [],
    status: []
  };
  try{
    logger.log("try: getAppFilter and schema exist" , 'info', currentFileName);

    if (getAppFilter && getAppFilter.exportData && (getAppFilter.exportData.type == 'pdf' || _.find(getAppFilter.exportData, {type: 'pdf'}))) {
      formattedFilterForPdf = getFiltersForPdf(getAppFilter, schema, users);
    }

    if (schema) {
        if(err){
          console.log(err);
        }

        _.forEach(versionWiseSchema, function (obj) {
          _.forEach(obj.attributes, function (attribute) {
            attributeHeaders.push({
              title: attribute.title,
              "s#": attribute["s#"],
              format: (attribute && attribute.type && attribute.type.format) ? attribute.type.format.title : ''
            });
          });

          _.forEach(obj.entities, function (entity) {
            var question = _.find(entity.questions, { isKeyValue: true })||{};
            entityHeaders.push({
                title: entity.title,
                "s#": question["s#"],
                parentId: entity["s#"],
                format: (question.type && question.type.format) ? question.type.format.title : '',
                isEntity: true
              });
          });
        });

        //PQT-4388 hiren
        attrEntityHeaders = [].concat(_.sortBy(_.values(attributeHeaders), 'title'), _.sortBy(_.values(entityHeaders), 'title'));
      _.forEach(entries, function(r) {
        var temp = {};

        _.forEach(attrEntityHeaders, function(header) {
          if (header.isEntity) {
            getValue(header.title, r.entries[header["s#"] + "-" + header["parentId"]] ? r.entries[header["s#"] + "-" + header["parentId"]][header["parentId"]] : '', header.format);
          } else {
            getValue(header.title, r.entries[header["s#"]] ? r.entries[header["s#"]].attributes : '', header.format);
          }
        });

        function getValue(title, value, format) {
          if (format == "Date") {
              temp[title] = value ? moment.utc(new Date(value)).tz(clientTZ).format('MM/DD/YYYY') : '';
          } else if (format == "Time") {
              temp[title] = value ? moment.utc(new Date(value)).tz(clientTZ).format('HH:mm:ss') : '';
          } else {
              temp[title] = value ? value : '';
          }
        }

        var createdDate = moment.utc(new Date(r.createdDate)).tz(clientTZ).format('MM/DD/YYYY HH:mm:ss');
        if (!r.createdDate) {
          createdDate = moment.utc(new Date(r.createdDateInfo)).tz(clientTZ).format('MM/DD/YYYY HH:mm:ss');
        }

        var modifiedDate = moment.utc(new Date(r.modifiedDate)).tz(clientTZ).format('MM/DD/YYYY HH:mm:ss');
        var modifiedBy;
        if (r && r.modifiedByName) {
          modifiedBy = r.modifiedByName;
        }else if(r && r.modifiedBy){
          var keepUserData = _.find(users, function (user) {
               return user._id.toString() == r.modifiedBy.toString(); //QC3-9347 - surjeet
          });
          if (keepUserData) {
            modifiedBy = `${keepUserData.firstname} ${keepUserData.lastname} (${keepUserData.username})`; //QC3-9243 - surjeet - added brackets
          }
        }

        var appStatus = getAppRecordStatus(r);

        temp['Created Date'] = createdDate;
        temp['Modified Date'] = modifiedDate;
        temp['Modified By'] = modifiedBy;
        // PQT-93 by Yamuna
        if(r.assignment && r.assignment.assignee){
          temp['Assignee'] = r.assignment.assignee.map(function (user) {
            return user.firstname + ' ' + user.lastname + ' ('+ user.username + ')';
          }).join(', ');
        } else {
          temp["Assignee"] = 'N/A';
        }
        temp['Record Status'] = appStatus;
        if(r.isRejected){
          temp["Record Status"] = `${temp["Record Status"]} (Rejected)`;
        }
        temp['Batch Entry ID'] = r && r.BatchEntry ? r.BatchEntry.title : 'N/A';
        if (getAppFilter && getAppFilter.appFilter && getAppFilter.appFilter.isForMasterAppOnly == 'apps') {
          temp['Master App'] = r.masterQCSettings ? r.masterQCSettings.title : 'N/A';
          isExistMasterAppRecord = true;
        }
        getAllDateForGenerateCSV.push(temp);
      });
    }
  } catch(e){
    logger.log("catch: getAppFilter and schema not exist : "+ e , 'error', currentFileName);
    console.log(e.stack);
  }
  _.forEach(attrEntityHeaders, function (attrEntityHeader) {
    headers.push(attrEntityHeader.title);
  });
  headers.push("Created Date", "Modified Date", "Modified By", "Assignee", "Record Status", "Batch Entry ID");
  if (isExistMasterAppRecord) {
    headers.push("Master App");
  }

  headers = _.uniq(headers);
  var data = {
    headers: headers,
    data: getAllDateForGenerateCSV,
    schemaTitle: schema.title,
    formattedFilter: formattedFilterForPdf,
    recordsCount: entries.length
  }
  deferred.resolve(data);
  });
  return deferred.promise;
}

function getSchema(data) {
  logger.log("function getSchema(data) :Called" , 'info', currentFileName);
  var deferred = q.defer();
  if (!data.isMaster) {
    getSchemaForMaster(data.id, data).then(function(masterUsedAllSchema){
      singleTitle = masterUsedAllSchema[0].title;
      deferred.resolve(masterUsedAllSchema);
    });
  } else {
    db.masterqcsettings.find({_id: db.ObjectID(data.id)},{schema:1,title:1}).toArray(function(err, master){
      master.forEach(function(m) {
        masterTitle = m.title;
        doDirectoryRelatedStuff(config.staticpath, m.title, 'create');
        var idsArray = getSchemaSuccess(m.schema);
        getSchemaForMaster(idsArray, data).then(function(data) {
          keepMasterAppTitle = m.title.split(/['/''\\''#''?''%']/g).join('_');//QC3-7536 -bug by bhavin ADD-- .split(/['/''\\''#''?''%']/g).join('_'); for replace   / or \ or # or ? or % to '_'
          deferred.resolve(data);
        });
      });
    });
  }
  return deferred.promise;
}
/**
 *
 * @author Surjeet Bhadauriya <surjeet.b@productivet.com>
 * @param {string} resource
 * @param {object} options
 * @param {function} cb
 */
function getResourceData(resource, options, cb) {
  db[resource].find(options).toArray(function(err, data) {
    if (err) {
      return cb(err, null);
    }

    cb(null, data);
  });
}

function doDirectoryRelatedStuff(prefixPath, folderName, action) {
  logger.log("function doDirectoryRelatedStuff(prefixPath, folderName, action) :Called" , 'info', currentFileName);

  dirForMasterApp = config.staticpath;
  if (!action || (action == 'create')) {
    logger.log("create action:master app" , 'info', currentFileName);
    dirForMasterApp = prefixPath + folderName.split(/['/''\\''#''?''%']/g).join('_') + '/';//QC3-7536 -bug by bhavin ADD-- .split(/['/''\\''#''?''%']/g).join('_'); for replace   / or \ or # or ? or % to '_'
    fs.removeSync(dirForMasterApp);
    fs.ensureDirSync(dirForMasterApp);
  }else {
    fs.removeSync(dirForMasterApp + folderName);
  }
}

function getAppRecordStatus(entry) {
  logger.log("function getAppRecordStatus(entry) :Called" , 'info', currentFileName);

  if (!(entry.entries && entry.entries.status)) {
    return "Draft";
  }
  var status = (entry.entries && entry.entries.status) ? entry.entries.status.status : 4;
  var hasPendingWorkflowReview = false;
  if (entry.workflowreview && entry.workflowreview.length) {
    hasPendingWorkflowReview = _.some((entry.workflowreview||[]), function (review) {
      return review.reviewstatus == 1
    });
  }
  if (status==3) {
    return 'Void';
  } else if (status == 4) {
    return 'Draft';
  } else if (hasPendingWorkflowReview && status == 1) {
    return 'Review Pending - Passed';
  } else if (hasPendingWorkflowReview && status == 0) {
    return 'Review Pending - Failed';
  } else if (status == 1) {
    return 'Passed';
  } else {
    return 'Failed'
  }
}

function getFiltersForPdf(getAppFilter, schema, users) {
  logger.log("function getFiltersForPdf(getAppFilter, schema) :Called" , 'info', currentFileName);

  var formattedFilter = {
    apps: [],
    sites: [],
    attributes: [],
    entities: [],
    status: [],
    assignee: [],
    //isRejected: ''
  };
  formattedFilter['apps'].push(schema.title);
  if (getAppFilter.appFilter && getAppFilter.appFilter.attributes.length > 0) {
    logger.log("if (getAppFilter.appFilter && getAppFilter.appFilter.attributes.length > 0) : attributes exists" , 'info', currentFileName);
    _.forEach(getAppFilter.appFilter.attributes, function (attributeOrEntity) {
      attributeOrEntity.keys = _.uniq(attributeOrEntity.keys);//mahammad
      if (!attributeOrEntity.isEntity) {
        _.forEach(schema.attributes, function(attribute) {
          _.forEach(attributeOrEntity.keys, function(key) {
            if (key == attribute['s#']) {
              //Start: QC3-8631 - Bug.
              //Changed By: Mahammad
              //Description: wrong date format is display
              if (!_.isUndefined(attribute) && !_.isUndefined(attribute.type) && !_.isUndefined(attribute.type.format) && attribute.type.format.title == 'Date') {
                formattedFilter.attributes.push({[attribute.title]: moment.utc(new Date(attributeOrEntity.value)).tz(clientTZ).format('MM/DD/YYYY')});
              }else {
                formattedFilter.attributes.push({[attribute.title]: attributeOrEntity.value});
              }
              //End: QC3-8631 - Bug.
            }
          });
        });
      }else {
        _.forEach(schema.entities, function(entity) {
          var keyEntityAttribute = _.find(entity.questions, {isKeyValue: true});
          _.forEach(attributeOrEntity.keys, function(key) {
            //Start: QC3-9016 - Jyotil - 'key == keyEntityAttribute['s#']' is ored because if it comes to entity.
            if (key == (keyEntityAttribute['s#']+'-'+entity['s#']) || key == keyEntityAttribute['s#']) {
            //End: QC3-9016 - Jyotil

              //Start: QC3-8631 - Bug.
              //Changed By: Mahammad
              //Description: wrong date format is display
              if (!_.isUndefined(keyEntityAttribute) && !_.isUndefined(keyEntityAttribute.type) && !_.isUndefined(keyEntityAttribute.type.format) && keyEntityAttribute.type.format.title == 'Date') {
                formattedFilter.entities.push({[entity.title]: moment.utc(new Date(attributeOrEntity.value)).tz(clientTZ).format('MM/DD/YYYY')});
              }else {
                formattedFilter.entities.push({[entity.title]: attributeOrEntity.value});
              }
              //End: QC3-8631 - Bug.
            }
          });
        });
      }
    });
  }

  //START :: PQT-93 by Yamuna
  if(getAppFilter.appFilter.assignee && getAppFilter.appFilter.assignee.length){
    getAppFilter.appFilter.assignee.forEach(function (assigneeUser) {
      var userObj = _.find(users, function (user) {
        return user._id == assigneeUser;
      });
      formattedFilter.assignee.push(userObj.firstname + ' ' + userObj.lastname + ' ('+ userObj.username + ')');
    })
  }
  //END :: PQT-93 by Yamuna

  if (getAppFilter.appFilter && getAppFilter.appFilter.dates) {
    logger.log("if (getAppFilter.appFilter && getAppFilter.appFilter.dates.length > 0) : dates exists" , 'info', currentFileName);

    // QC3-8533 + QC3-9358 - By: Mahammad.
    if(getAppFilter.appFilter.dates.start) {
      formattedFilter['startDate'] = moment.utc(getAppFilter.appFilter.dates.start).tz(clientTZ).format('MM/DD/YYYY');
    }
    if(getAppFilter.appFilter.dates.end){
      formattedFilter['endDate'] = moment.utc(getAppFilter.appFilter.dates.end).tz(clientTZ)
        // .subtract(1, "days")
        .format('MM/DD/YYYY');
    }
  }

  if (getAppFilter.appFilter && getAppFilter.appFilter.searchByDrop && getAppFilter.appFilter.dates.start) {
      if (getAppFilter.appFilter.searchByDrop == '0') {
        formattedFilter['endDate'] = moment.utc(getAppFilter.appFilter.dates.start).tz(clientTZ).format('MM/DD/YYYY');
      }
  }

  if (getAppFilter.appFilter && getAppFilter.appFilter.levelIds && getAppFilter.appFilter.levelIds.length > 0) {
    _.forEach(getAppFilter.appFilter.levelIds, function(levelId) {
      var foundSite = _.find(storeAllSites, {_id: db.ObjectID(levelId)});

      //Start: QC3-8458 - Bug.
      //Changed By: Mahammad
      //Description: display lettest site name to Applied Filter section.
      if (foundSite) {
        var foundSiteName = _.find(storeAllSiteNames, {_id: foundSite.level._id});
        if(foundSiteName){
          formattedFilter.sites.push({[foundSiteName.title]: foundSite.title});
        }
      }
      //End: QC3-8458 - Bug.

    });
  }

  if (getAppFilter.appFilter && getAppFilter.appFilter.status && getAppFilter.appFilter.status.length > 0) {
    _.forEach(getAppFilter.appFilter.status, function(status) {
      var tempStatus;
      switch(status){
        case 0:
        tempStatus = 'Failed';
        break;

        case 1:
        tempStatus = 'Passed';
        break;

        case 2:
        tempStatus = 'Review Pending';//QC3-7485 pending to Review Pending by bhavin
        break;

        case 3:
        tempStatus = 'Void';
        break;

        case 4:
        tempStatus = 'Draft';
        break;

        default:
        tempStatus = 'Draft';
      }
      formattedFilter.status.push(tempStatus);
    });
  }
  
  // PQT-399 by Yamuna
  if(getAppFilter.appFilter.isRejected){
    formattedFilter.status.push("Rejected");
  }

  formattedFilter.batchEntryId = getAppFilter.appFilter.batchEntryId;
  return formattedFilter;
}

function generateDocuments(requestedSchemaData, schema, generateDataForExxport) {
  logger.log("function generateDocuments(requestedSchemaData, schema, generateDataForExxport) :Called" , 'info', currentFileName);
  var deferred = q.defer();
  var pathToExport = (reqBodyFilters.exportType == 'all' ? dirForExportAll + '/' : dirForMasterApp);
  if (requestedSchemaData.type == 'csv') {
    var name = schema.title.replace(/[^a-zA-Z0-9_ ]/g, "");
    var fileName = (name    + '.csv').replace(/\/|\\+/g,'_');
    fileName = fileName.replace(regexIns, '_');
    var result = json2csv(generateDataForExxport);
    if (reqBodyFilters.exportType == 'all' && requestedSchemaData.isMaster) {
      multiMasterPath = config.staticpath + zipName + '/' + requestedSchemaData.title.replace(regexIns, '_') + '/';
    }
    fs.writeFile(multiMasterPath ? multiMasterPath + fileName : pathToExport + fileName, result, function(err, csv) {
      if (err) throw err;
      if (requestedSchemaData.isMaster) {
        count++;
        var tempKeepMasterAppsIdsCount = 0;
        for (var key in keepMasterAppsIds) {
          if (keepMasterAppsIds.hasOwnProperty(key)) {
            tempKeepMasterAppsIdsCount += parseInt(keepMasterAppsIds[key]);
          }
        }
        if (appHavingEntriesCount == tempKeepMasterAppsIdsCount && (reqBodyFilters.exportType == 'single' || noOfApps == Object.keys(keepMasterAppsIds).length)) {
          keepMasterAppTitle = (reqBodyFilters.exportType == 'single' ? keepMasterAppTitle : zipName).replace(regexIns, '_');
          zip.zipFolder(pathToExport, options, function(){
            zip.writeToFile(config.staticpath + keepMasterAppTitle.replace(regexIns, '_') + '.zip');
            deferred.resolve({filePath: path.join(config.staticpath, keepMasterAppTitle + '.zip'), fileName: keepMasterAppTitle + '.zip'});
          });
        }

      }else {
        if (reqBodyFilters.exportType == 'single') {
          deferred.resolve({filePath: path.join(config.staticpath, fileName), fileName: fileName});
        }else {
          exportAllCount++;
          if (appHavingEntriesCount == exportAllCount) {
            zip.zipFolder(pathToExport, options, function(){
              zip.writeToFile(config.staticpath + zipName + '.zip');
              deferred.resolve({filePath: path.join(config.staticpath, zipName + '.zip'), fileName: zipName + '.zip'});
            });
          }
        }
      }
    });
  }else if (requestedSchemaData.type == 'pdf') {
    var appData = [generateDataForExxport];
    if (!dirForMasterApp) {
      dirForMasterApp = config.staticpath;
    }

    var appEntriesDataObj = {
      title: "Generate PDF",
      author: "PERFEQTA",
      appEntriesData: appData
    };
    fs.readFile(config.hbspath + 'export-pdf/html.hbs', function read(err, data) {
      if (err) {
        logger.log("fs.readFile:err - failed : " + err , 'error', currentFileName);
        throw err;
      }
      logger.log("fs.readFile- success" , 'info', currentFileName);
      var template = handlebars.compile(data.toString());
      var html = template(appEntriesDataObj);
      var options = {
        "format": 'Tabloid',
        "height": "10.5in",
        "width": exportUtilty.setPdfWidth(appData[0].headers)
        // "timeout": 900000
      };
      var name = appData[0].schemaTitle.replace(/[^a-zA-Z0-9_ ]/g, "");
      var fileName = (name    + '.pdf');
      // fileName = (appData[0].schemaTitle.split(' ').join('') + '.pdf');
      fileName = fileName.replace(regexIns, '_');
      if (reqBodyFilters.exportType == 'all' && requestedSchemaData.isMaster) {
        multiMasterPath = config.staticpath + zipName + '/' + requestedSchemaData.title.replace(regexIns, '_') + '/';
      }
      console.log(config.staticpath,'--',zipName,'--',pathToExport,'--',zipName,'--',requestedSchemaData.title)
      pdf.create(html, options).toFile(multiMasterPath ? multiMasterPath + fileName : pathToExport + fileName.toString(), function(err, res) {
        if (err){
          logger.log("pdf.create: err - failed : " + err , 'error', currentFileName);
          count++;
          exportAllCount++;
        }else {
          logger.log("pdf.create: success" , 'info', currentFileName);
          if (requestedSchemaData.isMaster) {
            count++;
            var tempKeepMasterAppsIdsCount = 0;
            for (var key in keepMasterAppsIds) {
              if (keepMasterAppsIds.hasOwnProperty(key)) {
                tempKeepMasterAppsIdsCount += parseInt(keepMasterAppsIds[key]);
              }
            }
            if ((reqBodyFilters.exportType == 'single' || noOfApps == Object.keys(keepMasterAppsIds).length) && count == appHavingEntriesCount && count == tempKeepMasterAppsIdsCount) {
              keepMasterAppTitle = (reqBodyFilters.exportType == 'single' ? keepMasterAppTitle : zipName).replace(regexIns, '_');
              zip.zipFolder(pathToExport, options, function(){
                zip.writeToFile(config.staticpath + keepMasterAppTitle + '.zip');
                deferred.resolve({filePath: path.join(config.staticpath, keepMasterAppTitle + '.zip'), fileName: keepMasterAppTitle + '.zip'});
              });
            }
          }else {
            if (reqBodyFilters.exportType == 'single') {
              deferred.resolve({filePath: path.join(config.staticpath, fileName), fileName: fileName});
            }else {
              exportAllCount++;
              if (appHavingEntriesCount == exportAllCount) {
                zip.zipFolder(pathToExport, options, function(){
                  zip.writeToFile(config.staticpath + zipName + '.zip');
                  deferred.resolve({filePath: path.join(config.staticpath, zipName + '.zip'), fileName: zipName + '.zip'});
                });
              }
            }
          }
        }
      });
    });
  }else if (requestedSchemaData.type == 'xlsx') {
    var data = generateDataForExxport;
    var specification = {};
    var dataset = data.data;
    _.forEach(data.headers, function(value){
      specification[value] = {
        displayName: value,
        headerStyle: {},
        width: 150 // <- width in pixels Note: This is compulsary because if there are higher than 2007 excel than in windows os it wrap all the columns into left
      }
    });
    var requestedSchemaDataId = requestedSchemaData.id;
    if (!requestedSchemaData.isMaster) {
      requestedSchemaDataId = 'singleApp';
      getAllDateForExcel[requestedSchemaDataId] = getAllDateForExcel[requestedSchemaDataId] || [];
    }
    getAllDateForExcel[requestedSchemaDataId].push({
      name: schema.title.replace(/\/|\\+/g,''),
      specification: specification,
      data: dataset
    });

    if (requestedSchemaData.isMaster && keepMasterAppsIds[requestedSchemaDataId] != getAllDateForExcel[requestedSchemaDataId].length) {
      // exportAllCount++;
    }else {
      const report = excel.buildExport(getAllDateForExcel[requestedSchemaDataId]);
      if (!requestedSchemaData.isMaster) {
        if (reqBodyFilters.exportType == 'all') {
          pathToExport = config.staticpath;
          singleTitle = zipName
        }
        pathToExport = dirForMasterApp;
        if (singleTitle) {
          singleTitle = singleTitle.replace(regexIns, '_');
        }
        fs.writeFile((singleTitle ? pathToExport + singleTitle : pathToExport + 'Test_Single') + '.xlsx', report, function(err, xlsx) {
          if (err) throw err;
          deferred.resolve({filePath: path.join(pathToExport, singleTitle + '.xlsx'), fileName: singleTitle + '.xlsx'});
        });
      }else {
        if (reqBodyFilters.exportType == 'single') {
          pathToExport = config.staticpath;
        }
        if (reqBodyFilters.exportType == 'all') {
          keepMasterAppTitle = _.isObject(requestedSchemaData) ? requestedSchemaData.title : requestedSchemaData;
        }
        if (keepMasterAppTitle) {
          keepMasterAppTitle = keepMasterAppTitle.replace(regexIns, '_');
        }
        fs.writeFile((keepMasterAppTitle ? pathToExport + keepMasterAppTitle : pathToExport + 'Test_Master') + '.xlsx', report, function(err, xlsx) {
          if (err) throw err;
          if (reqBodyFilters.exportType == 'single') {
            deferred.resolve({filePath: path.join(pathToExport, keepMasterAppTitle + '.xlsx'), fileName: keepMasterAppTitle + '.xlsx'});
          }else {
            var tempKeepMasterAppsIdsCount = 0;
            for (var key in keepMasterAppsIds) {
              if (keepMasterAppsIds.hasOwnProperty(key)) {
                tempKeepMasterAppsIdsCount += parseInt(keepMasterAppsIds[key]);
              }
            }
            exportAllCount++;
            if (appHavingEntriesCount == tempKeepMasterAppsIdsCount && noOfApps == Object.keys(keepMasterAppsIds).length) {
              zip.zipFolder(pathToExport, options, function(){
                zip.writeToFile(config.staticpath + zipName + '.zip');
                deferred.resolve({filePath: path.join(config.staticpath, zipName + '.zip'), fileName: zipName + '.zip'});
              });
            }
          }
        });
      }
    }
  }else {
    deferred.resolve({filePath: 'Invalid File Extesion'});
  }
  return deferred.promise;
}

function renderPost(req, res, next) {
  logger.log("function renderPost(req, res, next) - called" , 'info', currentFileName);
  try {
    if (req.isEntityPost) {
      exportFileEntity.parseAndExportEntity(req, res, next).then(function (response) {
        return res.json({filePath: response.filePath, fileName: response.fileName});
      });
    }else {
      multiMasterPath = undefined;
      getAllDateForExcel = {};
      reqBodyFilters = req.body||{};
      keepMasterAppsIds = {};
      zip = new FolderZip();
      zipName = 'PERFEQTA_' + new Date().getTime() + '_' + moment(moment.utc(new Date()).toDate()).format('MM-DD-YYYY');

      var applicableSites = reqBodyFilters.isEntity ? advancedSearchByEntityCore.getApplicableSites(req.user['applicableSites']) : advancedSearchByAppCore.getApplicableSites(req.user['applicableSites']);
      reqBodyFilters.permissions = req.user['permissions']||[];
      reqBodyFilters.appFilter.permissions = req.user['permissions']||[];
      if (reqBodyFilters.appFilter.levelIds && reqBodyFilters.appFilter.levelIds.length) {

        reqBodyFilters.appFilter.levelIds = _.filter((reqBodyFilters.appFilter.levelIds||[]), function(level){
          return !!level;
        });

        var siteLength = (req.user['applicableSitesList']||[]).length || req.user['applicableSites'].length;
        if(reqBodyFilters.appFilter.levelIds.length != siteLength){
          var length = siteLength-1;
          var objKey = {};
          if (req.user['applicableSitesList']) {
           objKey["levelIds."+length] = {'$in':req.user['applicableSitesList'][length]};
          } else{
           objKey["levelIds."+length] = {'$in':req.user['applicableSites'][length]};
          }
          reqBodyFilters.appFilter.customQuery = objKey;
        }

        if (reqBodyFilters.appFilter.isEntity ? !advancedSearchByEntityCore.applicableSitesContainsSites(applicableSites, reqBodyFilters.appFilter.levelIds) : !advancedSearchByAppCore.applicableSitesContainsSites(applicableSites, reqBodyFilters.appFilter.levelIds)) {
          logger.log((reqBodyFilters.appFilter.isEntity ? "advancedSearchByEntityCore" : "advancedSearchByAppCore") + ".applicableSitesContainsSites: error - failed" , 'info', currentFileName);
          res.status(403).json({ message: 'You are not authorized.'});
          return;
        }
      } else if (applicableSites && applicableSites.length){
        reqBodyFilters.appFilter.applicableSites = applicableSites||[];
      }

      if (reqBodyFilters.exportType == 'single') {
        parseJsonForExportFile(reqBodyFilters.exportData).then(function (response) {
          if (response && response.filePath) {
            if (response.filePath == 'Invalid File Extesion') {
              logger.log("parseJsonForExportFile: response.filePath: error - invalid Extesion" , 'error', currentFileName);
              return res.status(400).end('Bad Request, Invalid File Extension');
            }
            setTimeout(function () {
              // res.download(response.filePath);
              res.json({filePath: response.filePath, fileName: response.fileName});
            }, 300);
          }
        });
      }else {
        dirForExportAll = config.staticpath + zipName;
        exportAllCount = 0;
        noOfApps = reqBodyFilters.exportData.length;
        doDirectoryRelatedStuff(config.staticpath, zipName.replace(regexIns, '_'), 'create');
        _.forEach(reqBodyFilters.exportData, function (item) {
          if (reqBodyFilters.exportType == 'all' && item.isMaster && item.type !== 'xlsx') {
            doDirectoryRelatedStuff(config.staticpath + zipName + '/', item.title.replace(regexIns, '_'), 'create');
          }
          parseJsonForExportFile(item).then(function (response) {
            if (response && response.filePath) {
              if (response.filePath == 'Invalid File Extesion') {
                logger.log("parseJsonForExportFile: response.filePath: error - invalid Extesion" , 'error', currentFileName);
                return res.status(400).end('Bad Request, Invalid File Extension');
              }
              setTimeout(function () {
                // res.download(response.filePath);
                res.json({filePath: response.filePath, fileName: response.fileName});
                doDirectoryRelatedStuff(config.staticpath, zipName.replace(regexIns, '_'), 'remove');
              }, 300);
            }
          });
        });
      }
    }
  } catch (err) {
    logger.log("catch: renderPost: err - invalid Extesion" , 'error', currentFileName);
    return res.json({err: err});
  }
}

module.exports = router;
