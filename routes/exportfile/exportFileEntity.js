/**
* @name exportfile
* @author Surjeet Bhadauriya <surjeet.b@productivet.com>
*
* @version 0.0.0
*/
'use strict'
var express = require('express');
var router = express.Router();
var fs = require('fs-extra');
var path = require('path');
var json2csv = require('json2csv');
var _ = require('lodash');
var q = require('q');
var moment = require('moment-timezone');
var db = require('../../lib/db');
var excel = require('node-excel-export');
var config = require('../../config/config.js');
var pdf = require('html-pdf');
var handlebars = require('handlebars');
var clientTZ = 'America/Chicago';
var logger = require('../../logger');
var currentFileName = __filename;
var exportUtilty = require('../../lib/utilities/exportUtilities.js');
var entriesUtility = require('../../lib/utilities/entriesUtiliese.js');

//QC3-8742 : Bug - mahammad Start

handlebars.registerHelper("valueOfObject",function(value,key){
  // var str = addNewlines(value[key]);
 return new handlebars.SafeString(value[key]);
});

//QC3-8742 : Bug - mahammad End

function parseAndExportEntity(req, res, next) {
  logger.log('Function: parseAndExportEntity (used in API : /exportfile/entity) : - Success', 'info', currentFileName);
  var deferred = q.defer();
  var entityFilter = _.cloneDeep(req.body.entityFilter);
  clientTZ = req.headers['client-tz']||'America/Chicago';
  getEntity(entityFilter, {}).then(function (entityData) {
    logger.log('Callback: inside callback of getEntity (used in API : /exportfile/entity) : - Success', 'info', currentFileName);
    if (entityData && _.size(entityData) > 0) {
      let parsedData = parseData(entityData);
      if (parsedData && _.size(parsedData) > 0) {
        generateDocuments(parsedData, entityFilter.exportDataType).then(function (response) {
          logger.log('Callback: inside callback of generateDocuments (used in API : /exportfile/entity) : - Success', 'info', currentFileName);
          deferred.resolve({filePath: response.filePath, fileName: response.fileName});
        });
      }
    }else {
      deferred.resolve({});
    }
  });
  return deferred.promise;
}

function getEntity(entityFilter, getEntityAndRecords) {
  logger.log('Function: getEntity (used in API : /exportfile/entity) : - Success', 'info', currentFileName);
  var deferred = q.defer();
  if (entityFilter.entities.length > 0) {
    var entityFilterInfo = entityFilter.entities.shift();
    var filter = {
      'isDeleted' : false //QC3-9088: mahammad
    };
    filter['entityRecord.' + entityFilterInfo['s#']] = entityFilterInfo.value;
    if (entityFilterInfo.type && entityFilterInfo.type == 'date') {
      filter['entityRecord.' + entityFilterInfo['s#']] = entityFilterInfo.value.toISOString();
    }
    db.entities.find({_id: db.ObjectID(entityFilterInfo.id)}).toArray(function (err, entity) {
      logger.log('DB call: enity  (used in API : /exportfile/entity) : - Success', 'info', currentFileName);
      if (err) throw err;
      db[entityFilterInfo.id].find(filter).sort(entityFilter.sort || {}).toArray(function (err, entityRecords) {
        logger.log('DB call: enity (used in API : /exportfile/entity) : - Success', 'info', currentFileName);
        if (err) throw err;
        getEntity(entityFilter, getEntityAndRecords).then(function (response) {
          logger.log('Callback: inside callback function of getEntity (used in API : /exportfile/entity) : - Success', 'info', currentFileName);
          if (entity && entity.length) {
            getEntityAndRecords[entity[0]._id] = {
              entity: entity[0],
              entityRecords: entityRecords,
              appliedFilter: []
            }
            var keyAttribute = _.find(entity[0].questions, {'s#': entityFilterInfo['s#']});
            if (keyAttribute && keyAttribute.type && keyAttribute.type.format && keyAttribute.type.format.title == 'Date') {
              getEntityAndRecords[entity[0]._id].appliedFilter[0] = {
                [keyAttribute.title]: moment.utc(new Date(entityFilterInfo.value)).tz(clientTZ).format('MM/DD/YYYY')
              }
            }else {
              getEntityAndRecords[entity[0]._id].appliedFilter[0] = {
                [keyAttribute.title]: entityFilterInfo.value
              }
            }
            deferred.resolve(getEntityAndRecords);
          }
        });
      });
    });
  }else {
    deferred.resolve(getEntityAndRecords);
  }
  return deferred.promise;
}

function parseData(entityData) {
  logger.log("function parseData(entityData) :Called" , 'info', currentFileName);
  var parsedData = {};
  var value = {};

  _.forEach(entityData, function (entityData, key) {
    if (entityData && entityData.entityRecords.length) {
      parsedData[entityData.entity._id] = {
        headers: _.map(entityData.entity.questions, 'title'),
        title: entityData.entity.title,
        data: [],
        recordsCount: entityData.entityRecords.length,
        appliedFilter: entityData.appliedFilter
      };

      //QC3-9088: START : mahammad :- for proper sequence as in questions
      _.forEach(entityData.entityRecords, function (record) {
        value = {};
        _.forEach(entityData.entity.questions, function (attribute) {
          if (record.entityRecord[attribute['s#']] || record.entityRecord[attribute['s#']] == 0) {
            if (attribute.type && attribute.type.format && attribute.type.format.title == 'Date') {
              value[attribute.title] = moment.utc(new Date(record.entityRecord[attribute['s#']])).tz(clientTZ).format('MM/DD/YYYY');
            }else if (attribute.type && attribute.type.format && attribute.type.format.title == 'Time') {
              value[attribute.title] = moment.utc(new Date(record.entityRecord[attribute['s#']])).tz(clientTZ).format('HH:mm:ss');
            }else if (attribute.type && attribute.type.title == 'File Attachment') {

              value[attribute.title] = (record.entityRecord[attribute['s#']]+'').substring((record.entityRecord[attribute['s#']]+'').indexOf('-') + 1, (record.entityRecord[attribute['s#']]+'').length) || '';
              value[attribute.title] = entriesUtility.getFileName(record.entityRecord[attribute['s#']], value[attribute.title]);

            }else {
              value[attribute.title] = record.entityRecord[attribute['s#']];
            }
          } else{
            value[attribute.title] = '';
          }
        });
        parsedData[entityData.entity._id].data.push(value);
      });
      //QC3-9088: END
    }
  });
  return parsedData;
}

function addNewlines(str) {
  if(str.length>25){
    return str.match(new RegExp('.{1,' + 25 + '}', 'g')).join('<br>');
  }
  return str;
}


function generateDocuments(parsedData, exportDataType) {
  logger.log("function generateDocuments(parsedData, exportDataType) :Called" , 'info', currentFileName);
  var deferred = q.defer();
  _.forEach(parsedData, function (value, key) {
    var fileName = value.title.replace(/[^a-zA-Z0-9_ ]/g, "") + '.' + exportDataType;
    if (exportDataType == 'csv') {
      var result = json2csv(value);
      fs.writeFile(config.staticpath + fileName, result, function(err, csv) {
        if (err) throw err;
        deferred.resolve({filePath: path.join(config.staticpath, fileName), fileName: fileName});
      });
    }else if (exportDataType == 'pdf') {
      var appEntriesDataObj = {
        title: "Generate PDF",
        author: "PERFEQTA",
        appEntriesData: [value]
      };
      fs.readFile(config.hbspath + 'export-entity-pdf/html.hbs', function read(err, data) {
        if (err) {
          logger.log("fs.readFile:err - failed : " + err , 'error', currentFileName);
          throw err;
        }
        logger.log("fs.readFile- success" , 'info', currentFileName);

        handlebars.registerHelper('expanded',function(str){
          str = addNewlines(str);
          return new handlebars.SafeString(str);
        })

        var template = handlebars.compile(data.toString());
        var html = template(appEntriesDataObj);
        var options = {
          "format": 'Tabloid',
          "height": "10.5in",
          "width": exportUtilty.setPdfWidth(value.headers),
          "timeout": 900000
        };
        pdf.create(html, options).toFile(config.staticpath + fileName.toString(), function(err, res) {
          if (err){
            logger.log("pdf.create: err - failed : " + err , 'error', currentFileName);
            throw err;
          }else {
            logger.log("pdf.create: success" , 'info', currentFileName);
            deferred.resolve({filePath: path.join(config.staticpath, fileName), fileName: fileName});
          }
        });
      });
    }else if(exportDataType == 'xlsx'){
      var specification = {};
      var dataForExcel = [];
      _.forEach(value.headers, function(val){
        specification[val] = {
          displayName: val,
          headerStyle: {},
          width: 150 // <- width in pixels Note: This is compulsary because if there are higher than 2007 excel than in windows os it wrap all the columns into left
        }
      });
      dataForExcel.push({
        name: fileName,
        specification: specification,
        data: value.data
      });
      const report = excel.buildExport(dataForExcel);
      fs.writeFile(config.staticpath + fileName, report, function(err, xlsx) {
        if (err) throw err;
        logger.log('fs.writeFile (used in API : /exportfile/entity) : - Success', 'info', currentFileName);
        deferred.resolve({filePath: path.join(config.staticpath, fileName), fileName: fileName});
      });
    }
  });
  return deferred.promise;
}

module.exports = {
  parseAndExportEntity: parseAndExportEntity,
  parseData: parseData,
  generateDocuments: generateDocuments
}
