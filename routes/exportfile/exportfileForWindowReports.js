/**
* @name Qulification Performance - get data for window export pdf/excel
* @author Yamuna Sardhara <yamuna.s@productivet.com>
*
* @version 1.0
*/

var express = require('express');
var router = express.Router();
var fs = require('fs-extra');
var path = require('path');
var json2csv = require('json2csv');
var _ = require('lodash');
var q = require('q');
var moment = require('moment-timezone');
var db = require('../../lib/db');
var excel = require('node-excel-export');
var config = require('../../config/config.js');
var debug = require('../../lib/logger').debug('routes:upload');
var exportFileEntity = require('./exportFileEntity.js');
var decode = require('../../lib/oauth/model').getAccessToken;
var lzstring = require('lz-string');
var pdf = require('html-pdf');
var handlebars = require('handlebars');
var advancedSearchByAppCore = require('../advancedSearch/advancedSearchByAppCore.js');
var advancedSearchByEntityCore = require('../advancedSearch/advancedSearchByEntityCore.js');
var exportUtilty = require('../../lib/utilities/exportUtilities.js');

var count = 0;
var exportAllCount = 0;
var appHavingEntriesCount =  0;
var noOfApps = 0;
var dirForMasterApp = config.staticpath;
var timeStamp = new Date().getTime();
var keepMasterAppTitle = '';
var getAllDateForExcel = {};
var masterTitle = '';
var storeAllSites = [];
var storeAllSiteNames = [];
var clientTZ = 'America/Chicago';
var logger = require('../../logger');
var currentFileName = __filename;
var request = require("request");
var http = require('https');
//var regexStr = /[`~!@#$%^&*()_|+\=÷¿?;:'"<>\{\}\[\]\\\/]/;
//var regexIns = new RegExp(regexStr, "g");

handlebars.registerHelper("valueNotEqual",function(value){
  if(typeof value == 'object'){
    return '';
  }else{
    return value;
  }
});

handlebars.registerHelper('ifConditionMatrix', function (v1, operator, v2, options) {
    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '!=':
            return (v1 != v2) ? options.fn(this) : options.inverse(this);
    }
});

function fetchExportedData(windowRecords, filterData){
  var deferred = q.defer();
  parseJsonForExportFile(windowRecords, filterData).then(function (response) {
    if (response && response.filePath) {
      if (response.filePath == 'Invalid File Extesion') {
        logger.log("parseJsonForExportFile: response.filePath: error - invalid Extesion" , 'error', currentFileName);
        deferred.resolve(response);
      }
      setTimeout(function () {
        deferred.resolve(response);
      }, 300);
    }
  });
  return deferred.promise;
}

function parseJsonForExportFile(windowRecords, filterData) {
  logger.log("function parseJsonForExportFile(getData) :Called" , 'info', currentFileName);
  var deferred = q.defer();
  // get sites and siteNames
  getSitesData().then(function(){
    getGeneralSettingData().then(function(generalSettings){
      //Get user for display modifiedBy name from id
      getUser().then(function(users) {
        //get schema
        getSchema(filterData.apps).then(function (getOneSchema) {
          getQuestions(getOneSchema, filterData).then(function (filterData1) {
            getCsvData(getOneSchema, windowRecords, [], users, filterData).then(function(generateDataForExxport){
              generateDocuments(filterData, getOneSchema, generateDataForExxport,generalSettings).then(function (response) {
                if (response && response.filePath) {
                  logger.log("generateDocuments :success - response.filePath found" , 'info', currentFileName);
                  deferred.resolve(response);
                }else{
                  deffered.reject(response);
                }
              }).catch(function(e){
                logger.log("catch: generateDocuments : "+ e , 'error', currentFileName);
              })
            }).catch(function(er){
              logger.log("catch: getCsvData : "+ er , 'error', currentFileName);
            })
          }).catch(function(err){
            logger.log("catch: getQuestions : "+ err , 'error', currentFileName);
          })
        }).catch(function(erro){
          logger.log("catch: getSchema : "+ erro , 'error', currentFileName);
        })
      }).catch(function(error){
        logger.log("catch: getUser : "+ error , 'error', currentFileName);
      })
    }).catch(function(error){
      logger.log("catch: getGeneralSettingData : "+ error , 'error', currentFileName);
    })
  }).catch(function(error){
    logger.log("catch: getSitesData : "+ error , 'error', currentFileName);
  });

  return deferred.promise;
}

function getCsvData(schema, windowRecords, windowRecordsDataPrepared, users, getWindowFilter){
  logger.log("function getCsvData(schema, entries, users, getWindowFilter) :Called" , 'info', currentFileName);
  var deferred = q.defer();
  if(windowRecords && windowRecords.length){
    var window = windowRecords.shift();
    var entries;
    if(window){
      var getAllDateForGenerateCSV = [];
      var headers = [];
      var entryData = {};
      var headersRawData = [];
      var windowHeaders = [];
      var windowData = [];
      var clientTZ = getWindowFilter.clientTZ;
      var counts = '';
      var firstStageHeaders = ['Unit Tested','Void QC', 'Remaining Units', 'Pending Failure', 'Remaining Failure',
      'Process Failure', 'Non Process Failure'];
      var firstStageData = [];
      var isSecondStage = false;
      var secondStageHeaders = [];
      var secondStageData = [];
      var formattedFilterForPdf = {
        apps: [],
        sites: [],
        attributes: [],
        entities: [],
        status: []
      };
      entries = window.records;
      // PQT-4167 by Yamuna
      if(window.counts == "No Records Found." || !entries.length){
        counts = 0;
      }else{
        counts = window.counts;
        isSecondStage = counts.secondStage;
      }
      try{
        logger.log("try: getWindowFilter and schema exist" , 'info', currentFileName);

        if (getWindowFilter && (getWindowFilter.exportAs == 'pdf')) {
          formattedFilterForPdf = getFiltersForPdf(getWindowFilter, schema);
        }
        if(counts && counts.firstStage){
          firstStageData.push(counts.firstStage ? counts.firstStage.getUnitTestedEntriesCount : 0,
            counts.firstStage ? counts.firstStage.voidQCEntries : 0,
            counts.firstStage ? (((window['Sample Size'] - counts.firstStage.getUnitTestedEntriesCount) < '0') ? 0 : (window['Sample Size'] - counts.firstStage.getUnitTestedEntriesCount)) : (window['Sample Size'] ? window['Sample Size'] :0),
            counts.firstStage ? counts.firstStage.pendingFailure : 0,
            counts.firstStage ? (((window.maximumAllowedProcessFailure - counts.firstStage.processFailure) < '0') ? 0 : (window.maximumAllowedProcessFailure - counts.firstStage.processFailure)) : (window.maximumAllowedProcessFailure ? window.maximumAllowedProcessFailure : 0),
            counts.firstStage ? counts.firstStage.processFailure : 0,
            counts.firstStage ? counts.firstStage.nonProcessFailure : 0);
            // QC3-10692 by Yamuna
            if(isSecondStage && counts.secondStageInfo){
              if(getWindowFilter && (getWindowFilter.exportAs == 'pdf')){//QC3-10851 Prachi
                secondStageHeaders= ['Unit Tested', 'Void QC', 'Remaining Units', 'Pending Failure', 'Remaining Failure',
                'Process Failure', 'Non Process Failure'];
                secondStageData.push(counts.secondStageInfo ? counts.secondStageInfo.getUnitTestedEntriesCount : 0,
                  counts.secondStageInfo ? counts.secondStageInfo.voidQCEntries : 0,
                  counts.secondStageInfo ? (((window['additionalTestSecondStage'] - counts.secondStageInfo.getUnitTestedEntriesCount) < '0') ? 0 : (window['additionalTestSecondStage'] - counts.secondStageInfo.getUnitTestedEntriesCount)) : (window['additionalTestSecondStage'] ? window['additionalTestSecondStage'] :0),
                  counts.secondStageInfo ? counts.secondStageInfo.pendingFailure : 0,
                  counts.secondStageInfo ? ((counts.secondStageInfo.processFailure > 0) ? 0 : 1) : 0,
                  counts.secondStageInfo ? counts.secondStageInfo.processFailure : 0,
                  counts.secondStageInfo ? counts.secondStageInfo.nonProcessFailure : 0);
                }else{
                  firstStageHeaders.push('', 'Unit Tested', 'Void QC', 'Remaining Units', 'Pending Failure',
                  'Remaining Failure', 'Process Failure', 'Non Process Failure');
                  firstStageData.push('', counts.secondStageInfo ? counts.secondStageInfo.getUnitTestedEntriesCount : 0,
                  counts.secondStageInfo ? counts.secondStageInfo.voidQCEntries : 0,
                  counts.secondStageInfo ? (((window['additionalTestSecondStage'] - counts.secondStageInfo.getUnitTestedEntriesCount) < '0') ? 0 : (window['additionalTestSecondStage'] - counts.secondStageInfo.getUnitTestedEntriesCount)) : (window['additionalTestSecondStage'] ? window['additionalTestSecondStage'] :0),
                  counts.secondStageInfo ? counts.secondStageInfo.pendingFailure : 0,
                  counts.secondStageInfo ? ((counts.secondStageInfo.processFailure > 0) ? 0 : 1) : 0,
                  counts.secondStageInfo ? counts.secondStageInfo.processFailure : 0,
                  counts.secondStageInfo ? counts.secondStageInfo.nonProcessFailure : 0);
                }
              }else{
                delete window['additionalTestSecondStage'];
              }
            }else{
              // PQT-233 by Yamuna
              firstStageData.push(0,0,(window['Sample Size'] ? window['Sample Size'] : 0),0,(window.maximumAllowedProcessFailure ? window.maximumAllowedProcessFailure : 0),0,0);
              if(window.secondStage){
                if(getWindowFilter && (getWindowFilter.exportAs == 'pdf')){//QC3-10851 Prachi
                  secondStageHeaders.push('Unit Tested', 'Void QC', 'Remaining Units', 'Pending Failure',
                  'Remaining Failure', 'Process Failure', 'Non Process Failure');
                  secondStageData.push(0,0,(window.additionalTestSecondStage ? window.additionalTestSecondStage : 0),0,0,0,0);
                }else{
                  firstStageHeaders.push('', 'Unit Tested', 'Void QC', 'Remaining Units', 'Pending Failure',
                  'Remaining Failure', 'Process Failure', 'Non Process Failure');
                  firstStageData.push('', 0,0,(window['Sample Size'] ? window['Sample Size'] : 0),0,(window.maximumAllowedProcessFailure ? window.maximumAllowedProcessFailure : 0),0,0);
                }
              }
            }
            delete window["_id"];
            delete window["records"];
            delete window["counts"];
            delete window['allowSecondStage'];
            if(window.windowType == '' || (window.windowType && (window.windowType != '1' || window.windowType != '2'))){
              delete window['windowPlan'];
            }
            if(window.windowType == '' || window.windowType != '2'){
              delete window['populationSize'];
            }
            windowHeaders = Object.keys(window);
            windowHeaders = windowHeaders.map(function(key) {
              if (key == 'title') {
                return 'Title';
              }else if(key == 'maximumAllowedProcessFailure'){
                return 'Maximum Allowed Process Failure in Period';
              }else if(key == 'windowType'){
                return 'Window Type';
              }else if(key == 'windowPlan'){
                return 'Window Plan';
              }else if(key == 'populationSize'){
                return 'Population Size';
              }else if (key == 'additionalTestSecondStage') { //QC3-10851 Prachi
                return 'Additional Test for Stage 2';
              }else{
                return key;
              }
            });
            windowData = _.map(window, function (val, key) {
              if (key == 'Test Type') {
                return val.title;
              } else if (key == 'Window Created Date') {
                return moment.utc(new Date(window['Window Created Date'])).tz(clientTZ).format('MM/DD/YYYY HH:mm:ss');
              } else if (key == 'Window Start Date') {
                return moment.utc(new Date(window['Window Start Date'])).tz(clientTZ).format('MM/DD/YYYY');
              } else if (key == 'Filter') {
                if (val && val.length) {
                  var master = '';
                  val.forEach(function (value,index) {
                    for (var key in value) {
                      var keyValue = key + " : " + value[key];
                      if(val.length != index+1){
                        master = master.concat(keyValue + " | ");
                      }else{
                        master = master.concat(keyValue);
                      }
                    }
                  })
                }
                return (val && val.length ? master : 'N/A');
              } else if (key == 'windowType') {
                if (val && val == '1') {
                  return 'Binomial Distribution';
                } else if (val && val == '2') {
                  return 'Hypergeometric Distribution';
                } else {
                  return 'Customized Windows';
                }
              } else if (key == 'windowPlan') {
                return val ? ((val == '1') ? '95% / 95%' : ((val == '2') ? '95% / 75%' : ((val == '3') ? '95% / 90%' : ''))) : '';
              } else {
                return val;
              }
            });

            if (schema && entries && (entries != 'No Records Found.') && entries.length) {
              _.forEach(entries, function(entry) {
                entryData = {};
                delete entry["_id"];
                delete entry["App"];
                if(headersRawData && !headersRawData.length){
                  headersRawData = Object.keys(entry);
                }
                if(!entry.allRecords){
                  entryData = _.map(entry, function(val, key) {
                    if (typeof val == 'object') {
                      return val.value;
                    }else{
                      return val;
                    }
                  });
                  if(headers && !headers.length){
                    headers = headersRawData;
                  }
                  getAllDateForGenerateCSV.push(entryData);
                }
              });
            }else{
              entries = [];
              getAllDateForGenerateCSV.push(entries);
            }
          } catch(e){
            logger.log("catch: getWindowFilter and schema not exist : "+ e , 'error', currentFileName);
          }
          var data = {
            windowHeaders: windowHeaders,
            windowData: windowData,
            firstStageHeaders: firstStageHeaders,
            firstStageData: firstStageData,
            isSecondStage: isSecondStage,
            secondStageHeaders: secondStageHeaders,
            secondStageData: secondStageData,
            headers: headers,
            data: getAllDateForGenerateCSV,
            formattedFilter: formattedFilterForPdf,
            recordsCount: (entries.length == 0)? entries.length : entries.length - 1
          }
          windowRecordsDataPrepared.push(data);
          getCsvData(schema, windowRecords, windowRecordsDataPrepared, users, getWindowFilter).then(function(data){
            deferred.resolve(windowRecordsDataPrepared);
          }, function(){
            deferred.resolve(windowRecordsDataPrepared);
          });
          count ++;
        }else{
          deferred.resolve(windowRecordsDataPrepared);
        }
      }else{
        deferred.resolve(windowRecordsDataPrepared);
      }
      return deferred.promise;
    }

    function getSitesData(){
      var deferred = q.defer();
      db.sites.find({}).toArray(function (err , data) {
        if(err){
          deferred.reject(err);
          return;
        }
        storeAllSites = data;
        db.siteNames.find({}).toArray(function (error , res) {
          if(error){
            deferred.reject(error);
            return;
          }
          storeAllSiteNames = res;
          deferred.resolve(storeAllSiteNames);
        });
      });
      return deferred.promise;
    }

    function getGeneralSettingData() {
      var deferred = q.defer();
      db.generalSettings.findOne({},function(error, res){
        if(error){
          deferred.reject(error);
          return;
        }
        deferred.resolve(res);
      });
      return deferred.promise;
    }

    function getUser() {
      logger.log("function getUser :Called" , 'info', currentFileName);
      var deferred = q.defer();
      db.users.find({},{username:1,firstname:1,lastname:1}).toArray(function (err , data) {
        deferred.resolve(data);
      });
      return deferred.promise;
    }

    function getSchema(appIds) {
      logger.log("function getSchema(appIds ,actualPassedData) :Called" , 'info', currentFileName);
      var deferred = q.defer();
      var schemaObjectIds = [];
      _.forEach(appIds, function (id) {
        schemaObjectIds.push(db.ObjectID(id));
      });
      db.schema.find({_id : {$in : schemaObjectIds}}).toArray(function (err, data) {
        deferred.resolve(data);
      });
      return deferred.promise;
    }

    //get testType
    function getQuestions(getOneSchema, filterData){
      var deferred = q.defer();
      if(filterData.testType && !filterData.testType.length){
        deferred.resolve(filterData);
      }
      var questions = [];
      var procedures = [];
      var questionsArray = [];
      var proceduresArray = [];
      var schemaCount = 0;

      getOneSchema.forEach(function(schema){
        schemaCount++;
        if(schema.procedures && schema.procedures.length){
          //get questions which contains acceptance criteria push into the blank array for removing unused objects
          questions = schema.procedures.reduce(function(c, v){
            v.questions.forEach(function (question) {
              if (question && question.criteria && question.criteria.length) {
                c.push({
                  _id : question._id,
                  title : schema.title+" - "+question.title,
                  's#' : question['s#'],
                  parentId : question.parentId,
                  type : question.type,
                  comboId : question.comboId
                });
              }
            });
            return c;
          }, []);
          var procIds = [];
          //get procedure which contains acceptance criteria questions
          procedures = schema.procedures.reduce(function(c, v){
            v.questions.forEach(function (question) {
              if (question && question.criteria && question.criteria.length) {
                if(_.indexOf(procIds, v._id) == -1){
                  c.push({
                    _id : v._id,
                    title : schema.title+" - "+v.title,
                    's#' : v['s#']
                  });
                  procIds.push(v._id);
                }
              }
            });
            return c;
          }, []);

          questions.forEach(function(question){
            questionsArray.push(question);
          });
          procedures.forEach(function(procedure){
            proceduresArray.push(procedure);
          });
        }
      });

      if(getOneSchema && getOneSchema.length == schemaCount){
        //need to give value of testtype
        if (filterData.testType && filterData.testType[0] && (filterData.testType[0].qSeq || filterData.testType[0]['comboId'])) {
          filterData.testType = filterData.testTypeOption == 'Procedure'?
          _.find(proceduresArray, {'s#': filterData.testType[0].qSeq}) :
          (filterData.testType['comboId'] ?
          (_.find(questionsArray, {'comboId': filterData.testType[0]['comboId']})) :
          (_.find(questionsArray, {'s#': filterData.testType[0].qSeq})));
        }
        deferred.resolve(filterData);
      }
      return deferred.promise;
    }

    function getAppRecordStatus(entry) {
      logger.log("function getAppRecordStatus(entry) :Called" , 'info', currentFileName);
      if (!(entry.entries && entry.entries.status)) {
        return "Draft";
      }
      var status = (entry.entries && entry.entries.status) ? entry.entries.status.status : 4;
      var hasPendingWorkflowReview = false;
      if (entry.workflowreview && entry.workflowreview.length) {
        hasPendingWorkflowReview = _.some((entry.workflowreview||[]), function (review) {
          return review.reviewstatus == 1
        });
      }
      if (status==3) {
        return 'Void';
      } else if (status == 4) {
        return 'Draft';
      } else if (hasPendingWorkflowReview && status == 1) {
        return 'Review Pending - Passed';
      } else if (hasPendingWorkflowReview && status == 0) {
        return 'Review Pending - Failed';
      } else if (status == 1) {
        return 'Passed';
      } else {
        return 'Failed'
      }
    }

    function getFiltersForPdf(getWindowFilter, schemas) {
      logger.log("function getFiltersForPdf(getWindowFilter, schema) :Called" , 'info', currentFileName);
      var formattedFilter = {
        apps: [],
        sites: [],
        attributes: [],
        entities: [],
        status: [],
        testTypeOption: '',
        testType: [],
        windowType: [],
        appWindowtypeAttributevalueLength: {}
      };
      var windowTypes = [ {id: 1, title: "Binomial Distribution"}, {id: 2, title: "Hypergeometric Distribution"}, {id: 3, title: "Customized Windows"} ];

      schemas.forEach(function (schema) {
        formattedFilter['apps'].push(schema.title);
        if (getWindowFilter.attributes && getWindowFilter.attributes.length > 0) {
          logger.log("if (getWindowFilter.attributes && getWindowFilter.attributes.length > 0) : attributes exists" , 'info', currentFileName);
          _.forEach(getWindowFilter.attributes, function (attributeOrEntity) {
            attributeOrEntity.keys = _.uniq(attributeOrEntity.keys);
            if (!attributeOrEntity.isEntity) {
              _.forEach(schema.attributes, function(attribute) {
                _.forEach(attributeOrEntity.keys, function(key) {
                  if (key == attribute['s#']) {
                    if (!_.isUndefined(attribute) && !_.isUndefined(attribute.type) && !_.isUndefined(attribute.type.format) && attribute.type.format.title == 'Date') {
                      if(!_.some(formattedFilter.attributes, function(value,key){return Object.keys(value)[0] == attribute.title && value[Object.keys(value)[0]] == moment.utc(new Date(attributeOrEntity.value)).tz(clientTZ).format('MM/DD/YYYY');})){
                        formattedFilter.attributes.push({[attribute.title]: moment.utc(new Date(attributeOrEntity.value)).tz(clientTZ).format('MM/DD/YYYY')});
                      }
                    }else {
                      if(!_.some(formattedFilter.attributes, function(value,key){return Object.keys(value)[0] == attribute.title && value[Object.keys(value)[0]] == attributeOrEntity.value;})){
                        formattedFilter.attributes.push({[attribute.title]: attributeOrEntity.value});
                      }
                    }
                  }
                });
              });
            }else {
              _.forEach(schema.entities, function(entity) {
                var keyEntityAttribute = _.find(entity.questions, {isKeyValue: true});
                _.forEach(attributeOrEntity.keys, function(key) {
                  if (key == (keyEntityAttribute['s#']+'-'+entity['s#']) || key == keyEntityAttribute['s#']) {
                    if (!_.isUndefined(keyEntityAttribute) && !_.isUndefined(keyEntityAttribute.type) && !_.isUndefined(keyEntityAttribute.type.format) && keyEntityAttribute.type.format.title == 'Date') {
                      if(!_.some(formattedFilter.entities, function(value,key){return Object.keys(value)[0] == entity.title && value[Object.keys(value)[0]] == moment.utc(new Date(attributeOrEntity.value)).tz(clientTZ).format('MM/DD/YYYY');})){
                        formattedFilter.entities.push({[entity.title]: moment.utc(new Date(attributeOrEntity.value)).tz(clientTZ).format('MM/DD/YYYY')});
                      }
                    }else {
                      if(!_.some(formattedFilter.entities, function(value,key){return Object.keys(value)[0] == entity.title && value[Object.keys(value)[0]] == attributeOrEntity.value;})){
                        formattedFilter.entities.push({[entity.title]: attributeOrEntity.value});
                      }
                    }
                  }
                });
              });
            }
          });
        }
      });
      formattedFilter.appWindowtypeAttributevalueLength.attributesLength = formattedFilter.attributes.length;
      formattedFilter.appWindowtypeAttributevalueLength.entitiesLength = formattedFilter.entities.length;
      formattedFilter.appWindowtypeAttributevalueLength.appLength = schemas.length;
      if (getWindowFilter && getWindowFilter.dates) {
        logger.log("if (getWindowFilter && getWindowFilter.dates.length > 0) : dates exists" , 'info', currentFileName);

        if(getWindowFilter.dates.start) {
          formattedFilter['startDate'] = moment.utc(getWindowFilter.dates.start).tz(clientTZ).add(1, 'days').format('MM/DD/YYYY');
        }
        if(getWindowFilter.dates.end){
          formattedFilter['endDate'] = moment.utc(getWindowFilter.dates.end).tz(clientTZ).format('MM/DD/YYYY');
        }
      }

      if (getWindowFilter && getWindowFilter.levelIds && getWindowFilter.levelIds.length > 0) {
        _.forEach(getWindowFilter.levelIds, function(levelId) {
          var foundSite = _.find(storeAllSites, {_id: db.ObjectID(levelId)});
          if (foundSite) {
            var foundSiteName = _.find(storeAllSiteNames, {_id: foundSite.level._id});
            if(foundSiteName){
              formattedFilter.sites.push({[foundSiteName.title]: foundSite.title});
            }
          }
        });
      }
      if(getWindowFilter && getWindowFilter.status){
        formattedFilter.status = getWindowFilter.status;
      }
      formattedFilter.appWindowtypeAttributevalueLength.windowStatusLength = formattedFilter.status.length;
      if(getWindowFilter && getWindowFilter.windowType && getWindowFilter.windowType.length){
        formattedFilter.windowType = getWindowFilter.windowType.map(function(type){
          return _.find(windowTypes,{id: type}).title;
        });
      }
      formattedFilter.appWindowtypeAttributevalueLength.windowTypeLength = getWindowFilter.windowType.length;
      // QC3-10957 by Yamuna
      if(getWindowFilter && getWindowFilter.testType){
        if(getWindowFilter.testType.length && Object.keys(getWindowFilter.testType[0] || {}).length){
          if(getWindowFilter && getWindowFilter.testTypeOption){
            formattedFilter.testTypeOption = getWindowFilter.testTypeOption;
          }
          formattedFilter.testType = getWindowFilter.testType[0].title;
        }else if(Object.keys(getWindowFilter.testType).length  > 1){
          if(getWindowFilter && getWindowFilter.testTypeOption){
            formattedFilter.testTypeOption = getWindowFilter.testTypeOption;
          }
          formattedFilter.testType = getWindowFilter.testType.title;
        }
      }
      return formattedFilter;
    }

    function generateDocuments(filterData, getOneSchema, generateDataForExxport, generalSettings){
      var deferred = q.defer();
      if (filterData.exportAs == 'pdf') {
        var appData = generateDataForExxport;
        if (!dirForMasterApp) {
          dirForMasterApp = config.staticpath;
        }
        var appEntriesDataObj = {
          title: "Generate PDF",
          author: "PERFEQTA",
          logo: config.apiEnvUrl + '/file/' + (generalSettings.siteLogo[0].title || ''),
          generalSettings: generalSettings,
          formattedFilter: appData[0].formattedFilter,
          appEntriesData: appData,
          appWindowtypeAttributevalueLength: appData[0].formattedFilter.appWindowtypeAttributevalueLength
        };
        fs.readFile(config.hbspath + 'export-window/html.hbs', function read(err, data) {
          if (err) {
            logger.log("fs.readFile:err - failed : " + err , 'error', currentFileName);
            deferred.reject(err);
            return;
          }
          logger.log("fs.readFile- success" , 'info', currentFileName);
          var template = handlebars.compile(data.toString());
          var html = template(appEntriesDataObj);
          var options = {
            "format": 'Tabloid',
            "height": "10.5in",
            "header": {
              "height": "1mm"
            },
            "footer": {
              "height": "10mm"
            },
            "width": exportUtilty.setPdfWidth(appData[0].headers)
          };
          var fileName = filterData.filename;
          fileName = fileName.replace(/[`~!@#$%^&*()_|+\=÷¿?;:'"<>\{\}\[\]\\\/]/g, '_');
          pdf.create(html, options).toFile(dirForMasterApp + fileName.toString(), function(err, res) {
            if (err){
              logger.log("pdf.create: err - failed : " + err , 'error', currentFileName);
              deferred.reject(err);
              return;
            }else {
              logger.log("pdf.create: success" , 'info', currentFileName);
              deferred.resolve({filePath: path.join(dirForMasterApp, fileName), fileName: fileName});
            }
          });
        });
      }else if (filterData.exportAs == 'xlsx') {
        const styles = {
          headerDark: {
            font: {
              bold: true,
            }
          }
        };
        var heading = [];
        generateDataForExxport.forEach(function(dataToExport){
          var windowSpecification1 = [];
          var specification1 = [];
          var windowStageSpecification = [];
          _.forEach(dataToExport.windowHeaders, function(value){
            windowSpecification1.push({
              value: value,
              style: styles.headerDark,
            });
          });
          _.forEach(dataToExport.headers, function(value){
            specification1.push({
              value: value,
              style: styles.headerDark,
            });
          });
          _.forEach(dataToExport.firstStageHeaders, function(value){
            windowStageSpecification.push({
              value: value,
              style: styles.headerDark,
            });
          });
          heading.push(windowSpecification1);
          heading.push(dataToExport.windowData);
          heading.push([{
            value: 'First Stage Information',
            style: styles.headerDark,
          }, '', '', '', '', '', '', '',
          dataToExport.isSecondStage ? {
            value: 'Second Stage Information',
            style: styles.headerDark,
          } : '']);
          heading.push(windowStageSpecification);
          heading.push(dataToExport.firstStageData);
          heading.push([]);
          heading.push(specification1);
          dataToExport.data.forEach(function (entriesData) {
            heading.push(entriesData);
          });
          heading.push([]);
          heading.push([]);
        });

        const report = excel.buildExport(
          [ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
            {
              name: 'Report', // <- Specify sheet name (optional)
              heading: heading, // <- Raw heading array (optional)
              specification: {}, // <- Report specification
              data: [] // <-- Report data
            }
          ]
        );

        var fileName = filterData.filename;
        fileName = fileName.replace(/[`~!@#$%^&*()_|+\=÷¿?;:'"<>\{\}\[\]\\\/]/g, '_');
        fs.writeFile(dirForMasterApp + fileName , report, function(err, xlsx) {
          if (err) {
            deferred.reject(err);
            return;
          }
          var resolveObject = {filePath: path.join(dirForMasterApp, fileName), fileName: fileName};
          deferred.resolve(resolveObject);
        });
      }else {
        deferred.resolve({filePath: 'Invalid File Extesion'});
      }
      return deferred.promise;
    }

    module.exports = {
      fetchExportedData: fetchExportedData
    }
