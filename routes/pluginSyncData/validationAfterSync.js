'use strict'
var db = require('../../lib/db');
var logger = require('../../logger');
var _ = require('lodash');
var config = require('../../config/config.js');
var q = require('q');


function validateAttribute(processSchema, val, attrPass1, failedRecords) {
    logger.log("--------step 13 ------validateAttribute---", 'debug', 'receive.js');

    // attrPass1 = [];
    var optchk = {
        'entries': {}
    };

    _.forEach(processSchema.keyvalue, function(keyvalue) {
        if (keyvalue.hasOwnProperty('parentId')) {
            if (!_.isUndefined(val.entries[keyvalue['s#'] + '-' + keyvalue.parentId])) {
                optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId] = {}
                optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId] = val.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId];
            }
        } else {
            optchk.entries[keyvalue['s#']] = {
                'attributes': val.entries[keyvalue['s#']].attributes
            }
        }
    });
    
    if (Object.getOwnPropertyNames(optchk.entries).length == 0) {
        attrPass1.push(val);
    } else {
        var fltr = _.filter(attrPass1, optchk);
        if (fltr.length > 0) {
            val['failreason'] = {};
            _.forEach(processSchema.keyvalue, function(keyvalue) {
                val['failreason'][keyvalue['s#']] = 'Either attribute or entity value already exist. Please provide other value.';
            });
            db.pluginTempRecords.updateOne({ _id: val._id }, { $set: { 'failreason': val.failreason } }, function(err, res) {
                if (err) {
                    console.log(err)
                }
                console.log(res);
            })
        } else {
            attrPass1.push(val);
        }
    }
}




function validateAttributeDb(processSchema, val, attrPass2, oldSchemas) {
    logger.log("--------step 14 ------validateAttributeDb---", 'debug', 'receive.js');

    // attrPass2 = [];
    var optchk = {
        'entries': {}
    };

    _.forEach(processSchema.keyvalue, function(keyvalue) {
        if (keyvalue.hasOwnProperty('parentId')) {
            if (keyvalue.type.format.title === 'Date') {
                optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId] = {};
                optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId] = val.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId];
            } else {
                if (!_.isUndefined(val.entries[keyvalue['s#'] + '-' + keyvalue.parentId])) {
                    optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId] = {};
                    optchk.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId] = val.entries[keyvalue['s#'] + '-' + keyvalue.parentId][keyvalue.parentId];
                }
            }
        } else {
            optchk.entries[keyvalue['s#']] = {
                'attributes': val.entries[keyvalue['s#']].attributes
            }
        }
    });

    if (Object.getOwnPropertyNames(optchk.entries).length == 0) {
        attrPass2.push(val);
    } else {
        var fltr = _.filter(oldSchemas, optchk);
        if (fltr.length > 0) {
            val['failreason'] = {};
            _.forEach(processSchema.keyvalue, function(keyvalue) {
                val['failreason'][keyvalue['s#']] = 'Either attribute or entity value already exist. Please provide other value.'
            });
            db.pluginTempRecords.updateOne({ _id: val._id }, { $set: { 'failreason': val.failreason } }, function(err, res) {
                if (err) {
                    console.log(err)
                } else {
                    console.log(res);
                }
            })
        } else {
            attrPass2.push(val);
        }
    }
}

function validateAttributeEntity(processSchema, val, attrPass1, failedRecords) {
    var optchk = {
        'entityRecord': {}
    };
    _.forEach(processSchema.questions, function(keyvalue) {
        if (keyvalue.isKeyValue) {
            optchk.entityRecord[keyvalue['s#']] = val.entityRecord[keyvalue['s#']]
        }
    });
    if (Object.getOwnPropertyNames(optchk.entityRecord).length == 0) {
        attrPass1.push(val);
    } else {
        var fltr = _.filter(failedRecords, optchk);
        if (fltr.length > 1) {
            if (!_.find(attrPass1, optchk)) {
                attrPass1.push(val);
            } else {
                val['failreason'] = {};
                _.forEach(processSchema.questions, function(keyvalue) {
                    if (keyvalue.isKeyValue) {
                        val['failreason'][keyvalue['s#']] = 'Key attribute value already exists. Please provide other value.';

                    }
                })
                db.pluginTempRecords.updateOne({ _id: val._id }, { $set: { 'failreason': val.failreason } }, function(err, res) {
                    if (err) {
                        console.log(err)
                    }
                })
            }
        } else {
            attrPass1.push(val);
        }
    }
}

function validateAttributeDbEntity(processSchema, val, attrPass2, oldSchemas) {
    var optchk = {
        'entityRecord': {}
    };
    _.forEach(processSchema.questions, function(keyvalue) {
        if (keyvalue.isKeyValue) {
            optchk.entityRecord[keyvalue['s#']] = val.entityRecord[keyvalue['s#']];
        }
    });
    if (Object.getOwnPropertyNames(optchk.entityRecord).length == 0) {
        attrPass2.push(val);
    } else {
        var keyArr = Object.getOwnPropertyNames(optchk.entityRecord)
        var fltr = _.filter(oldSchemas, function(enSchema) {
            return keyArr.some(function(val) {
                return optchk.entityRecord[val] == enSchema.entityRecord[val]
            })
        });
        if (fltr.length) {
            val['failreason'] = {}
            _.forEach(processSchema.questions, function(keyvalue) {
                if (keyvalue.isKeyValue) {
                    val['failreason'][keyvalue['s#']] = 'Key attribute value already exists. Please provide other value.'

                }
            })
            db.pluginTempRecords.updateOne({ _id: val._id }, { $set: { 'failreason': val.failreason } }, function(err, res) {
                if (err) {
                    console.log(err)
                }
            })
        } else {
            attrPass2.push(val);
        }
    }
}

module.exports = {
    validateAttributeDb: validateAttributeDb,
    validateAttribute: validateAttribute,
    validateAttributeDbEntity: validateAttributeDbEntity,
    validateAttributeEntity: validateAttributeEntity

}
