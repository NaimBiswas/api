'use strict'
var express = require('express');
var http = require('http');
var db = require('../../lib/db');
var apiRoutes = express.Router();
var _ = require('lodash');
var ip = require('ip');
var tdb = require('../../lib/db/tdb.js');
var q = require('q');
var logger = require('../../logger');
var validationAfterSync = require('./validationAfterSync');
var common = require('../appSync/common');
var autogenerate = require('../csventries/csventries');
var defaultRoute = require('../defaultRoute/defaultRoute.js');


// var request = require('request');



module.exports = function(app) {

    function processData(req, res, next) {
        switch (req.params.type) {
            case 'delete':
                deleteRecords(req, res, next);
                break;
            case 'deleteAll':
                deleteAllRecords(req, res, next);
                break;
            case 'import':
                importRecords(req, res, next);
                break;
            case 'importAll':
                importAllRecords(req, res, next);
                break;
            case 'default':
                break;
        }
    }

    apiRoutes.post('/:type', processData);
    app.use('/pluginSyncData', apiRoutes);
}

function deleteRecords(req, res, next) {
    var deleteIds = []
    var data = req.body;
    _.forEach(data.array, function(obj) {
        deleteIds.push(db.ObjectID(obj))
    })
    db.pluginTempRecords.updateMany({ '_id': { '$in': deleteIds }, 'mappingId': data.mappingId }, { $set: { 'isDeleted': true } });
    res.send("Record(s) Deleted successfully.");
}

function deleteAllRecords(req, res, next) {
    var data = req.body;
    db.pluginTempRecords.updateMany({ 'mappingId': data.mappingId, failreason: { $exists: (data.value == true) ? true : false } }, { $set: { 'isDeleted': true } });
    res.send("Record(s) Deleted successfully.");
}

function importRecords(req, response, next) {

    var insertIds = []
    var pluginTempData = [];
    var data = req.body;
    var qcActivity = [];
    var entityRecordList = [];
    var appRecordList = [];
    var resultofvalidation = [];
    _.forEach(data.array, function(obj) {
        insertIds.push(db.ObjectID(obj))
    })
    db.pluginTempRecords.find({ '_id': { '$in': insertIds }, 'mappingId': data.mappingId }).toArray(function(err, results) {
        if (err) {
            logger.log(err);
        } else {
            if (results) {
                pluginTempData = _.cloneDeep(results);
                allvalidation(pluginTempData, data).then(function(res) {
                    resultofvalidation = res;

                    if (resultofvalidation == 0) {
                        response.send("Record(s) imported successfully.");
                    }
                    verifiedData(resultofvalidation, data.appId, entityRecordList, appRecordList)
                    if (entityRecordList.length) {
                        insertBulkdata(entityRecordList, data.appId).then(function(entityRecord) {
                            entityRecord.ops.forEach(function(obj) {
                                db[data.appId].auditThis1(obj, ip.address());
                            })
                            db.pluginTempRecords.updateMany({ '_id': { '$in': insertIds }, 'mappingId': data.mappingId }, { $set: { 'isDeleted': true } }, function(err, res) {

                                if (err) {
                                    logger.log(err);
                                }
                                logger.log(res);
                            });
                            response.send("Record(s) imported successfully.");
                        }, function(err) {
                            logger.log(err);

                        })
                    }
                    if (appRecordList.length) {
                        db.schema.findOne({ _id: db.ObjectID(data.appId) }, { title: 1, keyvalue: 1, attributes: 1 }, function(err, result1) {
                            if (err) {
                                console.log(err);
                            }
                            checkAutoNumber(result1, appRecordList).then(function(entries) {
                                insertBulkdata(entries, data.appId).then(function(appRecords) {
                                    if (results && results.length) {
                                        appRecords.ops.forEach(function(obj) {
                                            qcActivity.push(qcActivityMapper(obj, result1));
                                            db[data.appId].auditThis1(obj, ip.address());
                                        });
                                        if (result1 && qcActivity && qcActivity.length) {
                                            setQcEntryActivity(qcActivity);
                                        }
                                    }
                                    db.pluginTempRecords.updateMany({ '_id': { '$in': insertIds }, 'mappingId': data.mappingId }, { $set: { 'isDeleted': true } }, function(err, data) {
                                        if (err) {
                                            logger.log(err);
                                        }
                                    });
                                    response.send("Record(s) imported successfully.");
                                }, function(err) {

                                })
                            }, function(err) {

                            })

                        })
                    }
                }, function(err) {
                    console.log("ok");
                });
            }
        }
    });

    // db.collection(db.ObjectID(data.appId)).remove({'_id':{'$in': deleteIds},'mappingId' : data.mappingId});
}

function importAllRecords(req, response, next) {
    var insertIds = [];
    var qcActivity = [];
    var pluginTempData = [];
    var data = req.body;
    var entityRecordList = [];
    var appRecordList = [];
    var resultofvalidation = [];
    _.forEach(data.array, function(obj) {
        insertIds.push(db.ObjectID(obj))
    })
    db.pluginTempRecords.find({ 'mappingId': data.mappingId, failreason: { $exists: false }, isDeleted: false }).toArray(function(err, results) {
        if (err) {
            logger.log(err);
        } else {
            if (results) {
                pluginTempData = _.cloneDeep(results);
                console.log(data);
                allvalidation(pluginTempData, data).then(function(res) {
                    resultofvalidation = res;
                    if (resultofvalidation == 0) {
                        response.send("Record(s) imported successfully.");
                    }
                    verifiedData(resultofvalidation, data.appId, entityRecordList, appRecordList)
                    if (entityRecordList.length) {
                        insertBulkdata(entityRecordList, data.appId).then(function(entityRecord) {
                            entityRecord.ops.forEach(function(obj) {
                                db[data.appId].auditThis1(obj, ip.address());
                            })
                            db.pluginTempRecords.updateMany({ 'mappingId': data.mappingId, failreason: { $exists: false } }, { $set: { 'isDeleted': true } }, function(err, res) {
                                    if (err) {
                                        logger.log(err);
                                    }
                                    logger.log(res);
                                })
                                response.send("Record(s) imported successfully.");
                        }, function(err) {
                            logger.log(err);
                        })
                    }
                    if (appRecordList.length) {
                        db.schema.findOne({ _id: db.ObjectID(data.appId) }, { title: 1, keyvalue: 1, attributes: 1 }, function(err, result1) {
                            checkAutoNumber(result1, appRecordList).then(function(entries) {
                                    insertBulkdata(entries, data.appId).then(function(appRecords) {
                                        if (results && results.length) {
                                            appRecords.ops.forEach(function(obj) {
                                                qcActivity.push(qcActivityMapper(obj, result1));
                                                db[data.appId].auditThis1(obj, ip.address());
                                            });
                                            if (result1 && qcActivity && qcActivity.length) {
                                                setQcEntryActivity(qcActivity);
                                            }
                                        }
                                        db.pluginTempRecords.updateMany({ 'mappingId': data.mappingId, failreason: { $exists: false } }, { $set: { 'isDeleted': true } }, function(err, data) {
                                            if (err) {
                                                logger.log(err);
                                            }
                                        });
                                        response.send("Record(s) imported successfully.");
                                    }, function(err) {

                                    })

                                }, function(err) {

                                })
                                //  db[data.appId].auditThis1(obj,ip.address());

                        })
                    }
                }, function(err) {
                    console.log("ok");
                });
            }
        }
    })
}





function verifiedData(pluginTempData, appId, entityRecordList, appRecordList) {
    _.forEach(pluginTempData, function(data) {
        delete data._id;
        delete data.failreason;
        delete data.values;
        delete data.mappingId;
        delete data.syncId;
        delete data.type;
        delete data.transactionId;
        delete data.autoSyncId;
        delete data.pluginUniqueIdentifier;

        data.createdDateInfo = new Date();
        data.createdDate = new Date();
        data.modifiedDate = new Date();
        if (data.entityRecord) {
            data.isDeleted = false;
            entityRecordList.push(data);
        } else {
            delete data.isDeleted;
            appRecordList.push(data);
        }
    });
}

function insertBulkdata(insertThisData, appId) {
    var defered = q.defer();
    db.collection(db.ObjectID(appId).toString()).insertMany(insertThisData, function(err, result) {
        if (err) {
            logger.log(err);
            defered.reject(err);
        } else {
            logger.log(result);
            defered.resolve(result);
        }
    })
    return defered.promise;
}

function qcActivityMapper(qcEntry, schemaDetail) {

    var attributes = (schemaDetail.keyvalue || []).filter(function(keyValue) {
        return (keyValue && keyValue['s#'] && qcEntry && qcEntry.entries && qcEntry.entries[keyValue['s#']]);
    }).map(function(keyValue) {
        var keyValueObj = {
            'title': keyValue.title,
            'type': keyValue.type,
        };
        if (keyValue['s#'].toString().split('-')[1]) {
            keyValueObj.value = qcEntry.entries[keyValue['s#']][keyValue['s#'].toString().split('-')[1]]
        } else {
            keyValueObj.id = keyValue._id;
            keyValueObj.value = qcEntry.entries[keyValue['s#']]['attributes']
        }
        return keyValueObj;
    });
    return {
        'title': schemaDetail.title,
        'attributes': attributes,
        'qcformId': schemaDetail._id.toString(),
        'qcentryId': qcEntry._id.toString(),
        'createdDate': qcEntry.createdDate,
        'status': 'Created',
        'createdBy': db.ObjectID(qcEntry.createdBy),
        'modifiedBy': db.ObjectID(qcEntry.modifiedBy),
        'createdByName': qcEntry.createdByName,
        'modifiedByName': qcEntry.modifiedByName,
        'modifiedDate': qcEntry.modifiedDate,
        'version': 1,
        'isMajorVersion': true
    };
}
// End - QC3-8865 - By Jyotil

/**
 * @author: Jyotil
 * @description : QC3-8865 -insert into qcentryactivity with {qcEntryActivity}
 * @param : {Array} qcEntryActivity
 */
function setQcEntryActivity(qcEntryActivity) {
    db['qcentryactivity'].insertMany(qcEntryActivity, function postThenCallback(err, Success) {
        if (err) {
            logger.log("collection: " + entity + " find", 'error', currentFileName);
        } else {
            logger.log('qcEntryActtivity are created')
        }
    });
}

function allvalidation(passedRecords, data) {
    var defered = q.defer();
    var attrPass1 = [];
    var attrPass2 = [];
    q.all([
        common.getSchema(data.appId, data.appVersion, data.type),
        common.getAppRecord(data.appId.toString(), data.type)
    ]).then(function(res) {
        var processSchema = res[0];
        var oldSchemas = res[1];
        // if(res[0]){
        //    return 0;
        // }
        _.forEach(passedRecords, function(passrecord, inx) {
                if (data.type == 'app') {
                    validationAfterSync.validateAttribute(processSchema[0], passrecord, attrPass1, passedRecords)
                } else {
                    validationAfterSync.validateAttributeEntity(processSchema[0], passrecord, attrPass1, passedRecords)
                }
            })
            // console.log(attrPass1);
        _.forEach(attrPass1, function(record, inx) {
                if (data.type == 'app') {
                    validationAfterSync.validateAttributeDb(processSchema[0], record, attrPass2, oldSchemas)
                } else {
                    validationAfterSync.validateAttributeDbEntity(processSchema[0], record, attrPass2, oldSchemas)
                }
            })
            // console.log(attrPass2);
        defered.resolve(attrPass2)
    }, function(err) {

    })
    return defered.promise;
}

function checkAutoNumber(schemaKey, entries) {
    var deffered = q.defer();
    var autoNumberObj = _.find(schemaKey.attributes, function(att) {
        return att.type.title == 'Auto Generate';
    });
    if (autoNumberObj) {
        defaultRoute.getNextAutoNumber(autoNumberObj._id, entries.length).then(function(data) {
            var autoNumberCounter = data.autoGeneratedAttributeCounter + 1;
            entries.forEach(function(entry) {
                entry.entries[autoNumberObj['s#']] = { attributes: autoNumberObj.autoGenerateNumberPrefix + autoNumberCounter + autoNumberObj.autoGenerateNumberSuffix }
                autoNumberCounter++;
            })
            deffered.resolve(entries);
        }).catch(function() {
            deffered.reject({ error: 'Auto increment number error' })
        })
    } else {
        deffered.resolve(entries);
    }
    return deffered.promise;
}
