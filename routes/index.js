'use strict'
var express = require('express');
var fs = require("fs");
var home = require('./home');
var users = require('./users');
var modules = require('./modules');
var tags = require('./tags');
var sites = require('./sites');
var batchentrysettings = require('./batchentrysettings');
var documentsettings = require('./documentsettings');
var dmsperfeqtamapping = require('./dmsperfeqtamapping');
var batchEntries = require('./batchEntries');
var siteNames = require('./siteNames');
var batchEntryService = require('../lib/batchEntryService/index');
var log = require('../logger');

var sets = require('./sets');
var moment = require('moment');
//Start: QC3-6731 - Improvement. -by bhavin add collection for advanced search
var searchFavorites = require('./searchFavorites');
//End: QC3-6731
// var extractScripts = require('./extractScripts'); //Prachi
var linkedTbl = require('./linkedTbl');
var calendar = require('./calendar');
var calendarThresholds = require('./calendarThresholds');
var calendarAutomated = require('./calendarAutomated');
var calendarWindows = require('./calendarWindows');
var questions = require('./questions');
var procedures = require('./procedures');
var entityRecords = require('./entityRecords');
var entities = require('./entities');
var schema = require('./schema');
var preSchema = require('./schema/preSchema');
var schedules = require('./schedules');
var thresholds = require('./thresholds');
var thresholdsNotification = require('./thresholdsNotification');
var roles = require('./roles');
var types = require('./types');
var attributes = require('./attributes');
var validators = require('./validators');
var securityQuestions = require('./securityQuestions');
var resources = require('./resources');
var settings = require('./settings');
var automated = require('./automated');
var manualimport = require('./manualimport');
var entries = require('./entries');
var status = require('./status');
var temp = require('./temp');
var file = require('./file');
var importfile = require('./importfile');
var csventries = require('./csventries');
var masterqcsettings = require('./masterqcsettings');
var masterQCEntries = require('./masterQCEntries');
var qcentryactivity = require('./qcentryactivity');
var schemaSyncDetails = require('./schemaSyncDetails');
var generalSettings = require('./generalSettings');
var loginattempts = require('./loginattempts');
var reportConfiguration = require('./reportConfiguration');
var reportPermissionConfiguration = require('./reportPermissionConfiguration');
var logiGetSecureKey = require('./logiGetSecureKey');
var drilldown = require('./drilldown');
var controlchart = require('./controlchart');
var controlChartParameters = require('./controlChartParameters');
var apiAccessConfig = require('./apiAccessConfig');
var groupsettings = require('./groupsettings');
var appAccessConfig = require('./appAccessConfig');
var appEntityConfigEntity = require('./appEntityConfigEntity');
var fetchEntityRecords = require('./fetchEntityRecords');
var fetchKeyAttributeFilterSchemes = require('./fetchKeyAttributeFilterSchemes');
var exportfile = require('./exportfile');
var generateDocuments = require('./exportfile/generateDocuments');
var defaultRoute = require('./defaultRoute');
var assignment = require('./assignment');
var collectionCheck = require('../lib/collectionCheck');
var _ = require('lodash');
var q = require('q');
var authorize = require('../lib/oauth/authorize');
var restler = require('restler');
var appInitiateentry = require('./appInitiateentry');
var mappedLinkToEntity = require('./mappedLinkToEntity');
var mailer = require('../lib/mailer');
var licenseAgreement = require('./users/licenseAgreement');
//bi
var reportTemplates = require('./reportTemplates');
var ReportSchemaColumns = require('./ReportSchemaColumns');
var queueFiles = require('./queueFiles');
var reportSettings = require('./reportSettings');
var customReportColumnDetail = require('./customReportColumnDetail');
var bireports = require('./bireports');
var customReport = require('./customReport');
var tokenValidator = require('./vc4/validateToken/validateToken');
var vclogin = require('./vc4/vclogin/vclogin');
var vc4Role = require('./vc4/role/role');
//bi

//plugin
var plugin = require('./plugin');
var pluginTempRecords = require('./pluginTempRecords');
var pluginConfig = require('./pluginConfig');
var pluginlist = require('./pluginlist');
var autoSync = require('./autoSync');
var autoSyncHistory = require('./autoSyncHistory');
var s3 = require('./s3');
// var autoSyncCron = require('./autoSyncCron');
//plugin

//Qulification Performance
var windows = require('./windows');
var windowQueueFiles = require('./windowQueueFiles')
var windowReport = require('./windowReport');
var logger = require('../logger');
var currentFileName = __filename;
//chandni - code start - Added below code for QC3-1904
var versions = require('./versions');
//chandni - code start - Added below code for QC3-1904

var db = require('../lib/db');
var loggedInUser = require('../lib/oauth/loggedInUserDetails');
var dbConfig = require('../lib/db/db');
var config = require('../config/config.js');
var auditThis1 = require('../lib/db/db').auditThis1;
var tokenUtility= require('../lib/utilities/tokenUtilities'); // PQT-2075 : saurabh

// redis code
var client;
var redisErrFlag = false;
var redisConnectionTries = 0;

function tryRedisConnect(rErr,rSuccess){

  redisConnectionTries++;
  client = redis.createClient({host:config.redisServer.host,port:config.redisServer.port});
  client.on("error", function (err) {
    console.log("Error----- " + err);
    redisErrFlag = true;
    //client.quit();


    if(redisConnectionTries<3){
      //tryRedisConnect(rErr,rSuccess)
    }
    else{
      if(rErr){
        rErr()
      }
    }

    // client = redis.createClient({host:config.redisServer.host,port:config.redisServer.port})
  });

  client.on("connect", function (err) {
    console.log("connect----- ");
    redisConnectionTries = 0;
    redisErrFlag = false;
    if(rSuccess){
      rSuccess();
    }

  });

  if(config.redisServer.password && !redisErrFlag){
    client.auth(config.redisServer.password, function() {});
  }

  client.on("end", function (err) {
    console.log("disconnect----- ");

  });

  client.on("reconnecting", function (err) {
    console.log("reconnecting----- ");

  });

  client.on("idle", function (err) {
    console.log("idle----- ");

  });

}

if(config.cacheOfbatchlockData == 'redis'){

  var redis = require("redis");
  tryRedisConnect();
}

function configure(app) {
  logger.log('Function: configure - Start', 'info', currentFileName);
  app.use('/', home);
  app.use('/users', users);
  app.use('/modules', modules);
  app.use('/tags', tags);
  app.use('/sites', sites);
  app.use('/batchentrysettings', batchentrysettings);
  app.use('/documentsettings', documentsettings);
  app.use('/dmsperfeqtamapping', dmsperfeqtamapping);
  app.use('/batchEntries', batchEntries);
  app.use('/siteNames', siteNames);
  app.use('/sets', sets);
  // app.use('/extractScripts', extractScripts);//Prachi
  app.use('/linkedTbl', linkedTbl);
  //Start: QC3-6731 - Improvement. -by bhavin add collection for advanced search
  app.use('/searchFavorites', searchFavorites);
  //End: QC3-6731 - Improvement.
  app.use('/calendar', calendar);
  app.use('/calendarThresholds', calendarThresholds);
  app.use('/questions', questions);
  app.use('/procedures', procedures);
  app.use('/entityRecords', entityRecords);
  app.use('/entities', entities);
  app.use('/schema', schema);
  app.use('/preSchema', preSchema);
  app.use('/licenseAgreement', licenseAgreement);
  app.use('/schedules', schedules);
  app.use('/thresholds', thresholds);
  app.use('/thresholdsNotification', thresholdsNotification);
  app.use('/roles', roles);
  app.use('/types', types);
  app.use('/attributes', attributes);
  app.use('/validators', validators);
  app.use('/securityQuestions', securityQuestions);
  app.use('/resources', resources);
  app.use('/settings', settings);
  app.use('/automated', automated);
  app.use('/calendarAutomated', calendarAutomated);
  app.use('/manualimport', manualimport);
  app.use('/entries', entries);
  app.use('/file', file);
  app.use('/status', status);
  app.use('/temp', temp);
  app.use('/importfile', importfile);
  app.use('/csventries', csventries);
  app.use('/masterqcsettings', masterqcsettings);
  app.use('/masterQCEntries', masterQCEntries);
  app.use('/qcentryactivity', qcentryactivity);
  app.use('/schemaSyncDetails', schemaSyncDetails);
  app.use('/generalSettings', generalSettings);
  app.use('/loginattempts', loginattempts);
  app.use('/reportConfiguration', reportConfiguration);
  app.use('/reportPermissionConfiguration', reportPermissionConfiguration);
  app.use('/logiGetSecureKey', logiGetSecureKey);
  app.use('/apiAccessConfig', apiAccessConfig);
  app.use('/controlChartParameters', controlChartParameters);
  app.use('/windowReport', windowReport);
  app.use('/groupsettings', groupsettings);
  app.use('/fetchKeyAttributeFilterSchemes', fetchKeyAttributeFilterSchemes);
  app.use('/exportfile', exportfile);
  app.use('/generatedocuments', generateDocuments);
  app.use('/appInitiateentry', appInitiateentry);
  app.use('/fetchEntityRecords', fetchEntityRecords);
  app.use('/appAccessConfig', appAccessConfig);
  app.use('/appEntityConfigEntity', appEntityConfigEntity);
  app.use('/defaultRoute', defaultRoute);
  app.use('/assignment', assignment);
  app.use('/mappedLinkToEntity', mappedLinkToEntity);

  //bi
  app.use('/drilldown', drilldown);
  app.use('/controlchart', controlchart);
  app.use('/customReportColumnDetail', customReportColumnDetail);
  app.use('/queueFiles', queueFiles);
  app.use('/bireports', bireports);
  app.use('/reportTemplates', reportTemplates);
  app.use('/reportSettings', reportSettings);
  app.use('/customReport', customReport)
  app.use('/ReportSchemaColumns', ReportSchemaColumns);
  //bi

  //Qulification Performance
  app.use('/calendarWindows', calendarWindows);
  app.use('/windows', windows);
  app.use('/windowQueueFiles', windowQueueFiles)
  require('./qulificationperformance')(app);

  //plugin
  app.use('/plugin', plugin);
  app.use('/autoSync', autoSync);
  app.use('/pluginTempRecords', pluginTempRecords);
  app.use('/pluginConfig', pluginConfig);
  app.use('/pluginlist', pluginlist);
  app.use('/autoSyncHistory', autoSyncHistory);
  // app.use('/s3', s3);

  require('./pluginCommon')(app);
  require('./pluginSyncData')(app);
  require('./appSync')(app);
  require('./s3')(app);
  // require('./autoSyncCron/autoSyncCron')(app);

   //Initiate the cron Application.
   require('./autoSync/cron').init();

  // app.use('/autoSyncCron', autoSyncCron);

  // require('./rabbitmq/receive')(app);
  //plugin

  //chandni - code start - Added below code for QC3-1904
  app.use('/versions', versions);


  //jyoil.r && akashdeep.s
  //Start: QC3-5223 - Backlog - Sets - The existing sets of the user shall update dynamically.
  //Changed By: Jyotil
  //Description: Added seeding method.
  require('./seeding')(app);
  require('./importSchema')(app);
  //End: QC3-5223 - Backlog - Sets - The existing sets of the user shall update dynamically.
  //akashdeep.s - Store Procedure in MongoDB

  require('./mongoStoreProcedure');

  //patch code
  var apiRoutes = express.Router();
  apiRoutes.get('/resetpassgeneralsetting', function (req, res, next) {
    logger.log('API: /resetpassgeneralsetting - Start', 'info', currentFileName);
    db.generalSettings.find({}).toArray(function (err, data) {
      if (err) {
        logger.log('Collection: generalSettings: if (err) - Success : ' + err, 'error', currentFileName);
        return res.status(422).json({ 'message': err.message });
      }
      logger.log('Collection: generalSettings - Success', 'info', currentFileName);
      res.status(200).json(data[0]);
    });
  });
  //Chandni : Code start for Swagger

  //api for validate perfeqta token
  apiRoutes.get('/validateToken',tokenValidator.tokenValidator);

  //login api for vision centric embedded mode
  apiRoutes.post('/vclogin',vclogin.login)

  //role api for vc4
  apiRoutes.post('/vcroles',vc4Role.getRole)

  // PQT-2075 -- START
  //Saurabh : Update Token Time

  apiRoutes.post('/updateToken/:path', function (req, res, next) {
    logger.log('API: /updateToken - Start', 'info', currentFileName);
    var path = (req.params.path || '').toLowerCase();

    var isCurrent = req['body'].isCurrent || false;
    var  expirationTime = tokenUtility.updateToken(isCurrent);
    var findId;
    if(req['body'].userId)
    {
    findId=  {'userId':db.ObjectID(req['body'].userId)}
  } else if(req['body'].token){
      findId = {'token': req['body'].token	}
    }
    if(findId){
  	db.allAuthToken.findAndModify(findId,{}, {
        $set:{
          ['expirationTime.'+path]:expirationTime,
          globleExpirationTime:expirationTime
        }
  		},function (err,data) {
        if(data){
          console.log(findId)
          console.log('updateTokenTime========')
          console.log(data.value)
          console.log('updateTokenTime========')

          res.json({
            data:data.value
        });
      }else{
        res.json({
          data :'something wrong'
      });
      }
      })
    }else{
      res.json({
        data :'something wrong'
    });
    }
  
  });

  apiRoutes.post('/userisalive/:path', function (req, res, next) {
    var path = (req.params.path || '').toLowerCase();
    var findId;
    if(req['body'].userId)
    {
    findId=  {'userId':db.ObjectID(req['body'].userId)}
    } else if(req['body'].token){
      findId = {'token': req['body'].token	}
    }
    if(findId){
    	db.allAuthToken.find(findId).toArray(function (err, data) {
  		data = data[0];
  		if (data) {
  			var isAlive = tokenUtility.checkUserIsAlive(data.expirationTime,path);
  			res.json({
  				"isalive": isAlive,
          "expirationTime": data.expirationTime
  			});
  		}else{
        res.json({
          data :'something wrong'
      });
      }
  	})
}else{
  res.json({
    data :'something wrong'
});
}
  });
// PQT-2075 -- STOP

  /**
  * @swagger
  * definition:
  *   RevisionModel:
  *     properties:
  *       major:
  *         type: string
  *       minor:
  *         type: string
  *       build:
  *         type: string
  *       revision:
  *         type: string
  */


  /**
  * @swagger
  * definition:
  *   VersionModel:
  *     properties:
  *       _id:
  *         type: string
  *       productVersion:
  *         type: string
  *       web:
  *         $ref: '#/definitions/RevisionModel'
  *       api:
  *         $ref: '#/definitions/RevisionModel'
  *       db:
  *         $ref: '#/definitions/RevisionModel'
  *       report:
  *         $ref: '#/definitions/RevisionModel'
  *       reportApi:
  *         $ref: '#/definitions/RevisionModel'
  *       createdBy:
  *         type: string
  *       modifiedBy:
  *         type: string
  *       createdByName:
  *         type: string
  *       modifiedByName:
  *         type: string
  *       createdDate:
  *         type: string
  *       modifiedDate:
  *         type: string
  */

  /**
  * @swagger
  * /version:
  *   get:
  *     tags: [Version]
  *     description: Returns the latest version of PERFEQTA
  *     produces:
  *       - application/json
  *     responses:
  *       200:
  *         description: An object of version
  *         schema:
  *           $ref: '#/definitions/VersionModel'

  */
  apiRoutes.get('/version', function (req, res, next) {
    logger.log('API: /version - Success', 'info', currentFileName);
    db.versions.find({}).sort({ $natural: -1 }).toArray(function (err, data) {

      if (err) {
        logger.log('Collection: versions: - Error - 422 : ' + err, 'error', currentFileName);
        res.status(422).json({ 'message': err.message });
      }
      else if (data[0]) {
        logger.log('Collection: versions: - Success', 'debug', currentFileName);
        res.status(200).json(data[0]);
      } else {
        logger.log('Collection: versions: - Error - 500', 'error', currentFileName);
        res.status(500).json("Oops! something went wrong. Kindly, contact Perfeqta support team.");
      }

    });
  });

  apiRoutes.post('/checkMaterInCalender', function (req, res, next) {
    if(req.body.masterId == 'new'){
      res.status(200).json({flag:true});
    }else{
      db.calendar.findOne({'master._id': db.ObjectID(req.body.masterId)},{title:1},function(err,data){
        if(err){
          res.status(200).json({flag:true});
        }else if(data){
          res.status(200).json({flag:false});
        }else{
          res.status(200).json({flag:true});
        }
      });
    }
  });

  //by surjeet.b@productivet.com.. used when two user simutaneously hit Sign button..
  apiRoutes.get('/checkEntryReviewStatus/schemaId/:schemaId/entryId/:entryId/reviewIndex/:reviewIndex', function (req, res, next) {
    if (req.params && Object.keys(req.params).length) {
      //var reviewIndex = parseInt(req.params.reviewIndex); //need to remove
      db[req.params.schemaId].find({_id: db.ObjectID(req.params.entryId)}, {workflowreview: 1, entries: 1}).toArray(function (err, response) {
        if (err) {
          return res.status(422).json({'err': err});
        }
        if (response && response[0]) {
          if ((response[0].workflowreview && response[0].workflowreview[req.params.reviewIndex] && response[0].workflowreview[req.params.reviewIndex].reviewstatus != 1) || (response[0].entries && response[0].entries.status && response[0].entries.status.status == 3)) {
            return res.status(422).json({data: 'Invalid Sign'});
          }
        }
        res.status(200).json({ data: true });
      });
    }else {
      res.status(422).json({ 'err': 'schemaId, entryId and reviewIndex are required.' });
    }
  });

  /*
    * Api for batch lock when any user login
  */
  // -- start --
  //var batchLockData = {}; // need to remove
  var expireTime = config.batchHeartBeat || (20 * 60 * 1000);
  apiRoutes.post('/batchLockData', function (req, res) {
    var appId = req.body.appId,
    batchId = req.body.batchId,
    userId = req.body.userId,
    fullName = req.body.fullName,
    removeAll = req.body.removeAll,
    removeOne = req.body.removeOne,
    nonBatch = req.body.nonBatch,
    checkBatchLockStatus = req.body.checkBatchLockStatus;
    if(config.cacheOfbatchlockData == 'redis'){
      if(!redisErrFlag){
        batchLockDataFn(appId,batchId,userId,fullName,removeAll,removeOne,nonBatch,checkBatchLockStatus,function(data){
          res.status(200).json(data);
        });
      }else{
        redisConnectionTries = 0;
        tryRedisConnect(function(){
          res.status(503).json({flag : false,msg : 'Redis server unavilable'});
        },
        function(){
          batchLockDataFn(appId,batchId,userId,fullName,removeAll,removeOne,nonBatch,checkBatchLockStatus,function(data){
            res.status(200).json(data);
          });
        })
      }
    }else{
      batchLockDataFnMongo(appId,batchId,userId,fullName,removeAll,removeOne,nonBatch,checkBatchLockStatus,function(data){
        res.status(200).json(data);
      });
    }
  });

  function batchLockDataFn(appId,batchId,userId,fullName,removeAll,removeOne,nonBatch,checkBatchLockStatus,cb){

    var redisKey = appId+'-'+batchId;

    if(removeAll){ // remove if user logout or close browser /* delete all user batch query  */
      client.keys('*', function (err, keys) {
        for(var i = 0, len = keys.length; i < len; i++) {
          client.get(keys[i], function(err, reply) {
            if(reply){
              reply = JSON.parse(reply);
              if(reply.userId == userId){
                client.del(keys[i]);
              }
            }
          });
        }
      });
      cb({flag : false});
    }else if(removeOne){ // Remove perticuler batch /* delete perticular query */
      client.del(redisKey);
      cb({flag : false});
    }else if(nonBatch){ // If not from batch always return true
      cb({flag : true});
    }else if(checkBatchLockStatus){ // only check if batch is already running or not for sign button
      client.get(redisKey, function (err, reply) {
        if (reply) {
          db['users'].findOne({ _id: db.ObjectID(userId) }, function (err, userData) {
            if (userData && userData.isConCurrentUserActive) {
              db["batchLockCache"].deleteMany({ userId: reply.userId });
            }
            reply = JSON.parse(reply);
            var obj = {};
            if (reply.userId == userId) { // for same user /* update query */
              obj.flag = true;
            } else { // for another user
              obj.flag = false;
              obj.fullName = reply.fullName;
              obj.userId = reply.userId;
            }
            cb(obj);
          });
        } else {
          cb({ flag: true });
        }
      });
    }else{
      client.get(redisKey, function(err, reply) {
        if(reply){ // check if batch start
          reply = JSON.parse(reply);
          var obj;
          if(reply.time + expireTime >= Date.now()){ // check already in process
            if(reply.userId == userId){ // for same user /* update query */
              reply.time = Date.now();
              client.set(redisKey, JSON.stringify(reply));
              cb({flag : true});
            }else{ // for another user
              obj = {
                flag : false,
                fullName : reply.fullName,
                userId : reply.userId
              }
              cb(obj);
            }
          }else{ // Batch start for new user if time is over /* insert query */
            client.set(redisKey, JSON.stringify(reply));
            obj = {
              userId : userId,
              fullName : fullName,
              time : Date.now()
            }
            client.set(redisKey, JSON.stringify(obj));
            cb({flag : true});
          }
        }else{ // fresh batch
          obj = {
            userId : userId,
            fullName : fullName,
            time : Date.now()
          }
          client.set(redisKey, JSON.stringify(obj));
          cb({flag : true});
        }
      });
    }
  }

  function batchLockDataFnMongo(appId,batchId,userId,fullName,removeAll,removeOne,nonBatch,checkBatchLockStatus,cb){

    var cacheKey = appId+'-'+batchId+'11';

    if(removeAll){ // remove if user logout or close browser /* delete all user batch query  */
      db["batchLockCache"].deleteMany({ userId: userId});
      cb({flag : false});
    }else if(removeOne){ // Remove perticuler batch /* delete perticular query */
      db["batchLockCache"].deleteMany({ cacheKey: cacheKey});
      cb({flag : false});
    }else if(nonBatch){ // If not from batch always return true
      cb({flag : true});
    } else if (checkBatchLockStatus) { // only check if batch is already running or not for sign button
      db['batchLockCache'].findOne({ cacheKey: cacheKey }, function (err, reply) {
        if (reply) {
          db['users'].findOne({ _id: db.ObjectID(userId) }, function (err, userData) {
            if (userData && !userData.isConCurrentUserActive) {
              db["batchLockCache"].deleteMany({ userId: reply.userId });
            }
            var obj = {};
            if (reply.userId == userId) { // for same user /* update query */
              obj.flag = true;
            } else if (reply.time + expireTime >= Date.now()) { //MDA HOT Fix.. check availability function is not working (surjeet.b@producitvet.com)
              obj.flag = false;
              obj.fullName = reply.fullName;
              obj.userId = reply.userId;
            } else { // for another user
              obj.flag = true;
            }
            cb(obj);
          });
        } else {
          cb({ flag: true });
        }
      });
    } else {
      db['batchLockCache'].findOne({ cacheKey: cacheKey}, function (err, reply) {
        var obj;
        if(reply){ // check if batch start
          if(reply.time + expireTime >= Date.now()){ // check already in process
            if(reply.userId == userId){ // for same user /* update query */
              reply.time = Date.now();
              db['batchLockCache'].update({ cacheKey: cacheKey},{$set: reply}, function (updateErr, updateData) {
                cb({flag : true});
              });
            }else{
              obj = {
                flag : false,
                fullName : reply.fullName,
                userId : reply.userId
              }
              cb(obj);
            }
          }else{
            obj = {
              cacheKey : cacheKey,
              userId : userId,
              fullName : fullName,
              time : Date.now()
            }
            db['batchLockCache'].update({ cacheKey: cacheKey},{$set: obj}, function (updateErr, updateData) {
              cb({flag : true});
            });
          }
        }else { // fresh batch
          obj = {
            cacheKey : cacheKey,
            userId : userId,
            fullName : fullName,
            time : Date.now()
          }
          db['batchLockCache'].insertOne(obj, function (insertOneErr, insertOneData) {
            cb({flag : true});
          });
        }
      });
    }
  }

  //Chandni : Code start for Swagger
  /**
  * @swagger
  * /postModules:
  *   post:
  *     tags: [Module]
  *     description: Creates a new module
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: module
  *         description: module object
  *         in: body
  *         required: true
  *         schema:
  *           $ref: '#/definitions/ModuleModel'
  *     responses:
  *       201:
  *         description: Successfully created
  */

  apiRoutes.post('/postModules', function (req, res, next) {
    logger.log('API: /postModules - Success', 'info', currentFileName);
    db.modules.insertAsync({
      title: req.body.title,
      isActive: req.body.isActive,
      _id: req.body._id
    }).then(function loginattemptsInsertAsyncThenCallback(log) {
      logger.log('Collection: modules:(CB)loginattemptsInsertAsyncThenCallback - Success', 'info', currentFileName);
      res.status(200).json([]);
    }).error(function loginattemptsInsertAsyncErrorCallback(err) {
      logger.log('Collection: modules: - Error : ' + err, 'error', currentFileName);
      res.status(422).json({ 'message': 'Failed to create modules.' });
    });
  });

  /**
  * @swagger
  * /version/all:
  *   get:
  *     tags: [Version]
  *     description: Returns all versions of PERFEQTA
  *     produces:
  *       - application/json
  *     responses:
  *       200:
  *         description: An array of version
  *         schema:
  *           $ref: '#/definitions/VersionModel'

  */
  apiRoutes.get('/version/all', function (req, res, next) {
    logger.log('API: /version/all: - Success', 'info', currentFileName);
    db.versions.find({}).toArray(function (err, data) {

      if (err) {
        logger.log('Collection: versions: - Error- 422 : ' + err, 'error', currentFileName);
        res.status(422).json({ 'message': err.message });
      }
      else if (data) {
        logger.log('Collection: versions: - Success', 'info', currentFileName);
        res.status(200).json(data);
      } else {
        logger.log('Collection: versions: - Error - 500', 'error', currentFileName);
        res.status(500).json("Oops! something went wrong. Kindly, contact Perfeqta support team.");
      }

    });
  });

  //Start-QC3-6781-Vaibhav
  apiRoutes.get('/getBatchEntryId', function(req, res, next){
    logger.log('API: /getBatchEntryId: - Success', 'info', currentFileName);
    var date=req.query.datestring;
    db.batchentrysettings.find().toArray(function(err,result){
      logger.log('Collection: batchentrysettings: - Find Success', 'info', currentFileName);
      var updatedBatchEntrySetting=result;
      var batchFormat=result[0].batchEntryFormat;
      var updatedBatchFormat=getBatchNameByFormat(batchFormat,date);
      var batchEntryId=result[0]._id;
      updatedBatchEntrySetting[0].batchEntryFormat=updatedBatchFormat;
      db.batchentrysettings.update({_id:batchEntryId}, updatedBatchEntrySetting[0],function(err, ids) {
        logger.log('Collection: batchentrysettings: - Update Success', 'info', currentFileName);
        res.json(result);
      });
    });
  });

 // QC3-7754 - Prachi - Start
//  apiRoutes.get('/getSelectedLinkedEntityRecord/:linkedEntityId/:linkEntityKeyAttributeId/:entityValue',function(req, res, next) {

//   try {
//      logger.log('API: /getSelectedLinkedEntityRecord/:linkedEntityId/:entityValue - Success', 'info', currentFileName);
//       var key = ""+req.params.linkEntityKeyAttributeId;
//        var value = ""+req.params.entityValue;


//       console.log(req.query);
//       return;

//       var searchObject = {}
//       console.log("8888");
//       if(value.length==10){
//         value = value.split("-").join("/");
//         searchObject = "/.*"+value+"*/";


//       }
//       else{
//         console.log("1010");
//         var valNumber = Number(value);
//         searchObject = { "$in": [valNumber , value ] }
//       }

//       // { "$in": [valNumber , value ] }
//        db[req.params.linkedEntityId].find({['entityRecord.' + key] : searchObject},{entityRecord:1}).toArray(function(err, entityRecordData) {
//          if (err) {
//            console.log('err ' + JSON.stringify(err));
//          }
//          console.log(entityRecordData)
//          res.status(200).json(entityRecordData);
//        });
//    }catch (e) {
//      logger.log("catch: API: /getSelectedLinkedEntityRecord/:linkedEntityId/:entityValue - failed" , 'error', currentFileName);
//      res.status(500).json("Invalid Data for Linked to Entity" + e);
//    }
//  });



/**
 * By MIral
 * Changed this api full api to solve QC3-8086
 */
apiRoutes.get('/getSelectedLinkedEntityRecord/',function(req, res, next) {

 try {

     var key = ""+req.query.linkEntityKeyAttributeId;
      var value = ""+req.query.entityValue;

      var searchObject = {}
      if(req.query.entityKeyAttributeDateFormat){

        //QC3-8309 by miral manage client offset if server is in UTC
        searchObject = moment(value,req.query.entityKeyAttributeDateFormat.toUpperCase())
        if(req.headers && req.headers['client-tz'] && (new Date()).getTimezoneOffset() == 0){

          searchObject = (new Date(searchObject.toDate().getTime()
            + (moment.tz.zone(req.headers['client-tz']).parse(moment(searchObject.toDate()))
              * 60 * 1000))).toISOString();

        }
        else{
          searchObject = searchObject.toISOString();
        }
      }
      else{
        searchObject = { "$in": [Number(value) , value ] }
      }

      db[req.query.linkedEntityId].find({['entityRecord.' + key] : searchObject},{entityRecord:1,isDeleted:1}).toArray(function(err, entityRecordData) {
        /* if (err) { // need to remove

        } */
        res.status(200).json(entityRecordData);
      });
  }catch (e) {
    logger.log("catch: API: /getSelectedLinkedEntityRecord/:linkedEntityId/:entityValue - failed : " + e , 'error', currentFileName);
    res.status(500).json("Invalid Data for Linked to Entity" + e);
  }
});

//QC3-8120 - Prachi - Start
apiRoutes.post('/addUpdateEntityRecord', function (req, res, next) {
  var entityCount = req.body.length;
  executeAddUpdateEntityRecordOnSaveAndAccept(req.body, req).then(function(data) {
    var cnt=0;
    _.forEach(data,function(result){
      if(result['success']){
        cnt++;
      }
    });
    if(entityCount==cnt){
      res.status(200).json(data);
    }else{
      res.status(500).json(data);
      return;
    }
  });
});

function executeAddUpdateEntityRecordOnSaveAndAccept(updateEntityData, req) {
  logger.log("function: executeAddUpdateEntityRecordOnSaveAndAccept - start", 'info', currentFileName);
  var deferred = q.defer();
  if (updateEntityData) {
    getEntityRecordForUpdateEntity(updateEntityData,[], req).then(function(data) {
      deferred.resolve(data);
    });
  }
  return deferred.promise;
}

/// start - PQT -1109 - purvi
apiRoutes.post('/schemaActiveInactive', function (req, res, next) {
  try {
    var entityValue = req.body;
    if (entityValue) {

      db.schema.findAndModify({ _id: db.ObjectID(entityValue._id) }, {}, { $set: { 'isActive': !(entityValue.isActive) } },
        function (err, schemaResponse) {
          if (err) {
            logger.log("error ::" + err);
          }
          if (schemaResponse) {
            var recordId = "";
            if (schemaResponse && schemaResponse.value && schemaResponse.value.versions) {
              recordId = _.last(schemaResponse.value.versions).record;
            }
            db.schemaAuditLogs.findAndModify({ _id: db.ObjectID(recordId), version: Math.floor(entityValue.version) }, {}, { $set: { 'isActive': !(entityValue.isActive) } },

              function (err, schemaAuditLogsResponse) {
                if (err) {
                  logger.log("error ::" + err);
                }
                if (schemaAuditLogsResponse) {
                  //update Schemalogs
                  var set = {
                    "path": "isActive",
                    "old": entityValue.isActive,
                    "new": !(entityValue.isActive),
                    "identity": db.ObjectID(recordId),
                    "modifiedDate": schemaResponse.value.modifiedDate,
                    "modifiedBy": db.ObjectID(schemaResponse.value.modifiedBy),
                    "version": entityValue.version,
                    "ip": "127.0.0.1",
                    "title": "Is Active"
                  }
                  db['schemaLogs'].insertOne(set, function (e, data) {
                    if (e) {
                      logger.log('Failed to capture log for ' + entityValue._id + ' change log is as follows');
                      logger.log(set);
                    }
                    res.status(200).json(schemaResponse);
                  });

                }
              })
          }
        })
    }
    else {
      res.status(500).json("Invalid Data for schemaActiveInactive" + e);
    }

  } catch (e) {
    logger.log("catch: API: /schemaActiveInactive :: failed " + e, 'error', currentFileName);
    res.status(500).json("Invalid Data for schemaActiveInactive" + e);
  }
});
/// end - PQT -1109 -purvi

function getEntityRecordForUpdateEntity(updateEntityValue, updatedData, req) {
  logger.log("function: getEntityRecordForUpdateEntity - start", 'info', currentFileName);
  var deferred = q.defer();
  if(updateEntityValue.length > 0){
    var entity = updateEntityValue.shift();
    var key = ""+entity.entityKeyAttributeSeqId;
    var select = {};
    if(Number(entity.entityRecordForUpdate[key])){
      select = {$or : [{['entityRecord.' + key] : entity.entityRecordForUpdate[key]},{['entityRecord.' + key] : parseInt(entity.entityRecordForUpdate[key])}], isDeleted:false};
    } else {
      select = {['entityRecord.' + key] : entity.entityRecordForUpdate[key], isDeleted:false};
    }
    db[entity['entityId']].find(select,{entityRecord:1, isDeleted:1}).toArray(function(err, entityRecordData) {
      if (err) {
        logger.log('Error find at the time of add/update entity record :' + err,'error');
      }
      var returnValue = {};
      returnValue['entityRecord'] = returnValue['entityRecord'] || {};
      var postModel;
      var options;
      var getUrl;
      if(entityRecordData.length == 0 && entity.typeOfEntity == 'addEntityRecord'){
        console.log('-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=- Add Entity Record -=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-');
        entity.entityRecordForUpdate['isActive'] = entity.entityRecordForUpdate['isActive'] == false ? false : true;
        postModel = {
          isDeleted : false,
          isFromApp : true,
          entityRecord : entity.entityRecordForUpdate
        };
        options = { headers : { "x-authorization" : req.headers['x-authorization'] } };
        getUrl = req.headers.referer ? req.headers.referer : (config.apiEnvUrl + '/');
        console.log('URL :: ' + getUrl + 'api/entityRecords/' + entity['entityId']);
        //restler.postJson('http://' + req.headers.host + '/entityRecords/' + entity['entityId'], postModel, options)
        restler.postJson(config.apiEnvUrl + '/entityRecords/' + entity['entityId'], postModel, options)
        .on('complete', function (cmplt) {
          db[entity['entityId']].find(select,{entityRecord:1, isDeleted:1}).toArray(function(err, entR) {
            if (err) {
              logger.log(err);
            }
            console.log("Entity Record Posted Successfully.");
            if (entR) {
              console.log(entR);
              if (returnValue['entityRecord']) {
                returnValue['entityRecord'][entity['entitySeqId']] = entR[0];
              }
            }
            returnValue['success'] = true;
            updatedData.push(returnValue);
            getEntityRecordForUpdateEntity(updateEntityValue, updatedData, req).then(function(err,d){
              deferred.resolve(updatedData);
            });
          });
        })
        .on('timeout', function () {
          logger.log('timeout on addEntityRecord','error');
        })
        .on('error', function (err) {
          logger.log('error on addEntityRecord' + err,'error');
        });
      } else {
        if(entity.typeOfEntity == 'addEntityRecord'){
          try{
            console.log('-=-=-=-=-=-=-=-=-=-=-The key attribute is already used in another entity record-=-=-=-=-=-=-=-=-=-=-');
            returnValue['errMessage']='The key attribute is already used in another entity record.';
            returnValue['success'] = false;
            returnValue['entityId']=entity['entityId'];
            returnValue['entityTitle']=entity['entityTitle'];
            returnValue['entitySeqId']=entity['entitySeqId'];
            returnValue['entityKeyAttributeSeqId']=entity['entityKeyAttributeSeqId'];
            updatedData.push(returnValue);
            getEntityRecordForUpdateEntity(updateEntityValue, updatedData, req).then(function(err,d){
              deferred.resolve(updatedData);
            });
          }
          catch(e){
            logger.log("addEntityRecord  :: " + e, 'error');
            deferred.resolve(updatedData);
          }
        } else if(entity.typeOfEntity == 'updateEntityRecord') {
          console.log('-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=- Update Entity Record -=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-');
          entity.entityRecordForUpdate['isActive'] = entity.entityRecordForUpdate['isActive'] == false ? false : true;
          postModel = {
            isDeleted : false,
            isFromApp : true,
            entityRecord : entity.entityRecordForUpdate
          };
          options = { headers : { "x-authorization" : req.headers['x-authorization'] } };
          getUrl = req.headers.referer ? req.headers.referer : (config.apiEnvUrl + '/');

          if (!_.isUndefined(entityRecordData) && !_.isUndefined(entityRecordData[0]) && _.size(entityRecordData[0]) > 0) {
            console.log('URL :: ' + getUrl + 'api/entityRecords/' + entity['entityId']+'/'+entityRecordData[0]['_id']);
            //restler.patchJson('http://' + req.headers.host + '/entityRecords/' + entity['entityId']+'/'+entityRecordData[0]['_id'], postModel, options)
            restler.patchJson(config.apiEnvUrl + '/entityRecords/' + entity['entityId']+'/'+entityRecordData[0]['_id'], postModel, options)
            .on('complete', function (cmplt) {
              //QC3-9203 Prachi
              if (returnValue['entityRecord']) {
                postModel['_id'] = entityRecordData[0]['_id'];
                returnValue['entityRecord'][entity['entitySeqId']] = postModel;
              }
              console.log("Entity Record Patch Successfully.");
              returnValue['success'] = true;
              updatedData.push(returnValue);
              getEntityRecordForUpdateEntity(updateEntityValue, updatedData, req).then(function(err,d){
                deferred.resolve(updatedData);
              });
            })
            .on('timeout', function () {
              logger.log('timeout on addEntityRecord','error');
            })
            .on('error', function (err) {
              logger.log('error on addEntityRecord'+err,'error');
            });
          }else {
            console.log('-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=- Add Entity Record -=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-');
            console.log('URL :: ' + getUrl + 'api/entityRecords/' + entity['entityId']);
            //restler.postJson('http://' + req.headers.host + '/entityRecords/' + entity['entityId'], postModel, options)
            restler.postJson(config.apiEnvUrl + '/entityRecords/' + entity['entityId'], postModel, options)
            .on('complete', function (cmplt) {
              db[entity['entityId']].find(select,{entityRecord:1, isDeleted:1}).toArray(function(err, entR) {
                if (err) {
                  logger.log(err);
                }
                console.log("Entity Record Posted Successfully.");
                if (entR) {
                  console.log(entR);
                  if (returnValue['entityRecord']) {
                    returnValue['entityRecord'][entity['entitySeqId']] = entR[0];
                  }
                }
                returnValue['success'] = true;
                updatedData.push(returnValue);
                getEntityRecordForUpdateEntity(updateEntityValue, updatedData, req).then(function(err,d){
                  deferred.resolve(updatedData);
                });
              });
            })
            .on('timeout', function () {
              logger.log('timeout on addEntityRecord','error');
            })
            .on('error', function (err) {
              logger.log('error on addEntityRecord'+err,'error');
            });
          }
        }else{
          returnValue['success'] = false;
          updatedData.push(returnValue);
          getEntityRecordForUpdateEntity(updateEntityValue, updatedData, req).then(function(err, d){
            deferred.resolve(updatedData);
          });
        }
      }
    });
  } else {
    deferred.resolve(updatedData);
  }
  return deferred.promise;
}
//QC3-8120 - Prachi - End

// QC3-7755 - Prachi - Start
 apiRoutes.get('/getLinkedEntityRecords/:entityId',function(req, res, next) {
   try {
     logger.log('API: /getLinkedEntityRecords/:entityId - Success', 'info', currentFileName);
     var getKey = [];
     db['entities'].findOne({_id: db.ObjectID(req.params.entityId)}, function(err, entity) {
       if (err) {
         logger.log(err,'error');
       }
       if (!_.isUndefined(entity) && !_.isUndefined(entity.questions) && entity.questions.length > 0) {
         _.forEach(entity.questions, function(question) {
           if (!_.isUndefined(question) && !_.isUndefined(question.typeSelector) && question.typeSelector == '2' && question.linkToEntity) {
             if (_.size(question.linkToEntity) > 0) {
               getKey.push({
                 entityId : question.linkToEntity.entityId,
                 entityKeyAttributeId : question.linkToEntity.entityKeyAttributeId,
                 entityKeyAttributeDateFormat : question.linkToEntity.entityKeyAttributeDateFormat
               });
             }
           }
         });
       }
       if (getKey && getKey.length > 0) {
         getEntityRecord(getKey, {},req).then(function(data) {
           logger.log('API: Success Response q Service Resolved - Success', 'info', currentFileName);
           res.status(200).json(data);
         });
       }else {
         res.status(200).json({data: 'Linked to entity is not found.'});
       }
     });
   }catch (e) {
     logger.log("catch: API: /getLinkedEntityRecords/:entityId - failed : " + e , 'error', currentFileName);
     res.status(500).json("Invalid Data for Linked to Entity" + e);
   }
 });

// PQT-394 :: START by Yamuna
 apiRoutes.get('/getYourAssignedRecords/:userId', function(req, res, next) {
   logger.log('API: /getYourAssignedRecords:userId - Success', 'info', currentFileName);
   var userId = req.params.userId;
   db['assignment'].aggregate(
     [{
         "$project": {
           last: {
             $arrayElemAt: ["$assignment", -1]
           },
           schemaId: 1,
           'entryObj.entryId': 1
         }
       },
       {
         "$match": {
           "last.assignee.id": userId
         }
       }
     ], function(err, response) {
       res.status(200).json({data: response});
     });
 });
 // PQT-394 :: END by Yamuna

 function getEntityRecord(entityLinkData, updatedLinkedData,req) {
   logger.log('Function: getEntityRecord (used in API : /getLinkedEntityRecords/:entityId) : - Success', 'info', currentFileName);
   var deferred = q.defer();
   if (entityLinkData.length > 0) {

     var entity = entityLinkData.shift();
     var key = ""+entity.entityKeyAttributeId;
     collectionCheck.collectionCheck(entity['entityId']);
     db[entity['entityId']].distinct('entityRecord.' + key, {"entityRecord.isActive" : true}, function(err, entRec) {
       updatedLinkedData[entity['entityId']] = (_.isUndefined(entity.entityKeyAttributeDateFormat) || _.isNull(entity.entityKeyAttributeDateFormat) || entity.entityKeyAttributeDateFormat === '' || _.isNaN(entity.entityKeyAttributeDateFormat)) ? entRec : getChangeFormat(entRec, entity.entityKeyAttributeDateFormat,req);
       getEntityRecord(entityLinkData, updatedLinkedData,req).then(function(data){
         deferred.resolve(data);
       });
     });
   }else {
     deferred.resolve(updatedLinkedData);
   }
   return deferred.promise;
 }

 function getChangeFormat(entityRecord, dateFormat,req) {

  var clientDate = new Date();
  var machineOffset = (new Date()).getTimezoneOffset();

  //QC3-9116 by miral manage client offset if server is in UTC
  var clientTimezone = (req && req.headers && req.headers['client-tz'])?
                              req.headers['client-tz']:false;
   _.forIn(entityRecord, function(record,index) {

    clientDate = new Date(record);

    if(clientTimezone && machineOffset  == 0 ){
      clientDate = new Date(clientDate.getTime()
                    - (moment.tz.zone(clientTimezone).parse(moment(clientDate))
                        * 60 * 1000));
    }

    entityRecord[index] = moment(clientDate).format(dateFormat.toUpperCase() || ('MM/DD/YYYY'));
    //  entityRecord[index] = moment(new Date(record)).format(dateFormat.toUpperCase() || ('MM/DD/YYYY'));
   });
   return entityRecord;
 }
 // QC3-7755 - Prachi - End

  function getBatchNameByFormat(batchFormatArr,date){
   // logger.log('Function: getBatchNameByFormat: - Success', 'info', currentFileName);
                    _.forEach(batchFormatArr,function(batchFormat){
                      if(batchFormat.isSelected){
                            if(!_.isUndefined(batchFormat.entryType)){
                              if((batchFormat.entryType==0 || batchFormat.entryType==1) && batchFormat.batchEntryFormatValue!=date){
                                batchFormat.batchEntryFormatValue=date;
                                batchFormat.batchCurrentValue=1;
                              }else{
                                batchFormat.batchCurrentValue=batchFormat.batchCurrentValue+1;
                              }
                          }
                      }

                    });

           return batchFormatArr;
   }
   //End-QC3-6781-Vaibhav

   //by surjeet.b@productivet.com

   /**
    * Edited by miral
    * Purpose of this API :
    *   Get list of entity connected to perticular entity
    *   For ex : ent1 is entity whose connected entity info you want
    *            then suppose ent1ID is s# of key attribute of ent1
    *            ent1ID is available in ent2a attr of ent2 and ent3a attr of ent3
    *            then this api will return ent2 and ent3
    *   extra thing that miral has done is this api will return respective count of record of entity with basic info
    *   that can be seen in ACCORDIAN of UI
    *
    * @param :linkedEntityId - entity id whose connected entities we want
    * @param :linkedEntityRecordId - respective record id of entity whose connected record count we need // this extra attribute added by miral to get count
    *
    * @return : list of connected entity list with count respective to provided entity record id
    */
    apiRoutes.get('/mappedLinkToEntityData/:parentEntId/:parentEntRecordId',function(req, res, next) {
  //  apiRoutes.get('/mappedLinkToEntityData/:linkedEntityId/:linkedEntityRecordId',function(req, res, next) {
     logger.log('API: /mappedLinkToEntityData: - Success', 'info', currentFileName);


      /**
       * first we need entity info of #parentEntId
       */
     db.entities.find({"_id":db.ObjectID(req.params.parentEntId)})
      .toArray(function(err,sData){
        /**
         * In case of error or no data for #parentEntId there is no mean to proceed
         */
        if(err || !sData.length){
          return res.status(422).json({sData: 'No entity found'});
        }


        /**
         * get key attribute of found entity
         */
        var linkedEntityKeyAttr = _.find(sData[0].questions,{isKeyValue:true});


        /**
         * we have #parentEntRecordId which is only Id not full info which is needed in further procedure
         */
        db[req.params.parentEntId].find({"_id": db.ObjectID(req.params.parentEntRecordId)})
        .toArray(function(err,rData){

            /**
             * in case of no record no mean to proceed further
             */
            if(err || !rData.length){
              return res.status(422).json({rData: 'No entity record found.'});
            }

            /**
             * hear we are extracting VALUE of parent etity's key attribute in #linkedEntAttrKeyVal
             */
            var linkedEntAttrKeyVal = "";
            if(linkedEntityKeyAttr.type && linkedEntityKeyAttr.type.format && linkedEntityKeyAttr.type.format.title == 'Date'){
              linkedEntAttrKeyVal = rData[0]["entityRecord"][linkedEntityKeyAttr["s#"]];

              linkedEntAttrKeyVal = moment(new Date(rData[0]["entityRecord"][linkedEntityKeyAttr["s#"]]))
                    .format(linkedEntityKeyAttr.type.format.metadata.format.toUpperCase() || ('MM/DD/YYYY'));

              // linkedEntAttrKeyVal = moment(rData[0]["entityRecord"][linkedEntityKeyAttr["s#"]])
              //   .format(linkedEntityKeyAttr.type.format.matadata.format.toUpperCase())
              // .toISOString();
            }
            else{
              linkedEntAttrKeyVal = rData[0]["entityRecord"][linkedEntityKeyAttr["s#"]]
            }


            /**
             * db.mappedLinkToEntity would have information about linking of entities
             * #linkedEntityId it is id at which we hold linked entity id
             */
            db.mappedLinkToEntity.find({linkedEntityId: db.ObjectID(req.params.parentEntId)},{entityId: 1, entityAttributeSeqId: 1}).toArray(function (err, linkedEntities) {
              if (err) {
                logger.log(err, 'error');
                return res.status(422).json({data: 'No linked entities found.'});
              }
              if (linkedEntities && linkedEntities.length) {

                /**
                 * #mappedLinkToEntity will not provide full information about entity that we need at UI
                 * so we need to query for it in #getEntities()
                 *
                 * #linkedEntities - result from db.mappedLinkToEntity
                 * #linkedEntAttrKeyVal - value that we extracted from above procedure
                 */
                getEntities(linkedEntities, {},linkedEntAttrKeyVal).then(function (data) {
                 res.status(200).json(data);
                });
              }else {
                res.status(422).json({data: 'No linked entities found.'});
              }
            });

        })


      })





   });


   apiRoutes.get('/mappedLinkToEntityData/:linkedEntityId',function(req, res, next) {
     logger.log('API: /mappedLinkToEntityData: - Success', 'info', currentFileName);

     db.mappedLinkToEntity.find({linkedEntityId: db.ObjectID(req.params.linkedEntityId)},{entityId: 1, entityAttributeSeqId: 1}).toArray(function (err, linkedEntities) {
       if (err) {
         logger.log(err, 'error');
       }
       if (linkedEntities && linkedEntities.length) {
         getEntities(linkedEntities, {}).then(function (data) {
          res.status(200).json(data);
         });
       }else {
         res.status(422).json({data: 'No linked entities found.'});
       }
     });
   });

   function getEntities(linkedEntities, entitiesData,linkedEntAttrKeyVal) {
     var deffered = q.defer();
     var entityObj = {}
     var linkedEntitiesIds = _.map(linkedEntities, 'entityId');
     var entityAttributeSeqId = _.map(linkedEntities, 'entityAttributeSeqId') || [];
     db.entities.find({_id: {'$in': linkedEntitiesIds}}, {title: true, questions: true, highlightEntityRecord: true, module: true}).toArray(function (err, entities) {
       _.forEach(entities, function (entity, index) {


        var thisEntSequanceList = _.map(entity.questions,'s#');
        var attribFIn =  _.intersection(entityAttributeSeqId, thisEntSequanceList);

        var rOptions = {};
        rOptions['entityRecord.'+attribFIn] =  linkedEntAttrKeyVal
        rOptions['isDeleted'] = false;


        /**
         * hear we are extracting count
         */
        //  QC3-9012 By Yamuna
         if(!db[entity._id]){
           //logger.log("Table is not bind with mongoskin DB");
           var entities1 = [];
           entities1.push({
             collection: entity._id,
             audit: true,
             index: {
               title: ''
             }
           });
           //logger.log("Here for add collection in db object");
           dbConfig.configure.apply(this, entities1);
           //logger.log("Adding collection to DB is Completed");
         }
        db[entity._id.toString()].count(rOptions,function(err,count){
          if(err){
            count = 0;
          }
          entityObj[entity._id] = {
            _id: entity._id,
            title: entity.title,
            questions: entity.questions,
            modules: _.map(entity.module,function(mod){return mod._id}),
            entityAttributeSeqId: entityAttributeSeqId,
            isExpand: false,
            highlightEntityRecord: !_.isUndefined(entity.highlightEntityRecord) ? true : false,
            count:count
          };
          if (index == 0) {
            entityObj[entity._id].isExpand = true;
          }
          if(entities.length<=(Object.keys(entityObj).length)){
            deffered.resolve(entityObj);
          }

        })

       });

     });
     return deffered.promise;
   }
   //end by surjeet.b@productivet.com


  //by surjeet.b@productivet.com
//Start:-QC3-7571 New get API For Get Unique Data Of LinkedApp
   apiRoutes.get('/getallLinkedinformation', function(req, res, next){
     logger.log('API: /getallLinkedinformation: - Success', 'info', currentFileName);
      loggedInUser.detailsByToken(req, res, function (err, result) {
      if (err || !result) {
        logger.log('Function: detailsByToken: - Error - 422 : ' + err, 'error', currentFileName);
        res.status(422).json({'message': (err.message || 'some error has been occured')});
      } else {
        logger.log('Function: detailsByToken: - Success', 'info', currentFileName);
        //var token = req.query.token || req.headers["token"] || '';

        var pageSize = parseInt(req.query.pagesize);
        var page = parseInt(req.query.page);
        var linkedOfId=req.query.linkedOfId;
        var linkedToType = req.query.linkedToType;
        if (!pageSize) {
                pageSize = 10;
            }
            if (!page) {
                page = 1;
            }
            var skip = ((page - 1) * pageSize);

             var arr=[];
              var ids=[];
              db.linkedTbl.distinct("linkedToId",{ "linkedOfId" : linkedOfId,"linkedToType" : {$in:linkedToType}},function(err, dstArray) {
                db.linkedTbl.find({ "linkedOfId" : linkedOfId,"linkedToType" : {$in:linkedToType}},{linkedToId:1,linkedToType:1,linkedOfType:1,_id:0}).sort({"_id":-1}).toArray(function(err,cal){
                  arr=[];
                  ids=[];
                  cal.forEach(function(a){
                    var isExist=false;
                    if(ids.length>0){
                    ids.forEach(function(id){
                        if(id==a.linkedToId){
                            isExist=true;
                        }else{
                            ids.push(a.linkedToId);
                        }
                        });
                    }else{
                        ids.push(a.linkedToId);
                    }
                    if(!isExist){
                    arr.push(a);
                    }

                    });
                    res.setHeader('Content-Type', 'application/json');
                      res.setHeader('X-Total-Items',dstArray.length);
                      res.setHeader('Access-Control-Expose-Headers', 'X-Total-Items');
                      res.json(arr.slice(skip,skip+pageSize));
                });
              });
          }
        });
   });
//End:-QC3-7571 New get API For Get Unique Data Of LinkedApp

  apiRoutes.get('/updateIndex/:searchBy', function (req, res, next) {
    logger.log('API: /updateIndex/:searchBy: - Success', 'info', currentFileName);
    var index;
    if (req.params.searchBy === 'Question Title') {
      index = {
        title: 'text',
        tags: 'text'
      };
    }
    else {
      index = {
        'type.title': 'text'
      };
    }
    logger.log('Collection: questions: - Before dropIndexes', 'info', currentFileName);
    db.questions.dropIndexes();
    logger.log('Collection: questions: - Before ensureIndex', 'info', currentFileName);
    db.questions.ensureIndex(index, { default_language: "none" }, function (err, data) {
      if (err) {
        logger.log('Collection: questions: - Error : ' + err, 'error', currentFileName);
        return res.status(422).json({ 'errorMessage': err.message });
      }
      logger.log('Collection: questions: - Success', 'info', currentFileName);
      res.status(200).json(data);
    });
  });
  /**
  * @swagger
  * definition:
  *   ScheduleNew:
  *     properties:
  *       id:
  *         type: integer
  *       title:
  *         type: string
  *       schema:
  *         $ref: '#/definitions/Schema'
  *       time:
  *         type: dateTime
  *       date:
  *         type: dateTime
  *       scheduleuniqueId:
  *         type: string
  *       workflowreview:
  *           type: array
  *           items:
  *              type: string
  *       calendarschemaId:
  *         type: string
  *       scheduledBy:
  *         type: string
  *       type:
  *         type: string
  *       isActive:
  *         type: boolean
  *       notified:
  *         type: boolean
  *       email:
  *         type: string
  *       occurence:
  *         type: integer
  *       isSetReviewFrequency:
  *         type: boolean
  *       ismasterQcForm:
  *         type: boolean
  *       scheduleId:
  *         type: string
  *       keyvalues:
  *           type: array
  *           items:
  *              $ref: '#/definitions/Attribute_schedule'

  */

  /**
  * @swagger
  * definition:
  *   Query:
  *     properties:
  *       tableName:
  *         type: string
  *       sort:
  *         type: string
  *       page:
  *         type: integer
  *       pageSize:
  *         type: integer
  *       query:
  *         $ref: '#/definitions/Query'
  *       from:
  *         type: string
  *       isSetReviewFrequency:
  *         type: boolean
  */

  /**
  * @swagger
  * definition:
  *   SchedulePost:
  *     properties:
  *       tableName:
  *         type: string
  *       sort:
  *         type: string
  *       page:
  *         type: integer
  *       pageSize:
  *         type: integer
  *       query:
  *         type: object
  *       from:
  *         type: string
  *       isSetReviewFrequency:
  *         type: boolean
  */

  /**
  * @swagger
  * /allAppEntries:
  *   post:
  *     tags: [Schedule]
  *     description: Returns all schedules
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: schedule
  *         description: schedule object
  *         in: body
  *         required: true
  *         schema:
  *           type: array
  *           items:
  *              $ref: '#/definitions/SchedulePost'
  *     responses:
  *       200:
  *         description: An array of schedule
  *         schema:
  *           $ref: '#/definitions/ScheduleNew'
  */
  function getEntry(masterSchema,masterSequenceId) {
    var deferred = q.defer();
    var i = 0;
    var entriesData = [];
    var rfun = function () {
      if(i < masterSchema.length ){
        var entities;
        //START :: QC3-9839 by Yamuna.s
        if (_.isUndefined(db[masterSchema[i]._id])) {
          log.log("Table is not bind with mongoskin DB");
          entities = [];
          entities.push({
            collection: masterSchema[i]._id,
            audit: true,
            index: {
              title: ''
            }
          });
          log.log("Here for add collection in db object");
          dbConfig.configure.apply(this, entities);
          log.log("Adding collection to DB is Completed");
        }
        log.log("Collection is with db :: " + masterSchema[i]._id);
        //END :: QC3-9839 by Yamuna.s

        if(db[masterSchema[i]._id]){
          entities = [];
          entities.push({
            collection: db[masterSchema[i]._id],
            audit: true,
            index: {
              title: ''
            }
          });
          dbConfig.configure.apply(this, entities);

        }
        db[masterSchema[i]._id].find({'masterQCSettings.sequenceId' : masterSequenceId})
        .toArray(function (err , data) {
          if(data.length){
            entriesData.push(data[0]);
            i++;
            rfun();
          }else{
            i++;
            rfun();
          }
        })
      }else{
        deferred.resolve(entriesData);
      }
    }
    rfun();
    return deferred.promise;
  }
  apiRoutes.post('/appReviewedWorkflow', function (req, res, next) {
    db['masterqcsettings'].find({'_id' : db.ObjectID(req.body.masterId)},{'schema' : 1}).toArray(function (err, masterData) {
      getEntry(masterData[0].schema,req.body.masterSequenceId).then(function (successRes) {
        // QC3-8794 - Kajal
        var filteredObj = {};
        try{
          _.forEach(successRes,function (entryres) {
            if(entryres.workflowreview){
              var isAnyReviewed = _.find(entryres.workflowreview,{reviewstatus:0});
              if(isAnyReviewed){
                filteredObj.isworkflow = true;
              }
              var lastReviewed = _.last(entryres.workflowreview);
              if(lastReviewed && lastReviewed.reviewstatus == 0 && lastReviewed.reviewstatus.islockrecord){
                filteredObj.islockrecord = true;
              }
              if(entryres && entryres.entries && entryres.entries.status  && entryres.entries.status.status == 3){
                filteredObj.isvoid = true;
              }
            }
          });
        } catch(e){
          log.log("appReviewedWorkflow api ::" + e);
        }

        return res.status(200).json(filteredObj);
      });
    });
  });

  apiRoutes.post('/allAppEntries', function (req, res, next) {
    Date.prototype.stdTimezoneOffset = function () {
      var jan = new Date(this.getFullYear(), 0, 1);
      var jul = new Date(this.getFullYear(), 6, 1);
      return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
    }

    Date.prototype.dst = function () {
      return this.getTimezoneOffset() < this.stdTimezoneOffset();
    }




    var machineDate = new Date();
    var machineOffset = machineDate.getTimezoneOffset();
    //var clientOffset = machineOffset;
    //var mcdiff = 0;
    //clientOffset = parseInt(req.headers['client-offset']) || -330;
    //CHECK FOR THE DST
    //var dst = 0;
    if (machineDate.dst()) {
      machineOffset += 60;
    }
    var obj = {};
    var cnt = 0;
    //var andArray = [];
    _.forEach(req.body, function (gta, idx) {
      //create query
      var qry = {};
      var srt = {};
      var selectQuery = {};
      var page = 1;
      var pageSize = 10000;

      _.forEach(gta, function (val, key) {
        if (key != 'tableName' && key != 'sort' && key != 'select' && key != 'deselect' && key != 'page' && key != 'pageSize' && key != 'query' && key != 'from') {
          if (key == '_id' && val) {
            val = db.ObjectID(val);
          }
          qry[key] = val;
        }

        if (key == '$and') {
          //log.log("qry['$and']"+val['$and']);

          if (qry['and']) {
            qry['and'].push(val[0]);
          } else {
            qry['and'] = val;
          }
          delete qry['$and'];
        }
        if (key == 'select') {
          selectQuery = {};
          _.forEach(val.split(','), function forEachCallback(field) {
            selectQuery[field] = 1;
          });
        }
        if (key == 'deselect') {
          selectQuery = {};
          _.forEach(val.split(','), function forEachCallback(field) {
            selectQuery[field] = 0;
          });
        }
        if (key == 'sort') {
          srt[val] = 1;
        }
        if (key == 'page') {
          page = val;
        }
        if (key == 'pageSize') {
          pageSize = val;
        }
        if (key == 'query') {
          //Patch for schedules only
          for (var k in val) {

            qry[k] = val[k];
            if (k == 'date') {
              var tempObj = {};
              for (var a in val[k]) {
                tempObj[a] = new Date(val[k][a]);
              }
              qry[k] = tempObj;
            }else if (k == 'betdate') {
              for (var a in val[k]) {
              var tempTime = new Date(val[k][a]);
              var dateObj = new Date(val[k][a]);

              //code to use only date from the overduedate
              dateObj.setHours(0);
              dateObj.setMinutes(0);
              dateObj.setSeconds(0);
              var timeObj = new Date();
              //code to use only time from the overduedate
              timeObj.setHours(0);
              timeObj.setMinutes(0);
              timeObj.setSeconds(0);
              timeObj.setHours(tempTime.getHours());
              timeObj.setMinutes(tempTime.getMinutes());
              timeObj.setSeconds(tempTime.getSeconds());
              timeObj.setDate(1);
              timeObj.setMonth(0);
              timeObj.setYear(1970);
              var nxtDate = new Date(dateObj.getTime() + 86400000);
              if (val.isdst == true) {
                dateObj = new Date(dateObj.getTime() + 3600000);
                timeObj = new Date(timeObj.getTime() + 3600000);
                nxtDate = new Date(nxtDate.getTime() + 3600000);
              }
              var finalOrObj=[];
              if (a == "$lte") {
                finalOrObj = [{
                  "$or": [{
                    "$and": [
                      { "type": { "$ne": 'Hourly' } },
                      { "$or": [{ "date": { "$lt": dateObj } }, { "date": { "$gte": nxtDate } }] },
                      { "date": { "$lte": dateObj } },
                      { 'entry': { '$exists': false }}]
                  },
                  {
                    "$and": [
                      { "type": { "$ne": 'Hourly' } },
                      {
                        "date":
                        {
                          "$gte": dateObj,
                          "$lt": nxtDate
                        }
                      },
                      {
                        "time":
                        {
                          "$lte": (timeObj.toISOString())
                        }
                      },
                      { 'entry': { '$exists': false }}
                    ]
                  },
                  {
                    "$and": [{ "date": { "$lte": new Date(val[k][a]) } }, { "type": 'Hourly' },
                    { 'entry': { '$exists': false }}]
                  }
                  ]
                }];
              } else if (a == "$gte") {
                finalOrObj = [{
                  "$or": [{
                    "$and": [
                      { "type": { "$ne": 'Hourly' } },
                      { "$or": [{ "date": { "$lt": dateObj } }, { "date": { "$gte": nxtDate } }] },
                      { "date": { "$gte": dateObj } },
                      { 'entry': { '$exists': false }}]
                  },
                  {
                    "$and": [
                      { "type": { "$ne": 'Hourly' } },
                      {
                        "date":
                        {
                          "$gte": dateObj,
                          "$lt": nxtDate
                        }
                      },
                      {
                        "time":
                        {
                          "$gte": (timeObj.toISOString())
                        }
                      },
                      { 'entry': { '$exists': false }}
                    ]
                  },
                  {
                    "$and": [{ "date": { "$gte": new Date(val[k][a]) } }, { "type": 'Hourly' },
                    { 'entry': { '$exists': false }}]
                  }
                  ]
                }];
              }
            }

              if (qry['and'] && finalOrObj && finalOrObj[0]) {
                  qry['and'].push(finalOrObj[0]);
              } else {
                qry['and'] = finalOrObj;
              }
              delete qry['betdate'];
            }
            //QC3-6951-vaibhav
            else if (k == 'overdueDate') {
              var finalOrObj = [];

              for (var a in val[k]) {
                if(Object.keys(val[k]).length == 2){
                  var tempOverdueTimeArr=[];
                  var timeObjArr =[];
                  var dateObjArr = [];
                  var nxtDateArr=[];
                  for (var a in val[k]) {
                    var tempOverdueTime = new Date(val[k][a]);
                    var dateObj = new Date(val[k][a]);



                    //code to use only date from the overduedate
                    dateObj.setHours(0);
                    dateObj.setMinutes(0);
                    dateObj.setSeconds(0);
                    var timeObj = new Date();
                    //code to use only time from the overduedate
                    timeObj.setHours(0);
                    timeObj.setMinutes(0);
                    timeObj.setSeconds(0);
                    timeObj.setHours(tempOverdueTime.getHours());
                    timeObj.setMinutes(tempOverdueTime.getMinutes());
                    timeObj.setSeconds(tempOverdueTime.getSeconds());
                    timeObj.setDate(1);
                    timeObj.setMonth(0);
                    timeObj.setYear(1970);
                    var nxtDate = new Date(dateObj.getTime() + 86400000);
                    if (val.isdst == true) {
                      dateObj = new Date(dateObj.getTime() + 3600000);
                      timeObj = new Date(timeObj.getTime() + 3600000);
                      nxtDate = new Date(nxtDate.getTime() + 3600000);
                    }

                    timeObjArr.push(timeObj);
                    dateObjArr.push(dateObj);
                    nxtDateArr.push(nxtDate);
                    tempOverdueTimeArr.push(tempOverdueTime);

                  }
                  finalOrObj = [{
                    "$or": [{
                      "$and": [
                        { "type": { "$ne": 'Hourly' } },
                        { "$or": [{ "overdueDate": { "$lt": dateObjArr[0] } }, { "overdueDate": {"$gte": nxtDateArr[1] } }] },
                        { "overdueDate": { "$lte": dateObjArr[0],"$gt": dateObjArr[1] } }]
                    },
                    {
                      "$and": [
                        { "type": { "$ne": 'Hourly' } },
                        {
                          "overdueDate":
                          {
                            "$gte": dateObjArr[1],
                            "$lt": dateObjArr[0]
                          }
                        },
                        {
                          "overdueCompareTime":
                          {
                            "$lte": (timeObjArr[0]),
                            "$gt": (timeObjArr[1]),
                          }
                        }
                      ]
                    },
                    {
                      "$and": [{ "overdueDate": { "$lte": tempOverdueTimeArr[0],"$gt": tempOverdueTimeArr[1] } }, { "type": 'Hourly' }]
                    }
                    ]
                  }];

                }else{
                //QC3-8516  vaibhav
                log.log("val[k][a])::" + val[k][a]);
                var tempOverdueTime = new Date(val[k][a]);
                var dateObj = new Date(val[k][a]);

                //code to use only date from the overduedate
                dateObj.setHours(0);
                dateObj.setMinutes(0);
                dateObj.setSeconds(0);
                var timeObj = new Date();
                //code to use only time from the overduedate
                timeObj.setHours(0);
                timeObj.setMinutes(0);
                timeObj.setSeconds(0);
                timeObj.setHours(tempOverdueTime.getHours());
                timeObj.setMinutes(tempOverdueTime.getMinutes());
                timeObj.setSeconds(tempOverdueTime.getSeconds());
                timeObj.setDate(1);
                timeObj.setMonth(0);
                timeObj.setYear(1970);
                var nxtDate = new Date(dateObj.getTime() + 86400000);
                if (val.isdst == true) {
                  dateObj = new Date(dateObj.getTime() + 3600000);
                  timeObj = new Date(timeObj.getTime() + 3600000);
                  nxtDate = new Date(nxtDate.getTime() + 3600000);
                }

                if (a == "$lte") {
                  finalOrObj = [{
                    "$or": [{
                      "$and": [
                        { "type": { "$ne": 'Hourly' } },
                        { "$or": [{ "overdueDate": { "$lt": dateObj } }, { "overdueDate": { "$gte": nxtDate } }] },
                        { "overdueDate": { "$lte": dateObj } },
                        { 'entry': { '$exists': false }}]
                    },
                    {
                      "$and": [
                        { "type": { "$ne": 'Hourly' } },
                        {
                          "overdueDate":
                          {
                            "$gte": dateObj,
                            "$lt": nxtDate
                          }
                        },
                        {
                          "overdueCompareTime":
                          {
                            "$lte": (timeObj)
                          }
                        },
                        { 'entry': { '$exists': false }}
                      ]
                    },
                    {
                      "$and": [{ "overdueDate": { "$lte": new Date(val[k][a]) } }, { "type": 'Hourly' },
                      { 'entry': { '$exists': false }}]
                    }
                    ]
                  }];
                } else if (a == "$gte") {
                  finalOrObj = [{
                    "$or": [{
                      "$and": [
                        { "type": { "$ne": 'Hourly' } },
                        { "$or": [{ "overdueDate": { "$lt": dateObj } }, { "overdueDate": { "$gte": nxtDate } }] },
                        { "overdueDate": { "$gte": dateObj } },
                        { 'entry': { '$exists': false }}]
                    },
                    {
                      "$and": [
                        { "type": { "$ne": 'Hourly' } },
                        {
                          "overdueDate":
                          {
                            "$gte": dateObj,
                            "$lt": nxtDate
                          }
                        },
                        {
                          "overdueCompareTime":
                          {
                            "$gte": (timeObj)
                          }
                        },
                        { 'entry': { '$exists': false }}
                      ]
                    },
                    {
                      "$and": [{ "overdueDate": { "$gte": new Date(val[k][a]) } }, { "type": 'Hourly' },
                      { 'entry': { '$exists': false }}]
                    }
                    ]
                  }];
                }

              }
            }

              //qry['overdueDate'] = tempObj;
              //qry['overdueCompareTime'] = tempObjTime;

              if (qry['or']) {
                finalOrObj[0].push(qry['or']);
                delete qry['or'];

              }
              if (qry['and']) {

                if(qry['and'][0]['$or']){
                 _.forEach(finalOrObj[0]['$or'], function (t) {
                    qry['and'][0]['$or'].push(t);
                  });
                }else{
                  qry['and'].push(finalOrObj[0]);
                }
              } else {
                qry['and'] = finalOrObj;
              }



            }
            else if (k=='dtRange') {

                var dtObj={"date":{}};
                if(qry['dtRange'][0]['date']['$gte'] && qry['dtRange'][0]['date']['$lte'])
                  {
                    dtObj['date']={'$gte':new Date(qry['dtRange'][0]['date']['$gte']),'$lte':new Date(qry['dtRange'][0]['date']['$lte'])}
                  }
                  for(var orObj in qry['and'][0]['$or']){
                    qry['and'][0]['$or'][orObj]['$and'].push(dtObj);
                  }
                  delete qry['dtRange'];
            }

            else if (k == 'andor') {

              if (qry['andor']) {
                var finalAnd=[];
                for(var andor in qry['andor']){
                  if(!qry['and'] ){
                    qry['and'] =[];
                  }
                  if(qry['andor'][andor]['$or']){
                    var orObj={"$or":[]};
                      for(var orr in qry['andor'][andor]['$or']){
                        if(qry['andor'][andor]['$or'][orr] ){

                          for (var k in qry['andor'][andor]['$or'][orr]){
                            if(k=='date'){
                              dateObj={"date":{}};
                              if(qry['andor'][andor]['$or'][orr]['date']['$gte'] && qry['andor'][andor]['$or'][orr]['date']['$lte'])
                                {
                                  dateObj['date']={'$gte':new Date(qry['andor'][andor]['$or'][orr]['date']['$gte']),'$lte':new Date(qry['andor'][andor]['$or'][orr]['date']['$lte'])}
                                }
                                orObj['$or'].push(dateObj);
                            }else{
                              if(orObj['$or']){
                                if(_.isArray(qry['andor'][andor]['$or'][orr][k]))
                                  {
                                    orObj['$or'].push(qry['andor'][andor]['$or'][orr][k][0]);
                                  }else{
                                      if(orObj['$or'][orr][k]){
                                        orObj['$or'][orr][k]= qry['andor'][andor]['$or'][orr][k];
                                      }else{
                                        orObj['$or'][orr][k]={};
                                        orObj['$or'][orr][k]= qry['andor'][andor]['$or'][orr][k];
                                      }

                                    }
                              }
                            }
                          }
                        }

                      }
                      finalAnd.push(orObj);
                  }
                }
                if(qry['and'] && qry['and'][0] && qry['and'][0]['$or']){
                  if(qry['and'][0]['$or']){
                    for(var x in finalAnd){
                      qry['and'][0]['$or'].push({'$and':finalAnd[x]['$or']});
                    }

                  }
                }else{
                  qry['and']=finalAnd;
                }
                delete qry['andor'];
              }
            }
            else if (k == 'or') {
              if (qry['and']) {
                if(qry['and'][0]['$or']){
                  var dateObj={};
                  if(qry['or'][0]['date']){

                    dateObj={"date":{}};
                    if(qry['or'][0]['date']['$gte'] && qry['or'][0]['date']['$lte'])
                      {
                        dateObj['date']={'$gte':new Date(qry['or'][0]['date']['$gte']),'$lte':new Date(qry['or'][0]['date']['$lte'])}
                      }
                      qry['and'][0]['$or'].push(dateObj);
                  }else{
                    // qry['and'][0]['$or'].push(qry['or'][0]);
                    for(var or in qry['or']){
                   qry['and'][0]['$or'].push(qry['or'][or]);
                 }
                  }


                }else{
                  qry['and'].push({"$or":[]});
                  qry['and'][0]['$or'].push(qry['or'][0]);
                }
              } else {
                qry['and'] =[{"$or":[]}];

                if(qry['or'][0]['date']){
                  var dateObj={};
                  dateObj={"date":{}};
                  if(qry['or'][0]['date']['$gte'])
                    {
                      dateObj['date']={'$gte':new Date(qry['or'][0]['date']['$gte']),'$lte':new Date(qry['or'][0]['date']['$lte'])}
                    }
                    qry['and'][0]['$or'].push(dateObj);
                }else{
                  // qry['and'][0]['$or'].push(qry['or'][0]);
                  for(var or in qry['or']){
                   qry['and'][0]['$or'].push(qry['or'][or]);
                 }
                }

              }
              delete qry['or'];
            }
            else if (k == 'schema._id') {
              var tempObj3 = [];
              _.forEach(val[k]['$in'], function (v) {
                tempObj3.push(db.ObjectID(v));
              });

              qry[k] = { '$in': tempObj3 }
            }
            else if (k == 'workflowreview') {
              //by surjeet.b.. $or query for finding schedules from either username or role..

              qry['$or'] = val[k];

              // var tempObj4 = [];
              // if(!_.isUndefined(val[k]['$in'])){
              //
              //       tempObj4.push(val[k]['$in']);
              //
              //   qry[k] = {'$in': tempObj4}
              // }
              // else{
              //    qry[k] = {'$nin': tempObj4}
              // }
            }
          }
        }
      });
      delete qry.workflowreview;
      delete qry.workflowreviewroles;
      delete qry.overdueDate;
      delete qry.isdst;
      if (qry['and']) {
        qry['$and'] = _.cloneDeep(qry['and']);
      }
      delete qry['and'];
      delete qry['or'];

      if(gta.tableName == 'roles'){
        delete selectQuery['modules'];
        selectQuery['modules._id'] = 1;
        selectQuery['modules.permissions.entity'] = 1;
      }

      var queryF;
      if (gta.tableName == "batchEntries") {
        var childQuery = {};

        if (qry.sequenceId) {
          childQuery.record = {
            'masterQCSettings.sequenceId': qry.sequenceId
          };
        }

        if (qry.formId) {
          childQuery.record = {
            'formId': qry.formId
          };
        }

        if (qry.sequenceId && qry.orBatchNo) {
          childQuery.record = {
            $or: [{
              'batchNo': qry.orBatchNo
            }, {
              'masterQCSettings.sequenceId': qry.sequenceId
            }]
          };
          childQuery.attribute = {
            'batchNo': qry.orBatchNo
          };
        }

        if (!qry.sequenceId && qry.orBatchNo) {
          childQuery.record = {
            'batchNo': qry.orBatchNo
          };
          childQuery.attribute = {
            'batchNo': qry.orBatchNo
          };
        }

        if (qry.recordSelect) {
          childQuery.recordSelect = qry.recordSelect;
        }

        queryF = batchEntryService.getBatchEntries(qry.schemaId, null, null, childQuery)
          .then(function (data) {
            resCallback(null, data)
          });
      } else {
        queryF = db[gta.tableName].find(qry, selectQuery).sort(srt).skip(pageSize * (page - 1)).limit(pageSize).toArray(resCallback);
      }

      function resCallback(err, dt) {
        cnt++;
        if (err) {
          log.log(err);
        }
        else {
          if (gta.tableName == 'calendar' && gta.from == 'calendar_list') {
            /* var inactiveschedule = _.filter(dt, function (a) {
              return a.isActive == false;
            }) */
            /* var activeschedule = _.filter(dt, function (a) {
              return a.isActive == true;
            }) */
            db[gta.tableName].find(qry, selectQuery).toArray(function (err, dt1) {
              obj["totalRecords"] = dt1.length;
              obj[gta.tableName] = dt;
              if (cnt == req.body.length) {
                return res.status(200).json(obj);
              }
            });
          }
          else {
            obj[gta.tableName] = dt;
            if (cnt == req.body.length) {
              return res.status(200).json(obj);
            }
          }
        }
      }
    });
  });


  //akashdeep.s - QC3-2093 - bypassig the routes
  require('../routes/oApp')(app);

  //akashdeep.s - API-Project
  require('../routes/apiProject')(app);

  //akashdeep.s - swagger by Project-API team
  require('../routes/swagger')(app);

  /**
  * @swagger
  * definition:
  *   CalendarPost:
  *     properties:
  *       tableName:
  *         type: string
  *       calendarIds:
  *         type: string
  *       IsmasterQcForm:
  *         type: boolean
  */

  /**
  * @swagger
  * /allCalendarEntries:
  *   post:
  *     tags: [Schedule]
  *     description: Returns all schedules entries
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: array of object
  *         description: schedule object
  *         in: body
  *         required: true
  *         schema:
  *           type: array
  *           items:
  *              $ref: '#/definitions/CalendarPost'
  *     responses:
  *       200:
  *         description: An array of schedule entries
  *         schema:
  *           type: array
  *           items:
  *              $ref: '#/definitions/Entry'
  */

  //by surjeet.b@producitvet.com.. fetch entries by appId and then map them with calendarIds..
  function getAllUsers() {
    logger.log('Function: getAllUsers- Success', 'info', currentFileName);
    var deferred = q.defer();
    var selectQuery = {};
    selectQuery['_id'] = 1;
    selectQuery['username'] = 1;
    selectQuery['firstname'] = 1;
    selectQuery['lastname'] = 1;
   // logger.log('Collection: users- Before Find', 'info', currentFileName);
    db.users.find({}, selectQuery).toArray(function (err , data) {
      logger.log('Collection: users- Success Find', 'info', currentFileName);
      deferred.resolve(data);
    });
    return deferred.promise;
  }
  function scheduleEntries(reqdata,res) {
    try{
      logger.log('Function: scheduleEntries- Success', 'info', currentFileName);
      var deferred = q.defer();
      var obj = {};
      var cnt = 0;
      _.forEach(reqdata, function (gta,idx) {
        var qry = {};
        //var srt = {};
        var selectQuery = {};
        var page=1;
        var pageSize=10000;
        _.forEach(gta, function (val,key) {
          if (key != 'tableName' && key != 'sort' && key != 'select' && key!='page' && key!='pageSize' && key!= 'query' && key !='from') {
            if (key == '_id' && val) {
              val = db.ObjectID(val);
            }
            qry[key] = val;
          }
          if(key == 'select'){
            selectQuery = {};
            _.forEach(val.split(','), function forEachCallback(field) {
              selectQuery[field] = 1;
            });
          }
          if (key == 'page') {
            page = val;
          }
          if (key == 'pageSize') {
            pageSize = val;
          }
        });
        delete qry.IsmasterQcForm;
        obj[gta.calendarIds] = obj[gta.calendarIds] || {};
        // console.log(obj);
        if(gta.tableName){
          //logger.log('Collection: '+gta.tableName+'- Before Find', 'info', currentFileName);
          db[gta.tableName].find(qry, selectQuery).toArray(function (err, dt) {
            cnt++;
            if (err) {
              logger.log('Collection: '+gta.tableName+'- Failed - '+ err, 'error', currentFileName);
              //logger.log(err);
            }
            else {
              logger.log('Collection: '+gta.tableName+'- Success', 'info', currentFileName);
              // Start: QC3-4195 Improvement - Create schedule for the whole master app instead of apps from master (1193)
              // Changed By: Kajal Patel
              // Description: code to create schedule for master.
              if(gta.IsmasterQcForm){
                obj[gta.calendarIds][gta.tableName] = dt;
              }else{
                obj[gta.calendarIds] = dt;
              }
              if (cnt == reqdata.length) {
                deferred.resolve(obj);
              }
            }
          });
        }else{
          deferred.resolve(obj);
        }
      });
      return deferred.promise;
    }
    catch(e){
      logger.log("Ex in allCalendarEntries" + e, 'error');
    }
  }
  apiRoutes.post('/allCalendarEntries', function (req, res, next) {
    logger.log('API: /allCalendarEntries- Success', 'info', currentFileName);
    scheduleEntries(req.body).then(function (success) {
      logger.log('Function: scheduleEntries(Promise)- Success', 'info', currentFileName);
      return res.status(200).json(success);
    });
  });
  apiRoutes.get('/getAllCollection', function (req, res, next) {
    getAllCollection(res);
  });
  setTimeout(function(){
    getAllCollection();
  },25000)
  function getAllCollection(res){
    var allCollection = Object.keys(db);
    var errorArr = [];
    var appCount = 0;
    var entityCount = 0;
    db['schema'].find({},{title : 1}).toArray(function (err,schemas) {
      for (var schema of schemas) {
        if(allCollection.indexOf(schema._id.toString()) == -1){
          schema.type = 'schema'
          errorArr.push(schema)
        }else{
          appCount = appCount + 1;
        }
      }
      db['entities'].find({},{title : 1}).toArray(function (err,entities) {
        for (var entity of entities) {
          if(allCollection.indexOf(entity._id.toString()) == -1){
            entity.type = 'entity';
            errorArr.push(entity)
          }else{
            entityCount = entityCount + 1;
          }
        }
        var resObj = {data:errorArr,length:errorArr.length,appCount : appCount,entityCount:entityCount}
        if(resObj && resObj.data && resObj.data.length){
          var today = new Date();

          var fileName = today.getDate() +'-'+(today.getMonth()+1)+'-'+today.getFullYear()+'--'+today.getHours()+':'+today.getMinutes()+'.json';
          var dir = './logger app start/';
          if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
          }
          fs.writeFile(dir+fileName,JSON.stringify(resObj) ,function(err){
            if(err){
              logger.log('file not write - collection not loaded', 'error', currentFileName);
            }
          });
          resObj.envurl = config.envurl;
          mailer.send('not-loaded-collections', resObj,  config.staticURL, function sendMailCallback(err, value) {
            if(err){
              logger.log('Mail sending fail - collection not loaded', 'error', currentFileName);
            }else{
              logger.log('Mail send - collection not loaded', 'debug', currentFileName);
            }
          });
        }
        if(res){
          res.setHeader('Content-Type', 'application/json');
          res.status(200).send(resObj)
        }
      })
    })
  }
  apiRoutes.post('/allReviewPendingEntries', function (req, res, next) {
    logger.log('API: /allReviewPendingEntries- Success', 'info', currentFileName);
    getAllUsers().then(function (successres) {
      logger.log('Function: getAllUsers(Promise)- Success', 'info', currentFileName);
      var allusers = successres;
      var qry = {};
      var srt = {};
      var page = 1;
      var pageSize = 10000;
      _.forEach(req.body, function (val,key) {
        if(val&&  key != 'tableName' && key != 'sort' && key != 'select' && key!='page' && key != 'pageSize'){
          if(key == 'query'){
            _.forEach(val, function (queryval, querykey) {//Start::QC3-7490 - Vaibhav
              if(querykey=='reviewworkflow.user.id'){//Start:QC3-7613 - Vaibhav
                var orQry=[];
                var userObj= queryval;
                if(userObj.userId.length>0){
                  var reviewUserId={'reviewworkflow.user.id': userObj.userId};
                  orQry.push(reviewUserId);
                  //PQT-15 Prachi - Start
                  var getUser = _.find(allusers, function(user) {
                    if (userObj.userId == user._id.toString()) {
                      return user;
                    }
                  });

                  // Mahammad + Jyotil
                  qry['$and'] = qry['$and'] || [];
                  qry['$and'].push({
                    $or: [{
                      'reviewworkflow.reviewPendingUsers': {
                        '$exists': false
                      }
                    }, {
                      'reviewworkflow.reviewPendingUsers': getUser.username
                    }]
                  });
                  // Mahammad + Jyotil

                  //PQT-15 Prachi - End
                }

                var userRoles=[];//QC3-7799-Vaibhav
                _.forEach(userObj.userRoles,function(usr,inx){
                  userRoles.push(usr.title);
                });
                var userSelect={'reviewworkflow.userSelectionType':"0",'reviewworkflow.roles':{'$in':userRoles}}; //QC3-7799-Vaibhav
                orQry.push(userSelect);

                // Mahammad + Jyotil
                qry['$and'] = qry['$and'] || [];
                qry['$and'].push({
                  $or: orQry
                }); //End:QC3-7613 - Vaibhav
                // Mahammad + Jyotil

              }else{
                qry[querykey] = queryval;
              }//End::QC3-7490 - Vaibhav
            });
          }
        }
        if (key == 'page') {
          page = val;
        }
        if (key == 'pageSize') {
          pageSize = val;
        }
        if (key == 'sort') {
          if(val.indexOf('-') > -1)
          {
            srt[val.substring(1,val.length)] = -1;
          } else {
            srt[val] = 1;
          }
        }
      });
      //QC3-2767
      //Changed By:Kajal Patel

      logger.log('Collection: reviewpendingworkflow- Before Find', 'info', currentFileName);
      db['reviewpendingworkflow'].find(qry).sort(srt).skip(pageSize*(page-1)).limit(pageSize).toArray(function (err, reviewdata) {
        logger.log('Collection: reviewpendingworkflow(Promise)- After Find', 'info', currentFileName);
        var cnt = 0;
        var selectQuery = {};
        var entryArray = [];
        if(reviewdata.length > 0){
          _.forEach(reviewdata,function (dt) {
             logger.log("dt.schemaId :: " + dt.schemaId + " - dt.verificationDate :: " + dt.verificationDate, 'info');
              //logger.log("dt.verificationDate :: " + dt.verificationDate, 'info');
              if(_.isUndefined(db[dt.schemaId])){
                //logger.log("Table is not bind with mongoskin DB");
                var entities = [];
                entities.push({
                  collection: dt.schemaId,
                  audit: true,
                  index: {
                    title: ''
                  }
                });
                //logger.log("Here for add collection in db object");
                dbConfig.configure.apply(this, entities);
                //logger.log("Adding collection to DB is Completed");
              }
              logger.log("Collection is with db :: " + dt.schemaId);
            db[dt.schemaId].find({_id : db.ObjectID(dt.entryId)}).toArray(function (err, entry) {
              try{
                selectQuery['title'] = 1;
                selectQuery["keyvalue"] = 1;
                selectQuery['attributes'] = 1;
                selectQuery['entities'] = 1;
                logger.log('Collection: schema- Before Find', 'info', currentFileName);
                db['schema'].find({_id : db.ObjectID(dt.schemaId)},selectQuery).toArray(function (err,schema) {
                  logger.log('Collection: schema(Promise)- After Find', 'info', currentFileName);
                  cnt++;
                  try{
                    var pendingEntry = entry[0];
                    var user = _.find(allusers,{_id : db.ObjectID(pendingEntry.modifiedBy)});
                    var obj = {
                      username : user.username,
                      firstname :user.firstname,
                      lastname : user.lastname
                    }
                    pendingEntry.lastmodifieduser = obj;
                    pendingEntry.schema = {};
                    pendingEntry.schema._id = schema[0]._id;
                    pendingEntry.schema.title= schema[0].title;
                    //endingEntry.verificationDate=dt.verificationDate

                    pendingEntry["schemaAttrEntity"] = buildSchemaAttrEntity(schema[0], entry[0]);
                    entryArray.push(pendingEntry);
                  } catch (e){
                    logger.log("Error in reviewpendingworkflow :: " +e,'error');
                  }
                  if(cnt == reviewdata.length){
                    //logger.log('Collection: reviewpendingworkflow : if(cnt == reviewdata.length)- Before Find', 'info', currentFileName);
                    db['reviewpendingworkflow'].find(qry).count(function (err , pendingres) {
                      //logger.log('Collection: reviewpendingworkflow : if(cnt == reviewdata.length)- After Find', 'info', currentFileName);
                      // obj["totalRecords"]=dt1.length;
                      // entryArray["totalRecords"]=reviewdata.length;
                      // console.log(a);
                      if(srt['verificationDate']==1){
                        entryArray= _.sortBy(entryArray, ['verificationDate']);
                      }else{
                        entryArray= _.sortBy(entryArray, ['verificationDate']).reverse();
                      }
                      var finalres = {
                        totalRecords : pendingres,
                        entry : entryArray
                      }
                      res.status(200).json(finalres);
                    })

                  }
                });
              } catch (e){
                logger.log("Error in reviewpendingworkflow :: " +e,'error');
              }
            });
          });
        }else{
          var finalres = {
            totalRecords : '',
            entry : []
          }
          res.status(200).json(finalres);
        }
      });
    });
  });
/**
 *
 * @author Surjeet Bhadauriya <surjeet.b@productivet.com>
 * @param {object} schema
 * @param {object} entry
 * @returns
 */
function buildSchemaAttrEntity(schema, entry) {
    if(!schema || !entry){
      return [];
    }

    var schemaAttrEntity = [];

    _.forEach(schema.attributes, function(attribute) {
        schemaAttrEntity.push({ title: attribute.title, "s#": attribute["s#"], type: attribute.type });
    });

    _.forEach(schema.entities, function(entity) {
        var keyAttribute = _.find(entity.questions, {isKeyValue: true});
        schemaAttrEntity.push({ title: keyAttribute.title, "s#": keyAttribute["s#"], type: keyAttribute.type, parentId: keyAttribute.parentId });
    });

    return schemaAttrEntity;
  }

  /**
  * @swagger
  * definition:
  *   CalendarSchedulePost:
  *     properties:
  *       scheduleuniqueId:
  *         type: string

  */

  /**
  * @swagger
  * /allCalendarSchedule:
  *   post:
  *     tags: [Schedule]
  *     description: Returns all schedules entries
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: array of object
  *         description: schedule object
  *         in: body
  *         required: true
  *         schema:
  *           type: array
  *           items:
  *              $ref: '#/definitions/CalendarSchedulePost'
  *     responses:
  *       200:
  *         description: An array of schedule entries
  *         schema:
  *           type: array
  *           items:
  *              $ref: '#/definitions/ScheduleNew'
  */
  //QC3-4917 : Schedules are not getting display on the screen takes too much time to load the page
  //changed by: Kajal Patel fetch schedule entries by uniqueId.
  apiRoutes.post('/allCalendarSchedule', function (req, res, next) {
    logger.log('API: /allCalendarSchedule- Start', 'info', currentFileName);
    var obj = [];
    var cnt = 0;
    _.forEach(req.body, function (gta,idx) {
      var qry = {};
      //var srt = {};
      //var selectQuery = {};
      //var page=1;
      //var pageSize=10000;
      _.forEach(gta, function (val,key) {
        if(key == 'scheduleuniqueId'&& val){
          qry[key] = val;
        }
        qry['isSetReviewFrequency'] = false;
        //qry['isActive'] = true;
      });

      //logger.log('Collection: calendar - Before Find', 'info', currentFileName);
      db['calendar'].find(qry).toArray(function (err, dt) {
        logger.log('Collection: calendar - After Find', 'info', currentFileName);

        cnt++;
        if( dt.length > 0){
          obj.push(dt[0]);
          if (cnt == req.body.length) {
            return res.status(200).json(obj);
          }
        }
        else {
          //Start::QC3-5698-Review Schedule : When user click on review schedule then no any records get display on the screen.
          if (cnt == req.body.length) {
            return res.status(200).json(obj);
          }
          //End:QC3-5698- Review Schedule : When user click on review schedule then no any records get display on the screen.
          //return res.status(200).json(obj);
        }
      });
      //
    });
  });

  apiRoutes.post('/updateManyEntityRecords', function (req, res, next) {
    logger.log('API: /updateManyEntityRecords- Success', 'info', currentFileName);
      if (req.body && req.body._id) {
        var entityId = req.body._id;

        var valIds = _.map((req.body.valIds || []), function (val) {
          return db.ObjectID(val);
        });

        let objectToUpdate = {
          isDeleted: true,
          "entityRecord.isActive": false,
          modifiedDate: new Date()
        }

        if (req.user) {
          objectToUpdate.modifiedByName = req.user.firstname + ' ' + req.user.lastname + ' (' + req.user.username + ')';
          objectToUpdate.modifiedBy = req.user._id;
        }

        //logger.log("collection: " + entityId.toString() + " updateMany", 'info', currentFileName);
        db[entityId.toString()].updateMany({_id:{$in: valIds}}, {
            $set: objectToUpdate
          },function(response){
            //logger.log("collection: " + entityId.toString() + " updateMany", 'info', currentFileName);
            return res.json(response);
          });
      } else {
        next();
      }
  });

  apiRoutes.get('/schemaToMajorVersion', function (req, res, next) {
    var type = 'schema';
    res.send('schema updated sucessfully..')

    db[type].find({}).forEach(function (item) {
        item.isMajorVersion = true;
        item.isDraft = false
        db[type+'Logs'].find({ identity: db.ObjectID(item._id), ip: { $ne: null } }, { ip: 1 })
          .sort({ $natural: -1 })
          .limit(1)
          .forEach(function (array) {
            auditThis1({
              collection: type,
              logsCollection: type+'Logs'
            }, false, item, array.ip);

          });
    });

  });

  app.use('/', apiRoutes);

  require('../routes/advancedSearch')(app);
  require('../routes/exportSchema')(app); // Jyotil - Export App
  require('../routes/importSchema')(app); // Jyotil - Impport App
  require('../routes/review').attachRoutes(app);
  require('../routes/delete')(app);
  // require('../routes/deletesearchFavorites')(app);
  //chandni - code end - Added below code for QC3-1904


  apiRoutes.get('/entity/keyattr/:id',function(req,res){
    db.collection('entities').findOne({_id:db.ObjectID(req.params.id)},
      function(err,data){
        var keyQuestion = data.questions.find(function (_question) {
          return _question.isKeyValue
        });
        var keyQuestionSequienceId = keyQuestion['s#'];
        var keyType = keyQuestion.type;
        // var keyQuestionSequienceId = data.questions.find(function(_question){
        //   return _question.isKeyValue
        // })['s#'];

        var project = {$project:{"value" : "$entityRecord."+keyQuestionSequienceId,"_id":-1,'isActive':"$entityRecord.isActive",'isDeleted':1}}
        db.collection(req.params.id).aggregate(project,function(err,data){
          res.json({data : data,keyType: keyType})
        })
      })
  })


  //by surjeet.b@productivet.com
  apiRoutes.post('/postBatchEntry', function (req, res, next) {
    if (req.body.batchEntryId == null) {
      var loggedInUser = _.cloneDeep(req.body.loggedInUser[0]);
      var buildObj = {
        batchEntries: req.body.batchEntries,
        batchAttributesInfo: req.body.batchAttributesInfo,
        schemaId: req.body.schemaId,
        schemaTitle: req.body.schemaTitle,
        createdBy: loggedInUser._id,
        modifiedBy: loggedInUser._id,
        createdByName: loggedInUser.firstname + ' ' + loggedInUser.lastname + ' (' + loggedInUser.username + ')',
        modifiedByName: loggedInUser.firstname + ' ' + loggedInUser.lastname + ' (' + loggedInUser.username + ')',
        createdDate: new Date(),
        modifiedDate: new Date()
      }

      var batchName = req.body.batchName;
      delete req.body.loggedInUser;
      delete req.body.batchName;
      batchEntryService.updateBatchEntry(buildObj, null, batchName, function (err, datad) {
        if (err) {
          return res.json({error: true});
        }
        res.json({ data: datad[0] });
        removesaveAutoBatchCorrectData(req.body.entryId);
      });

    }else {

      var buildObj = {
        batchEntries: req.body.batchEntries,
        batchAttributesInfo: req.body.batchAttributesInfo,
        schemaId: req.body.schemaId
      };

      batchEntryService.updateBatchEntry(buildObj, db.ObjectID(req.body.batchEntryId), req.body.batchName, function (err, datad) {
        if (datad) {
          res.json({ data: datad[0] });
          removesaveAutoBatchCorrectData(req.body.entryId);
        } else {
          res.json({ error: true });
        }
      });
    }
  });

  function removesaveAutoBatchCorrectData(entryId) {
    if (entryId) {
      db.autoBatchCorrectData.remove({entryId: db.ObjectID(entryId)});
    }
  }
  //end by surjeet.b@productivet.com

}

module.exports = {
  configure: configure
};
