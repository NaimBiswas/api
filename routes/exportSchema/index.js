/**
* @author Jyotil Raval <jyotil.r@productivet.com>
* @description Export Schema with Whole Schema detail, Attributes, Entities, EntityRecord, Procedure, Questions, Sets
* @param {Array} schema._id
* @return {String} FileName.pqt || FileName.zip encryped file or zip of encryped files of schema detail with extension {.pqt}
* @version 1.0
*/

(function () {
  'use strict';
  var express = require('express');
  var db = require('../../lib/db');
  var crypto = require('../../lib/utilities/cryptoUtilitiese');
  var _ = require('lodash');
  var fs = require('fs');
  var decode = require('../../lib/oauth/model').getAccessToken;
  var lzstring = require('lz-string');
  var logger = require('../../logger');
  var currentFileName = __filename;
  var exportSchemaCore = {
    app: require('./exportSchemaCore.js')
  };


  module.exports = function (app) {
    var apiRoutes = express.Router();

    apiRoutes.post('/app', function (req, res, next) {
      authenticRequest(req, res, next, 'app');
    });

    function authenticRequest (req, res, next, type) {
      logger.log('API Called : api/importSchema', 'info', currentFileName);
      var xToken = req.headers['x-authorization'] || req.query['x-authorization'];
      if (xToken) {
        logger.log('if(xToken) : Inside', 'info', currentFileName);
        db['allAuthToken'].findOne({
          token: xToken
        }, function (err, token) {
          if (err || !token) {
            logger.log('Collection: allAuthToken - Failed : ' + err, 'error', currentFileName);
            res.status(403).json({
              message: 'You are not authorized.'
            });
            return;
          }
          logger.log('Collection: allAuthToken- success', 'info', currentFileName);
          renderPost(req, res, next, (type || 'app'));
        });
      } else if (req.headers['authorization'] || req.query['authorization']) {
        logger.log("else if (req.headers['authorization'] || req.query['authorization'])- Inside authorization brear token", 'info', currentFileName);
        var decompressToken = lzstring.decompressFromEncodedURIComponent(req.headers['authorization']);
        var token = decompressToken;
        if (token && _.startsWith(token, 'Bearer ')) {
          logger.log("if(token && _.startsWith(token,'Bearer ')) : Inside decompressed authorization brear token ", 'info', currentFileName);
          decode(token.substring(7), function (err, data) {
            if (err) {
              logger.log('Error: decoding authorization brear token failed : '+ err, 'error', currentFileName);
              res.status(403).json({
                message: 'You are not authorized.'
              });
              return;
            }
            logger.log('decode: Success - Inside decoded authorization brear token Info', 'info', currentFileName);
            renderPost(req, res, next, (type || 'app'));
          });
        } else {
          logger.log('else: authorization brear token decompression failed', 'info', currentFileName);
          res.status(403).json({
            message: 'You are not authorized.'
          });
        }
      } else {
        logger.log('else: No token found', 'info', currentFileName);
        res.status(403).json({
          message: 'You are not authorized.'
        });
      }
    }

    function renderPost (req, res, next, type) {
      logger.log('Function: RenderPost - Start', 'info', currentFileName);
      var options = req.body || {};
      var schemaTitles = req.body.schemas || [];
      options.schemas = options.schemas.map(function(schema){
        return schema._id;
      });
      schemaTitles = schemaTitles.map(function(schema){
        return schema.title;
      });
      exportSchemaCore[type || 'app'].generateSchemaDetail(options).then(function (response) {
        // var encryptedResponse = JSON.stringify(response);
        var encryptedResponse = crypto.encrypt(JSON.stringify(response));
        var filename = (new Date()).getTime() + '-' + schemaTitles.join('+') + '.pqt';
        fs.writeFile((process.env.FILE_STORE+'/'+filename), encryptedResponse, function (err) {
          if (err) {
            res.status(400).json({
              message: err
            });
          } else {
            res.status(200).json(filename);
          }
        });
      }).catch(function (err) {
        res.status(400).json({
          message: err
        });
      });
    }

    app.use('/exportSchema/', apiRoutes);
  }

})();
