/**
* @author Jyotil Raval <jyotil.r@productivet.com>
* @description Export Schema with Whole Schema detail, Attributes, Entities, EntityRecord, Procedure, Questions, Sets
* @param {Array} schema._id
* @return {File} FileName.pqt || FileName.zip encryped file or zip of encryped files of schema detail with extension {.pqt}
* @version 1.0
*/

(function () {
  'use strict';

  var db = require('../../lib/db');
  var _ = require('lodash');
  var q = require('q');
  var logger = require('../../logger');
  var currentFileName = __filename;

  function generateFilter(schemaDetail) {
    logger.log('Function: generateFilter - Start', 'info', currentFileName);
    var filter = {};
    var attributeFilter = [];
    var entityFilter = [];
    var proceduresFilter = [];
    var questionsFilter = [];
    var setsFilter = [];
    var appFilter = [];
    (schemaDetail || []).forEach(function(schema){
      logger.log('Loop: schemaDetail - Start', 'info', currentFileName);
      var attributes = [];
      var entities = [];
      var procedures = [];

      attributes = (schema.attributes || []).map(function (attribute) {
        logger.log('Loop: schema.attributes - Start', 'info', currentFileName);
        return {
          _id : attribute._id,
          version : attribute.version
        }
      });
      entities = (schema.entities || []).map(function (entity) {
        logger.log('Loop: schema.entities - Start', 'info', currentFileName);
        var sets = [];
        (entity.questions || []).forEach(function (question){
          logger.log('Loop: entity.questions - Start', 'info', currentFileName);
          if (question.validation && question.validation.validationSet && question.validation.validationSet.existingSet) {
            sets.push(question.validation.validationSet.existingSet);
          }
        });
        setsFilter = setsFilter.concat(sets);
        if(entity.conditionalWorkflows && entity.conditionalWorkflows.length){
          entity.conditionalWorkflows.forEach(function(workflow){
            if(workflow.action == '2'){
              schema.procedures.push(workflow.procedureToExpand)
            }
          });
        }
        return {
          _id : entity._id,
          version : entity.version
        }
      });
      procedures = (schema.procedures || []).map(function (procedure) {
        logger.log('Loop: schema.procedures - Start', 'info', currentFileName);
        var questions = [];
        var sets = [];
        var procFilter = [];
        var schemaIds = [];
        (procedure.conditionalWorkflows || []).forEach(function(workflow){
          if(!workflow.isApp){
            procFilter.push(workflow.procedureToExpand);
          } else {
            schemaIds.push(workflow.appToExpand._id);
          }
        });
        appFilter = appFilter.concat(schemaIds);
        procFilter = procFilter.map(function(proc){
          return {
            _id : proc._id,
            version : proc.version
          }
        });
        proceduresFilter = (proceduresFilter || []).concat(procFilter);
        questions = (procedure.questions || []).map(function (question) {
          logger.log('Loop: procedure.questions - Start', 'info', currentFileName);
          if (question && question.validation && question.validation.validationSet && question.validation.validationSet.existingSet) {
            sets.push(question.validation.validationSet.existingSet);
          }
          return {
            _id : question._id,
            version : question.version
          }
        });
        questionsFilter = (questionsFilter || []).concat(questions);
        setsFilter = (setsFilter || []).concat(sets);
        return {
          _id : procedure._id,
          version : procedure.version
        }
      });

      attributeFilter = (attributeFilter || []).concat(attributes);
      entityFilter = (entityFilter || []).concat(entities);
      proceduresFilter = (proceduresFilter || []).concat(procedures);
    });

    filter['attributeFilter'] = _.uniqWith(attributeFilter, _.isEqual);
    filter['entityFilter'] = _.uniqWith(entityFilter, _.isEqual);
    filter['proceduresFilter'] = _.uniqWith(proceduresFilter, _.isEqual);
    filter['questionsFilter'] = _.uniqWith(questionsFilter, _.isEqual);
    filter['setsFilter'] = _.uniqWith(setsFilter, _.isEqual);
    filter['appFilter'] = _.uniqWith(appFilter, _.isEqual);

    return filter;
  }

  function generateSchemaRelation(schemas) {
    logger.log('Function: generateSchemaRelation - Start', 'info', currentFileName);
    var deferred = q.defer();
    try {
      //var attributeFilter = [];
      //var entityFilter = [];
      //var procedureFilter = [];
      //var questionFilter = [];
      //var setFilter = [];
      //var appFilter = [];
      var schemaRelation = {};
      if (schemas.length) {
        getSchemaDeails(schemas).then(function (schemaDetail) {
          logger.log('Function: getSchemaDeails - Call', 'info', currentFileName);
          schemaRelation['schemas'] = schemaDetail;
          var filter = generateFilter(schemaDetail);
          q.all([
            generateAttributeDetails(filter.attributeFilter),
            generateEntityDetails(filter.entityFilter),
            generateProceduresDetails(filter.proceduresFilter),
            generateQuestionsDetails(filter.questionsFilter),
            generateSetsDetails(filter.setsFilter),
            generateSchemaRelation(filter.appFilter)
          ]).then(function (response) {
            schemaRelation['attributes'] = _.remove(_.uniqWith((response[0] || []).concat(response[5]['attributes']), _.isEqual));
            schemaRelation['entities'] = _.remove(_.uniqWith((response[1] || []).concat(response[5]['entities']), _.isEqual));
            schemaRelation['procedures'] = _.remove(_.uniqWith((response[2] || []).concat(response[5]['procedures']), _.isEqual));
            schemaRelation['questions'] = _.remove(_.uniqWith((response[3] || []).concat(response[5]['questions']), _.isEqual));
            schemaRelation['sets'] = _.remove(_.uniqWith((response[4] || []).concat(response[5]['sets']), _.isEqual));
            schemaRelation['schemas'] = _.remove(_.uniqWith((schemaRelation['schemas'] || []).concat(response[5]['schemas']), _.isEqual));
            deferred.resolve(schemaRelation);
          }).catch(function (err) {
            deferred.reject(err);
          })
        }).catch(function (err) {
          deferred.reject(err);
        });
      } else {
        deferred.resolve(schemaRelation);
      }
    } catch (err) {
      deferred.reject(err);
    }
    return deferred.promise;
  }

  var generateAttributeDetails = function (attributes) {
    logger.log('Function: generateAttributeDetails - Start', 'info', currentFileName);
    var deferred = q.defer();
    try {
      if (attributes.length) {
        var collection = 'attributesAuditLogs';
        var attributesFilter = {};
        attributesFilter['$or'] = (attributes || []).map(function(attribute){
          return {
            identity : db.ObjectID(attribute._id),
            version : attribute.version
          }
        });
        db[collection].find(attributesFilter).toArray(function (err, attributesDetail) {
          if (err) {
            deferred.reject(err);
          } else {
            deferred.resolve(attributesDetail);
          }
        })
      } else {
        var attributesDetail = [];
        deferred.resolve(attributesDetail);
      }
    } catch (err) {
      deferred.reject(err);
    }
    return deferred.promise;
  }

  var generateEntityDetails = function (entities) {
    logger.log('Function: generateEntityDetails - Start', 'info', currentFileName);
    var deferred = q.defer();
    try {
      if (entities.length) {
        var collection = 'entitiesAuditLogs';
        var entitiesFilter = {};
        entitiesFilter['$or'] = (entities || []).map(function(entity){
          return {
            identity : db.ObjectID(entity._id),
            version : entity.version
          }
        });
        db[collection].find(entitiesFilter).toArray(function (err, entitiesDetail) {
          if (err) {
            deferred.reject(err);
          } else {
            deferred.resolve(entitiesDetail);
          }
        })
      } else {
        var entitiesDetail = [];
        deferred.resolve(entitiesDetail);
      }
    } catch (err) {
      deferred.reject(err);
    }
    return deferred.promise;
  }

  var generateProceduresDetails = function (procedures) {
    logger.log('Function: generateProceduresDetails - Start', 'info', currentFileName);
    var deferred = q.defer();
    try {
      if (procedures.length) {
        var collection = 'proceduresAuditLogs';
        var proceduresFilter = {};
        proceduresFilter['$or'] = (procedures || []).map(function(procedure){
          return {
            identity : db.ObjectID(procedure._id),
            version : procedure.version
          }
        });
        db[collection].find(proceduresFilter).toArray(function (err, proceduresDetail) {
          if (err) {
            deferred.reject(err);
          } else {
            deferred.resolve(proceduresDetail);
          }
        })
      } else {
        var proceduresDetail = [];
        deferred.resolve(proceduresDetail);
      }
    } catch (err) {
      deferred.reject(err);
    }
    return deferred.promise;
  }

  var generateQuestionsDetails = function (questions) {
    logger.log('Function: generateQuestionsDetails - Start', 'info', currentFileName);
    var deferred = q.defer();
    try {
      if (questions.length) {
        var collection = 'questionsAuditLogs';
        var questionsFilter = {};
        questionsFilter['$or'] = (questions || []).map(function(question){
          return {
            identity : db.ObjectID(question._id),
            version : question.version
          }
        });
        db[collection].find(questionsFilter).toArray(function (err, questionsDetail) {
          if (err) {
            deferred.reject(err);
          } else {
            deferred.resolve(questionsDetail);
          }
        })
      } else {
        var questionsDetail = [];
        deferred.resolve(questionsDetail);
      }
    } catch (err) {
      deferred.reject(err);
    }
    return deferred.promise;
  }

  var generateSetsDetails = function (sets) {
    logger.log('Function: generateSetsDetails - Start', 'info', currentFileName);
    var deferred = q.defer();
    try {
      if (sets.length) {
        var collection = 'sets';
        var setFilter = {};
        setFilter['$or'] = (sets || []).map(function(set){
          return {
            _id : db.ObjectID(set._id)
          }
        });
        db[collection].find(setFilter).toArray(function (err, setsDetail) {
          if (err) {
            deferred.reject(err);
          } else {
            setsDetail = (setsDetail || []).map(function (set) {
              delete set.versions;
              set.version = 1;
              set.isMajorVersion = true;
              // return _.omit(set, ['createdBy', 'modifiedBy', 'createdByName', 'modifiedByName', 'createdDate', 'modifiedDate']);
              return set;
            });
            deferred.resolve(setsDetail);
          }
        })
      } else {
        var setsDetail = [];
        deferred.resolve(setsDetail);
      }
    } catch (err) {
      deferred.reject(err);
    }
    return deferred.promise;
  }

  var getSchemaDeails = function (schemas) {
    logger.log('Function: getSchemaDeails - Start', 'info', currentFileName);
    var deferred = q.defer();
    try {
      var collection = 'schema';
      var schemaFilter = {};
      schemaFilter['$or'] = (schemas || []).map(function(schema){
        return {
          _id : db.ObjectID(schema)
        }
      });
      db[collection].find(schemaFilter).toArray(function (err, schemaDetail) {
        if (err) {
          deferred.reject(err);
        } else {
          schemaDetail = (schemaDetail || []).map(function (schema) {
            delete schema.versions;
            schema.version = 1;
            schema.isMajorVersion = true;
            // return _.omit(schema, ['workflowreview', 'allicableSites', 'module', 'allowCreaterObj', 'createdBy', 'modifiedBy', 'createdByName', 'modifiedByName', 'createdDate', 'modifiedDate']);
            return schema;
          });
          deferred.resolve(schemaDetail);
        }
      });
    } catch (err) {
      deferred.reject(err);
    }
    return deferred.promise;
  }

  var getDbVersion = function() {
    logger.log('Function: getDbVersion - Start', 'info', currentFileName);
    var deferred = q.defer();
    var collection = 'versions';
    db[collection].findOne(function (err, versions) {
      if (err) {
        deferred.reject(err);
      } else {
        versions = _.pick(versions, 'productVersion', 'web', 'api', 'db', 'report', 'reportApi');
        deferred.resolve(versions);
      }
    })
    return deferred.promise;
  }

  function generateSchemaDetail(options) {
    logger.log("function: generateSchemaDetail - start", 'info', currentFileName);
    try {
      var deferred = q.defer();
      q.all([
        getDbVersion(),
        generateSchemaRelation(options.schemas)
      ]).then(function (instance) {
        options.versions = instance[0];
        options.schemas = instance[1].schemas;
        options.attributes = instance[1].attributes;
        options.entities = instance[1].entities;
        options.procedures = instance[1].procedures;
        options.questions = instance[1].questions;
        options.sets = instance[1].sets;
        deferred.resolve(options);
      }).catch(function (err) {
        deferred.reject(err);
      });
    } catch (err) {
      deferred.reject(err);
    }
    return deferred.promise;
  }


  module.exports = {
    generateSchemaDetail: generateSchemaDetail
  }
})();
