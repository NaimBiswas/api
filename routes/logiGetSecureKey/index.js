'use strict'
var express = require('express');
var http = require('http');
var router = express.Router();

/* GET home page. */
router.get('/logiAppUrl/:logiAppUrl', function(req, res, next) {
  var body = '';
  http.get(req.params.logiAppUrl, function (result) {
    result
    .on("data", function (chunk) {
      body = body + chunk;
    });
    result
    .on('end', function () {
      res.send([{chunk: body}]);
    })
  })
  .on('error', function (e) {
    res.status(401).json([{message: e.message}]);
  });
});

module.exports = router;
