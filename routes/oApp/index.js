/*
author - akashdeep.s - akashdeep.s@productive
here we are bypasig this url to check for the token app and user
*/
;(function(){
  'use strict';
  var express = require('express');
  var moment = require('moment');
  var db = require('../../lib/db');
  var securityProtocols = require('../../lib/securityProtocols/oAppAccessToken');
  var _ = require('lodash');
  var oAppPermission = require('../../lib/oauth/oAppPermission');
  var logger = require('../../logger');
  var currentFileName = __filename;

  module.exports = function (app) {
    var apiRoutes = express.Router();

    apiRoutes.post('/apptoken',function(req,res,next){
      logger.log("API Called : api/oApp/apptoken :POST method" , 'info', currentFileName);
      var appId = req.body.app._id;
      var userId = req.body.user._id;
      if (!appId || !userId) {
        logger.log("if(!appId || !userId) :Bad Request - failed" , 'info', currentFileName);
        res.status(400).json({error: 'Bad Request.'})
      } else {
        logger.log("if(!appId || !userId) :Bad Request - success" , 'info', currentFileName);
        var token = securityProtocols.generateOAppToken({appId: appId, userId: userId});
        res.status(200).json(token);
      }
    });

    apiRoutes.post('/decode',function(req,res,next){
      logger.log("API Called : api/oApp/decode :POST method" , 'info', currentFileName);

      var token = req.body.token;
      var json = securityProtocols.decodeOAppToken(token);
      if (json && json.userId && json.appId) {
        delete json.iat;
        logger.log("if(json && json.userId && json.appId) :Bad Request - failed" , 'info', currentFileName);
        res.status(200).json(json);
      }else {
        logger.log("if(json && json.userId && json.appId) :success" , 'info', currentFileName);
        res.status(400).json({error: 'Bad Request.'});
      }
    });

    //restricting it to post so, that this can not be directly hit by url.
    apiRoutes.post('/verify',function(req,res,next){
      logger.log("API Called : api/oApp/verify :POST method" , 'info', currentFileName);
      
      var email = req.body.email;
      var token = req.body.token;
      var json = securityProtocols.decodeOAppToken(token);
      var appId = json.appId;
      var userId = json.userId;
      var filter = {'$or':[{'user._id' : json.userId},{'createdBy': db.ObjectID(json.userId)}], 'app._id' : json.appId, token : token};
      if (email) {
        filter['emails.email'] = email;
      }

    db.generalSettings.findOne({}, {'_id':0,'siteLogo':1,'supportContactDetails':1}, function(err, generalSettings){
      db.appAccessConfig.findOne(filter,function(err,conf){
        if(err){
          logger.log("collection: db.appAccessConfig - failed : " + err , 'error', currentFileName);
          return res.status(400).json({error : err.message, generalSettings: generalSettings});
        }
        if(!conf || !conf.isActive){
          logger.log("collection: db.appAccessConfig: conf.isActive - you are not authorize to access this app." , 'info', currentFileName);
          res.status(401).json({error : 'you are not authorize to access this app.', generalSettings: generalSettings});
        }
        else{
          //Validate expiration of date for less than today
          if (conf.expireDate && moment().isAfter(conf.expireDate,'day')) {
            logger.log("collection: db.appAccessConfig: conf.expireDate - You are not allow to submit more entries." , 'error', currentFileName);
            res.status(400).json({error: 'App submitting date is expired, You are not allow to submit more entries.', generalSettings: generalSettings});
          } else {
            var totalAllowed = _.sumBy(((_.filter(conf.emails||[],function (m) {
              return m.linkShared && !m.isDeleted;
            }))||[]), 'allowed')|| conf.noOfTimeAllowed;
            totalAllowed = totalAllowed>=conf.noOfTimeAllowed?totalAllowed :conf.noOfTimeAllowed;
            var mail = _.find((conf.emails||[]),{email: email});

            if (mail && (mail.isDeleted || mail.linkShared==false)) {
              logger.log("if (mail && (mail.isDeleted || mail.linkShared==false)) :Not a valid Email, You are not allow to submit more entries." , 'error', currentFileName);
              res.status(400).json({error: 'Not a valid Email, You are not allow to submit more entries.', generalSettings: generalSettings});
            }
            else if (mail && ((conf.isForPerEmail && mail.used>=mail.allowed) || !conf.isForPerEmail && conf.noOfTimeUsed >= conf.noOfTimeAllowed)) {
              logger.log("if (mail && ((conf.isForPerEmail && mail.used>=mail.allowed) || !conf.isForPerEmail && conf.noOfTimeUsed >= conf.noOfTimeAllowed)) :App submitting limit is crossed, You are not allow to submit more entries." , 'info', currentFileName);
              res.status(400).json({error: 'App submitting limit is crossed, You are not allow to submit more entries.', generalSettings: generalSettings});
            }else {
              if (!mail && email && (((!email || !conf.isForPerEmail) && conf.noOfTimeUsed >= conf.noOfTimeAllowed)||((!email || conf.isForPerEmail)&& conf.noOfTimeUsed >= totalAllowed))) {
                logger.log("if (!mail && email && (((!email || !conf.isForPerEmail) && conf.noOfTimeUsed >= conf.noOfTimeAllowed)||((!email || conf.isForPerEmail)&& conf.noOfTimeUsed >= totalAllowed))) :App submitting limit is crossed, You are not allow to submit more entries." , 'info', currentFileName);
                res.status(400).json({error : 'App submitting limit is crossed, You are not allow to submit more entries.', generalSettings: generalSettings});
              } else if (!mail && (((!email || !conf.isForPerEmail) && conf.noOfTimeUsed >= conf.noOfTimeAllowed)||((!email || conf.isForPerEmail)&& conf.noOfTimeUsed >= totalAllowed))) {
                logger.log("if (!mail && (((!email || !conf.isForPerEmail) && conf.noOfTimeUsed >= conf.noOfTimeAllowed)||((!email || conf.isForPerEmail)&& conf.noOfTimeUsed >= totalAllowed))) :App submitting limit is crossed, You are not allow to submit more entries." , 'info', currentFileName);
                res.status(400).json({error : 'App submitting limit is crossed, You are not allow to submit more entries.', generalSettings: generalSettings});
              } else {
                db.appAccessConfig.findOneAndUpdate({
                  _id: conf._id
                },{
                  '$push': {
                    'tried': {'xAgent': req.ip}
                  }
                },function (err, data) {
                  // if(err){
                  //   cb(err);
                  //   return;
                  // }
                  // cb(null, data);
                });
                //send userdetails nad permission, so build permission here.
                //get schema and user
                //q all is not working so alternate way
                db.schema.findOne({ _id: db.ObjectID(appId) }, function (err, sch) {
                  if (err) {
                    logger.log("collection: db.schema: failed : " + err , 'error', currentFileName);
                    next(err);
                    return;
                  }
                  if (!sch || !sch.isActive) {
                    next({ error: 'No App found' });
                    return;
                  }
                  db.users.findOne({ _id: db.ObjectID(userId)}, function (err,usr) {
                    if (err) {
                      logger.log("collection: db.users: failed : " + err , 'error', currentFileName);
                      next(err);
                      return;
                    }
                    if (!usr || !sch.isActive) {
                      next({ error: 'No User found' });
                      return;
                    }
                    oAppPermission.buildPermission(usr, sch);
                    //we can not send all user info, limit the information here

                    conf.user = _.pick(usr,['username','permissions']);
                    res.json(conf);
                  });
                });
              }

            }

          }

        }
      });
    });
    });

    app.use('/oapp/',apiRoutes);
  }

})();
