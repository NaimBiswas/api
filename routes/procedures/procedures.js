'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var linkedTbl = require('../../lib/linkedTbl');
var _ = require('lodash');
var db = require('../../lib/db');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'procedures',
    collection: 'procedures',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    POST: {
      ONE: {
        after: afterPost
      }
    },
    PATCH: {
      ONE: {
        after: afterPatch
      }
    }
  }

});
/**
 * @swagger
 * definition:
 *   Procedure:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       isActive:
 *         type: boolean
 *       isAuditing:
 *         type: boolean
 *       tag:
 *           type: array
 *           items:
 *              type: string
 *       questions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Question'
 *       instructionURL:
 *         type: string
 *       createdDate:
 *         type: dateTime
 *       modifiedDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       createdBy:
 *         $ref: '#/definitions/CreatedBy'
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       version:
 *         type: integer
 *       isMajorVersion:
 *         type: boolean
 */

/**
 * @swagger
 * /procedures:
 *   get:
 *     tags: [Procedure]
 *     description: Returns all procedures
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of procedure that you want to see in the output (Exp Values -> [ index,title,modifiedDate,modifiedBy,version,versions,isActive,isAuditing])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of procedure with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of procedures
 *         schema:
 *           $ref: '#/definitions/Procedure'
 */

/**
 * @swagger
 * /procedures/{id}:
 *   get:
 *     tags: [Procedure]
 *     description: Returns a object procedure
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Procedure's id
 *         in: path
 *         required: true
 *         type: integer
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An object of procedure
 *         schema:
 *           $ref: '#/definitions/Procedure'

 */

/**
* @swagger
* /procedures:
*   post:
*     tags: [Procedure]
*     description: Creates a new procedure
*     produces:
*       - application/json
*     parameters:
*       - name: procedure
*         description: Procedure object
*         in: body
*         required: true
*         schema:
*           $ref: '#/definitions/Procedure'
*     responses:
*       201:
*         description: Successfully created
*/

/**
 * @swagger
 * /procedures/{id}:
 *   patch:
 *     tags: [Procedure]
 *     description: Updates a Procedure
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: procedure's id
 *         in: path
 *         required: true
 *         type: integer
 *       - name: procedure
 *         description: procedure's object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/Procedure'
 *     responses:
 *       200:
 *         description: Successfully updated
 */

function afterPost(req, res, next) {
  linkedTbl.insertInLinked('procedures', req.data);
  res.status(200).json(req.data);
}

function afterPatch(req, res, next) {
  linkedTbl.insertInLinked('procedures', req.data);
  res.status(200).json(req.data);
}

module.exports = route.router;
