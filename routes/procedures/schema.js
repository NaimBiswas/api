'use strict'
/**
 * @name procedures-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var anyArray = types.anyArray;
var rBool = types.rBool;
var bool = types.bool;
var number = types.number;
var string = types.string;
var required = types.required;
var stringArray = types.stringArray;
var any = types.any;
var rBigString = types.bigString;
var bigString = types.bigString;
var extraLargeString = types.extraLargeString;
var rExtraLargeString = types.rExtraLargeString;

var schema = {
  title: rBigString.label('Procedure title'),
  tags: anyArray.label('Tags string array'),
  isActive: rBool.label('Active?'),
  questions: array(object({
    _id: rId.label('Question id'),
    title: rExtraLargeString.label('Questions title'),
    addBarcodeRule: bool.label('Add Barcode Rule'),
    checkValidCriteria: bool.label('Check Validity For Acceptance Criteria'),
    addBarcodeRulePrefix: any.label('Add Barcode Rule Prefix').allow(''),
    addBarcodeRuleSuffix: any.label('Add Barcode Rule Suffix').allow(''),
    version: number.label('Question version'),
    comboId: string.label('Question Combo Id'),
    's#': string.label('Sequence number'),
    isoptionalquestion: bool.label('isoptionalquestion'),
    type: object({
      _id: rId.label('Type id'),
      title: rString.label('Type title'),
      format: object({
        _id: rId.label('Format id'),
        title: rString.label('Format title'),
        metadata: any.label('Metadata required for the format')
      })
    }).required().label('Type object'),
    validation: any.label('Validation array'),
    isInstructionForUser: bool.label('Is Instruction for user'),
    instructionForUser: any.label('Instruction for user').allow(''),
    criteria: any
  })).required().label('Questions array of object'),
  isInstructionForUser: bool.label('Is Instruction for user'),
  instructionURL: any.label('Schema instructionURL'),
  file: any.label('Schema upload file'),
  dmsfile : any.label('Schema upload file'),
  instructionForUser: any.label('Instruction for user').allow(''),
  referencedQuestions: any.label("referenced Questions"),
  isAuditing: bool.label("Is Auditing ?"),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
