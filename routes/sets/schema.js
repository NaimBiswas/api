'use strict'
/**
 * @name sets-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var array = types.array;
var rString = types.rString;
var rBool = types.rBool;
var rSmallString = types.rSmallString;
var any=types.any;
var string = types.string;

var schema = {
  title: rString.label('Sets title'),
  isActive: rBool.label('Sets active?'),
  values: array(object({
    value: rSmallString.label('Values value'),
    title: rSmallString.label('Values title'),
    isActive: rBool.label('Values active?'),
     isDefaultValue:any.label('Values isDefaultValue?')
  })).required().label('Values object'),
  timeZone: string.label('Time Zone')

};

module.exports = schema;
