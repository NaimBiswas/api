'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'sets',
    collection: 'sets',
    schema: schema,
    softSchema: softSchema
  }
});

/**
 * @swagger
 * definition:
 *   Value:
 *     properties:
 *       title:
 *         type: string
 *       value:
 *         type: string
 *       isActive:
 *         type: boolean
 */

/**
 * @swagger
 * definition:
 *   Set:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       isActive:
 *         type: boolean
 *       modifiedDate:
 *         type: dateTime
 *       createdDate:
 *         type: dateTime
 *       createdByName:
 *         type: string
 *       modifiedByName:
 *         type: string
 *       modifiedBy:
 *         type: integer
 *       createdBy:
 *         type: integer
 *       version:
 *         type: integer
 *       isMajorVersion:
 *         type: boolean
 *       versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *       values:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Value'
 */


/**
 * @swagger
 * /sets:
 *   get:
 *     tags: [Set]
 *     description: Returns all sets
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,title,modifiedDate,modifiedBy,version,versions,isActive])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: An array of sets
 *         schema:
 *           $ref: '#/definitions/Set'
 */

 /**
  * @swagger
  * /sets/{id}:
  *   get:
  *     tags: [Set]
  *     description: Returns a object set
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: Set's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: pagesize
  *         description: Page Size
  *         in: query
  *         required: false
  *         type: string
  *     responses:
  *       200:
  *         description: An object of set
  *         schema:
  *           $ref: '#/definitions/Set'
  */

 /**
  * @swagger
  * /sets/{id}:
  *   patch:
  *     tags: [Set]
  *     description: Updates a set
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: Set's id
  *         in: path
  *         required: true
  *         type: integer
  *       - name: set
  *         description: set object
  *         in: body
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Set'
  *     responses:
  *       200:
  *         description: Successfully updated
  */

  /**
   * @swagger
   * /sets:
   *   post:
   *     tags: [Set]
   *     description: Creates a new set
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: sets
   *         description: Set object
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/Set'
   *     responses:
   *       201:
   *         description: Successfully created
   */
  //Chandni : Code end for Swagger

module.exports = route.router;
