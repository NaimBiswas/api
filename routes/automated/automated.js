'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var db = require('../../lib/resources').db;
var moment = require('moment');
//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'automated',
    collection: 'automated',
    schema: schema,
    softSchema: softSchema
  },
  types:{
    POST:{
      ONE: {
        after : afterPost
      }
    },
    PATCH:{
      ONE: {
        after : afterPatch
      }
    }
  }
});
var debug = route.debug;

function afterPatch(req, res, next) {
  //req.data contains the posted / patched
  //remove existing data from calendar for
  //req.data and
  //add new requested data
  if(req.data.importFile !== undefined && req.data.importFile === 'On Defined Schedule') {
    db
    .calendarAutomated
    .removeManyAsync({scheduleId: req.data._id})
    .then(function (data) {
      var calandarEntries = generateEntries(req.data);
      (req.status ? res.status(req.status) : res).json(req.data);
      storeEntries(calandarEntries);
    });
  } else {
    (req.status ? res.status(req.status) : res).json(req.data);
  }
}

function storeEntries(entries) {
  db
  .calendarAutomated
  .insertManyAsync(entries)
  .then(function (data) {
    debug('Calander entries generated and stored sucessfully!');
  })
  .catch(function (err) {
    debug('Failed to store generated calander entries!')
    debug(err);
  });
}

function afterPost(req, res, next) {
  //req.data contains the posted / patched
  //schedule object with createdBy and modifiedBy
  //And all, orignal object can be accessed from
  //req.items

  if(req.data.importFile !== undefined && req.data.importFile === 'On Defined Schedule') {
    storeEntries(generateEntries(req.data));
  }
  res.status(req.status).json(req.data);
  // res.json(calandarEntries);
}

function generateEntries(schedule) {
  var calandarEntries = [];
  var startDate = new Date(schedule.startDate);
  var endDate = new Date();
  var totaldays;
  var days;
  if(schedule.occurence == 0){
    endDate = new Date(schedule.endDate);
  }else {
    var firstDate = new Date(startDate);
    if(schedule.frequency.type === 'Daily'){
      days = schedule.frequency.daily.repeats.every;
      totaldays = (days * schedule.occurence)-1;
      endDate = new Date(firstDate.setDate(firstDate.getDate() + parseInt(totaldays)));
    }
    if(schedule.frequency.type === 'Weekly'){
      var week = schedule.frequency.weekly.repeats.every * 7;
      totaldays = (week * schedule.occurence)-1;
      endDate = new Date(firstDate.setDate(firstDate.getDate() + parseInt(totaldays)));
    }
    if(schedule.frequency.type === 'Monthly'){
      var newDate = new Date(startDate);
      var month;
      if(schedule.frequency.subType === 'EveryMonthly'){
        month = schedule.frequency.monthly.repeats.dayOfMonth.month;
      }else if(schedule.frequency.subType === 'WeeklyMonthly'){
        month = schedule.frequency.monthly.repeats.dayOfWeek.month;
      }
      totaldays = 0;
      var totalMonths = (month) * schedule.occurence;
      for (var k = 0; k < totalMonths; k++) {
        var lastDate = new Date(newDate.getFullYear(),newDate.getMonth() + 1, 0);
        totaldays =  totaldays + lastDate.getDate();
        newDate = new Date(newDate.setMonth(newDate.getMonth() + 1));
      }
      endDate = new Date(firstDate.setDate(firstDate.getDate() + parseInt(totaldays)));
    }
  }
  var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
  var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) ;
  if(schedule.frequency.type === 'Daily'){
    days = schedule.frequency.daily.repeats.every;
    var filteredDays = (parseInt(diffDays) / parseInt(days)) + 1;
    var dateDaily = new Date(schedule.startDate);
    for(var day = 0 ; day < parseInt(filteredDays)  ; day ++)
    {
      if(day == 0 && schedule.frequency.daily.repeats.every == 1){
        calendardetails(dateDaily);
      }
      else{
        var calendarDateDaily = dateDaily.setDate(dateDaily.getDate() + parseInt(days));
        var date  = new Date(calendarDateDaily);
        calendardetails(date);
      }
      // if(day = 0){
      //   calendardetails(new Date(startDate);
      // }
      // else {
      //
      // }

    }

  }
  else if(schedule.frequency.type === 'Weekly')
  {
    var skipDays = (parseInt(schedule.frequency.weekly.repeats.every) - 1) * 7 ;
    var currentDate = new Date(schedule.startDate);
    var skipDate = new Date(startDate.setDate(startDate.getDate() + parseInt(skipDays)));
    for (var weekly = 0 ; weekly < diffDays + 1 ; weekly ++){
      var weekday = currentDate.getDay();
      var a = new Date(skipDate);
      var addincalendarDate = new Date(a.setDate(a.getDate() + parseInt(7)));
      if(calandarEntries.length < schedule.occurence || schedule.occurence == 0){
        if(currentDate >= skipDate && currentDate < addincalendarDate){
          for(var i = 0 ; i < schedule.frequency.weekly.repeats.days.length ; i++){
            if(schedule.frequency.weekly.repeats.days[i] == "Mon"){
              if(weekday == 1){
                date  = currentDate;
                calendardetails(date);
              }
            }
            if(schedule.frequency.weekly.repeats.days[i] == "Tue"){
              if(weekday == 2){
                date  = currentDate;
                calendardetails(date);
              }
            }
            if(schedule.frequency.weekly.repeats.days[i] == "Wed"){
              if(weekday == 3){
                date  = currentDate;
                calendardetails(date);
              }
            }
            if(schedule.frequency.weekly.repeats.days[i] == "Thu"){
              if(weekday == 4){
                date  = currentDate;
                calendardetails(date);
              }
            }
            if(schedule.frequency.weekly.repeats.days[i] == "Fri"){
              if(weekday == 5){
                date  = currentDate;
                calendardetails(date);
              }
            }
            if(schedule.frequency.weekly.repeats.days[i] == "Sat"){
              if(weekday == 6){
                date  = currentDate;
                calendardetails(date);
              }
            }
            if(schedule.frequency.weekly.repeats.days[i] == "Sun"){
              if(weekday == 0){
                date  = currentDate;
                calendardetails(date);
              }
            }
          }
        }
      }
      var date1 = new Date(addincalendarDate).setHours(0,0,0,0);
      var currentdate = new Date(currentDate).setHours(0,0,0,0);

      if(date1 === currentdate){
        skipDate = new Date(skipDate.setDate(skipDate.getDate() + parseInt(skipDays) + parseInt(7)));
        diffDays++;
        continue;
      }
      currentDate = new Date(currentDate.setDate(currentDate.getDate() + 1));
    }
  }

  else if(schedule.frequency.type === 'Monthly')
  {
    var  totalmonths = (endDate.getFullYear() - startDate.getFullYear()) * 12;
    totalmonths -= startDate.getMonth() ;
    totalmonths += endDate.getMonth() + 1;
    var startYear = startDate.getFullYear();

    if(schedule.frequency.subType === 'EveryMonthly')
    {
      var repeattotalmonth = parseInt(totalmonths) / parseInt(schedule.frequency.monthly.repeats.dayOfMonth.month);
      var skipMonth = (parseInt(schedule.frequency.monthly.repeats.dayOfMonth.month) - 1);
      var firstmonth = startDate.getMonth() + parseInt(skipMonth);
      for(var ct = 1 ; ct <= repeattotalmonth ; ct ++)
      {
        var totalMonthDays = new Date(startYear,firstmonth,0).getDate();
        for(var d = 1; d <= totalMonthDays; d++ )
        {
          if(d === parseInt(schedule.frequency.monthly.repeats.dayOfMonth.day))
          {
            date  = new Date(startYear,firstmonth,d);

            if(  date > startDate && date < endDate)
            {
              calendardetails(date);
            }
          }
        }

        firstmonth = firstmonth + (skipMonth + 1);
      }
    }
    else if(schedule.frequency.subType === 'WeeklyMonthly')
    {

      if(schedule.frequency.monthly.repeats.dayOfWeek.every === 'first'
      || schedule.frequency.monthly.repeats.dayOfWeek.every === 'second'
      || schedule.frequency.monthly.repeats.dayOfWeek.every === 'third'
      || schedule.frequency.monthly.repeats.dayOfWeek.every === 'fourth' )
      {

        setWeeklyMonthly();
      }

    }

  }

  function  setWeeklyMonthly(){

    // var startDate = new Date(schedule.startDate);
    // var endDate = new Date(schedule.endDate);
    var  totalmonths = (endDate.getFullYear() - startDate.getFullYear()) * 12;
    totalmonths -= startDate.getMonth() ;
    totalmonths += endDate.getMonth() + 1;
    var startYear = startDate.getFullYear();
    var repeatmonthWeekly = parseInt(totalmonths) / parseInt(schedule.frequency.monthly.repeats.dayOfWeek.month);
    var skipWeeklyMonth = (parseInt(schedule.frequency.monthly.repeats.dayOfWeek.month) - 1);
    var firstmonthWeekly = (startDate.getMonth()) + parseInt(skipWeeklyMonth);
    for(var ct = 1 ; ct <= repeatmonthWeekly ; ct ++)
    {
      var firstDate = new Date();
      var lastDate = new Date();
      if(schedule.frequency.monthly.repeats.dayOfWeek.every === 'first')
      {
        firstDate = new Date(startYear,firstmonthWeekly,1);
        lastDate = new Date(startYear,firstmonthWeekly,7);
      }
      else if(schedule.frequency.monthly.repeats.dayOfWeek.every === 'second')
      {
        firstDate = new Date(startYear,firstmonthWeekly,8);
        lastDate = new Date(startYear,firstmonthWeekly,14);
      }
      else if(schedule.frequency.monthly.repeats.dayOfWeek.every === 'third')
      {
        firstDate = new Date(startYear,firstmonthWeekly,15);
        lastDate = new Date(startYear,firstmonthWeekly,21);
      }
      else if(schedule.frequency.monthly.repeats.dayOfWeek.every === 'fourth')
      {
        firstDate = new Date(startYear,firstmonthWeekly,22);
        lastDate = new Date(startYear,firstmonthWeekly,28);
      }
      for(var idate = firstDate; idate <= lastDate; idate.setDate(idate.getDate() + 1))
      {
        var day = idate.getDay();
        if(schedule.frequency.monthly.repeats.dayOfWeek.day === 'monday' && day === 1
        || schedule.frequency.monthly.repeats.dayOfWeek.day === 'tuesday' && day === 2
        || schedule.frequency.monthly.repeats.dayOfWeek.day === 'wednesday' && day === 3
        || schedule.frequency.monthly.repeats.dayOfWeek.day === 'thursday' && day === 4
        || schedule.frequency.monthly.repeats.dayOfWeek.day === 'friday' && day === 5
        || schedule.frequency.monthly.repeats.dayOfWeek.day === 'saturday' && day === 6
        || schedule.frequency.monthly.repeats.dayOfWeek.day === 'sunday' && day === 0)
        {
          var  dateMonthly  =  new Date(idate);
          calendardetails(dateMonthly);
        }

      }

      firstmonthWeekly = firstmonthWeekly + (skipWeeklyMonth + 1);

    }
  }

  function calendardetails(calendarDate) {
    var calendarobjDate = new Date(calendarDate);
    var calendarobj ={};

    calendarobj.title = 'Automated Import Schedule';
    //calendarobj.schema._id = schedule.schema._id;
    //calendarobj.schema.title = schedule.schema.title;
    calendarobj.scheduledBy = schedule.scheduledBy || schedule.createdBy;
    calendarobj.type = schedule.frequency.type;
    calendarobj.time = schedule.time;
    // var timearray = calendarobj.time.split(/[ :.]+/);
    var time = new Date(calendarobj.time);
    var h;
    var m;
    h = time.getHours();
    m = time.getMinutes();
    //
    // if(timearray[2] === "AM")
    // {
    //   if(parseInt(timearray[0]) === 12)
    //   {
    //     timearray[0] = 0;
    //   }
    //   h = parseInt(timearray[0]);
    //   m = parseInt(timearray[1]);
    // }
    // if(timearray[2] === "PM" )
    // {
    //   if(parseInt(timearray[0]) === 12)
    //   {
    //     timearray[0] = 0;
    //   }
    //   h = parseInt(timearray[0]) + 12;
    //   m =  parseInt(timearray[1]);
    // }
    // var a = new Date(calendarobjDate).setHours(h,m,0,0);
    // console.log(new Date(a));
    // // currentDate = new Date(currentDate.setDate(currentDate.getDate() + 1));

    // calendarobj.date  = new Date(calendarDate).setHours(h,m,0,0);
    var caldate = new Date(calendarobjDate).setHours( h, m, 0);
    calendarobj.date = moment(caldate).toDate();
    calendarobj.email = schedule.email || '';
    calendarobj.notified = false;
    calendarobj.scheduleId = schedule._id.toString();
    calandarEntries.push(calendarobj);
  }
  return calandarEntries;
}

module.exports = route.router;
