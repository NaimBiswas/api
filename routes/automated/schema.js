'use strict'
/**
 * @name schedules-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var rBool = types.rBool;
var rDate = types.rDate;
var date = types.date;
var string = types.string;
var required = types.required;
var forbidden = types.forbidden;
var stringArray = types.stringArray;
var rNumber = types.rNumber;
var number = types.number;

var schema = {

    module: object({
        _id: rId.label('Module id'),
        title: rString.label('Module title')
    }).required().label('Module object'),
    schema: object({
        _id: rId.label('Schema id'),
        title: rString.label('Schema title'),
        version : number.label('Schema version'),
    }).required().label('Schema object'),
    mapping: object({
        _id: rId.label('Mapping id'),
        title: rString.label('Mapping title')
    }).required().label('Mapping object'),
  importFile: rString.label('Import File'),
  frequency: object({
    type: rString.label('Frequency type'),
    subType: rString.label('SubType type'),
    daily: object({
      repeats: object({
        every: number.label('Daily Every')
        .when('subType', {is:'Every', then:required()})
        .when('subType', {is:'EveryBusinessDay', then:forbidden()}),
        everyBusinessDay: string.label('Every Business Day')
        .when('subType', {is:'EveryBusinessDay', then:required()})
        .when('subType', {is:'Every', then:forbidden()})
      })
    }).when('type', {is:'Daily', then:required(), otherwise: forbidden()}).label('Daily object'),
    weekly: object({
      repeats: object({
        every: rNumber.label('Weekly Every'),
        days: stringArray.label('Weekly days')
      })
    }).when('type', {is:'Weekly', then:required(), otherwise: forbidden()}).label('Weekly object'),
    monthly: object({
      repeats: object({
        dayOfMonth: object({
          day: rNumber.label('DayOfMonth Day'),
          month: rNumber.label('DayOfMonth Month'),
        }).when('subType', {is:'EveryMonthly', then:required()}).label('DayOfMonth object'),
        dayOfWeek: object({
          every: rString.label('DayOfWeek Every'),
          day: rString.label('DayOfWeek Day'),
          month: rNumber.label('DayOfWeek Month'),
        }).when('subType', {is:'WeeklyMonthly', then:required()}).label('DayOfWeek object')
      })
    }).when('type', {is:'Monthly', then:required(), otherwise: forbidden()}).label('Monthly object')
  }).when('importFile', {is:'On Defined Schedule', then:required(), otherwise: forbidden()}).label('Frequency object'),
  startDate: rDate.label('Start date').when('importFile', {is:'On Defined Schedule', then:required(), otherwise: forbidden()}),
  occurenceState : rBool.label('occurenceState').when('importFile', {is:'On Defined Schedule', then:required(), otherwise: forbidden()}),
  endDate: date.label('End date').when('importFile', {is:'On Defined Schedule', then:required(), otherwise: forbidden()})
  .when('occurenceState',  {is:'false',then: required()}).when('importFile', {is:'On Defined Schedule', then:required(), otherwise: forbidden()}),
  occurence: number.label('Occurence').when('importFile', {is:'On Defined Schedule', then:required(), otherwise: forbidden()})
  .when('occurenceState',  {is:'true',then: required()}).when('importFile', {is:'On Defined Schedule', then:required(), otherwise: forbidden()}),
  time: string.label('Schedule time').when('importFile', {is:'On Defined Schedule', then:required(), otherwise: forbidden()}),
  reminderEmail: rBool.label('Reminder'),
  email: string.label('Email').email(),
  isActive: rBool.label('Active?'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
