'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var settings = require('../../lib/settings');
var config = require('../../lib/config');
var logger = require('../../logger');
var currentFileName = __filename;

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'generalSettings',
    collection: 'generalSettings',
    schema: schema,
    softSchema: softSchema
  },
  types:{
    /**
     * @swagger
     * definition:
     *   GeneralSettings:
     *     properties:
     *       id:
     *         type: integer
     *       siteTitle:
     *         type: string
     *       tagline:
     *         type: string
     *       siteLogo:
     *         type: string
     *       passwordAgingLimit:
     *         type: integer
     *       passwordAgingMessage:
     *         type: string
     *       reUsedPasswordsLimit:
     *         type: integer
     *       reUsedPasswordsMessage:
     *         type: string
     *       failedLoginAttemptsIsTrue                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      :
     *         type: boolean
     *       failedLoginAttemptsLimit:
     *         type: integer
     *       failedLoginAttemptsLimitMessage:
     *         type: string
     *       hideProcTitle                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      :
     *         type: boolean
     *       displayNullValues                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      :
     *         type: boolean
     *       modifiedDate:
     *         type: dateTime
     *       createdDate:
     *         type: dateTime
     *       createdByName:
     *         type: string
     *       modifiedByName:
     *         type: string
     *       modifiedBy:
     *         $ref: '#/definitions/ModifiedBy'
     *       createdBy:
     *         $ref: '#/definitions/CreatedBy'
     *       versions:
     *           type: array
     *           items:
     *              type: object
     *              allOf:
     *              - $ref: '#/definitions/Version'
     *       version:
     *           type: integer
     *       isMajorVersion:
     *           type: boolean
     *       supportContactDetails:
     *         type: string
     *       showHideCriteriaDetails:
     *           type: boolean
     */

    /**
     * @swagger
     * /generalSettings:
     *   get:
     *     tags: [General Settings]
     *     description: Returns all generalSettings
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: pagesize
     *         description: Page Size
     *         in: query
     *         required: false
     *         type: string
     *     responses:
     *       200:
     *         description: An array of generalSettings
     *         schema:
     *           $ref: '#/definitions/GeneralSettings'

     */

     /**
      * @swagger
      * /generalSettings/{id}:
      *   get:
      *     tags: [General Settings]
      *     description: Returns a object generalSettings
      *     produces:
      *       - application/json
      *     parameters:
      *       - name: id
      *         description: Questtion's id
      *         in: path
      *         required: true
      *         type: integer
      *       - name: pagesize
      *         description: Page Size
      *         in: query
      *         required: false
      *         type: string
      *     responses:
      *       200:
      *         description: An object of generalSettings
      *         schema:
      *           $ref: '#/definitions/GeneralSettings'

      */
      /**
       * @swagger
       * /generalSettings/{id}:
       *   patch:
       *     tags: [General Settings]
       *     description: Updates a generalSettings
       *     produces:
       *       - application/json
       *     parameters:
       *       - name: id
       *         description: generalSettings's id
       *         in: path
       *         required: true
       *         type: integer
       *       - name: generalSettings
       *         description: generalSettings object
       *         in: body
       *         required: true
       *         schema:
       *           $ref: '#/definitions/GeneralSettings'
       *     responses:
       *       200:
       *         description: Successfully updated
       */
    POST:{
      ONE: {
        before : beforeUpdateSettings,
        after  : updateSettings
      },
      ONESOFT: {
        before: beforeUpdateSettings,
        after  : updateSettings
      }
    },
    PATCH:{
      ONE: {
        before : beforeUpdateSettings,
        after  : updateSettings
      },
      ONESOFT: {
        before: beforeUpdateSettings,
        after  : updateSettings
      }
    }
  }
});

function beforeUpdateSettings(req, res, next) {
  logger.log("function beforeUpdateSettings(req, res, next) :Called" , 'info', currentFileName);

  next();
}

//QC3-4940
//by surjeet.b
//for updating support info
function updateSettings(req, res, next) {
  logger.log("function updateSettings(req, res, next) :Called" , 'info', currentFileName);
  if (settings && settings.default) {
    if (req.body.supportContactDetails != '' && req.body.supportContactDetails != null && req.body.supportContactDetails != undefined) {
      settings.default.support = req.body.supportContactDetails;
    }else {
      settings.default.support = config.config().support;
    }
  }
  res.send({success: true});
}

module.exports = route.router;
