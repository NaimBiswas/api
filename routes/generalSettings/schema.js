'use strict'
/**
 * @name generalSettings-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */

var types = require('../../lib/validator/types');

var rBool = types.rBool;
var bool = types.bool;
var rBigString = types.rBigString;
var bigString = types.bigString;
var rNumber = types.rNumber;
var number = types.number;
var required = types.required;
var string = types.string;
var any = types.any;
//Start - Lokesh Boran : QC3-5095
var mediumLargeString = types.mediumLargeString;
//End - Lokesh Boran : QC3-5095

//QC3-4968
//solved by roshni.b
//type extraLargeString is added for supportContactDetails

//QC3-5095
//solved by roshni.b
//now '' (blank data) allowed for supportContactDetails field
var schema = {
  siteTitle: rBigString.label('siteTitle'),
  tagline: rBigString.label('tagline'),
  fileattachmentsettings: bigString.label('File Attachment Settings').allow(null,""),
  siteLogo: any.label('siteLogo'),
  //Start - Lokesh Boran : QC3-5095
  supportContactDetails: mediumLargeString.label('supportContactDetails'),
  //End - Lokesh Boran : QC3-5095
  passwordAgingLimit: number.label('passwordAgingLimit'),
  passwordAgingMessage: bigString.label('passwordAgingMessage'),
  reUsedPasswordsLimit: number.label('reUsedPasswordsLimit'),
  reUsedPasswordsMessage: bigString.label('reUsedPasswordsMessage'),
  // failedLoginAttemptsIsTrue: rBool.label('failedLoginAttemptsIsTrue'),
  failedLoginAttemptsLimit: number.allow(null).default(0).label('failedLoginAttemptsLimit'),
  // .when('failedLoginAttemptsIsTrue', {
  //   is: true,
  //   then: required()
  // }),
  failedLoginAttemptsLimitMessage: bigString.label('failedLoginAttemptsLimitMessage'),
  hideProcTitle:bool.label('hideProcTitle'),
  showHideCriteriaDetails:bool.label('Show Acceptance Criteria Icon in the App'),
  displayNullValues:bool.label('displayNullValues'),
  // .when('failedLoginAttemptsIsTrue', {
  //   is: true,
  //   then: required()
  // })
  emailAlertForAssignment: bool.label('emailAlertForAssignment'),
  allowPrintSites:bool.label('allowPrintSites'),


  // by miral QC3-9174
  minAttributeEntity:number.label('Minimum entity or attributes required'),
  closeWindowDay: number.allow(null).label('Close day of Window'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
