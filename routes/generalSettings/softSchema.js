'use strict'
/**
 * @name generalSettings-softSchema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

//var bool = types.bool;
var bigString = types.bigString;
var number = types.number;
var required = types.required;
var any = types.any;
var rBool = types.rBool;
var bool = types.bool;
var any = types.any;
var string = types.string;
//Start - Lokesh Boran : QC3-5095
var mediumLargeString = types.mediumLargeString;
//End - Lokesh Boran : QC3-5095

//QC3-4968
//solved by roshni.b
//type extraLargeString is added for supportContactDetails

//QC3-5095
//solved by roshni.b
//now '' (blank data) allowed for supportContactDetails field
var schema = {
  siteTitle: bigString.label('siteTitle'),
  tagline: bigString.label('tagline'),
  fileattachmentsettings: bigString.label('File Attachment Settings').allow(null,""),
  siteLogo: any.label('siteLogo'),
  //Start - Lokesh Boran : QC3-5095
  supportContactDetails: mediumLargeString.label('supportContactDetails'),
  //End - Lokesh Boran : QC3-5095
  passwordAgingLimit: number.label('passwordAgingLimit'),
  passwordAgingMessage: any.label('passwordAgingMessage'),
  reUsedPasswordsLimit: number.label('reUsedPasswordsLimit'),
  reUsedPasswordsMessage: any.label('reUsedPasswordsMessage'),
  //failedLoginAttemptsIsTrue: bool.label('failedLoginAttemptsIsTrue'),
  failedLoginAttemptsLimit: number.allow(null).default(0).label('failedLoginAttemptsLimit'),
  // .when('failedLoginAttemptsIsTrue', {
  //   is: true,
  //   then: required()
  // }),
  failedLoginAttemptsLimitMessage: any.label('failedLoginAttemptsLimitMessage'),
  hideProcTitle: bool.label('hideProcTitle'),
  showHideCriteriaDetails:bool.label('Show Acceptance Criteria Icon in the App'),
  displayNullValues: bool.label('displayNullValues'),
  // .when('failedLoginAttemptsIsTrue', {
  //   is: true,
  //   then: required()
  // })
  emailAlertForAssignment: bool.label('emailAlertForAssignment'),
  minAttributeEntity:number.label('Minimum entity or attributes required'),
  allowPrintSites:bool.label('allowPrintSites'),
  closeWindowDay: number.allow(null).label('Close day of Window'),
  timeZone: string.label('Time Zone')
};

module.exports = schema;
