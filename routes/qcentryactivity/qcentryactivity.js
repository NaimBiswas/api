'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'qcentryactivity',
    collection: 'qcentryactivity',
    schema: schema,
    softSchema: softSchema
  }
});

/**
 * @swagger
 * definition:
 *   QCEntryActivity:
 *     properties:
 *       _id:
 *         type: string
 *       qcformId:
 *         type: string
 *       qcentryId:
 *         type: string
 *       title:
 *         type: string
 *       status:
 *         type: string
 *       modifiedBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       createdBy:
 *         $ref: '#/definitions/ModifiedBy'
 *       version:
 *         type: integer
 *       isMajorVersion:
 *         type: boolean

 */

/**
 * @swagger
 * definition:
 *   QCEntryActivityPost:
 *     properties:
 *       qcformId:
 *         type: string
 *       qcentryId:
 *         type: string
 *       status:
 *         type: string
 *       attributes:
 *           type: array
 *           items:
 *              type: string

 */

/**
 * @swagger
 * /qcentryactivity:
 *   post:
 *     tags: [Advanced Search]
 *     description: Creates a new qc entry activity
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: qcentryactivity
 *         description: qcentryactivity object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/QCEntryActivityPost'
 *     responses:
 *       201:
 *         description: Successfully created
 */

 /**
  * @swagger
  * /qcentryactivity/{id}:
  *   get:
  *     tags: [Advanced Search]
  *     description: Returns qc entry activity
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: id
  *         description: QC Entry Activity Id
  *         in: path
  *         required: true
  *         type: string
  *       - name: pagesize
  *         description: Page Size
  *         in: query
  *         required: false
  *         type: string
  *       - name: q[qcentryId]
  *         description: QC Entry Id
  *         in: query
  *         required: false
  *         type: string
  *       - name: sort
  *         description: Mention comma seperated names of properties of entries with which you want to sort output (Exp Values -> [ -createdDate])
  *         in: query
  *         required: false
  *         type: string
  *     responses:
  *       200:
  *         description: An object of qc entry activity
  *         schema:
  *           $ref: '#/definitions/QCEntryActivity'
  */



module.exports = route.router;
