'use strict'
/**
 * @name modules-schema
 * @author Kajal Patel <kajal.p@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var id = types.id;
var rId = types.rId;
var rString = types.rString;
var string = types.string;
var required = types.required;
var optionalArray = types.optionalArray;
var any = types.any;
var date = types.date;
var number = types.number;

var schema = object({
  recordVersion: number.label('Entry Version'), //QC3-8959 Prachi
  qcformId: rId.label('form id'),
  qcentryId : rId.label('entry id'),
  title : rString.label('activity title'),
  status: rString.label('activity status'),
  attributes : optionalArray(object({
    id : id.label('attribute id'),
    title: string.label('attribute title'),
    value : any,
    type: object({
      _id: rId.label('Type id'),
      title: rString.label('Type title'),
      format: object({
        _id: rId.label('Format id'),
        title: rString.label('Format title'),
        metadata: any
        //formatString: string.label('Format string')
      })
    }).required().label('Type object'),
  })).label('attribute object'),
  spentTime: object({
    startTime: date.label('Start Time'),
    endTime: date.label('End Time')
  }).label('spentTime'),
  createdDate: date.label('Created Date'),
  modifiedDate: date.label('Modified Date'),
  timeZone: string.label('Time Zone')
});

module.exports = schema;
