'use strict'
/**
 * @name modules-schema
 * @author Kajal Patel <kajal.p@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var id = types.id;
var string = types.string;
var optionalArray = types.optionalArray;
var required = types.required;
var any = types.any;
var date = types.date;
var number = types.number;


var schema = object({
  recordVersion: number.label('Entry Version'), //QC3-8959 Prachi
  qcformId: id.label('form id'),
  qcentryId : id.label('entry id'),
  title : string.label('activity title'),
  status: string.label('Attribute status'),
  attributes : optionalArray(object({
    id : id.label('attribute id'),
    title: string.label('attribute title'),
    value : any,
    type: object({
      _id: id.label('Type id'),
      title: string.label('Type title'),
      format: object({
        _id: id.label('Format id'),
        title: string.label('Format title'),
        metadata: any
        //formatString: string.label('Format string')
      })
    }).required().label('Type object')
  })).label('attribute object'),
  spentTime: object({
    startTime: date.label('Start Time'),
    endTime: date.label('End Time')
  }).label('spentTime'),
  createdDate: date.label('Created Date'),
  modifiedDate: date.label('Modified Date'),
  timeZone: string.label('Time Zone')

});

module.exports = schema;
