'use strict'
/**
 * @name modules-schema
 * @author Amit Dave <dave.a@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var rBool = types.rBool;
var rNumber = types.rNumber;
var string = types.string;
var required = types.required;
var any = types.any;
var bool = types.bool;


var schema = object({
  reportType : any,
  identity : any,
  objectName: any,
  user : any,
  timeZone: string.label('Time Zone')
});

module.exports = schema;
