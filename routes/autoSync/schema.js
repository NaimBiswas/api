
'use strict'
/**
 * @name questions-schema
 * @author vishesh  <vijeta.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var array = types.array;
var anyArray = types.anyArray;
var bool = types.bool;
var string = types.string;
var any = types.any;

var schema = {

    timeZone: string.label('Time Zone'),
    title :string.label('Time Zone'),
    mappingListArray:anyArray.label('Level Ids'),
    isActive: bool.label('isSyncing'),
    minutes: any,
    occurenceState : bool.label('isSyncing'),
    frequency : any,
    startDate : any,
    endDate : any,
    occurence : any,
    manualTime : any,
    cronJob : any,
    expression : any,
    displayModuleTitle : any,
    selectedMappingTitle : any,

};

module.exports = schema;
