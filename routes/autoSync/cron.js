(function() {

    'use strict';
    var db = require('../../lib/db');
    var q = require('q');
    var _ = require('lodash');
    var logger = require('../../logger');
    var CronJob = require('cron').CronJob;
    var amqp = require('amqplib/callback_api');
    var config = require('../../config/config.js');
    var allJobs = {};
    var cronService = function() {};
    var rabbitMQConn = {};
    var sendToSyncQueue = {};
    var autoSyncHistoryRecords = {};
    function init() {
        logger.log("Cron INIT",'debug','cron.js');
        var deffered = q.defer();
        //Check for already ruuning job and stop all of them
        for (var v in allJobs) {
            logger.log("v-----------" + v,'debug','cron.js');
            allJobs[v].stop();
        }

        if(rabbitMQConn && rabbitMQConn.conn){
            rabbitMQConn.conn.close();
        }

        //Step 1 : Get the Configurations of Auto sync having isActive true;
        db['autoSync'].find({ isActive: true }).toArray(function(err, autoSyncData) {
            if (err) {
                return deffered.reject(err);
            }

            amqp.connect(config.rabbitmqPluginConnectionUrl, function(err, conn) {
                if (err) {
                    return err;
                }
                rabbitMQConn.conn = conn;
                rabbitMQConn.conn.createChannel(function(err, ch) {
                    if (err) {
                        return err;
                    }
                    var q = 'pluginchannels';                    
                    ch.assertQueue(q, { durable: false });
                    _.forEach(autoSyncData, function(autoSyncExpression, idx) {
                        allJobs[idx] = {};
                        logger.log("vallJobs--------- "+ allJobs[idx]);
                        allJobs[idx] = new CronJob(autoSyncExpression.expression, function() {
                            //this runs when job tick event occurs.
                            var timeStamp = +new Date();
                                _.forEach((autoSyncExpression.selectedMappingTitle || []), function(createdMappingTitle) {
                                    sendToSyncQueue = {
                                        'id': createdMappingTitle.id,
                                        'autoSyncId': autoSyncExpression._id,
                                        'transactionId': timeStamp
                                    }
                                    ch.sendToQueue(q, new Buffer(JSON.stringify(sendToSyncQueue)));
                                });
                                autoSyncHistory(autoSyncExpression,timeStamp);
                            },
                            function(){
                                //this runs when job stops.
                            },
                            true,
                            config.timeZonesP[config.currentTimeZoneP]
                        );
                    })
                });
            });

            logger.log("I am here",'debug','cron.js');
            return deffered.resolve(autoSyncData);
        });
        return deffered.promise;
    }

    function autoSyncHistory(autoSyncConfiguration,timeStamp) {
        autoSyncHistoryRecords = {
            'autoSyncId': autoSyncConfiguration._id.toString(),
            'autoSyncName': autoSyncConfiguration.title,
            'transactionId': timeStamp,
            'startDate': new Date(),
            'endDate': '',
            'mappings': []
        }
        _.forEach(autoSyncConfiguration.selectedMappingTitle, function(mapping) {
            autoSyncHistoryRecords.mappings.push({ id: mapping.id, title: mapping.label, startDate: +new Date(), endDate: '' })
        })
        db.collection('autoSyncHistory').insert(autoSyncHistoryRecords ,function(err, results) {
            if (err) {
                logger.log(err);
                return err;
            }
        })
    }

    cronService.prototype.init = init;
    module.exports = new cronService();

})();
