
'use strict'
/**
 * @name questions-schema
 * @author vishesh  <vijeta.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var anyArray = types.anyArray;
var rBool = types.rBool;
var bool = types.bool;
var number = types.number;
var rNumber = types.number;
var string = types.string;
var required = types.required;
var stringArray = types.stringArray;
var any = types.any;
var rBigString = types.bigString;
var bigString = types.bigString;
var optionaldate = types.optionaldate;
var smallString = types.smallString;


var schema = {
    timeZone: string.label('Time Zone'),
    title :string.label('Time Zone'),
    mappingListArray:anyArray.label('Level Ids'),
    isActive: bool.label('isSyncing'),
    minutes: any,
    occurenceState : bool.label('isSyncing'),
    frequency : any,
    startDate : any,
    endDate : any,
    occurence : any,
    manualTime : any,
    cronJob : any,
    expression : any,
    displayModuleTitle : any,
    selectedMappingTitle : any,


    // object({
    //   _id: rId.label('Instance id'),
    //   title: rString.label('Instance title')
    // }).required().label('Schema module'),
};

module.exports = schema;
