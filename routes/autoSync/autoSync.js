'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var validate = require('../../lib/validator');
var db = require('../../lib/db');
var cron = require('./cron')

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'autoSync',
    collection: 'autoSync',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    POST: {
      ONE: {
        after: saveCron
      }
    },
    PATCH: {
      ONE: {
        after: saveCron
      }
    }
  }
});

function saveCron(req, res, next) {
  cron.init();
  next();
}

module.exports = route.router;

/**
 * @swagger
 * definition:
 *   Version:
 *     properties:
 *       version:
 *         type: integer
 *       record:
 *         type: integer
 */

/**
 * @swagger
 * definition:
 *    DayOfMonth:
 *      properties:
 *       day:
 *          type: string
 *       month: 
 *          type: string
 */

/**
 * @swagger
 * definition:
 *    Repeats4:
 *      properties:
 *        dayOfMonth:
 *          type: array         
 *          items:
 *            type: object
 *            allOf:
 *              - $ref: '#/definitions/DayOfMonth'
 */

/**
 * @swagger
 * definition:
 *    Repeats3:
 *      properties:
 *        repeats:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *             - $ref: '#/definitions/Repeats4' 
 */

/**
 * @swagger
 * definition:
 *    Monthly:
 *      properties:
 *         repeats:
 *           type: array
 *           items:
 *             type: object
 *             allOf:
 *               - $ref: '#/definitions/Repeats3'
 */

/**
 * @swagger
 * definition:
 *    Days:
 *     properties:
 *      days:
 *         type: array
 *         items:
 *          type: object 
 */

/**
 * @swagger
 * definition:
 *    Repeats2:
 *      properties:
 *        every:
 *          type: string
 *        days:
 *          type: array
 *          items:
 *             type: object
 *             allOf:
 *              - $ref: '#/definitions/Days'
 */

/**
 * @swagger
 * definition:
 *    Weekly:
 *      properties:
 *         repeats:
 *            type: array
 *            items:
 *              type: object
 *              allOf:
 *               - $ref: '#/definitions/Repeats2'
 */

/**
 * @swagger
 * definition:
 *    Repeats1:
 *      properties:
 *        every:
 *          type: string
 *        everyBusinessDay:
 *          type: string
 */

/**
 * @swagger
 * definition:
 *    Daily:
 *      properties:
 *        repeats:
 *           type: array
 *           items:
 *             type: object
 *             allOf:
 *               - $ref: '#/definitions/Repeats1'
 */

/**
 * @swagger
 * definition:
 *    Repeats:
 *      properties:
 *        every:
 *          type: string
 */

/**
 * @swagger
 * definition:
 *    Hourly:
 *      properties:
 *         repeats:
 *            type: array
 *            items:
 *              type: object
 *              allOf:
 *               - $ref: '#/definitions/Repeats'
 */

/**
 * @swagger
 * definition:
 *    frequency:
 *      properties:
 *        minutes: 
 *          type: string
 *        type:
 *          type: string
 *        subType:
 *          type: string
 *        hourly:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *             - $ref: '#/definitions/Hourly'
 *        daily:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *             - $ref: '#/definitions/Daily'
 *        weekly:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *             - $ref: '#/definitions/Weekly'
 *        monthly:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *             - $ref: '#/definitions/Monthly'
 */

/**
 * @swagger
 * definition:
 *    selectedMappingTitle:
 *      properties:
 *        id:
 *          type: integer
 *        label:
 *          type: string 
 */

/**
 * @swagger
 * definition:
 *    mappingListArray:
 *      properties:
 *        mappingListArray:
 *          type: string
 */

/**
 * @swagger
 * definition:
 *    AutoSync:
 *      properties:
 *        id: 
 *          type: integer
 *        title:
 *          type: string
 *        mappingListArray:
 *          type: array
 *          items:
 *            type: object
 *            allOf: 
 *             - $ref: '#/definitions/mappingListArray'
 *        isActive:
 *          type: boolean
 *        expression: 
 *          type: string
 *        selectedMappingTitle:
 *          type: array
 *          items:
 *            type: object
 *            allOf:
 *              - $ref: '#/definitions/selectedMappingTitle'
 *        frequency:
 *          type: array
 *          items:
 *             type: object
 *             allOf:
 *              - $ref: '#/definitions/frequency'
 *        manualTime:
 *          type: dateTime
 *        createdBy:
 *          type: integer
 *        modifiedBy: 
 *          type: integer
 *        timeZone: 
 *          type: string
 *        createdByName:
 *          type: string
 *        modifiedByName:
 *          type: string
 *        createdDate:
 *          type: dateTime
 *        modifiedDate:
 *          type: dateTime
 *        versions:
 *           type: array
 *           items:
 *              type: object
 *              allOf:
 *              - $ref: '#/definitions/Version'
 *        version:
 *           type: integer
 *        isMajorVersion:
 *           type: boolean
 */

/**
 * @swagger
 * /autoSync:
 *  get:
 *    tags: [Administration]
 *    description: List of mappings
 *    produces: 
 *      - application/json
 *    parameters:
 *       - name: page
 *         description: Page Number
 *         in: query
 *         required: false
 *         type: string
 *       - name: pagesize
 *         description: Page Size
 *         in: query
 *         required: false
 *         type: string
 *       - name: select
 *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,title,frequency,modifiedDate,modifiedBy,isActive])
 *         in: query
 *         required: false
 *         type: string
 *       - name: sort
 *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
 *         in: query
 *         required: false
 *         type: string
 *    responses:
 *       200:
 *         description: An array of mappings
 *         schema:
 *           $ref: '#/definitions/AutoSync'
 */