'use strict'
var schema = require('./schema');
var dbConfig = require('../../lib/db/db');
var log = require('../../logger');
var debug = require('../../lib/logger').debug('routes:schema');
var entries = require('../entries');
var _ = require('lodash');
var db = require('../../lib/db');
var linkedTbl = require('../../lib/linkedTbl');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'licenseAgreement',
    collection: 'licenseAgreement',
    schema: schema
  },
  types: {
    POST: {
      ONE: {
        after: afterPost
      },
      ONESOFT: {
        after: afterPost
      }
    },
    PATCH: {
      ONE: {
        after: afterPatch
      },
      ONESOFT: {
        after: afterPatch
      }
    }
  }
});

function afterPost(req, res, next) {
  res.json(req.data);
}



function afterPatch(req, res, next) {
  res.json(req.data);
}


module.exports = route.router;