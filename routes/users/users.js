'use strict'
var express = require('express');
var registration = require('../../lib/registration');
var validate = require('../../lib/validator');
var schema = require('./schema');
var db = require('../../lib/db');
var _ = require('lodash');
var model = require('../../lib/oauth/model');
var loggedInUser = require('../../lib/oauth/loggedInUserDetails');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'users',
    collection: 'users',
    schema: schema.beginSignUpSchema,
    softSchema: schema.updateUserSchema,
    resource: registration
  },
  types: {
    //Chandni : Code start for Swagger

    /**
    * @swagger
    * definition:
    *   DataModel:
    *     properties:
    *       id:
    *         type: integer
    *       title:
    *         type: string
    */

    /**
    * @swagger
    * definition:
    *   DataModelWithoutTitle:
    *     properties:
    *       id:
    *         type: integer
    */


    /**
    * @swagger
    * definition:
    *   User:
    *     properties:
    *       username:
    *         type: string
    *       firstname:
    *         type: string
    *       lastname:
    *         type: string
    *       email:
    *         type: string
    *       loggedInDateTime:
    *         type: dateTime
    *       modifiedDate:
    *         type: dateTime
    *       verificationCode:
    *         type: string
    *       isActive:
    *         type: boolean
    *       allowAfterfailedLoginAttempts:
    *         type: boolean
    *       isConCurrentUserActive:
    *         type: boolean
    *       isConCurrentUserAdmin:
    *         type: boolean
    *       passwordGenerationMethod:
    *         type: string
    *       roles:
    *           type: array
    *           items:
    *              type: object
    *              allOf:
    *              - $ref: '#/definitions/DataModel'
    *       applicableSites:
    *           type: array
    *           items:
    *              type: object
    *              allOf:
    *              - $ref: '#/definitions/DataModel'
    */


    /**
    * @swagger
    * /users:
    *   get:
    *     tags: [User]
    *     description: Returns all users
    *     produces:
    *       - application/json
    *     parameters:
    *       - name: page
    *         description: Page Number
    *         in: query
    *         required: false
    *         type: string
    *       - name: pagesize
    *         description: Page Size
    *         in: query
    *         required: false
    *         type: string
    *       - name: select
    *         description: Mention comma seperated names of properties of user that you want to see in the output (Exp Values -> [ index,username,displayroles,firstname,lastname,Reset,modifiedDate,modifiedBy,isActive])
    *         in: query
    *         required: false
    *         type: string
    *       - name: sort
    *         description: Mention comma seperated names of properties of user with which you want to sort output (Exp Values -> [ -modifiedDate])
    *         in: query
    *         required: false
    *         type: string
    *     responses:
    *       200:
    *         description: An array of users
    *         schema:
    *           $ref: '#/definitions/User'

    */
    //Chandni : Code end for Swagger

    /**
    * @swagger
    * /users/{id}:
    *   get:
    *     tags: [User]
    *     description: Returns a object user
    *     produces:
    *       - application/json
    *     parameters:
    *       - name: id
    *         description: User's id
    *         in: path
    *         required: true
    *         type: integer
    *       - name: pagesize
    *         description: Page Size
    *         in: query
    *         required: false
    *         type: string
    *     responses:
    *       200:
    *         description: An object of user
    *         schema:
    *           $ref: '#/definitions/User'

    */
    GET: {},
    /**
    * @swagger
    * /users/{id}:
    *   put:
    *     tags: [User]
    *     description: Updates a user
    *     produces:
    *       - application/json
    *     parameters:
    *       - name: id
    *         description: User's id
    *         in: path
    *         required: true
    *         type: integer
    *       - name: user
    *         description: User object
    *         in: body
    *         required: true
    *         schema:
    *           $ref: '#/definitions/User'
    *     responses:
    *       200:
    *         description: Successfully updated
    */
    PUT: {},
    /**
    * @swagger
    * /users:
    *   post:
    *     tags: [User]
    *     description: Creates a new user
    *     produces:
    *       - application/json
    *     parameters:
    *       - name: user
    *         description: User object
    *         in: body
    *         required: true
    *         schema:
    *           $ref: '#/definitions/User'
    *     responses:
    *       201:
    *         description: Successfully created
    */
    POST: {
      'ONE':{
        insecure: true
      }
    },
    /**
    * @swagger
    * /users/{id}:
    *   patch:
    *     tags: [User]
    *     description: Updates a user
    *     produces:
    *       - application/json
    *     parameters:
    *       - name: id
    *         description: User's id
    *         in: path
    *         required: true
    *         type: integer
    *       - name: user
    *         description: User object
    *         in: body
    *         required: true
    *         schema:
    *           $ref: '#/definitions/User'
    *     responses:
    *       200:
    *         description: Successfully updated
    */
    PATCH: {
      'ONE' : {
        before: beforePatch
      }
    }
  }
});

function beforePatch(req,res,next){
  registration.getExpireDateFromGeneralSettings(req,res,next);
}

var router = route.router;

// /* POST users listing. Initiates sign-up */
// router.post('/', validate(schema.beginSignUpSchema), function initiateSignupRouteCallback(req, res, next) {
//   registration.beginSignUpAsync(req.items)
//   .then(function beginSignUpAsyncThenCallback(value){
//     res.status(201).json(value);
//   })
//   .catch(function beginSignUpAsyncErrorCallback(err) {
//     res.status(422).json({'message': 'User already registered.'});
//   });
// });


/*
PUT user/{code}. Code can be representing - 'finishSignUp' or 'finishForgetPassword'
as off now
*/

router.use(function (req, res, next) {
  if (!(req.headers['x-authorization'] || req.query['x-authorization'] || req.headers['authorization'] || req.query['authorization'])) {
    next();
  }else {
    loggedInUser.detailsByToken(req, res, function (err, result) {
      if (err || !result) {
        res.status(422).json({'message': (err.message || 'some error has been occured')});
      } else {
        var clientIP = req.connection.remoteAddress || req.ip || null;
        req.user = result;
        req.clientIP = clientIP;
        next();
      }
    });
  }
});

router.patch('/code/:code/status/:status', validate(schema.finishSignUpSchema), registration.getExpireDateFromGeneralSettings, function codeStatusRouteCallback(req,res,next) {
  switch (req.items.status) {
    case 'signing-up':
    //req.items.passwordExpiresDate = req.passwordExpiresDate;
    registration.finishSignUpAsync(req.items)
    .then(function finishSignUpAsyncThenCallback(value) {
      res.status(200).json(value);
    })
    .catch(function finishSignUpAsyncErrorCallback(err) {
      res.status(422).json({'message': 'Failed to execute step. The Code is invalid or expired.'});
    });
    break;
    case 'forgot-password':
    //req.items.passwordExpiresDate = req.passwordExpiresDate;
    registration.finishForgetPasswordAsync(req.items)
    .then(function finishForgetPasswordThenCallback(value) {
      res.status(200).json(value);
    })
    .catch(function finishForgetPasswordErrorCallback(err) {
      res.status(422).json({'message': 'Failed to execute step. The Code is invalid or expired.'});
    });
    break;
    case 'switching-email':
    registration.finishEmailChangeAsync(req.items)
    .then(function finishForgetPasswordThenCallback(value) {
      res.status(200).json(value);
    })
    .catch(function finishForgetPasswordErrorCallback(err) {
      res.status(422).json({'message': 'Failed to execute step. The Code is invalid or expired.'});
    });
    break;
    default:
    res.status(422).json({'message': 'Failed to execute step. Please make sure that the code is valid.'});
  }
});

/**
* @swagger
* /users/{username}/forgot-password:
*   patch:
*     tags: [User]
*     description: Reset a user's password
*     produces:
*       - application/json
*     parameters:
*       - name: username
*         description: User's username
*         in: path
*         required: true
*         type: integer
*       - name: username
*         description: User's username
*         in: formData
*         required: true
*         type: string
*     responses:
*       200:
*         description: Successfully updated
*/
router.patch('/:username/forgot-password', validate(schema.beginForgotPasswordSchema), function forgotPasswordRouteCallback(req,res,next) {
  registration.beginForgetPasswordAsync(req.items)
  .then(function beginForgetPasswordThenCallback(value) {
    res.status(200).json(value);
  })
  .catch(function beginForgetPasswordErrorCallback(err) {
    res.status(422).json({'message': 'Failed to execute step. The Code is invalid or expired.'});
  });
});

router.get('/username/:username/:forgotPasswordCode', validate(schema.findOneUserByNameAndForgotPasswordCodeSchema), function getUserByUserNameAndForgotPasswordCode(req, res, next) {
  req.items.tempAction = 'Others'
  if(req.items && req.items.username && req.items.username.indexOf('-emailChanged') != -1){
    req.items.tempAction = req.items.username.split('-')[1]
    req.items.username = req.items.username.split('-')[0];
  }
  registration.getUserByUserNameAndForgotPasswordCodeAsync(req.items)
  .then(function beginForgetPasswordThenCallback(value) {
    res.status(200).json(value);
  })
  .catch(function beginForgetPasswordErrorCallback(err) {
    res.status(422).json({'message': 'Failed to execute step. The Code is invalid or expired.'});
  });
})

//reset the token from existing token
router.get('/resetAccessToken/:userid', function (req, res, next) {
  db['users'].findOne({ _id: db.ObjectID(req.params.userid) }, function (e, u) {
    if (e) { next; }
    if (u) {
      req.user = u;
      model.generateToken('roles', req, function (error, token) {
        if (error) { next; };
        var response = {
          token_type: 'bearer',
          access_token: token
        };
        res.set({ 'Cache-Control': 'no-store', 'Pragma': 'no-cache' });
        res.jsonp(response);
        next;
      });
    }
  });

});

/**
* @swagger
* /users/{username}/forgot-esignature:
*   patch:
*     tags: [User]
*     description: Reset a user's esignature PIN
*     produces:
*       - application/json
*     parameters:
*       - name: username
*         description: User's username
*         in: path
*         required: true
*         type: integer
*       - name: username
*         description: User's username
*         in: formData
*         required: true
*         type: string
*     responses:
*       200:
*         description: Successfully updated
*/
router.patch('/:username/forgot-esignature/', validate(schema.beginForgotESignatureSchema), function (req, res, next) {
  registration.beginForgetESignatureAsync(req.items)
  .then(function (value) {
    res.status(200).json(value);
  })
  .catch(function (err) {
    res.status(422).json({ 'message': 'Failed to execute step. The Code is invalid or expired.' });
  });
});
router.get('/verifyesign/:username/:forgotESignatureCode', validate(schema.findOneUserByNameAndForgotESignatureCodeSchema), function(req, res, next) {
  registration.getUserByUserNameAndForgotESignatureCodeAsync(req.items)
  .then(function(value) {
    res.status(200).json(value);
  })
  .catch(function(err) {
    res.status(422).json({ 'message': 'Failed to execute step. The Code is invalid or expired.' });
  });
})
router.patch('/updateesignpin/:username/:code', validate(schema.finishForgotESignatureSchema), function (req, res, next) {
  registration.finishForgetESignatureAsync(req.items)
  .then(function (value) {
    res.status(200).json(value);
  })
  .catch(function (err) {
    res.status(422).json({ 'message': 'Failed to execute step. The Code is invalid or expired.' });
  });
});

router.get('/:_id/abort/:code', validate(schema.abortCodeSchema), function abortCodeSchemaCallback(req, res, next) {
  registration.abortCodeAsync(req.items)
  .then(function abortCodeThenCallback(value) {
    res.status(200).json(value);
  })
  .catch(function abortCodeErrorCallback(err) {
    res.status(422).json({'message': 'Failed to execute step. The Code is invalid or expired.'});
  });
});

// function by surjeet.b@productivet.com for verifying verification code
router.get('/:_id/verifyVerificationCode/:verificationCode', validate(schema.verifyVerificationCodeSchema), function verifyVerificationCodeSchemaCallback(req, res, next) {
  registration.verifyVerificationCodeAsync(req.items)
  .then(function verifyVerificationCodeThenCallback(value) {
    res.status(200).json(value);
  })
  .catch(function verifyVerificationCodeErrorCallback(err) {
    res.status(422).json({'message': 'Invalid Verification Code.'});
  });
});

// function by surjeet.b@productivet.com to restrict multiple email alert
router.get('/:username/emailRestriction', validate(schema.verifyemailRestrictionSchema), function emailRestrictionSchemaCallback(req, res, next) {
  registration.emailRestrictionAsync(req.items)
  .then(function emailRestrictionThenCallback(value) {
    res.status(200).json(value);
  })
  .catch(function emailRestrictionErrorCallback(err) {
    res.status(422).json({'message': 'some error has been occured'});
  });
});

/**
* @swagger
* /users/{username}/resetPassword:
*   patch:
*     tags: [User]
*     description: Updates a user's password
*     produces:
*       - application/json
*     parameters:
*       - name: username
*         description: User's username
*         in: path
*         required: true
*         type: integer
*       - name: passwordGenerationMethod
*         description: Password Generation Method (system / Admin)
*         in: formData
*         required: true
*         type: string
*       - name: newPassword
*         description: New Password to set if Password Generation Method is "Admin"
*         in: formData
*         required: false
*         type: string
*     responses:
*       200:
*         description: Successfully updated
*/
// function by surjeet.b@productivet.com to restrict multiple email alert
router.patch('/:username/resetPassword', validate(schema.verifyresetPasswordSchema), registration.getExpireDateFromGeneralSettings, function beginResetPasswordSchemaCallback(req, res, next) {
  //req.items.passwordExpiresDate = req.passwordExpiresDate;
  // loggedInUser.detailsByToken(req, res, function (err, result) {
    // if (err || !result) {
    //   res.status(422).json({'message': (err.message || 'some error has been occured')});
    //   // return;
    // } else if (result) {
    req.clientIP = req.clientIP || req.connection.remoteAddress;
      if (req) {
      // var clientIP = req.connection.remoteAddress || req.ip || null;
      req.items.auditInfo = req.items.auditInfo ? req.items.auditInfo : {};
      req.items.auditInfo.ip = req.clientIP; // QC3-9624 - Issue 4 - Jyotil
      registration.beginResetPasswordAsync(req.items, req.clientIP, req.user)
      .then(function beginResetPasswordThenCallback(value) {
        res.status(200).json(value);
      })
      .catch(function beginResetPasswordErrorCallback(err) {
        res.status(422).json({'message': 'some error has been occured'});
      });
    } else {
      res.status(422).json({'message': (err.message || 'some error has been occured')});
    }

  // });
});

//by surjeet.b@productivet.com to reset user access
router.patch('/:username/resetAccess/:status', function beginResetAccessSchemaCallback(req, res, next) {
  req.clientIP = req.clientIP || req.connection.remoteAddress;
  if (req) {

    switch(req.params.status){
      case '1':
      registration.beginResetAccessAsync(req.params, req.clientIP, req.user)
      .then(function beginResetAccessThenCallback(value) {
        res.status(200).json(value);
      })
      .catch(function beginResetAccessErrorCallback(err) {
        res.status(422).json({'message': 'some error has been occured'});
      });
      break;

      case '2':
      var userInfo = req.params;
      _.merge(userInfo, req.body);

      registration.finishResetAccessAsync(userInfo, req.clientIP, req.user)
      .then(function finishResetAccessThenCallback(value) {

        //console.log(res);
        res.status(200).json(value);
      })
      .catch(function finishResetAccessErrorCallback(err) {
        res.status(422).json({'message': 'some error has been occured'});
      });
      break;

      default:
      res.status(422).json({'message': 'Failed to execute step. Please make sure that the code is valid.'});
    }
  } else {
    res.status(422).json({'message': 'some error has been occured'});
  }
});

// router.patch('/:username/resetAccess/:status', function finishResetAccessSchemaCallback(req, res, next) {
//
// });

//by surjeet.b@productivet.com..
router.get('/:_id/resend/:code', validate(schema.abortCodeSchema), function abortCodeSchemaCallback(req, res, next) {
  registration.resendCodeAsync(req.items)
  .then(function resendCodeThenCallback(value) {
    res.status(200).json(value);
  })
  .catch(function resendCodeErrorCallback(err) {
    res.status(422).json({'message': 'Failed to execute step. The Code is invalid or expired.'});
  });
});

//by surjeet.b@productivet.com..
router.get('/:_id/unverifiedUserMailSend/:code', validate(schema.abortCodeSchema), function abortCodeSchemaCallback(req, res, next) {
  registration.unverifiedUserMailSendAsync(req.items)
  .then(function resendCodeThenCallback(value) {
    res.status(200).json(value);
  })
  .catch(function resendCodeErrorCallback(err) {
    res.status(422).json({'message': 'Failed to execute step. The Code is invalid or expired.'});
  });
});

router.get('/:_id/adminId/:_adminId/isConCurrentUserActive/:isConCurrentUserActive', validate(schema.isConCurrentUserActiveSchema), function isConCurrentUserCallback(req, res, next) {
  registration.conCurrentUserUpdateAsync(req.items)
  .then(function isConCurrentUserCallback1(value) {
    res.status(200).json(value);
  })
  .catch(function isConCurrentUserCallback1(err) {
    res.status(422).json({'message': 'Failed to execute step. To update isConCurrentUserActive.'});
  });
});

router.get('/:_id/isConCurrentUserActive/:isConCurrentUserActive/username/:username', validate(schema.isConCurrentUserActiveSchemaByUserName), function isConCurrentUserCallback(req, res, next) {
  registration.conCurrentUserUpdateByUserNameAsync(req.items)
  .then(function (value) {
    res.status(200).json(value);
  })
  .catch(function (err) {
    res.status(422).json({'message': 'Failed to execute step. To update isConCurrentUserActive.'});
  });
});

router.get('/:_id/isConCurrentUserAdmin/:isConCurrentUserAdmin', validate(schema.isConCurrentUserAdminSchema), function (req, res, next) {
  registration.conCurrentAdminUserUpdateAsync(req.items)
  .then(function (value) {
    res.status(200).json(value);
  })
  .catch(function (err) {
    res.status(422).json({'message': 'Failed to execute step. To update isConCurrentUserActive.'});
  });
});

router.patch('/:_id/password-log', validate(schema.validateNewPasswordSchema), function(req,res,next) {
  registration.updatePasswordLogAsync(req.items)
  .then(function (value) {
    res.status(200).json(value);
  })
  .catch(function (err) {
    res.status(422).json({'message': 'Failed to execute step. The password-log not updated.'});
  });
});

router.patch('/:_id/new-password', validate(schema.validateNewPasswordSchema), function(req,res,next) {
  registration.validateNewPasswordAsync(req.items)
  .then(function (value) {
    res.status(200).json(value);
  })
  .catch(function (err) {
    res.status(422).json({'message': 'Failed to execute step. The password-log not validate.'});
  });
});

router.patch('/:_id/change-password', validate(schema.finishChangePasswordSchema), function (req,res,next) {
  registration.finishChangePasswordAsync(req.items)
  .then(function (value) {
    res.status(200).json(value);
  })
  .catch(function (err) {
    res.status(422).json({'message': 'Failed to execute step. The Change Password is invalid or expired.'});
  });
});

router.patch('/:_id/updateLicenseAgreement', validate(schema.updateLicenseAgreementSchema), function(req,res,next) {
  registration.updateLicenseAgreementInfoAsync(req.items)
  .then(function (value) {
    res.status(200).json(value);
  })
  .catch(function (err) {
    res.status(422).json({'message': 'Failed to execute step to update license agreement information.'});
  });
});

module.exports = router;
