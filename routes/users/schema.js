var joi = require('joi');
var types = require('../../lib/validator/types');

var rId = types.rId;
var id = types.id;
var rString =  types.rString;
var string =  types.string;
var rBool =  types.rBool;
var bool =  types.bool;
var rNumber =  types.rNumber;
var number =  types.number;
var object =  types.object;
var required =  types.required;
var array =  types.array;
var bigString = joi.string().min(2).max(500);
var rBigString = bigString.required();
var forbidden = joi.forbidden.bind(joi);
var plainString = joi.string();
var optionalArray = types.optionalArray;
var stringArray = types.stringArray;
var date = types.date;
var anyArray = types.anyArray;
var any = types.any;

var beginSignUpSchema = {
  username: rString.label('User Name').regex(/^[a-zA-Z0-9)\(._]+$/g),
  firstname: rString.label('First Name'),
  lastname: rString.label('Last Name'),
  email: rString.label('Email').email(),
  ip: bigString.label('IP Address'),
  swaggerApiKey: bigString.label('Swagger Api Key'),
  assignmentEmailNotification : bool.label('assignmentEmailNotification'),
  temporaryPassword: string.label('temporaryPassword'),
  loggedInDateTime : date.label('Logged In Date & Time'),
  passwordGenerationMethod: plainString
  .valid('System','Admin')
  .label('Password Generation Method')
  .required(),
  password: string.label('Password').when('passwordGenerationMethod',{
    is: 'Admin',
    then: joi.required(),
    otherwise: joi.forbidden()
  }),
  verificationCode: rString.label('Verification Code'),
  isActive: rBool.label('Active?'),
  allowAfterfailedLoginAttempts: rBool.label('Allow user to Login after Failed Login Attempts'),
  failedLoginAttempts: number.label('failedLoginAttempts'),
  isConCurrentUserActive: rBool.label('IsConCurrentUserActive?'),
  isConCurrentUserAdmin: rBool.label('IsConCurrentUserAdmin?'),
  forceRemovedUser : string.label('force Removed User'),
  roles: array(object({
    _id: id.label('Role Id'),
    title: string.label('Role Title'),
    isActive: bool.label('Role Active?'),
    description: string.label('Role Description'),
    modules: array(object({
      _id: id.label('Modules id'),
      permissions: array(object({
        entityType: string.label('Entity Type'),
        title: string.label('Title'),
        view: bool.label('Permissions View'),
        create: bool.label('Permissions Create'),
        edit: bool.label('Permissions Edit'),
        review: bool.label('Permissions Review'),
        editSchema: bool.label('Permissions editSchema')
      })).label('Permissions Array')
    })).label('Modules Array')
  })),
  displayroles :  any.label('roles array'),
  applicableSites: array(
    optionalArray(object({
      _id: rId.label('Id'),
      title: rString.label('Title')
    }))
  ),
    passwordLog: stringArray.label('Password Log'),
  createById : id.label('Create By ID'),
  performedAction: bigString.label('Performed Action').default('Profile Updated'),
  performedActionOn : date.label('Action Performed On').default(new Date()),
  licenseAgreementStatus: string.label('License Agreement Status'),
  licenseAcceptanceDateTime: date.label('Date & Time of License Acceptance'),
  licenseAgreementVersion : number.label('License Agreement Version'),
  timeZone: string.label('Time Zone')
  //,
  // roles: array(object({
  //   _id: rId.label('Role Id'),
  //   title: rString.label('Role Title'),
  //   description: rBigString.label('Role Description'),
  //   permissions: array(object({
  //     entityType: plainString
  //     .valid('attribute','question','procedure','schema')
  //     .label('Entity Type to which permission is applied')
  //     .required(),
  //     view: bool.label('Has View Permission'),
  //     create: bool.label('Has Create Permission'),
  //     edit: bool.label('Has Edit Permission'),
  //     review: bool.label('Has Review Permission'),
  //     editSchema: bool
  //     .when('entityType',{
  //       is: joi.valid('attribute','question','procedure'),
  //       then: joi.forbidden()
  //     })
  //     .label('Has Edit Schema Permission'),
  //     module: object({
  //       _id: rId.label('Module Id'),
  //       title: rString.label('Module Title')
  //     }),
  //     title: rString.label('Display text for this permission set')
  //   }).or('view','create','edit','review','editSchema'))
  // }))
};


var updateUserSchema = joi.object().keys({
  //_id: id.label('User Id'),
  username: string.label('User Name').forbidden(),
  firstname: string.label('First Name'),
  lastname: string.label('Last Name'),
  email: string.label('Email').email(),
  isActive: bool.label('Active?'),
  assignmentEmailNotification : bool.label('assignmentEmailNotification'),
  ip: bigString.label('IP Address'),
  swaggerApiKey: bigString.label('Swagger Api Key'),
  temporaryPassword: string.label('temporaryPassword'),
  loggedInDateTime : date.label('Logged In Date & Time'),
  newEmail: string.label('New Email').email(),
  isUnverfiedUser: bool.label('Is Unverfied User'),
  passwordGenerationMethod: plainString
  .valid('System','Admin')
  .label('Password Generation Method'),
  resetPasswordTo: string.label('Reset Password To')
  .when('passwordGenerationMethod',{
    is: 'Admin',
    then: joi.required(),
    otherwise: joi.forbidden()
  }),
  password: joi.string()
  .label('Password').min(2).max(50),
  newPassword: joi.string()
  .label('New Password').min(2).max(50),
  securityQuestion: joi.string()
  .label('Security Question').min(2).max(200),
  securityAnswer: joi.string()
  .label('Security Answer').min(2).max(50),
  forceRemovedUser : string.label('force Removed User'),
  passwordResetCode: string.label('Password Reset Code'),
  verificationCode: string.label('Verification Code'),
  allowAfterfailedLoginAttempts: bool.label('Allow user to Login after Failed Login Attempts'),
  failedLoginAttempts: number.label('failedLoginAttempts'),
  passwordForverificationCode: string.label('Password to reset old verification code'),
  currentVerificationCode: string.label('Current Verification Code'),
  roles: array(object({
    _id: id.label('Role Id'),
    title: string.label('Role Title'),
    isActive: bool.label('Role Active?'),
    description: string.label('Role Description'),
    modules: array(object({
      _id: id.label('Modules id'),
      permissions: array(object({
        entityType: string.label('Entity Type'),
        title: string.label('Title'),
        view: bool.label('Permissions View'),
        create: bool.label('Permissions Create'),
        edit: bool.label('Permissions Edit'),
        review: bool.label('Permissions Review'),
        editSchema: bool.label('Permissions editSchema')
      })).label('Permissions Array')
    })).label('Modules Array')
  })),
  displayroles :  any.label('roles array'),
  applicableSites: array(
    array(object({
      _id: rId.label('Id'),
      title: rString.label('Title')
    }))
  ),
  passwordLog: stringArray.label('Password Log'),
  performedAction: bigString.label('Performed Action').default('Profile Updated'),
  performedActionOn : date.label('Action Performed On').default(new Date()),
  licenseAgreementStatus: string.label('License Agreement Status'),
  licenseAcceptanceDateTime: date.label('Date & Time of License Acceptance'),
  licenseAgreementVersion : number.label('License Agreement Version'),
  timeZone: string.label('Time Zone')
  // roles: array(object({
  //   _id: id.label('Role Id'),
  //   title: string.label('Role Title'),
  //   isActive: bool.label('Role Active?'),
  //   description: string.label('Role Description'),
  //   modules: array(object({
  //     _id: id.label('Modules id'),
  //     permissions: array(object({
  //       entityType: string.label('Entity Type'),
  //       title: string.label('Title'),
  //       view: bool.label('Permissions View'),
  //       create: bool.label('Permissions Create'),
  //       edit: bool.label('Permissions Edit'),
  //       review: bool.label('Permissions Review'),
  //       editSchema: bool.label('Permissions editSchema')
  //     })).label('Permissions Array')
  //   })).label('Modules Array')
  // }))
  // roles: array(object({
  //   _id: id.label('Role Id'),
  //   title: string.label('Role Title'),
  //   description: bigString.label('Role Description'),
  //   permissions: array(object({
  //     entityType: plainString
  //     .valid('attribute','question','procedure','schema')
  //     .label('Entity Type to which permission is applied'),
  //     view: bool.label('Has View Permission'),
  //     create: bool.label('Has Create Permission'),
  //     edit: bool.label('Has Edit Permission'),
  //     review: bool.label('Has Review Permission'),
  //     editSchema: bool
  //     .when('entityType',{
  //       is: joi.valid('attribute','question','procedure'),
  //       then: forbidden()
  //     })
  //     .label('Has Edit Schema Permission'),
  //     module: object({
  //       _id: id.label('Module Id'),
  //       title: string.label('Module Title')
  //     }),
  //     title: string.label('Display text for this permission set')
  //   }).or('view','create','edit','review','editSchema','title'))
  // }))
}).nand('passwordGenerationMethod','password')
.and('password','newPassword')
.and('email','newEmail')
.with('verificationCode',  'currentVerificationCode');
//'passwordForverificationCode',
var finishSignUpSchema = {
  timeZone: string.label('Time Zone'),
  email: joi.string().label('Email').max(150).email().when('status', {
    is: 'signing-up',
    then: joi.required()
  }),
  newEmail: joi.string().email()
  .label('New Email')
  .min(2).max(150)
  .when('status', {
    is: joi.valid('switching-email','signing-up'),
    then: joi.required(),
    otherwise: joi.forbidden()
  }).allow(''),
  verificationCode: string.label('Verification Code')
  .when('status', {
    is: 'signing-up',
    then: joi.required()
  }),
  code: joi.string().label('code').min(24).max(24).required(),
  securityQuestion: joi.string()
  .label('Security Question')
  .min(2).max(200)
  .when('status', {
    is: joi.valid('signing-up','forgot-password'),
    then: joi.required()
  }),
  securityAnswer: joi.string()
  .label('Security Answer')
  .min(2).max(50)
  .when('status', {
    is: joi.valid('signing-up','forgot-password'),
    then: joi.required()
  }),
  username: string.label('Username')
  .when('status', {
    is: 'forgot-password',
    then: joi.required()
  }),
  newPassword: string.label('New Password')
  .when('status', {
    is: 'forgot-password',
    then: joi.required()
  }),
  confirmPassword: joi.string()
  .when('status', {
    is: 'forgot-password',
    then: joi.required()
  })
  .equal(joi.ref('newPassword'))
  .label('Confirm Password')
  ,
  status: joi.string().label('Status')
  .valid(['signing-up','forgot-password','switching-email']).required(),
  password: joi.string()
  .label('Password').min(2).max(50)
  .when('status', {
    is: joi.valid('signing-up'),
    then: joi.required()
  })
  //.regex(/^(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z])(?=\D*\d)(?=[^!#%]*[!#%])[A-Za-z0-9!#%]{8,32}$/)
  //.required().options({language: {string: 'dude'} })
};


var beginForgotPasswordSchema = object({
  username: rString.label('Username'),
  timeZone: string.label('Time Zone')
});

var finishForgotPasswordSchema = object({
  username: rString.label('Username'),
  forgotPasswordCode: rString.label('Forgot Password Code'),
  securityQuestion: rBigString.label('Security Question'),
  securityAnswer: rString.label('Security Answer'),
  password: rString.label('Password'),
  newPassword: rString.label('New Password'),
  timeZone: string.label('Time Zone')
});

var findOneUserByNameAndForgotPasswordCodeSchema = object({
  username: rString.label('Username'),
  forgotPasswordCode: rString.label('Forgot Password Code'),
  timeZone: string.label('Time Zone')
});

var beginForgotESignatureSchema = object({
  username: rString.label('Username'),
  timeZone: string.label('Time Zone')
});

var resetAccessSchema = object({
  username: rString.label('Username'),
  timeZone: string.label('Time Zone')
});

var finishForgotESignatureSchema = object({
  username: rString.label('Username'),
  forgotESignatureCode: rString.label('Forgot ESignature Code'),
  currentPassword: rString.label('Current Password'),
  newESignaturePIN: rString.label('New eSignature PIN'),
  confirmNewESignaturePIN: rString.label('Confirm eSignature PIN'),
  performedAction: bigString.label('Performed Action').allow(''),
  timeZone: string.label('Time Zone')
});

var findOneUserByNameAndForgotESignatureCodeSchema = object({
  username: rString.label('Username'),
  forgotESignatureCode: rString.label('Forgot ESignature Code'),
  timeZone: string.label('Time Zone')
});

var abortCodeSchema = object({
  _id: joi.any().required().label('User Id'),
  code: rString.label('Forgot Password Code'),
  timeZone: string.label('Time Zone')
});

//below two schema is made by surjeet.b@productivet.com
var verifyVerificationCodeSchema = object({
  _id: joi.any().required().label('User Id'),
  verificationCode: rString.label('Verification Code'),
  timeZone: string.label('Time Zone')
});

var verifyemailRestrictionSchema = object({
  // _id: joi.any().required().label('User Id'),
  username: rString.label('Username'),
  timeZone: string.label('Time Zone')
});

var isConCurrentUserActiveSchema = object({
  _id: joi.any().required().label('User Id'),
  _adminId: joi.any().label('Admin Id'),
  isConCurrentUserActive: rBool.label('isConCurrentUserActive ?'),
  username: joi.any().label('username'),
  timeZone: string.label('Time Zone')
});

var isConCurrentUserActiveSchemaByUserName = object({
  _id: joi.any().required().label('User Id'),
  isConCurrentUserActive: rBool.label('isConCurrentUserActive ?'),
  username: joi.any().label('username'),
  timeZone: string.label('Time Zone')
});

var isConCurrentUserAdminSchema = object({
  _id: joi.any().required().label('User Id'),
  isConCurrentUserAdmin: rBool.label('isConCurrentUserAdmin ?'),
  timeZone: string.label('Time Zone')
});

var verifyresetPasswordSchema = object({
  passwordGenerationMethod: plainString
  .valid('System','Admin')
  .label('Password Generation Method')
  .required(),
  newPassword: joi.string()
  .label('New Password').min(2).max(50),
  auditInfo: object({
    resetUserId: rString.label('resetUserId'),
    resetByUserId: rString.label('resetByUserId'),
    resetDate: date.label('resetDate'),
    reset: rString.label('reset'),
  }),
  timeZone: string.label('Time Zone')
});

var validateNewPasswordSchema = object({
  newPassword: string.label('newPassword'),
  performedAction: bigString.label('Performed Action').allow(''),
  timeZone: string.label('Time Zone')
});

var finishChangePasswordSchema = object({
  password: rString.label('Password'),
  newPassword: rString.label('New Password'),
  timeZone: string.label('Time Zone')
});

var updateLicenseAgreementSchema = object({
  licenseAgreementStatus: string.label('License Agreement Status'),
  licenseAcceptanceDateTime: date.label('Date & Time of License Acceptance'),
  licenseAgreementVersion : number.label('License Agreement Version'),
  timeZone: string.label('Time Zone')
})

module.exports = {
  beginSignUpSchema: beginSignUpSchema,
  updateUserSchema: updateUserSchema,
  finishSignUpSchema: finishSignUpSchema,
  beginForgotPasswordSchema: beginForgotPasswordSchema,
  finishForgotPasswordSchema: finishForgotPasswordSchema,
  findOneUserByNameAndForgotPasswordCodeSchema: findOneUserByNameAndForgotPasswordCodeSchema,
  abortCodeSchema: abortCodeSchema,
  verifyVerificationCodeSchema: verifyVerificationCodeSchema,
  verifyemailRestrictionSchema: verifyemailRestrictionSchema,
  isConCurrentUserActiveSchema: isConCurrentUserActiveSchema,
  isConCurrentUserAdminSchema: isConCurrentUserAdminSchema,
  verifyresetPasswordSchema: verifyresetPasswordSchema,
  validateNewPasswordSchema: validateNewPasswordSchema,
  finishChangePasswordSchema: finishChangePasswordSchema,
  isConCurrentUserActiveSchemaByUserName: isConCurrentUserActiveSchemaByUserName,
  beginForgotESignatureSchema : beginForgotESignatureSchema,
  finishForgotESignatureSchema : finishForgotESignatureSchema,
  resetAccessSchema: resetAccessSchema,
  findOneUserByNameAndForgotESignatureCodeSchema : findOneUserByNameAndForgotESignatureCodeSchema,
  updateLicenseAgreementSchema: updateLicenseAgreementSchema,

}
