'use strict'
/**
 * @name questions-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var anyArray = types.anyArray;
var rBool = types.rBool;
var bool = types.bool;
var number = types.number;
var string = types.string;
var required = types.required;
var stringArray = types.stringArray;
var any = types.any;
var rBigString = types.bigString;
var bigString = types.bigString;


var schema = {
  title: rBigString.label('Questions title'),
  tags: anyArray.label('Tags array'),
  type: object({
    _id: rId.label('Type id'),
    title: rString.label('Type title'),
    format: object({
      _id: rId.label('Format id'),
      title: rString.label('Format title'),
      metadata: any.label('Metadata required for the format')
    })
  }).required().label('Attribute type'),
  validation: any,
  isInstructionForUser: bool.label('Is Instruction for user'),
  instructionForUser: any.label('Instruction for user').allow(''),
  addBarcodeRule: bool.label('Add Barcode Rule'),
  addBarcodeRulePrefix: any.label('Add Barcode Rule Prefix').allow(''),
  addBarcodeRuleSuffix: any.label('Add Barcode Rule Suffix').allow(''),
  isActive: rBool.label('Active?'),
  auditTrailInfo: any,
  timeZone: string.label('Time Zone')
};

module.exports = schema;
