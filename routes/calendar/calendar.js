'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'calendar',
    collection: 'calendar',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    /**
     * @swagger
     * /calendar:
     *   get:
     *     tags: [Schedule]
     *     description: Returns s schedule's object
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: q[isSetReviewFrequency]
     *         description: Is Set Review Frequency flag
     *         in: query
     *         required: true
     *         type: string
     *       - name: q[scheduleuniqueId]
     *         description: Schedule Unique Id
     *         in: query
     *         required: true
     *         type: string
     *       - name: schedule
     *         description: schedule object
     *         in: body
     *         required: true
     *         schema:
     *           type: array
     *           items:
     *              $ref: '#/definitions/SchedulePost'
     *     responses:
     *       200:
     *         description: An Object of schedule
     *         schema:
     *           $ref: '#/definitions/ScheduleNew'
     */

    GET: {},
    PUT: false,
    // PATCH: false,
    POST: false
  }
});

module.exports = route.router;
