'use strict'
/**
 * @name calendar-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var string = types.string;
var rDate = types.rDate;
var any = types.any;
var bool = types.bool;

var schema = {
  entry: any.label("Entry Object"),
  skippedMasterApps: any.label("Skipped master apps"),
  isSkippedAll: bool.label("Is skipped all"),
  isSkippedProgressively: bool.label("Is skipped all")
  // isPartiallySkipped: bool.label("Partially Skipped")
};

module.exports = schema;
