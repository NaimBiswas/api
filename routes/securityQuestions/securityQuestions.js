'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');

//This call will automatically build following routes
// GET (pagged)
// GET/:_id
// PUT/:_id
// POST
var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'securityQuestions',
    collection: 'securityQuestions',
    schema: schema,
    softSchema: softSchema
  },
  types: {
    PUT: false,
    PATCH: false,
    GET: {
      ALL: {
        insecure: true
      }
    }
  }
});


module.exports = route.router;
