var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rBigString = types.rBigString;
var rBool = types.rBool;
var string = types.string;


var schema = {
  title: rBigString.label('Module Title'),
  isActive: rBool.label('Module Active?'),
  timeZone: string.label('Time Zone')
};


module.exports = schema;
