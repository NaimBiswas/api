'use strict'
/**
 * @name procedures-schema
 * @author Shoaib Ganja <shoaib.g@productivet.com>
 *
 * @version 1.0
 */
var types = require('../../lib/validator/types');

var object = types.object.bind(types);
var rId = types.rId;
var rString = types.rString;
var array = types.array;
var rBool = types.rBool;
var number = types.number;
var bool = types.bool;
var string = types.string;
var required = types.required;
var stringArray = types.stringArray;
var any = types.any;
var rBigString = types.bigString;
var bigString = types.bigString;
var id = types.id;
var extraLargeString = types.extraLargeString;
var rExtraLargeString = types.rExtraLargeString;

var schema = {
  entityId : id.label('Entity Id'),
  entityAttributeSeqId : string.label('Entity Attribute Sequence Id'),
  linkedEntityId : id.label('Linked Entity Id'),
  linkedEntityKeyAttributeSeqId : string.label('Linked Entity Key Attribute Sequence Id'),
};

module.exports = schema;
