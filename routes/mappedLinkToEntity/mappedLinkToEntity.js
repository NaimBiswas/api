'use strict'
var schema = require('./schema');
var softSchema = require('./softSchema');
var entityRecords = require('../entityRecords');
var dbConfig = require('../../lib/db/db');
var db = require('../../lib/db');
var log = require('../../logger');
var _ = require('lodash');
var logger = require('../../logger');
var currentFileName = __filename;

var route = require('../../lib/routeBuilder').build({
  entity: {
    name: 'mappedLinkToEntity',
    collection: 'mappedLinkToEntity',
    schema: schema,
    softSchema: softSchema
  }
});
module.exports = route.router;

/**
 * @swagger
 * definition:
 *    MappedLinkToEntity:
 *      properties:
 *        id:
 *          type: string
 *        entityId:
 *          type: string
 *        entityAttributeSeqId:
 *          type: string
 *        linkedEntityId:
 *          type: string
 *        linkedEntityKeyAttributeSeqId:
 *          type: string
 *        linkedEntityKeyAttributeFormat:
 *          type: string
 */

/**
 * @swagger
 * /mappedLinkToEntity:
 *  get:
 *    tags: [Entity]
 *    description: List of mappedLinkToEntity
 *    produces:   
 *      - application/json
 *    parameters:
 *      - name: page
 *        description: Page Number
 *        in: query
 *        required: false
 *        type: string
 *      - name: pagesize
 *        description: Page Size
 *        in: query
 *        required: false
 *        type: string
 *    responses:
 *      200:
 *        description: An array of mappedLinkToEntity
 *        schema: 
 *          $ref: '#/definitions/MappedLinkToEntity'     
 */