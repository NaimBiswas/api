/**
* @name Qulification Performance - q service for export window
* @author Yamuna Sardhara <yamuna.s@productivet.com>
*
* @version 1.0
*/
'use strict';

var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('../lib/logger').configure(__dirname, enableBunyan);
var amqp = require('amqplib/callback_api');
var config = require('../config/config.js');
var fs = require('fs-extra');
var Pusher = require('pusher');
var Handlebars = require("handlebars");
var db = require('../lib/resources').db;
var createUrlReplaceString = config.hbspath.replace('loggerPublisher/', '');
var content = fs.readFileSync(createUrlReplaceString + 'export-window/html.hbs', 'utf8');
var pusher = new Pusher({
  encrypted: true
});

var exportManipulator = require('../routes/qulificationperformance/exportWindows.js');
function startConnection(){
  amqp.connect('amqp://localhost', function(err, conn) {
    if (err) {
      console.error("[AMQP]", err.message);
      return setTimeout(startConnection, 5000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] reconnecting");
      return setTimeout(startConnection, 5000);
    });
    conn.createChannel(function (err, ch) {
      var q = config.serverName;
      ch.assertQueue(q, { durable: true });
      ch.consume(q, function (msg) {
        var req =  JSON.parse(msg.content.toString());
        exportManipulator.exportWindowManipulation(req).then(function(resp){
              db.collection('windowQueueFiles').update({ _id: db.ObjectID(resp.savequeueId) }, { $set: { isGenerate: true, status: 'Done' } }, function (err, result) {
              });
        })
      }, {noAck: true});
    });
  });
}
startConnection();
