'use strict';

module.exports = (function() {

  var QuePublisher = require('./quePublisher');
  var quePublisher = QuePublisher.Create();
  var config = require('../config/config.js');

  var schedulePublisher = {
    connect: function(){
      console.log("** schedule before connection")
      quePublisher.connect(config.mq.connection, config.mq.scheduleMq.queue, (pub) => {
        console.log("** schedule after connection")
      });
    },
    publish: (key, data, cb) => {
      console.log("** schedule in publish")
      if (data) {
        if(quePublisher.connection){
            quePublisher.publish(key, data, config.mq.scheduleMq.queue, (pubInner,err) => {

                if(cb){
                  cb(pubInner,err)
                }
              console.log("** schedule is published")

            });
        }
        else{
          if(cb){
            cb(null,"Not connected");
          }
        }
      }
    }
  };
  return schedulePublisher;
})();
