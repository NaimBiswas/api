/*eslint consistent-this: [2, "that"]*/

//'use strict';

module.exports = (() => {
    
      var Amqp = require('amqplib/callback_api');
      var quePublisher = {
    
        Create: function () {
    
          //use that as this pattern
          var that = this;
    
          //connect
          that.connect = (host, queName, callback) => {
    
            that.host = host;
            that.queName = queName;

            var connectionString = ''.concat('amqp://', host);
    
            Amqp.connect(connectionString, (err, con) => {
              if (err) {
              //  console.error("[AMQP]", err.message);
                return;
                // return setTimeout(start, 1000);
              }

              con.on("error", function (err) {
                if (err.message !== "Connection closing") {
                //  console.error("[AMQP] conn error", err.message);
                }
              });
    
              con.on("close", function () {
              //  console.error("[AMQP] Closed");
                return ;
              });
    
              //create channel
              con.createChannel((err, ch) => {
    
                if (err) {
                  //console.log(JSON.stringify(err));
                  return;
                }

                ch.assertQueue(queName, {durable: false});
    
                //store important variables
                that.channel = ch;
                that.queName = queName;
                that.connection = con;
    
                //callback
                if (callback) {
                  callback(that);
                }
    
              });
            });
          };
    
          that.publish = (keys, data, queName, callback) => {
            var ch = that.channel;
            // var queName = that.queName;
            var msg = JSON.stringify(data);
    

            console.log("queName",queName);
            try{
              ch.sendToQueue(queName, new Buffer(msg));
            }
            catch(e){

              that.connect(that.host,that.queName,function(that){
                // that.publish(keys, data, queName);
              });

              if (callback) {
                callback(that,e);
                return
              }

            }
    
            //callback
            if (callback) {
              callback(that);
            }
          };
    
          that.close = () => {
            var con = that.connection;
    
            setTimeout(() => {
              con.close();
              return;
            }, 500);
          };
    
          return that;
        }
      };
    
      return quePublisher;
    })();
    