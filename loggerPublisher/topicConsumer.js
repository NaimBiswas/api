/*eslint consistent-this: [2, "that"]*/

'use strict';

module.exports = (() => {

  var Amqp = require('amqplib/callback_api');

  var TopicConsumer = {

    Create: function () {

      //use that as this pattern
      var that = this;

      //connect
      that.connect = (host, exchange, keys, callback, user, password) => {
        
        var connectionString = ''.concat('amqp://', host);

        if(user && password) connectionString = ''.concat('amqp://', user, ':', password, '@',  host);

        console.log(connectionString);
        
        Amqp.connect(connectionString, (e, con) => {
          if (e) {
            console.log(JSON.stringify(e));
            return;
          }

          //create channel
          con.createChannel((err, ch) => {

            if (err) {
              console.log(JSON.stringify(err));
              return;
            }

            ch.assertExchange(exchange, 'topic', { durable: false });
            ch.assertQueue('', { exclusive: true }, (er, q) => {

              //bind
              keys.forEach((key) => {
                ch.bindQueue(q.queue, exchange, key);
              });

              //store important variables
              that.channel = ch;
              that.queue = q;
              that.exchange = exchange;
              that.connection = con;

              //callback
              if (callback) {
                callback(that);
              }
            });

          });
        });
      };

      that.consume = (callback) => {
        var q = that.queue;
        var ch = that.channel;

        //consume
        ch.consume(q.queue, (msg) => {
          if (callback) {
            callback(that, msg.content.toString());
          }
        }, { noAck: true });

      };

      that.close = () => {
        var con = that.connection;

        setTimeout(() => {
          con.close();
          return;
        }, 500);
      };

      return that;
    }
  };

  return TopicConsumer;
})();
