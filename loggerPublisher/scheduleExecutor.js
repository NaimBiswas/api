/*eslint consistent-this: [2, "that"]*/

'use strict';

var config = require('../config/config.js');


if(!global.hasSessionLogger){
  var session_logger = require('../lib/logger').configure(__dirname, false);
}

var schema = require('../routes/schedules/schema');
var softSchema = require('../routes/schedules/softSchema');
var db = require('../lib/resources').db;
var dbConfig = require('../lib/db/db');
var q = require('q');
var moment = require('moment-timezone');
var _ = require('lodash');
var uuid = require('node-uuid');
// var Worker = require('webworker-threads').Worker;

module.exports = (function(){


  var scheduleId = "";

  var clientOffset=-330;

  function afterPost(req){

    scheduleId = req.data._id;

    storeEntries(generateEntries(req.data,req.headers));
  }

  function afterPatch(req) {

    scheduleId = req.data._id;

    //req.data contains the posted / patched
    //remove existing data from calendar for
    //req.data and
    //add new requested data
    //Start: QC3-4534 - Improvement - Once any Frequency is updated from the set frequency in administration then all past dated schedules as well as all completed schedules should be there as per previous settings.
    //Changed By: Vaibhav Vora
    //Description : code to check whether the frequency is completed or past dated.
     //QC3-5899-vaibhav
    Date.prototype.stdTimezoneOffset = function () {
          var jan = new Date(this.getFullYear(), 0, 1);
          var jul = new Date(this.getFullYear(), 6, 1);
          return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
      }
     Date.prototype.dst = function () {
          return this.getTimezoneOffset() < this.stdTimezoneOffset();
      }
    var currDate=new Date();
      var machineDate = new Date();
    var machineOffset = machineDate.getTimezoneOffset();
      clientOffset = machineOffset;
      clientOffset = parseInt(req.headers['client-offset']) || -330;
      //CHECK FOR THE DST
      if (machineDate.dst()) {
          machineOffset += 60;
      }
    //   $or:[
    //   {author:"Daniel"},
    //   {author:"Jessica"}
    // ]
    var shedSchemaId=_.isUndefined( req.data.schema)?req.data.master._id:req.data.schema._id;
    //Code for single form schedule
    //Start:: QC3-5075 - Review schedule : for single and master app when review with in hours only changed then in review schedule today date's schedule get disappear
    //Changed By: Vaibhav Vora
    if(!_.isUndefined( req.data.schema)){
      //QC3-8396, QC3-8398 by Yamuna
      if(_.isUndefined(db[shedSchemaId])){
        // log.log("Table is not bind with mongoskin DB",'info');
        var entities = [];
        entities.push({
          collection: shedSchemaId,
          audit: true,
          index: {
            title: ''
          }
        });
        // log.log("Here for add collection in db object",'info');
        dbConfig.configure.apply(this, entities);
        // log.log("Adding collection to DB is Completed",'info');
      }
      // log.log("Collection is with db :: " + shedSchemaId,'info');
        q.all([db
        .calendar.find({scheduleId: req.data._id}).toArray(),db[shedSchemaId].find({}).toArray()])
        .then(function(result){
        var dt=result[0];
        var dt1=result[1];
                var remainedData=[];
                var scheduleIds=[];
                var scheduleUniqueId=[];
                 var calandarEntries = generateEntries(req.data,req.headers);
                _.forEach(dt,function(remained){
                // remained.
                //Start :: QC3-4893-Master app & single app : Overdue status containing schedules get generated again and again when user change/update frequency
                //Changed By: Vaibhav Vora
                //var findSchedule=_.find(calandarEntries,{date: remained.date,time:remained.time,keyvalues:remained.keyvalues,title:remained.title,type:remained.type} );
                  //Start::QC3-5390-Schedule : with overdue status duplicate schedule is not generated when initially there is no attribute and then user add attributes
                  //Changed By:: Vaibhav Vora
                  var findSchedule=_.find(calandarEntries,function(r){
                    return (new Date(r.date).getTime() == new Date(remained.date).getTime() && new Date(r.time).getTime() == new Date(remained.time).getTime()
                    && _.isEqual(r.keyvalues,remained.keyvalues) && r.title==remained.title && r.type==remained.type
                    && new Date(r.overdueDate).getTime() == new Date(remained.overdueDate).getTime() && new Date(r.overdueTime).getTime() == new Date(remained.overdueTime).getTime());
                  });
                  //End::QC3-5390-Schedule : with overdue status duplicate schedule is not generated when initially there is no attribute and then user add attributes
                var tempCurrentForCompare = new Date(currDate.getTime() + (machineOffset * 60 * 1000) - (clientOffset * 60 * 1000));
                var overdueDate= new Date(remained.overdueDate.getTime() + (machineOffset * 60 * 1000) - (clientOffset * 60 * 1000));
                //QC3-6951-vaibhav
                if((overdueDate<tempCurrentForCompare  && (_.isUndefined(findSchedule))) || (remained.isSkippedAll && remained.isSkippedAll==true)){
                   remainedData.push(remained);
                   scheduleIds.push(remained._id);
                if(!_.isUndefined(remained.scheduleuniqueId)){
                     scheduleUniqueId.push(remained.scheduleuniqueId);
                   }

                }
                //End :: QC3-4893-Master app & single app : Overdue status containing schedules get generated again and again when user change/update frequency
                  _.forEach(dt1,function(entry){
                    //  var calendarIds=[];
                    //  _.forEach(entry.calendarIds,function(a){
                    //       calendarIds.push(db.ObjectID(a));
                    //  });
                    //  console.log("remained._id"+remained._id +"--"+"calendarIds::"+JSON.stringify(calendarIds))
                    if(!_.isUndefined(entry.calendarIds)){
                      if(entry.calendarIds.indexOf(remained._id.toString()) > -1){

                        if(entry.reviewStatus == "Done" || entry.reviewStatus == "Pending" || entry.reviewStatus == "Draft"){
                          remainedData.push(remained);
                          scheduleIds.push(remained._id);
                          scheduleUniqueId.push(remained.scheduleuniqueId);
                        }
                      }
                     }
                    });

                });
                    _.forEach(remainedData,function(res2){
                    //var findData=_.find(calandarEntries,{time:res.time,date:res.date,type:res.type});
                    _.remove(calandarEntries,function(n){
                        //Start:QC3-5023 - Schedules with overdue status don't get generated when user update the frequency
                        //Changed By:: Vaibhav
                        var removeValue= (new Date(n.time).getTime()== new Date(res2.time).getTime() && n.type==res2.type && n.date.getTime()==res2.date.getTime()
                        && n.title==res2.title && n.isActive==res2.isActive && new Date(n.overdueTime).getTime()== new Date(res2.overdueTime).getTime() &&  n.overdueDate.getTime()==res2.overdueDate.getTime());
                        //End:QC3-5023 - Schedules with overdue status don't get generated when user update the frequency
                        if(!_.isUndefined(n.keyvalues) && !_.isUndefined(res2.keyvalues) && removeValue ){
                          if(n.keyvalues.length!=res2.keyvalues.length){
                            removeValue=false;
                          }else{
                            if(_.isEqual(n.keyvalues, res2.keyvalues)){
                              //EMPTY IF TO PASS ELSE PART
                            }else{
                              removeValue=false;
                            }
                          }
                        }
                        return removeValue;
                    });
                });
                //QC3-8477-Vaibhav
                //code for removing review schedule whose frequncy schedules are not there
                var tempcalandarEntries=_.cloneDeep(calandarEntries);
                _.forEach(tempcalandarEntries,function(clenty){
                  if(clenty && clenty.isSetReviewFrequency==true){
                    var tempCalendar=_.find(tempcalandarEntries,{"scheduleuniqueId":clenty.scheduleuniqueId,"isSetReviewFrequency":false});
                    if(_.isUndefined(tempCalendar)){
                      _.remove(calandarEntries,function(rc){
                        return rc.scheduleuniqueId == clenty.scheduleuniqueId;
                      });
                    }
                  }
                });

              q.all([db
              .calendar
            // .removeManyAsync({scheduleId: req.data._id,date:{$gte:currDate},scheduleId:{$nin:scheduleIds}})])
            .removeManyAsync({$and:[{scheduleId: req.data._id},{_id:{$nin:scheduleIds}},{scheduleuniqueId:{$nin:scheduleUniqueId}}]})])
              .then(function (data) {



                // miralhear
                // db.schedules.update({_id:db.ObjectId(req.data._id)},{scheduleInProcess:false})
                // (req.status ? res.status(req.status) : res).json(req.data);
                storeEntries(calandarEntries);
              });
        })
        //Code for master app
    }else{
      q.all([db
        .calendar.find({scheduleId: req.data._id}).toArray(),
      db.masterqcsettings.find({_id: db.ObjectID(shedSchemaId)}).toArray()])
        .then(function(result){
        var dt=result[0];
        var tbl=result[1];

        var singleEntries= tbl[0].qcform;
        var qArray=[];
        _.forEach(singleEntries,function(ent){



          // if(!db[ent.id]){
          //   var entities = [];
          //   entities.push({
          //     collection: ent.id,
          //     audit: true,
          //     index: {
          //       title: ''
          //     }
          //   });
          //   // log.log("Here for add collection in db object");
          //   dbConfig.configure.apply(this, entities);
          // }


          // QC3-8628 - kajal
          if(_.isUndefined(db[ent.id])){
            // log.log("Table is not bind with mongoskin DB");
            var entities = [];
            entities.push({
              collection: ent.id,
              audit: true,
              index: {
                title: ''
              }
            });
            // log.log("Here for add collection in db object");
            dbConfig.configure.apply(this, entities);
            // log.log("Adding collection to DB is Completed");
          }


          qArray.push(db[ent.id].find({}).toArray());

          // end - QC3-8628 - kajal

        });
        var remainedData=[];
        var scheduleIds=[];
        var scheduleUniqueId=[];
        q.all(qArray).then(function(resultofEntry){

                  var calandarEntries = generateEntries(req.data,req.headers);
                  _.forEach(dt,function(remained){
                    //Start :: QC3-4893-Master app & single app : Overdue status containing schedules get generated again and again when user change/update frequency
                    //Changed By: Vaibhav Vora
                     //var findSchedule=_.find(calandarEntries,{date: remained.date,time:remained.time,keyvalues:remained.keyvalues,title:remained.title,type:remained.type} );
                     //Start::QC3-5390-Schedule : with overdue status duplicate schedule is not generated when initially there is no attribute and then user add attributes
                  //Changed By:: Vaibhav Vora
                    var findSchedule = _.find(calandarEntries,function(r){
                      return (new Date(r.date).getTime() == new Date(remained.date).getTime() && new Date(r.time).getTime() == new Date(remained.time).getTime()
                      && _.isEqual(r.keyvalues,remained.keyvalues) && r.title==remained.title && r.type==remained.type &&
                      new Date(r.overdueDate).getTime() == new Date(remained.overdueDate).getTime() && new Date(r.overdueTime).getTime() == new Date(remained.overdueTime).getTime());
                    });
                  //End::QC3-5390-Schedule : with overdue status duplicate schedule is not generated when initially there is no attribute and then user add attributes

                    var tempCurrentForCompare = new Date(currDate.getTime() + (machineOffset * 60 * 1000) - (clientOffset * 60 * 1000));
                    //if(remained.date<tempCurrentForCompare && (_.isUndefined(findSchedule))){
                       var overdueDate= new Date(remained.overdueDate.getTime() + (machineOffset * 60 * 1000) - (clientOffset * 60 * 1000));
                      //QC3-6951-vaibhav
                      if((overdueDate<tempCurrentForCompare && (_.isUndefined(findSchedule))) || (remained.isSkippedAll && remained.isSkippedAll==true) || (remained.entry && remained.entry.entryId)){
                      remainedData.push(remained);
                      scheduleIds.push(remained._id);
                      if(!_.isUndefined(remained.scheduleuniqueId)){
                              scheduleUniqueId.push(remained.scheduleuniqueId);
                      }
                    }
                    //End :: QC3-4893-Master app & single app : Overdue status containing schedules get generated again and again when user change/update frequency
                     for(var i=0;i<qArray[0].length;i++){
                      var dt1=resultofEntry[0][i];
                  // remained.
                    //_.forEach(dt1,function(entry){
                      //  var calendarIds=[];
                      //  _.forEach(entry.calendarIds,function(a){
                      //       calendarIds.push(db.ObjectID(a));
                      //  });
                      //  console.log("remained._id"+remained._id +"--"+"calendarIds::"+JSON.stringify(calendarIds))
                     // console.log(JSON.stringify(entry.calendarIds) +"-"+remained._id);
                      if(!_.isUndefined(dt1.calendarIds)){
                        if(dt1.calendarIds.indexOf(remained._id.toString()) > -1){

                          if(dt1.reviewStatus == "Done" || dt1.reviewStatus == "Pending" ){
                            remainedData.push(remained);
                            scheduleIds.push(remained._id);
                            scheduleUniqueId.push(remained.scheduleuniqueId);
                          }
                        }
                      }
                  //  });
                    }
                  });
                    _.forEach(remainedData,function(res1){
                    //var findData=_.find(calandarEntries,{time:res.time,date:res.date,type:res.type});
                    _.remove(calandarEntries,function(n){
                      //Start:QC3-5023 - Schedules with overdue status don't get generated when user update the frequency
                      //Changed By:: Vaibhav
                        var removeValue= (new Date(n.time).getTime()== new Date(res1.time).getTime() && n.type==res1.type && n.date.getTime()==res1.date.getTime()
                        && n.title==res1.title && n.isActive==res1.isActive && new Date(n.overdueTime).getTime()== new Date(res1.overdueTime).getTime() && n.overdueDate.getTime()==res1.overdueDate.getTime());
                      //End:QC3-5023 - Schedules with overdue status don't get generated when user update the frequency
                    if(!_.isUndefined(n.keyvalues) && !_.isUndefined(res1.keyvalues) && removeValue ){
                      if(n.keyvalues.length!=res1.keyvalues.length){
                        removeValue=false;
                      }else{
                        if(_.isEqual(n.keyvalues, res1.keyvalues)){
                          //EMPTY IF TO PASS ELSE PART
                        }else{
                          removeValue=false;
                        }
                      }
                    }
                        return removeValue;
                    });
                     //code for removing review schedule whose frequncy schedules are not there
                    _.forEach(calandarEntries,function(clenty){
                      if(clenty && clenty.isSetReviewFrequency==true){
                        var tempCalendar=_.find(calandarEntries,{"scheduleuniqueId":clenty.scheduleuniqueId,"isSetReviewFrequency":false});
                        if(_.isUndefined(tempCalendar)){
                            _.remove(calandarEntries,function(rc){
                              return rc.scheduleuniqueId == clenty.scheduleuniqueId;
                            });
                        }
                      }
                    });
                });
                //QC3-8477-Vaibhav
                //code for removing review schedule whose frequncy schedules are not there
                var tempcalandarEntries=_.cloneDeep(calandarEntries);
                _.forEach(tempcalandarEntries,function(clenty){
                  if(clenty && clenty.isSetReviewFrequency==true){
                    var tempCalendar=_.find(tempcalandarEntries,{"scheduleuniqueId":clenty.scheduleuniqueId,"isSetReviewFrequency":false});
                    if(_.isUndefined(tempCalendar)){
                      _.remove(calandarEntries,function(rc){
                        return rc.scheduleuniqueId == clenty.scheduleuniqueId;
                      });
                    }
                  }
                });

          q.all([db
            .calendar
            .removeManyAsync({ $and: [{ scheduleId: req.data._id }, { _id: { $nin: scheduleIds } }, { scheduleuniqueId: { $nin: scheduleUniqueId } }] })])
            .then(function (data) {
              storeEntries(calandarEntries);
            });
        });
      }).catch(function(err){
        console.log(err)
      });
      }

    //End:: QC3-5075 - Review schedule : for single and master app when review with in hours only changed then in review schedule today date's schedule get disappear
    //End: QC3-4534 - Improvement - Once any Frequency is updated from the set frequency in administration then all past dated schedules as well as all completed schedules should be there as per previous settings.
    //Changed By: Vaibhav Vora

  }



  function updateScheduleProcessStatus(){
    db.schedules.update({_id:db.ObjectID(scheduleId)},{
        $set: {
          scheduleInProcess:false
        }
    })
  }

  function storeEntries(entries) {
    db
    .calendar
    .insertManyAsync(entries)
    .then(function (data) {
      updateScheduleProcessStatus();
    })
    .catch(function (err) {
      updateScheduleProcessStatus();
    });
  }


  function generateEntries(schedule,reqHeaders) {
    Date.prototype.stdTimezoneOffset = function () {
        var jan = new Date(this.getFullYear(), 0, 1);
        var jul = new Date(this.getFullYear(), 6, 1);
        return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
    }

    Date.prototype.dst = function () {
        return this.getTimezoneOffset() < this.stdTimezoneOffset();
    }


  var calandarEntriesoccurence = [];
  var calandarEntries = [];
  var machineDate = new Date();
  var machineOffset = machineDate.getTimezoneOffset();
    clientOffset = machineOffset;
    clientOffset = parseInt(reqHeaders['client-offset']) || -330;
    //CHECK FOR THE DST
    if (machineDate.dst()) {
        machineOffset += 60;
    }

  var startDate = new Date(schedule.startDate);
  var endDate = new Date();

  var days;
  var hours;
  var totaldays;
  var calendarDateDaily;
  var endCompareDate;

  if(schedule.endDate !== null && schedule.occurence<=0)
  {
        endDate = new Date(schedule.endDate);
  }
  else {
    var firstDate = startDate;
    if(schedule.frequency.type === 'Daily')
    {
      days = schedule.frequency.daily.repeats.every;
      totaldays = (days * schedule.occurence)-1;
      endDate = new Date(firstDate.setDate(firstDate.getDate() + parseInt(totaldays)));
    }
    if(schedule.frequency.type === 'Hourly')
    {
      hours = schedule.frequency.hourly.repeats.every;
      totaldays = (hours * schedule.occurence)-1;
      endDate = new Date(firstDate.setDate(firstDate.getDate() + parseInt(totaldays)));
    }
    if(schedule.frequency.type === 'Weekly')
    {
      var week = schedule.frequency.weekly.repeats.every * 7;
      totaldays = (week * schedule.occurence)-1;
      endDate = new Date(firstDate.setDate(firstDate.getDate() + parseInt(totaldays)));
    }
    if(schedule.frequency.type === 'Monthly')
    {
      var newDate = new Date(startDate);
      var month;

      if(schedule.frequency.subType === 'EveryMonthly')
      {
        month = schedule.frequency.monthly.repeats.dayOfMonth.month;
      }
      else if(schedule.frequency.subType === 'WeeklyMonthly')
      {
        month = schedule.frequency.monthly.repeats.dayOfWeek.month;
      }

      totaldays = 0;
      var totalMonths = (month) * schedule.occurence;

      for (var i = 0; i < totalMonths; i++) {
          var lastDate = new Date(newDate.getFullYear(), newDate.getMonth() + 1, 0);
          totaldays = totaldays + lastDate.getDate();
          newDate = new Date(newDate.getFullYear(), newDate.getMonth() + 1, 0);
          //QC3-8863-Vaibhav
          newDate = new Date(newDate.getTime() +  (lastDate.getDate())*86400000);
      }
      //Patch for adding perfectly same days
      //endDate = new Date(firstDate.setDate(firstDate.getDate() + parseInt(totaldays)));
      endDate = new Date(firstDate.getTime() +  totaldays*86400000);

    }
    //console.log("endDate after::"+endDate.toString());
  }
    //This lines are added for fixing QC3-2216 as start date does not keeping its value
    startDate= new Date(schedule.startDate);

   //This lines are added for fixing QC3-2216 as start date does not keeping its value

  var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
  var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
  var a;

  if(schedule.frequency.type === 'Daily')
  {
  //  startDate = new Date(startDate.getTime() - (machineOffset * 60 * 1000));
    days = schedule.frequency.daily.repeats.every;
    var filteredDays = (parseInt(diffDays) / parseInt(days)) + 1;
        var dateDaily = new Date(schedule.startDate);
        //console.log(dateDaily.toString());
        //dateDaily = new Date(dateDaily.getTime() + (machineOffset * 60 * 1000));
    for(var day = 0 ; day < parseInt(filteredDays)  ; day ++)
    {
      if(day == 0){
        calendardetails(dateDaily);
      }
      else{
        var dateDailyCompareDate=new Date(dateDaily).setHours(0,0,0,0);
        endCompareDate=new Date(endDate).setHours(0,0,0,0);

        if(dateDailyCompareDate < endCompareDate){
          calendarDateDaily = dateDaily.setDate(dateDaily.getDate() + parseInt(days));
          calendardetails(calendarDateDaily);
        }

      }
    }


  }else if(schedule.frequency.type === 'Hourly')
  {
    hours = schedule.frequency.hourly.repeats.every;

    var totalhours = (diffDays+1) * 24;
    var schedulehr = totalhours / hours;
    // var filteredDays = (parseInt(diffDays) / parseInt(days)) + 1;
    a = new Date(schedule.startDate);
  //  a = new Date(a.getTime() + (machineOffset * 60 * 1000));
    var time = new Date(schedule.time);
    // time = new Date(time.getTime() + (machineOffset * 60 * 1000));
    // log.log("schedule.isdst"+schedule.isdst);
    if (schedule.isdst==true) {
      clientOffset += 60;
    }
    time=new Date(time.getTime() + (machineOffset * 60 * 1000) - (clientOffset * 60 * 1000));
    //console.log('startdate' + a);
   // a=new Date(a.getTime() - (clientOffset * 60 * 1000));
  // dateDaily   = new Date(a.getFullYear(),a.getMonth(),a.getDate(),time.getHours(),time.getMinutes(),time.getSeconds(),0);
     a.setHours(a.getHours() +time.getHours());
       a.setMinutes(a.getMinutes() +time.getMinutes());
       a.setSeconds(a.getSeconds() +time.getSeconds());
    dateDaily=a;
    var scheduleendDate;

    if(schedule.endDate !== null && schedule.occurence<=0)
    {
      scheduleendDate = new Date(schedule.endDate);
    }
    else {
      scheduleendDate = endDate;
    }
    scheduleendDate = new Date(scheduleendDate.setDate(scheduleendDate.getDate() + 1));

    for(var hour = 0 ; hour < schedulehr  ; hour ++)
    {
      //console.log("1schedule.occurence-"+schedule.occurence +"--1schedule.endDate-"+schedule.endDate);
       if(schedule.occurence>0){
        if(hour < schedule.occurence){
          //console.log("hour < schedule.occurence::"+hour +"::"+schedule.occurence);
           if(hour == 0){
          calendarDateDaily = dateDaily.setHours(dateDaily.getHours());
           }
           else{
          calendarDateDaily = dateDaily.setHours(dateDaily.getHours() + parseInt(hours));
           }
          calendarDateDaily = new Date(calendarDateDaily);
          if(new Date(dateDaily) < new Date(scheduleendDate)){
            calendardetails(calendarDateDaily);
          }
        }
      }
    else if(schedule.occurence == 0 || (schedule.endDate !== null && !_.isUndefined(schedule.endDate))){
        if(hour == 0){
        calendarDateDaily = dateDaily.setHours(dateDaily.getHours());
      }
      else{
        calendarDateDaily = dateDaily.setHours(dateDaily.getHours() + parseInt(hours));
      }
        calendarDateDaily = new Date(calendarDateDaily);
        //console.log("dateDaily::"+dateDaily.toString()+"==scheduleendDate::"+scheduleendDate.toString());
        if(new Date(dateDaily) < new Date(scheduleendDate)){
          calendardetails(calendarDateDaily);
        }
      }

    }

  }
  else if(schedule.frequency.type === 'Weekly')
  {
    var skipDays = (parseInt(schedule.frequency.weekly.repeats.every) - 1) * 7 ;
    var currentDate = new Date(startDate);
    //currentDate = new Date(currentDate.getTime() + (machineOffset * 60 * 1000));
    var startDateforskip  = startDate;
    var skipDate = new Date(startDateforskip.setDate(startDateforskip.getDate()));
    // skipDate = new Date(skipDate.getTime() + (machineOffset * 60 * 1000));
    var addincalendarDate;
    for (var weekly = 0 ; weekly < diffDays + 1 ; weekly ++)
    {
      // var weekday = new Date(currentDate.getTime() - (clientOffset * 60 * 1000)).getDay();

      var weekday = new Date(currentDate.getTime()).getUTCDay();

      a = new Date(skipDate);
      addincalendarDate = new Date(a.setDate(a.getDate() + parseInt(7)));
      var occurance= schedule.isSetReviewFrequency?(2*schedule.occurence):schedule.occurence;
      if(calandarEntries.length < occurance || schedule.occurence == 0)
      {
        var currCompareDate=new Date(currentDate).setHours(0,0,0,0);
        endCompareDate=new Date(endDate).setHours(0,0,0,0);
       if(currentDate >= skipDate && currentDate < addincalendarDate  && currCompareDate<=endCompareDate )
        {
          for(var i = 0 ; i < schedule.frequency.weekly.repeats.days.length ; i++)
          {

            if(schedule.frequency.weekly.repeats.days[i] == "Mon")
            {
              if(weekday == 1)
              {
                var date = new Date(currentDate);
                 // date = new Date(date.getTime() + (machineOffset * 60 * 1000));
                calendardetails(date);
              }
            }
            if(schedule.frequency.weekly.repeats.days[i] == "Tue")
            {
              if(weekday == 2)
              {
                  date = new Date(currentDate);
                 // date = new Date(date.getTime() + (machineOffset * 60 * 1000));
                calendardetails(date);
              }
            }
            if(schedule.frequency.weekly.repeats.days[i] == "Wed")
            {
              if(weekday == 3)
              {
                  date = new Date(currentDate);
                  //date = new Date(date.getTime() + (machineOffset * 60 * 1000));
                calendardetails(date);
              }
            }
            if(schedule.frequency.weekly.repeats.days[i] == "Thu")
            {
              if(weekday == 4)
              {
                  date = new Date(currentDate);
                  //date = new Date(date.getTime() + (machineOffset * 60 * 1000));
                calendardetails(date);
              }
            }
            if(schedule.frequency.weekly.repeats.days[i] == "Fri")
            {
              if(weekday == 5)
              {
                  date = new Date(currentDate);
                  //date = new Date(date.getTime() + (machineOffset * 60 * 1000));
                calendardetails(date);
              }
            }
            if(schedule.frequency.weekly.repeats.days[i] == "Sat")
            {
              if(weekday == 6)
              {
                  date = new Date(currentDate);
                  //date = new Date(date.getTime() + (machineOffset * 60 * 1000));
                calendardetails(date);
              }
            }
            if(schedule.frequency.weekly.repeats.days[i] == "Sun")
            {
              if(weekday == 0)
              {
                  date = new Date(currentDate);
                 // date = new Date(date.getTime() + (machineOffset * 60 * 1000));
                calendardetails(date);
              }
            }
          }
        }
      }
      var date1 = new Date(addincalendarDate).setHours(0,0,0,0);
      var currentdate = new Date(currentDate).setHours(0,0,0,0);
      if(date1 === currentdate)
      {
        skipDate = new Date(skipDate.setDate(skipDate.getDate() + parseInt(skipDays) + parseInt(7)));
        diffDays++;
        continue;
      }
     // currentDate = new Date(currentDate.setDate(currentDate.getDate() + parseInt(1)));
     currentDate = new Date(currentDate.getTime() + 86400000);

    }
  }

  else if(schedule.frequency.type === 'Monthly')
  {
    var  totalmonths = (endDate.getFullYear() - startDate.getFullYear()) * 12;
    totalmonths -= startDate.getMonth() ;
    totalmonths += endDate.getMonth() + 1;
    //QC3-8009-Vaibhav
    //var startYear = startDate.getFullYear();
    var startYear = new Date(startDate.getTime() + (machineOffset * 60 * 1000) - (clientOffset * 60 * 1000)).getFullYear();

    if(schedule.frequency.subType === 'EveryMonthly')
    {
      var repeattotalmonth = parseInt(totalmonths) / parseInt(schedule.frequency.monthly.repeats.dayOfMonth.month);
      var skipMonth = (parseInt(schedule.frequency.monthly.repeats.dayOfMonth.month) - 1);
     // var firstmonth = startDate.getMonth();
       var chkDayforMonth=startDate;
       chkDayforMonth = new Date(chkDayforMonth.getTime());
      //  // chkDayforMonth.setDate(chkDayforMonth.getDate() + parseInt(1));
        var firstmonth = (chkDayforMonth.getUTCMonth());

      for(var ct = 1 ; ct <= Math.ceil(repeattotalmonth) ; ct ++)
      {
        var totalMonthDays = new Date(startYear,firstmonth+1,0).getDate();
        for(var d = 1; d <= totalMonthDays; d++ )
        {
          if(d === parseInt(schedule.frequency.monthly.repeats.dayOfMonth.day))
          {
              date = new Date(startYear, firstmonth, d);
              // var compDate = new Date(startYear, firstmonth, d,startDate.getHours(),startDate.getMinutes(),startDate.getSeconds());
              var compDate = new Date(startYear, firstmonth, d);
              //  date.setHours(startDate.getHours());
              //   date.setMinutes(startDate.getMinutes());
              //   date.setSeconds(startDate.getSeconds());
             //Start:QC3-5352 - Frequency and review schedule : for single and master app start date of monthly is not considered

             //QC3-10001
             compDate = new Date(compDate.getTime() + (
                moment.tz.zone(reqHeaders['client-tz']).parse(moment(compDate))
                * 60 * 1000));

             //Created By:Vaibhav Vora
            if(compDate >= startDate && compDate <= endDate) //mhear
            {
              date = new Date(date.getTime() - (machineOffset * 60 * 1000)  + (clientOffset * 60 * 1000));
              //End:QC3-5352 - Frequency and review schedule : for single and master app start date of monthly is not considered
              calendardetails(date);
            }
          }
        }

        firstmonth = firstmonth + (skipMonth + 1);
      }
    }
    else if(schedule.frequency.subType === 'WeeklyMonthly')
    {

      if(schedule.frequency.monthly.repeats.dayOfWeek.every === 'first'
      || schedule.frequency.monthly.repeats.dayOfWeek.every === 'second'
      || schedule.frequency.monthly.repeats.dayOfWeek.every === 'third'
      || schedule.frequency.monthly.repeats.dayOfWeek.every === 'fourth' )
      {

          setWeeklyMonthly(machineOffset, clientOffset);
      }

    }

  }

  function setWeeklyMonthly(mcdiff, clientOffset) {

    // var startDate = new Date(schedule.startDate);
    // var endDate = new Date(schedule.endDate);
    var  totalmonths = (endDate.getFullYear() - startDate.getFullYear()) * 12;
    totalmonths -= startDate.getMonth() ;
    totalmonths += endDate.getMonth() + 1;
    //var startYear = startDate.getFullYear();
    var startYear = new Date(startDate.getTime() + (machineOffset * 60 * 1000) - (clientOffset * 60 * 1000)).getFullYear();
    var repeatmonthWeekly = parseInt(totalmonths) / parseInt(schedule.frequency.monthly.repeats.dayOfWeek.month);
    var skipWeeklyMonth = (parseInt(schedule.frequency.monthly.repeats.dayOfWeek.month) - 1);
    // var chkDayforMonth = new Date(startDate.getTime() - (clientOffset * 60 * 1000));

    //By miral QC3-9177 -- #offsetIssue
    //offset is added with momment because of DST problem
    var chkDayforMonth = new Date(startDate.getTime() - (
        moment.tz.zone(reqHeaders['client-tz']).parse(moment(startDate))
        * 60 * 1000));

      //  // chkDayforMonth.setDate(chkDayforMonth.getDate() + parseInt(1));
        var firstmonthWeekly = (chkDayforMonth.getUTCMonth());
   // var firstmonthWeekly = (startDate.getMonth()) + parseInt(skipWeeklyMonth);
    // var chkDayforMonth=startDate;
    // chkDayforMonth.setDate(chkDayforMonth.getDate() + parseInt(1));
   // var firstmonthWeekly = (startDate.getUTCMonth());

    for(var ct = 1 ; ct <= Math.ceil(repeatmonthWeekly) ; ct ++)
    {
      var firstDate = new Date();
      var lastDate = new Date();
      if(schedule.frequency.monthly.repeats.dayOfWeek.every === 'first')
      {
        firstDate = new Date(startYear,firstmonthWeekly,1);
        lastDate = new Date(startYear, firstmonthWeekly, 7);
      }
      else if(schedule.frequency.monthly.repeats.dayOfWeek.every === 'second')
      {
        firstDate = new Date(startYear,firstmonthWeekly,8);
        lastDate = new Date(startYear, firstmonthWeekly, 14);
      }
      else if(schedule.frequency.monthly.repeats.dayOfWeek.every === 'third')
      {
        firstDate = new Date(startYear,firstmonthWeekly,15);
        lastDate = new Date(startYear,firstmonthWeekly,21);
      }
      else if(schedule.frequency.monthly.repeats.dayOfWeek.every === 'fourth')
      {
        firstDate = new Date(startYear,firstmonthWeekly,22);
        lastDate = new Date(startYear,firstmonthWeekly,28);
      }
      //console.log("fDate-"+firstDate.toString()+"::lDate-"+lastDate.toString());
    //  firstDate = new Date(firstDate.getTime() + (mcdiff * 60 * 1000));
    //  lastDate = new Date(lastDate.getTime() + (mcdiff * 60 * 1000));
      for(var idate = firstDate; idate <= lastDate; idate.setDate(idate.getDate() + 1))
      {
         var day = idate.getDay();
          // var day = new Date(idate.getTime() - (clientOffset * 60 * 1000)).getDay();
        if(schedule.frequency.monthly.repeats.dayOfWeek.day === 'monday' && day === 1
        || schedule.frequency.monthly.repeats.dayOfWeek.day === 'tuesday' && day === 2
        || schedule.frequency.monthly.repeats.dayOfWeek.day === 'wednesday' && day === 3
        || schedule.frequency.monthly.repeats.dayOfWeek.day === 'thursday' && day === 4
        || schedule.frequency.monthly.repeats.dayOfWeek.day === 'friday' && day === 5
        || schedule.frequency.monthly.repeats.dayOfWeek.day === 'saturday' && day === 6
        || schedule.frequency.monthly.repeats.dayOfWeek.day === 'sunday' && day === 0)
        {
            var dateMonthly = new Date(idate);
            var compDate = new Date(dateMonthly);
            var compEndDate=new Date(endDate);
            //Patch for daylight issue for month
            if(startDate.getHours()!=endDate.getHours()){
            compEndDate=new Date(endDate.getFullYear(), endDate.getMonth(),endDate.getDate(),startDate.getHours(),startDate.getMinutes(),startDate.getSeconds());
            compDate = new Date(dateMonthly.getFullYear(), dateMonthly.getMonth(),dateMonthly.getDate(),startDate.getHours(),startDate.getMinutes(),startDate.getSeconds());
            }
            //Start:QC3-5352 - Frequency and review schedule : for single and master app start date of monthly is not considered
             //Created By:Vaibhav Vora
           //console.log("startDate-"+startDate.toString()+"::endDate-"+endDate.toString()+"::dateMonthly-"+dateMonthly.toString());
            if(compDate <= compEndDate && compDate >= startDate){
                 dateMonthly = new Date(dateMonthly.getTime() - (machineOffset * 60 * 1000) + (clientOffset * 60 * 1000));
                 //End:QC3-5352 - Frequency and review schedule : for single and master app start date of monthly is not considered
              calendardetails(dateMonthly);
            }
        }

      }

      firstmonthWeekly = firstmonthWeekly + (skipWeeklyMonth + 1);

    }
  }

  function getMasterQcIds(masterAppId){
    var deffered = q.defer();
    db.masterqcsettings.findOne({_id: db.ObjectID(masterAppId)}, function(err, resultSchemas){
      if(err){
        deffered.reject(err);
        return;
      }
      deffered.resolve(resultSchemas.schema)
    });
    return deffered.promise;
  }

  function calendardetails(calendarDate) {
    var majorReleaseDate;
    var createdDate;

    var calendarobj ={
      schema: {
        _id: '',
        title: ''
      },
      master: {
        _id: '',
        title: ''
      },
      scheduleuniqueId : '',
      workflowreview : []
    }
    var newscheduleDate = '';
    var newscheduleDateForOverdue = '';
  clientOffset = parseInt(reqHeaders['client-offset']) || -330;
    var a = new Date(calendarDate);
    var time = new Date(schedule.time);

    if (schedule.frequency.type !== 'Hourly') {
      // 27/02/2018
      majorReleaseDate = new Date("2018-04-20");
      createdDate = new Date(schedule.createdDate);
      if(createdDate <  majorReleaseDate){
        a.setHours(a.getHours() +time.getHours());
        a.setMinutes(a.getMinutes() +time.getMinutes());
        a.setSeconds(a.getSeconds() +time.getSeconds());
        newscheduleDate=a;
      }
      else{
        // minus the client offset and attach to the time and add that time for overdue #overdueIssues
        var timeTemp = new Date(time.getTime()-(clientOffset * 60 * 1000));
        newscheduleDateForOverdue = new Date(a)
        newscheduleDateForOverdue.setHours(newscheduleDateForOverdue.getHours()+timeTemp.getHours())
        newscheduleDateForOverdue.setMinutes(newscheduleDateForOverdue.getMinutes()+timeTemp.getMinutes())
        newscheduleDateForOverdue.setSeconds(newscheduleDateForOverdue.getSeconds()+timeTemp.getSeconds())

        //QC3-9176 by miral - deduct the given offset #overdueIssues
        a.setHours(a.getHours() +timeTemp.getHours());
        a.setMinutes(a.getMinutes() +timeTemp.getMinutes());
        a.setSeconds(a.getSeconds() +timeTemp.getSeconds());
        newscheduleDate=a;
          // newscheduleDate = new Date(calendarDate);
      }
    }
    else {
        newscheduleDate = a;
        majorReleaseDate = new Date("2018-04-20");
        createdDate = new Date(schedule.createdDate);
        if(createdDate <  majorReleaseDate){
          //EMPTY IF TO PASS ELSE PART
        }
        else{
          //#overdueIssues
          newscheduleDateForOverdue = a;
        }
    }
    calendarobj.title = schedule.title;
    // Start: QC3-4195 Improvement - Create schedule for the whole master app instead of apps from master (1193)
    // Changed By: Kajal Patel
    // Description: code to create schedule for master.
    if(schedule.ismasterQcForm){
      calendarobj.master._id = db.ObjectID(schedule.master._id);
      calendarobj.master.title = schedule.master.title;
      calendarobj.calendarmasterId = db.ObjectID(schedule.master._id);
      delete calendarobj.schema;
    }else{
      calendarobj.schema._id = db.ObjectID(schedule.schema._id);
      calendarobj.schema.title = schedule.schema.title;
      calendarobj.calendarschemaId = db.ObjectID(schedule.schema._id);
      delete calendarobj.master;

    }

    calendarobj.scheduledBy = schedule.scheduledBy || schedule.createdBy;
    calendarobj.type = schedule.frequency.type;
    calendarobj.time = schedule.time;
    calendarobj.date = newscheduleDate;

    majorReleaseDate = new Date("2018-04-20");
    createdDate = new Date(schedule.createdDate);
    if(createdDate <  majorReleaseDate){
      calendarobj.overdueDate=new Date(getOverdueDate(schedule.requiredTimeFrequencytype,newscheduleDate,schedule.requiredTimeFrequency));
    }
    else{
      calendarobj.overdueDate=new Date(getOverdueDate(schedule.requiredTimeFrequencytype,newscheduleDateForOverdue,schedule.requiredTimeFrequency));
    }

    calendarobj.overdueTime=new Date(getOverdueDate(schedule.requiredTimeFrequencytype,schedule.time,schedule.requiredTimeFrequency)).toISOString();

    calendarobj.overdueCompareTime=new Date(calendarobj.overdueTime);
    //QC3-8804 - vaibhav
    if(calendarobj.overdueCompareTime.getDate() !=1){
      calendarobj.overdueCompareTime.setDate(1);
      calendarobj.overdueCompareTime.setMonth(0);
      calendarobj.overdueCompareTime.setYear(1970);
    }
    // log.log("calendarobj.overdueCompareTime"+calendarobj.overdueCompareTime);
    //   calendarobj.date = new Date(calendarobj.date.getTime() - (clientOffset * 60 * 1000));
    calendarobj.isActive = schedule.isActive;
    calendarobj.email = schedule.email;
    calendarobj.notified = false;
    calendarobj.keyvalues = schedule.attribute;
    calendarobj.scheduleId = schedule._id;
    calendarobj.scheduleuniqueId = uuid.v1();
    _.forEach(schedule.workflowreview.user , function(user){
      calendarobj.workflowreview.push(user.id);
    })
    calendarobj.workflowreviewroles = [];
    if (schedule.workflowreview && schedule.workflowreview.userSelectionType == '0') {
      calendarobj.workflowreviewroles = schedule.workflowreview.roles;
    }
    //QC3-8815-Backlog - Schedule - Remove the 'Review Schedule' tab and rename other tabs
    calendarobj.isSetReviewFrequency = false;
    calendarobj.ismasterQcForm = schedule.ismasterQcForm;
    if(schedule.masterQCSettingsId){
      calendarobj.masterQCSettingsId = schedule.masterQCSettingsId;
      getMasterQcIds(schedule.masterQCSettingsId).then(function (responseIds) {
        calendarobj.masterQCSchemaIds = responseIds;
      })
    }
    calendarobj.timeZone = reqHeaders['client-tz'] ? reqHeaders['client-tz'] : config.timeZonesP[config.currentTimeZoneP];
    // QC3-8863 - kajal
    calandarEntriesoccurence.push(calendarobj);
    if(schedule.occurence > 0){
      if(calandarEntriesoccurence.length <= schedule.occurence){
        calandarEntries.push(calendarobj);
      }
    }else{
      calandarEntries.push(calendarobj);
    }

  }
  return calandarEntries;
  }



  function getOverdueDate(type,dt,time){
    var calendarOverDueDate = new Date(dt);
    if(type==1){
      calendarOverDueDate = calendarOverDueDate.setHours(calendarOverDueDate.getHours() +parseInt(time));
    }else if(type==0){
      calendarOverDueDate = calendarOverDueDate.setMinutes(calendarOverDueDate.getMinutes() +parseInt(time));
    }else{
      calendarOverDueDate = calendarOverDueDate.setDate(calendarOverDueDate.getDate() + parseInt(time));
    }
    return calendarOverDueDate;
  }


  return {
    afterPost:afterPost,
    afterPatch:afterPatch
  }

})
