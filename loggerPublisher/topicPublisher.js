/*eslint consistent-this: [2, "that"]*/

//'use strict';

module.exports = (() => {

  var Amqp = require('amqplib/callback_api');
  var TopicPublisher = {

    Create: function () {

      //use that as this pattern
      var that = this;

      //connect
      that.connect = (host, exchange, callback) => {

        var connectionString = ''.concat('amqp://', host);

        Amqp.connect(connectionString, (err, con) => {
          if (err) {
          //  console.error("[AMQP]", err.message);
            return;
            // return setTimeout(start, 1000);
          }

          con.on("error", function (err) {
            if (err.message !== "Connection closing") {
            //  console.error("[AMQP] conn error", err.message);
            }
          });

          con.on("close", function () {
          //  console.error("[AMQP] Closed");
            return ;
          });

          //create channel
          con.createChannel((err, ch) => {

            if (err) {
              //console.log(JSON.stringify(err));
              return;
            }

            ch.assertExchange(exchange, 'topic', { durable: false });

            //store important variables
            that.channel = ch;
            that.exchange = exchange;
            that.connection = con;

            //callback
            if (callback) {
              callback(that);
            }

          });
        });
      };

      that.publish = (keys, data, callback) => {
        var ch = that.channel;
        var ex = that.exchange;
        var msg = JSON.stringify(data);

        //publish
        ch.assertExchange(ex, 'topic', { durable: false });
        try{
          ch.publish(ex, keys, new Buffer(msg));
        }
        catch(e){
          console.log("sdsd")
        }
        

        //callback
        if (callback) {
          callback(that);
        }
      };

      that.close = () => {
        var con = that.connection;

        setTimeout(() => {
          con.close();
          return;
        }, 500);
      };

      return that;
    }
  };

  return TopicPublisher;
})();
