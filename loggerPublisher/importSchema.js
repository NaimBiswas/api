'use strict';

// var db = require('../lib/db');
var _ = require('lodash');
var restler = require('restler');
var config = require('../config/config.js');
var session_logger = require('../lib/logger').configure(__dirname, false);
var db = require('../lib/resources').db;

module.exports = (function () {

    function importSchema(req, res, next) {

        function checkExistingSet(attribute, next) {
            if (attribute.validation && attribute.validation.validationSet && attribute.validation.validationSet.existingSet && attribute.validation.validationSet.existingSet.title) {
                checkAlreadyExists('sets', attribute.validation.validationSet.existingSet.title, function (exists) {
                    if (exists) {
                        _.assign(attribute.validation.validationSet.existingSet, exists);
                        next();
                    } else {
                        var set = _.omit(attribute.validation.validationSet.existingSet, ['_id', 'route', 'reqParams', 'restangularized', 'fromServer', 'parentResource', 'restangularCollection', 'version']);
                        insertData('sets', 'POST', omitDefaultProps(set), req, res, function (newAttribute) {
                            console.log(newAttribute);
                            _.assign(attribute.validation.validationSet.existingSet, omitDefaultProps(newAttribute));
                            next();
                        });
                    }
                });
            } else {
                next();
            }
        }

        function checkAttributes(attribute, counter, next) {
            console.log('req.body.attributes.length', req.body.attributes.length, counter);
            if (req.body.attributes.length == counter) {
                next();
            } else {
                checkAlreadyExists('attributes', attribute.title, function (exists) {
                    if (exists) {
                        _.assign(attribute, omitDefaultProps(exists));
                        attribute.module = {
                            _id: req.body.module._id,
                            title: req.body.module.title
                        };
                        attribute['s#'] = ((new Date()).getTime()).toString(); //?
                        counter++;
                        checkExistingSet(attribute, function () {
                            checkAttributes(req.body.attributes[counter], counter, next);
                        });
                    } else {
                        attribute._id = new db.ObjectID();
                        attribute.module._id = req.body.module._id;
                        attribute = _.omit(attribute, ['s#', 'version', 'module.title']);
                        checkExistingSet(attribute, function () {
                            insertData('attributes', 'POST', attribute, req, res, function (newAttribute) {
                                console.log(newAttribute);
                                newAttribute['s#'] = ((new Date()).getTime()).toString(); //?
                                newAttribute.version = 1;
                                newAttribute.module.title = req.body.module.title;
                                _.assign(attribute, omitDefaultProps(newAttribute));
                                counter++;
                                checkAttributes(req.body.attributes[counter], counter, next);
                            });
                        });
                    }
                });
            }
        }

        function checkProcedures(procedure, counter, next) {
            console.log('req.body.procedures.length', req.body.procedures.length, counter);
            if (req.body.procedures.length == counter) {
                next();
            } else {
                procedure.title = procedure.title.substring(0, procedure.title.lastIndexOf('P') - 1) || procedure.title;
                checkAlreadyExists('procedures', procedure.title, function (exists) {
                    if (exists) {
                        _.assign(procedure, _.omit(omitDefaultProps(exists), ['isActive', 'isAuditing']));

                        procedure['s#'] = ((new Date()).getTime()).toString(); //?
                        counter++;
                        checkQuestions(procedure, 0, function () {
                            _.forEach(procedure.questions, function (element) {
                                delete element.isActive;
                                delete element.parentId;
                                delete element.isoptionalquestion;
                            });
                            checkProcedures(req.body.procedures[counter], counter, next);
                        });
                    } else {
                        procedure['s#'] = ((new Date()).getTime()).toString(); //?
                        checkQuestions(procedure, 0, function () {
                            _.forEach(procedure.questions, function (element) {
                                delete element.isActive;
                                delete element.parentId;
                                delete element.isoptionalquestion;
                            });
                            var keepQuestions = _.cloneDeep(procedure.questions);
                            procedure = _.omit(procedure, ['_id', 's#', 'version', 'conditional', 'isOptionalProcedure', 'optionalProcedureTitle', 'conditionalWorkflows', 'questionUsedInProcedureCondition']);
                            procedure.isActive = true;
                            insertData('procedures', 'POST', procedure, req, res, function (newAttribute) {
                                console.log(newAttribute);
                                newAttribute.version = 1;
                                _.assign(keepQuestions, omitDefaultProps(newAttribute));
                                procedure = newAttribute;
                                counter++;
                                checkProcedures(req.body.procedures[counter], counter, next);
                            });
                        });
                    }
                });
            }
        }

        function checkQuestions(procedure, counter, next) {
            console.log('procedure.questions.length', procedure.questions.length, counter);
            if (procedure.questions.length == counter) {
                next();
            } else {
                var question = procedure.questions[counter];
                question.title = question.title.substring(0, question.title.lastIndexOf('Q') - 1) || question.title;
                checkAlreadyExists('questions', question.title, function (exists) {
                    if (exists) {
                        _.assign(question, omitDefaultProps(exists));
                        question['s#'] = ((new Date()).getTime()).toString(); //?
                        counter++;
                        checkExistingSet(question, function () {
                            checkQuestions(procedure, counter, next);
                        });
                    } else {
                        var keepQuestion = _.cloneDeep(question);
                        question.isActive = true;
                        question = _.omit(question, ['_id', 's#', 'version', 'comboId', 'parentId', 'isoptionalquestion', 'optionalProcedureTitle',
                            'conditionalWorkflows', 'questionUsedInProcedureCondition', 'criteria', 'checkValidCriteria']);
                        checkExistingSet(question, function () {
                            insertData('questions', 'POST', question, req, res, function (newAttribute) {
                                console.log('post questions', newAttribute);
                                newAttribute['s#'] = ((new Date()).getTime()).toString(); //?
                                newAttribute.version = 1;
                                newAttribute.comboId = newAttribute['s#'] + procedure['s#'];
                                newAttribute.parentId = procedure['s#'];
                                _.assign(keepQuestion, omitDefaultProps(newAttribute));
                                question = keepQuestion;
                                counter++;
                                checkQuestions(procedure, counter, next);
                            });
                        });
                    }
                });
            }
        }

        function checkEntities(entity, counter, next) {
            console.log('req.body.entities.length', req.body.entities.length, counter);
            if (req.body.entities.length == counter) {
                next();
            } else {
                entity.title = entity.title.substring(0, entity.title.lastIndexOf('E') - 1) || entity.title;
                checkAlreadyExists('entities', entity.title, function (exists) {
                    if (exists) {
                        _.assign(entity, omitDefaultProps(exists));
                        entity['s#'] = ((new Date()).getTime()).toString(); //?
                        delete entity.tags;
                        delete entity.module;
                        delete entity.modulesTitle;
                        delete entity.isActive;
                        delete entity.isAuditing;
                        console.log(entity);
                        counter++;
                        checkEntities(req.body.entities[counter], counter, next);
                    } else {
                        var keepEntity = _.cloneDeep(entity);
                        entity.isActive = true;
                        entity.tags = ['IMPORTED'];
                        // entity.tags = [entity.title.split(' ').join('').toLowerCase()];
                        entity.questions.forEach(function (element) {
                            delete element.parentId;
                            delete element.isAppearance;
                            delete element.isDisplayQuestion;
                        });
                        entity.module = [{
                            _id: req.body.module._id,
                            title: req.body.module.title
                        }];
                        entity.modulesTitle = req.body.module.title;
                        // entity.description = entity.title;
                        entity = _.omit(entity, ['_id', 's#', 'isShowQuestionAdded', 'isDisplayQuestion', 'filteredEntQuesSelected', 'filteredEntQues', 'isExpBuild', 'entityFilters',
                            'conditionalWorkflows', 'isSetLabel', 'entityLabelTitle', 'getInactiveRecord', 'markEntityValueInActive', 'enableEntityRecord']);
                        insertData('entities', 'POST', entity, req, res, function (newAttribute) {
                            console.log(newAttribute);
                            newAttribute['s#'] = ((new Date()).getTime()).toString(); //?
                            _.assign(entity, keepEntity, omitDefaultProps(newAttribute));
                            // entity = keepEntity;
                            delete entity.tags;
                            delete entity.module;
                            delete entity.modulesTitle;
                            delete entity.isActive;
                            delete entity.isAuditing;

                            console.log(entity);
                            counter++;
                            checkEntities(req.body.entities[counter], counter, next);
                        });
                    }
                });
            }
        }

        function initializePostObj() {
            req.body = _.omit(req.body, ['_id', 'version']);
            req.body.module = {
                "_id": "5729a719e22e3adc3ad2ebf6",
                "title": "Module 1"
            };
            req.body.siteYesNo = 'no';
            req.body.allicableSites = [
                [
                    {
                        "_id": "5729a7a82d0800cb3b459ebc",
                        "title": "India"
                    }
                ],
                [
                    {
                        "_id": "5729a7b42d0800cb3b459ec5",
                        "title": "Gujarat"
                    }
                ],
                [
                    {
                        "_id": "5729a7df2d0800cb3b459eef",
                        "title": "Rajkot"
                    }
                ],
                [
                    {
                        "_id": "58480548e38425036bf0fc32",
                        "title": "Ring Road"
                    }
                ]
            ];
        }

        function activate() {
            initializePostObj();

            checkAlreadyExists('schema', req.body.title, function (exists) {
                checkAttributes(req.body.attributes[0], 0, function () {
                    checkProcedures(req.body.procedures[0], 0, function () {
                        checkEntities(req.body.entities[0], 0, function () {
                            insertData('schema', 'POST', omitDefaultProps(req.body), req, res);
                        });
                    });
                });
            });
        }

        activate();
    }

    function omitDefaultProps(obj) {
        return _.omit(obj, ['createdBy', 'modifiedBy', 'createdByName', 'modifiedByName', 'createdDate', 'modifiedDate', 'timeZone', 'versions', 'isMajorVersion']);
    }

    function checkAlreadyExists(collection, title, next) {
        db[collection].findOne({ 'title': title }, function (err, schema) {
            if (!schema) {
                next(false);
            } else {
                // title = title + '-' + (new Date).getTime();
                next(schema);
            }
        });
    }

    function insertData(collection, method, data, req, res, next) {
        if (method == 'POST') {
            // rest = restler.postJson;

            db[collection].insertOne(data, function (error, response) {

                if (collection == 'schema') {
                    console.log('<----- ' + req.body.title + ' INSERTED. ----->', result);
                    db[collection].auditThis1(response.ops[0], requestFrom);
                    return;
                } else {
                    next(result);
                }
                console.log();
                db[collection].auditThis1(response.ops[0], requestFrom);
            });

        } else if (method == 'PATCH') {
            // rest = restler.patchJson;
        }
    }

    return {
        importSchema: importSchema,
        checkAlreadyExists: checkAlreadyExists
    };
})();