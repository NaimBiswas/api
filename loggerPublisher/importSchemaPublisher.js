'use strict';

module.exports = (function () {

    var QuePublisher = require('./quePublisher');
    var quePublisher = QuePublisher.Create();
    var config = require('../config/config.js');

    return {
        connect: function () {
            // console.log("CONNECTING TO IMPORT SCHEMA...");
            quePublisher.connect(config.mq.connection, config.mq.importSchema.queue, (pub) => {
                // console.log("CONNECTED TO IMPORT SCHEMA...");
            });
        },
        publish: (key, data) => {
            if (data && quePublisher.connection) {
                quePublisher.publish(key, data, config.mq.importSchema.queue, (pubInner) => {
                    console.log("IMPORT SCHEMA PUBLISHED.")
                });
            }
        }
    };

})();
