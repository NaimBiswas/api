var amqp = require('amqplib/callback_api');
var config = require('../config/config.js');
var importSchemaInstance = require('./importSchema');

amqp.connect('amqp://' + config.mq.connection, function (err, conn) {
  conn.createChannel(function (err, ch) {
    var q = config.mq.importSchema.queue;

    ch.assertQueue(q, { durable: false });

    ch.consume(q, function (msg) {
      var req = JSON.parse(msg.content.toString());
      importSchemaInstance.importSchema(req);
    }, { noAck: true });

  });
});
