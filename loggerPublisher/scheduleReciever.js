/*eslint consistent-this: [2, "that"]*/

'use strict';
var amqp = require('amqplib/callback_api');
var config = require('../config/config.js');

var scheduleExecutor = require('./scheduleExecutor');


function startConnection(){

  amqp.connect('amqp://'+config.mq.connection, function(err, conn) {
  
      if (err) {
        console.error("[AMQP]", err.message);
        return setTimeout(startConnection, 5000);
      }
      conn.on("error", function(err) {
        if (err.message !== "Connection closing") {
          console.error("[AMQP] conn error", err.message);
        }
      });
      conn.on("close", function() {
        console.error("[AMQP] reconnecting");
        return setTimeout(startConnection, 5000);
      });
  
  
      conn.createChannel(function(err, ch) {
      var q = config.mq.scheduleMq.queue;
  
      ch.assertQueue(q, {durable: false});
  
      ch.consume(q, function(msg) {
        var req =  JSON.parse(msg.content.toString());
  
  
        var scheduleExecutorInstance = scheduleExecutor();
  
        if(req.method=="post"){
          scheduleExecutorInstance.afterPost(req);
        }
        else{
          scheduleExecutorInstance.afterPatch(req);
        }
  
  
      }, {noAck: true});
  
    });
  });
}

startConnection();

