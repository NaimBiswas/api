'use strict';

module.exports = (() => {

  var TopicPublisher = require('./topicPublisher');
  var accountPub = TopicPublisher.Create();
  var config = require('../config/config.js');
  var _ = require('lodash');

  var pendingLogs = [];
  var connectionRequestFullfilled = false;

function makeConnection(){
  connectionRequestFullfilled = false;
  accountPub.connect(config.mq.connection, config.mq.accountMq.queue, (pub) => {
    connectionRequestFullfilled = true;
  });
}
makeConnection();
 
  var PublishHelper = {
    publish: (key, data) => {
      if (data) {
        if(accountPub.connection){
          _.forEach(pendingLogs,function(pLog){
            accountPub.publish(pLog.key, pLog.data, (pubInner) => {
            });
          });
          pendingLogs = [];

          accountPub.publish(key, data, (pubInner) => {
          });
        } else{

          if(connectionRequestFullfilled){
            makeConnection();
          }

          pendingLogs.push({
            key:key,
            data:data
          });
        }
      }

    }
  };
  return PublishHelper;
})();

/* Sample code for calling the publisher */
// var PublishHelper = require('../publishHelper');
// PublishHelper.publish('applicant.created', applicant);
