'use strict'
/**
* @name db
* @author Jaydipsinh Vaghela <jaydeep.v@productivet.com>
*
* @version 0.0.0
*/
var log4js = require('log4js');
var fs = require('fs');
var config = require('../config/config.js');
var PublishHelper = require('../loggerPublisher/publishHelper.js');
var logMode = config.log.mode;
var logLevel = config.log.level;

function canWrite(path, callback) {
  fs.access(path, fs.W_OK, function(err) {
    callback(null, !err);
  });
}



//by surjeet.b...
var logger4js;
canWrite('./logger/capturedLogs.log', function(err, isWritable) {
  if(isWritable){
    log4js.configure({
      appenders: [
          { type: 'file', filename: './logger/capturedLogs.log', category: 'captureLog', backups: 25, maxLogSize: 81920 }
      ]
    });
    logger4js = log4js.getLogger('captureLog');
  }
});


var logLevels = {
  debug: 4,
  info: 3,
  warning: 2,
  error: 1,
  none: 0
}

var checkLogEligibility = function(level){
  var suppliedLevel = logLevels[level];
  var allowedLevel = logLevels[logLevel];
  // console.log('supplied : ' + suppliedLevel);
  // console.log('actual : ' + allowedLevel);
  if(suppliedLevel <= allowedLevel){
    return true;
  }
  return false;
}


var _logWithLevel = function(message, level, filename) {
  var file = '';
  if (filename) {
    file = filename;
    file= '('.concat(file).concat(') : ');
  }
  if (file) {
    message = file.concat(message||'');
  }

  if(logMode.console && level != 'info'){
    //will log on terminal
    console.log(message);
  }

  if(logMode.disk){
    //will log to file.
    
    canWrite('./logger/capturedLogs.log', function(err, isWritable) {
      if(logger4js && isWritable){
        logger4js.debug({message: message, level: level});
      }
    })
    
  }

  if(logMode.mq){
    //will log on rabbitmq server..
    var obj = {
        "status": "none",
        "source": 'none',
        "statusCode": 'none',
        "statusMessage": message,
        "timestamp": new Date(),
        "env": config.envurl,
        "level": level
    };
    var key = config.mq ? config.mq.keys[0] : 'perfeqta.logging';
    PublishHelper.publish(key, { content: obj });
  }
};

/**
* For saving
*
* @param  {String} - Message for saving to logger file,
* @return null - Print in logger file.
*
* @public
*/
var _log = function(message, level, filename) {
  if(!level){level = "debug";}

  if(checkLogEligibility(level)){
      _logWithLevel(message, level, filename);
  }
};

module.exports = {
  log: _log
};
