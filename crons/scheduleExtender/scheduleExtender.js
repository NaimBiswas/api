var config = require('../../config/config.js');
var Promise = require('bluebird');
var _ = require('lodash');
var db = Promise.promisifyAll(require('../../lib/db'));
var MICRO = require("../../microservices/MICRO");
var schedulePublisherInstance = MICRO.getPublisherInstance("schedule");
var log = require('../../logger');
var _extendedSchedules = [];

async function activate(_type) {
    try {
        _extendedSchedules = await getSchedules(_type);
        var _extendedSchedulesClone = _.cloneDeep(_extendedSchedules);
        for (let idx=0;idx<_extendedSchedules.length;idx++) {
            _extendedSchedulesClone[idx] = _.omit(_extendedSchedulesClone[idx], ["version", "versions", "isMajorVersion", "modifiedDate", "createdDate", "modifiedByName", "createdByName", "createdBy", "modifiedBy"]);
            if(_.get(_extendedSchedulesClone[idx], 'endDate')){
                _extendedSchedulesClone[idx]['endDate'] = updateDate(new Date(_extendedSchedulesClone[idx].endDate));
                updateSchedule(_extendedSchedules[idx]);
            }
        }
        createScheduleTasks(_extendedSchedulesClone);
    } catch (error) {
        log.err('inside file: scheduleExtender.js - function: activate', error);
    }
}

function createScheduleTasks(_schedules){
    var _schedule = _schedules.shift();
    schedulePublisherInstance.call("patch",{data:_schedule, headers: { 'client-offset': config.timeZones[config.currentTimeZone] || -330 , 'client-tz': config.timeZonesP[config.currentTimeZoneP] || 'America/New_York' }, method:"patch"}, function patch(err, data){
        if(err){
            log.err('inside file: scheduleExtender.js - function: createScheduleTasks', err);
        }
        else if(_schedules.length){
            createScheduleTasks(_schedules)
        }
    });
}

function query(_scheduleType){
    var _query = {
        isActive: true,
        'frequency.type': _scheduleType,
        // 'startDate': { TBD
        //     $lte: moment().endOf('day').toDate().toISOString()
        // },
        // 'endDate': {
        //     $gte: moment().startOf('day').toDate().toISOString()
        // }
    }
    return _query;
}

async function getSchedules(_type){
    return await db.collection('schedules').find(query()).toArrayAsync();
}

async function updateSchedule(_schedule){
    return await db.collection('schedules').updateAsync({ _id: _schedule._id }, { $set: { 'endDate':  updateDate(new Date(_schedule.endDate)) } })
}

function updateDate(_date){
    return new Date(_date.setDate(_date.getDate() + 1)).toISOString();
}

module.exports = {
	activate: activate
}