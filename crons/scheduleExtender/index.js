const configFetch = require('../../configFetch');

startApp()
function startApp() {
    var CronJob = require('cron').CronJob;
    var scheduleExtender = require('./scheduleExtender');
    var _ = require('lodash');
    var moment = require('moment');
    var job = new CronJob('0 */1 * * *', function () {
            scheduleExtender.activate('Weekly')
                .then(function (data) {
                    console.log('Schedules end date extended and new tasks created' + getCurrentDate().time);
                })
                .catch(function (err) {
                    console.log(err + getCurrentDate().time);
                    return err
                });
        }, function () { },
        true
    );

    function getCurrentDate() {
        return {
            time: moment().format('MMMM Do YYYY, h:mm:ss a')
        }
    }
}