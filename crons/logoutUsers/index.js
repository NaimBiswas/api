

var CronJob = require('cron').CronJob;
var logoutUsers = require('./logoutUsers');
var config = require('../../config/config.js');
var nodemailer = require('nodemailer');
var _ = require('lodash');
var smtpTransport = require('nodemailer-smtp-transport');
var moment = require('moment');

var job = new CronJob('*/1 * * * *', function () {
    logoutUsers.activate()
        .then(function(data){ 
                if( data.length){ 
                    console.log('Logout done '+ JSON.stringify(data), 'Logout done '+ getCurrentDate().time)
                } else{
                    console.log( ' No loggedin users found for before ' + getCurrentDate().time)
                }
            return data
        })
        .catch(function(err){
            console.log('Logout user','Has error',err); 
            return err
        });

  }, function () {},
  true
);

 
function getCurrentDate() {
	return {
		time: moment().format('MMMM Do YYYY, h:mm:ss a')
	}
} 