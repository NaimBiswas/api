var Promise = require('bluebird');

var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('../../lib/logger').configure(__dirname, enableBunyan);
require('../../lib/promise-me');
var _ = require('lodash');
var db = Promise.promisifyAll(require('../../lib/db'));
var moment = require('moment');

function activate() {
	return db.collection('allAuthToken')
		.find({
			globleExpirationTime: {
				$lt: moment.utc().toDate()
			}
		})
		.toArrayAsync()
		.then(function (wasteLogedinToken) {
			return Promise.reduce((wasteLogedinToken || []), function (logoutResult, _user) {
				return db.collection('users').findOneAndUpdateAsync({
						_id: _user.userId,
						isConCurrentUserActive: true
					}, {
						$set: {
							isConCurrentUserActive: false,
							forceRemovedUser: 'system'
						}
					}, {
						upsert: false
					})
					.then(function (existUser) {
						existUser = existUser.value || null;
						if (existUser) {
							db.collection('loginattempts').insertAsync({
								username: getUsername(existUser),
								name: existUser.firstname + ' ' + existUser.lastname,
								date: moment.utc().toDate(),
								access: 'granted',
								ip: '0.0.0.0',
								hostName: 'system',
								isAuthenticatedUser: true,
								roles: rolesArray(existUser),
								navigatorInfoInString: 'system',
								logoutTime:moment.utc().toDate()
							})
							logoutResult.push({
								user: _user._id,
								username: getUsername(existUser)
							});
						}
						return logoutResult || [];
					})
			}, [])
		})
}

function rolesArray(existUser) {
	var rolesArray = _.map(existUser.roles, function (role) {
		return _.result(role, ['title']);
	});
	return rolesArray.join(", ")
}

function getUsername(existUser) {
	return existUser.firstname + ' ' + existUser.lastname + ' (' + existUser.username.toLowerCase() + ')';
}

module.exports = {
	activate: activate
}