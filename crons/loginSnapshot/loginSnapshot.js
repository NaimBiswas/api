var Promise = require('bluebird');

var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('../../lib/logger').configure(__dirname, enableBunyan);
require('../../lib/promise-me');
var config = require('../../config/config.js');
var CronJob = require('cron').CronJob;
var mailer = require('../../lib/mailer');
var log = require('../../logger');
var _ = require('lodash');
var db = Promise.promisifyAll(require('../../lib/db')); //require('./db');
// var debug = require('./logger').debug('lib:scheduler');
var handlebars = require('handlebars');
var moment = require('moment');
var mysqlDb = require('./mysqlDb');

function activate(){

    var currentDate = new Date();
    return db.collection('users')
        .find({isConCurrentUserActive: true})
        .toArrayAsync()
        .then(function(currentlyLoggedIn){

            var snapShotArray = _.map(currentlyLoggedIn,function(_user){
                return {
                    userInfo: JSON.stringify(_user),
                    userName: _user.username,
                    firstName: _user.firstname,
                    lastName: _user.lastname,
                    email: _user.email,
                    ip: _user.ip,
                    loggedInDateTime: _user.loggedInDateTime,
                    env: config.envurl,
                    date: currentDate
                }
            })

            console.log('found',snapShotArray)
            return mysqlDb.getSnapshotTable()
                            .bulkCreate(snapShotArray);
        })
        .then(function(snapshotResult) {
            console.log('snapshot',snapShotArray)
            return snapshotResult;
        })
        .catch(function(err){
            console.log('error',err)
        })
}


module.exports = {
    activate: activate
}