const Sequelize = require('sequelize');
const config = require('../../config/config');

var sequelizeInstance = new Sequelize(config.loginSnapshot.database,
  config.loginSnapshot.userName,
  config.loginSnapshot.password, {
    host: config.loginSnapshot.host,
    dialect: 'mysql',
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
  });

  var snapshotTable = sequelizeInstance.define(config.loginSnapshot.tableName, {
    userInfo: {
      type: Sequelize.TEXT
    },
    userName: {
      type: Sequelize.STRING
    },
    firstName: {
      type: Sequelize.STRING
    },
    lastName: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
    },
    ip: {
      type: Sequelize.STRING
    },
    loggedInDateTime: {
      type: Sequelize.STRING
    },
    env: {
      type: Sequelize.STRING
    },
    date: {
      type: Sequelize.DATE
    },
    
  });

snapshotTable.sync({ force: false }).then(() => {
  
});

function getSnapshotTable() {
  return snapshotTable;
}

module.exports = {
  getSnapshotTable: getSnapshotTable
}