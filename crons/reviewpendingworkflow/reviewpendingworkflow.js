var Promise = require('bluebird');

var enableBunyan = process.env.enableBunyan === 'true';
require('../../lib/logger').configure(__dirname, enableBunyan);
require('../../lib/promise-me');
var db = Promise.promisifyAll(require('../../lib/db'));

function activate() {
    return db.collection('reviewpendingworkflow')
        .find({})
        .toArrayAsync()
        .then(function (entries) {

            return Promise.reduce(entries, function (result, item) {

                return db.collection(item.schemaId).findOneAsync(item.entryId, {
                    'workflowreview.reviewstatus': 1
                }).then(function (entry) {

                    if (entry && entry.workflowreview) {
                        var signed = entry.workflowreview.every(function (review) {
                            return review.reviewstatus === 0;
                        });

                        if (signed) {
                            console.log('Removed: ' + item.schemaId + ' --> ' + item.entryId.valueOf());

                            result.push(item);

                            db.collection('reviewpendingworkflow').remove(item._id);
                        }
                    }

                    return result;
                });

            }, []);

        });
}

module.exports = {
    activate: activate
}