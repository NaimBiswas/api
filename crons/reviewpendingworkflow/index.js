var CronJob = require('cron').CronJob;
var reviewpendingworkflow = require('./reviewpendingworkflow');
var config = require('../../config/config.js');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

// 0 */1 * * * - runs every hour
var job = new CronJob('0 */1 * * *', function () {
    console.log('Review Pending Workflow', 'cron has started');

    reviewpendingworkflow.activate()
        .then(function (data) {
            console.log(data);

            if (data && data.length) {
                sendMail('Removed Following items from reviewpendingworkflow:\n' + JSON.stringify(data), 'Done');
            }
        });
});

var transporter = nodemailer.createTransport(smtpTransport({
    host: config.mail.host,
    port: config.mail.port,
    auth: {
        user: config.mail.user,
        pass: config.mail.pass
    }
}));

function sendMail(message, subJectAttacher) {
    var date = new Date();

    message = " " + config.envurl + "  Date : " + date.toString() + " - " + message
    transporter.sendMail({
        from: config.mail.from, //do-not-reply@e.productivet.com
        to: config.logoutUserMail,
        subject: 'Perfeqta system mail - ' + config.envurl + ' - Removal of Review Completed Apps mail + (' + subJectAttacher + ')',
        html: message,
        priority: 'high'
    }, function sendMailCallback(err, d) {
        console.log(err, d)
    })
}