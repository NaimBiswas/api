var should = require('should');
var request = require('supertest');
var host = (process.env.host || "localhost:3000");
var client = (process.env.client ?  process.env.client + '.' : "");
var colors = require('colors');
var log = require('../logger');


var api = 'http://api.' +  client + host;
var web = 'http://web.' + client + host;

log.log('Running against ' + api.green.underline, 'info');

var token = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJfaWQiOiI1NjdmMjM0MDM1NDE4YTdiZWUzNTEwMjEiLCJlbWFpbCI6ImRhdmVhbWl0QGxpdmUuY29tIiwiZmlyc3RuYW1lIjoiQW1pdCIsImxhc3RuYW1lIjoiRGF2ZSIsInN0YXR1cyI6ImFjdGl2ZSIsInNlY3VyaXR5UXVlc3Rpb24iOiJwZXQgbmFtZT8iLCJzZWN1cml0eUFuc3dlciI6IjxSRVRSQUNURUQ-IiwicGFzc3dvcmQiOiI8UkVUUkFDVEVEPiIsImlhdCI6MTQ1MzM3NTYxNn0.lIpCf0VrWm3aYbCK8D8Uv1TVl_6CnM1O41o6L0TYa8FVUeM35kuclizwbiuQgPCdWtwMnSZ4yMzV49Z9bhbidpDWeSpAbtySOagqU9oSKvae8GB082OFnRLinoocmJ2h78R_gl9N9ikeSfJBExrhgwJhcY3Nsjyp8uJ7QbUDN2I';
describe("Sanity Checks", function () {

  describe("Only authenticated users are able to GET", function () {
    it('settings', function (done) {
      request(api)
      .get('/settings/default')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(401, done);
    });
  });


  describe("Default values are set for for", function () {

    it('settings', function (done) {
      request(api)
      .get('/settings/default')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        res.body._id.should.equal('default');
        res.body.web.should.equal(web);
        done();
      });
    });

    it('users - (Admin users must be set)', function (done) {
      request(api)
      .get('/users?pagesize=100')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        res.body.length.should.not.equal(0);

        if(res.body.length){
          res.body.filter(function (u) {
            return u.username === 'admin';
          }).length.should.not.equal(0);
        }

        done();
      });
    });

    it('types - (Atleast one value must be set)', function (done) {
      request(api)
      .get('/types')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        res.body.length.should.not.equal(0);
        done();
      });
    });



  });



});
