'use strict';

var express = require('express');
var resources = require('../resources');
var query = require('../query');
var libLogger = require('../logger');
var _debug = libLogger.debug('routes:builder');
var logger = require('../../logger');
var currentFileName = __filename;
var validate = require('../validator');
//var authorize = require('../oauth/authorize');
var _ = require('lodash');
var db = require('../db');
var mapUserIdWithUser = require('../registration').mapUserIdWithUser;
var config = require('../../config/config.js');

function build(entity){
  logger.log('Function: build - Success', 'info', currentFileName);
  var debug = libLogger.debug('middleware:' + entity.name);

  function getAllRouteHandler(req, res, next) {
    logger.log('Function: getAllRouteHandler - Success', 'info', currentFileName);
    entity.getAllAsync(req.paginateOptions)
    .then(function getThenCallback(data) {
      logger.log('Function: getThenCallback - Success', 'info', currentFileName);
      mapUserIdWithUser(data,function (err,data) {
        logger.log('Function: mapUserIdWithUser - Success', 'info', currentFileName);
        req.data = data;
        next();
        return;
      });
    })
    .catch(function(ex){
      logger.log('Function: getAllAsync - Catch : ' + ex, 'error', currentFileName);
      debug(ex);
      next(ex);
    });
  }


  function getOneRouteHandler(req, res, next) {
    logger.log('Function: getOneRouteHandler - Success', 'info', currentFileName);
    entity.getOneAsync(req.params._id)
    .then(function getModulesThenCallback(data) {
      if(data){
        logger.log('Function: getModulesThenCallback: if(data) - Success', 'info', currentFileName);
        mapUserIdWithUser(data,function (err,data) {
          logger.log('Function: mapUserIdWithUser - Success', 'info', currentFileName);
          req.data = data;
          next();
          return;
        });
      }else {
        logger.log('Function: getModulesThenCallback: else - Success', 'info', currentFileName);
        res.status(404).json({message: 'Resource you are looking for does not exsists'});
      }
    })
    .catch(function(ex){
      logger.log('Function: getOneAsync - Catch : ' + ex, 'error', currentFileName);
      debug(ex);
      next(ex);
    });
  }


  function getOneMajorRouteHandler(req, res, next) {
    logger.log('Function: getOneMajorRouteHandler - Success', 'info', currentFileName);
    entity.getMajorVersionAsync(req.params._id)
    .then(function getModulesThenCallback(data) {
      if(data){
        mapUserIdWithUser(data,function (err,data) {
          req.data = data;
          next();
          return;
        });
      }else {
        res.status(404).json({message: 'Resource you are looking for does not exsists'});
      }
    })
    .catch(function(ex){
      logger.log('Function: getOneMajorRouteHandler - Catch : ' + ex, 'error', currentFileName);
      debug(ex);
      next(ex);
    });
  }


  function getOnePublishedRouteHandler(req, res, next) {
    logger.log('Function: getOnePublishedRouteHandler - Success', 'info', currentFileName);
    entity.getMajorVersionAsync(req.params._id)
    .then(function getModulesThenCallback(data) {
      if(data){
        mapUserIdWithUser(data,function (err,data) {
          req.data = data;
          next();
          return;
        });
      }else {
        res.status(404).json({message: 'Resource you are looking for does not exsists'});
      }
    })
    .catch(function(ex){
      logger.log('Function: getOnePublishedRouteHandler - Catch : '+ ex, 'error', currentFileName);
      debug(ex);
      next(ex);
    });
  }



  function getAllVersionsRouteHandler(req, res, next) {
    logger.log('Function: getAllVersionsRouteHandler - Success', 'info', currentFileName);
    entity.getAllVersionsAsync(req.params._id, req.paginateOptions)
    .then(function getThenCallback(data) {
      if(data.length){
        mapUserIdWithUser(data,function (err,data) {
          req.data = data;
          next();
          return;
        });
      }else {
        res.status(404).json({message: 'Resource you are looking for does not exsists'});
      }
    })
    .catch(function(ex){
      logger.log('Function: getAllVersionsRouteHandler - Catch : ' + ex, 'error', currentFileName);
      debug(ex);
      next(ex);
    });
  }

  function getAllLogsHandler(req, res, next) {
    logger.log('Function: getAllLogsHandler - Success', 'info', currentFileName);
    entity.getChangeLogsAsync(req.params._id, req.paginateOptions)
    .then(function getThenCallback(data) {
      if(data.length){
        mapUserIdWithUser(data,function (err,data) {
          req.data = data;
          next();
          return;
        });
      }else {
        res.status(404).json([{message: 'Resource you are looking for does not exsists'}]); //QC3-10158
      }
    })
    .catch(function(ex){
      logger.log('Function: getAllLogsHandler - Catch : ' + ex, 'error', currentFileName);
      debug(ex);
      next(ex);
    });
  }



  function getOneVersionRouteHandler(req, res, next) {
    logger.log('Function: getOneVersionRouteHandler - Success', 'info', currentFileName);
    entity.getVersionAsync(req.params._id,req.params.version)
    .then(function getModulesThenCallback(data) {
      if(data){
        mapUserIdWithUser(data,function (err,data) {
          req.data = data;
          next();
          return;
        });
      }else {
        res.status(404).json({message: 'Resource you are looking for does not exsists'});
      }
    })
    .catch(function(ex){
      logger.log('Function: getOneVersionRouteHandler - Catch : ' + ex, 'error', currentFileName);
      debug(ex);
      if(ex.name === 'MongoError'){
        next(ex);
      }else {
        res.status(422).json({
          'message': ex.message
        });
      }
    });
  }

  function postRouteHandler(req, res, next) {
    logger.log('Function: postRouteHandler - Success', 'info', currentFileName);
    if(req.items._id){
      logger.log('if(req.items._id) - Success', 'info', currentFileName);
      req.params._id = req.items._id.length === 24 ? resources.db.ObjectID(req.items._id) : req.items._id;
    }
    if (req.items.createById && req.method === 'POST') {
      logger.log('if (req.items.createById && req.method === POST) - Success', 'info', currentFileName);
            req.user._id = req.items.createById;
            delete req.items.createById;
    }
    req.items.createdBy = req.user._id;
    req.items.modifiedBy = req.user._id;
    req.items.timeZone = getCurrentTimeZone(req);

    db['users'].findOne({_id: db.ObjectID(req.user._id)}, function(e,u) {
      if(e){
        logger.log('Collection: users - Error : ' +e, 'error', currentFileName);
        return;
      }
      logger.log('Collection: users - Success', 'info', currentFileName);
      // if(u){
      //   req.items.createdByName = u.firstname + " " + u.lastname + " ("+req.user.username+")";
      //   req.items.modifiedByName = u.firstname + " " + u.lastname + " ("+req.user.username+")";

      // }
      // req.items.__IP__ = req.ip;

    //Start : Lokesh Boran : QC3-4396
      if(u){
        logger.log('Collection: users: if(u) - Success', 'info', currentFileName);
        req.items.createdByName = u.firstname + " " + u.lastname + " ("+u.username+")";
        req.items.modifiedByName = u.firstname + " " + u.lastname + " ("+u.username+")";
      }
      //End : Lokesh Boran : QC3-4396

      req.items.__IP__ = req.ip;
      entity.createAsync(req.items)
      .then(function postThenCallback(data) {
        logger.log('Function: createAsync: (CB)postThenCallback - Success', 'info', currentFileName);
        req.status = 201;
        req.data = data;
        next();
        return;
      })
      .catch(function(ex){
        logger.log('Function: postRouteHandler - Catch : ' + ex, 'error', currentFileName);
        debug(ex);
        if(ex.name === 'MongoError'){
          next(ex);
        }else {
          res.status(422).json({
            'message': ex.message
          });
        }
        //return;
      });
    });

  }

  function postSoftlyRouteHandler(req, res, next) {
    logger.log('Function: postSoftlyRouteHandler - Success', 'info', currentFileName);
    if(req.items._id){
      req.params._id = req.items._id.length === 24 ? resources.db.ObjectID(req.items._id) : req.items._id;
    }

    req.items.createdBy = req.user._id;
    req.items.modifiedBy = req.user._id;
    req.items.__IP__ = req.ip;
    req.items.timeZone = getCurrentTimeZone(req);

    entity.softCreateAsync(req.items)
    .then(function postThenCallback(data) {
      logger.log('Function: softCreateAsync: (CB)postThenCallback - Success', 'info', currentFileName);
      req.status = 201;
      req.data = data;
      next();
      return;
    })
    .catch(function(ex){
      logger.log('Function: postSoftlyRouteHandler - Catch : ' + ex, 'error', currentFileName);
      debug(ex);
      if(ex.name === 'MongoError'){
        next(ex);
      }else {
        res.status(422).json({
          'message': ex.message
        });
      }
    });
  }

  function putRouteHandler(req, res, next) {
    logger.log('Function: putRouteHandler - Success', 'info', currentFileName);
    req.items.modifiedBy = req.user._id;
    req.items.__IP__ = req.ip;
    req.items.timeZone = getCurrentTimeZone(req);

    entity.updateAsync( resources.db.ObjectID(req.params._id), req.items)
    .then(function putThenCallback(data) {
      logger.log('Function: updateAsync: (CB)putThenCallback - Success', 'info', currentFileName);
      req.data = data;
      next();
      return;
    })
    .catch(function(ex){
      logger.log('Function: putRouteHandler - Catch : '+ ex, 'error', currentFileName);
      debug(ex);
      if(ex.name === 'MongoError'){
        next(ex);
      }else {
        res.status(422).json({
          'message': ex.message
        });
      }
    });
  }


  function putSoftlyRouteHandler(req, res, next) {
    logger.log('Function: putSoftlyRouteHandler - Success', 'info', currentFileName);
    req.items.modifiedBy = req.user._id;
    req.items.__IP__ = req.ip;
    req.items.timeZone = getCurrentTimeZone(req);

    entity.softUpdateAsync( resources.db.ObjectID(req.params._id), req.items)
    .then(function putThenCallback(data) {
      logger.log('Function: softUpdateAsync: (CB)putThenCallback - Success', 'info', currentFileName);
      req.data = data;
      next();
      return;
    })
    .catch(function(ex){
      logger.log('Function: postSoftlyRouteHandler - Catch : ' + ex, 'error', currentFileName);
      debug(ex);
      next(ex);
    });
  }





  function patchRouteHandler(req, res, next) {
    logger.log('Function: patchRouteHandler - Success', 'info', currentFileName);
    req.items.modifiedBy = req.user._id;
    req.items.__IP__ = req.ip;
    req.items.timeZone = getCurrentTimeZone(req);
    req.items.customHeaders = {
      allowPut: req.headers['allow-put']
    };

    //Only parse _id to objectID if and only if its length is exactly equal to 24.
    req.params._id = req.params._id.length === 24 ? resources.db.ObjectID(req.params._id) : req.params._id;
    db['users'].findOne({_id: db.ObjectID(req.user._id)}, function(e,u) {
      if(e){
        logger.log('Collection: users - Error : '+ e, 'error', currentFileName);
        return;
      }
      if(u){
        logger.log('if(u) - Success', 'info', currentFileName);
        req.items.modifiedByName = u.firstname + " " + u.lastname + " ("+req.user.username+")";

        entity.updateAsync( req.params._id, req.items)
        .then(function putThenCallback(data) {
          logger.log('Function: updateAsync: (CB)putThenCallback - Success', 'info', currentFileName);
          req.data = data;
          next();
          return;
        })
        .catch(function(ex){
          logger.log('Function: patchRouteHandler - Catch : ' + ex, 'error', currentFileName);
          debug(ex);
          if(ex.name === 'MongoError'){
            next(ex);
          }else {
            res.status(422).json({
              'message': ex.message
            });
          }
        });
      }
    });

  }

  function patchSoftlyRouteHandler(req, res, next) {
    logger.log('Function: patchSoftlyRouteHandler - Success', 'info', currentFileName);
    req.items.modifiedBy = req.user._id;
    req.items.__IP__ = req.ip;
    req.items.timeZone = getCurrentTimeZone(req);
    //PQT-956 by Yamuna
    req.items.customHeaders = {
      allowPut: req.headers['allow-put']
    };

    entity.softUpdateAsync( resources.db.ObjectID(req.params._id), req.items)
    .then(function putThenCallback(data) {
      logger.log('Function: softUpdateAsync: (CB)putThenCallback - Success', 'info', currentFileName);
      req.data = data;
      next();
      return;
    })
    .catch(function(ex){
      logger.log('Function: patchSoftlyRouteHandler - Catch : '+ex, 'error', currentFileName);
      debug(ex);
      next(ex);
    });
  }

  return {
    GET : {
      'ALL': getAllRouteHandler,
      'ONE': getOneRouteHandler,
      'MAJOR': getOneMajorRouteHandler,
      'PUBLISHED': getOnePublishedRouteHandler,
      'ALLVERSIONS': getAllVersionsRouteHandler,
      'ONEVERSION': getOneVersionRouteHandler,
      'ALLLOGS': getAllLogsHandler
    },
    POST: {
      'ONE': postRouteHandler,
      'ONESOFT': postSoftlyRouteHandler
    },
    PUT: {
      'ONE': putRouteHandler,
      'ONESOFT': putSoftlyRouteHandler
    },
    PATCH :{
      'ONE': patchRouteHandler,
      'ONESOFT': patchSoftlyRouteHandler
    }
  }
}

function getCurrentTimeZone(req) {
  return req.headers['client-tz'] ? req.headers['client-tz'] : config.timeZonesP[config.currentTimeZoneP];
}

function postRouteHandlerExport(collection, data, req, res, next) {
  logger.log('Function: postRouteHandler - Success', 'info', currentFileName);
  if (data._id) {
    logger.log('if(data._id) - Success', 'info', currentFileName);
    req.params._id = data._id.length === 24 ? resources.db.ObjectID.isValid(data._id) : data._id;
  }

  if (data.createById && req.method === 'POST') {
    logger.log('if (data.createById && req.method === POST) - Success', 'info', currentFileName);
    req.user._id = data.createById;
    delete data.createById;
  }

  data.createdBy = req.user._id;
  data.modifiedBy = req.user._id;
  data.timeZone = getCurrentTimeZone(req);

  db['users'].findOne({ _id: db.ObjectID(req.user._id) }, function (e, u) {
    if (e) {
      logger.log('Collection: users - Error : ' + e, 'error', currentFileName);
      return;
    }
    logger.log('Collection: users - Success', 'info', currentFileName);

    if (u) {
      logger.log('Collection: users: if(u) - Success', 'info', currentFileName);
      data.createdByName = u.firstname + " " + u.lastname + " (" + u.username + ")";
      data.modifiedByName = u.firstname + " " + u.lastname + " (" + u.username + ")";
    }

    data.__IP__ = req.ip;
    var resource = resources.fetch({
      name: collection,
      collection: collection
    });

    resource.create(data, function postThenCallback(error, responce) {
      logger.log('Function: createAsync: (CB)postThenCallback - Success', 'info', currentFileName);
      next(responce);
      return;
    })
  });
}

function executeAPIOperation(collection, data, req, res, next) {

  logger.log('API Called : api/advancedsearch', 'info', currentFileName);

  var xToken = req.headers['x-authorization'] || req.query['x-authorization'];
  if (xToken) {
    logger.log('if(xToken) : Inside', 'info', currentFileName);
    db['allAuthToken'].findOne({
      token: xToken
    }, function (e, token) {
      if (e || !token) {

        logger.log('Collection: allAuthToken - Failed : ' + e, 'error', currentFileName);

        res.status(403).json({
          message: 'You are not authorized.'
        });
        return;
      }

      logger.log('Collection: allAuthToken- success', 'info', currentFileName);

      db['users'].findOne({
        _id: db.ObjectID(token.userId || token.user._id)
      }, function (err, user) {
        if (err) {
          logger.log('Collection:  call -failed : ' + err, 'error', currentFileName);
          res.status(403).json({
            message: 'You are not authorized.'
          });
          return;
        }

        logger.log('Collection:  users - success', 'info', currentFileName);
        req.user = user || {};

        if (req.user['applicableSites']) {
          logger.log("if (req.user['applicableSites']): Inside", 'info', currentFileName);
          var accessiableSites = req.user['applicableSites'];
          req.user['applicableSitesList'] = _.map(_.cloneDeep(req.user['applicableSites']), function (sites) {
            return _.map(sites, function (subSites) { return subSites._id });
          });
          req.user['applicableSites'] = _.map([].concat.apply([], accessiableSites), function (site) {
            return site._id;
          });
        }
        var rolesToValidate = _.map((user.roles || []), function (role) {
          return db.ObjectID(role._id);
        });
        db['roles'].find({
          _id: {
            '$in': rolesToValidate
          },
          modules: {
            $exists: true
          }
        }, {
            modules: 1
          }).toArray(function (err, roles) {

            logger.log('Collection: roles- success', 'info', currentFileName);
            var schema = [];
            _.forEach((roles || []), function (role) {
              _.forEach((role.modules || []), function (module) {
                _.forEach((module.permissions || []), function (permission) {
                  if (permission.view && permission.entityType === 'schemas') {
                    schema.push(permission._id + '');
                    // schema.push(typeof permission._id == 'string'? db.ObjectID(permission._id):permission._id);
                  }
                });
              });
            });

            req.user.authorizedSchemas = _.uniq(schema);
            postRouteHandlerExport(collection, data, req, res, next);

          });

      });

    });
  } else if (req.headers['authorization'] || req.query['authorization']) {
    logger.log("else if (req.headers['authorization'] || req.query['authorization'])- Inside authorization brear token", 'info', currentFileName);
    var decompressToken = lzstring.decompressFromEncodedURIComponent(req.headers['authorization']);
    var token = decompressToken;
    if (token && _.startsWith(token, 'Bearer ')) {
      logger.log("if(token && _.startsWith(token,'Bearer ')) : Inside decompressed authorization brear token ", 'info', currentFileName);
      decode(token.substring(7), function (err, data) {
        if (err) {
          logger.log('Error: decoding authorization brear token failed : ' + err, 'error', currentFileName);
          res.status(403).json({
            message: 'You are not authorized.'
          });
          return;
        }
        logger.log('decode: Success - Inside decoded authorization brear token Info', 'info', currentFileName);
        req.user = data.user || {};
        postRouteHandlerExport(collection, data, req, res, next);
      });
    } else {
      logger.log('else: authorization brear token decompression failed', 'info', currentFileName);
      res.status(403).json({
        message: 'You are not authorized.'
      });
    }
  } else {
    logger.log('else: No token found', 'info', currentFileName);
    res.status(403).json({
      message: 'You are not authorized.'
    });
  }
}

module.exports = {
  build: build,
  postRouteHandler: executeAPIOperation
}
