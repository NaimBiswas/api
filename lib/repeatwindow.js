/**
* @name Qulification Performance - Repeat Window Service
* @author Prachi Thakkar <prachi.t@productivet.com>
*
* @version 1.0
*/

//var Promise = require('promise');
var colors = require('colors');
var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('./logger').configure(__dirname, enableBunyan);
var config = require('../config/config.js');
var CronJob = require('cron').CronJob;
var logger = require('../logger');
var mailer = require('./mailer');
var _ = require('lodash');
var db = require('./db');
var debug = require('./logger').debug('lib:repeatwindow');
var q = require('q');
var moment = require('moment-timezone');
var restler = require('restler');
var ip = require('ip');
var handlebars = require('handlebars');
var currentFileName = __filename;
var windowFrequency = require('../routes/windows/windowFrequencyAlert.js');
var getRecords = require('../routes/qulificationperformance/searchRecords.js');
var formattedModelForEmail = require('../routes/qulificationperformance/formattedModelForEmail.js');

//For Dev Deployment
// var repeatJob = new CronJob('0 */2 * * *', function () {
//   repeatWindow();
// }, function () {},
// true
// );

//For qa deployment: Every Month Repeat Window 12 AM
var job = new CronJob('0 0 1 * *', function() {
  console.log('---------------------------------Repeat Window repeatJob started-------------------------------------');
  repeatWindow();
}, function () {},
true
);

handlebars.registerHelper('ifCondition', function (v1, operator, v2, options) {
    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
          }
});

//For removing previous month frequency from calendar Windows and add in to calendarWindowsNotified collection which mail was sent
function removePreviousMonthFrequency(){
  console.log('---------------------------------Repeat Window removePreviousMonthFrequency-------------------------------------');
  var deferred = q.defer();
  var startDate = new Date(new Date().getFullYear(), new Date().getMonth()-1, 1);
  var endDate = new Date(new Date().getFullYear(), new Date().getMonth(), 0);
  db
  .collection('calendarWindows')
  .find({
    'notified': true,
    'date': {
      $gte: new Date(startDate.toISOString()),
      $lte: new Date(endDate.toISOString())
    }
  }).toArray(function (err, entries) {
    if(err){
      logger.log("collection:calendarWindows error" , 'error', currentFileName);
      deferred.reject(err);
    }
    if(entries && entries.length){
      db
      .calendarWindowsNotified
      .insertManyAsync(entries)
      .then(function (data) {
        //Successfully
        db
        .calendarWindows
        .removeManyAsync({
          'notified': true,
          'date': {
            $gte: new Date(startDate.toISOString()),
            $lte: new Date(endDate.toISOString())
          }
        })
        .then(function (data) {
          deferred.resolve(true);
        })
        .catch(function (err) {
          //fail to remove
          deferred.reject(err);
        });
      })
      .catch(function (err) {
        //fail to insert
        deferred.reject(err);
      });
    }else{
      deferred.resolve(true);
    }
  });
  return deferred.promise;
}

//For getting windows of the previous month
function getAllWindows() {
  console.log('---------------------------------Repeat Window getAllWindows-------------------------------------');
  var deferred = q.defer();
  var currentDate = new Date(new Date().getFullYear(), new Date().getMonth(), 0); //last date of previous month
  db.windows.find({'windowPeriod.month':currentDate.getMonth(),'windowPeriod.year':currentDate.getFullYear()}).toArray(function (error, windows){
    if(error){
      logger.log("db call: windows - err", 'error', currentFileName);
      deferred.reject(error);
    }
    var currentWindows = windows.filter(function(window){
      return window.windowStatus.title == "Current";
    });
    var repeatWindows = windows.filter(function(window){
      return window.repeatWindow == true;
    });
    windows = {
      currentWindows: currentWindows,
      repeatWindows: repeatWindows
    }
    deferred.resolve(windows);
  });
  return deferred.promise;
}

//Update window frequency here
function updateWindowFrequency(windows, frequencyGeneratedWindow) {
  console.log('---------------------------------Repeat Window updateWindowFrequency-------------------------------------');
  var deferred = q.defer();
  if (windows.length) {
    var window = windows.shift();
    if(!window.repeatWindow){
      window.windowPeriod.month = window.windowPeriod.month + 1;
    }
    windowFrequency.storeEntries(window, '', function(err, res){
      frequencyGeneratedWindow.push(window);
      if(err){
        logger.log("function: storeEntries - err", 'error', currentFileName);
      }
      updateWindowFrequency(windows, frequencyGeneratedWindow).then(function(err,data){
        deferred.resolve(frequencyGeneratedWindow);
      }, function(){
        deferred.resolve(frequencyGeneratedWindow);
      });

    });
  }else{
    deferred.resolve(frequencyGeneratedWindow);
  }
  return deferred.promise;
}

//This function for Audit Trail
function getAuditFunction(windowData, callback) {
  console.log('---------------------------------Repeat Window getAuditFunction-------------------------------------');
  try {
    if (windowData) {
      //For windows audit trail
      db.windows.auditThis1(windowData, ip.address());
      callback(null, windowData);
    }
  } catch (e) {
    callback(e);
  }

}

//For create repeat window, generate Audit trail, Update frequency and sent email
function updateWindowDetail(windows, repeatWindows) {
  console.log('---------------------------------Repeat Window updateWindowDetail-------------------------------------');
  var deferred = q.defer();
  if (windows && windows.length) {
    var data = windows.shift();
    if (data) {
      db.windows.count({}, function(err, getAllWindowCount) {
        //Delete unwanted window data
        var keepId = _.cloneDeep(data._id);
        if (data.firstStageInformation) {
          delete data.firstStageInformation;
        }
        delete data._id;
        delete data.windowId;
        delete data.windowStartDate;
        delete data.windowStatus;
        delete data.modifiedBy;
        delete data.modifiedByName;
        delete data.modifiedDate;
        delete data.version;
        delete data.versions;
        delete data.isMajorVersion

        //Create new window object
        if (data.windowPeriod.month == 11) {
          data.windowPeriod.month = 0;
          data.windowPeriod.year = data.windowPeriod.year + 1;
        }else {
          data.windowPeriod.month = data.windowPeriod.month + 1;
        }
        data.title = data.title + '(' + data.windowPeriod.year + '-' + ( data.windowPeriod.month ) + ')';
        data.windowId = getAllWindowCount + 1;
        data.windowStartDate = new Date(data.windowPeriod.year, data.windowPeriod.month, 1);
        data.windowStatus = {
          'id':1,
          'title':'Current'
        }
        data.timeZone  = config.timeZonesP[config.currentTimeZoneP] ? config.timeZonesP[config.currentTimeZoneP] : data.timeZone;
        repeatWindows.push(data);
        db.windows.insertOne(data, function(err,insertedData){
          console.log('-------------------------------_Window Inserted-------------------------------');
          if(err){
            //fail
            logger.log("call:insertOne error : " + err , 'error', currentFileName);
          }
          db.windows.updateOne({ "_id" : db.ObjectID(keepId) }, { $set: { "isGeneratedRepeatWindow": true } }, function(err, update) {
            console.log('-------------------------------_Window Updated-------------------------------');
            if(err){
              //fail
              logger.log("call:updateOne repeatWindow error : " + err , 'error', currentFileName);
            }
            //This function for Audit Trail
            getAuditFunction(insertedData.ops[0], function(err, windowData) {
              var windowDataForUpdateFrequncy = [];
              windowDataForUpdateFrequncy.push(windowData);
              //This function for update window frequency
              updateWindowFrequency(windowDataForUpdateFrequncy,[]).then(function(err,data) {
                var repeatWindowdata = repeatWindows[repeatWindows.length - 1];
                var params = {
                  id: repeatWindowdata._id,
                  windowId: repeatWindowdata.windowId
                }
                getRecords.fetchAppRecords(params, function(error, data) {
                  if(err){
                    //something goes wrong
                  }
                  if(data == "No Records Found."){
                    repeatWindowdata.countDetails = '0';

                  }else if(_.size(data)){
                    repeatWindowdata.countDetails = data;
                  }
                formattedModelForEmail.getFormattedModel(repeatWindowdata).then(function(modelForEmail){
                  modelForEmail.templateType = "createWindow";
                  notify(modelForEmail,function(err,result){
                    if(err){
                      logger.log("call:notify error : " + err , 'error', currentFileName);
                      return;
                    }
                  logger.log("call:notify res" , 'info', currentFileName);
                  //call function recursively
                  updateWindowDetail(windows, repeatWindows).then(function(err,data){
                    deferred.resolve(repeatWindows);
                  }, function(){
                    deferred.resolve(repeatWindows);
                  });
                });
              });
              });
              });
            });
          });
        });
      });
    }else {
      updateWindowDetail(windows, repeatWindows).then(function(err,data){
        deferred.resolve(repeatWindows);
      }, function(){
        deferred.resolve(repeatWindows);
      });
    }
  }else {
    console.log('----------------------------------No Window Found-------------------------------');
    deferred.resolve(repeatWindows);
  }
  return deferred.promise;
}

/**
* Method: repeatWindow
* Purpose : Function for Repeat Window
*/
function repeatWindow() {
  console.log('---------------------------------Repeat Window call:starting repeatJob-------------------------------------');
  logger.log("call:starting repeatJob" , 'info', currentFileName);
  removePreviousMonthFrequency().then(function() {
    getAllWindows().then(function(windows) {
      updateWindowFrequency(windows.currentWindows,[]).then(function(data) {
        updateWindowDetail(windows.repeatWindows, []).then(function(data) {
          //Resolve Success
        });
      });
    });
  });
}

function notify(entity,cb){
  mailer.send('qp-notify-common', entity, entity.emailAddresses, function sendMailCallback(err, value) {
    if (err) {
      debug.err('Failed sending mail - window-created-notify');
      debug.err(err);
      cb(err, entity);
    } else {
      logger.log('Email has been sent to ' + entity.emailAddresses);
      debug('Mail send successfully to: ' + entity.emailAddresses);
      cb(null, entity);
    }
  });
}
