/**
* @name logger
* @author Dave Amit <dave.a@productivet.com>
*
* @version 0.0.0
*/

'use strict'
var bunyan = require('bunyan');
var path = require('path');
var reqDebug = require('debug');
var bunyanLog = {};
var _ = require('lodash');

/**
* Debug configuration routine to setup bunyan and debug module.
*
* @param  {String} - Name of the log, this name will appear. All logs emited
*         using this instance will be prepaned by name.
*         E.g. "IdealApiArch:core  System startup sucessful"
* @return {Object} - Configured db instance.
*
* @public
*/
function debug (name){
  var debug = reqDebug('IdealApiArch:' + name);
  var warn = reqDebug('IdealApiArch:' + name + ":warn");
  var error = reqDebug('IdealApiArch:' + name + ":error");

  var log = createChild({component: name});

  function info (text){
    log.info(text);
    if(text instanceof Error){
      error(text.message.red);
      error(text);
    } else if(text instanceof Object){
      debug(text);
    }
    else{
      if(!_.isUndefined(text)){
        debug(text.green);
      }
    }
  }

  info.warn = function wrn(text){
    log.warn(text);
    if(text instanceof Object){
      warn(text);
    }
    else{
      warn(text.blue);
    }
  }

  info.err = function err(text){
    log.error(text);
    if(text instanceof Error){
      error(('********************* Error: ' + text.message.bold.italic.underline + ' ************************').red);
      error(text);
      error('********************************** Error End ***********************************'.red);
    } else if(text instanceof Object){
      error(text);
    }
    else{
      error(text.red);
    }
  }

  info.log = function (text){
    log.log(text);
    if(text instanceof Object){
      debug(text);
    }
    else{
      if(_.isUndefined(text)){
        debug(text.green);
      }
    }
  }
  return info;
}
/**
* Debug configuration routine to setup bunyan log files.
*
* @param  {String} - Root directory, this is used to locate storage directory.
* @param  {Boolean} - indicates whether bunyan logger is enabled or disabled.
* @return {Function} - createChild routine.
*
* @public
*/
function configure(dir,enabled){

  if(enabled){
    bunyanLog = bunyan.createLogger({
      name: 'IdealApiArch',
      serializers: {
        req: bunyan.stdSerializers.req,
        res: bunyan.stdSerializers.res,
        err: bunyan.stdSerializers.err
      },
      streams: [{
        type: 'rotating-file',
        path: path.join(dir, 'logs', 'req.json'),
        period: '1d',   // daily rotation
        count: 3        // keep 3 back copies
      }]
    });
  } else {
    bunyanLog = {
      child: function child(context) {
        return {
          info: function info() {

          },
          error: function error() {

          },
          warn: function warn() {

          },
          log: function log() {

          }
        };
      }
    }
  }

  return createChild;
}

/**
* Debug configuration routine to setup bunyan and debug module.
*
* @param  {String} - Name of the log, this name will appear. All logs emited
*         using this instance will be prepaned by name.
*         E.g. "IdealApiArch:core  System startup sucessful"
* @return {Object} - Configured bunyan logger instance.
*
* @public
*/
function createChild(context){
  return bunyanLog.child(context);
};



module.exports = {
  createChild: createChild,
  debug: debug,
  configure: configure
};
