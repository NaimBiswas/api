/**
* @name logger
* @author Dave Amit <dave.a@productivet.com>
*
* @version 0.0.0
*/

'use strict'
module.exports = require('./logger');
