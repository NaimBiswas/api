
var exponentialType = require('./condition_types/exponential_type.threshold');
var dateType = require('./condition_types/date_type.threshold');
var defaultType = require('./condition_types/default_type.threshold');
var numberType = require('./condition_types/number_type.threshold');
var LeftOperand = require('./left_operand.threshold');
var RightOperand = require('./right_operand.threshold');
var _ = require('lodash');

function Condition(_condition, model) {
    
    var leftOperand;
    var rightOperand; 
    var index;

    if(!isGroup()){
        leftOperand = LeftOperand(_condition.leftOperand);
        rightOperand = RightOperand(_condition.rightOperand);
        
        leftOperand.setModel(model);
    
        rightOperand.setFrom(_condition.rightOperand);
        rightOperand.setTo(_condition.rightEndOperand);
        rightOperand.setDateSelection(_condition.dateSelection);
    
        leftOperand.setFormat(leftOperand.getFormat());
        rightOperand.setFormat(leftOperand.getFormat());
        
        leftOperand.setViewType(leftOperand.getViewType());
        rightOperand.setViewType(leftOperand.getViewType());
    }
    
    var _self = {
        data: _condition,
        setIndex: setIndex,
        getIndex: getIndex,
        leftOperand: leftOperand,
        rightOperand: rightOperand,
        getResult: getResult,
        isGroup: isGroup,
        isStandalone: isStandalone,
        logicalOperation: logicalOperation,
        getConditionGroup: getConditionGroup
    }
    return _self;

    function setIndex(_index) {
        index = _index;
    }

    function getIndex() {
        return index;
    }
    
    function getResult() {
        return getConditions()[_condition.comparison.toLowerCase()](_self)
    }

    // @private to getResult()
    function getConditions() {
        switch (leftOperand.getFormat()) {
            case 'Exponential':
                return exponentialType;
            case 'Scientific':
                 return exponentialType;
            case 'Date':
                return dateType;
            case 'Number':  
                return numberType; 
            case 'Currency':  
                return numberType;
            case 'Percentage':  
                return numberType;  
            default:
                return defaultType;
        }
    }

    function getConditionGroup() {
        return _condition.conditions;
    }

    function logicalOperation() {
        return ' ' + ((index && _condition.bool) ? ' ' + logicalMeanings[_condition.bool] + ' ' : '')
    }

    function isGroup() {
        return !!_condition.conditions;
    }

    function isStandalone() {
        return !_.isUndefined(_condition.comparison) && _condition.comparison !== null;
    }
}

Condition.instanciate = function instanciateCondition(cb, options){
    return function(_condition, index){
        var iCondition = Condition(_condition, options.model);
        iCondition.setIndex(index);
        cb(iCondition)
    }
}

Condition.wrap = function wrap(mainConditionGroup, model) {

    return wrapGroup(mainConditionGroup);

    function wrapGroup(conditionGroup) {
        var val = '';
        _.forEach(conditionGroup, Condition.instanciate(function (iCondition) {
            
            if (iCondition.isGroup()) {
                val = val + iCondition.logicalOperation() + '(' + wrapGroup( iCondition.getConditionGroup()) + ')';
            } else if (iCondition.isStandalone()) {
                val = val + iCondition.logicalOperation() + iCondition.getResult();
            }
        }, {model: model}) );
        return val;
    }
}


    // @public #recursive
Condition.stringify = function stringifyCondition(_conditions) {

// _calendar.threshold.conditions
    var appliedConditions = "";
    recursiveCondition(_conditions);
    return appliedConditions;

    function recursiveCondition(conditions) {
        (conditions || []).forEach(function (c) {
            if (c.conditions) {
                if (c.bool && appliedConditions) {
                    appliedConditions += ' ' + c.bool + ' ';
                }
                appliedConditions += '(';
                recursiveCondition(c.conditions);
                appliedConditions += ')';
            } else {
                if (c.leftOperand && c.leftOperand.type.format.title == 'Date' && c.dateSelection) {
                    if (c.bool && appliedConditions) {
                        appliedConditions += ' ' + c.bool + ' ';
                    }
                    appliedConditions += (c.leftOperand.title + ' ' + c.comparison + ' ' + (c.dateSelection === 'Specific Date' ? moment(c.rightOperand).format('MM/DD/YYYY') : c.dateSelection));
                } else {
                    if (c && c.leftOperand) {
                        if (c.bool && appliedConditions) {
                            appliedConditions += ' ' + c.bool + ' ';
                        }
                        appliedConditions += (c.leftOperand.title + ' ' + c.comparison + ' ' + c.rightOperand);
                    }
                }
            }
        })
    }
}

var logicalMeanings = {
    "AND": '&&',
    "OR": '||'
};

module.exports = Condition
