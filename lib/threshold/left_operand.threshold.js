var _ = require('lodash');
var Operand = require('./operand.threshold');
var thresholdConstants = require('./constants.threshold');

function LeftOperand(_operand) {

    var model;

    function setModel(_model) {
        model = _model
    }

    function getModel() {
        return model;
    }

    function getValue(){
        switch (_self.getViewType()) {
            case thresholdConstants.viewType.ATTRIBUTE:{
                return model[`${_operand['s#']}`];
            }
            case thresholdConstants.viewType.QUESTION:{
                return model[`${_operand['s#']}-${_operand['parentId']}`];
            }
            case thresholdConstants.viewType.SITE:{
                if (model.sites) {
                    return _.map(model.sites.allicableSites[_operand.comboId], 'title');
                } else {
                    return [];
                }
            }
            case thresholdConstants.viewType.RECORD_STATUS:{
                return model['recordStatus'];
            }
        }
    }
    
    function getViewType() {
        _operand
        if(_self.viewType){
            return _self.viewType;
        } else if(isAttribute()){
            return thresholdConstants.viewType.ATTRIBUTE
        } else if(isQuestion()){
            return thresholdConstants.viewType.QUESTION
        } else if(isSite()){
            return thresholdConstants.viewType.SITE
        } else if(isRecordStatus()) {
            return thresholdConstants.viewType.RECORD_STATUS
        }
    }

    function isAttribute() {
        return (_.isObject(_operand) && !_operand.parentId);
    }
    function isQuestion() {
        return !!(_.isObject(_operand) && _operand.parentId);
    }
    function isSite(){
        return !!(_operand.isSite);
    }
    function isRecordStatus() {
        return _operand.comboId == 'recordstatus';
    }

    function getFormat() {
        return _self.format || (_.get(_operand, 'type.format.title') || 'None');
    }

    var _self = Object.assign(Operand(_operand), {
        getViewType: getViewType,
        getValue: getValue,
        setModel: setModel,
        getModel: getModel,
        getFormat: getFormat
    });

    return _self;
}

module.exports = LeftOperand