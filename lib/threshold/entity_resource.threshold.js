var Promise = require('bluebird');
var config = require('../../config/config.js');
var _ = require('lodash');
var db = Promise.promisifyAll(require('../db'));
var debug = require('../logger').debug('lib:scheduler');
var Resource = require('./resource.threshold');

function EntityResource(schemas){
    
    // @public
    function getSchemasDetailed() {
        var ids = _.map((schemas || []), function (schem) {
            return db.ObjectID(schem._id.toString());
        });

        return db
            .collection("entities")
            .find({
                '_id': {
                    '$in': ids
                }
            })
            .toArrayAsync();
    }

    // @public
    function getFields(entities) {
        var fields = {};

        _.forEach(entities, function (_entity) {
            fields[_entity.title] = [];
            _.forEach(_entity.questions, function (que) {
                fields[_entity.title].push({
                    title: que.title,
                    's#': que['s#'],
                    type: que.type
                });
            });
            //recordStatus
            fields[_entity.title].push({title: 'recordStatus'});
        });

        return fields;
    }

    // @public
    function mapRecord(record) {
        var transformedModelEntity = _.reduce(record.entityRecord, function (a, value, question) {
            var procIds = question.split('-');
            if (procIds.length > 1) {
                if (value[procIds[1]] || value[procIds[1]] === 0) {
                    a[question] = value[procIds[1]];
                }
            } else {
                if (procIds[0] && procIds[0] !== undefined && procIds[0] !== 'isActive' && procIds[0] !== 'entityvalueinactivereason') {
                    a[question] = value;
                }
            }
           
            return a;
        }, {});

        //PQT-1431
        transformedModelEntity.recordStatus = getRecordStatus(record);
    
        //PQT-2050
        if(record.sites && record.sites.allicableSites){
        _.forEach(record.sites.allicableSites, function (sites, index) {        
            var siteObj = _.reduce(sites, function (a, siteLevel, idx) {
                a[index] = siteLevel.title;
                return a;
            }, {});
            Object.assign(transformedModelEntity,siteObj);       
        });//
        }

        transformedModelEntity.sites = record.sites;
        return transformedModelEntity;
    }

    // @public
    function reflectRecord(record, mapedRecord) {
    }

    // @private to mapRecord()
    function getRecordStatus(record) {
        return record.entityRecord.isActive? 'Active' : 'Inactive'
    }

    var _self = Object.assign(Resource(schemas), {
        getFields: getFields,
        mapRecord: mapRecord,
        reflectRecord: reflectRecord,
        getSchemasDetailed: getSchemasDetailed
    });

    return _self;

}

module.exports = EntityResource
