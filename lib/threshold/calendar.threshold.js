
var Promise = require('bluebird');
var EntityResource = require('./entity_resource.threshold');
var SchemaResource = require('./schema_resource.threshold');
var _ = require('lodash');
var db = Promise.promisifyAll(require('../db'));
var moment = require('moment-timezone');
var config = require('../../config/config.js');
var logService = require('../logService');
var Condition = require('./condition.threshold');

function Calendar(_calendar) {
    var logger;
    var resource;
    var logInfo = { 'calendarThresholdId': _calendar._id, 'title': _calendar.threshold.title };
    
    function _constructor() {
        if (_calendar.thresholdType == "entity") {
            resource = EntityResource(_calendar.schema);
        } else {
            resource = SchemaResource(_calendar.schema);
        }
        return {
            hasMatchingCount: hasMatchingCount,
            resource: resource,
            logInfo: logInfo,
            setLogger: setLogger,
            data: _calendar,
            getRecords: getRecords,
            getResource: getResource,
            shouldSendFullReport: shouldSendFullReport,
            updateLastRunTime: updateLastRunTime,
            stringifyConditions: stringifyConditions,
            getThreshold: getThreshold,
            shouldSendWithEvenZERORecords: shouldSendWithEvenZERORecords
        };
    }
    var self = _constructor();
    return self;

    // @public
    function getRecords() {
        var filteredRecords = [];
        return resource.eachSchema_Records(getLimits_schemaRecord(), function(schema, records, index){

            filteredRecords[index] = filterRecords(records, getThreshold().conditions);
        }).then(function(){
            return filteredRecords;
        })
    }

    // @public
    function filterRecords(records, conditions) {
        return _.filter(records, function (_record, idx) {

            var mapedRecord = resource.mapRecord(_record);
            resource.reflectRecord(_record, mapedRecord);
            
            var wrapedConditionResult = Condition.wrap(conditions, mapedRecord);

            return eval(wrapedConditionResult);
        });
    }  
    
    // @private to getRecords()
    function getLimits_schemaRecord() {
        
        if (shouldSearchWithLimit()) {

            var dateStart;
            var dateEnd;
            
            if (getThreshold().frequency.type == 'Hourly') {
                dateEnd = moment().startOf('hour');
                dateStart = _.cloneDeep(dateEnd).add(-1, 'hour');
            } else {
                dateEnd = moment().startOf('day');
                if (getThreshold().lastRunTime) {
                    dateStart = moment(getThreshold().lastRunTime).startOf('day');
                } else {
                    dateStart = moment(getThreshold().startDate).startOf('day');
                }
            }

            return {
                modifiedDate: {
                    $gte: dateStart.toDate(),
                    $lt: dateEnd.toDate()
                }
            };
        } else {
            return {};
        }
    }

    // @private to getLimits_schemaRecord()
    function shouldSearchWithLimit(){
        return _.get(getThreshold(), 'reportconfiguration.whichMatchingRecord')  != 'all';
    }
    function shouldSendWithEvenZERORecords(){
        return !!getThreshold().sendEmailIfNoRecords;
    }

    

    // @public
    function shouldSendFullReport() {
        return _.get(getThreshold(), 'reportconfiguration.exatmatchrecord') === 'Send Full Report';
    }

    // @public
    function hasMatchingCount(filteredRecords) {
        var entryCount = (filteredRecords || []).reduce(function (acc, curr) {
            return acc + (curr || []).length;
        }, 0);

       // if (filteredRecords && filteredRecords.length) {
            switch (getThreshold().matchingtrcord.operator.toLowerCase()) {
                case 'equal to':
                    return entryCount == parseInt(getThreshold().matchingtrcord.value);
                    break;

                case 'not equal to':
                    return entryCount != parseInt(getThreshold().matchingtrcord.value);
                    break;

                case 'greater than':
                    return entryCount > parseInt(getThreshold().matchingtrcord.value);
                    break;

                case 'less than':
                    return entryCount < parseInt(getThreshold().matchingtrcord.value);
                    break;

                case 'greater than or equal to':
                    return entryCount >= parseInt(getThreshold().matchingtrcord.value);
                    break;

                case 'less than or equal to':
                    return entryCount <= parseInt(getThreshold().matchingtrcord.value);
                    break;

                default:
                    return 0;
            }
       // }

    }

    // @public
    function setLogger(_logger) {
        logger = _logger;
        self.logger = logger;
    }

    // @public
    function getResource() {
        return resource;
    }

    // @public
    function updateLastRunTime() {
        db.thresholds.update({ '_id': getThreshold()._id }, { $set: { 'lastRunTime': new Date() } }, { multi: false });
    }

    // @public #recursive
    function stringifyConditions() {
        return Condition.stringify(getThreshold().conditions)
    }

    // @private
    function getThreshold(){
        return _calendar.threshold;
    }
}

// @static
Calendar.getCurrentAll = function getCurrent(cronType) {

    var start;
    var end;
    var type;  

    if (cronType == 'hourly') {
        start = moment().startOf('hour');
        end = _.cloneDeep(start).add(1, 'hour');
        type = {
            '$eq': 'Hourly'
        };
    } else {
        start = moment().startOf('day');
        end = _.cloneDeep(start).add(1, 'day')
        type = {
            '$ne': 'Hourly'
        };
    }

    var matchCondition = {
        'isActive': true,
        'type': type,
        'notified': false,              
        'date': {
            $gte: start.toDate(),
            $lt: end.toDate()
        }
    }

    return db.collection('calendarThresholds')
        .aggregateAsync([{
            $match: matchCondition
        },
        {
            $lookup: {
                from: "thresholds",
                localField: "thresholdId",
                foreignField: "_id",
                as: "threshold"
            }
        },
        {
            "$unwind": "$threshold"
        }
        ]).then(function (calendars) {
            if (!calendars || !calendars.length) {
                throw "no threshold found for today";
            } else {
                return calendars;
            }
        })
}

// @static
Calendar.promiseEach = function promiseEach(cb, logger) {
    return function (calendars) {
        return Promise.each(calendars, Calendar.instanciate(cb, logger))
    }
}

// @static
Calendar.instanciate = function instanciateCalendar(cb, logger) {
    return function (_calendar, idx) {

        var logger_calendar = logger.getInstance("Calendar threshold name (" + _calendar.threshold.title + ")",
            { 'calendarThresholdId': _calendar._id, 'title': _calendar.threshold.title })

        logger_calendar.log("Start Processing Calendar");

        var iCalendar = Calendar(_calendar);
        iCalendar.setLogger(logger_calendar);

        return cb(iCalendar, idx)
    }
}

module.exports = Calendar;