var Promise = require('bluebird');
var config = require('../../config/config.js');
var _ = require('lodash');
var db = Promise.promisifyAll(require('../db'));
var debug = require('../logger').debug('lib:scheduler');
var Resource = require('./resource.threshold');

function SchemaResource(schemas){

    // @public
    function getSchemasDetailed() {
        var ids = _.map((schemas || []), function (schem) {
            return db.ObjectID(schem._id.toString());
        });

        return db
            .collection("schema")
            .find({
                '_id': {
                    '$in': ids
                }
            })
            .toArrayAsync();
    } 

    // @public
    function getFields(allSchemas) {
        var fields = {};

        _.forEach(allSchemas, function (_schema) {
            fields[_schema.title] = [];

            _.forEach(_schema.attributes, function (attr) {
                fields[_schema.title].push({
                    _id: attr._id,
                    title: attr.title,
                    's#': attr['s#'],
                    type: attr.type
                });
            });

            _.forEach(_schema.entities, function (entity) {
                _.forEach(entity.questions, function (que) {
                    if (que.isAppearance) {
                        fields[_schema.title].push({
                            _id: que._id,
                            title: que.title,
                            's#': que['s#'],
                            parentId: que.parentId,
                            type: que.type,
                            isEntity: true
                        });
                    }
                });
            });

            _.forEach(_schema.procedures, function (attr) {
                _.forEach(attr.questions, function (que) {
                    fields[_schema.title].push({
                        _id: que._id,
                        title: que.title,
                        's#': que['s#'],
                        parentId: que.parentId,
                        type: que.type,
                        isEntity: false
                    });
                });
            });

            fields[_schema.title].push({title: 'recordStatus'});
        });

        return fields;
    }

    // @public
    function mapRecord(record) {
        var transformedModelApp = _.reduce(record.entries, function (a, value, question) {
            var procIds = question.split('-');
            if (procIds.length > 1) {
                if (value[procIds[1]] || value[procIds[1]] === 0) {
                    a[question] = value[procIds[1]];
                }
            } else {
                if (procIds[0] && procIds[0] !== 'FailMesssageForQCForm' && procIds[0] !== 'draftStatus' && procIds[0] !== 'isattributesadded' && procIds[0] !== 'status' && procIds[0] !== 'settings' && procIds[0] !== 'reviewStatus' &&
                    procIds[0] !== 'verifiedBy' && procIds[0] !== 'createdBy' && procIds[0] !== 'modifiedBy' && procIds[0] !== 'createdDate' && procIds[0] !== 'modifiedDate' && procIds[0] !== 'version' && procIds[0] !== 'versions' && procIds[0] !== 'isMajorVersion') {
                    a[question] = value.attributes;
                }
                if (question === 'recordStatus') {
                    a[question] = value;
                }
            }
            return a;
        }, {});
        transformedModelApp.recordStatus = getRecordStatus(record)
        return transformedModelApp;
    }

    // @public
    function reflectRecord(record, mapedRecord) {
        record.entries.recordStatus = mapedRecord.recordStatus;
    }

    // @private to mapRecord()
    function getRecordStatus(record) {
        var status;
        if (record.isRejected) {            
            if (record.entries.status.status === 1) {
                status = getReviewStatus(record) + "Passed" + '(Rejected)';
            } 
            if (record.entries.status.status === 0) {
                status = getReviewStatus(record) + "Failed" + '(Rejected)';
            }
        } else {
            if (record.entries.status.status === 1) {
                status =  getReviewStatus(record) + "Passed" ;
            } else if (record.entries.status.status === 0) {
                status =  getReviewStatus(record) + "Failed"; 
            } else if (record.entries.status.status === 3) {
                status = "Void";
            } else if (record.entries.status.status === 4) {
                status = "Draft";
            }
        }
        return status;
    }

    // @private to getRecordStatus()
    function getReviewStatus(record) {
        var reviewPending;
        if (record.workflowreview && record.workflowreview.length > 0) {
            reviewPending = _.find(record.workflowreview, { reviewstatus: 1 });
        }
        if (reviewPending) {
            return "Review Pending - ";
        } else {
            return "";
        }
    }

    var _self = Object.assign(Resource(schemas), {
        getFields: getFields,
        mapRecord: mapRecord,
        reflectRecord: reflectRecord,
        getSchemasDetailed: getSchemasDetailed
    });

    return _self;
}

module.exports = SchemaResource;
