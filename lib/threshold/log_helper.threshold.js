
var _ = require('lodash');

function map_found_thresholds(thresholds){
    return _.map(thresholds, function (_threshold) {
        return {
            _id: _threshold._id,
            title: _threshold.threshold.title,
            thresholdId: _threshold.thresholdId,
            schema: _threshold.schema,
            module: _threshold.module,
            thresholdCondition: _threshold.thresholdCondition,
            date: _threshold.date,
        }
    })
}


module.exports = {
    map_found_thresholds: map_found_thresholds
}