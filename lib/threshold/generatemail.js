var Promise = require('bluebird');


var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('../logger').configure(__dirname, enableBunyan);
var config = require('../../config/config.js');

var mailer = require('../mailer');
var settings = require('../settings');
var log = require('../../logger');
var _ = require('lodash');
var mapUsers = require('../registration').mapUserIdWithUser;
var db = Promise.promisifyAll(require('../db')); //require('./db');
var debug = require('../logger').debug('lib:scheduler');
var handlebars = require('handlebars');
var fs = require('fs');
var pdf = require('html-pdf');
var moment = require('moment');
var exportUtilty = require('../utilities/exportUtilities.js');
var generateExcel = require('../utilities/generateExcel.js');
var q = require('q');
var entriesUtility = require('../utilities/entriesUtiliese.js');

var machineDate = new Date();
var machineoffset = machineDate.getTimezoneOffset();
var clientOffset = config.timeZones[config.currentTimeZone] || -330;
var clientTZ = config.timeZonesP[config.currentTimeZoneP] || 'America/Chicago';
var mcdiff = 0;

var logService = require('../logService');
var logThreshold;
var logInfo;
var eachThresholdLog;

var storeAllSiteNames = [];
  db.siteNames.find({}).sort({ _id: 1 }).toArray(function (err, res) {
    storeAllSiteNames = res;
  });


/**
 * Method: generateMailObj
 * Purpose : Function for mapping object as per pdf or mail
 * @param {Array} entitySelectionParams
 * @param {Object} fieldParams
 * @param {Object} conditionsAndSchema
 * @param {Object} calendarThreshold
 * @param {Object} texatmatchrecord
 * @param {Object} thresholdType
 * @param {Callback Function} callback
 */
function generateMailObj(entitySelectionParams, fieldParams, schemas, calendarThreshold, texatmatchrecord, thresholdType, _eachThresholdLog, callback) {
  var conditionsAndSchema = {
      schemas: schemas
  };
  
  var mailObj = [];
  var entitySelectionOrAllEntries = entitySelectionParams;
  var comboOrUniqueID;
  var measurementUnit = '';
  var power = '';
  var currency = '';  

  eachThresholdLog = _eachThresholdLog;


  var foundRecords = _.some(entitySelectionOrAllEntries, function(item){return item.length});
  if (!foundRecords) {
    //START:: PQT-2485 by Yamuna
    var emptyThresoldNotifyFunctions = [];
      calendarThreshold.noRecordsFound = true;
      emptyThresoldNotifyFunctions.push(thresoldNotify(calendarThreshold));
    q.all(emptyThresoldNotifyFunctions).then(function (response) {
      eachThresholdLog.log("noRecordsFoundEntries notify completed", { 'extraInfo': entitySelectionOrAllEntries });
      callback();
    });
    return;
  } else {
    _.forEach(entitySelectionOrAllEntries, function (entitySelectionParam, parentIndex) {
      mailObj[parentIndex] = [];
      _.forEach(entitySelectionParam, function (entry, childIndex) {
        mailObj[parentIndex][childIndex] = [];
        _.forEach(fieldParams[calendarThreshold.schema[0].title], function (field) {
          if (field) {
            if (field.parentId) {
              comboOrUniqueID = field['s#'] + '-' + field.parentId;
            } else {
              comboOrUniqueID = field['s#'];
            } 
           
            if (thresholdType == 'entity') {
               //PQT-1431 Hiren
            if(field.title === 'recordStatus'){
              comboOrUniqueID = 'isActive';
            }
              if (entry && entry.entityRecord && entry.entityRecord.hasOwnProperty(comboOrUniqueID)) {
                if (comboOrUniqueID.indexOf('-') === -1) {
                  if (field.type && field.type.format && field.type.format.title == 'Date') {
                    mailObj[parentIndex][childIndex].push({
                      title: field['title'],
                      value: moment(new Date(entry.entityRecord[comboOrUniqueID])).utc().tz(clientTZ).format('MM/DD/YYYY')
                    });
                  } else if (field.type && field.type.format && field.type.format.title == 'Time') {
                    mailObj[parentIndex][childIndex].push({
                      title: field['title'],
                      value: moment(new Date(entry.entityRecord[comboOrUniqueID])).utc().tz(clientTZ).format('HH:mm:ss')
                    });
                  } else if (field.type && field.type.format && field.type.title == 'File Attachment') {
                    var value = (entry.entityRecord[comboOrUniqueID] || '')[0].name; // PQT-5559 issue 1
                    value = entriesUtility.getFileName(entry.entityRecord[comboOrUniqueID], value);
 
                    mailObj[parentIndex][childIndex].push({
                      title: field['title'],
                      value: value // (entry.entityRecord[comboOrUniqueID] || '').toString().split('-')[1]
                    });
                    
                    //else if for PQT-1431 Hiren
                  } else if(field.title === 'recordStatus'){
                    mailObj[parentIndex][childIndex].push({
                      title: 'Status',
                      value: entry.entityRecord[comboOrUniqueID] ? 'Active' : 'Inactive'
                    });
                  } else {
                    measurementUnit = '';
                    power = '';
                    currency = '';
                    if (field.type && field.type.format && field.type.format.metadata) {
                      if (field.type.format.metadata.MeasurementUnit) {
                        measurementUnit = ' (' + field.type.format.metadata.MeasurementUnit + ')';
                      }
                      if (field.type.format.metadata.power) {
                        power = ' (10^' + field.type.format.metadata.power.toString() + ')';
                      }
                    }
                    if (field.type && field.type.format && field.type.format.title == 'Currency') {
                      currency = '$';
                    }
                    mailObj[parentIndex][childIndex].push({
                      title: field['title'],
                      value: currency + '' + entry.entityRecord[comboOrUniqueID] + '' + power + measurementUnit
                    });
                  }
                } else {
                  mailObj[parentIndex][childIndex].push({
                    title: field['title'],
                    value: entry.entityRecord[comboOrUniqueID][field.parentId]
                  });
                }
              } else {
                mailObj[parentIndex][childIndex].push({
                  title: field['title'],
                  value: ''
                });
              }
            } else {
              //PQT-1431 Hiren
            if(field.title === 'recordStatus'){
              comboOrUniqueID = 'recordStatus';
            }
              if (entry && entry.entries && entry.entries.hasOwnProperty(comboOrUniqueID)) {
                if (comboOrUniqueID.indexOf('-') === -1) {
                  if (field.type && field.type.format && field.type.format.title == 'Date') {
                    mailObj[parentIndex][childIndex].push({
                      title: field['title'],
                      value: moment(new Date(entry.entries[comboOrUniqueID].attributes)).utc().tz(clientTZ).format('MM/DD/YYYY')
                    });
                    //else if for PQT-1431 Hiren
                  } else if(field.title === 'recordStatus'){
                    mailObj[parentIndex][childIndex].push({
                      title: 'Record Status',
                      value: entry.entries[comboOrUniqueID]
                    });
                  } else {
                    mailObj[parentIndex][childIndex].push({
                      title: field['title'],
                      value: entry.entries[comboOrUniqueID].attributes
                    });
                  }
                } else {
                  if (entry.entries[comboOrUniqueID] && entry.entries[comboOrUniqueID][field.parentId]) {
                    if (field.type && field.type.format && field.type.format.title == 'Date') {
                      mailObj[parentIndex][childIndex].push({
                        title: field['title'],
                        value: moment(new Date(entry.entries[comboOrUniqueID][field.parentId])).utc().tz(clientTZ).format('MM/DD/YYYY')
                      });
                    } else if (field.type && field.type.format && field.type.format.title == 'Time') {
                      mailObj[parentIndex][childIndex].push({
                        title: field['title'],
                        value: moment(new Date(entry.entries[comboOrUniqueID][field.parentId])).utc().tz(clientTZ).format('HH:mm:ss')
                      });
                    } else if (field.type && field.type.title == 'File Attachment') {
                      // PQT-5559 issue 1
                      var value ='';
                      _.forEach((entry.entries[comboOrUniqueID] || '')[field.parentId], function(item){
                        value = value + entriesUtility.getFileName(entry.entries[comboOrUniqueID], item.name) + ',';
                      })
                      if(value){
                        value = value.slice(0, value.length-1)
                      }

                    	mailObj[parentIndex][childIndex].push({
                    		title: field['title'],
                    		value: value // (entry.entries[comboOrUniqueID][field.parentId] || '').toString().split('-')[1]
                    	});

                    } else {
                      measurementUnit = '';
                      power = '';
                      currency = '';
                      if (field.type && field.type.format && field.type.format.metadata) {
                        if (field.type.format.metadata.MeasurementUnit) {
                          measurementUnit = ' (' + field.type.format.metadata.MeasurementUnit + ')';
                        }
                        if (field.type.format.metadata.power) {
                          power = ' (10^' + field.type.format.metadata.power.toString() + ')';
                        }
                      }
                      if (field.type && field.type.format && field.type.format.title == 'Currency') {
                        currency = '$';
                      }
                      mailObj[parentIndex][childIndex].push({
                        title: field['title'],
                        value: currency + '' + entry.entries[comboOrUniqueID][field.parentId] + '' + power + measurementUnit
                      });
                    }
                  } else {
                    mailObj[parentIndex][childIndex].push({
                      title: field['title'],
                      value: ''
                    });
                  }
                }
              } else {
                mailObj[parentIndex][childIndex].push({
                  title: field['title'],
                  value: ''
                });
              }
            }
          }
        });

        if (thresholdType === 'entity' && entry.sites && entry.sites.enabled) {
          entry.sites.allicableSites.forEach(function (sites, index) {
            mailObj[parentIndex][childIndex].push({
              title: storeAllSiteNames[index].title,
              value: sites.map(function (site) {
                return site.title;
              }).join('|')
            });
          });
        }


      });
    });
  }

  if (mailObj.length) {

    if (!calendarThreshold) {
      debug('No scheduled entries');
      return;
    } else {
      debug(calendarThreshold + " scheduled entries");
    }

    // PDF
    // setTimeout(function () {
    //   generatePdf(mailObj, conditionsAndSchema, calendarThreshold, callback);
    // }, (Math.floor(Math.random() * (25000 - 2000 + 1)) + 2000));

    // start log  
    eachThresholdLog.log("Data Object created for Mail", { 'extraInfo': mailObj });
    //end: log

    // EXCEL
    generateExcelData(mailObj, conditionsAndSchema, calendarThreshold, callback);
  } else {
    callback();
  }
}


function generateExcelData(mailObj, conditionsAndSchema, calendarThreshold, callback) {
  var dataset = (mailObj || []).map(function (array, index) {
    return {
      name: conditionsAndSchema.schemas[index],
      specification: (array[0] || []).reduce(function (obj, value) {
        obj[value.title] = {
          displayName: value.title,
          headerStyle: {},
          width: 150
        };
        return obj;
      }, {}),
      data: (array || []).map(function (item) {
        return (item || []).reduce(function (obj, item) {
          obj[item.title] = item.value;
          return obj;
        }, {});
      })
    };
  });

  var tempFileName = (new Date()).getTime() + '-' + (calendarThreshold.threshold['title'] || '').replace(/[^a-zA-Z0-9 ]/g, "");
  // start log  
  eachThresholdLog.log("File generated.", { 'extraInfo': tempFileName });
  //end: log
  calendarThreshold.tempFileName = tempFileName;

  var hasData = dataset.some(function(item){
    return item.data.length;
  });

  if(!hasData){
    afterCreatingFile(calendarThreshold, null, callback);
  } else {
    generateExcel(tempFileName, dataset, function (error, result) {
      afterCreatingFile(calendarThreshold, result.fileName, callback);
    });
  }
}


function afterCreatingFile(calendarThreshold, tempFileName, callback) {
  mapUserIdWithUser(calendarThreshold, function (e, calendarThreshold) {
    nofity(calendarThreshold, tempFileName, function (e, d) {
      log.log(`Threshold ${calendarThreshold.title} is successfully sent`);
      if (e) {
        log.log(`error in notification for threshold : ${calendarThreshold.title}`);
        return;
      }

      // start log  
      eachThresholdLog.log("Mail has been Sent successfully.", { 'extraInfo': calendarThreshold.title });
      //end: log

      d.notified = true;
      d.scheduledBy = d.scheduledBy._id.toString();
      db.calendarThresholds.save(d);
      // FOR REMOVEING FILE AFTER SENDING TO MAIL 
      // try {
      //   fs.unlinkSync(unlinkFilePath);
      // } catch (error) {
      //   log.log(`Error on deleting file ${unlinkFilePath}`);
      //   log.log(error);
      // }
      callback();
    });
  });
}


/**
* Method: mapUserIdWithUser
* Purpose : Function to map createdBy, modifiedBy and scheduledBy with user name
* @param {Object/Array} data
* @param {Callback Function} cb
*/
function mapUserIdWithUser(data, cb) {
  var returnOne = !(data instanceof Array);
  if (returnOne) {
    data = [data];
  }
  var ids = _
    .chain(data)
    .map(function (record) {
      var ret = [];

      if (record.createdBy) {
        ret.push(db.ObjectID(record.createdBy));
      }
      if (record.modifiedBy) {
        ret.push(db.ObjectID(record.modifiedBy));
      }
      if (record.scheduledBy) {
        ret.push(db.ObjectID(record.scheduledBy));
      }

      if (record.path === 'modifiedBy' || record.path === 'createdBy' || record.path === 'scheduledBy') {
        debug(record);
        if (record.old) {
          ret.push(db.ObjectID(record.old));
        }
        if (record.new) {
          ret.push(db.ObjectID(record.new));
        }
      }
      return ret;
    })
    .filter(function (record) {
      return record.length;
    }).reduce(function (a, n) {
      return _.union(a, n);
    }, [])
    .value();

  db
    .users
    .find({
      '_id': {
        $in: ids
      }
    }, {
        'firstname': 1,
        'lastname': 1,
        'username': 1
      })
    .toArray(function (e, d) {
      d = _.reduce(d, function (acc, user) {
        acc[user._id] = user;
        return acc;
      }, {});

      _.forEach(data, function (record) {

        if (record.createdBy) {
          record.createdBy = d[record.createdBy.toString()];
        }
        if (record.modifiedBy) {
          record.modifiedBy = d[record.modifiedBy.toString()];
        }
        if (record.scheduledBy) {
          record.scheduledBy = d[record.scheduledBy.toString()];
        }


        if (record.path === 'modifiedBy' || record.path === 'createdBy' || record.path === 'scheduledBy') {
          debug(record);
          if (record.old) {
            record.old = d[record.old];
            if (record.old) {
              record.old = record.old.firstname + ' ' + record.old.lastname
            } else {
              record.old = 'Anonymous'
            }
          }
          if (record.new) {
            record.new = d[record.new];
            if (record.new) {
              record.new = record.new.firstname + ' ' + record.new.lastname
            } else {
              record.new = 'Anonymous'
            }
          }
        }
      });

      cb(null, returnOne ? data[0] : data);
    });
}

/**
* Method: nofity
* Purpose : Function for sending threshold mail
* @param {Object} entity
* @param {Callback Function} cb
*/
function nofity(entity, tempFileName, cb) {
  var schemaTitles = '';
  _.forEach(entity.schema, function (schema, index) {
    schemaTitles += index == 0 ? schema.title : ', ' + schema.title;
  });

  var appliedConditions = '';

  // ================ QC3-11396 + QC3-11402: mahammad ================

  rConditionStringify(entity.thresholdCondition);

  function rConditionStringify(conditions) {
    (conditions || []).forEach(function (c) {
      if (c.conditions) {
        if (c.bool && appliedConditions) {
          appliedConditions += ' ' + c.bool + ' ';
        }
        appliedConditions += '(';
        rConditionStringify(c.conditions);
        appliedConditions += ')';
      } else {
        if (c.leftOperand && c.leftOperand.type && c.leftOperand.type.format && c.leftOperand.type.format.title == 'Date' && c.dateSelection) {
          if (c.bool && appliedConditions) {
            appliedConditions += ' ' + c.bool + ' ';
          }
          appliedConditions += (c.leftOperand.title + ' ' + c.comparison + ' ' + (c.dateSelection === 'Specific Date' ? moment(c.rightOperand).format('MM/DD/YYYY') : c.dateSelection));
        } else {
          if (c && c.leftOperand) {
            if (c.bool && appliedConditions) {
              appliedConditions += ' ' + c.bool + ' ';
            }
            appliedConditions += (c.leftOperand.title + ' ' + c.comparison + ' ' + c.rightOperand);
          }
        }
      }
    })
  }

  // ================ QC3-11396 + QC3-11402: mahammad ================

  debug('Sending notification to ' + entity.email);
  var thresholdType = '';
  if (!entity.thresholdType || entity.thresholdType == 'app') {
    thresholdType = 'appThreshold';
  } else {
    thresholdType = 'entityThreshold';
  }
  _.merge(entity, settings);

  var obj = {
    'schemaTitles': schemaTitles,
    [thresholdType]: true,
    'appliedConditions': appliedConditions,
   
  };

  if(tempFileName){
    obj['fileInfo'] = [{
      path: config.staticpath + tempFileName
    }];
  }

  _.merge(entity, obj);
  log.log('inside file: threshold.js - function: notify - status: sending mail');

  function rSendMail_retrySafe(retryCount) {
    setTimeout(function () {
      mailer.send('threshold-notify-mail-content', entity, entity.email, function sendMailCallback(err, value) {
        if (err) {
          debug.err('Failed sending mail - review-scheduler-notify');
          debug.err(err);
          if (retryCount < 5) {
            retryCount++;
            rSendMail_retrySafe(retryCount);
          } else {
            cb(err, entity);
          }
        } else {
          // start log  
           eachThresholdLog.log("Email has been sent successfully.", { 'extraInfo': entity.email });
         //end: log
          entity.fileInfo = [];
          delete entity.fileInfo;
          debug('Mail send successfully to: ' + entity.email.underline);
          addToThresholdNotification(entity, tempFileName)
          cb(err, entity);
        }
      })
    }, (Math.floor(Math.random() * (25000 - 2000 + 1)) + 2000));
  }

  rSendMail_retrySafe(0);
}

/**
* Method: addToThresholdNotification
* Purpose : Function to add notification after sending threshold
* @param {Object} entry
*/
function addToThresholdNotification(entry, tempFileName) {
  var d = {
    thresholdId: (entry.thresholdId).toString(),
    title: entry.title,
    email: entry.email,
    logdate: new Date(),
    fileName: tempFileName
  };
  db.thresholdsNotification.save(d);
}


// PQT-2485 by Yamuna
function thresoldNotify(entity, tempFileName) {
  var deffer = q.defer();
  if (!entity) {
    deffer.reject(entity);
    return;
  }
  setTimeout(function () {
    
    //ChangeOnline
    log.log("sending notification to - "+entity.email);
    nofity(entity, tempFileName, function (e, d) {
      log.log('sent');
      if (e) {
        return;
      }
      var calendarDate = new Date(d.date).setHours(0,0,0,0);
      d.notified = true;

      //ChangeOnline
      if(d.scheduledBy){
        if(d.scheduledBy._id){
          d.scheduledBy = d.scheduledBy._id.toString();
        } else {
          d.scheduledBy = d.scheduledBy
        }
      }
      db.calendarThresholds.save(d);

      //OnlineChange
      log.log('-0-0-0-0-0-0-0-0-0 -   No matching mail sent    -0-0-0-0-0-0-0-0-0 -')
      log.log("mail sent for - "+d.title+"")      
      log.log("mail sent for - "+JSON.stringify(d)+"")
      log.log('-0-0-0-0-0-0-0-0-0 -   No matching mail sent    -0-0-0-0-0-0-0-0-0 -')

      deffer.resolve('success')
    });
  }, (Math.floor(Math.random() * (25000 - 2000 + 1)) + 2000));

  return deffer.promise;
}


module.exports = {
  generateMailObj: Promise.promisify(generateMailObj)
}