var conditions = db.getCollection('thresholds').findOne(ObjectId("5c6fe832058f6e414f2584aa")).conditions

conditions.forEach(function(data){
    var lOp = data.leftOperand 
    lOp.oType = "leftOperand";
    db.getCollection('unittestdata').insert(lOp);
})


db.getCollection('unittestdata').update({_id: ObjectId("5c7e8f0da1bc78df62909be6")},{$set: {format: "None"}})
db.getCollection('unittestdata').update({_id: ObjectId("5c7e8f0da1bc78df62909be7")},{$set: {format: "Number"}})
db.getCollection('unittestdata').update({_id: ObjectId("5c7e8f0da1bc78df62909be8")},{$set: {format: "Exponential"}})
db.getCollection('unittestdata').update({_id: ObjectId("5c7e8f0da1bc78df62909be9")},{$set: {format: "Percentage"}})
db.getCollection('unittestdata').update({_id: ObjectId("5c7e8f0da1bc78df62909bea")},{$set: {format: "Currency"}})
db.getCollection('unittestdata').update({_id: ObjectId("5c7e8f0da1bc78df62909beb")},{$set: {format: "Scientific"}})
db.getCollection('unittestdata').update({_id: ObjectId("5c7e8f0da1bc78df62909bec")},{$set: {format: "Date"}})
db.getCollection('unittestdata').update({_id: ObjectId("5c7e8f0da1bc78df62909bed")},{$set: {format: "None"}})
db.getCollection('unittestdata').update({_id: ObjectId("5c7e8f0da1bc78df62909bee")},{$set: {format: "None"}})
db.getCollection('unittestdata').update({_id: ObjectId("5c7e8f0da1bc78df62909bef")},{$set: {format: "None"}})





// NO: #125

db.getCollection('unittestdata').find({oType: 'leftOperand-viewType'})
db.getCollection('thresholds').find()
db.getCollection('thresholds').find().forEach(function(_threshold){
    
    function conditionW(conditions){
        conditions.forEach(function(_condition){
            if(_condition.conditions){
                conditionW(_condition.conditions);
            } else {
                addToUnitTestData(_condition.leftOperand);
            }
        })
    }
    conditionW(_threshold.conditions)
    
    function addToUnitTestData(cc){
        cc.oType= 'leftOperand-viewType';
        cc.schema = _threshold.schema;
        cc.thresholdType = _threshold.thresholdType;
        delete cc._id;
        db.getCollection('unittestdata').insert(cc);
    }
})


