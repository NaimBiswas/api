var assert = require('assert');
var _ = require('lodash');
var Promise = require('bluebird');
var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('../../logger').configure(__dirname, enableBunyan);

var config = require('../../../config/config.js');
var db = require('../../db');
var LeftOperand = require('../left_operand.threshold');
var SchemaResource = require('../schema_resource.threshold');
require('../../../lib/promise-me');

var resource = SchemaResource([
    {
        "_id": "5c6e5a36058f6e414f23f09e",
        "title": "h app"
    }
]);

describe('Threshold mapping racords properly', function () {

    this.timeout(100000)

    var collectionData;
    before('settinga', function (donea) {
        db.collection('5c7d122432b75b2a934bc19a').find({}).toArray(function (err, testData) {
            collectionData = testData;
            donea();
        });
    });

    var leftOperandData
    before('settingb', function (donea) {
        db.collection('unittestdata').find({ _id: db.ObjectID("5c7e6236a1bc78df62909be5") }, function (err, testData) {
            leftOperandData = testData;
            donea();
        })
    });

    var manyLeftOperands
    before('settingc', function (donea) {
        db.collection('unittestdata').find({ oType: 'leftOperand' }).toArray(function (err, testData) {
            manyLeftOperands = testData;
            donea();
        })
    });

    var manyLeftOperandsViewType
    before('settingc', function (donea) {
        db.collection('unittestdata').find({ oType: 'leftOperand-viewType' }).toArray(function (err, testData) {
            manyLeftOperandsViewType = testData;
            donea();
        })
    });

    it('--', function(){
        describe('is mapper perfect? ', function(){

            _.forEach(collectionData, function (_record) {
                var d = resource.mapRecord(_record);
                if(_record._id.toString() == '5c7f9bf4693556055bf81944'){
                    it('record one' + _record._id, function () {
                        assert.equal(!!(_record && _record.recordStatus), true, JSON.stringify(_record));
                    })
                }
            });
        })
    })

    it('----', function () {

        describe('LeftOperand OK ?', function () {
            describe('formats are OK?', function () {
                // console.log('**************', manyLeftOperands)
                _.forEach(manyLeftOperandsViewType, function (_leftOperand) {
                    var leftOperand = LeftOperand(_leftOperand);
                    it(_leftOperand._id + " should be " + _leftOperand.type.format.title + ", and is " + leftOperand.getFormat(), function () {
                        assert.equal(leftOperand.getFormat(), _leftOperand.type.format.title);
                    })
                });
            });

            // Seed script NO: #125
            describe('viewType are OK?', function () {
                // console.log('**************', manyLeftOperandsViewType)
                _.forEach(manyLeftOperandsViewType, function (_leftOperand) {
                    var leftOperand = LeftOperand(_leftOperand);
                    it(_leftOperand._id + " should have view type " + leftOperand.getViewType(), function () {
                        assert.equal(!!leftOperand.getViewType(), true);
                    })
                });
            });

            //mhear
            describe('can get value ?', function () {
                // console.log('**************', manyLeftOperandsViewType)
                this.timeout(100000)
                var recs = []
                before(function(done){
                    var manyLeftOperandsViewTypeApp = manyLeftOperandsViewType.filter(function(mCale){
                        return mCale.thresholdType == "app"
                    });
                    console.log(manyLeftOperandsViewTypeApp.length);
                    // console.log(manyLeftOperandsViewType);
                    var g = manyLeftOperandsViewTypeApp.filter(function(leftOp, index){
                        return index>90 && index < 100;
                    })
                    // console.log(g);
                    Promise.each(g, function (_leftOperand) {
                        var leftOperand = LeftOperand(_leftOperand);
                        return Promise.each(_leftOperand.schema, function(_schema){
                            return db.collection(_schema._id).find().toArrayAsync().then(function (records) {
                                recs.push({
                                    leftOperand: leftOperand,
                                    records: records,
                                    schema: _schema._id
                                });
                            });
                        }) 
                    }).then(function(){done()});
                });

                
                it("--", function(){
                    describe('checking all data', function(){

                            recs.forEach(function(recc){
                                var leftOperand = recc.leftOperand;
                                if(recc.schema != '5c6be52d5c09ed5dd44a13e5'){
                                    recc.records.forEach(function (_record) {
                                        
                                        // console.log('---a',recc.leftOperand.getModel());
                                        it('checking record in '+recc.schema+' - '+ _record._id +' - has value ' + 's' /*leftOperand.getValue()*/ + " in attribute "+ leftOperand.info.title,function(){
                                            leftOperand.setModel(resource.mapRecord(_record));
                                            
                                            // if(/*_record._id.toString() == '5c8288f16586527d9a87a0ec'*/ Object.keys(leftOperand.getModel()).length>1){
                                                // console.log('---',recc.leftOperand.getModel());
                                                var cId = (leftOperand.info['s#'] || "") + (leftOperand.info['parentId'] ? '-'+leftOperand.info['parentId'] : "");
                                                console.log('--', _record._id,leftOperand.getModel()[cId]);
                                                console.log(leftOperand.getValue())
                                                assert.equal(!leftOperand.getModel()[cId] || !!leftOperand.getValue(), 
                                                    true, leftOperand.getModel()[cId]+ '-');
                                            // }
                                        })
                                    });
                                }
                            })
                    })
                })
            });
        });
    });
});

