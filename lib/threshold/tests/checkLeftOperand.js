var assert = require('assert');

var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('../../logger').configure(__dirname, enableBunyan);

var config = require('../../../config/config.js');
var db = require('../../db');
var left_operand = require('../left_operand.threshold');


describe('Basic Mocha String Test', function () {
 it('should return number of charachters in a string', function (done) {

        db.collection('unittestdata').find({},function(err, testData){
            // console.log(data);

            testData.forEach(function(testD) {
                left_operand(testD);
            });
            assert.equal("Hello".length, 4);
            done()
        })

        
    });
 it('should return first charachter of the string', function () {
        assert.equal("Hello".charAt(0), 'H');
    });
});