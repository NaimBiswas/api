var _ = require('lodash');
var Operand = require('./operand.threshold');
var thresholdConstants = require('./constants.threshold');
var moment = require('moment-timezone');


function RightOperand(_operand) {

    var fromOp;
    var toOp; 
    var dateSelection;
    function setFrom(_fromOp) {
        fromOp = _fromOp;
    }
    function setTo(_toOp) {
        toOp = _toOp;
    }
    function setDateSelection(_dateSelection) {
        dateSelection = _dateSelection;
    }

    function getFromValue() {
        return _getValue(fromOp);
    }
    function getToValue() {
        return _getValue(toOp);
    }
    function getValue() {
        return _getValue(_operand);
    }
    function _getValue(op){
        switch (_self.getViewType()) {
            case thresholdConstants.viewType.ATTRIBUTE:
            case thresholdConstants.viewType.QUESTION:{
                if (_self.getFormat() == 'None' || _self.getFormat() == 'Date') {
                    return ''+op;
                } else {
                    return op;
                }
            }
            case thresholdConstants.viewType.SITE:{
                return op.replace(/'/g, '')
            }
        }
    }

    function getRange() {
        var today = moment().startOf('day');

        switch (dateSelection) {
            case 'This Week':{
                return {
                    start: moment().startOf('week'),
                    end: moment().endOf('week')
                };
            }
            case 'This Month':{
                return {
                    start: moment().startOf('month'),
                    end: moment().endOf('month')
                };
            }
            case 'This Year':{
                return {
                    start: moment().startOf('year'),
                    end: moment().endOf('year')
                };
            }
            case 'Previous Week':{
                return {
                    start: moment().day(-7),
                    end: moment().day(-1)
                };
            }
            case 'Previous Month':{
                var thisMonth = today.month();
                today.add(-1, 'month');
                if (today.month() != thisMonth - 1 && (today.month() != 11 || (thisMonth == 11 && today.date() == 1))) {
                    today.date(0);
                }
                return {
                    start: today.startOf('month'),
                    end: _.cloneDeep(today).endOf('month')
                }
            }                
            case 'Previous Year':{
                var thisYear = today.year();
                today.add(-1, 'year');
                if (today.year() != thisYear - 1 && (today.year() != 11 || (thisYear == 11 && today.date() == 1))) {
                    today.date(0);
                }
                return {
                    start: today.startOf('year'),
                    end: _.cloneDeep(today).endOf('year')
                }
            }
            case 'Specific Date':{
                return {
                    start: new Date(getValue()),
                    end: new Date(getValue())
                }
            }
            case 'Today':{
                return {
                    start: moment().add(-1,'days').toDate(),
                    end: moment().add(-1,'days').toDate()
                }
            }
            default:{
                return {
                    start: moment().toDate(),
                    end: moment().toDate()
                };
            }
        }
    }
    
    var _self = Object.assign(Operand(_operand), {
        
        setFrom: setFrom,
        setTo: setTo,
        setDateSelection: setDateSelection,

        getFromValue: getFromValue,
        getToValue: getToValue,
        getValue: getValue,
        getRange: getRange
    });

    return _self;
}

module.exports = RightOperand;