var _ = require('lodash');

function Operand(_operand) {

    function setFormat(_format) {
        _self.format = _format;
    }
    function getFormat() {
        return _self.format;
    }

    function setViewType(_viewType){
        _self.vieType = _viewType;
    }
    function getViewType(){
        return _self.vieType
    }

    function getPower(){
        if(_.get(_operand,'type.format.metadata.power')){
            return _operand['type']['format']['metadata']['power']
        } else {
            return 0;
        }
    }

    var _self = {
        setViewType: setViewType,
        getViewType: getViewType,
        info: _operand,
        setFormat: setFormat,
        getFormat: getFormat,
        getPower: getPower
    }

    return _self;
}

module.exports = Operand