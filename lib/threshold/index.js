var Promise = require('bluebird');

var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('../logger').configure(__dirname, enableBunyan);

var config = require('../../config/config.js');
var CronJob = require('cron').CronJob;
var log = require('../../logger');
var _ = require('lodash');
var db = Promise.promisifyAll(require('../db')); //require('./db');
var dateServies = require('../dateServies');
var fromZone = dateServies.fromZone;
var configZone = fromZone(config.currentTimeZone);
var logService = require('../logService');

var generateMail = require('./generatemail.js');
var Calendar = require('./calendar.threshold');
var moment = require('moment-timezone');
var logHelper = require('./log_helper.threshold');

var jobDaily = new CronJob('5 0 * * *', function () {
    initAlerts('daily');
}, function () { },
    true
);

var jobHourly = new CronJob('0 */1 * * *', function () {
    initAlerts('hourly');
}, function () { },
    true
);

// initAlerts('daily');

function initAlerts(cronType) {

    var logger = logService._module('threshold', cronType + " " + (new Date()).toISOString());
    logger.log("starting cron in initAlerts");

    Calendar.getCurrentAll(cronType)
        .then(Calendar.promiseEach(function(iCalendar,idx){
            
            return Promise.props({
                    columnHeaders: iCalendar.getResource().getColumnHeaders(),
                    records: iCalendar.getRecords()
                })
                .then(function (mailData) {
                    
                   iCalendar.updateLastRunTime();

                    // to generate excel/pdf file and email
                    if (iCalendar.hasMatchingCount(mailData.records) || 
                        iCalendar.shouldSendFullReport() || 
                        iCalendar.shouldSendWithEvenZERORecords()) {
                        // comment only for QA
                        logger.log('---Ready for email ---');
                        //
                        return generateMail.generateMailObj(mailData.records, 
                                                            mailData.columnHeaders, 
                                                            iCalendar.getResource().getSchemaTitles() , 
                                                            _.cloneDeep(iCalendar.data), 
                                                            iCalendar.getThreshold().reportconfiguration.exatmatchrecord, 
                                                            iCalendar.data.thresholdType, 
                                                            iCalendar.logger);
                    } else {
                        // comment only for QA
                        logger.log('---Does not Satisfy condition & Ready for next calender threshold ---');
                        //
                        return 'need to change';
                    }
                }).catch(function(err){
                    console.log("Error found----"+err.stack);                      
                });

            
        
            }, logger)).catch(function(err){
                logger.log("Error in getCurrentAll()", {
                    stack: err.stack,
                    err: err
                });
            });

}






