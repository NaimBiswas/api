
var _ = require('lodash');
var moment = require('moment-timezone');

module.exports =  {

    'greater than': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return (leftOperand.getValue() || leftOperand.getValue() == 0) && 
                (leftOperand.getValue()*(Math.pow(10,leftOperand.getPower())) > rightOperand.getValue());
    },
    'greater than or equal to': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return (leftOperand.getValue() || leftOperand.getValue() == 0) && 
                (leftOperand.getValue()*(Math.pow(10,leftOperand.getPower())) >= rightOperand.getValue());
    },
    'less than': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return (leftOperand.getValue() || leftOperand.getValue() == 0) && 
                (leftOperand.getValue()*(Math.pow(10,leftOperand.getPower())) < rightOperand.getValue());
    },
    'less than or equal to': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return (leftOperand.getValue() || leftOperand.getValue() == 0) && 
                (leftOperand.getValue()*(Math.pow(10,leftOperand.getPower())) <= rightOperand.getValue());
    },
    'equal to': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;


        return (leftOperand.getValue() || leftOperand.getValue() == 0) && 
                (_.isArray(leftOperand.getValue()) ? 
                (_.includes(leftOperand.getValue(), rightOperand.getValue()))
                :(leftOperand.getValue()*(Math.pow(10,leftOperand.getPower())) == rightOperand.getValue()));
    },
    'not equal to': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return (leftOperand.getValue() || leftOperand.getValue() == 0) && 
            (_.isArray(leftOperand.getValue()) ? 
            !(_.includes(leftOperand.getValue(), rightOperand.getValue()))
            :(leftOperand.getValue()*(Math.pow(10,leftOperand.getPower())) != rightOperand.getValue()));
    },
     'is empty':  function (_condition) {  
         
        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;
   
        return !leftOperand.getValue() || 
                    (_.isArray(leftOperand.getValue()) ? 
                    !(leftOperand.getValue().length)
                    :(leftOperand.getValue() == undefined)) || 
                (leftOperand.getValue() == "(Not yet evaluated)");
    },
    'is not empty':  function (_condition) { 
        
        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;
           
        return (leftOperand.getValue() && 
                (_.isArray(leftOperand.getValue()) ? 
                    (leftOperand.getValue().length)
                    :(leftOperand.getValue() != undefined))) &&
                (leftOperand.getValue() != "(Not yet evaluated)");
    }
}

