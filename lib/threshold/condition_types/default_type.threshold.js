var _ = require('lodash');
var moment = require('moment-timezone');

module.exports =  {

   'equal to': function(_condition){
    
        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        if(_.isArray(leftOperand.getValue())){
            return _.includes(leftOperand.getValue(), rightOperand.getValue());
        } else {
            return (leftOperand.getValue() == rightOperand.getValue())
        }
    },
    'not equal to': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;


        if(_.isArray(leftOperand.getValue())) {
            return !(_.includes(leftOperand.getValue(), rightOperand.getValue()))
        } else {
            return (leftOperand.getValue() != rightOperand.getValue());
        }
    },
    'contains': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return leftOperand.getValue() && leftOperand.getValue().indexOf(rightOperand.getValue()) !== -1;
    },
    'does not contain': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return !leftOperand.getValue() || leftOperand.getValue().indexOf(rightOperand.getValue()) === -1;
    },
    'starts with': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return leftOperand.getValue() && leftOperand.getValue().startsWith(rightOperand.getValue());
    },
    'ends with': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return leftOperand.getValue() && leftOperand.getValue().endsWith(rightOperand.getValue());
    },
    // PQT-1431 record status
    'is':  function (_condition) {

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return leftOperand.getValue() == rightOperand.getValue();     
    },
    'is empty':  function (_condition) {     

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return !leftOperand.getValue() || 
                (_.isArray(leftOperand.getValue())? 
                    !(leftOperand.getValue().length) :
                    leftOperand.getValue() == undefined) || 
                (leftOperand.getValue() && leftOperand.getValue() == "(Not yet evaluated)");
    },
    'is not empty':  function (_condition) {  
        
        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return leftOperand.getValue() &&
                 (_.isArray(leftOperand.getValue())?
                    (leftOperand.getValue().length): 
                    leftOperand.getValue() != undefined) &&
                 (leftOperand.getValue() != "(Not yet evaluated)");
    }
}

