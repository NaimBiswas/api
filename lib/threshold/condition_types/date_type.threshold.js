var _ = require('lodash');
var moment = require('moment-timezone');


module.exports =  {

    'greater than': function(_condition){
      
        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return leftOperand.getValue() && 
                ((moment(leftOperand.getValue()).startOf('day'))
                .isAfter(moment(rightOperand.getRange().end).startOf('day')));
    },
    'greater than or equal to': function(_condition){
        
        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return leftOperand.getValue() && 
                ((moment(leftOperand.getValue()).startOf('day'))
                .isSameOrAfter(moment(rightOperand.getRange().end).startOf('day')));
    },
    'less than': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return leftOperand.getValue() && 
                ((moment(leftOperand.getValue()).startOf('day'))
                .isBefore(moment(rightOperand.getRange().start).startOf('day')));
    },
    'less than or equal to': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return leftOperand.getValue() && 
                ((moment(leftOperand.getValue()).startOf('day'))
                .isSameOrBefore(moment(rightOperand.getRange().start).startOf('day')));
    },
    'equal to': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;
        
        return leftOperand.getValue() && 
                (((moment(leftOperand.getValue()).startOf('day')).isSameOrAfter(moment(rightOperand.getRange().start).startOf('day'))) &&
                ((moment(leftOperand.getValue()).startOf('day')).isSameOrBefore(moment(rightOperand.getRange().end).startOf('day'))));
    },
    'not equal to': function(_condition){
        
        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;
        

        return leftOperand.getValue() && 
            !(((moment(leftOperand.getValue()).startOf('day')).isSameOrAfter(moment(rightOperand.getRange().start).startOf('day'))) &&
            ((moment(leftOperand.getValue()).startOf('day')).isSameOrBefore(moment(rightOperand.getRange().end).startOf('day'))));
    },
    'between': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;
        
        return leftOperand.getValue() && 
                (((moment(leftOperand.getValue()).startOf('day')).isSameOrAfter(moment(rightOperand.getFromValue()).startOf('day'))) &&
                ((moment(leftOperand.getValue()).startOf('day')).isSameOrBefore(moment(rightOperand.getToValue()).startOf('day'))));
    },
    'not between': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;
        
        return leftOperand.getValue() && 
                !(((moment(leftOperand.getValue()).startOf('day')).isSameOrAfter(moment(rightOperand.getFromValue()).startOf('day'))) &&
                ((moment(leftOperand.getValue()).startOf('day')).isSameOrBefore(moment(rightOperand.getToValue()).startOf('day'))));
    },    
    'is empty':  function (_condition) {  
        
        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return !leftOperand.getValue() || 
                (_.isArray(leftOperand.getValue()) ? 
                    !(leftOperand.getValue().length)
                    :(leftOperand.getValue() == undefined)) || 
                (leftOperand.getValue() == "(Not yet evaluated)");
    },
    'is not empty':  function (_condition) {
        
        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return (leftOperand.getValue() && 
                (_.isArray(leftOperand.getValue()) ? 
                    (leftOperand.getValue().length)
                    :(leftOperand.getValue() != undefined))) && 
                (leftOperand.getValue() != "(Not yet evaluated)");
    }
}


