var _ = require('lodash');
var moment = require('moment-timezone');

module.exports =  {

    'greater than': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return (leftOperand.getValue() || leftOperand.getValue() == 0) && (leftOperand.getValue() > rightOperand.getValue());
    },
    'greater than or equal to': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return (leftOperand.getValue() || leftOperand.getValue() == 0) && leftOperand.getValue() >= rightOperand.getValue();
    },
    'less than': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return (leftOperand.getValue() || leftOperand.getValue() == 0) && leftOperand.getValue() < rightOperand.getValue();
    },
    'less than or equal to': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return (leftOperand.getValue() || leftOperand.getValue() == 0) && leftOperand.getValue() <= rightOperand.getValue();
    },
    'equal to': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return (leftOperand.getValue() || leftOperand.getValue() == 0) && 
            (_.isArray(leftOperand.getValue()) ? 
            (_.includes(leftOperand.getValue(), rightOperand.getValue()))
            :(leftOperand.getValue() == rightOperand.getValue()));
    },
    'not equal to': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return (leftOperand.getValue() || leftOperand.getValue() == 0) &&
                (_.isArray(leftOperand.getValue()) ? 
                !(_.includes(leftOperand.getValue(), rightOperand.getValue()))
                :(leftOperand.getValue() != rightOperand.getValue()));
    },
    'contains': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return leftOperand.getValue() && (leftOperand.getValue()).indexOf(rightOperand.getValue()) !== -1;
    },
    'does not contain': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return !leftOperand.getValue() || (leftOperand.getValue()).indexOf(rightOperand.getValue()) === -1;
    },
    'starts with': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return leftOperand.getValue() && (leftOperand.getValue()).startsWith(rightOperand.getValue());
    },
    'ends with': function(_condition){

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return leftOperand.getValue() && (leftOperand.getValue()).endsWith(rightOperand.getValue());
    },
    // PQT-1431 record status
    'is':  function (_condition) { 


        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;

        return leftOperand.getValue() == rightOperand.getValue();        
      },    
    'is empty':  function (_condition) {  

        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;
               
        return !leftOperand.getValue() || 
                    (_.isArray(leftOperand.getValue()) ? 
                    !(leftOperand.getValue().length)
                    :(leftOperand.getValue() == undefined)) || 
                (leftOperand.getValue() == "(Not yet evaluated)");
    },
    'is not empty':  function (_condition) {   
        
        var leftOperand = _condition.leftOperand;
        var rightOperand = _condition.rightOperand;
                     
        return (leftOperand.getValue() && 
                    (_.isArray(leftOperand.getValue()) ? 
                    (leftOperand.getValue().length)
                    :(leftOperand.getValue() != undefined))) && 
                (leftOperand.getValue() != "(Not yet evaluated)");
    }
}

