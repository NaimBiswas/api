
var Formater = require('./formater.threshold');
var entriesUtility = require('../utilities/entriesUtiliese.js');

function parseEntry(entry, comboOrUniqueID, field, resource) {
  
  var record = entry


  var mapedRecord = resource.mapRecord(record)
  if(mapedRecord[comboOrUniqueID]){
    var iFormater = Formater(field, mapedRecord[comboOrUniqueID])
    var fomatedTitle = iFormater.titleRecordStatus() || iFormater.titleDefault();
    var formatedValue =  iFormater.valueDate() || iFormater.valueTime() || iFormater.valueFileAttachment() || iFormater.valueMeta();
    
    return {
      title: fomatedTitle,
      value: formatedValue
    }
  } else {
    var iFormater = Formater(field, mapedRecord[comboOrUniqueID])
    var fomatedTitle = iFormater.titleRecordStatus() || iFormater.titleDefault();
    return {
      title: fomatedTitle,
      value: ''
    } 
  }

  

  //PQT-1431 Hiren
     if(field.title === 'recordStatus'){
        comboOrUniqueID = 'recordStatus';
      }
        if (entry && entry.entries && entry.entries.hasOwnProperty(comboOrUniqueID)) {
          if (comboOrUniqueID.indexOf('-') === -1) {
            if (field.type && field.type.format && field.type.format.title == 'Date') {
              return {
                title: field['title'],
                value: moment(new Date(entry.entries[comboOrUniqueID].attributes)).utc().tz(clientTZ).format('MM/DD/YYYY')
              }
            } else if(field.title === 'recordStatus'){
              return {
                title: 'Record Status',
                value: entry.entries[comboOrUniqueID]
              };
            } else {
              return {
                title: field['title'],
                value: entry.entries[comboOrUniqueID].attributes
              };
            }
          } else {
            if (entry.entries[comboOrUniqueID] && entry.entries[comboOrUniqueID][field.parentId]) {
              if (field.type && field.type.format && field.type.format.title == 'Date') {
                return {
                  title: field['title'],
                  value: moment(new Date(entry.entries[comboOrUniqueID][field.parentId])).utc().tz(clientTZ).format('MM/DD/YYYY')
                };
              } else if (field.type && field.type.format && field.type.format.title == 'Time') {
                return {
                  title: field['title'],
                  value: moment(new Date(entry.entries[comboOrUniqueID][field.parentId])).utc().tz(clientTZ).format('HH:mm:ss')
                };
              } else if (field.type && field.type.title == 'File Attachment') {
              
                  var value = (entry.entries[comboOrUniqueID][field.parentId] || '').toString().split('-')[1];
                  value = entriesUtility.getFileName(entry.entries[comboOrUniqueID][field.parentId], value);
  
                  return {
                     title: field['title'],
                    value: value
                  } 
              } else {
                measurementUnit = '';
                power = '';
                currency = '';
                if (field.type && field.type.format && field.type.format.metadata) {
                  if (field.type.format.metadata.MeasurementUnit) {
                    measurementUnit = ' (' + field.type.format.metadata.MeasurementUnit + ')';
                  }
                  if (field.type.format.metadata.power) {
                    power = ' (10^' + field.type.format.metadata.power.toString() + ')';
                  }
                }
                if (field.type && field.type.format && field.type.format.title == 'Currency') {
                  currency = '$';
                }
                return {
                  title: field['title'],
                  value: currency + '' + entry.entries[comboOrUniqueID][field.parentId] + '' + power + measurementUnit
                };
              }
            } else {
              return {
                title: field['title'],
                value: ''
              };
            }
          }
        } else {
          return {
            title: field['title'],
            value: ''
          };
        }
}