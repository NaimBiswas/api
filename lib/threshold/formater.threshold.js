
function FormaterThreshold(field, fieldValue) {


  var _self = {
    valueDate: valueDate,
    valueTime: valueTime,
    valueFileAttachment: valueFileAttachment,
    valueMeta: valueMeta,
    titleRecordStatus: titleRecordStatus,
    titleDefault: titleDefault
  }
  return _self;

  function titleRecordStatus() {
    if (field.title === 'recordStatus') {
      return 'Record Status'
    }
  }

  function titleDefault() {
    return field['title']
  }

  function valueDate() {
    if (_.get(field, 'type.format.title') == 'Date') {
      return moment(fieldValue)
        .utc().tz(clientTZ).format('MM/DD/YYYY')
    }
  }

  function valueTime() {
    if (_.get(field, 'type.format.title') == 'Time') {

      return moment(new Date(fieldValue))
        .utc().tz(clientTZ).format('HH:mm:ss');
    }
  }

  function valueFileAttachment(field, fieldValue) {
    if (_.get(field, 'type.title') == 'File Attachment') {
      fieldValue = _.isArray(fieldValue) ? fieldValue : [{ name: fieldValue }];
      var fileNames = _.map(fieldValue, function (item) {
        return (item.name || '').toString().split('-')[1];
      }).join(' | ');
      return fileNames;
    }
  }

  function valueMeta(field, fieldValue) {
    var measurementUnit = '';
    var power = '';
    var currency = '';
    if (_.get(field, 'type.format.metadata.MeasurementUnit')) {
      measurementUnit = ' (' + field.type.format.metadata.MeasurementUnit + ')';
    }
    if (_.get(field, 'type.format.metadata.power')) {
      power = ' (10^' + field.type.format.metadata.power.toString() + ')';
    }
    if (_.get(field, 'type.format.title') == 'Currency') {
      currency = '$';
    }
    return currency + '' + fieldValue + '' + power + measurementUnit;
  }
}

module.exports = FormaterThreshold;