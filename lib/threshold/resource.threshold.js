var Promise = require('bluebird');
var config = require('../../config/config.js');
var _ = require('lodash');
var db = Promise.promisifyAll(require('../db'));
var debug = require('../logger').debug('lib:scheduler');

function Resource(schemas){

    // @public
    function getSchemaRecords(schema, filter) {
        return db
            .collection(db.ObjectID(schema._id).toString())
            .find(filter)
            .toArrayAsync()
            .then(function (entries) {
                if (entries && entries.length) {
                    return entries;
                } else {
                    return [];
                }
            });
    }
    // @public
    function getSchemas() {
        return schemas;
    }

    // @public
    function getSchemaTitles (){
        return _.map(getSchemas(),'title');
    }

    // @public
    function getColumnHeaders() {
        return _self.getSchemasDetailed()
            .then(function (allSchemas) {
                return _self.getFields(allSchemas)
            })
    }
    
    // @public - #promiseEach
    function eachSchema_Records(filter, cb){
        return Promise.each(_self.getSchemas(), function (schema, index) {

            return _self.getSchemaRecords(schema, filter).then(function(records){
                cb(schema, records, index)
            })
        })          
    }

    var _self = {
        getSchemaTitles: getSchemaTitles,
        getSchemas: getSchemas,
        getColumnHeaders: getColumnHeaders,
        eachSchema_Records: eachSchema_Records,
        getSchemaRecords: getSchemaRecords
    }

    return _self;

}


module.exports = Resource;
