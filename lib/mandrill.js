var Promise = require('bluebird');
var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('./logger').configure(__dirname, enableBunyan);
require('./promise-me');
var config = require('../config/config.js');
var CronJob = require('cron').CronJob;
var mailer = require('./mailer/mailer');
var settings = require('./settings');
var log = require('../logger');
var _ = require('lodash');
var db = Promise.promisifyAll(require('./db')); //require('./db');
var debug = require('./logger').debug('lib:scheduler');
var fs = require('fs');
var pdf = require('html-pdf');
var moment = require('moment');
var q = require('q');
var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill(config.MandrillAPIKEY);
var handlebars = require('handlebars');

var cronString = '0 */3 * * *';

var working = 0;
var job = new CronJob(cronString, function () {
        working = 0;
        initCronJob();
    }, function () {},
    true
);

//initCronJob();

function initCronJob() {
    getRecordsFromDB();
}

// get records from our database
function getRecordsFromDB() {

    var timeSpan = new Date();
    timeSpan.setHours(timeSpan.getHours() - 1);
    db
        .collection('mailLogs')
        .find({
            mendrillTryCount: {
                $lte: 3
            },
            notifiedAdmin: false,
            mendrillStatus: {
                $ne: 'sent'
            },
            mailSentDate: {
                $lte: timeSpan
            }
        })
        .toArray(function (error, response) {
            if (error) {
                log.log(error);
            } else {
                if (response && response.length > 0) {
                    var sendEmailAndUpdate = [];

                    // 1. filter data for emails which have not been send a single time
                    var firstEmails = [];
                    firstEmails = _.filter(response, {
                        'localStatus': 'Failed'
                    });
                    if (firstEmails.length > 0) {
                        _.forEach(firstEmails, function (item) {
                            item['processed'] = true;
                            // try to send email again                 
                            // update in database records covered in after email has been sent in error and success part
                            sendEmailAndUpdate.push(reSendEmail(item, "local", false));
                        });
                    }

                    // 2. filter data for which email try reached to maximum limit
                    // to notify admin
                    var adminEmails = [];
                    adminEmails = _.filter(response, {
                        'mendrillTryCount': 3
                    });
                    if (adminEmails.length > 0) {
                        _.forEach(adminEmails, function (item) {
                            item['processed'] = true;

                            // update database records with notified true
                            sendEmailAndUpdate.push(updateRecordsInDB(item._id.toString(), {
                                'notifiedAdmin': true
                            }, false))
                        });
                        // update admin about this by mail
                        var item = createObj(adminEmails);
                        sendEmailAndUpdate.push(reSendEmail(item, 'mandrill', true));
                    }

                    // 3. filter data to proceed further
                    var dataToProcess = _.filter(response, function (item) {
                        return (!item.processed && ((item.mendrillTryCount != 3 && item.mendrillStatus !== 'sent') || item.localStatus !== 'Failed'));
                    });

                    if (sendEmailAndUpdate.length > 0) {
                        q.all(sendEmailAndUpdate).then(function (result) {
                            mainProcess(dataToProcess);
                        });
                    } else {
                        mainProcess(dataToProcess);
                    }
                } else {
                    console.log("done");
                }
            }
        });
}

// create object to send email to admin when maximum try reached
function createObj(adminEmails) {
    var item = {};
    item['entriesObj'] = adminEmails;
    item['date'] = moment(moment.utc(new Date()).toDate()).format('MM/DD/YYYY HH:mm:ss')
    item['to'] = config.notifyErrorEmail;
    item['action'] = 'emailFailureAlert';
    return item;
}

// function to update status in database based on mandrill status sent
function mainProcess(dataToProcess) {

    if (dataToProcess && dataToProcess.length > 0) {
        Promise.each(dataToProcess, function (item, index) {
            return getRecordsFromMendrill(item).then(function (result) {
                // if ( item._id.toString() ==='5bc5d7f0895a973d15383fa4' || item._id.toString() === '5bc5ec76e7ddb44863c02e3e' || item._id.toString() === '5bc5ee6873bb094a1b50a314') {
                //     result[0].state = 'rejected';
                // }
                if (!result || !result.length) {
                    log.log('no mandrill record found');
                    return;
                } else {
                    var updateObj = {};

                    updateObj['mendrillStatus'] = result[0].state;
                    // when we get mendrill status other than sent, update in db and resend mail 
                    if (result[0].state !== 'sent') {
                        updateObj['mendrillTryCount'] = item['mendrillTryCount'] + 1;
                        return q.all([reSendEmail(item, 'mandrill', false), updateRecordsInDB(item._id.toString(), updateObj, true)]);
                    } else {
                        // when we get mendrill status sent then only update in local database
                        return q.all([updateRecordsInDB(item._id.toString(), updateObj, false)]);
                    }
                }
            }).catch(function (err) {
                log.log(err);
            });

        }).then(function () {
            console.log("done");
        });
    } else {
        console.log('done');
    }
}

// get records from mandrill database
function getRecordsFromMendrill(item) {
    var deferred = q.defer();

    var tags = [item._id.toString()];
    var limit = 10;

    mandrill_client.messages.search({
        "tags": tags,
        "limit": limit
    }, function (result) {
        deferred.resolve(result);
    }, function (e) {
        // Mandrill returns the error as an object with name and message keys
        console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
        // A mandrill error occurred: ServiceUnavailable - Service Temporarily Unavailable
        deferred.resolve(null);
    });
    return deferred.promise;
}

// update records in database
function updateRecordsInDB(id, updateObj, mailSent) {
    var deferred = q.defer();

    if (mailSent == true) {
        updateObj['mailSentDate'] = new Date();
    }
    updateObj['modifiedDate'] = new Date();

    db.collection('mailLogs')
        .updateOne({
                _id: db.ObjectID(id)
            }, // where condition
            {
                $set: updateObj
            } // object to update
            ,
            function (err, result) {
                deferred.resolve(result);
            });

    return deferred.promise;
}

// send email
function reSendEmail(item, type, alerToAdmin) {
    var deferred = q.defer();

    var emailDetail = {};
    emailDetail['text'] = item.subject;
    emailDetail = item;
    if (alerToAdmin === false) {
        if (item.attachments && item.attachments.length > 0) {
            _.forEach(item.attachments, function (file) {
                file.path = __dirname + '/../tmp/' + file.path;
            });
            emailDetail['fileInfo'] = item.attachments;
        }
    }

    mailer.send(emailDetail.action, emailDetail, emailDetail.to, function sendMailCallback(err, value) {
        if (err) {
            // to insett error for those email which are first time sent by mandrill
            if (type == 'local') {
                updateRecordsInDB(item._id.toString(), {
                    '$push': {
                        'localErrors': err
                    }
                }, true)
                // $inc: { "localTryCount": 1 }
            }

            debug.err('Failed sending mail - reSend Email');
            debug.err(err);
            deferred.resolve(err);
        } else {
            // to update database records which are first time sent by mandrill
            if (type && type == 'local') {
                updateRecordsInDB(item._id.toString(), {
                    localStatus: 'Sent'
                }, true);
            }

            log.log('Email has been sent to ' + item.to);
            debug('Mail send successfully to: ' + item.to);
            deferred.resolve(value);
        }
    }, true);

    return deferred.promise;
}
