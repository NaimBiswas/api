//by surjeet.b@productivet.com


/* some qiuck info...

1. To test scheduler set crone job
var job = new CronJob('* * * * * *', function() {
Runs every second
}

2. To run every day 12 AM !!
var job = new CronJob('0 0 0 * * *', function() {
Runs every day
at 12:00:00 AM.
}
*/


var colors = require('colors');
var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('./logger').configure(__dirname, enableBunyan);
var moment = require('moment-timezone');
var CronJob = require('cron').CronJob;
var mailer = require('./mailer');
var settings = require('./settings');
var log = require('../logger');
var config = require('../config/config.js');
var _ = require('lodash');
var mapUsers = require('./registration').mapUserIdWithUser;
var db = require('./db');
var debug = require('./logger').debug('lib:scheduler');
var handlebars = require('handlebars');

// PQT-1974 by Yamuna
handlebars.registerHelper('toJSON', function(obj) {
  return JSON.stringify(obj, null, 3);
});
handlebars.registerHelper('ifCondition', function (v1, operator, v2, options) {
  switch (operator) {
      case '==':
          return (v1 == v2) ? options.fn(this) : options.inverse(this);
        }
});

var currentFileName = __filename;
var logger = require('../logger');

var runHourlyJob = false;
var runDailyJob = false;
var working = 0;
var mcdiff;
var scheduleNotificationId = '';

var job = new CronJob('5 0 * * *', function() {
  runDailyJob = true;
  working = 0;
  notifyAll();

}, function () {
  runDailyJob = false;
  /* This function is executed when the job stops */
},
true  /* , Start the job right now */
/* timeZone /* OPTIONAL Time zone of this job. */
);

//QC3-4513
//surjeet.b... croneJob time is changed to 59th minute of every hour to resolve this bug
var hourlyJob = new CronJob('59 * * * *', function() {
  runHourlyJob = true;
  working = 0;
  notifyAll();

}, function () {
  runHourlyJob = false;
  /* This function is executed when the job stops */
},
true  /* , Start the job right now */
/* timeZone /* OPTIONAL Time zone of this job. */
);

function notifyAll(){
  logger.log("function: notifyAll - start" , 'info', currentFileName);
  log.log('starting...', 'info');
  //Implementing a psudo barier -- leave if working.
  //Will only enter if working is 0! ;)
  if(working) { debug('returning ... '); log.log('working', 'info'); return; }

  var machineDate = new Date();
  var machineoffset = machineDate.getTimezoneOffset();
  var clientOffset = config.timeZones[config.currentTimeZone] || -330;
  mcdiff = 0;
  mcdiff = -machineoffset + parseInt(clientOffset);

  var today = new Date();
  var start = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
  start.setDate(start.getDate());
  start = new Date(start.getTime() + (mcdiff * 60 * 1000));
  var end = new Date(start.getTime() + (24 * 60 * 60 * 1000));
  // var end = new Date(start.getTime() + (24 *60*60*1000));

//QC3-5271
//surjeet.b... now inactive freq will not recieve email

  logger.log("collection:calendar before fetch" , 'info', currentFileName);
  db
  .collection('calendar')
  .find({
    'notified': false,
    'date': {
      $gte: new Date(start.toISOString()),
      $lte: new Date(end.toISOString())
    },
    isActive: true
  })
  .toArray(function (e, entries) {
    // log.log(entries);
    if(!entries || !entries.length ){
      logger.log("collection:calendar error" , 'error', currentFileName);
      debug('No scheduled entries');
      return;
    }else {
      var notificationObj = {
        entries: scheduleMailNotification(entries),
        notifiedEntriesCount: 0,
        notified: false
      };
      
      db.collection('calendarNotification').insertOne(notificationObj,function auditCallback(err, data) {
        log.log("err" + err);
        if(data && data.ops && data.ops.length){
          scheduleNotificationId = data.ops[0]._id;
        }
      });
      debug(entries.length + " scheduled entries");
    }

    logger.log("collection:calendar fetch" , 'info', currentFileName);

    logger.log("call:mapUsers before" , 'info', currentFileName);
    mapUsers(entries, function (e, entries) {
      setTimeout(function () {
      logger.log("call:mapUsers res" , 'info', currentFileName);
      _.forEach(entries, function (entity) {
        if(!entity) {
          logger.log("if(!entity) { return from hear" , 'info', currentFileName);
          return;
        }
        if (entity.type == 'Hourly' && runHourlyJob == true) {
          logger.log("if (entity.type == 'Hourly' && runHourlyJob == true) {" , 'info', currentFileName);
          var startTime = new Date();
          var endTime = new Date(startTime.getTime() + (60*60*1000));
          //Start: QC3-5256 - Single app freq reminder and review reminder email : When App name is changed then in email template changed app name is not reflecting
          //Changed By: Jyotil
          //Description: code to check whether the frequency is completed or past dated.
          var dbCall = '';
          var doCallID = '';
          if (moment.utc((entity.date).toISOString()).isSameOrAfter(moment.utc((startTime).toISOString())) && moment.utc((entity.date).toISOString()).isBefore(moment.utc((endTime).toISOString()))) {
            logger.log(`if (moment.utc((entity.date).toISOString()).isSameOrAfter(moment.utc((startTime).toISOString())) && moment.utc((entity.date).toISOString()).isBefore(moment.utc((endTime).toISOString()))) {` , 'info', currentFileName);
            if (entity.schema && entity.schema._id) {
              logger.log(`if (entity && entity.schema && entity.schema._id) {` , 'info', currentFileName);

              dbCall = 'schema';
              doCallID = entity.schema._id.toString();
            } else if (entity.master && entity.master._id) {
              logger.log(`else if (entity && entity.master && entity.master._id) {` , 'info', currentFileName);
              dbCall = 'masterqcsettings';
              doCallID = entity.master._id.toString();
            }
            if (dbCall) {
              logger.log(`if (dbCall) {` , 'info', currentFileName);

              logger.log("collection:dbCall before fetch" , 'info', currentFileName);
              db
              .collection(dbCall)
              //Start: QC3-5273 - Single & master app freq reminder and review reminder email : For inactive app also email reminder is being sent to user
              //Changed By: Jyotil
              //Description: code to get 'isActive' flag.
              .find({'_id':db.ObjectID(doCallID)}, {'title':true, 'isActive':true})
              //End: QC3-5273 - Single & master app freq reminder and review reminder email : For inactive app also email reminder is being sent to user
              .toArray(function (err, data) {

                logger.log("collection:dbCall fetch" , 'info', currentFileName);

                dbCall==='schema' ? (entity.schema.title = data[0].title) : (entity.master.title = data[0].title);
                working ++;
                _.forEach(entity.keyvalues, function(keyVal){
                  if(keyVal !== null && keyVal.attributeValue && keyVal.attributeValue.length === 24 && keyVal.attributeValue.charAt(23) === 'Z' && keyVal.attributeValue.charAt(10) === 'T' && keyVal.attributeValue.charAt(19) === '.'){
                    keyVal.attributeValue = moment(moment.utc(new Date(keyVal.attributeValue)).toDate()).format('MM/DD/YYYY');
                	}
                });
                //Start: QC3-5273 - Single & master app freq reminder and review reminder email : For inactive app also email reminder is being sent to user
                //Changed By: Jyotil
                //Description: code to check data got now is 'isActive' or not.
                if (data[0].isActive) {
                  logger.log("if (data[0].isActive) {" , 'info', currentFileName);
                //End: QC3-5273 - Single & master app freq reminder and review reminder email : For inactive app also email reminder is being sent to user
              
                  logger.log("call:notify before" , 'info', currentFileName);
                  // PQT-1212 by Yamuna
                  entity.appRecordUrl = generateAppRecordUrl(entity);
                  nofity(entity, function (e, d) {
                    working --;
                    if(e){
                      logger.log("call:notify error : " + e , 'error', currentFileName);
                      return;
                    }
                    logger.log("call:notify res" , 'info', currentFileName);
                    d.notified = true;
                    d.scheduledBy = d.scheduledBy._id;
                    db.calendar.save(d);
                  });
                //Start: QC3-5273 - Single & master app freq reminder and review reminder email : For inactive app also email reminder is being sent to user
                //Changed By: Jyotil
                //Description: code to check data got now is 'isActive' or not.
                }
                //End: QC3-5273 - Single & master app freq reminder and review reminder email : For inactive app also email reminder is being sent to user
              });
            }
          }
        }else {
          logger.log(`if (entity.type == 'Hourly' && runHourlyJob == true) {` , 'info', currentFileName);



          if (runDailyJob == true) {
            logger.log(`if (runDailyJob == true) {` , 'info', currentFileName);
            if (entity && entity.schema && entity.schema._id) {
              logger.log(`if (entity && entity.schema && entity.schema._id) {` , 'info', currentFileName);
              dbCall = 'schema';
              doCallID = entity.schema._id;
            } else if (entity && entity.master && entity.master._id) {
              logger.log(`else if (entity && entity.master && entity.master._id) {` , 'info', currentFileName);
              dbCall = 'masterqcsettings';
              doCallID = entity.master._id;
            }
            if (dbCall) {
              logger.log(`if (dbCall) {` , 'info', currentFileName);

              logger.log("collection:dbCall before fetch" , 'info', currentFileName);
              db
              .collection(dbCall)
              //Start: QC3-5273 - Single & master app freq reminder and review reminder email : For inactive app also email reminder is being sent to user
              //Changed By: Jyotil
              //Description: code to get 'isActive' flag.
              .find({'_id':db.ObjectID(doCallID)}, {'title':true, 'isActive':true})
              //End: QC3-5273 - Single & master app freq reminder and review reminder email : For inactive app also email reminder is being sent to user
              .toArray(function (err, data) {

                logger.log("collection:dbCall fetch" , 'info', currentFileName);
                dbCall==='schema' ? (entity.schema.title = data[0].title) : (entity.master.title = data[0].title);
                _.forEach(entity.keyvalues, function(keyVal){
                	if(keyVal !== null && keyVal.attributeValue && keyVal.attributeValue.length === 24 && keyVal.attributeValue.charAt(23) === 'Z' && keyVal.attributeValue.charAt(10) === 'T' && keyVal.attributeValue.charAt(19) === '.'){
                    keyVal.attributeValue = moment(moment.utc(new Date(keyVal.attributeValue)).toDate()).format('MM/DD/YYYY');
                	}
                });
                //Start: QC3-5273 - Single & master app freq reminder and review reminder email : For inactive app also email reminder is being sent to user
                //Changed By: Jyotil
                //Description: code to check data got now is 'isActive' or not.
                if (data[0].isActive) {
                  logger.log("if (data[0].isActive) {" , 'info', currentFileName);
                //End: QC3-5273 - Single & master app freq reminder and review reminder email : For inactive app also email reminder is being sent to user

                  logger.log("call:nofity before" , 'info', currentFileName);
                  entity.appRecordUrl = generateAppRecordUrl(entity);
                  nofity(entity, function (e, d) {
                    working --;
                    if(e){
                      logger.log("call:nofity error : " + e , 'error', currentFileName);
                      return;
                    }
                    logger.log("call:nofity res" , 'info', currentFileName);
                    d.notified = true;
                    d.scheduledBy = d.scheduledBy._id;
                    db.calendar.save(d);
                  });
                //Start: QC3-5273 - Single & master app freq reminder and review reminder email : For inactive app also email reminder is being sent to user
                //Changed By: Jyotil
                //Description: code to check data got now is 'isActive' or not.
                }
                //End: QC3-5273 - Single & master app freq reminder and review reminder email : For inactive app also email reminder is being sent to user
              });
            }
          }
        }
        //End: QC3-5256 - Single app freq reminder and review reminder email : When App name is changed then in email template changed app name is not reflecting
      });
      if(scheduleNotificationId){
        db.collection('calendarNotification').findOneAndUpdate({ _id: db.ObjectID(scheduleNotificationId.valueOf())}, 
        {
          $set:
          {notifiedEntriesCount : entries.length, 
          notified: true}
            },  function (err, data) {
          if(err) {
            log.log("error ::"+err)
          }
          if(data){
            log.log("calendarNotification updated sucessfully.")
          }
        });
      }
  }, 12000);
    });
  })

  logger.log("function: notifyAll - end" , 'info', currentFileName);
}

function generateAppRecordUrl(entity){
  if(entity.ismasterQcForm){
    return "/tabs/"+entity.calendarmasterId+"/new///false///"+entity._id+"//";
  }else{
    return "/entries/"+entity.calendarschemaId+"/new/////"+entity._id+"///";
  }
}

function nofity(entity, cb){
  logger.log("function: nofity - start" , 'info', currentFileName);
  debug('Sending notification to ' + entity.email);
  log.log('inside file: scheduler.js - function: notify - status: sending mail');

  //QC3-8478 - Vaibhav
  entity.keyValuesEmail=[];
  //QC3-9384 by Yamuna
  entity.keyvalues = _.filter(entity.keyvalues, function(value){return value});
  if (entity.keyvalues && entity.keyvalues.length == 0) {
    logger.log("if (entity.keyvalues && entity.keyvalues.length == 0) {" , 'info', currentFileName);
    entity.keyValuesEmail.push({title: 'N/A', attributeValue: 'N/A'});
  }else{
    entity.keyValuesEmail=_.cloneDeep(entity.keyvalues);
  }
  _.merge(entity, settings);
  if (entity.type == 'Hourly') {
    logger.log("if (entity.type == 'Hourly') {" , 'info', currentFileName);
    _.merge(entity, {displayTime: moment(moment.utc(new Date(entity.date) - (mcdiff * 60 * 1000)).toDate()).format('HH:mm')});
  }else {
    logger.log("else of if (entity.type == 'Hourly') {" , 'info', currentFileName);
    _.merge(entity, {displayTime: moment(moment.utc(new Date(new Date(entity.time))).toDate()).format('HH:mm')});
  }
  _.merge(entity, {displayDate: moment(moment.utc(new Date(entity.date)).toDate()).format('MM/DD/YYYY')});
  debug(entity);
  //scheduler for sedning mail for the reminder for review qc form...
  if (entity.isSetReviewFrequency) {

    logger.log("if (entity.isSetReviewFrequency) {" , 'info', currentFileName);

    logger.log("call:mailer before" , 'info', currentFileName);

    log.log('inside for:: ' + entity.isSetReviewFrequency);
    mailer.send('review-scheduler-notify', entity, entity.email, function sendMailCallback(err, value) {
      // cb(err, entity);
      if (err) {
        logger.log("call:mailer error : " + err , 'error', currentFileName);
        debug.err('Failed sending mail - review-scheduler-notify');
        debug.err(entity);
        debug.err(err);
        cb(err, entity);
      }else {
        logger.log("call:mailer res" , 'info', currentFileName);
        log.log('Email to ' + entity.email + ' sent sucessfully', 'info');
        debug('Email to ' + entity.email.underline + ' sent sucessfully');
        cb(null, entity);
      }
    })
  }

  //scheduler for sedning mail for the reminder to do entry in qc form...
  if (!entity.isSetReviewFrequency) {
    logger.log("if (!entity.isSetReviewFrequency) {" , 'info', currentFileName);
    log.log('inside for:: ' + entity.isSetReviewFrequency);
    log.log('inside file: scheduler.js - function: notify - status: sending mail 1', 'info');

    logger.log("call:mailer before" , 'info', currentFileName);
    mailer.send('schedule-notify', entity, entity.email,function sendMailCallback(e,b) {
      if(e){
        logger.log("call:mailer error : " + e , 'error', currentFileName);
        debug.err('Failed sending email - schedule-notify');
        debug.err(entity);
        debug.err(e);
        cb(e, entity);
      }else{
        logger.log("call:mailer res" , 'info', currentFileName);
        log.log('Email to ' + entity.email + ' sent sucessfully', 'info');
        debug('Email to ' + entity.email.underline + ' sent sucessfully.');
        cb(null, entity);
      }
    });
  }
  logger.log("function: nofity - end" , 'info', currentFileName);
}

function scheduleMailNotification(entries){
  var entriesArray = [];
  var entryObj = {};
  entriesArray = entries.map(function(entry){
    if(entry){
      entryObj = {
        id: entry._id,
        title: entry.title,
        schemaTitle: entry.schema ? entry.schema.title : (entry.master ? entry.master.title : ''),
        type: entry.type,
        scheduledBy: entry.scheduledBy ? entry.scheduledBy.firstname + ' ' + entry.scheduledBy.lastname + ' ('+ entry.scheduledBy.username + ')' : '',
        date: moment(moment.utc(entry.date).toDate()).format('MM/DD/YYYY HH:mm:ss'),
        time: moment(moment.utc(entry.time).toDate()).format('MM/DD/YYYY HH:mm:ss'),
        overdueDate: moment(moment.utc(entry.overdueDate).toDate()).format('MM/DD/YYYY HH:mm:ss'),
        overdueTime: moment(moment.utc(entry.overdueTime).toDate()).format('MM/DD/YYYY HH:mm:ss')
      } 
      return entryObj;
    }else{
      return {}
    }
  })
  return entriesArray;
  }
debug('********************* | Thats All :)  | *********************');
