/**
* @name Qulification Performance - Close Window Service
* @author Yamuna Sardhara <yamuna.s@productivet.com>
*
* @version 1.0
*/

//To run every day 12 AM !!
// var job = new CronJob('59 * * * *', function() {
//   checkGeneralSettings();
// }, function () {},
// true
// );

//var Promise = require('promise');
var colors = require('colors');
var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('./logger').configure(__dirname, enableBunyan);
var CronJob = require('cron').CronJob;
var logger = require('../logger');
var mailer = require('./mailer');
var db = require('./db');
var debug = require('./logger').debug('lib:closewindow');
var q = require('q');
var moment = require('moment-timezone');
var handlebars = require('handlebars');
var currentFileName = __filename;
var searchRecords = require('../routes/qulificationperformance/searchRecords.js');
var formattedModelForEmail = require('../routes/qulificationperformance/formattedModelForEmail.js');
var ip = require('ip');

// To run everday 12:09 for testing
var job = new CronJob('9 0 * * *', function() {
  console.log('-------------------------------Inside Close Window Job-------------------------');
  closeWindow();
}, function () {},
true
);

handlebars.registerHelper('ifCondition', function (v1, operator, v2, options) {
    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
          }
});

//For check generalSettings configure day is equal to today or not
function checkGeneralSettings(){
  console.log('-------------------------------checkGeneralSettings------------------------');
  var deferred = q.defer();
  db.generalSettings.findOne({}, {'_id':0,'closeWindowDay':1}, function(err, generalSettings){
    if(err){
      deferred.reject(err);
    }
    var currentDay = new Date().getDate();
    var lastDay = new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).getDate();
    if(generalSettings && generalSettings.closeWindowDay == 31 || ((generalSettings.closeWindowDay == 30 || generalSettings.closeWindowDay == 29) && (new Date().getMonth() == 1))){
      if(currentDay == lastDay){
        deferred.resolve(true);
      }else{
        deferred.resolve(false);
      }
    }else if(currentDay == generalSettings.closeWindowDay){
      deferred.resolve(true);
    }else{
      deferred.resolve(false);
    }
  });
  return deferred.promise;
}

//For getting all current window of the previous month
function getAllWindows() {
  console.log('-------------------------------------getAllWindows-----------------------------');
  var deferred = q.defer();
  var startDate = new Date(new Date().getFullYear(), new Date().getMonth()-1, 1);
  var endDate = new Date(new Date().getFullYear(), new Date().getMonth(), 0);
  var closeWindowQuery = {['$or']: []};
  closeWindowQuery['$or'].push({'windowStartDate' :{'$gte': new Date(startDate).toISOString(),'$lte': new Date(endDate).toISOString()}});
  closeWindowQuery['$or'].push({'windowStartDate':{$gte:new Date(startDate.toISOString()),$lte: new Date(endDate.toISOString())}});
    db.windows.find(closeWindowQuery).toArray(function (error, windows){
      if(error){
        logger.log("db call: windows - err", 'error', currentFileName);
        deferred.reject(error);
      }
      var currentWindows = windows.filter(function(window){
        return window.windowStatus.title == "Current";
      });
      deferred.resolve(currentWindows);
    });
    return deferred.promise;
  }

//For Update the Window status to Closed and send email
function closeWindowFunction(windows, closedWindows){
  console.log('------------------------closeWindowFunction--------------------------');
  var deferred = q.defer();
  if (windows.length) {
    var window = windows.shift();
    if(window){
      var params = {id: window._id, windowId: window.windowId};
      searchRecords.fetchAppRecords(params,function (err, response) {
        if(err){
          logger.log("call:getAppRecordCount error" + err , 'error', currentFileName);
        }
        var query = {};
        if ((response != "No Records Found.") && response.secondStage && response.secondStageInfo) {
          query = {
            "windowStatus": {"id" : 3,"title" : "Closed" },
            "firstStageInformation": response.firstStage,
            "secondStageInformation": response.secondStageInfo
          };
        }else if ((response != "No Records Found.") && response.firstStage) {
          query = {
            "windowStatus": {"id" : 3,"title" : "Closed" },
            "firstStageInformation": response.firstStage
          };
          //QC3-10838 by Yamuna
        }else if(response == "No Records Found."){
          query = {
          "windowStatus": {"id" : 3,"title" : "Closed" }
          };
        }
        db.windows.findAndModify({_id: db.ObjectID(window._id)}, {},{ $set: query},{new: true}, function(err, windowData) {
          if(err) {
            logger.log("error ::" + err);
            deferred.resolve(closedWindows);
          }
          if(windowData && windowData.value){
            db.windows.auditThis1(windowData.value, ip.address());
            console.log('Window Updated Successfully.');
            formattedModelForEmail.getFormattedModel(windowData.value).then(function(modelForEmail){
              modelForEmail.templateType = "closeWindow";
              notify(modelForEmail,function(err,result){
                if(err){
                  logger.log("call:notify error : " + err , 'error', currentFileName);
                  deferred.reject(err);
                  return;
                }
                logger.log("call:notify res" , 'info', currentFileName);
                closeWindowFunction(windows, closedWindows).then(function(err,data){
                  deferred.resolve(closedWindows);
                }, function(){
                  deferred.resolve(closedWindows);
                });
              });
            });
          }else {
            deferred.resolve(closedWindows);
          }
        });
      });
    }else{
      closeWindowFunction(windows, closedWindows).then(function(err,data){
        deferred.resolve(closedWindows);
      }, function(){
        deferred.resolve(closedWindows);
      });
    }
  }else{
    deferred.resolve(closedWindows);
  }
  return deferred.promise;
}

// For notify email
function notify(entity,cb){
  mailer.send('qp-notify-common', entity, entity.emailAddresses, function sendMailCallback(err, value) {
    if (err) {
      debug.err('Failed sending mail - close-window-notify');
      debug.err(err);
      cb(err, entity);
    } else {
      logger.log('Email has been sent to ' + entity.emailAddresses);
      debug('Mail send successfully to: ' + entity.emailAddresses);
      cb(null, entity);
    }
  });
}

function closeWindow(){
  logger.log("call:starting closeWindow CronJob" , 'info', currentFileName);
  checkGeneralSettings().then(function(isClosedDay){
    console.log(isClosedDay + ' : isClosedDay');
    if(isClosedDay){
      getAllWindows().then(function(windows) {
        closeWindowFunction(windows, []).then(function(data){
        })
      });
    }
  })
}
