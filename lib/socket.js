'use strict'
var socketioJwt = require('socketio-jwt');
var debug = require('./logger').debug('lib:socket');
var io = {};

function configure(http) {
    io = require('socket.io')(http);
    debug('Setting up socket io');

    io.on('connection',socketioJwt.authorize({
        secret: require('./oauth').pk,
        timeout: 15000
    }));

    io.on('hello',function onHelloCallback(msg){
        debug(msg);
    });

    io.on('authenticated',function onAuthenticatedCallback(socket){
        debug('User Authenticated : ' + socket.decoded_token.email);
        // socket.on('hello',function onHelloCallback(msg){
        //     debug('User connected');
        // });
    });
}

module.exports = {
  io: io,
  configure: configure
};
