'use strict'
var debug = require('./logger').debug('lib:settings');
var logger = require('./logger');
var resources = require('./resources');
var WatchJS = require("watchjs");
var watch = WatchJS.watch;
var config = require('../config/config.js');
var db = require('./db');

//console.log('--------------------settings.js--------------------');
//console.log('envurl: ' + config.envurl);


var settingsResource = resources.fetch({
  name: 'settings',
  collection: 'settings'
});

var settings = {
  default: {}
};

function fetchSettings() {
  if(settings.loaded){
    return;
  }

  settingsResource
  .getOneAsync('default')
  .then(function (settingsFromDb) {
    if(!settingsFromDb){
      settingsResource.createAsync(settings.default = {
        _id: 'default',
        title: 'Perfeqta Settings',
        company: 'Perfeqta',
        product: 'Perfeqta Product',
        web: config.envurl
      })
      .then(function createSettings(data) {
        debug('Default settings inserted');
      })
      .catch(function catchSettingsError(err) {
        debug.error('Failed to created setitngs!');
        debug(err);
      });
    }else {
      settings.default = settingsFromDb;
      settings.default.support=settings.default.support?settings.default.support:'support@beperfeqta.com';

      db['generalSettings'].find().toArray().then(function(data){
         settings.default.support = data[0].supportContactDetails || settings.default.support;
      });
    }

    debug('Running with following settings: ');
    delete settings.default._id;
    debug(settings);

    settings.resource = settingsResource;

  });
}
watch(settings, "default", function () {
  if (settings && settings.default && settings.default.title) {
    //EMPTY IF TO PASS ELSE PART
  }
  else {
      //fetchSettings();
      settings.default = {
        "_id" : "default",
        "title" : "QC Manager 3.0 Settings",
        "company" : "The PERFEQTA",
        "product" : "PERFEQTA",
        "web" : config.envurl
      }
  }
    var d = new Date();
    var fs = require('fs');
    var util = require('util');
    var log_file = fs.createWriteStream(__dirname + '/ADlog.log', { flags: 'w' });
    var log_stdout = process.stdout;
    log_file.write(util.format(d) + '\n');
    log_stdout.write(util.format(d) + '\n');
    console.log("Settings Changed");
});
fetchSettings();

module.exports = settings;
