var db = require('../db');
var _ = require('lodash');
var Promise = require('bluebird');

function findOne(batchId, cb) {
    var queryGenerate = {
        record: {
            batchNo: batchId
        },
        attribute: {
            batchNo: batchId
        }
    };

    getBatchEntries(null, null, null, queryGenerate).then(function (batchEntries) {
        cb(null, batchEntries.find(function (batchEntry) {
            return batchEntry.batchEntries && batchEntry.batchEntries[batchId]
        }));
    }).catch(function (err) {
        cb(err);
    });
}

function resetBatchEntry(schemaId, batchNo) {

    return db.collection('batchEntries').find({
            'schemaId': schemaId
        })
        .toArrayAsync()
        .then(function (batchEntries) {
            var qAll = [];
            batchEntries.forEach(function (batchEntry) {
                qAll.push(db.collection('batchEntriesRecords').removeAsync({
                    batchEntriesId: batchEntry._id,
                    batchNo: batchNo
                }));
                qAll.push(db.collection('batchEntriesAttributes').removeAsync({
                    batchEntriesId: batchEntry._id,
                    batchNo: batchNo
                }));
            });
            return Promise.all(qAll);
        });
}

function updateBatchEntry(updateObject, id, batchNo, cb) {

    var batchEntries = _.cloneDeep(updateObject.batchEntries);
    var batchAttributesInfo = [_.cloneDeep(updateObject.batchAttributesInfo)];

    if (id) {
        db.collection('batchEntriesRecords').count({
            batchEntriesId: db.ObjectID(id.toString()),
            'batchNo': batchNo
        }, function (err, count) {
            if (err) {
                cb(err);
            } else {
                batchEntries.forEach((bEntry, index) => {
                    bEntry.batchNo = batchNo;
                    bEntry.schemaId = updateObject.schemaId;
                    bEntry.sequenceNo = count + index;
                    bEntry.batchEntriesId = db.ObjectID(id.toString());
                });

                batchAttributesInfo.forEach((bEntry, index) => {
                    bEntry.batchNo = batchNo;
                    bEntry.schemaId = updateObject.schemaId;
                    bEntry.batchEntriesId = db.ObjectID(id.toString());
                });

                insertChildData();
            }
        });
    } else {
        delete updateObject.batchEntries;
        delete updateObject.batchAttributesInfo;
        db.collection('batchEntries').update({
            'schemaId': updateObject.schemaId
        }, {
            $set: updateObject
        }, {
            upsert: true,
            new: true
        }, function (errorA, data) {
            if (errorA) {
                cb(errorA);
            } else {
                db.collection('batchEntries').findOne({
                        'schemaId': updateObject.schemaId
                    }, {
                        _id: true
                    },
                    function (err, batchEntryGot) {
                        if (!err) {

                            id = batchEntryGot._id
                            db.collection('batchEntriesRecords').count({
                                batchEntriesId: id,
                                'batchNo': batchNo
                            }, function (err, count) {
                                if (err) {
                                    cb(err);
                                } else {
                                    batchEntries.forEach((bEntry, index) => {
                                        bEntry.batchNo = batchNo;
                                        bEntry.schemaId = updateObject.schemaId;
                                        bEntry.sequenceNo = count + index;
                                        bEntry.batchEntriesId = id;
                                    });

                                    batchAttributesInfo.forEach((bEntry, index) => {
                                        bEntry.batchNo = batchNo;
                                        bEntry.schemaId = updateObject.schemaId;
                                        bEntry.batchEntriesId = id;
                                    });

                                    insertChildData();
                                }

                            });
                        } else {
                            cb('Id not found');
                        }
                    });
            }
        });
    }


    function insertChildData() {
        var mArr = [];

        if (batchEntries && batchEntries.length) {
            mArr.push(Promise.each(batchEntries, function (batchEntry) {
                delete batchEntry._id;

                if (batchEntry.formId) {
                    return db.collection('batchEntriesRecords').updateAsync({
                        formId: batchEntry.formId
                    }, {
                        $set: batchEntry
                    }, {
                        upsert: true
                    });
                } else {
                    return Promise.resolve(true);
                }
            }));
        }

        if (batchAttributesInfo && batchAttributesInfo.length) {
            mArr.push(Promise.each(batchAttributesInfo, function (battributeInfo) {
                delete battributeInfo._id;

                return db.collection('batchEntriesAttributes').updateAsync({
                    batchNo: battributeInfo.batchNo,
                    schemaId: battributeInfo.schemaId,
                    batchEntriesId: db.ObjectID(battributeInfo.batchEntriesId.toString())
                }, {
                    $set: battributeInfo
                }, {
                    upsert: true
                });
            }));
        }

        Promise.all(mArr).then(function () {
            var childQuery = {
                record: {
                    batchNo: batchNo
                }
            };

            getBatchEntries(null, null, id, childQuery)
                .then(function (datad) {
                    cb(null, datad)
                });
        });
    }
}


function getCount(schemaId, batchNo, cb) {
    getBatchEntries(schemaId).then(function (batchEntries) {
        var filteredBatchEntries = batchEntries.filter(function (batchEntry) {
            return batchEntry.batchEntries[batchNo];
        });
        cb(null, filteredBatchEntries.length);
    }).catch(function (err) {
        cb(err);
    });
}

function getBatchEntries(schemaId, batchEntries, id, childQueries) {

    var batchEntriesData = [];

    if (batchEntries) {
        return extractData(batchEntries, function (batchEntry) {
            batchEntriesData.push(batchEntry);
        }, childQueries).then(function () {
            return batchEntriesData;
        });
    } else {
        var queryObj = {};
        if (id) {
            queryObj['_id'] = id;
        }
        if (schemaId) {
            queryObj['schemaId'] = schemaId;
        }

        return db.collection('batchEntries').find(queryObj).toArrayAsync()
            .then(function (batchEntries) {
                return extractData(batchEntries, function (batchEntry) {
                    batchEntriesData.push(batchEntry);
                }, childQueries).then(function () {
                    return batchEntriesData;
                });
            });
    }
}

function extractData(batchEntries, pushBatchentryCB, query) {

    var qall = [];

    batchEntries.forEach(function (batchEntry) {

        query = query || {};
        query.record = query.record || {};
        query.attribute = query.attribute || {};
        query.record = Object.assign(query.record, {
            batchEntriesId: batchEntry._id
        });
        query.attribute = Object.assign(query.attribute, {
            batchEntriesId: batchEntry._id
        });

        if (typeof query.recordSelect === 'string') {
            query.recordSelect = _.split(query.recordSelect, ',');
            query.recordSelect = _.reduce(query.recordSelect, function (resD, field) {
                resD[field] = true;
                return resD;
            }, {})
        } else {
            query.recordSelect = {};
        }

        qall.push(
            Promise.props({
                batchRecordData: db.collection('batchEntriesRecords')
                    .find(query.record, query.recordSelect)
                    .toArrayAsync(),
                batchAttributeData: db.collection('batchEntriesAttributes')
                    .find(query.attribute)
                    .toArrayAsync()
            }).then(function (aData) {
                var batchEntriesObject = {};
                aData.batchRecordData.forEach(function (bRec) {
                    batchEntriesObject[bRec.batchNo] = batchEntriesObject[bRec.batchNo] || [];
                    batchEntriesObject[bRec.batchNo].push(bRec);
                });

                var batchAttributesObject = {}
                aData.batchAttributeData.forEach(function (bRec) {
                    batchAttributesObject[bRec.batchNo] = bRec;
                });

                batchEntry.batchEntries = batchEntriesObject;
                batchEntry.batchAttributesInfo = batchAttributesObject;

                pushBatchentryCB(batchEntry);
                return true;
            }));
    });

    return Promise.all(qall);
}

module.exports = {
    resetBatchEntry: resetBatchEntry,
    getCount: getCount,
    updateBatchEntry: updateBatchEntry,
    getBatchEntries: getBatchEntries,
    findOne: findOne
}