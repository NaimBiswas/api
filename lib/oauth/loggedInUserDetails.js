var debug = require('../logger').debug('lib:oauth');
var decode = require('./model').getAccessToken;
var _ = require('lodash');
var lzstring = require('lz-string');
var db = require('../db');
var model =require('./model');
var q = require('q');
var oAppPermission = require('./oAppPermission');

var currentFileName = __filename;
var logger = require('../../logger');

function getAllPermission(u,cb){
  logger.log("function: getAllPermission - start" , 'info', currentFileName);

  //get all roles, module of this user.
  var rolesToFetch =_.sortedUniq(_.map(u.roles, function (role) {
    return role.title;
  }));

  logger.log("collection:modules,roles before fetch" , 'info', currentFileName);
  q.all([
    //1:Module
     db.modules.find({}).toArray(),
     db.roles.find({ title: { $in : rolesToFetch } }).toArray()
  ]).then(function(resp){

    logger.log("collection:modules,roles fetch" , 'info', currentFileName);
    var modules = resp[0];
    var roles = resp[1];
     u.permissions = _.reduce(roles, function (a, role) {
        _.forEach(role.modules, function (module) {
                a[module._id] = a[module._id] || {};
                _.forEach(module.permissions, function (p) {
                    //Generate code
                    var per =
                    (p.view ? '1' : '0') // view
                  + (p.edit ? '1' : '0')
                  + (p.create ? '1' : '0')
                  + (p.editSchema ? '1' : '0')
                  + (p.review ? '1': '0')
                  + (p.assignment ? '1' : '0'); //for qualificationPerformance by Yamuna
                    if (per.indexOf('1') !== -1) {
                        if (a[module._id][p.entity]) {
                            var p2 = a[module._id][p.entity];
                            per = _.map(per, function (v, i) {
                                //If either of permissions are 1, then return 1 otherwise 0
                                // doing a logical or
                                //Shorter version
                                return (parseInt(per[i]) || parseInt(p2[i])).toString();
                                //Longer version
                                //(per[i] === '1') ? '1' : ((p2[i] === '1') ? '1' : '0');
                            }).join('');
                        };
                        a[module._id][p.entity] = per;
                    }
               });
        });
         return a;
    }, {});
    delete u.permissions[undefined];
     _.forEach(u.permissions, function (per,key) {
         var indx = _.findIndex(modules, function (mr) {
             return mr.isActive && mr._id == key;
         });
         if (indx == -1) {
             delete u.permissions[key];
         }
      });
    cb(null,u);
  },function(err){
    logger.log("collection:modules,roles error : " + err , 'error', currentFileName);
    cb(err,null);
  });


  logger.log("function: getAllPermission - end" , 'info', currentFileName);
}

function detailsByToken(req, res, cb) {
    logger.log("function: detailsByToken - start" , 'info', currentFileName);
    //check for validity of token X-Authorization;
    var xToken = req.headers['x-authorization'] || req.query['x-authorization'];
    if(xToken){
      logger.log("if(xToken){" , 'info', currentFileName);

      logger.log("collection:allAuthToken before fetch" , 'info', currentFileName);
      db['allAuthToken'].findOne({token : xToken},function(err,token){
        if(err){
          logger.log("collection:allAuthToken error : " + err , 'error', currentFileName);
          cb(err);
          return;
        }

        logger.log("collection:allAuthToken fetch" , 'info', currentFileName);
        if(!token){
          logger.log("if(!token){" , 'info', currentFileName);
          //case :1 forcefully looged out user, then need to check user concurrency and populate message accordingly.
          if(xToken.indexOf('_') > 0){
            logger.log("if(xToken.indexOf('_') > 0){" , 'info', currentFileName);
            var uId = db.ObjectID(xToken.split('_')[1]);

            logger.log("collection:users before fetch" , 'info', currentFileName);
             db['users'].findOne({_id : uId},function(err,u){
               if(err){
                 logger.log("collection:users error : " + err , 'error', currentFileName);
                 cb(err);
                 return;
               }

               logger.log("collection:users fetch" , 'info', currentFileName);
               if(!u){
                 logger.log("if(!u){" , 'info', currentFileName);
                  res.status(401).json({message: 'Need to pass valid token.'});
                  return;
               }
               else if (!u.isConCurrentUserActive) {
                 logger.log("else if (!u.isConCurrentUserActive) {" , 'info', currentFileName);
                  res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session has been ended by ' + u.forceRemovedUser });
                  return;
              }
              else{
                  logger.log("else of if(!u){" , 'info', currentFileName);
                 res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session has been ended.'});
                 return;
              }

              
              //cb(null, usersDetails);
             });
          }else{
            logger.log("else of if(xToken.indexOf('_') > 0){" , 'info', currentFileName);
            res.status(401).json({message: 'Need to pass valid token.'});
          }

        }
        else{
          logger.log("else of if(!token){" , 'info', currentFileName);
          //check for expirey of token if any - todo
          //Step :1 - get User and then we will get Roles form there to build our run time Permissions.
          //again there may be token which does not contain userId, i.e for outside user.
          if(token.tokenType === 'User' || token.tokenType === 'App'){
            logger.log("if(token.tokenType === 'User' || token.tokenType === 'App'){" , 'info', currentFileName);

            logger.log("collection:users before fetch" , 'info', currentFileName);
            db['users'].findOne({_id : db.ObjectID(token.userId || token.user._id)},function(err,u){
              if(err){
                logger.log("collection:users error : " + err , 'error', currentFileName);
                cb(err);
                return;
              }
              logger.log("collection:users fetch" , 'info', currentFileName);
              if(!u || !u.isActive){
                logger.log("if(!u || !u.isActive){" , 'info', currentFileName);
                //return and say that user is not valid.
                res.status(401).json({message: 'Token associted with Invalid User.'});
              }
              else if(token.tokenType === "User"){
                logger.log(`else if(token.tokenType === "User"){` , 'info', currentFileName);
                //now we have user, all we have to do is get the roles of this user and fetch the roles and create the permission metrix.
                //before that check for the concurrent users, keeping the previous code as it is.
                if (u.usersUUID == token.usersUUID) {
                  logger.log(`if (u.usersUUID == token.usersUUID) {` , 'info', currentFileName);
                  if (req.query && (req.query.pageName == "conCurrentUser" ) && u.isConCurrentUserAdmin && !u.isConCurrentUserActive) {
                    logger.log(`if (req.query && (req.query.pageName == "conCurrentUser" ) && u.isConCurrentUserAdmin && !u.isConCurrentUserActive) {` , 'info', currentFileName);
                    //so here all good, we can create our own permission matrix and will assign that to req.user;

                    logger.log(`call:getAllPermission before` , 'info', currentFileName);
                    getAllPermission(u,function(er,u){
                      if(er){
                        logger.log('call:getAllPermission error : ' + er , 'error', currentFileName);
                        cb(er);
                      }
                      logger.log(`call:getAllPermission res` , 'info', currentFileName);
                      req.user = u;
                      cb(null, u);
                    });
                  }
                  else if (!u.isConCurrentUserActive) {
                    logger.log(`else if (!u.isConCurrentUserActive) {` , 'info', currentFileName);
                      res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session has been ended by ' + u.forceRemovedUser });
                      return;
                  }
                  else {
                    logger.log(`else of if (req.query && (req.query.pageName == "conCurrentUser" ) && u.isConCurrentUserAdmin && !u.isConCurrentUserActive) {` , 'info', currentFileName);

                    logger.log(`call:getAllPermission before` , 'info', currentFileName);
                    getAllPermission(u,function(er,u){
                      if(er){
                        logger.log('call:getAllPermission error : ' + er , 'error', currentFileName);
                        cb(er);
                      }

                      logger.log(`call:getAllPermission res` , 'info', currentFileName);
                      req.user = u;
                      cb(null, u);
                    });
                  }
                } else {
                  logger.log(`else of if (u.usersUUID == token.usersUUID) {` , 'info', currentFileName);
                //Chandni : Code start for Swagger
                //Below code is written to verfiy that request came from swagger
                  if(data.user && data.user.usersUUID && (data.user.usersUUID == u.swaggerApiKey)){
                    logger.log(`if(data.user && data.user.usersUUID && (data.user.usersUUID == u.swaggerApiKey)){` , 'info', currentFileName);

                    logger.log(`call:getAllPermission before` , 'info', currentFileName);
                    getAllPermission(u,function(er,u){
                      if(er){
                        logger.log('call:getAllPermission error : ' + er , 'error', currentFileName);
                        cb(er);
                      }

                      logger.log(`call:getAllPermission res` , 'info', currentFileName);
                      req.user = u;
                      cb(null, u);
                    });
                  }
                  //Chandni : Code end for Swagger
                  res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session has been ended by ' + u.forceRemovedUser  });
                  return;
                }
              }else if(token.tokenType === "App"){
                logger.log(`else if(token.tokenType === "App"){` , 'info', currentFileName);
                 //here we have to handle permission for outside users. - to do-
              //for now we are maintaing the all selectedModules and building a user according to behalf of that user
              //so give defalut permission and bind to that user and , no need to check sites permission as discussed with @umesh.s
              // need to fetch App first

              logger.log("collection:appAccessConfig before fetch" , 'info', currentFileName);
              db.appAccessConfig.findOne({token : token.token},function(err,appAccessConfiguration){
                if(err){
                  logger.log("collection:appAccessConfig error : " + err , 'error', currentFileName);
                  cb(err);
                }
                logger.log("collection:appAccessConfig fetch" , 'info', currentFileName);
                if(!appAccessConfiguration || !appAccessConfiguration.isActive){
                  logger.log("if(!appAccessConfiguration || !appAccessConfiguration.isActive){" , 'info', currentFileName);
                  res.status(401).json({message: 'Token associted with Invalid App.'});
                }
                var schemaId = db.ObjectID(appAccessConfiguration.app._id);

                logger.log("collection:schema before fetch" , 'info', currentFileName);
                db.schema.findOne({_id : schemaId},function(err,sch){
                  if(err){
                    logger.log("collection:schema error : " + err , 'error', currentFileName);
                    cb(err);
                  }
                  logger.log("collection:schema fetch" , 'info', currentFileName);
                  if(!sch || !sch.isActive){
                    logger.log("if(!sch || !sch.isActive){" , 'error', currentFileName);
                    res.status(401).json({message: 'Token associted with Invalid App.'});
                  }
                  oAppPermission.buildPermission(u,sch);
                  u.authType = "App";
                  req.user = u;
                  cb(null, u);
                });
              });
              }
              else{
                logger.log("else of if(!u || !u.isActive){" , 'info', currentFileName);
                //why that's happened on behalf of user but not type is there.
                res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session is not valid.' });
                return;
              }
            });
          }else{
            logger.log("else of if(token.tokenType === 'User' || token.tokenType === 'App'){" , 'info', currentFileName);
           //here we will maintain ApiToken - but that is in near future.
           res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session is not valid.' });
           return;
            // cb();
          }
          //now the user is valid;
        }
      });

     }
    else{
      logger.log("else of if(xToken){" , 'info', currentFileName);
       res.status(401).json({message: 'Need to pass bearer token authorization header'});
    }
    logger.log("function: detailsByToken - end" , 'info', currentFileName);
}




module.exports = {
  detailsByToken : detailsByToken,
  getAllPermission:getAllPermission
};
