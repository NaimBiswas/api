'use strict'
/**
* @name oauth.authorize
* @author Dave Amit <dave.a@productivet.com>
*
* @version 0.0.0
*/


var debug = require('../logger').debug('lib:oauth');
var decode = require('./model').getAccessToken;
var _ = require('lodash');
var lzstring = require('lz-string');
var db = require('../db');
var model =require('./model');
var q = require('q');
var oAppPermission = require('./oAppPermission');

var currentFileName = __filename;
var logger = require('../../logger');


function getAllPermission(u,cb){
  logger.log("function: getAllPermission - start" , 'info', currentFileName);

  //get all roles, module of this user.
  var rolesToFetch =_.sortedUniq(_.map(u.roles, function (role) {
    return role.title;
  }));

  logger.log("collection:modules,roles before fetch" , 'info', currentFileName);
  q.all([
    //1:Module
     db.modules.find({}).toArray(),
     db.roles.find({ title: { $in : rolesToFetch } }).toArray()
  ]).then(function(resp){

    logger.log("collection:modules,roles fetch" , 'info', currentFileName);
    var modules = resp[0];
    var roles = resp[1];
     u.permissions = _.reduce(roles, function (a, role) {
        _.forEach(role.modules, function (module) {
                a[module._id] = a[module._id] || {};
                _.forEach(module.permissions, function (p) {
                    //Generate code
                    var per =
                    (p.view ? '1' : '0') // view
                  + (p.edit ? '1' : '0')
                  + (p.create ? '1' : '0')
                  + (p.editSchema ? '1' : '0')
                  + (p.review ? '1': '0')
                  + (p.assignment ? '1' : '0'); //for qualificationPerformance by Yamuna
                    if (per.indexOf('1') !== -1) {
                      logger.log("if (per.indexOf('1') !== -1) {" , 'info', currentFileName);
                        if (a[module._id][p.entity]) {
                          logger.log("if (a[module._id][p.entity]) {" , 'info', currentFileName);
                            var p2 = a[module._id][p.entity];
                            per = _.map(per, function (v, i) {
                                //If either of permissions are 1, then return 1 otherwise 0
                                // doing a logical or
                                //Shorter version
                                return (parseInt(per[i]) || parseInt(p2[i])).toString();
                                //Longer version
                                //(per[i] === '1') ? '1' : ((p2[i] === '1') ? '1' : '0');
                            }).join('');
                        };
                        a[module._id][p.entity] = per;
                    }
               });
        });
         return a;
    }, {});
    delete u.permissions[undefined];
     _.forEach(u.permissions, function (per,key) {
         var indx = _.findIndex(modules, function (mr) {
             return mr.isActive && mr._id == key;
         });
         if (indx == -1) {
             delete u.permissions[key];
         }
      });
    cb(null,u);
  },function(err){
    logger.log("collection:modules,roles error : " + err , 'error', currentFileName);
    cb(err,null);
  });

  logger.log("function: getAllPermission - end" , 'info', currentFileName);
}

function authenticate(req, res, next) {
  logger.log("function: authenticate - start" , 'info', currentFileName);
  //check for validity of token X-Authorization;
  var xToken = req.headers['x-authorization'] || req.query['x-authorization'];
  if(xToken){
    logger.log("if(xToken){" , 'info', currentFileName);

    logger.log("collection:allAuthToken before fetch" , 'info', currentFileName);
    db['allAuthToken'].findOne({token : xToken},function(err,token){
      if(err){

        logger.log("collection:allAuthToken error : " + err , 'error', currentFileName);
        next(err);
        return;
      }
      logger.log("collection:allAuthToken fetch" , 'info', currentFileName);
      if(!token){
        logger.log("if(!token){" , 'info', currentFileName);
        //case :1 forcefully looged out user, then need to check user concurrency and populate message accordingly.
        if(xToken.indexOf('_') > 0){
          logger.log("if(xToken.indexOf('_') > 0){" , 'info', currentFileName);
          var uId = db.ObjectID(xToken.split('_')[1]);

          logger.log("collection:users before fetch" , 'info', currentFileName);
           db['users'].findOne({_id : uId},function(err,u){
             if(err){

              logger.log("collection:users error : "+ err , 'error', currentFileName);
               next(err);
               return;
             }

            logger.log("collection:users fetch" , 'info', currentFileName);
             if(!u){
               logger.log("if(!u){" , 'info', currentFileName);
                res.status(401).json({message: 'Need to pass valid token.'});
                return;
             }
             else if (!u.isConCurrentUserActive) {
               logger.log("else if (!u.isConCurrentUserActive) {" , 'info', currentFileName);
                res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session has been ended by ' + u.forceRemovedUser });
                return;
            }
            else{
              logger.log("else of if(!u){" , 'info', currentFileName);
               res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session has been ended.'});
               return;
            }
            // var usersDetails = u;
            // delete usersDetails.password;
            // delete usersDetails.temporaryPassword;
            // delete usersDetails.passwordLog;
            // delete usersDetails.failedLoginAttempts;
            // delete usersDetails.securityAnswer;
            // delete usersDetails.passwordExpiresOn;
            // delete usersDetails.forgotPasswordCode;
            // delete usersDetails.verificationCode;
            // delete usersDetails.versions;
            // req.user = usersDetails;
           });
        }else{
          logger.log("else of if(xToken.indexOf('_') > 0){" , 'info', currentFileName);
          res.status(401).json({message: 'Need to pass valid token.'});
        }

      }
      else{
        logger.log("else of if(!token){" , 'info', currentFileName);
        //check for expirey of token if any - todo
        //Step :1 - get User and then we will get Roles form there to build our run time Permissions.
        //again there may be token which does not contain userId, i.e for outside user.
        if(token.tokenType === 'User' || token.tokenType === 'App'){

          logger.log("if(token.tokenType === 'User' || token.tokenType === 'App'){" , 'info', currentFileName);

          logger.log("collection:users before fetch" , 'info', currentFileName);
          db['users'].findOne({_id : db.ObjectID(token.userId || token.user._id)},function(err,u){
            if(err){
              logger.log("collection:users error : " + err , 'error', currentFileName);
              next(err);
              return;
            }


            logger.log("collection:users fetch" , 'info', currentFileName);
            if(!u || !u.isActive){
              logger.log("if(!u || !u.isActive){" , 'info', currentFileName);
              //return and say that user is not valid.
              res.status(401).json({message: 'Token associted with Invalid User.'});
            }
            else if(token.tokenType === "User"){
              logger.log(`else if(token.tokenType === "User"){` , 'info', currentFileName);
              //now we have user, all we have to do is get the roles of this user and fetch the roles and create the permission metrix.
              //before that check for the concurrent users, keeping the previous code as it is.
              if (u.usersUUID == token.usersUUID) {
                logger.log("if (u.usersUUID == token.usersUUID) {" , 'info', currentFileName);
                if (req.query && (req.query.pageName == "conCurrentUser" ) && u.isConCurrentUserAdmin && !u.isConCurrentUserActive) {
                  logger.log(`if (req.query && (req.query.pageName == "conCurrentUser" ) && u.isConCurrentUserAdmin && !u.isConCurrentUserActive) {` , 'info', currentFileName);
                  //so here all good, we can create our own permission matrix and will assign that to req.user;
                  logger.log(`call:getAllPermission before` , 'info', currentFileName);
                  getAllPermission(u,function(er,u){
                    if(er){
                      logger.log('call:getAllPermission error : '+ er , 'error', currentFileName);
                      next(er);
                    }

                    logger.log(`call:getAllPermission res` , 'info', currentFileName);
                    req.user = u;
                    next();
                  });
                }
                else if (!u.isConCurrentUserActive) {
                    logger.log(`else if (!u.isConCurrentUserActive) {` , 'info', currentFileName);
                    res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session has been ended by ' + u.forceRemovedUser });
                    return;
                }
                else {
                  logger.log(`else of if (req.query && (req.query.pageName == "conCurrentUser" ) && u.isConCurrentUserAdmin && !u.isConCurrentUserActive) {` , 'info', currentFileName);
                  logger.log(`call:getAllPermission before` , 'info', currentFileName);
                  getAllPermission(u,function(er,u){
                    if(er){
                      logger.log('call:getAllPermission error : '+ er , 'error', currentFileName);
                      next(er);
                    }


                    logger.log(`call:getAllPermission res` , 'info', currentFileName);
                    req.user = u;
                    next();
                  });
                }
              } else {
              //Chandni : Code start for Swagger
              //Below code is written to verfiy that request came from swagger
                if(data && data.user && data.user.usersUUID && (data.user.usersUUID == u.swaggerApiKey)){
                  logger.log(`if(data.user && data.user.usersUUID && (data.user.usersUUID == u.swaggerApiKey)){` , 'info', currentFileName);

                  logger.log(`call:getAllPermission before` , 'info', currentFileName);
                  getAllPermission(u,function(er,u){
                    logger.log(`call:getAllPermission error` , 'error', currentFileName);

                    if(er){
                      next(er);
                    }
                    logger.log(`call:getAllPermission res` , 'info', currentFileName);
                    req.user = u;
                    next();
                  });
                }
                //Chandni : Code end for Swagger
                res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session has been ended by ' + u.forceRemovedUser  });
                return;
              }
            }else if(token.tokenType === "App"){
              logger.log(`else if(token.tokenType === "App"){` , 'info', currentFileName);
               //here we have to handle permission for outside users. - to do-
            //for now we are maintaing the all selectedModules and building a user according to behalf of that user
            //so give defalut permission and bind to that user and , no need to check sites permission as discussed with @umesh.s
            // need to fetch App first
            logger.log("collection:appAccessConfig before fetch" , 'info', currentFileName);
            db.appAccessConfig.findOne({token : token.token},function(err,appAccessConfiguration){
              if(err){
                logger.log("collection:appAccessConfig error : " + err , 'error', currentFileName);
                next(err);
              }
              logger.log("collection:appAccessConfig fetch" , 'info', currentFileName);
              if(!appAccessConfiguration || !appAccessConfiguration.isActive){
                res.status(401).json({message: 'Token associted with Invalid App.'});
              }
              var schemaId = db.ObjectID(appAccessConfiguration.app._id);

              logger.log("collection:schema before fetch" , 'info', currentFileName);
              db.schema.findOne({_id : schemaId},function(err,sch){
                if(err){
                  logger.log("collection:schema error" , 'error', currentFileName);
                  next(err);
                }

                logger.log("collection:schema fetch" , 'info', currentFileName);
                if(!sch || !sch.isActive){
                  logger.log("if(!sch || !sch.isActive){" , 'info', currentFileName);
                  res.status(401).json({message: 'Token associted with Invalid App.'});
                }
                oAppPermission.buildPermission(u,sch);
                u.authType = "App";
                req.user = u;
                next();
              });
            });
            }
            else{
              //why that's happened on behalf of user but not type is there.
              next();
            }
          });
        }else{
          logger.log("if(!u || !u.isActive){" , 'info', currentFileName);

         //here we will maintain ApiToken - but that is in near future.
          next();
        }
        //now the user is valid;
      }
    });

   }
  else if(req.headers['authorization'] || req.query['authorization']){

    logger.log("else if(req.headers['authorization'] || req.query['authorization']){" , 'info', currentFileName);

     var decompressToken = lzstring.decompressFromEncodedURIComponent(req.headers['authorization']);
     var token = decompressToken;
     if(token && _.startsWith(token,'Bearer ')) {
       logger.log("if(token && _.startsWith(token,'Bearer ')) {" , 'info', currentFileName);

          logger.log("cal:decode before" , 'info', currentFileName);
           decode(token.substring(7), function (err, data) {

               if (err) {
                  logger.log("cal:decode error : "+ err , 'error', currentFileName);
                   next(err);
                   return;
               }
               logger.log("cal:decode res" , 'info', currentFileName);
               console.log(JSON.stringify(data));
               debug('token belongs to ' + data.user.username);

               logger.log("collection:users before fetch" , 'info', currentFileName);
               db['users'].findOne({ _id: db.ObjectID(data.user._id) }, function (e, u) {
                   if (e) {
                      logger.log("collection:users error : " + e , 'error', currentFileName);
                      return;
                   }
                   logger.log("collection:users fetch" , 'info', currentFileName);
                   if (u) {
                     logger.log("if (u) {" , 'info', currentFileName);
                       //if (u.usersUUID == data.user.usersUUID) {
                           //|| req.baseUrl == '/settings'
                           if (req.query && (req.query.pageName == "conCurrentUser" ) && u.isConCurrentUserAdmin && !u.isConCurrentUserActive) {

                              logger.log(`if (req.query && (req.query.pageName == "conCurrentUser" ) && u.isConCurrentUserAdmin && !u.isConCurrentUserActive) {` , 'info', currentFileName);
                               req.user = data.user;
                               //db.users.update({_id: u._id}, {$set: {isConCurrentUserActive: true}});
                               next();
                               return;
                           }
                           else if (!u.isConCurrentUserActive) {

                              logger.log(`else if (!u.isConCurrentUserActive) {` , 'info', currentFileName);


                               //db.users.update({_id: u._id}, {$set: {isConCurrentUserActive: true}});
                               res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session has been ended by ' + u.forceRemovedUser });
                               //next();
                               return;
                           }
                           else {

                             logger.log(`else of if (req.query && (req.query.pageName == "conCurrentUser" ) && u.isConCurrentUserAdmin && !u.isConCurrentUserActive) {` , 'info', currentFileName);
                               req.user = data.user;
                               next();
                               return;
                           }

                       //}
                      //  else {
                      //    console.log("data.user========");
                      //    console.log(data.user);
                      //    console.log("u----=-------");
                      //    console.log(u);
                      //  //Chandni : Code start for Swagger
                      //  //Below code is written to verfiy that request came from swagger
                      //    if(data.user && data.user.usersUUID
                      //       && (data.user.usersUUID == u.swaggerApiKey)
                      //      ){
                      //      req.user = data.user;
                      //      next();
                      //      return;
                      //    }
                      //    //Chandni : Code end for Swagger
                       //
                      //      res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session has been ended by ' + u.forceRemovedUser  });
                      //      //next();
                      //      return;
                      //  }
                   }else {
                     logger.log("else of if (u) {" , 'info', currentFileName);
                     next();
                   }
               });
           });
       }
     else {
       res.status(401).json({message: 'Need to pass bearer token authorization header'});
     }
  } else {
    logger.log("else of if(xToken){" , 'info', currentFileName);
    res.status(401).json({message: 'Need to pass bearer token authorization header'});

  }

  // var decompressToken = lzstring.decompressFromEncodedURIComponent(req.headers['authorization']);
  // var token = decompressToken;
  // if(token && _.startsWith(token,'Bearer ')) {
  //       decode(token.substring(7), function (err, data) {
  //           if (err) {
  //               next(err);
  //               return;
  //           }

  //           debug('token belongs to ' + data.user.username);
  //           db['users'].findOne({ _id: db.ObjectID(data.user._id) }, function (e, u) {
  //               if (e) {
  //                   return;
  //               }
  //               if (u) {
  //                   if (u.usersUUID == data.user.usersUUID) {
  //                       //|| req.baseUrl == '/settings'
  //                       if (req.query && (req.query.pageName == "conCurrentUser" ) && u.isConCurrentUserAdmin && !u.isConCurrentUserActive) {
  //                           req.user = data.user;
  //                           //db.users.update({_id: u._id}, {$set: {isConCurrentUserActive: true}});
  //                           next();
  //                           return;
  //                       }
  //                       else if (!u.isConCurrentUserActive) {
  //                           //db.users.update({_id: u._id}, {$set: {isConCurrentUserActive: true}});
  //                           res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session has been ended by ' + u.forceRemovedUser });
  //                           //next();
  //                           return;
  //                       }
  //                       else {
  //                           req.user = data.user;
  //                           next();
  //                           return;
  //                       }

  //                   } else {
  //                   //Chandni : Code start for Swagger
  //                   //Below code is written to verfiy that request came from swagger
  //                     if(data.user && data.user.usersUUID
  //                        && (data.user.usersUUID == u.swaggerApiKey)
  //                       ){
  //                       req.user = data.user;
  //                       next();
  //                       return;
  //                     }
  //                     //Chandni : Code end for Swagger

  //                       res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session has been ended by ' + u.forceRemovedUser  });
  //                       //next();
  //                       return;
  //                   }
  //               }
  //           });
  //       });
  //   }
  // else {
  //   res.status(401).json({message: 'Need to pass bearer token authorization header'});
  // }
  logger.log("function: authenticate - end" , 'info', currentFileName);
}

function authorize(entityType, access, req, res, next) {
  logger.log("function: authorize - start" , 'info', currentFileName);
  //by surjeet.b@productivet.com
  //variable allowLimitedAccess is used to allow default permission to all user..
  //so here by passing security question, change password, eSign PIN, My Profile API..
    var allowLimitedAccess = false;
    var allowSetting = false;
  if((!_.isUndefined(req.body.securityQuestion) && !_.isUndefined(req.body.securityAnswer)) ||
  (!_.isUndefined(req.body.password) && !_.isUndefined(req.body.newPassword)) ||
  (!_.isUndefined(req.body.currentVerificationCode) && !_.isUndefined(req.body.verificationCode)) ||
  ((!_.isUndefined(req.body.firstname) && !_.isUndefined(req.body.lastname)) || (!_.isUndefined(req.body.firstname) && !_.isUndefined(req.body.lastname) && !_.isUndefined(req.body.email) && !_.isUndefined(req.body.newEmail)))
  && _.isUndefined(req.body.roles)){
    logger.log(`if((!_.isUndefined(req.body.securityQuestion) && !_.isUndefined(req.body.securityAnswer)) ||
        (!_.isUndefined(req.body.password) && !_.isUndefined(req.body.newPassword)) ||
        (!_.isUndefined(req.body.currentVerificationCode) && !_.isUndefined(req.body.verificationCode)) ||
        ((!_.isUndefined(req.body.firstname) && !_.isUndefined(req.body.lastname)) || ((!_.isUndefined(req.body.firstname) && !_.isUndefined(req.body.lastname) && !_.isUndefined(req.body.email) && !_.isUndefined(req.body.newEmail))))
        && _.isUndefined(req.body.roles)){` , 'info', currentFileName);
    allowLimitedAccess = true;
    }
    if (req.baseUrl == '/generalSettings' && req.method == 'GET') {
      logger.log(`if (req.baseUrl == '/generalSettings' && req.method == 'GET') {` , 'info', currentFileName);
        allowSetting = true;
    }

    if (req.baseUrl == '/reportSettings') {
        allowSetting = true;
    }

    if (req.baseUrl == '/appEntityConfigEntity') {
        allowSetting = true;
    }

    if (req.baseUrl == '/customReport') {
        allowSetting = true;
    }

    if (req.baseUrl == '/versions') {
      logger.log(`if (req.baseUrl == '/versions') {` , 'info', currentFileName);
        allowSetting = true;
    }
    if (req.baseUrl == '/batchentrysettings') {
      logger.log(`if (req.baseUrl == '/batchentrysettings') {` , 'info', currentFileName);
        allowSetting = true;
    }
    if (req.baseUrl == '/assignment') {
      logger.log(`if (req.baseUrl == '/assignment') {` , 'info', currentFileName);
        allowSetting = true;
    }
    if (req.baseUrl == '/linkedTbl') {
      logger.log(`if (req.baseUrl == '/linkedTbl') {` , 'info', currentFileName);
        allowSetting = true;
    }
    if (req.baseUrl == '/mappedLinkToEntity') {
      logger.log(`if (req.baseUrl == '/mappedLinkToEntity') {` , 'info', currentFileName);
        allowSetting = true;
    }
    //akashdeep.s - QC3-4428 - added role-schema-update to bypass at patch
    if (req.baseUrl == '/roles' && (req.method == "POST" || req.method == "PATCH") && req.headers['role-schema-update'] == 'true') {
      logger.log(`if (req.baseUrl == '/roles' && (req.method == "POST" || req.method == "PATCH") && req.headers['role-schema-update'] == 'true') {` , 'info', currentFileName);
      console.log("role-schema-update" + req.headers['role-schema-update']);
      allowSetting = true;
    }
    if (req.baseUrl == '/') {
      logger.log(`if (req.baseUrl == '/') {` , 'info', currentFileName);
        allowSetting = true;
    }

    if (req.baseUrl == '/test-modules') {
      logger.log(`if (req.baseUrl == '/test-modules') {` , 'info', currentFileName);
        allowSetting = true;
    }

    if (req.baseUrl == '/batchentrysettings') {
        allowSetting = true;
    }
    if (req.baseUrl == '/windowQueueFiles') {
      allowSetting = true;
    }
    if (req.baseUrl == '/windowReport') {
      allowSetting = true;
    }
    // Speacail case written for entity table permission by jaydipsinh
    if (req.baseUrl.toLowerCase().startsWith('/entityrecords/')) {
      logger.log(`if (req.baseUrl.toLowerCase().startsWith('/entityrecords/')) {` , 'info', currentFileName);
        allowSetting = true;
    }

    //bi -temple
   if (req.baseUrl == '/reportTemplates') {
       allowSetting = true;
   }


  debug('Requesting ' + access + ' access to ' + entityType.underline);
  if(req.user){
    logger.log(`if(req.user){` , 'info', currentFileName);

    var allowed = _.filter(req.user.permissions,function (permission) {
      return _.filter(permission, function (m, p) {
        return p === entityType && m[access] === '1';
      }).length > 0;
      //return permission.entity === entityType && permission[access];
    }).length;
    //check for user id is string than only convert to ObjectId
    var uid = req.user._id;
    if(typeof uid === 'string'){
      logger.log(`if(typeof uid === 'string'){` , 'info', currentFileName);
      uid = db.ObjectID(req.user._id);
    }

    logger.log("collection:users before fetch" , 'info', currentFileName);
    db['users'].findOne({ _id:  uid}, function (e, u) {
        if (e) {
          logger.log("collection:users error : "+e , 'error', currentFileName);
          return;
        }
        logger.log("collection:users fetch" , 'info', currentFileName);
        if (u) {
          logger.log("if (u) {" , 'info', currentFileName);
            if (req.user.authType === 'App') {
              logger.log("if (req.user.authType === 'App') {" , 'info', currentFileName);

                //BY Miral to solve 8254
                /**
                 * so this bug says Activity information is no visible when new entry done from shared app outside the app
                 * Problem was Unauthorized access
                 *
                 * #entityType will have list of modules like qcentryactivity
                 * #allowed will check if user has access to given module
                 * in our case user would never have access to "qcentryactivity" because it is not in the list of permission screen
                 * when #entityType will have "qcentryactivity" #allowed will alwas be false and
                 * condition will go in else and "You are not authorized to access this resource" will be shown
                 * so as exception we have added check for "qcentryactivity" in below if condition if #entityType has "qcentryactivity"
                 */
                if (allowed || allowLimitedAccess || allowSetting || entityType.indexOf("qcentryactivity")!=-1 ) {
                  logger.log("if (allowed || allowLimitedAccess || allowSetting) {" , 'info', currentFileName);
                    next();
                    return;
                } else {
                  console.log('FAT GAYA')
                  logger.log("else of if (allowed || allowLimitedAccess || allowSetting) {" , 'info', currentFileName);
                    res.status(403).json({ message: 'You are not authorized to access this resource' });
                }
            } else {
                logger.log("else of if (req.user.authType === 'App') {" , 'info', currentFileName);
                if (!u.isConCurrentUserAdmin && !u.isConCurrentUserActive) {
                  logger.log("if (!u.isConCurrentUserAdmin && !u.isConCurrentUserActive) {" , 'info', currentFileName);
                    //Chandni : Code start for Swagger
                    //Below code is written to verfiy that request came from swagger

                    if (u.swaggerApiKey && req.user && req.user.usersUUID
                        && (req.user.usersUUID == u.swaggerApiKey)
                    ) {
                      logger.log(`if (u.swaggerApiKey && req.user && req.user.usersUUID
                                  && (req.user.usersUUID == u.swaggerApiKey)
                              ) {` , 'info', currentFileName);

                        next();
                        return;
                    }
                    //Chandni : Code end for Swagger
                    res.status(403).json({ code: 403.1, message: 'We regret to inform you that your session has been ended by ' + u.forceRemovedUser });
                }
                else {
                  logger.log("else of if (!u.isConCurrentUserAdmin && !u.isConCurrentUserActive) {" , 'info', currentFileName);
                     if (allowed || allowLimitedAccess || allowSetting) {
                       logger.log("if (allowed || allowLimitedAccess || allowSetting) {" , 'info', currentFileName);
                        next();
                        return;
                    }
                    else {
                      console.log('Yaha fata')
                        logger.log("else of if (allowed || allowLimitedAccess || allowSetting) {" , 'info', currentFileName);
                        res.status(403).json({ message: 'You are not authorized to access this resource' });
                    }
                }
            }
        }
    });
  } else {
    logger.log(`else of if(req.user){` , 'info', currentFileName);
    res.status(403).json({message: 'You are not authorized to access this resource'});
  }
  logger.log("function: authorize - end" , 'info', currentFileName);
}

module.exports = function (entityType) {
  return (function (entityType, access) {
    if(access === 'anonymous'){
      logger.log(`if(access === 'anonymous'){` , 'info', currentFileName);
      return function (req, res, next) {
        req.user = {
          _id: '000000000000000000000000',
          title: 'Anonymous'
        }
        next();
        return;
      }
    }
    function accessCode(access) {
      switch (access) {
        case 'view':
        return 0;
        case 'edit':
        return 1;
        case 'create':
        return 2;
        case 'editSchema':
        return 3;
        case 'review':
        return 4;
        case 'assignment':
        return 5;
        default:
        return -1;
      }
    }
    return [authenticate,authorize.bind(authorize, entityType, accessCode(access))];
  }).bind(this,entityType);
}

module.exports.authenticate = authenticate;