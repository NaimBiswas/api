'use strict'
/**
* @name oauth
* @author Dave Amit <dave.a@productivet.com>
*
* @version 0.0.0
*/
var oauthserver = require('oauth2-server');
var model =require('./model');
var oauth = oauthserver({
  model: model,
  grants: ['password'],
  debug: true,
  accessTokenLifetime: null
});

var currentFileName = __filename;
var logger = require('../../logger');

/**
* Configuration routine
*
* @param {string} - name of the template, Conventionally its name of the
*        folder, in it there must be two files html.hbs (Body) and text.hbs
*        (Subject).
*        REF: http://handlebarsjs.com/
* @param {Object} - Model to bind to the template
* @param {Function} - Standard Callback
* @return {Object} - Object with two properties, "text" and "html"
*
* @private
*/
function configure (app) {
  logger.log("function: configure - start" , 'info', currentFileName);
    app.all('/oauth/token', 
      oauth.grant()
    );

  app.get('/oauth/me', oauth.authorise(), function (req, res) {
    res.json(req.user);
  });

  logger.log("function: configure - end with return something" , 'info', currentFileName);
  return {
    registerErrorHandler: function () {
      app.use(oauth.errorHandler());
    }
  };
}


module.exports = {
  pk: model.pub,
  configure: configure,
  authorize: oauth.authorise()
};
