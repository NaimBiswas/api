'use strict'
/**
* @name oauth
* @author Dave Amit <dave.a@productivet.com>
*
* @version 0.0.0
*/

var debug = require('../logger').debug('lib:oauth');
var registration = require('../registration');
var jwt = require('jwt-simple');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var db = require('../db');
var lzstring = require('lz-string');
var uuid = require('node-uuid');
var log = require('../../logger'); 
var tokenUtility = require('../../lib/utilities/tokenUtilities');


var currentFileName = __filename;
var logger = require('../../logger');

var pk = fs.readFileSync(path.join(__dirname, '..','..','keys','development.pem'));
var pub = fs.readFileSync(path.join(__dirname, '..','..','keys','development.public.pem'));

debug('Server running with pk / pub');
debug(pk);
debug(pub);

//
// oauth2-server cbs
//

function getAccessToken (bearerToken, cb) {

  logger.log("function: getAccessToken - start" , 'info', currentFileName);

  //  debug('in getAccessToken (bearerToken: ' + bearerToken + ')');
  //var decompressToken = lzstring.decompressFromEncodedURIComponent(bearerToken);
  var payload =  jwt.decode(bearerToken, pub, false, 'RS256');
  //var payload =  jwt.decode(decompressToken, pub, false, 'RS256');

  
  if(payload) {
    logger.log("if(payload) {" , 'info', currentFileName);
    cb(null, {
      user: payload,
      expires: null
    });

  } else {
    logger.log("else of if(payload) {" , 'info', currentFileName);
    cb(err,null);
  }

  logger.log("function: getAccessToken - end" , 'info', currentFileName);
}


function getClient (clientId, clientSecret, cb) {
  logger.log("function: getClient - start" , 'info', currentFileName);
  debug('in getClient (clientId: ' + clientId + ', clientSecret: <RETRACTED>)');
  cb(null,{clientId: 1});
  logger.log("function: getClient - end" , 'info', currentFileName);
};

function grantTypeAllowed (clientId, grantType, cb) {
  logger.log("function: grantTypeAllowed - start" , 'info', currentFileName);
  debug('in grantTypeAllowed (clientId: ' + clientId + ', grantType: ' + grantType + ')');

  if (grantType === 'password') {
    return cb(false, true);
  }

  cb(false, false);
  logger.log("function: grantTypeAllowed - end" , 'info', currentFileName);
};

function generateToken(type, req, cb) {
  logger.log("function: generateToken - start" , 'info', currentFileName);
  if(type == 'accessToken') {
    logger.log("if(type == 'accessToken') {" , 'info', currentFileName);
    req.user.usersUUID = uuid.v1();
  }

  //console.log(type)

  var rolesToFetch = _.map(req.user.roles, function (role) {
    return role.title;
  });

  // reduce site object to its _id (omiting name)
  req.user.applicableSites = _.map(req.user.applicableSites,function (lvl1) {
    return _.map(lvl1, function (site) {
      return site._id;
    });
  });

    if (!req.user.isConCurrentUserActive && !_.isUndefined(req.user.isConCurrentUserActive) || type == 'roles') {
      logger.log(`if (!req.user.isConCurrentUserActive && !_.isUndefined(req.user.isConCurrentUserActive) || type == 'roles') {` , 'info', currentFileName);

        logger.log("collection:modules before fetch" , 'info', currentFileName);
        db.modules.find({}, function (er, modls) {
          logger.log("collection:modules fetch" , 'info', currentFileName);
            modls.toArray(function (er, mArr) {
                logger.log("collection:roles before fetch" , 'info', currentFileName);
                db.roles.find({ title: { $in : rolesToFetch } }, function (e, data) {
                    logger.log("collection:roles fetch" , 'info', currentFileName);
                    data.toArray(function (e, ary) {
                        // Set up permissions in a coded way,
                        req.user.permissions = _.reduce(ary, function (a, role) {
                            _.forEach(role.modules, function (module) {
                                    a[module._id] = a[module._id] || {};
                                    _.forEach(module.permissions, function (p) {
                                        //Generate code
                                        var per =
                                        (p.view ? '1' : '0') // view
                                      + (p.edit ? '1' : '0')
                                      + (p.create ? '1' : '0')
                                      + (p.editSchema ? '1' : '0')
                                      + (p.review ? '1': '0')
                                      + (p.assignment ? '1' : '0'); //for qualificationPerformance by Yamuna
                                        if (per.indexOf('1') !== -1) {
                                          logger.log("if (per.indexOf('1') !== -1) {" , 'info', currentFileName);
                                            if (a[module._id][p.entity]) {
                                                logger.log("if (a[module._id][p.entity]) {" , 'info', currentFileName);
                                                var p2 = a[module._id][p.entity];
                                                per = _.map(per, function (v, i) {
                                                    //If either of permissions are 1, then return 1 otherwise 0
                                                    // doing a logical or
                                                    //Shorter version
                                                    return (parseInt(per[i]) || parseInt(p2[i])).toString();
                                                    //Longer version
                                                    //(per[i] === '1') ? '1' : ((p2[i] === '1') ? '1' : '0');
                                                }).join('');
                                            };
                                            a[module._id][p.entity] = per;
                                        } else {
                                          logger.log("else of if (per.indexOf('1') !== -1) {" , 'info', currentFileName);
                                         //log.log('Not Found :: ' + per);
                                        }
                                    });
                            });
                            return a;
                        }, {});

                        delete req.user.permissions[undefined];
                        _.forEach(req.user.permissions, function (per,key) {
                            var indx = _.findIndex(mArr, function (mr) {
                                return mr.isActive && mr._id == key;
                            });
                            if (indx == -1) {
                                logger.log("if (indx == -1) {" , 'info', currentFileName);
                                delete req.user.permissions[key];
                            }
                        });


                        //delete req.user.roles;
                        var cdt = new Date();
                        var adToken = cdt.getTime() + '_' + req.user._id;
                        req.user.adToken = adToken;

                        var token = jwt.encode(req.user, pk, 'RS256');

                        var compressToken = lzstring.compressToEncodedURIComponent(token);

                        if (type == 'accessToken') {
                          logger.log("if (type == 'accessToken') {" , 'info', currentFileName);
                            db.users.update({ _id: req.user._id }, { $set: { usersUUID: req.user.usersUUID } });
                        }
                       //update or create token and add in db, currently keeping both token for roleback purpose.

                       logger.log("collection:allAuthToken before fetch" , 'info', currentFileName);
                       db.allAuthToken.update({userId : req.user._id},
                        {
                          token : adToken,
                          lastUsed : cdt, 
                          userId : req.user._id, 
                          usersUUID: req.user.usersUUID, 
                          tokenType : 'User',
                          globleExpirationTime:tokenUtility.updateToken(),
                          expirationTime:{
                            perfeqta:tokenUtility.updateToken()
                          }
                          },
                          {upsert : true},
                          function(err,tok){
                         if(err){
                           logger.log("collection:allAuthToken error" , 'error', currentFileName);
                           log.log(err);
                         }
                         logger.log("collection:allAuthToken fetch" , 'info', currentFileName);
                         //compressToken adToken- removing this token but will fetch from front end
                          cb(null,compressToken);
                       });
                    });
                });
            });
        });

    } else {
        logger.log(`else of if (!req.user.isConCurrentUserActive && !_.isUndefined(req.user.isConCurrentUserActive) || type == 'roles') {` , 'info', currentFileName);
        var token = jwt.encode(req.user, pk, 'RS256');

        var compressToken = lzstring.compressToEncodedURIComponent(token);
        if (req.user.code == 418) {
          logger.log(`if (req.user.code == 418) {` , 'info', currentFileName);
            cb(null, compressToken);
        }
        else {
          logger.log(`else of if (req.user.code == 418) {` , 'info', currentFileName);
            cb(null, null);
        }

  }

  // req.user.permissions = _.reduce(req.user.roles,function (a,c) {
  //   if(c.permissions && c.permissions.length){
  //     return _.union(a,c.permissions);;
  //   }else {
  //     return a;
  //   }
  // },[]);

  // delete req.user.roles;
  // var token = jwt.encode(req.user, pk, 'RS256');
  // cb(null,token);

  logger.log("function: generateToken - end" , 'info', currentFileName);
}

function setAuditLogs(username, req) {
  db['users'].findOne({'username' : username}, function (err, userDetail) {
    var record = _.cloneDeep(userDetail);
    // var record = _.cloneDeep(_.pick(userDetail, "_id", "modifiedBy", "modifiedByName", "modifiedDate", "ip"));
    record.modifiedDate = new Date();
    record.modifiedBy = userDetail._id+'';
    record.modifiedByName = userDetail.firstname+ ' ' +userDetail.lastname + ' ('+userDetail.username+')';
    db['users'].update({
      '_id' : db.ObjectID(record._id)
    }, {$set: {
      modifiedDate : record.modifiedDate,
      modifiedBy : record.modifiedBy,
      modifiedByName : record.modifiedByName
    }});
    record.version = userDetail.version;
    record.versions = userDetail.versions;
    record['isActive'] = 'false (User Inactivated by multiple failed login attempts)';
    db['users'].auditThis1Softly(record, record.ip);
  });
}

function saveAccessToken(token, clientId, expires, userId, cb) {
    logger.log("function: saveAccessToken - start" , 'info', currentFileName);
  //debug('in saveAccessToken (token: ' + token + ', clientId: ' + clientId + ', userId: ' + userId.email + ', expires: ' + expires + ')');
  cb(null,null);
  logger.log("function: saveAccessToken - end" , 'info', currentFileName);
};

/*
* Required to support password grant type
*/

function getUser(username, password, cb, req) {
  logger.log("function: getUser - start" , 'info', currentFileName);
  debug('in getUser (username: ' + username + ', password: <RETRACTED>)');
  var navigator = {};
 _.forEach(req.useragent, function(value,key){
    if(value != false){
      navigator[key] = value;
    }
  });
  req.useragent.navigatorInfoInString =  navigator.platform + ', ' + navigator.browser+ ', ' + navigator.version; 
  if (req.body.navigatorInformation) {
    req.body.navigatorInfoInString = req.body.navigatorInformation.platformName + ', ' + req.body.navigatorInformation.browserName+ ', ' + req.body.navigatorInformation.browserVersion;
  } else {
    req.body.navigatorInfoInString = req.useragent.navigatorInfoInString;
  }
  logger.log("collection:registration before fetch" , 'info', currentFileName);
  registration.login({username: username, password: password,
    ip : req.ip,hostname : req.hostname, navigatorInformation: navigator, navigatorInfoInString: req.useragent.navigatorInfoInString
  },function(e,user) {
    if(e){
      logger.log("collection:registration error : " + e , 'error', currentFileName);
      debug('Login failed for ' + username);
      e = null;
    }
    if (user && user.code == 400.1) {
      setAuditLogs(username);
    }
    logger.log("collection:registration fetch" , 'info', currentFileName);
    cb(e,user);
  },req.user);
  logger.log("function: getUser - end" , 'info', currentFileName);
};

/*
* Required to support refreshToken grant type
*/
function saveRefreshToken(token, clientId, expires, userId, cb) {
  //debug('in saveRefreshToken (token: ' + token + ', clientId: ' + clientId +', userId: ' + userId + ', expires: ' + expires + ')');

};

function getRefreshToken(refreshToken, cb) {
  //debug('in getRefreshToken (refreshToken: ' + refreshToken + ')');
};


module.exports = {
  pk: pk,
  pub: pub,
  getAccessToken: getAccessToken,
  getClient: getClient,
  grantTypeAllowed: grantTypeAllowed,
  generateToken: generateToken,
  saveAccessToken: saveAccessToken,
  getUser: getUser,
  saveRefreshToken: saveRefreshToken,
  getRefreshToken: getRefreshToken
};
