/*
    author - akashdeep.s - akashdeep.s@productivet.com - QC3-2093
    description : allow externalApp Permission of below listed only
                  all modules must be bind to App Module only.
*/
;(function(){
    'use strict';
    //position view-edit-create-editSchema-review
    //1 is for active
    //0 is for deactive
    var permissions = {
        sites : '10000',
        sets : '10000',
        questions : '10000',
        schema : '10000',
        generalSettings : '10000',
        siteNames : '10000',
        types : '10000',
        settings : '10000',
        pluginlist : '10000' //Vishesh API-437
    }
    var buildPermission = function(u,s){
        //u - user, s - schema, cb - callback
        var obj = {};
        obj[s.module._id] = permissions;
        //also provide entry permission
        obj[s.module._id][s._id] = "10100";
        u.permissions = obj;
    }
    module.exports = {
        permissions : permissions,
        buildPermission : buildPermission
    }
})()