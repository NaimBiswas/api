'use strict'
/**
* @name mailer
* @author Dave Amit <dave.a@productivet.com>
*
* @version 0.0.0
*/
module.exports = require('./mailer');
