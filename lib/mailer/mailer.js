'use strict'
/**
* @name mailer
* @author Dave Amit <dave.a@productivet.com>
*
* @version 0.0.0
*/

var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');
var debug = require('../logger').debug('lib:mailer');
var logger = require('../../logger');
var currentFileName = __filename;
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var _ = require('lodash');
var settingConfig = require('../config');
var db = require('../db');
var decode = require('decode-html')
// var transporter = nodemailer.createTransport({
//   service: 'zoho',
//   auth: {
//     user: 'qc3@daveamit.com',
//     pass: '!@#123qwe'
//   }
// });
var config = require('../../config/config.js');
var collectionCheck = require('../../lib/collectionCheck');
//logger.log('--------------------mailer.js--------------------');
//console.log(config);
logger.log('host: ' + config.mail.host,'debug');
logger.log('user: ' + config.mail.user,'debug');
logger.log('pass: ' + config.mail.pass,'debug');
logger.log('from: ' + config.mail.from,'debug');
logger.log('port: ' + config.mail.port,'debug');

/*var transporter = nodemailer.createTransport(smtpTransport({
host: 'smtp.gmail.com',
port: 587,
auth: {
user: 'protsystems123',
pass: 'Prot@123'
}
}));*/

var transporter = nodemailer.createTransport(smtpTransport({
  host: config.mail.host,
  port: config.mail.port,
  auth: {
    user: config.mail.user,
    pass: config.mail.pass
  }
}));


/**
* Renders a template into string
*
* @param {string} - name of the template, Conventionally its name of the
*        folder, in it there must be two files html.hbs (Body) and text.hbs
*        (Subject).
*        REF: http://handlebarsjs.com/
* @param {Object} - Model to bind to the template
* @param {Function} - Standard Callback
* @return {Object} - Object with two properties, "text" and "html"
*
* @private
*/
function render(template,model,cb) {
  logger.log('Function: render - Start', 'info', currentFileName);
  var templateDir = path.join(__dirname, 'templates', template);

  var newsletter = new EmailTemplate(templateDir)
  newsletter.render(model, function renderCallback(err, results) {
    if(cb){
      if(err){
        //logger.log('Function: renderCallback - Error', 'error', currentFileName);
        debug.err('Failed rendering templates');
        debug.err(err);
      }
      cb(err,results);
    }
  });

}

/**
* Sends the email
*
* @param  {String} - Email address of the recipient
* @param  {Object} - Object with two params, html and text.
* @param  {Function} - Standard Callback
* @return {Object} - Status.
*
* @private
*/
function sendmail(to,content,fileAttachments,template,cb,isSystem) {
  // by shivani - PQT-1961
  var file = ''
  if(fileAttachments && fileAttachments.length){
   file = [{path :path.basename(fileAttachments[0].path)}];
  }
   var obj={};
   if(template != 'system-error' && !isSystem){
 
     var mailDetails={
       from: config.mail.from,
       to: to,
       subject: content.text,
       html: content.html,
       attachments: file,
       priority: 'high',
       localStatus: 'Pending',
       mendrillTryCount:0,
       timestemp:new Date(),
       notifiedAdmin:false,
       mendrillStatus:'NA',
       localError : null,
       action : template,
       createdDate:new Date(),
       modifiedDate:new Date(),
       mailSentDate:new Date()
     }
     
 
     db.mailLogs.insert(mailDetails, function(err, insertedObj){
 
       if(insertedObj && insertedObj.ops && insertedObj.ops[0] && insertedObj.ops[0]._id){
        var mailId = insertedObj.ops[0]._id;
      
       } 
 
       transporter.sendMail({
         from: config.mail.from,  //do-not-reply@e.productivet.com
         to: to,
         subject: content.text,
         html: content.html,
         attachments: fileAttachments,
         priority: 'high',
         tags : [mailId]
       },function sendMailCallback(err,d){
         
         if(err){
           if(mailId){
            
             db.mailLogs.update({ _id: mailId }, { $set: { localStatus: 'Failed', localError:err} });
           }
           debug.err('Failed sending email to: ' + to);
           debug.err(err);
         } else{
           if(mailId){
          
             db.mailLogs.update({ _id: mailId }, { $set: { localStatus: 'Sent' } });
           }
         }
       //end : shivani PQT-1691
         if(cb){
           debug('Email sent with following detail:')
           debug(d);
           cb(err,d)
         }
       });
 
     })
   } else {
 
     transporter.sendMail({
       from: config.mail.from,  //do-not-reply@e.productivet.com
       to: to,
       subject: content.text,
       html: content.html,
       attachments: fileAttachments,
       priority: 'high'
     },function sendMailCallback(err,d){
 
       if(err){
         debug.err('Failed sending email to: ' + to);
         debug.err(err);
       }
     //end : shivani PQT-1691
       if(cb){
         debug('Email sent with following detail:')
         debug(d);
         cb(err,d)
       }
     });
 
   }
 
 }
 

/**
* Renders template and sends email to given recipient
*
* @param {string} - name of the template, Conventionally its name of the
*        folder, in it there must be two files html.hbs (Body) and text.hbs
*        (Subject).
*        REF: http://handlebarsjs.com/
* @param {Object} - Model to bind to the template
* @param {String} - Email address of the recipient
* @param {Function} - Standard Callback
* @return {Object} - Status
*
* @public
*/
function send (template,model,to,cb, isSystem) {
  var tempmodel;
 // logger.log('-----------------------------model 1-----------------------------');
  //logger.log(JSON.stringify(model.default));
  if (model && (!model.default || model.default == undefined || model.default == null || model.default == '' || Object.keys(model).length == 0 || Object.keys(model.default).length == 0 )) {
    logger.log('inside file: mailer.js - function: send - status: missing product information, now replacing with static config 1');
    tempmodel = settingConfig.config();
    model.default = {};
    model.default._id = tempmodel._id;
    model.default.to = to;
    model.default.title = tempmodel.title;
    model.default.company = tempmodel.company;
    model.default.product = tempmodel.product;
    model.default.support = tempmodel.support;
    model.default.web = tempmodel.web;
  }

  //logger.log('--------------------may be updated model-----------------------');
  //logger.log(JSON.stringify(model.default));

  if (!model || Object.keys(model).length == 0) {
    //logger.log('inside file: mailer.js - function: send - status: missing product information, now replacing with static config 2');
    tempmodel = settingConfig.config();
    model = {};
    model.default = {};
    model.default.to = to;
    model.default._id = tempmodel._id;
    model.default.title = tempmodel.title;
    model.default.company = tempmodel.company;
    model.default.product = tempmodel.product;
    model.default.support = tempmodel.support;
    model.default.web = tempmodel.web;
  }

  //logger.log('---------------------------------final model object-------------------------------');
  //logger.log(JSON.stringify(model.default));
    //hot fixed
    collectionCheck.collectionCheck('generalSettings');
    db.collection('generalSettings').find().toArrayAsync().then(function(data){
      logger.log('Collection : generalSettings - Success', 'info', currentFileName);
      if (data && data.length) {
        //logger.log('if(data && data.length) - Success', 'info', currentFileName);
        model.default.support = data[0].supportContactDetails || settingConfig.config().support;
      }
       model.default.support = model.default.support ? model.default.support : 'support@beperfeqta.com';
       //logger.log('---model.default.support---');
      // logger.log(model.default.support);
       debug('sending email (' + template + ') to ' + to + ' with');
       render(template,{ model: model },function renderCallback(e,templates) {
         //logger.log('Function : renderCallback- Success', 'info', currentFileName);
         debug('Rendered templates');
         //debug(templates);
         var fileAttachmentsT;
         if (!_.isUndefined(model.fileInfo) && model.fileInfo) {
           //logger.log('if (!_.isUndefined(model.fileInfo) && model.fileInfo)- Success', 'info', currentFileName);
           if (model.fileInfo.length > 0) {
             //logger.log('if (model.fileInfo.length > 0)- Success', 'info', currentFileName);
             fileAttachmentsT = model.fileInfo;
           }else {
             //logger.log('else - Success', 'info', currentFileName);
             fileAttachmentsT = [];
           }
         }
         sendmail(to,templates,fileAttachmentsT,template,cb,isSystem);
       });
    });
    //end hot fixed
}

function simple_independent(to, subject, message, cb){
  transporter.sendMail({
    from: config.mail.from,  //do-not-reply@e.productivet.com
    to: to,
    subject: content.text,
    priority: 'high'
  }, cb);
}

module.exports = {
  send: send,
  transporter: transporter,
  simple_independent: simple_independent
};
