'use strict'
var debug = require('./logger').debug('lib:query');
var _ = require('lodash');
var db = require('./db');
var logger = require('../logger');
var currentFileName = __filename;
var isOrQuery = false;
var optOrAnd;

function apply(options, entity, cb) {
  logger.log('Function: apply - Success', 'info', currentFileName);
  debug('Applying following options');
  //options.q.title.$exists = false;

  //Chandni Patel : Code Start : Added to serach using textScore to get exact match on top
  if (options.q && options.q.$text && options.q.$text.$search) {
    // Lokesh Boran : QC3-4984 Start
   entity = entity.find(JSON.parse(JSON.stringify(options.q)), { score: { $meta: "textScore" }});
   options.q.score = { $meta: "textScore" };
    // Lokesh Boran : QC3-4984 End
  } else {
    entity = entity.find(options.q || {});
  }
  //Code End

  //entity.find(function (err, data) {
  //    logger.log(err);
  //    logger.log(data);
  //});
  entity.count()
    .then(function paginateCountCallback(count) {
      var arg = {
        pageSize: options.pageSize,
        page: options.page,
        totalItems: count,
        totalPages: Math.ceil(count / options.pageSize)
      };

      options.augment = augment.bind(this, arg);
      //options.responseInfo = arg;

      options.res.setHeader('X-Page-Size', arg.pageSize);
      options.res.setHeader('X-Page', arg.page);
      options.res.setHeader('X-Total-Pages', arg.totalPages);
      options.res.setHeader('X-Total-Items', arg.totalItems);
      options.res.setHeader('Access-Control-Expose-Headers', 'X-Page-Size, X-Page, X-Total-Pages, X-Total-Items');

      cb(null, entity);
    })
    .catch(cb);



  if (options.sort) {
    // entity = entity.sort(options.sort);

    //Chandni Patel : Code Start : Added to sort using textScore to get exact match on top
    if (options.q && options.q.score) {
      //Start - Lokesh Boran : QC3-4567
      //akashdeep.s - QC3-5494 - QC3-5492 - QC3-5244 - updated so that search and sorting both work, but when user enter anything it will must not be sorted untill he manually sort.
      if (options.sort.length > 0 && options.sort[0][0] && options.sort[0][0] === "undefined")
        entity = entity.sort({ score: { $meta: "textScore" }, title: 1 });
      else
        entity = entity.sort({ score: { $meta: "textScore" }, title: 1 }).sort(options.sort);
      //akashdeep.s - QC3-5494 - QC3-5492 - QC3-5244
      //End - Lokesh Boran : QC3-4567
    } else {
        entity = entity.sort({ score: { $meta: "textScore" }, title: 1 }).sort(options.sort);
    }
    //end

  }

  if (options.select) {
    //Chandni Patel : Code Start : Added to serach using textScore to get exact match on top
    if (_.isUndefined(options.q) || _.isUndefined(options.q.score)) {
      entity = entity.project(options.select);
    }
    //end
  }

  if (options.skip) {
    entity = entity.skip(options.skip);
  }
  if (options.limit) {
    entity = entity.limit(options.limit);
  }


}

function mapSortFieldCallback(field) {
  logger.log('Function: mapSortFieldCallback - Called', 'info', currentFileName);
  if (field.charAt(0) === '-') {
    return [field.substr(1), -1];
  } else {
    return [field, 1];
  }
}

function augment(options, data) {
  logger.log('Function: augment - Called', 'info', currentFileName);
  options.data = data;
  return options;
}
function middleware(req, res, next) {
  logger.log('Function: middleware - Called', 'info', currentFileName);
  var pageSize = parseInt(req.query.pagesize || 10);
  var page = parseInt(req.query.page || 1);
  var options = {
    pageSize: pageSize,
    page: page,
    skip: pageSize * (page - 1),
    limit: pageSize,
  };
  options.apply = apply.bind(this, options);

  if (req.query.sort) {
    options.sort = _.map(req.query.sort.split(','), mapSortFieldCallback);
  }

  if (req.query.q) {
    if (typeof req.query.q === 'string') {
      options.q = { $text: { $search: req.query.q, $language: 'none' } };
    } else {
      options.q = req.query.q;
      _.forEach(options.q, function (value, key, c) {
        //See if its an in query.
        var arrQuery;
        if(value && typeof value === 'string' && value.startsWith('$')){
          //logger.log('Here for normal search 2  :: ' + value);
          //logger.log('Key :: ' + key);
          //unset the key.
          delete options.q[key];
          //Split value to extract meaning.
          //every record will have 3 values, at0: opeator, at1: datatype
          //at2:value.
          var v = value.split('|');
          //Iterate through all parts.
          //by surjeet.b@productivet.com.. here if condition placed for $not query passed from front-end..
          var tempV = [];
          var optionsQuery;
          if (_.includes(v, '$not')) {
            _.forEach(v, function (v, index) {
              if (index !== 0) {
                tempV.push(v);
              }
            });
            _.forEach(tempV, function (value, index) {
              if ((index + 1) % 3 == 0) {
                (options.q[key] = options.q[key] || {})
                [tempV[index - 2]] = parse(tempV[index - 1], value);
              }
            });
            options.q[key] = {
              '$not': options.q[key]
            }
          }else if (_.includes(v, '$or1') || _.includes(v, '$and1')) {
            //by surjeet.b... this is generic $or && $and query...
            //pass query like: q[username,firstname] = $or1|$in|a|surjeet,yuvraj|$in|a|ankita,anamika from the front-end
            //hence username = $in|a|surjeet,yuvraj and firstname = $in|a|ankita,anamika
            //means fetch the records which have username surjeet,yuvraj OR fetch the records which have firstname ankita,anamika
            //so the query will be like: {$or: [{username: {$in: [surjeet,yuvraj]}}, {firstname: {$in: [ankita,anamika]}}]}
            optOrAnd = (v[0] == '$or1') ? '$or' : ((v[0] == '$and1') ? '$and' : '');
            var expressions = [];
            if (!optOrAnd || _.isUndefined(optOrAnd)) {
              optOrAnd = '$or';
            }
            tempV = _.filter(v, function (v, index) {
              return index !=0;
            });

            _.forEach(tempV,function (value, index) {
              if((index + 1) % 3 == 0){
                (options.q[key] = options.q[key] || {})
                [tempV[index - 2]] = parse(tempV[index - 1],value);
                expressions.push(_.cloneDeep(options.q[key]));
              }
            });
            optionsQuery = {
              [optOrAnd]: []
            }
            var keys = key.split(',');
            _.forEach(keys, function (key, index) {
              optionsQuery[optOrAnd].push({[key]: expressions[index]});
            })
            options.q = optionsQuery;
          }else if (_.includes(v, '$or') || _.includes(v, '$and')) {
            //this is specific for entity filter screen
            optOrAnd = v[0];
            if (!optOrAnd || _.isUndefined(optOrAnd)) {
              optOrAnd = '$or';
            }

            _.forEach(v, function (v, index) {
              if (index !== 0) {
                tempV.push(v);
              }
            });
            _.forEach(tempV, function (value, index) {
              if ((index + 1) % 3 == 0) {
                (options.q[key] = options.q[key] || {})
                [tempV[index - 2]] = parse(tempV[index - 1], value);
              }
            });
            var keepOptions = _.cloneDeep(options.q[key]);
            optionsQuery = {
              [optOrAnd]: []
            }
            optionsQuery[optOrAnd].push({[key]: keepOptions});
            // optionsQuery[optOrAnd].push({[key]: keepOptions});
            if (optionsQuery) {
              optionsQuery[optOrAnd] = _.reduce(optionsQuery[optOrAnd], function (a, item, index) {
                var timepass = _.cloneDeep(item);
                var o = item[Object.keys(item)[0]];
                var v = o[Object.keys(o)[0]];
                var tempAppliedValue = [];
                _.forEach(v, function (value) {
                  if (index == 0) {
                      if (parseFloat(value)) {
                        tempAppliedValue.push(value);
                      }else {
                        tempAppliedValue.push(value.toString());
                      }
                  }else {
                    if (/^[0-9\-\+]*$/.test(value) == true) {
                      tempAppliedValue.push(parseInt(value));
                    } else {
                      tempAppliedValue.push(value.toString());
                    }
                  }
                });
                var key1 = Object.keys(item)[0];
                var key2 = Object.keys(o)[0];
                timepass[key1][key2] = tempAppliedValue;
               a.push(timepass);
               return a;
             }, []);
            }
            options.q[key] = optionsQuery;
            isOrQuery = true;
          } else {
            _.forEach(v, function (value, index) {
              //Only enter if we are at value index
              //For this, it will enter for 2, 5, 8 etc.
              //v[index-2] = operator
              //v[index-1] = datatype
              //v[index] or value = <value>
              if ((index + 1) % 3 == 0) {
                (options.q[key] = options.q[key] || {})
                [v[index - 2]] = parse(v[index - 1], value);
              }
            });
          }
          function parse(dt, val) {
            logger.log('Function: parse - Called', 'info', currentFileName);
            //some help from surjeet...
            //use d for date, r for regex, n for number, t for text(String), and a for array
            var valArray;
            var keyArray;
            switch (dt) {
              case 'd':
                return new Date(val);
              case 'r':
                return { $regex: new RegExp(val.toLowerCase(), 'i') };
              case 'n':
                /* if (/^[0-9\-\+]*$/.test(val) == true) { // need to remove
                  val = parseInt(val);
                }
                if (/^[0-9 \.\-\+]*$/.test(val) == true) {
                  val = parseFloat(val);
                }
                return val; */
              case 't':
                if (/^[0-9\-\+]*$/.test(val) == true) {
                  val = parseInt(val);
                }
                if (/^[0-9 \.\-\+]*$/.test(val) == true) {
                  val = parseFloat(val);
                }
                return val;
              case 'e':
                //check existence of the key.. pass like {q['entries.1234567890']: $exists|e|true}
                return val && val == 'true' ? true : false;
                break;
              case 'a':
                valArray = val.split(',');
                keyArray = [];
                _.forEach(valArray, function (chkvalue) {
                  var isNotANumber = false;
                  //Either for string or int
                  keyArray.push(chkvalue);
                  if (/^[0-9\-\+]*$/.test(chkvalue) == true) {
                    chkvalue = parseInt(chkvalue);
                    isNotANumber = true;
                  }
                  if (/^[0-9 \.\-\+]*$/.test(chkvalue) == true) {
                    chkvalue = parseFloat(chkvalue);
                    isNotANumber = false;
                  }
                  //akashdeep.s - QC3-5780 - check for expontial value
                  if (/^[0-9e\.\-\+]*$/.test(chkvalue) == true) {
                    chkvalue = Number(chkvalue);
                    isNotANumber = false;
                  }
                  if (chkvalue.length == 24 && db.ObjectID.isValid(chkvalue)) {
                    chkvalue = db.ObjectID(chkvalue);
                    isNotANumber = false;
                  }

                  //commented by surjeet.b@productivet.com... for filter the record for the numeric value..
                  // if (req.baseUrl.startsWith('/entityRecords')) {
                  //   if(/^[0-9\-\+]*$/.test(chkvalue) == true){
                  //     // chkvalue = parseInt(chkvalue);
                  //     isNotANumber = false;
                  //   }
                  // }
                  if (isNotANumber) {
                    keyArray.push(new RegExp('.*' + chkvalue + '.*', 'i'));
                  } else {
                    keyArray.push(chkvalue);
                  }
                });
                return keyArray;
              case 'dstring':
                valArray = val.split(',');
                keyArray = [];
                _.forEach(valArray, function (chkvalue) {
                  keyArray.push(chkvalue);
                });
                return keyArray;
              default:
                return val;
            }
          }
        }

        else if(value && typeof value === 'string' && value.startsWith('|between|')) {
          var values = value.replace('|between|', '').split(',');
          options.q[key] = {
            $gte: new Date(values[0]),
            $lte: new Date(values[1])
          };
        }

        else if (key === '$text') {
          //logger.log('Here for normal search');
          options.q.$where = 'this';
          //Start: QC3-8020 - Bug.
          //Changed By: Mahammad
          //Description: Replace 'if (req.baseUrl == '/questions')' with this for serching by title and tags also with SINGLE CHARACTER.

          // if(req.baseUrl == '/attributes'){
          //   options.q.$where = 'this.title.toLowerCase().indexOf("' + options.q.$text.toLowerCase() + '") > -1 || this.type.title.indexOf("' + options.q.$text.toLowerCase() + '") > -1';
          //   delete options.q.$text;
          // }
          // else if(req.baseUrl == '/users'){
          //   options.q.$where = 'this.username.toLowerCase().indexOf("' + options.q.$text.toLowerCase() + '") > -1 || this.firstname.indexOf("' + options.q.$text.toLowerCase() + '") > -1 || this.lastname.indexOf("' + options.q.$text.toLowerCase() + '") > -1 || this.email.indexOf("' + options.q.$text.toLowerCase() + '") > -1 || this.newEmail.indexOf("' + options.q.$text.toLowerCase() + '") > -1';
          //   delete options.q.$text;
          // } else
          if (['/entities', '/questions', '/procedures'].indexOf(req.baseUrl) >= 0) {
            //req.options.search = 'this.title.indexOf("op") !== -1';
            //options.q.search = 'this.title.indexOf()'
            options.q.$where = 'this.title.toLowerCase().indexOf("' + options.q.$text.toLowerCase() + '") > -1 || this.tags.indexOf("' + options.q.$text.toLowerCase() + '") > -1';
            delete options.q.$text;
            // Lokesh Boran Task : QC3-4759 Start
          } else if (['/sites','/siteNames','/sets','/schema','/preSchema','/roles','/schedules','/masterqcsettings','/groupsettings','/plugin','/pluginConfig','/autoSync', '/windowReport'].indexOf(req.baseUrl)>=0) {
            // Lokesh Boran Task : QC3-4759 End
            //req.options.search = 'this.title.indexOf("op") !== -1';
            //options.q.search = 'this.title.indexOf()'
            options.q.$where = 'this.title.toLowerCase().indexOf("' + options.q.$text.toLowerCase() + '") > -1';
            delete options.q.$text;

          //End: QC3-8020 - Bug.

          }else if (['/reportSettings'].indexOf(req.baseUrl)>=0) {
            // Lokesh Boran Task : QC3-4759 End
            //req.options.search = 'this.title.indexOf("op") !== -1';
            //options.q.search = 'this.title.indexOf()'
            options.q.$where = 'this.viewName.toLowerCase().indexOf("' + options.q.$text.toLowerCase() + '") > -1';

            delete options.q.$text;
          }else if(['/appAccessConfig'].indexOf(req.baseUrl)>=0){
             options.q.$where = 'this.app.title.toLowerCase().indexOf("' + options.q.$text.toLowerCase() + '") > -1 || this.module.title.toLowerCase().indexOf("' + options.q.$text.toLowerCase() + '") > -1';
             delete options.q.$text;
          // QC3-10718 by Yamuna
          }else if(['/windowQueueFiles'].indexOf(req.baseUrl)>=0){
            options.q.$where = 'this.filename.toLowerCase().indexOf("' + options.q.$text.toLowerCase() + '") > -1';
            delete options.q.$text;
          }else {
            options.q.$text = { $search: options.q.$text };
          }
        }
        else if ('_id' === key && options.q[key].length == 24) {
          options.q[key] = db.ObjectID(options.q[key]);
        }
        //QC3-6731 --by bhavin --for get api convert key boolean value to stringtype
         else if (req.baseUrl == '/searchFavorites' && key === "systemSearch") {
            options.q[key] = options.q[key] === 'true';
         }
        //QC3-6731
        //Start:QC3-7555 -by bhavin for converting in objectID
        else if (req.baseUrl == '/searchFavorites' && key === "createdBy") {
          //  createdBy:{$in:['57f799ecc3fba72445038171',ObjectId('57f799ecc3fba72445038171')]}
            arrQuery = {
              $in: []
            }
          arrQuery['$in'].push(db.ObjectID(options.q[key]));
          arrQuery['$in'].push(options.q[key]+'');
          options.q[key] = arrQuery;
        }
        //End:QC3-7555
        else if ('isEntity' === key) {
          options.q[key] = options.q[key] === 'true';
        }
        else if ('isWindow' === key) {
          options.q[key] = options.q[key] === 'true';
        }
        else if ('isActive' === key) {
          options.q[key] = options.q[key] === 'true';
        }
        else if ('isVisible' === key) {
          options.q[key] = options.q[key] === 'true';
        }
        else if ('allowToSkip' === key) {
          options.q[key] = options.q[key] === 'true';
        }
        else if ('entityId' === key) {
          //options.q[key] = options.q[key] === 'true';
          options.q['entityId'] = db.ObjectID(options.q[key]);
        }
        else if ('discoverable' === key) {
          options.q[key] = options.q[key] === 'true';
        }
        else if ('isDeleted' === key) {
          options.q[key] = options.q[key] === 'true';
        }
        else if ('isRemoved' === key) {
          options.q[key] = options.q[key] === 'true';
        }
        else if ('ismasterQcForm' === key) {
          options.q[key] = options.q[key] === 'true';
        }
        else if ('toSync' === key) {
          options.q[key] = options.q[key] === 'true';
        }
        else if ('isShared' === key) {
          options.q[key] = options.q[key] === 'true';
        }
        else if ('allowSpecialSettings' === key) {
          options.q[key] = options.q[key] === 'true';
        }
        else if ('isSetReviewFrequency' === key) {
          options.q[key] = options.q[key] === 'true';
        }
        else if ('calendarschemaId' === key) {
          options.q['calendarschemaId'] = db.ObjectID(options.q[key]);
        }
        else if ('linkedEntityId' === key) {
          options.q['linkedEntityId'] = db.ObjectID(options.q[key]);
        }
        else if (key && (key).toString().startsWith('entityRecord')) {
          //By Miral to solve QC3-8293>issue2
          var preExp = (typeof value !== 'object') ? (value ? value.substring(0,1) : "" )  :"";
          if(preExp=="=" && value){
            value = value.substring(1,value.length);
          }

          if (/^[0-9\-\+]*$/.test(value) == true) {
            /**
             * BY Miral
             * to solve entity record search related bugs
             * QC3-8098
             */
            arrQuery = [];
            arrQuery.push(parseInt(value));
            arrQuery.push(parseFloat(value));
            arrQuery.push(value + '');
            options.q[key] = {"$in":arrQuery};
          } else if((/^(\d+\.?\d{0,25}|\.\d{0,25})$/).test(value) == true) {
            arrQuery = [];
            arrQuery.push(parseFloat(value));
            arrQuery.push(value + '');
            options.q[key] = { "$in": arrQuery };
          } else if(preExp=="=") { //QC3-9754 + QC3-9734 - By Miral to solve QC3-8293>issue2
            options.q[key] = value;
          } else if (value instanceof RegExp) { //QC3-8990 Prachi - by surjeet.b@productivet.com - removed !(not) condition as it was wrong
              // QC3-11186 - surjeet.b@productive.com
              // Special case for special characters, in case insensitive query (,),$ etc.. keyword does not return the expected result
              options.q[key] = new RegExp((value).replace(/[-[\]{}()*+?.,\\/^$|#\s]/g, "\\$&"), 'i');
            }
        }
        else if (key && key.startsWith('reportTemplates')) {
          if (/^[0-9\-\+]*$/.test(value) == true) {
            arrQuery = {
              $in: []
            }
            arrQuery['$in'].push(parseInt(value));
            arrQuery['$in'].push(parseFloat(value));
            arrQuery['$in'].push(new RegExp(value, 'i'));
            arrQuery['$in'].push(value + '');
            options.q[key] = arrQuery;
          } else if (value && value.length === 24 && value.charAt(23) === 'Z' && value.charAt(10) === 'T' && value.charAt(19) === '.') {
            options.q[key] = new RegExp(value, 'i');
          }
          else {
            options.q[key] = new RegExp(value, 'i');
          }
        }
        else if (req.baseUrl == '/reportTemplates') {

            options.q[key] = new RegExp(value, 'i');
        }
        //QC3-7953: by mahammad
        //check for wehter the current entity attributes are availabe or not
        else if (key === 'exists') {


          var valArray = value.split(',');
          var keyArray = valArray.map(field => ({
              ['entityRecord.' + field]: {$exists: true}
          }));


          /**
           * BY Miral
           * to solve entity record search related bugs
           * QC3-8098
           */

          if(options.q['$or']){
            options.q["$and"] = options.q["$and"]?options.q["$and"]:[];
            options.q["$and"].push({"$or":keyArray})
          }
          else{
            options.q['$or'] = keyArray
          }

          delete options.q["exists"];

          // options.q = {
          //   ['$or']: keyArray
          // };

        }
        /**
         * By MIral
         * hear we need to check if value in search is string or not
         * if it is string then we should check is INTEGER or FLOAT
         *
         * it was not reported bug
         */
        else if (typeof options.q[key] === 'string') {

          arrQuery = [];
          if (/^[0-9\-\+]*$/.test(options.q[key]) == true) {
            arrQuery.push(parseInt(options.q[key]));
          }
          else if (/^[0-9 \.\-\+]*$/.test(options.q[key]) == true) {
            arrQuery.push(parseFloat(options.q[key]));
          }
          arrQuery.push(value + '');
          if(value == 'true' || value == 'false'){
            arrQuery.push(value == 'true');
          }

          options.q[key] = {"$in":arrQuery};
        }


        //QC3-7953: end
        // else {
        //   options.q[key] = (typeof options.q[key] === 'string')? options.q[key].trim() : options.q[key];
        //   if (options.q[key] === ((typeof options.q[key] === 'string')? parseInt(options.q[key].trim()) : options.q[key] + '')) {
        //     //options.q[key] = parseInt(options.q[key]);
        //     var arrQuery = {
        //       $in: []
        //     }
        //     arrQuery['$in'].push(options.q[key] + '');
        //     arrQuery['$in'].push(parseInt(options.q[key]));
        //     options.q[key] = arrQuery;
        //   } else if (options.q[key] === (parseFloat(options.q[key]) + '')) {
        //     //options.q[key] = parseFloat(options.q[key]);
        //     var arrQuery = {
        //       $in: []
        //     }
        //     arrQuery['$in'].push(options.q[key] + '');
        //     arrQuery['$in'].push(parseFloat(options.q[key]));
        //     options.q[key] = arrQuery;
        //   }
        // }
        // Commented because child documents DO NOT have ObjectID
        // else if(_.endsWith(key,'_id') && options.q[key].length == 24){
        //   options.q[key] = db.ObjectID(options.q[key]);
        // }
      });
    }
  }


  if (req.query.select) {
    options.select = {};
    _.forEach(req.query.select.split(','), function forEachCallback(field) {
      options.select[field] = 1;
    });
  }

  if(req.query.deselect){
    options.deselect = {};
    _.forEach(req.query.deselect.split(','), function forEachCallback(field) {
      options.deselect[field] = 0;
    });
  }
  
  options.res = res;
  req.paginateOptions = options;

  //console.log(req.paginateOptions.q);

  if (isOrQuery) {
    var modifiedOptions = {};
    for (var variable in options.q) {
      if (options.q.hasOwnProperty(variable)) {
        if (options.q[variable].hasOwnProperty('$or') || options.q[variable].hasOwnProperty('$and')) {
          modifiedOptions[Object.keys(options.q[variable])[0]] = options.q[variable][Object.keys(options.q[variable])[0]];
        } else {
          modifiedOptions[variable] = options.q[variable];
        }
      }
    }
    options.q = modifiedOptions;
    isOrQuery = false;
  }
  //console.log(JSON.stringify(options.q));
  next();
}

module.exports = {
  paginate: middleware
};
