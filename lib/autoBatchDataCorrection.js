/**
* @name autoBatchDataCorrection
* @author Surjeet Bhadauriya <surjeet.b@productivet.com>
*
* @version 0.0.0
*/

var colors = require('colors');
var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('./logger').configure(__dirname, enableBunyan);
require('../lib/promise-me');
var moment = require('moment');
var CronJob = require('cron').CronJob;
var mailer = require('./mailer');
var settings = require('./settings');
var batchEntryService = require('../lib/batchEntryService/index');
var log = require('../logger');
var config = require('../config/config.js');
var _ = require('lodash');
var log4js = require('log4js');
var db = require('./db');
var dateTime = '';
process.env.dateTime = ''
var notificationId = '';
var diffInHours = '';
var path = require('path');
var fs = require('fs-extra');

//Fix for autoBatchDataCorrection pm2 continues restart by Yamuna
var logDir = path.join(process.cwd(), 'loggerAutoBatchCorrectData');
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

log4js.configure({
  appenders: [
    {
      type: 'file',
      filename: './loggerAutoBatchCorrectData/autoBatchCorrectData.log',
      category: 'captureLog',
      backups: 100,
      maxLogSize: 20480
    }
  ]
});
var logger4js = log4js.getLogger('captureLog');

var job = new CronJob('0 */1 * * *', function() {
  init();
}, function () {
  log.log('Job Done..!!');
  /* This function is executed when the job stops */
},
true  /* , Start the job right now */
/* timeZone /* OPTIONAL Time zone of this job. */
);
// init();

function pad(n) {
  return n<10 ? '0'+n : n;
}

function getCreatedByName(users, userId){
  var foundUser = users.find(function(user){
    return user._id.toString() == userId;
  });
  return foundUser.firstname + ' ' + foundUser.lastname + ' (' + foundUser.username + ')';
}

function buildBatchEntriesToUpdate(entityToProcess, batchData, schema, entry, filteredBatchEntries){
  var tempFilteredBatchEntries = {};
  if(filteredBatchEntries.length){
    tempFilteredBatchEntries = Object.assign({},filteredBatchEntries[filteredBatchEntries.length - 1]);
    tempFilteredBatchEntries.formId = (entityToProcess.entryId||'').toString();
    tempFilteredBatchEntries.counter = filteredBatchEntries.length;
    tempFilteredBatchEntries.masterQCSettings = entry.masterQCSettings;
    tempFilteredBatchEntries.calendarIds = entityToProcess.calendarIds;
  }else{
    tempFilteredBatchEntries = {
      formId: (entityToProcess.entryId||'').toString(),
      copySiteSelections: [],
      copyNonKeyAttributes: [],
      copyNonKeyEntities: [],
      masterQCSettings: entry.masterQCSettings,
      calendarIds: entityToProcess.calendarIds,
      counter: filteredBatchEntries.length
    };
  }
  if (batchData) {
    var foundParallelBatchEntries =  batchData.batchEntries[entityToProcess.batchEntryId].find(function(data){
      return data.counter == filteredBatchEntries.length && data.masterQCSettings.schemaId != entityToProcess.masterQCSettings.schemaId;
    });
    if(foundParallelBatchEntries){
      tempFilteredBatchEntries.masterQCSettings.sequenceId = foundParallelBatchEntries.masterQCSettings.sequenceId;
      if(!filteredBatchEntries.length){
        tempFilteredBatchEntries.calendarIds = foundParallelBatchEntries.calendarIds;
      }
    }
  }
  return tempFilteredBatchEntries;
}

function buildBatchAttributesToUpdate(tempFilteredBatchEntries, entityToProcess, batchData, schema, entry, filteredBatchAttributesInfo, users){
  var addTitle = false;
  if(!filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId]){
    addTitle = true;
  }
  filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId] = filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId] || {};
  filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId].attributesTitle = filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId].attributesTitle || [];
  filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId].values = filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId].values || [];

  var attrObj = {};
  schema.attributes.forEach(function (attribute) {
    var attrValue;
    entry.entries[attribute['s#']] = entry.entries[attribute['s#']]||{};
    if (entry.entries[attribute['s#']].attributes) {
      attrObj[(attribute.title).replace('.','')] = entry.entries[attribute['s#']].attributes;
      if (attribute.type.format.title === 'Date') {
        attrValue = new Date(entry.entries[attribute['s#']].attributes);
        attrValue = attrValue.getMonth() + 1 + '/' + pad(attrValue.getDate()) + '/' + attrValue.getFullYear();
        attrObj[(attribute.title).replace('.','')] = attrValue;
      }
      if (attribute.type.format.title === 'Time') {
        attrValue = new Date(vm.entry.entries[attribute['s#']].attributes);
        attrValue = attrValue.getHours() + ':' + attrValue.getMinutes() + ':' + attrValue.getSeconds();
        attrObj[(attribute.title).replace('.','')] = attrValue;
      }
    } else {
      attrObj[(attribute.title).replace('.','')] = '';
    }
    if(addTitle){
      filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId].attributesTitle.push(attribute.title);
    }
  });

  schema.entities.forEach(function (entity) {
    entity.questions.forEach(function (question) {
      if (question.isKeyValue) {
        if (entry && entry.entries && entry.entries[question['s#'] + '-' + entity['s#']] && entry.entries[question['s#'] + '-' + entity['s#']][entity['s#']]) {
          attrObj[question['s#'] + '-' + entity['s#']] = entry.entries[question['s#'] + '-' + entity['s#']][entity['s#']];
          if(question && question.type && question.type.format && question.type.format.title == 'Number'){
            attrObj[question['s#'] + '-' + entity['s#']] = entry.entries[question['s#'] + '-' + entity['s#']][entity['s#']];
          }
          var entityValue;
          if (question.type.format.title === 'Date') {
            entityValue = new Date(entry.entries[question['s#'] + '-' + entity['s#']][entity['s#']]);
            entityValue = entityValue.getMonth() + 1 + '/' + pad(entityValue.getDate()) + '/' + entityValue.getFullYear();
            attrObj[question['s#'] + '-' + entity['s#']] = entityValue;
          }
          if (question.type.format.title === 'Time') {
            entityValue = new Date(entry.entries[question['s#'] + '-' + entity['s#']][entity['s#']]);
            entityValue = entityValue.getHours() + ':' + entityValue.getMinutes() + ':' + entityValue.getSeconds();
            attrObj[question['s#'] + '-' + entity['s#']] = entityValue;
          }
        } else {
          attrObj[question['s#'] + '-' + entity['s#']] = '';
        }
        if(addTitle){
          filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId].attributesTitle.push((question['s#'] + '-' + entity['s#']).toString());
        }
      }
    });
  });
  if(addTitle){
    filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId].attributesTitle.push('Created Date');
    filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId].attributesTitle.push('Created By');
  }

  attrObj.createdDate = (entry.modifiedDate.getMonth() + 1) + '/' + pad(entry.modifiedDate.getDate()) + '/' + entry.modifiedDate.getFullYear() + ' ' +
  pad(entry.modifiedDate.getHours()) + ':' + pad(entry.modifiedDate.getMinutes()) + ':' + pad(entry.modifiedDate.getSeconds());
  attrObj.createdBy = getCreatedByName(users, entry.modifiedBy);
  attrObj.masterQCSettingsId = tempFilteredBatchEntries.masterQCSettings.masterQCSettingsId;
  attrObj.sequenceId = tempFilteredBatchEntries.masterQCSettings.sequenceId;
  attrObj.schemaId = entityToProcess.masterQCSettings.schemaId;

  filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId].values = filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId].values || [];
  filteredBatchAttributesInfo[entityToProcess.masterQCSettings.schemaId].values.push(attrObj);
  return filteredBatchAttributesInfo;
}

function updateBatchAndEntry(entityToProcess, updatedBatchEntries, updatedBatchAttributesInfo, key, key1, query, cb, batchEntryId, batchNo) {
  var seqId = updatedBatchEntries[updatedBatchEntries.length - 1].masterQCSettings.sequenceId;
  var updateQuery = {'$set': {}};
  updateQuery['$set'][key] = updatedBatchEntries;
  updateQuery['$set'][key1] = updatedBatchAttributesInfo;
  
  var buildObj = {
    batchEntries: updatedBatchEntries,
    batchAttributesInfo: updatedBatchAttributesInfo,
    schemaId: query.schemaId
  }

  batchEntryService.updateBatchEntry(buildObj, batchEntryId, batchNo,
      function(err, updatedBatchEntry){
    if (err) {
      return cb(err, null);
    }
    db.collection(entityToProcess.masterQCSettings.schemaId.toString()).update(
      {
        _id: db.ObjectID((entityToProcess.entryId || "").toString())
      },
      {
        $set: {
          "masterQCSettings.sequenceId": seqId,
          "batchEntryId": entityToProcess.batchEntryId
        }
      },
      function(err, updatedEntry) {
        if (err) {
          return cb(err, null);
        }
        cb(null, {
          updatedBatchEntry: updatedBatchEntry,
          updatedEntry: updatedEntry
        });
      }
    );
    });
  }

  function getRequiredData(entityToProcess, cb) {
    db.schema.findOne({_id: db.ObjectID(entityToProcess.masterQCSettings.schemaId)}, {attributes: 1, entities: 1, keyvalue: 1}, function (err, schema) {
      if (err) {
        return cb(err, null);
      }
      db.collection(entityToProcess.masterQCSettings.schemaId).findOne({_id: db.ObjectID(entityToProcess.entryId)}, function (err, entry) {
        if (err) {
          return cb(err, null);
        }
        cb(null, {schema: schema, entry: entry});
      });
    });
  }


  function removesaveAutoBatchCorrectData(entryId) {
    if (entryId) {
      db.autoBatchCorrectData.remove({entryId: db.ObjectID(entryId)});
    }
  }

  function processBatchCorrection(autoBatchCorrectData, users) {
    if (!autoBatchCorrectData.length) {
      log.log('success - No more pending entry in the queue');
      return;
    }
    var entityToProcess = autoBatchCorrectData.shift();
    log.log('Processing....');
    log.log(entityToProcess);
    logger4js.info({status: 'Processing', data: entityToProcess});
    getRequiredData(entityToProcess, function (err, requiredData) {
      if (err) {
        return cb(err, null);
      }

      var batchNo = entityToProcess.batchEntryId;
      var query = {};
      var key = 'batchEntries.' + entityToProcess.batchEntryId;
      var key1 = 'batchAttributesInfo.' + entityToProcess.batchEntryId;
      query[key] = {'$exists': true};
      var select = {
        [key]: 1,
        [key1]: 1,
        schemaId: 1
      };

      batchEntryService.findOne(entityToProcess.batchEntryId, function (err, batchData) {
        var tempFilteredBatchEntries;
        var updatedBatchEntries = [];
        if(batchData && batchData.batchEntries && batchData.batchEntries[entityToProcess.batchEntryId]){
          var filteredBatchEntries = batchData.batchEntries[entityToProcess.batchEntryId].filter(function(data){
            if(data.masterQCSettings){
              return data.masterQCSettings.schemaId == entityToProcess.masterQCSettings.schemaId;
            }
          });
          filteredBatchEntries = _.sortBy(filteredBatchEntries, 'counter');
          tempFilteredBatchEntries = buildBatchEntriesToUpdate(entityToProcess, batchData, requiredData.schema, requiredData.entry, filteredBatchEntries);
          updatedBatchEntries = batchData.batchEntries[entityToProcess.batchEntryId] || [];
          updatedBatchEntries.push(tempFilteredBatchEntries);
          var filteredBatchAttributesInfo = batchData.batchAttributesInfo[entityToProcess.batchEntryId];
          var updatedBatchAttributesInfo = buildBatchAttributesToUpdate(tempFilteredBatchEntries, entityToProcess, batchData, requiredData.schema, requiredData.entry, filteredBatchAttributesInfo, users);
        }else {
          //this is the case where only one entry is there and it is not saved in the batch
          query = {schemaId: entityToProcess.masterQCSettings.masterQCSettingsId};
          tempFilteredBatchEntries = buildBatchEntriesToUpdate(entityToProcess, null, requiredData.schema, requiredData.entry, []);
          
          updatedBatchEntries.push(tempFilteredBatchEntries);
          updatedBatchAttributesInfo = buildBatchAttributesToUpdate(tempFilteredBatchEntries, entityToProcess, null, requiredData.schema, requiredData.entry, {}, users);
        }
        updateBatchAndEntry(entityToProcess, updatedBatchEntries, updatedBatchAttributesInfo, key, key1, query, function (err, updatedData) {
          if (err) {
            log.log(err);
            logger4js.error({status: 'Error', data: entityToProcess});
            // return; //though there is error in one of the entry..., procced further to update other entry
          }
          log.log('success!!');
          log.log(entityToProcess);
          if (updatedBatchEntries.length == autoBatchCorrectData.length && diffInHours > 7) {
            if (notificationId) {
              db['autoBatchDataCorrectionNotification'].findOneAndUpdate({ _id: db.ObjectID(notificationId.valueOf()) },
                {
                  $set:
                  {
                    notifiedEntriesCount: entityToProcess.length,
                    notified: true
                  }
                }, function (err, data) {
                  if (err) {
                    logger4js.info({status: 'error while updating autoBatchDataCorrectionNotification', data: err});
                  }
                  if (data) {
                    logger4js.info({status: 'autoBatchDataCorrectionNotification updated sucessfully.', data: data});
                  }
                });
            }
          }
          logger4js.info({status: 'Success', data: entityToProcess});
          removesaveAutoBatchCorrectData((entityToProcess.entryId||'').toString());
          processBatchCorrection(autoBatchCorrectData, users);
        }, batchData && batchData._id, batchNo);
      });
    });
  }

  function init(){
    log.log('Initiating job...');
    notificationId = '';
    process.env.dateTime = dateTime ? dateTime : new Date();
    var startTime = moment(moment.utc(new Date(process.env.dateTime)).toDate()).format('h:mm:ss a');
    var endTime = moment(moment.utc(new Date()).toDate()).format('h:mm:ss a');
    diffInHours = moment.utc(moment(endTime, "HH:mm:ss a").diff(moment(startTime, "HH:mm:ss a"),'hour'));
    db.autoBatchCorrectData.find({}).toArray(function (err, autoBatchCorrectData) {
      if (err || !autoBatchCorrectData.length) {
        logger4js.info({status: 'No autoBatchCorrectData found - No process', data: null});
        return;
      }
      if(diffInHours > 7){
        var notificationObj = {
          entries: autoBatchCorrectData,
          notifiedEntriesCount: 0,
          notified: false
        }
        db['autoBatchDataCorrectionNotification'].insertOne(notificationObj,function auditCallback(err, data) {
          logger4js.info({status: 'err', data: err});
          if(data && data.ops && data.ops.length){
            notificationId = data.ops[0]._id;
          }
        });
      }
      // notify(autoBatchCorrectData); //do notify how much entries will be update..
      logger4js.info({status: 'Number of autoBatchCorrectData to be update', count: autoBatchCorrectData.length});
      db.users.find({}, {username: 1, firstname: 1, lastname: 1}).toArray(function (err, users) {
        if (err || !users.length) {
          return;
        }
        processBatchCorrection(autoBatchCorrectData, users);
      });
    });
  }

  // function notify(autoBatchCorrectData){
  //   mailer.send('auto-batch-correct-data', autoBatchCorrectData, config.staticURL,function sendMailCallback(err, response) {
  //     if(err){
  //       log.log('Failure in sending email: ', err);
  //     }else{
  //       console.log(response);
  //       log.log('Mail has been sent to ' + config.staticURL);
  //     }
  //   });
  // }
