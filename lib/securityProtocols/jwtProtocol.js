'use strict'
/**
* @name node-jose JWT token generation
* @author Lokesh Boran <lokesh.b@productivet.com>
*
* @version 0.0.0
*/

var debug = require('../logger').debug('lib:oauth');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var db = require('../db');
var lzstring = require('lz-string');
var uuid = require('node-uuid');
var log = require('../../logger');
var jwt = require('jsonwebtoken');
var config = require('../../config/config');
var tokenOptions = config.oAppSecurityProtocols.tokenOptions;


var currentFileName = __filename;
var logger = require('../../logger');

var pk = fs.readFileSync(path.join(__dirname, '..','..','keys','development.pem'));
var pub = fs.readFileSync(path.join(__dirname, '..','..','keys','development.public.pem'));

// var compressToken = lzstring.compressToEncodedURIComponent(token);

function sign(input, expiryDate) {
  //To set expiration time with token - for future use
  if (expiryDate) {
    //with input you can use input.exp
    tokenOptions.expiresIn = `${Math.round(((new Date(expiryDate)) - (new Date()))/ (1000))}s`;
  }
  logger.log("function: sign - start" , 'info', currentFileName);
  var signature = jwt.sign(input, pk, tokenOptions);
  logger.log("function: sign - end" , 'info', currentFileName);
  return signature;
}

function generate(input) {
  logger.log("function: generate - start" , 'info', currentFileName);
  var encoded = jwt.encode(input, pk, tokenOptions);
  logger.log("function: generate - end" , 'info', currentFileName);
  return encoded;
}

function verifySignature(input) {
  logger.log("function: verifySignature - start" , 'info', currentFileName);
  var decoded = jwt.verify(input, pub, tokenOptions);
  logger.log("function: verifySignature - end" , 'info', currentFileName);
  return decoded;
}




module.exports = {
  sign: sign,
  verifySignature: verifySignature,
  generate:generate
};
