'use strict'
/**
* @name security token for oapp
* @author Lokesh Boran <lokesh.b@productivet.com>
*
* @version 0.0.0
*/
var encryptDecrypt = require('./encryptDecrypt');
var jwt = require('./jwtProtocol');


var currentFileName = __filename;
var logger = require('../../logger');

function _generateToken(input) {
  logger.log("function: _generateToken - start and end" , 'info', currentFileName);
  return jwt.sign(input);
}

function _encryptToken(token) {
  logger.log("function: _encryptToken - start and end" , 'info', currentFileName);
  return encryptDecrypt.encrypt(token);
}

function _decryptToken(shaString) {
  logger.log("function: _decryptToken - start and end" , 'info', currentFileName);
  return encryptDecrypt.decrypt(shaString);
}

function _verifyToken(token) {
  logger.log("function: _verifyToken - start and end" , 'info', currentFileName);
  return jwt.verifySignature(token);
}

function _generateOAppToken(input) {
  logger.log("function: _generateOAppToken - start and end" , 'info', currentFileName);
  return _encryptToken(_generateToken(input));
}

function _decodeOAppToken(token) {
  logger.log("function: _decodeOAppToken - start and end" , 'info', currentFileName);
  return _verifyToken(_decryptToken(token));
}

module.exports = {
  generateToken: _generateToken,
  verifyToken: _verifyToken,
  encryptToken: _encryptToken,
  decryptToken: _decryptToken,
  generateOAppToken: _generateOAppToken,
  decodeOAppToken: _decodeOAppToken
}
