var db = require('./db');
var dbConfig = require('./db/db');

function collectionCheck(collectionId) {
  if(!db[collectionId]){
    var entities = [];
    entities.push({
      collection: collectionId,
      audit: true,
      index: {
        title: ''
      }
    });
    dbConfig.configure.apply(this, entities);
  }
}


module.exports = {
  collectionCheck: collectionCheck
};
