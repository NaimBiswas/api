
var Operator = require('./operator');

// to set left operand and right operand, type(format of question) and set condition
function Condition(_condition) {
    return {
        getLeftOperand: getLeftOperand,
        getRightOperand: getRightOperand
    }

    // for left operand
    function getLeftOperand() {
        var _op1 = Operator.Operator(_condition.leftOperand);
        _op1.setFormat(_condition.leftOperand['type'] ? _condition.leftOperand['type']['format']['title'] : 'None');
        _op1.setOptions(_condition);
        return _op1;
    }
    // for right operand
    function getRightOperand() {
        var _op2 = Operator.Operator(_condition.rightOperand);
        _op2.setFormat(_condition.leftOperand['type'] ? _condition.leftOperand['type']['format']['title'] : 'None');
        _op2.setOptions(_condition);
        return _op2;
    }
}

module.exports = {
    Condition: Condition
}
