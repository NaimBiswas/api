var Promise = require('bluebird');
var config = require('../../config/config.js');
var settings = require('../settings');
var _ = require('lodash');
var db = Promise.promisifyAll(require('../db'));
var debug = require('../logger').debug('lib:scheduler');
var handlebars = require('handlebars');
var fs = require('fs');
var pdf = require('html-pdf');
var moment = require('moment');
var dateServies = require('../dateServies');
var fromZone = dateServies.fromZone;
var configZone = fromZone(config.currentTimeZone);
var logService = require('../logService');
var exportUtilty = require('../utilities/exportUtilities.js');
var generateExcel = require('../utilities/generateExcel.js');
var clientTZ = config.timeZonesP[config.currentTimeZoneP] || 'America/Chicago';

/**
* Method: getResourceDetail
* Purpose : fetch entity detail 
*/
function getResourceDetail(threshold) {
    var ids = _.map((threshold.schema || []), function (schem) {
        return db.ObjectID(schem._id.toString());
    });

    return db
        .collection("entities")
        .find({
            '_id': {
                '$in': ids
            }
        })
        .toArrayAsync();
} // end getResourceDetail

/**
 * Method: generateFileds
 * Purpose : fetch detail for entity questions 
*/
function generateFileds(entities) {
    var fields = {};

    _.forEach(entities, function (_entity) {
        fields[_entity.title] = [];
        _.forEach(_entity.questions, function (que) {
            fields[_entity.title].push({
                title: que.title,
                's#': que['s#'],
                type: que.type
            });
        });
         //recordStatus
         fields[_entity.title].push({title: 'recordStatus'});
    });

    return fields;
}// error handler catch

/**
* Method: transformedModel
* Purpose : generate object for entity
*/
function transformedModel(entity) {
    var transformedModelEntity = _.reduce(entity.entityRecord, function (a, value, question) {
        var procIds = question.split('-');
        if (procIds.length > 1) {
            if (value[procIds[1]] || value[procIds[1]] === 0) {
                a[question] = value[procIds[1]];
            }
        } else {
            if (procIds[0] && procIds[0] !== undefined && procIds[0] !== 'isActive' && procIds[0] !== 'entityvalueinactivereason') {
                a[question] = value;
            }
        }
        //PQT-1431
        if (question === 'isActive') {
            a['recordStatus'] = (value === true) ? 'Active' : 'Inactive';
        }
        return a;
    }, {});

   
    //PQT-2050
    if(entity.sites && entity.sites.allicableSites){
    _.forEach(entity.sites.allicableSites, function (sites, index) {        
        var siteObj = _.reduce(sites, function (a, siteLevel, idx) {
            a[index] = siteLevel.title;
            return a;
        }, {});
        Object.assign(transformedModelEntity,siteObj);       
    });//
    }
    return transformedModelEntity;
} // end transformedModel


module.exports = {
    generateFileds: generateFileds,
    transformedModel: transformedModel,
    getResourceDetail: getResourceDetail
}
