var _ = require('lodash');
module.exports =  {

    'greater than': function(op1,op2){
        return `(${op1.toString()} || ${op1.toString()} == 0) && (${op1.toString()} > ${op2.toString()})`;
    },
    'greater than or equal to': function(op1,op2){
        return `(${op1.toString()} || ${op1.toString()} == 0) && ${op1.toString()} >= ${op2.toString()}`;
    },
    'less than': function(op1,op2){
        return `(${op1.toString()} || ${op1.toString()} == 0) && ${op1.toString()} < ${op2.toString()}`;
    },
    'less than or equal to': function(op1,op2){
        return `(${op1.toString()} || ${op1.toString()} == 0) && ${op1.toString()} <= ${op2.toString()}`;
    },
    'equal to': function(op1,op2){
        return `(${op1.toString()} || ${op1.toString()} == 0) && (_.isArray(${op1.toString()}) ? 
        (_.includes(${op1.toString()}, ${op2.toString()}))
        :(${op1.toString()} == ${op2.toString()}))`;       
    },
    'not equal to': function(op1,op2){
        return `(${op1.toString()} || ${op1.toString()} == 0) && (_.isArray(${op1.toString()}) ? 
        !(_.includes(${op1.toString()}, ${op2.toString()}))
        :(${op1.toString()} != ${op2.toString()}))`;  
    },
    'contains': function(op1,op2){
        return `${op1.toString()} && (${op1.toString()}).indexOf(${op2.toString()}) !== -1`;
    },
    'does not contain': function(op1,op2){
        return `!${op1.toString()} || (${op1.toString()}).indexOf(${op2.toString()}) === -1`;
    },
    'starts with': function(op1,op2){
        return `${op1.toString()} && (${op1.toString()}).startsWith(${op2.toString()})`;
    },
    'ends with': function(op1,op2){
        return `${op1.toString()} && (${op1.toString()}).endsWith(${op2.toString()})`;
    },
    // PQT-1431 record status
    'is':  function (op1,op2) {         
          return `(model['recordStatus']==${op2.toString()})`;        
      },    
    'is empty':  function (op1,op2) {         
        return `(!${op1.toString()} || (_.isArray(${op1.toString()}) ? 
        (_.includes(${op1.toString()}, undefined))
        :(${op1.toString()} == undefined))) || (${op1.toString()} && ${op1.toString()} == "(Not yet evaluated)")`;             
    },
    'is not empty':  function (op1,op2) {         
        return `(${op1.toString()} && (_.isArray(${op1.toString()}) ? 
        !(_.includes(${op1.toString()}, undefined))
        :(${op1.toString()} != undefined))) && (${op1.toString()} && ${op1.toString()} != "(Not yet evaluated)")`;             
    }
}

