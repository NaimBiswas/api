
module.exports =  {

    'greater than': function(op1,op2){
      //  return `(configZone.getDateOnly(${op1.toString()})).isAfter(configZone.getDateOnly(${op2.toString()}))`;
      return `${op1.toString()} && (moment(${op1.toString()}).startOf('day')).isAfter(moment(${op2.toString()}).startOf('day'))`;
    },
    'greater than or equal to': function(op1,op2){
       // return `(configZone.getDateOnly(${op1.toString()})).isSameOrAfter(configZone.getDateOnly(${op2.toString()}))`;
       return `${op1.toString()} && (moment(${op1.toString()}).startOf('day')).isSameOrAfter(moment(${op2.toString()}).startOf('day'))`;
    },
    'less than': function(op1,op2){
       // return `(configZone.getDateOnly(${op1.toString()})).isBefore(configZone.getDateOnly(${op2.toString()}))`;
       return `${op1.toString()} && (moment(${op1.toString()}).startOf('day')).isBefore(moment(${op2.toString()}).startOf('day'))`;
    },
    'less than or equal to': function(op1,op2){
       // return `(configZone.getDateOnly(${op1.toString()})).isSameOrBefore(configZone.getDateOnly(${op2.toString()}))`;
       return `${op1.toString()} && (moment(${op1.toString()}).startOf('day')).isSameOrBefore(moment(${op2.toString()}).startOf('day'))`;
    },
    'equal to': function(op1,op2){
       // return `(configZone.getDateOnly(${op1.toString()})).isSame(configZone.getDateOnly(${op2.toString()}))`;
       return `${op1.toString()} && (moment(${op1.toString()}).startOf('day')).isSame(moment(${op2.toString()}).startOf('day'))`;
    },
    'not equal to': function(op1,op2){
      //  return `!(configZone.getDateOnly(${op1.toString()})).isSame(configZone.getDateOnly(${op2.toString()}))`;
      return `${op1.toString()} &&  !(moment(${op1.toString()}).startOf('day')).isSame(moment(${op2.toString()}).startOf('day'))`;
    },
    'between': function(op1,op2){
      //  var ret = `(((configZone.getDateOnly(${op1.toString()})).isSameOrAfter(configZone.getDateOnly(${op2.getFromString()}))) &&
       // ((configZone.getDateOnly(${op1.toString()})).isSameOrBefore(configZone.getDateOnly(${op2.getToString()}))))`;
        return `${op1.toString()} && (((moment(${op1.toString()}).startOf('day')).isSameOrAfter(moment(${op2.getFromString()}).startOf('day'))) &&
        ((moment(${op1.toString()}).startOf('day')).isSameOrBefore(moment(${op2.getToString()}).startOf('day'))))`;
    },
    'not between': function(op1,op2){
       // return `!(((configZone.getDateOnly(${op1.toString()})).isSameOrAfter(configZone.getDateOnly(${op2.getFromString()}))) &&
       // ((configZone.getDateOnly(${op1.toString()})).isSameOrBefore(configZone.getDateOnly(${op2.getToString()}))))`;
       return `${op1.toString()} && !(((moment(${op1.toString()}).startOf('day')).isSameOrAfter(moment(${op2.getFromString()}).startOf('day'))) &&
       ((moment(${op1.toString()}).startOf('day')).isSameOrBefore(moment(${op2.getToString()}).startOf('day'))))`;
    },    
    'is empty':  function (op1,op2) {         
        return `(!${op1.toString()} || (_.isArray(${op1.toString()}) ? 
        (_.includes(${op1.toString()}, undefined))
        :(${op1.toString()} == undefined))) || (${op1.toString()} && ${op1.toString()} == "(Not yet evaluated)")`;             
    },
    'is not empty':  function (op1,op2) {         
        return `(${op1.toString()} && (_.isArray(${op1.toString()}) ? 
        !(_.includes(${op1.toString()}, undefined))
        :(${op1.toString()} != undefined))) && (${op1.toString()} && ${op1.toString()} != "(Not yet evaluated)")`;             
    }
}

