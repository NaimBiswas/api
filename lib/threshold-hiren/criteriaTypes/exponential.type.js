
var _ = require('lodash');

module.exports =  {

    'greater than': function(op1,op2){
        return `(${op1.toString()} || ${op1.toString()} == 0) && (${op1.toString()}*(Math.pow(10,${getPower(op1)})) > ${op2.toString()})`;
    },
    'greater than or equal to': function(op1,op2){
        return `(${op1.toString()} || ${op1.toString()} == 0) && (${op1.toString()}*(Math.pow(10,${getPower(op1)})) >= ${op2.toString()})`;
    },
    'less than': function(op1,op2){
        return `(${op1.toString()} || ${op1.toString()} == 0) && (${op1.toString()}*(Math.pow(10,${getPower(op1)})) < ${op2.toString()})`;
    },
    'less than or equal to': function(op1,op2){
        return `(${op1.toString()} || ${op1.toString()} == 0) && (${op1.toString()}*(Math.pow(10,${getPower(op1)})) <= ${op2.toString()})`;
    },
    'equal to': function(op1,op2){
        return `(${op1.toString()} || ${op1.toString()} == 0) && (_.isArray(${op1.toString()}) ? 
        (_.includes(${op1.toString()}, ${op2.toString()}))
        :(${op1.toString()}*(Math.pow(10,${getPower(op1)})) == ${op2.toString()}))`;       
    },
    'not equal to': function(op1,op2){
        return `(${op1.toString()} || ${op1.toString()} == 0) && (_.isArray(${op1.toString()}) ? 
        !(_.includes(${op1.toString()}, ${op2.toString()}))
        :(${op1.toString()}*(Math.pow(10,${getPower(op1)})) != ${op2.toString()}))`; 
    },
     'is empty':  function (op1,op2) {     
        return `(!${op1.toString()} || (_.isArray(${op1.toString()}) ? 
        (_.includes(${op1.toString()}, undefined))
        :(${op1.toString()} == undefined))) || (${op1.toString()} && ${op1.toString()} == "(Not yet evaluated)")`;
    },
    'is not empty':  function (op1,op2) {         
        return `(${op1.toString()} && (_.isArray(${op1.toString()}) ? 
        !(_.includes(${op1.toString()}, undefined))
        :(${op1.toString()} != undefined))) && (${op1.toString()} && ${op1.toString()} != "(Not yet evaluated)")`;
    }
}


function getPower(_operator){
    if(_.get(_operator.info,'type.format.metadata.power')){
        return _operator.info['type']['format']['metadata']['power']
    } else {
        return 0;
    }
}