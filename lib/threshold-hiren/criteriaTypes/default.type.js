var _ = require('lodash');

// only for entity sites
function isIncludeSites(op1, op2, entitySites) {
    if (entitySites) {
        return entitySites.allicableSites[op1.comboId].some(function (item) {
            return item.title === op2.replace(/'/g, '');
        });
    } else {
        return false;
    }
}

module.exports =  {

   'equal to': function(op1,op2){
        return `${op1.toString()} && (_.isArray(${op1.toString()}) ? 
        (_.includes(${op1.toString()}, ${op2.toString()}))
        :(${op1.toString()} == ${op2.toString()}))`;       
    },
    'not equal to': function(op1,op2){
        return `${op1.toString()} && (_.isArray(${op1.toString()}) ? 
        !(_.includes(${op1.toString()}, ${op2.toString()}))
        :(${op1.toString()} != ${op2.toString()}))`;  
    },
    'contains': function(op1,op2, entitySites){
        if(op1.info && op1.info.isSite){
            return isIncludeSites(op1.info, op2.toString(), entitySites);
        }else{
            return `${op1.toString()} && (${op1.toString()}).indexOf(${op2.toString()}) !== -1`;
        }
    },
    'does not contain': function(op1,op2, entitySites){
        if(op1.info && op1.info.isSite){
            return !isIncludeSites(op1.info, op2.toString(), entitySites);
        }else{
             return `!${op1.toString()} || (${op1.toString()}).indexOf(${op2.toString()}) === -1`;
        }
    },
    'starts with': function(op1,op2){
        return `${op1.toString()} && (${op1.toString()}).startsWith(${op2.toString()})`;
    },
    'ends with': function(op1,op2){
        return `${op1.toString()} && (${op1.toString()}).endsWith(${op2.toString()})`;
    },
    // PQT-1431 record status
    'is':  function (op1,op2) {         
          return `(model['recordStatus']==${op2.toString()})`;        
      },    
    'is empty':  function (op1,op2) {         
        return `(!${op1.toString()} || (_.isArray(${op1.toString()}) ? 
        (_.includes(${op1.toString()}, undefined))
        :(${op1.toString()} == undefined))) || (${op1.toString()} && ${op1.toString()} == "(Not yet evaluated)")`;             
    },
    'is not empty':  function (op1,op2) {         
        return `(${op1.toString()} && (_.isArray(${op1.toString()}) ? 
        !(_.includes(${op1.toString()}, undefined))
        :(${op1.toString()} != undefined))) && (${op1.toString()} && ${op1.toString()} != "(Not yet evaluated)")`;             
    }
}

