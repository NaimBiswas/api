
var Promise = require('bluebird');
var Entity = require('./entity');
var Schema = require('./schema');
var _ = require('lodash');
var db = Promise.promisifyAll(require('../db'));
var moment = require('moment-timezone');
var dateServies = require('../dateServies');
var fromZone = dateServies.fromZone;
var config = require('../../config/config.js');
var configZone = fromZone(config.currentTimeZone);
var logService = require('../logService');
var conditionService = require('./condition')
var logThreshold;


var operators = {};

var boolMeaning = {
    "AND": '&&',
    "OR": '||',
    "*": '*',
    "/": '/',
    "+": '+',
    "-": '-',
    "%": '%'
};

function Threshold(_threshold) {

     logThreshold = logService._module('threshold', _threshold.type);
    var eachThresholdLog;



    /**
    * var: resourceType
    * Purpose : to decide resource type schema/entity/schedule 
   */
    var resourceType;
    var logInfo = { 'calendarThresholdId': _threshold._id, 'title': _threshold.threshold.title };
    if (_threshold.thresholdType == "entity") {
        resourceType = Entity;
    } else {
        resourceType = Schema;
    }
    // end var 

    var self = {
        getMatchedConditionData: getMatchedConditionData,
        countForMatchedReords: countForMatchedReords,
        resourceType: resourceType,
        logInfo: logInfo,
        setLog: setLog
    }
    return self;


    /**
    * Method: setLog
    * Purpose : get log instance
    */
    function setLog(_eachThresholdLog) {
        eachThresholdLog = _eachThresholdLog;
    }


    /**
     *  * * Lead function * *
     * Method: getMatchedConditionData
     * Purpose : Function to get matched entity/app records
     */
    function getMatchedConditionData(fields, resourceDetail) {
        var formAllEntries = [];
        var filteredRecords = [];
        var conditionsAndSchema = {
            conditions: [],
            schemas: []
        };
        var threshold = _threshold.threshold;

        _.forEach(threshold.conditions, function (c) {
            if (c && c.leftOperand) {
                conditionsAndSchema.conditions.push(c.leftOperand.title + ' ' + c.comparison + ' ' + c.rightOperand);
            }
        });// error handler catch

        // create query to fetch records
        var filter = getRecordsQuery();

        // process for each resource app/entity
        return Promise.each(threshold.schema, function (schema, index) {

            conditionsAndSchema.schemas.push(schema.title);

            // get resouce records
            return getResource_Records(schema, filter).then(function (entries) {

                //start: log
                var extraInfo = { 'extraInfo': [{ 'resourceId': schema._id, 'title': schema.title }] };
                eachThresholdLog.log("Processing Each Resource", extraInfo);
                //end: log

                // only for record status
                if (_threshold.thresholdType == "app") {
                    resourceType.recordStatus(entries);
                }

                filteredRecords[index] = processRecords(entries, threshold);

                //start: log
                var extraInfo = { 'extraInfo': [{ 'resourceId': schema._id, 'title': schema.title, 'total': filteredRecords[index].length }] };
                eachThresholdLog.log("Total Records Satisfy conditions", extraInfo);
                //end: log

                // only for send full report
                formAllEntries.push(entries);
            }).catch(function(err){
                logThreshold.log("Error while processing resource entries"+ err);                      
            });

        }).then(function () {
            return {
                filteredRecords: filteredRecords,
                formAllEntries: formAllEntries,
                conditionsAndSchema: conditionsAndSchema,
                fields: fields
            };// error handler catch
        });// error handler catch

    } // end getMatchedConditionData


    /**    
     * @host: getMatchedConditionData
     * Method: getRecordsQuery
     * Purpose : create query to fetch records 
    */
    function getRecordsQuery() {

        var filter = {};      
        var modifiedEnd;
        var modifiedStart;

        if (_threshold.threshold.reportconfiguration.whichMatchingRecord != 'all') {
                      
            if (_threshold.threshold.frequency.type == 'Hourly') {  
                modifiedEnd = moment().startOf('hour');     
                modifiedStart = _.cloneDeep(modifiedEnd).add(-1, 'hour');
            } else {
                modifiedEnd = moment().startOf('day');
              if(_threshold.threshold.lastRunTime){
                modifiedStart = moment(_threshold.threshold.lastRunTime).startOf('day');
              }else{
                modifiedStart = moment(_threshold.threshold.startDate).startOf('day');
              }               
            }

            // comment only for QA
                logThreshold.log('---Record Start Time ---'+ modifiedStart.toDate());
                logThreshold.log('---Record End Time---'+ modifiedEnd.toDate());
            //
            filter.modifiedDate = {
                $gte: modifiedStart.toDate(),
                $lt: modifiedEnd.toDate()
            };
        }
        return filter;
    } // end getRecordsQuery


    /**
     *  @host: getMatchedConditionData
     * Method: getResource_Records
     * Purpose : get resouce records
    */
    function getResource_Records(schema, filter) {
        return db
            .collection(db.ObjectID(schema._id).toString())
            .find(filter)
            .toArrayAsync()
            .then(function (entries) {
                if (entries && entries.length) {
                    return entries;
                }else{
                    return [];
                }
            });

    } // end getResource_Records // error handler catch


    /**    
     *  @host: getMatchedConditionData 
     * Method: processrecords
     * Purpose : to check weather records satisfies given condition
    */
    function processRecords(entries, threshold) {
        var recordSelection = [];

        _.forEach(entries, function (entity, idx) {

            var appOrEntity = '';
            var transformedModel = {};
            var entitySites = []
            if (threshold.thresholdType == 'entity') {
                appOrEntity = 'entityRecord';
                entitySites  = entity.sites;
            } else {
                appOrEntity = 'entries';
            }

            transformedModel = resourceType.transformedModel(entity);

            var format = 'Number';
            var proc = '';
            // Only for date, Function to parse conditions
            threshold.conditions = parseConditions(threshold.conditions);

            var lamda = '(function(model){ return !!(' + parseCriteria(proc, threshold.conditions, format, entity[appOrEntity], entitySites) + ')})(' + JSON.stringify(transformedModel) + ');';
            lamda = lamda.replace('(function(model){ return   &&', '(function(model){ return  ').replace('(function(model){ return   ||', '(function(model){ return  ');
           
            // evaluate lambda function
            var res = eval(lamda);

            if (res) {
                recordSelection.push(entity);
            }
        });// error handler catch

        return recordSelection;
    } // end processrecords   

    /**
   * @host: processRecords
   * Method: parseConditions
   * Purpose : Only for date, Function to parse conditions
   * @param {Array} conditions
   */
    function parseConditions(conditions) {
        var startEndDate;
        _.forEach(conditions, function (condition) {
            if (condition.conditions) {
                parseConditions(condition.conditions);
            } else {
                if (condition && condition.dateSelection) {
                    startEndDate = getStartEndDate(condition);
                    if (condition.comparison.toLowerCase() == ('Greater than').toLowerCase() || (condition.comparison).toLowerCase() == ('Less than or equal to').toLowerCase()) {
                        condition.rightOperand = startEndDate.lastDay.toISOString();
                        condition.rightEndOperand = undefined;
                    } else {
                        condition.rightOperand = startEndDate.firstDay.toISOString();
                        condition.rightEndOperand = startEndDate.lastDay == undefined ? undefined : startEndDate.lastDay.toISOString();
                        if (condition.dateSelection != 'Today' && condition.dateSelection != 'Specific Date') {
                            if (condition.comparison == 'Equal to') {
                                condition.comparison = 'Between';
                            } else if (condition.comparison == 'Not equal to') {
                                condition.comparison = 'Not Between';
                            }
                        }
                    }
                }
            }
        });// error handler catch
        return conditions;
    }

    /**
     * @host: parseConditions
     *  Method: getStartEndDate
    * Purpose : set start and end date 
    */
    function getStartEndDate(condition) {
        var startEndDate;
        var today = moment().startOf('day');

        switch (condition.dateSelection) {
            case 'This Week':
                startEndDate = {
                    firstDay: moment().startOf('week'),
                    lastDay: moment().endOf('week')
                };
                break;

            case 'This Month':
                startEndDate = {
                    firstDay: moment().startOf('month'),
                    lastDay: moment().endOf('month')
                };
                break;

            case 'This Year':
                startEndDate = {
                    firstDay: moment().startOf('year'),
                    lastDay: moment().endOf('year')
                };
                break;

            case 'Previous Week':
                startEndDate = {
                    firstDay: moment().day(-7),
                    lastDay: moment().day(-1)
                };
                break;

            case 'Previous Month':
                var thisMonth = today.month();
                today.add(-1, 'month');
                if (today.month() != thisMonth - 1 && (today.month() != 11 || (thisMonth == 11 && today.date() == 1))) {
                    today.date(0);
                }
                startEndDate = {
                    firstDay: today.startOf('month'),
                    lastDay: _.cloneDeep(today).endOf('month')
                };
                break;

            case 'Previous Year':
                var thisYear = today.year();
                today.add(-1, 'year');
                if (today.year() != thisYear - 1 && (today.year() != 11 || (thisYear == 11 && today.date() == 1))) {
                    today.date(0);
                }
                startEndDate = {
                    firstDay: today.startOf('year'),
                    lastDay:  _.cloneDeep(today).endOf('year')
                };
                break;

            case 'Specific Date':
                startEndDate = {
                    firstDay: new Date(condition.rightOperand),
                    lastDay: new Date(condition.rightOperand)
                };
                break;
            
            case 'Today':
                startEndDate = {
                    firstDay: moment().toDate(),
                    lastDay: moment().toDate()
                };
                break;

            default:
                startEndDate = {
                    firstDay: moment().toDate(),
                    lastDay: moment().toDate()
                };
        }
        return startEndDate;
    }// getStartEndDate

    /**
     * @host: processRecords
     * Method: parseCriteria
     * Purpose : Function to parse records on desired formates
     */
    function parseCriteria(procId, criteria, format, model, entitySites) {
        var val = '';
        _.forEach(criteria, function (c) {
            if (c.conditions) { //check group conditions
                val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + '(' + parseCriteria(procId, c.conditions, format, model) + ')';
            } else {
                if (!_.isUndefined(c.comparison) && c.comparison !== null) {

                    var cond = conditionService.Condition(c);
                    // _op1 will hold regarding left operand
                    var _op1 = cond.getLeftOperand();
                    // _op2 will hold regarding rght operand (value)
                    var _op2 = cond.getRightOperand();
                
                    val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + _op1.getConditions()[c.comparison.toLowerCase()](_op1, _op2, entitySites);

                } else {
                    if (!_.isUndefined(c.leftOperand) && c.leftOperand !== null) {
                        if (c.leftOperand['type']['format']['title'] === 'Exponential') {
                            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + '(model[\'' + c.leftOperand['s#'] + '-' + c.leftOperand['parentId'] + '\'] * (Math.pow(10, ' + c.leftOperand['type']['format']['metadata']['power'] + ')))';
                        } else {
                            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + '(model[\'' + c.leftOperand['s#'] + '-' + c.leftOperand['parentId'] + '\'])';
                        }
                    } else if (!_.isUndefined(c.rightOperand) && c.rightOperand !== null) {
                        if (_.isObject(c.rightOperand) && c.rightOperand['type']['format']['title'] === 'Exponential') {
                            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + (_.isObject(c.rightOperand) ? '(model[\'' + c.rightOperand['s#'] + '-' + c.rightOperand['parentId'] + '\'] * (Math.pow(10, ' + c.rightOperand['type']['format']['metadata']['power'] + ')))' : parseFloat(c.rightOperand) ? parseFloat(c.rightOperand) : 0);
                        } else if (_.isObject(c.rightOperand) && c.rightOperand['type']['format']['title'] === 'Date') {
                            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + (_.isObject(c.rightOperand) ? '(model[\'' + c.rightOperand['s#'] + '-' + c.rightOperand['parentId'] + '\'])' : new Date(c.rightOperand) ? new Date(c.rightOperand) : 0);
                        } else {
                            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + (_.isObject(c.rightOperand) ? '(model[\'' + c.rightOperand['s#'] + '-' + c.rightOperand['parentId'] + '\'])' : new Date(c.rightOperand) ? new Date(c.rightOperand) : 0);
                        }
                    }
                }
            }
        });// error handler catch
        return val;
    }

    /**
     * @host: from index file
    * Method: countForMatchedReords
    * Purpose : to check weather recordCount satisfies condition of total records
   */
    function countForMatchedReords(filteredRecords) {
        var entryCount = (filteredRecords || []).reduce(function (acc, curr) {
            return acc + (curr || []).length;
        }, 0);// error handler catch

        var threshold = _threshold.threshold;

        if (filteredRecords && filteredRecords.length) {
            switch (threshold.matchingtrcord.operator.toLowerCase()) {
                case 'equal to':
                    return entryCount == parseInt(threshold.matchingtrcord.value);
                    break;

                case 'not equal to':
                    return entryCount != parseInt(threshold.matchingtrcord.value);
                    break;

                case 'greater than':
                    return entryCount > parseInt(threshold.matchingtrcord.value);
                    break;

                case 'less than':
                    return entryCount < parseInt(threshold.matchingtrcord.value);
                    break;

                case 'greater than or equal to':
                    return entryCount >= parseInt(threshold.matchingtrcord.value);
                    break;

                case 'less than or equal to':
                    return entryCount <= parseInt(threshold.matchingtrcord.value);
                    break;

                default:
                    return 0;
            }
        }

    } // end countForMatchedReords


} // end Threshold


module.exports = {
    Threshold: Threshold
}