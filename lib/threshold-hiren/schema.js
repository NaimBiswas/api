var Promise = require('bluebird');
var config = require('../../config/config.js');
var settings = require('../settings');
var _ = require('lodash');
var db = Promise.promisifyAll(require('../db'));
var debug = require('../logger').debug('lib:scheduler');
var handlebars = require('handlebars');
var fs = require('fs');
var pdf = require('html-pdf');
var moment = require('moment');
var dateServies = require('../dateServies');
var fromZone = dateServies.fromZone;
var configZone = fromZone(config.currentTimeZone);
var logService = require('../logService');
var exportUtilty = require('../utilities/exportUtilities.js');
var generateExcel = require('../utilities/generateExcel.js');
var clientTZ = config.timeZonesP[config.currentTimeZoneP] || 'America/Chicago';


/**
* Method: getResourceDetail
* Purpose : fetch entity detail 
*/
function getResourceDetail(threshold) {
    var ids = _.map((threshold.schema || []), function (schem) {
        return db.ObjectID(schem._id.toString());
    });// error handler catch

    return db
        .collection("schema")
        .find({
            '_id': {
                '$in': ids
            }
        })
        .toArrayAsync();
} // end getResourceDetail

/**
 * Method: generateFileds
 * Purpose : fetch detail for app questions 
*/
function generateFileds(schemas) {
    var fields = {};

    _.forEach(schemas, function (_schema) {
        fields[_schema.title] = [];

        _.forEach(_schema.attributes, function (attr) {
            fields[_schema.title].push({
                _id: attr._id,
                title: attr.title,
                's#': attr['s#'],
                type: attr.type
            });
        });// error handler catch

        _.forEach(_schema.entities, function (entity) {
            _.forEach(entity.questions, function (que) {
                if (que.isAppearance) {
                    fields[_schema.title].push({
                        _id: que._id,
                        title: que.title,
                        's#': que['s#'],
                        parentId: que.parentId,
                        type: que.type,
                        isEntity: true
                    });
                }
            });// error handler catch
        });// error handler catch

        _.forEach(_schema.procedures, function (attr) {
            _.forEach(attr.questions, function (que) {
                fields[_schema.title].push({
                    _id: que._id,
                    title: que.title,
                    's#': que['s#'],
                    parentId: que.parentId,
                    type: que.type,
                    isEntity: false
                });
            });// error handler catch
        });// error handler catch

        //recordStatus
        fields[_schema.title].push({title: 'recordStatus'});
    });// error handler catch

    return fields;
} // end generateFileds

/**
* Method: transformedModel
* Purpose : generate object for app
*/
function transformedModel(entity) {
    var transformedModelApp = _.reduce(entity.entries, function (a, value, question) {
        var procIds = question.split('-');
        if (procIds.length > 1) {
            if (value[procIds[1]] || value[procIds[1]] === 0) {
                a[question] = value[procIds[1]];
            }
        } else {
            if (procIds[0] && procIds[0] !== 'FailMesssageForQCForm' && procIds[0] !== 'draftStatus' && procIds[0] !== 'isattributesadded' && procIds[0] !== 'status' && procIds[0] !== 'settings' && procIds[0] !== 'reviewStatus' &&
                procIds[0] !== 'verifiedBy' && procIds[0] !== 'createdBy' && procIds[0] !== 'modifiedBy' && procIds[0] !== 'createdDate' && procIds[0] !== 'modifiedDate' && procIds[0] !== 'version' && procIds[0] !== 'versions' && procIds[0] !== 'isMajorVersion') {
                a[question] = value.attributes;
            }
            if (question === 'recordStatus') {
                a[question] = value;
            }
        }
        return a;
    }, {});// error handler catch
    return transformedModelApp;
}

/** start PQT-1431
 * Method: recordStatus, workFlowReview
 * Purpose : Function to parse record status
 */
function recordStatus(entries) {
    var status = "";
    _.forEach(entries, function (entry) {
        if (entry.isRejected) {            
            if (entry.entries.status.status === 1) {
                status = workFlowReview(entry) + "Passed" + '(Rejected)';
            } 
            if (entry.entries.status.status === 0) {
                status = workFlowReview(entry) + "Failed" + '(Rejected)';
            }
        } else {
            if (entry.entries.status.status === 1) {
                status =  workFlowReview(entry) + "Passed" ;
            } else if (entry.entries.status.status === 0) {
                status =  workFlowReview(entry) + "Failed"; 
            } else if (entry.entries.status.status === 3) {
                status = "Void";
            } else if (entry.entries.status.status === 4) {
                status = "Draft";
            }
        }
        entry.entries['recordStatus'] = status;
    });// error handler catch
}
function workFlowReview(entry) {
    var reviewPending;
    if (entry.workflowreview && entry.workflowreview.length > 0) {
        reviewPending = _.find(entry.workflowreview, { reviewstatus: 1 });
    }
    if (reviewPending) {
        return "Review Pending - ";
    } else {
        return "";
    }
}
// end PQT-1431   


module.exports = {
    generateFileds: generateFileds,
    transformedModel: transformedModel,
    recordStatus: recordStatus,
    getResourceDetail: getResourceDetail
}
