var Promise = require('bluebird');

var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('../logger').configure(__dirname, enableBunyan);

var config = require('../../config/config.js');
var CronJob = require('cron').CronJob;
var log = require('../../logger');
var _ = require('lodash');
var db = Promise.promisifyAll(require('../db')); //require('./db');
var dateServies = require('../dateServies');
var fromZone = dateServies.fromZone;
var configZone = fromZone(config.currentTimeZone);
var logService = require('../logService');
var logThreshold;
var generateMail = require('./generatemail.js');
var Threshold = require('./threshold');
var moment = require('moment-timezone');

var jobDaily = new CronJob('5 0 * * *', function () {
    initAlerts('daily');
}, function () { },
    true
);

var jobHourly = new CronJob('0 */1 * * *', function () {
    initAlerts('hourly');
}, function () { },
    true
);

//initAlerts('hourly')

/**
 *  * * Main Lead function * *
 * Method: initAlerts
 * Purpose : Function to start smart alerts
 */
function initAlerts(cronType) {

    logThreshold = logService._module('threshold', cronType);
    logThreshold.log("starting cron in initAlerts");

    // get the calendarthreshold which needs to be processed
    getTodayThreshold(cronType)
        .then(function (thresholds) {

            // process each calendarthreshold
            Promise.each(thresholds, function (threshold, idx) {

                var eachThresholdLog = logThreshold.getInstance(threshold.threshold.title,
                    { 'calendarThresholdId': threshold._id, 'title': threshold.threshold.title })


                //start: log
                var extraInfo = { 'extraInfo': [{ 'calendarThresholdId': threshold._id }] };
                eachThresholdLog.log("Start Processing Threshold", extraInfo);
                //end: log

                var _threshold = Threshold.Threshold(threshold);
                _threshold.setLog(eachThresholdLog);

                // get resource : schema/entity detail
                return _threshold.resourceType.getResourceDetail(threshold)
                    .then(function (resourceDetail) {

                        //start: log
                        var thresholdForLog = _.map(resourceDetail, function (logItem) {
                            return {
                                _id: logItem._id,
                                title: logItem.title
                            }
                        })// error handler catch
                        eachThresholdLog.log("Resources found", { 'extraInfo': thresholdForLog });
                        //end: log

                        // filter fields
                        var fields = _threshold.resourceType.generateFileds(resourceDetail);

                        // check for threshold conditions
                        return _threshold.getMatchedConditionData(fields, resourceDetail);
                    })// error handler catch
                    .then(function (recordData) {
                        // check number of matching records
                        recordData.filteredRecords = _threshold.countForMatchedReords(recordData.filteredRecords) ? recordData.filteredRecords : [];

                        //to update threshold collection to insert lastRuntime
                         db.thresholds.update({'_id':threshold.thresholdId},{$set:{'lastRunTime':new Date()}},{multi:false}); 

                        // to generate excel/pdf file and email
                        if (recordData.filteredRecords.length || threshold.threshold.reportconfiguration.exatmatchrecord === 'Send Full Report') {
                            // comment only for QA
                                logThreshold.log('---Ready for email ---');
                            //
                            return generateMail.generateMailObj(recordData.filteredRecords, recordData.fields, recordData.conditionsAndSchema, _.cloneDeep(threshold), recordData.formAllEntries, threshold.threshold.reportconfiguration.exatmatchrecord, threshold.thresholdType, eachThresholdLog);
                        } else {
                            // comment only for QA
                                logThreshold.log('---Does not Satisfy condition & Ready for next calender threshold ---');
                            //
                            eachThresholdLog.log("Condition not Satisfied.", extraInfo);
                            return 'need to change';
                        }
                    }).catch(function(err){
                        console.log("Error found----"+err);                      
                    });

            }).then(function () {
                console.log("All thresholds are processed.");
            });
        }).catch(function(err){
            console.log("No threshold found ");                      
        });
}


/**
 * Method: getTodayThreshold
 * Purpose : fetch only those records  be processed for smart alert
*/
function getTodayThreshold(cronType) {

    var start;
    var end;
    var type;  

    if (cronType == 'hourly') {
        start = moment().startOf('hour');
        end = _.cloneDeep(start).add(1, 'hour');
        type = {
            '$eq': 'Hourly'
        };
    } else {
        start = moment().startOf('day');
        end = _.cloneDeep(start).add(1, 'day')
        type = {
            '$ne': 'Hourly'
        };
    }

    // comment only for QA
        logThreshold.log('---Start Time---'+ start.toDate());
        logThreshold.log('---End Time---'+ end.toDate());
    // 

    log.log("before asking for today's threshold of", {
        start: start,
        end: end
    });

    return db.collection('calendarThresholds')
        .aggregateAsync([{
            $match: {
                'isActive': true,
                'type': type,
                'notified': false,                 
                'date': {
                    $gte: start.toDate(),
                    $lt: end.toDate()
                }
            }
        },
        {
            $lookup: {
                from: "thresholds",
                localField: "thresholdId",
                foreignField: "_id",
                as: "threshold"
            }
        },
        {
            "$unwind": "$threshold"
        }
        ]).then(function (thresholds) {
            if (!thresholds || !thresholds.length) {
                log.log("no threshold found for today");
                throw "no threshold found for today";
            } else {

                var thresholdForLog = _.map(thresholds, function (_threshold) {
                    return {
                        _id: _threshold._id,
                        title: _threshold.threshold.title,
                        thresholdId: _threshold.thresholdId,
                        schema: _threshold.schema,
                        module: _threshold.module,
                        thresholdCondition: _threshold.thresholdCondition,
                        date: _threshold.date,
                    }
                })

                logThreshold.log("Found Thresholds", { 'extraInfo': thresholdForLog });
                return thresholds;
            }
        }).catch(function (err) {
            logThreshold.log("Error while finding for threshold", err);
        });
}






