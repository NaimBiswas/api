var exponentialType = require('./criteriaTypes/exponential.type');
var dateType = require('./criteriaTypes/date.type');
var defaultType = require('./criteriaTypes/default.type');
var numberType = require('./criteriaTypes/number.type');
var _ = require('lodash');

function Operator(_operator) {

    var format;
    var condition;

    // to set the format of the question
    function setFormat(_format) {
        format = _format;
    }

    // to get the format of the question
    function getFormat() {
        return format;
    }

    // to decide which file will be used (exponentialType/dateType/defaultType) to make conditions
    function getConditions() {
        switch (getFormat()) {
            case 'Exponential':
                return exponentialType;
            case 'Scientific':
                 return exponentialType;
            case 'Date':
                return dateType;
            case 'Number':  
                return numberType; 
            case 'Currency':  
                return numberType;
            case 'Percentage':  
                return numberType;  
            default:
                return defaultType;
        }
    }

    // to check weather it is attribute or not
    function isAttribute() {
        return !!_operator.parentId;
    }

    //only used for between and not between in date type
    function getFromString() {
        return toString();
    }
    //only used for between and not between in date type
    function getToString() {
       return getOperand(condition.rightEndOperand);
    }

    // to set the condition, for rightendOperand
    function setOptions(_condition) {
        condition = _condition
    }

    // to fetch the value for left and right 
    function toString() {
        return getOperand(_operator);
    }

    // sub-part of toString()
    function getOperand(op) {

        // for left operand
        if (_.isObject(op)) {
            if (isAttribute()) {
                return `model['${op['s#']}-${op['parentId']}']`;
            } else {
                return `model['${op['s#']}']`;
            }
        } else { // for right operand
            if (getFormat() == 'None' || getFormat() == 'Date') {
                return `'${op}'`;
            } else {
                return op;
            }
        }
    }

    var _exports = {
        isAttribute: isAttribute,
        toString: toString,
        info: _operator,
        getConditions: getConditions,
        setFormat: setFormat,
        getFromString: getFromString,
        getToString: getToString,
        setOptions: setOptions
    }

    return _exports;
}

module.exports = {
    Operator: Operator
}