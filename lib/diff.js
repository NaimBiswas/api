var _ = require('lodash');
function diff(ns, o, n) {
  var val = _.reduce(n,function (ret, value, property) {
    if(value && value._bsontype){
      value = value.toString('hex');
    }
    if(o[property] && o[property]._bsontype){
      o[property] = o[property].toString('hex');
    }
    if ((property == 'performedActionOn') && o[property] && o[property].toISOString) {
        o[property] = o[property].toISOString();
    }
    if ((property == 'performedActionOn') && value && value.toISOString) {
      value = value.toISOString();
    }

    if (!isDateType(value, o[property])
    && !isMongoObject(value, o[property])
    && !_.isEqual(value, o[property])
    && (typeof value === 'object' || typeof o[property] === 'object') && (!property || (!property.endsWith || property != 'performedActionOn'))) {
      if (checkFieldType('file', o[property], value)) {
        ret.push({
          path: (ns ? (ns + '.') : ns) + property,
          old: o[property],
          new: value
        });
      } else {
        ret.push(diff.bind(this, (ns ? (ns + '.') : ns) + property, o[property] || {}, value));
      }
    }else {
      if (!_.isEqual(value, o[property])) {
        ret.push({
          path: (ns ? (ns + '.') : ns) + property,
          old: o[property],
          new: value
        });
      }
    }
    delete o[property];
    return ret;
  },[]);

  if(typeof o === 'object' && _.keys(o).length){
    val = _.reduce(o,function (ret, value, property) {
      if(value){
        if(typeof value === 'object' && !isDateType(value) && !isMongoObject(value)){
          if (checkFieldType('file', value)) {
            ret.push({
              path: (ns ? (ns + '.') : ns) + property,
              old: value,
              new: undefined
            });
          } else {
            ret.push(diff.bind(this, (ns ? (ns + '.') : ns) + property, value, {}));
          }
        }else {
          ret.push({
            path: (ns ? (ns + '.') : ns) + property,
            old: value,
            new: undefined
          });
        }
      }
      return ret;
    },val);
  }
  return val;
}

function isMongoObject(newVal, oldVal) {
  return ((newVal && newVal._bsontype) || (oldVal && oldVal._bsontype));
}

function isDateType(newVal, oldVal) {
  return ((typeof newVal == 'object' && _.isDate(newVal)) || (typeof oldVal == 'object' && _.isDate(oldVal)));
}

function checkFieldType(fieldtype, o, n) {
  return (_.isObject(n) && n['fieldType'] === fieldtype) || (_.isObject(o) && o['fieldType'] === fieldtype);
}

function trampoline(o,n) {
  var res = diff('',o,n);
  if(res && res.length){
    var allResolved = false;

    while(!allResolved){
      var unresolved = _.filter(res, function (value) {
        return _.isFunction(value)
      });
      res = _.filter(res, function (value) {
        return !_.isFunction(value)
      });

      allResolved = !unresolved.length;
      _.forEach(unresolved, function (thunk) {
        res = res.concat(thunk());
        // res = _.union(res, thunk());
      });
    }
  }
  return res;
}


module.exports = trampoline;