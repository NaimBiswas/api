'use strict'
var db = require('./db');
var mailer = require('./mailer');
var debug = require('./logger').debug('lib:registration');
var crypto = require('crypto');
var resources = require('./resources');
var _ = require('lodash');
var randomstring = require("randomstring");
var settings = require('./settings');
var log = require('../logger');
var auditTodb = require('./db/db');
var moment = require('moment');
var config = require('../config/config');
var ip = require('ip');

var currentFileName = __filename;
var logger = require('../logger');

var usersResource = resources.fetch({
  name: 'users',
  collection: 'users',
  retract: ['password', 'verificationCode', 'securityAnswer']
});

function conCurrentUserFunctionality(user, userObject, cb) {
  logger.log("function: conCurrentUserFunctionality - start" , 'info', currentFileName);
  if(!_.isUndefined(user.isConCurrentUserActive) && user.isConCurrentUserActive) {
    logger.log("if(!_.isUndefined(user.isConCurrentUserActive) && user.isConCurrentUserActive) {" , 'info', currentFileName);
    userObject.conCurrentUserCode = 'alreadyLogedIn';
    cb(null, userObject);
  } else if(!user.isConCurrentUserActive) {

    logger.log("else if(!user.isConCurrentUserActive) {" , 'info', currentFileName);

    logger.log("collection:users before fetch" , 'info', currentFileName);
    db.users.find({isConCurrentUserActive: true}).count()
    .then(function (loggedInUsers) {
      logger.log("collection:users fetch" , 'info', currentFileName);

      logger.log("collection:modules before fetch" , 'info', currentFileName);
      db.modules.find({"isActive" : true}, function (e, data){
        if(e) {
          logger.log("collection:modules error : " + e , 'error', currentFileName);
          cb(e, userObject);
        }
        logger.log("collection:modules fetch" , 'info', currentFileName);
        data.toArray(function (err, module) {
          if(err) {
            cb(err, userObject);
            return;
          }
          if(loggedInUsers >= +module[0].noOfConCurrentUsers) {

            logger.log("if(loggedInUsers >= +module[0].noOfConCurrentUsers) {" , 'info', currentFileName);
            //PQT-4348 - Purvi
            // var isAdminUser = _.filter(user.roles,{title: 'Admin'}).length>0;
            // var isAdminUser = _.filter(user.roles, ({title}) => title === 'Admin Full Control' || title === 'Admin').length>0;
            var isAdminUser = _.some(user.roles, function (role) {
              return role.title === 'Admin Full Control' || role.title === 'Admin';
          });
            if(isAdminUser){
              logger.log("if(isAdminUser){" , 'info', currentFileName);
              userObject.conCurrentUserCode = 'adminTries';

              //Chandni Patel : Below code is added to fix this bug -->> QC3-1444 Bug : Concurrent users - Admin user cannot find the list of logged in users


              db.users.findOneAndUpdate({
                _id: user._id
              },{
                $set: {
                  isConCurrentUserAdmin: true
                }
              });
              //Code End

            } else{

              logger.log("else of if(isAdminUser){" , 'info', currentFileName);

              userObject.conCurrentUserCode = 'conCurrentUserExceed';
            }
          } else{
            logger.log("else of if(loggedInUsers >= +module[0].noOfConCurrentUsers) {" , 'info', currentFileName);
            user.isConCurrentUserActive = true;
            updateUserIsConCurrentUserActive(user);
          }
          cb(null, userObject);
        });
      });
    })
    .catch(function(err){
      logger.log("collection:users error : " + err , 'error', currentFileName);
      cb(err, userObject);
    })

  }
  logger.log("function: conCurrentUserFunctionality - end" , 'info', currentFileName);
}


function updateUserIsConCurrentUserActive(user){
  logger.log("function: updateUserIsConCurrentUserActive - start" , 'info', currentFileName);
  db.users.findOneAndUpdate({
    _id: user._id
  },{
    $set: {
      isConCurrentUserActive: user.isConCurrentUserActive
    }
  });
  logger.log("function: updateUserIsConCurrentUserActive - end" , 'info', currentFileName);
}

function updateUserIsConCurrentUserActiveWithAdmin(user, admin) {

  logger.log("function: updateUserIsConCurrentUserActiveWithAdmin - start" , 'info', currentFileName);
  db.users.findOneAndUpdate({
    _id: user._id
  }, {
    $set: {
      isConCurrentUserActive: user.isConCurrentUserActive,
      forceRemovedUser : admin.username.toLowerCase()
    }
  });
  logger.log("function: updateUserIsConCurrentUserActiveWithAdmin - end" , 'info', currentFileName);
}

function conCurrentUserUpdate(user, cb){
  logger.log("function: conCurrentUserUpdate - start" , 'info', currentFileName);
  if (user) {
    logger.log("if (user) {" , 'info', currentFileName);
    if (user._adminId && user._adminId.length === 24) {
      logger.log("if (user._adminId && user._adminId.length === 24) {" , 'info', currentFileName);
      user._adminId = db.ObjectID(user._adminId);
      logger.log("collection:users before fetch" , 'info', currentFileName);
      db.users.findOne({ _id : user._adminId }, function (err,fuser) {
        if (err) {
          logger.log("collection:users error : " + err , 'error', currentFileName);
        }
        else {
          logger.log("collection:users fetch" , 'info', currentFileName);
          user.isConCurrentUserActive = false;
          updateUserIsConCurrentUserActiveWithAdmin(user, fuser);
          // user._id = db.ObjectID(user._adminId);
          fuser.isConCurrentUserActive = true;
          user.isConCurrentUserActive = true;
          updateUserIsConCurrentUserActive(fuser);
        }
        cb(null, user);
      });
    }
    else {
      logger.log("else of if (user._adminId && user._adminId.length === 24) {" , 'info', currentFileName);
      user.isConCurrentUserActive = false;
      updateUserIsConCurrentUserActive(user);
      cb(null, user);
    }
  }

  logger.log("function: conCurrentUserUpdate - end" , 'info', currentFileName);
}

function conCurrentUserUpdateByUserName(user, cb){
  logger.log("function: conCurrentUserUpdateByUserName - start" , 'info', currentFileName);
  if(user){
    logger.log("if(user){" , 'info', currentFileName);
    db.users.findOneAndUpdate({
      username: user.username.toLowerCase()
    }, {
        $set: {
          isConCurrentUserActive: false,
          isConCurrentUserAdmin: false,
          forceRemovedUser: user.username.toLowerCase()
        }
      });
    cb(null, user);
  }
  logger.log("function: conCurrentUserUpdateByUserName - end" , 'info', currentFileName);
}


function conCurrentAdminUserUpdate(user, cb){
  logger.log("function: conCurrentAdminUserUpdate - start" , 'info', currentFileName);

  logger.log("collection:users before fetch" , 'info', currentFileName);
  db.users.findOneAndUpdate({
    _id: user._id
  },{
    $set: {
      isConCurrentUserAdmin: user.isConCurrentUserAdmin
    }
  },function updateOneCallback(err, data) {
    if(err){
      logger.log("collection:users error : " + err , 'error', currentFileName);
      cb(err);
      return;
    }
    logger.log("collection:users fetch" , 'info', currentFileName);
    cb(null, data);
  });
  logger.log("function: conCurrentAdminUserUpdate - end" , 'info', currentFileName);
}

function updatePasswordLog(user, cb){
  logger.log("function: updatePasswordLog - start" , 'info', currentFileName);

  logger.log("collection:generalSettings before fetch" , 'info', currentFileName);
  db['generalSettings'].find(function(e, data) {
    if(e) {
      logger.log("collection:generalSettings error : " + e , 'error', currentFileName);
      return; }

    logger.log("collection:generalSettings fetch" , 'info', currentFileName);
    data.toArray(function (err, generalSettingsResponse) {
      if(!_.isUndefined(generalSettingsResponse)){
        logger.log("if(!_.isUndefined(generalSettingsResponse)){" , 'info', currentFileName);

        logger.log("collection:users before fetch" , 'info', currentFileName);
        db['users'].findOne({_id: user._id}, function(e, userResponse) {
          if(e) {
            logger.log("collection:users error : " + e , 'error', currentFileName);
            return; }
          logger.log("collection:users fetch" , 'info', currentFileName);
          if(userResponse){
            logger.log("if(userResponse){" , 'info', currentFileName);
            userResponse.passwordLog = userResponse.passwordLog || [];
            if(generalSettingsResponse[0].reUsedPasswordsLimit <= userResponse.passwordLog.length){
              logger.log("if(generalSettingsResponse[0].reUsedPasswordsLimit <= userResponse.passwordLog.length){" , 'info', currentFileName);
              userResponse.passwordLog.splice(0, (userResponse.passwordLog.length - generalSettingsResponse[0].reUsedPasswordsLimit) + 1);
            }
            userResponse.passwordLog.push(userResponse.password);
            if(generalSettingsResponse[0].reUsedPasswordsLimit === 0){
              logger.log("if(generalSettingsResponse[0].reUsedPasswordsLimit === 0){" , 'info', currentFileName);
              userResponse.passwordLog = [];
            }
            db.users.findOneAndUpdateAsync({
              _id: user._id
            }, {
                $set: {
                  passwordLog: userResponse.passwordLog

                }
              })
            cb(null, user);
          }
        });
      }
    });
  });
}

function validateNewPassword(user, cb) {
  logger.log("function: validateNewPassword - start" , 'info', currentFileName);

   logger.log("collection:generalSettings before fetch" , 'info', currentFileName);
  db['generalSettings'].find(function(e, data) {
    if(e) {
      logger.log("collection:generalSettings error : " + e , 'error', currentFileName);
      return; }
    logger.log("collection:generalSettings fetch" , 'info', currentFileName);
    data.toArray(function (err, generalSettings) {
      if(!_.isUndefined(generalSettings)){
        logger.log("if(!_.isUndefined(generalSettings)){" , 'info', currentFileName);

         logger.log("collection:users before fetch" , 'info', currentFileName);
        db['users'].findOne({_id: user._id}, function(e, userResponse) {
          if(e) {
             logger.log("collection:users error : " + e , 'error', currentFileName);
            return; }
           logger.log("collection:users fetch" , 'info', currentFileName);
          if(userResponse){
            logger.log("if(userResponse){" , 'info', currentFileName);
            if(generalSettings[0].reUsedPasswordsLimit > 0 && !_.isUndefined(userResponse.passwordLog) && userResponse.passwordLog.length > 0){
              logger.log("if(generalSettings[0].reUsedPasswordsLimit > 0 && !_.isUndefined(userResponse.passwordLog) && userResponse.passwordLog.length > 0){" , 'info', currentFileName);
              _.forEach(userResponse.passwordLog, function (item) {
                if(item === crypto.createHash('sha1').update(user.newPassword).digest('hex')){
                  logger.log("if(item === crypto.createHash('sha1').update(user.newPassword).digest('hex')){" , 'info', currentFileName);
                  cb(null, {code: 400.1, message: generalSettings[0].reUsedPasswordsMessage});
                  return;
                }
              });
              setAuditLogs(userResponse, _.cloneDeep(user), userResponse, ip.address(), user.performedAction);
            }
            cb(null, user);
          }
        });
      }
    });
  });
  logger.log("function: validateNewPassword - end" , 'info', currentFileName);
}

// Utility functions start
function getRandomPassword() {
  logger.log("function: getRandomPassword - start and end with return" , 'info', currentFileName);
  return randomstring.generate({
    length: 12,
    charset: 'alphabetic'
  });
}

function getExpireDateFromGeneralSettings(req, res, next) {
  logger.log("function: getExpireDateFromGeneralSettings - start" , 'info', currentFileName);

  logger.log("collection:generalSettings before fetch" , 'info', currentFileName);
  db['generalSettings'].find(function(e, data) {
    if(e) {
      logger.log("collection:generalSettings error : " + e , 'error', currentFileName);
      next();}

    logger.log("collection:generalSettings fetch" , 'info', currentFileName);
    data.toArray(function (err, generalSettingsResponse) {
      var days = 0;
      if(!_.isUndefined(generalSettingsResponse[0].passwordAgingLimit)){

        logger.log("if(!_.isUndefined(generalSettingsResponse[0].passwordAgingLimit)){" , 'info', currentFileName);
        days = generalSettingsResponse[0].passwordAgingLimit;
      }
      var expInDays = days || 999;
      var dt = new Date();
      dt.setDate(dt.getDate() + expInDays);
      req.items.passwordExpiresDate = dt;
      next();
    });
  });
}

function expiryDate(days) {
  logger.log("function: expiryDate - start" , 'info', currentFileName);
  var expInDays = days || 3;
  var dt = new Date();
  dt.setDate(dt.getDate() + expInDays);

  logger.log("function: expiryDate - end" , 'info', currentFileName);
  return dt;
}
// Utility functions end

function login(creds, cb) {
  logger.log("function: login - start" , 'info', currentFileName);

  var ip = creds.ip;
  var hostname = creds.hostname;
  debug('Attempting login routine with');
  debug({
    username: creds.username.toLowerCase(),
    password: "<RETRACTED>",
    crypto: crypto.createHash('sha1').update(creds.password).digest('hex')
  });

  var loginCredsObj = {
    username: creds.username.toLowerCase(),
    password: crypto.createHash('sha1').update(creds.password).digest('hex'),
    isActive: true,
    status: { $in: ['active', 'signingup', 'recovering-password', 'reseting-password'] }
  }

  logger.log("collection:users before fetch" , 'info', currentFileName);
  db.users.findOneAsync({
    username: creds.username.toLowerCase(),
  }).then(function (resUser) {

    logger.log("collection:users fetch" , 'info', currentFileName);


    if (resUser && resUser.status == 'reseting-password') {
      logger.log("if (resUser && resUser.status == 'reseting-password') {" , 'info', currentFileName);
      loginCredsObj = {
        username: creds.username.toLowerCase(),
        $or: [{temporaryPassword: creds.password}, {password: crypto.createHash('sha1').update(creds.password).digest('hex')}],
        isActive: true,
        status: { $in: ['active', 'signingup', 'recovering-password', 'reseting-password'] }
      }
    }


    logger.log("collection:users before fetch" , 'info', currentFileName);
    db.users.findOneAsync(loginCredsObj).then(function usersFindOneAsyncThenCallback(user) {

      logger.log("collection:users fetch" , 'info', currentFileName);
      if(!user){
        logger.log("if(!user){" , 'info', currentFileName);
        debug.err('No active user found with usename: ' + creds.username.underline + ' and password: <RETRACTED>');

        logger.log("collection:users before fetch" , 'info', currentFileName);
        db.users.findOneAsync({
          username: creds.username.toLowerCase(),
        }).then(function (existUser) {

          logger.log("collection:users fetch" , 'info', currentFileName);
          if(existUser != null && !_.isUndefined(existUser)) {
            logger.log("if(existUser != null && !_.isUndefined(existUser)) {" , 'info', currentFileName);

            if (!existUser.isActive) {
              logger.log("if (!existUser.isActive) {" , 'info', currentFileName);
              cb(null,{ code: 418, message: 'Sorry, your account has been locked. Please contact your administrator to activate your account.'});
            }else {
              logger.log("else of if (!existUser.isActive) {" , 'info', currentFileName);
              var rolesArray = _.map(existUser.roles, function (role) {
                return _.result(role, ['title']);
              });

              logger.log("collection:loginattempts before fetch" , 'info', currentFileName);
              db.loginattempts.insertAsync({
                username: existUser.firstname + ' ' + existUser.lastname + ' (' + existUser.username.toLowerCase() + ')',
                name: existUser.firstname + ' ' + existUser.lastname,
                date: new Date(),
                access: 'denied',
                ip : ip,
                hostName : hostname,
                isAuthenticatedUser: false,
                roles: rolesArray.join(", "),
                navigatorInformation:creds.navigatorInformation,
                navigatorInfoInString: creds.navigatorInfoInString
              }).then(function loginattemptsInsertAsyncThenCallback(log) {
                logger.log("collection:loginattempts fetch" , 'info', currentFileName);

                debug.warn('Access denied to ' + creds.username.underline + ' at ' + (new Date() +'').underline );

                logger.log("collection:generalSettings before fetch" , 'info', currentFileName);
                db['generalSettings'].find(function(e, data) {
                  if(e) {
                    logger.log("collection:generalSettings error : " + e , 'error', currentFileName);
                    return; }

                  logger.log("collection:generalSettings fetch" , 'info', currentFileName);
                  data.toArray(function (err, generalSettingsResponse) {
                    if(!_.isUndefined(generalSettingsResponse)){
                      var failedLoginAttemptsCount =0;
                      if (existUser.failedLoginAttempts) {
                        logger.log("if (existUser.failedLoginAttempts) {" , 'info', currentFileName);
                        failedLoginAttemptsCount = existUser.failedLoginAttempts + 1;
                      } else {
                        logger.log("else of if (existUser.failedLoginAttempts) {" , 'info', currentFileName);
                        failedLoginAttemptsCount = failedLoginAttemptsCount +1;
                      }

                      if(generalSettingsResponse[0].failedLoginAttemptsLimit > 0 && failedLoginAttemptsCount >= generalSettingsResponse[0].failedLoginAttemptsLimit){

                        logger.log("if(generalSettingsResponse[0].failedLoginAttemptsLimit > 0 && failedLoginAttemptsCount >= generalSettingsResponse[0].failedLoginAttemptsLimit){" , 'info', currentFileName);
                        if (_.isUndefined(existUser.allowAfterfailedLoginAttempts) || !existUser.allowAfterfailedLoginAttempts) {

                          logger.log("if (_.isUndefined(existUser.allowAfterfailedLoginAttempts) || !existUser.allowAfterfailedLoginAttempts) {" , 'info', currentFileName);


                          db.users.findOneAndUpdate({
                            username: creds.username.toLowerCase()
                          },{
                            $set: {
                              isActive: false,
                              failedLoginAttempts : failedLoginAttemptsCount
                            }
                          });
                          inactiveExistingSet(existUser);
                          cb(null, {code: 400.1, message: generalSettingsResponse[0].failedLoginAttemptsLimitMessage, isConCurrentUserActive: false});
                        }else {

                          logger.log("else of if (_.isUndefined(existUser.allowAfterfailedLoginAttempts) || !existUser.allowAfterfailedLoginAttempts) {" , 'info', currentFileName);

                          cb(new Error('Login failed'));
                        }
                      } else {

                        logger.log("else of if(generalSettingsResponse[0].failedLoginAttemptsLimit > 0 && failedLoginAttemptsCount >= generalSettingsResponse[0].failedLoginAttemptsLimit){" , 'info', currentFileName);

                        db.users.findOneAndUpdate({
                          username: creds.username.toLowerCase()
                        },{
                          $set: {
                            failedLoginAttempts : failedLoginAttemptsCount
                          }
                        });
                        if (!existUser.isActive) {
                          logger.log("if (!existUser.isActive) {" , 'info', currentFileName);

                          cb(null,{ code: 418, message: 'Sorry, your account has been locked. Please contact your administrator to activate your account.'});
                        }else if (generalSettingsResponse[0].failedLoginAttemptsLimit > 0 && failedLoginAttemptsCount < generalSettingsResponse[0].failedLoginAttemptsLimit && (_.isUndefined(existUser.allowAfterfailedLoginAttempts) || !existUser.allowAfterfailedLoginAttempts)) {

                          logger.log("else if (generalSettingsResponse[0].failedLoginAttemptsLimit > 0 && failedLoginAttemptsCount < generalSettingsResponse[0].failedLoginAttemptsLimit && (_.isUndefined(existUser.allowAfterfailedLoginAttempts) || !existUser.allowAfterfailedLoginAttempts)) {" , 'info', currentFileName);


                          var forgotPasswordLimitMessaage = 'Incorrect Username and/or Password. This is attempt number ' + (parseInt(failedLoginAttemptsCount)) + '. Per Sytem Settings, you will be locked out after '  + (parseInt(generalSettingsResponse[0].failedLoginAttemptsLimit))+ ' consecutive incorrect password attempts.';
                          cb(existUser,{ code: 418, message: forgotPasswordLimitMessaage, forgotPasswordLimit: generalSettingsResponse[0].failedLoginAttemptsLimit - 1});
                        }else {
                          logger.log("else of if (!existUser.isActive) {" , 'info', currentFileName);
                          cb(new Error('Login failed'));
                        }
                      }
                    }
                  });
                });
              }).error(function loginattemptsInsertAsyncErrorCallback(err) {
                logger.log("collection:loginattempts error : " +err , 'error', currentFileName);
                debug.warn('Failed to log un-sucessful login attempt by ' + creds.username.underline + ' at ' + (new Date()+'').underline );
              });
            }

          }else {
            logger.log("else of if(existUser != null && !_.isUndefined(existUser)) {" , 'info', currentFileName);
            cb(new Error('Login failed'));
          }
        });
      } else {

        logger.log("else of if(!user){" , 'info', currentFileName);

        //by surjeet.b@productivet.com to check for the permission of the users based on roles assigned to them.
        //if any one of assigned role of the user is active then only user will allowed to login into the system else will denied.
        var assignedRoleIsactiveCount = 0;

        logger.log("collection:roles before fetch" , 'info', currentFileName);
        db.generalSettings.find({}).toArray(function (err, gsData) {
          if(err) {
            logger.log("collection:generalSettings error : " + e , 'error', currentFileName);
            cb(e);
          } else {
        db.roles.find({}).toArray(function (err, data) {

          logger.log("collection:roles fetch" , 'info', currentFileName);
          _.forEach(user.roles, function (role) {
            if (_.find(data, {'_id': db.ObjectID(role._id)}) && _.find(data, {'_id': db.ObjectID(role._id)}).isActive === true ) {
              assignedRoleIsactiveCount++;
            }
          })
          if (assignedRoleIsactiveCount > 0) {

            logger.log("if (assignedRoleIsactiveCount > 0) {" , 'info', currentFileName);
            //debug('Found following user');
            //debug(user);
            delete user.password;
            delete user.securityAnswer;
            delete user.verificationCode;

            var rolesArray = _.map(user.roles, function (role) {
              return _.result(role, ['title']);
            });

              var expInDays = 999;
              if(!_.isUndefined(gsData[0].passwordAgingLimit)){
                expInDays = gsData[0].passwordAgingLimit || 999;
              }
            
              if(user.passwordModifiedDate && moment(user.passwordModifiedDate).add(expInDays, 'days') < new Date()){
                logger.log("if(user.passwordExpiresOn ? user.passwordExpiresOn < new Date() : false) {" , 'info', currentFileName);
                user.status = 'password-expired';
              } else if(user.passwordExpiresOn ? user.passwordExpiresOn < new Date() : false) {
                logger.log("if(user.passwordExpiresOn ? user.passwordExpiresOn < new Date() : false) {" , 'info', currentFileName);
                user.status = 'password-expired';
              } else if (!user.passwordExpiresOn && user.loggedInDateTime && moment(user.loggedInDateTime).add(expInDays, 'days') < new Date()) {
                logger.log("if(user.passwordExpiresOn ? user.passwordExpiresOn < new Date() : false) {" , 'info', currentFileName);
                user.status = 'password-expired';
              }


            db.users.findOneAndUpdate({
              username: creds.username.toLowerCase()
            },{
              $set: {
                failedLoginAttempts: 0,
                loggedInDateTime : new Date(),
                ip : ip
              }
            });

            if(user.status === 'signingup' || user.status === 'recovering-password' || user.status === 'password-expired'){

              logger.log(`if(user.status === 'signingup' || user.status === 'recovering-password' || user.status === 'password-expired'){` , 'info', currentFileName);
             
              insertLoginAttempts(user, 'granted', creds);

              if(user.status === 'password-expired') {
                logger.log("if(user.status === 'password-expired') {" , 'error', currentFileName);

                if (gsData && gsData.length) {
                  cb(null,{
                    _id: user._id,
                    username: user.username.toLowerCase(),
                    firstname: user.firstname,
                    lastname: user.lastname,
                    email: user.email,
                    code: db.ObjectID(),
                    status: user.status,
                    passwordAgingMessage: gsData[0].passwordAgingMessage,
                    isConCurrentUserActive: user.isConCurrentUserActive
                  });
                } else {
                  cb(null,{
                    _id: user._id,
                    username: user.username.toLowerCase(),
                    firstname: user.firstname,
                    lastname: user.lastname,
                    email: user.email,
                    code: db.ObjectID(),
                    status: user.status,
                    isConCurrentUserActive: user.isConCurrentUserActive
                  });
                }
              } else {

                logger.log("else of if(user.status === 'password-expired') {" , 'info', currentFileName);

                cb(null,{
                  _id: user._id,
                  username: user.username.toLowerCase(),
                  firstname: user.firstname,
                  lastname: user.lastname,
                  email: user.email,
                  code: user.code.toString('hex'),
                  status: user.status,
                  isConCurrentUserActive: user.isConCurrentUserActive
                });
              }
            } else if(user.status === 'reseting-password'){
              logger.log("else if(user.status === 'reseting-password'){" , 'info', currentFileName);

              //step2 - for reset password by surjeet.b@productivet.com

              insertLoginAttempts(user, 'granted', creds);

              var resetUserCbObj = {
                _id: user._id,
                username: user.username.toLowerCase(),
                firstname: user.firstname,
                lastname: user.lastname,
                email: user.email,
                forgotPasswordCode: user.forgotPasswordCode.toString('hex'),
                status: user.status,
                isConCurrentUserActive: user.isConCurrentUserActive
              };
              if (creds.password == resUser.temporaryPassword) {
                logger.log(`if (creds.password == resUser.temporaryPassword) {` , 'info', currentFileName);

                resetUserCbObj = {
                  _id: user._id,
                  username: user.username.toLowerCase(),
                  firstname: user.firstname,
                  lastname: user.lastname,
                  email: user.email,
                  status: user.status,
                  roles: user.roles,
                  applicableSites: user.applicableSites,
                  securityQuestion: user.securityQuestion,
                  passwordExpiresOn: user.passwordExpiresOn,
                  isConCurrentUserActive: user.isConCurrentUserActive
                };
                if(user.isConCurrentUserActive) {

                  logger.log(`if(user.isConCurrentUserActive) {` , 'info', currentFileName);

                  cb(null, {code: 403.1, message: 'We regret to inform you that your session has been ended by ' + user.forceRemovedUser });
                } else {
                  logger.log(`else of if(user.isConCurrentUserActive) {` , 'info', currentFileName);
                  conCurrentUserFunctionality(user, resetUserCbObj, cb);
                }
              }else {
                logger.log(`else of if (creds.password == resUser.temporaryPassword) {` , 'info', currentFileName);
                cb(null,resetUserCbObj);
              }
            } else {

              logger.log(`else of if(user.status === 'signingup' || user.status === 'recovering-password' || user.status === 'password-expired'){` , 'info', currentFileName);

              var userObject = {
                _id: user._id,
                username: user.username.toLowerCase(),
                firstname: user.firstname,
                lastname: user.lastname,
                email: user.email,
                status: user.status,
                roles: user.roles,
                applicableSites: user.applicableSites,
                securityQuestion: user.securityQuestion,
                passwordExpiresOn: user.passwordExpiresOn,
                isConCurrentUserActive: user.isConCurrentUserActive
              };

              if(user.isConCurrentUserActive) {
                logger.log(`if(user.isConCurrentUserActive) {` , 'info', currentFileName);
                cb(null, {code: 403.1, message: 'We regret to inform you that your session has been ended by ' + user.forceRemovedUser });
              } else {
                logger.log(`else of if(user.isConCurrentUserActive) {` , 'info', currentFileName);
                conCurrentUserFunctionality(user, userObject, cb);
                insertLoginAttempts(user, 'granted', creds);
              }
            }
          } else {
            logger.log("else of if (assignedRoleIsactiveCount > 0) {" , 'info', currentFileName);

            debug.err('Permission denied ' + user.username.toLowerCase() + ' have no active roles.');
            cb('User have no active roles.')
          }
        });
      }
      });

      }
    }).catch(function usersFindOneAsyncErrorCallback(err) {

      logger.log("usersFindOneAsyncErrorCallback error : " + err , 'error', currentFileName);
      cb(err);
    });




  }).catch(function (err) {

    //midpoint
    //something went wrong..
    cb(err);
  });

  logger.log("function: login - end" , 'info', currentFileName);
}

function insertLoginAttempts(user, access, credentials) {
  logger.log("collection:loginattempts before fetch", 'info', currentFileName);

  var rolesArray = _.map(user.roles, function (role) {
    return _.result(role, ['title']);
  });
  
  return db.loginattempts.insertAsync({
    username: user.firstname + ' ' + user.lastname + ' (' + user.username.toLowerCase() + ')',
    email: user.email,
    name: user.firstname + ' ' + user.lastname,
    date: new Date(),
    access: access,
    ip: credentials.ip,
    hostName: credentials.hostname,
    isAuthenticatedUser: true,
    roles: rolesArray.join(", "),
    navigatorInformation: credentials.navigatorInformation,
    navigatorInfoInString: credentials.navigatorInfoInString
  })
  .then(function insertLoginAttemptsAsyncCallback(log) {
    logger.log("collection:loginattempts fetch", 'info', currentFileName);
    log = log.ops[0];
    debug('Access granted to ' + log.username.underline + ' at ' + (log.date + '').underline);
    return log;
  }).error(function insertLoginAttemptsAsyncErrorCallback(err) {
    logger.log("collection:loginattempts error : " + err, 'error', currentFileName);
    debug('Failed to log sucessful login attempt by ' + user.username.underline + ' at ' + (user.date + '').underline);
  });
}

function inactiveExistingSet(existUser) {
  db.collection('sets')
    .findOneAsync({
      title: 'Existing Users'
    }, {
      'title': 1,
      'values': 1,
      'isActive': 1
    }).then(function (result) {
      var hasExistUser = _.findIndex(result.values, function (o) { return o.title.toLowerCase() == existUser.username.toLowerCase() });
      if (hasExistUser >= 0) {
        result.values[hasExistUser].isActive = false;
      }
      db.collection('sets')
        .findOneAndUpdateAsync({
          title: 'Existing Users'
        }, {
          $set: {
            values: result.values
          }
        }, {
          returnOriginal: true
          }).then(function (response) {
           logger.log(existUser.username.toLowerCase() + ' is inactivated.' , 'info', currentFileName);
        })
    })
}

function beginSignUp(user, cb) {

  return db.users
    .findOneAsync({
      email: user.email,
      username: user.username.toLowerCase()
    })
    .then(function usersFindOneAsyncThenCallback(u) {
      if (u) {
        debug.warn(user.email.underline + ' is already registered.');
        cb('user is already registered.');
      } else {
        user.code = db.ObjectID();
        user.status = 'signingup';

        if (user.passwordGenerationMethod === 'System') {
          user.password = getRandomPassword();
        }

        var tempPassword = user.password;
        user.temporaryPassword = user.password;

        user.password = crypto.createHash('sha1').update(user.password).digest('hex');
        user.verificationCode = crypto.createHash('sha1').update(user.verificationCode).digest('hex');
        user.verificationCodeModifiedDate = new Date();

        usersResource.create(user, function usersInsertCallback(e, u) {
          cb(e, u);
          if (e) {
            debug.err('Failed registering the user:');
            debug.err(user);
            debug.err(e);
            cb(e);
            return;
          } else {
            u.tempPassword = tempPassword;

            _.merge(u, settings);

            log.log('inside file: registration.js - function: beginSignUp - status: sending mail','debug');

            mailer.send('begin-signup', u, user.email, function sendMailCallback(e, b) {
              if (e) {
                debug.err('Failed sending email - begin-signup');
                debug.err(user);
                debug.err(e);
              } else {
                debug(user.email.underline + ' got registered sucessfully');
              }
            });
          }
        });
      }
    })
    .catch(function usersFindOneAsyncErrorCallback(err) {
      debug.err(err);
      cb(err);
    });
}

function finishSignUp(user, cb) {
  //code,securityQuestion,securityAnswer,password
  debug('Initiating search for entry with email: ' + user.email.underline + ' and code(signingup): ' + user.code.underline);
  var setEmail = user.email;
  if (user.newEmail && user.newEmail != '') {
    setEmail = user.newEmail;
  }
  db.users.findOne({
    email: user.email,
    code: db.ObjectID(user.code),
    status: 'signingup'
  }, function (err, currentUser) {
  db.users.findOneAndUpdateAsync({
    email: user.email,
    code: db.ObjectID(user.code),
    status: 'signingup'
  },
    {
      $set: {
        email: setEmail,
        securityQuestion: user.securityQuestion,
        securityAnswer: crypto.createHash('sha1').update(user.securityAnswer).digest('hex'),
        status: 'active',
        password: crypto.createHash('sha1').update(user.password).digest('hex'),
        temporaryPassword: user.password,
        verificationCode: crypto.createHash('sha1').update(user.verificationCode).digest('hex'),
        verificationCodeModifiedDate: new Date(),
        passwordModifiedDate: new Date(),
        securityAnswerModifiedDate: new Date(),
        passwordExpiresOn: user.passwordExpiresDate,
        modifiedDate: new Date(),
        modifiedBy: currentUser._id + '',
        modifiedByName: (currentUser.firstname || '').concat(' ').concat(currentUser.lastname || '').concat(' (').concat(currentUser.lastname || '').concat(')')
      },
      $unset: {
        newEmail: '',
        code: '',
        passwordGenerationMethod: ''
      }
    }, {
      returnOriginal: false
    }).then(function usersFindOneAndUpdateThenCallback(value) {
      if (value && value.lastErrorObject && value.lastErrorObject.updatedExisting) {
        setAuditLogs(_.cloneDeep(value.value), _.cloneDeep(value.value), _.cloneDeep(value.value), ip.address());// PQT-4305_.cloneDeep(value.value.ip)); // QC3-9624 - Issue 4 - Jyotil

        delete value.value.securityAnswer;
        delete value.value.password;
        delete value.value.verificationCode;
        cb(null, _.cloneDeep(value.value));//QC3-7614 bug by bhavin correct spelling mistake of cloneDeep

        _.merge(value.value, settings);
        log.log('inside file: registration.js - function: finishSignUp - status: sending mail','debug');

        debug('User successfully signed up. Now Sending email to ' + value.value.email.underline);
        mailer.send('finish-signup', value.value, value.value.email, function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email to ' + value.value.email.underline);
            debug.err(e);
          } else {
            debug(value.value.email.underline + ' got registered sucessfully.');
          }
        });
      } else {
        debug.err(user.code.underline + ' is either invalid, or it does not belong to ' + user.email.underline + ' or it has ' + 'expired'.underline);
        cb('Please make sure the code is valid.')
      }
    }).catch(function usersFindOneAndUpdateErrorCallback(err) {
      debug.err(err);
      cb(err);
    });
  });
}

function dateDiff(start, end) {
  var __startTime = moment(start).format();
  var __endTime = moment(end || new Date()).format();

  var __duration = moment.duration(moment(__endTime).diff(__startTime));
  var __hours = __duration.asHours();
  return __hours
}

function getUserByUserNameAndForgotPasswordCode(user, cb) {
  var self = user;
  debug('Fetching user ' + user.username.toLowerCase() + ' with code ' + user.forgotPasswordCode + ' for recovery');
  db.users.findOneAsync({
    $and: [{$or: [{
      username: user.username.toLowerCase()
    },{
      _id: user.username.length === 24 ? db.ObjectID(user.username.toLowerCase()) : user.username.toLowerCase()
    }]},{$or: [{
      code: db.ObjectID(user.forgotPasswordCode)
    }, {
      forgotPasswordCode: db.ObjectID(user.forgotPasswordCode)
    },{
      emailChangeCode: db.ObjectID(user.forgotPasswordCode)
    }]
    }]
  })
    .then(function findOneUserByNameAndForgotPasswordCodeCallback(user) {

      if (user && new Date(user.passwordModifiedDate) < new Date(user.performedActionOn) && self.tempAction != 'emailChanged' && user.performedAction == 'Reset User Password'  && dateDiff(user.performedActionOn) > (config.linkExpiresInHours || 1)) {
        cb('Invalid username or code or code is expired');
        return;
      }

      if (!user || (user.performedAction && user.performedActionOn && user.performedAction == 'Reset User Access' && self.tempAction != 'emailChanged'
        && (dateDiff(user.performedActionOn) > (config.linkExpiresInHours || 1)  || !user.code))) {
        cb('Invalid username or code or code is expired');
        return;
      }

      cb(null, _.pick(user, '_id', 'username', 'firstname', 'lastname', 'securityQuestion', 'email', 'newEmail'));
    })
    .catch(function handleFindOneUserByNameAndForgotPasswordCode(err) {
      cb('Invalid username or code or code is expired');
    });
}


function resendCode(user, cb) {
  debug('Fetching user ' + user._id + ' with code ' + user.code + ' for resend (email) code');

  if (typeof user._id === 'string' && user._id.length === 24) {
    user._id = db.ObjectID(user._id);
  }

  db.users.findOneAsync({
    _id: user._id,
    emailChangeCode: db.ObjectID(user.code)
  })
    .then(function findOneForResendCodeCallback(user) {
      if (!user) {
        cb('Invalid username or code or code is expired');
        return;
      }

      debug('Email change request for ' + user.username.toLowerCase()
        + ' from ' + user.email + ' to ' + user.newEmail);

      _.merge(user, settings);
      log.log('inside file: registration.js - function: resendCode - status: sending mail 1','debug');

      //Send verification Email to newEmail
      mailer.send('begin-change-email', user, user.newEmail, function sendMailCallback(e, b) {
        if (e) {
          debug.err('Failed sending email - begin-change-email');
          debug.err(user);
          debug.err(e);
        } else {
          debug(user.newEmail.underline + ' verification mail sent sucessfully');
        }
      });

      log.log('inside file: registration.js - function: beginSignUp - status: sending mail 2','debug');
      //Send notification Email to currentEmail
      mailer
        .send('begin-change-email-notify',
        user,
        user.email,
        function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email - begin-signup');
            debug.err(user);
            debug.err(e);
          } else {
            debug(user.email.underline
              + ' notified sucessfully of the email change'
              + ' to ' + user.newEmail.underline
            );
          }
        });
      cb(null, _.pick(user, '_id', 'username', 'firstname', 'lastname', 'securityQuestion'));
    })
    .catch(function findOneForResendCodeSchemaCallback(err) {
      cb('Invalid username or code or code is expired');
    });
}


//by surjeet.b@productivet.com...
function unverifiedUserMailSend(user, cb) {
  debug('Fetching user ' + user._id + ' with code ' + user.code + ' for resend (email) code');
  if (typeof user._id === 'string' && user._id.length === 24) {
    user._id = db.ObjectID(user._id);
  }

  db.users.findOneAsync({
    _id: user._id,
  })
    .then(function findOneForResendCodeCallback(u) {
      if (!user) {
        cb('Invalid username or code or code is expired');
        return;
      }

      _.merge(u, settings);
      u.tempPassword = u.temporaryPassword;
      log.log('inside file: registration.js - function: unverifiedUserMailSend - status: sending mail','debug');
      var toMail = u.email;
      if (u.newEmail && u.newEmail != '' && u.status == 'signingup') {
        toMail = u.newEmail;
      }
      mailer.send('begin-signup', u, toMail, function sendMailCallback(e, b) {
        if (e) {
          debug.err('Failed sending email - begin-signup');
          debug.err(u);
          debug.err(e);
        } else {
          debug(u.email + ' got registered sucessfully');
        }
      });

      cb(null, {'toMail': toMail});
    })
    .catch(function findOneForResendCodeSchemaCallback(err) {
      cb('Invalid username or code or code is expired');
    });
}

function abortCode(user, cb) {
  debug('Fetching user ' + user._id + ' with (email) code ' + user.code + ' for abort');

  if (typeof user._id === 'string' && user._id.length === 24) {
    user._id = db.ObjectID(user._id);
  }


  db.users.findOneAndUpdateAsync({
    _id: user._id,
    emailChangeCode: db.ObjectID(user.code)
  },
    {
      $unset: {
        emailChangeCode: '',
        emailChangeCodeExpiresOn: '',
        newEmail: ''
      }
    }, {
      returnOriginal: false
    })
    .then(function findOneForAbortCodeCallback(user) {
      if (!user || !user.value) {
        cb('Invalid username or code or code is expired');
        return;
      }
      user = user.value;
      debug(user);
      cb(null, _.pick(user, '_id', 'username', 'firstname', 'lastname', 'email'));
    })
    .catch(function findOneForAbortCodeSchemaCallback(err) {
      cb('Invalid username or code or code is expired');
    });
}

function beginForgetPassword(user, cb) {
  debug('Initiating search for entry with username: ' + user.username.underline);
  db.users.findOne({username: user.username.toLowerCase(), isActive: true}, function (err, currentUser) {
    var updatedInfo = {
      forgotPasswordCode: db.ObjectID(),
      modifiedDate: new Date()
    };
    if (currentUser) {
      updatedInfo.modifiedBy = currentUser._id + '';
      updatedInfo.modifiedByName = (currentUser.firstname || '').concat(' ').concat(currentUser.lastname || '').concat(' (').concat(currentUser.lastname || '').concat(')');
    }
  db.users.findOneAndUpdateAsync({
    username: user.username.toLowerCase(),
    status: { $in: ['active', 'reseting-password'] },
    isActive: true
  },
    {
      $set: updatedInfo
    }, {
      returnOriginal: false
    }).then(function usersFindOneAndUpdateThenCallback(value) {
      if (value && value.lastErrorObject && value.lastErrorObject.updatedExisting) {

        delete value.value.securityAnswer;
        delete value.value.password;
        delete value.value.verificationCode;
        setAuditLogs(value.value, _.cloneDeep(user), currentUser, value.value.ip, 'Forgot Password and Set up a new password');
        cb(null, value.value);

        _.merge(value.value, settings);
        log.log('inside file: registration.js - function: beginForgetPassword - status: sending mail','debug');

        debug('User successfully signed up. Now Sending email to ' + value.value.email.underline);
        mailer.send('begin-forgot-password', value.value, value.value.email, function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email to ' + value.value.email.underline);
            debug.err(e);
          } else {
            debug(value.value.email.underline + ' forgot password request sent sucessfully.');
          }
        });
      } else {
        debug.err(user.username.toLowerCase() + ' is either invalid or is ' + 'inactive'.underline);
        cb('Please make sure the username is valid.')
      }
    }).catch(function usersFindOneAndUpdateErrorCallback(err) {
      debug.err(err);
      cb(err);
    });
  });
}

function finishChangePassword(user, cb) {
  db.users.findOne(db.ObjectID(user._id), function (err, currentUser) {
  db.users.findOneAndUpdateAsync({
    _id: user._id,
    status: { $in: ['active', 'reseting-password'] }
  },
    {
      $set: {
        password: crypto.createHash('sha1').update(user.newPassword).digest('hex'),
        passwordModifiedDate: new Date(),
        passwordExpiresOn: user.passwordExpiresDate,
        modifiedDate: new Date(),
        status: 'active',
        isConCurrentUserActive: false
      },
      $unset: {
        passwordGenerationMethod: ''
      }
    }, {
      returnOriginal: false
    }).then(function (value) {
      debug(value);
      if (value && value.lastErrorObject && value.lastErrorObject.updatedExisting) {

        delete value.value.securityAnswer;
        delete value.value.password;
        delete value.value.verificationCode;
        setAuditLogs(_.cloneDeep(value.value), _.cloneDeep(user), currentUser, value.value.ip, 'Reset Password (Due to Password Expired)');

        cb(null, value.value);

        _.merge(value.value, settings);

        // debug('User successfully signed up. Now Sending email to ' + value.value.email.underline);
        // mailer.send('finish-forgot-password',value.value,value.value.email,function sendMailCallback(e,b) {
        //   if(e){
        //     debug.err('Failed sending email to ' + value.value.email.underline);
        //     debug.err(e);
        //   }else{
        //     debug(value.value.email.underline + ' password recovered sucessfully.');
        //   }
        // });
      } else {
        debug.err(user._id + ' is either invalid or is ' + 'inactive'.underline);
        cb('Please make sure the username is valid.')
      }
    }).catch(function usersFindOneAndUpdateErrorCallback(err) {
      debug.err(err);
      cb(err);
    });
  });
}

function finishForgetPassword(user, cb) {
  if (user.newPassword !== user.confirmPassword) {
    cb('Passwords do not match');
    return;
  }
  debug('Initiating search for entry with username: ' + user.username.underline + ' and code(forgotPasswordCode): ' + user.code.underline);
  debug({
    username: user.username.toLowerCase(),
    forgotPasswordCode: db.ObjectID(user.code),
    securityQuestion: user.securityQuestion,
    securityAnswer: crypto.createHash('sha1').update(user.securityAnswer).digest('hex'),
    status: 'active'
  });
  db.users.findOne({
    username: user.username.toLowerCase(),
    status: {
      $in: ['active', 'reseting-password']
    }
    }, function (err, currentUser) {
  db.users.findOneAndUpdateAsync({
    username: user.username.toLowerCase(),
    forgotPasswordCode: db.ObjectID(user.code),
    securityQuestion: user.securityQuestion,
    securityAnswer: crypto.createHash('sha1').update(user.securityAnswer).digest('hex'),
    status: { $in: ['active', 'reseting-password'] }
  },
    {
      $set: {
        password: crypto.createHash('sha1').update(user.newPassword).digest('hex'),
        temporaryPassword: user.newPassword,
        passwordModifiedDate: new Date(),
        passwordExpiresOn: user.passwordExpiresDate,
        modifiedDate: new Date(),
        status: 'active',
        modifiedBy: currentUser._id + '',
        modifiedByName: (currentUser.firstname || '').concat(' ').concat(currentUser.lastname || '').concat(' (').concat(currentUser.lastname || '').concat(')')
      },
      $unset: {
        forgotPasswordCode: '',
        passwordGenerationMethod: ''
      }
    }, {
      returnOriginal: false
    }).then(function usersFindOneAndUpdateThenCallback(value) {
      debug(value);
      if (value && value.lastErrorObject && value.lastErrorObject.updatedExisting) {

        delete value.value.securityAnswer;
        delete value.value.password;
        delete value.value.verificationCode;

        cb(null, value.value);

        _.merge(value.value, settings);
        log.log('inside file: registration.js - function: finishForgetPassword - status: sending mail','debug');

        debug('User successfully signed up. Now Sending email to ' + value.value.email.underline);
        mailer.send('finish-forgot-password', value.value, value.value.email, function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email to ' + value.value.email.underline);
            debug.err(e);
          } else {
            debug(value.value.email.underline + ' password recovered sucessfully.');
          }
        });
      } else {
        debug.err(user.username.toLowerCase() + ' is either invalid or is ' + 'inactive'.underline);
        cb('Please make sure the username is valid.')
      }
    }).catch(function usersFindOneAndUpdateErrorCallback(err) {
      debug.err(err);
      cb(err);
    });
  });
}


function beginForgetESignature(user, cb) {
  debug('Initiating search for entry with username: ' + user.username.underline);
  db.users.findOne({username: user.username.toLowerCase(),
    isActive: true}, function (err, currentUser) {
  db.users.findOneAndUpdateAsync({
    username: user.username.toLowerCase(),
    //akashdeep.s - QC3-6062 - checking for reseting-password as well
    status: { $in: ['active', 'reseting-password'] },
    isActive: true
  },
    {
      $set: {
        forgotESignatureCode: db.ObjectID(),
        modifiedDate: new Date()
      }
    }, {
      returnOriginal: false
    }).then(function (value) {
      if (value && value.lastErrorObject && value.lastErrorObject.updatedExisting) {
        setAuditLogs(value.value, _.cloneDeep(user), currentUser, value.value.ip, 'Forgot eSignature PIN');
        cb(null, value.value);

        _.merge(value.value, settings);
        log.log('inside file: registration.js - function: beginForgetESignature - status: sending mail','debug');

        debug('User successfully signed up. Now Sending email to ' + value.value.email.underline);
        mailer.send('begin-forgot-esignature', value.value, value.value.email, function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email to ' + value.value.email.underline);
            debug.err(e);
          } else {
            debug(value.value.email.underline + ' forgot password request sent sucessfully.');
          }
        });
      } else {
        debug.err(user.username.toLowerCase() + ' is either invalid or is ' + 'inactive'.underline);
        cb('Please make sure the username is valid.')
      }
    }).catch(function usersFindOneAndUpdateErrorCallback(err) {
      debug.err(err);
      cb(err);
    });
  });
}
function getUserByUserNameAndForgotESignatureCode(user, cb) {
  debug('Fetching user ' + user.username.toLowerCase() + ' with code ' + user.forgotESignatureCode + ' for recovery');

  db.users.findOneAsync({
    $and: [{$or: [{
      username: user.username.toLowerCase()
    }, {
      _id: user.username.length === 24 ? db.ObjectID(user.username.toLowerCase()) : user.username.toLowerCase()
    }]},{$or: [{
      code: db.ObjectID(user.forgotESignatureCode)
    }, {
      forgotESignatureCode: db.ObjectID(user.forgotESignatureCode)
    },
    {
      emailChangeCode: db.ObjectID(user.forgotESignatureCode)
    }]
    }]
  })
    .then(function (user) {
      if (!user) {
        cb('Invalid username or code or code is expired');
        return;
      }

      cb(null, _.pick(user, '_id', 'username', 'firstname', 'lastname', 'securityQuestion', 'email', 'newEmail'));
    })
    .catch(function handleFindOneUserByNameAndForgotPasswordCode(err) {
      cb('Invalid username or code or code is expired');
    });
}
function finishForgetESignature(user, cb) {
  if (user.newESignaturePIN != user.confirmNewESignaturePIN) {
    cb('eSignature PINs does not match');
    return;
  }
  db.users.findOne({
    username: user.username.toLowerCase(),
    isActive: true
  }, function (err, currentUser) {
  db.users.findOneAsync({
    forgotESignatureCode: db.ObjectID(user.forgotESignatureCode),
    username: user.username.toLowerCase(),
    $or: [{ temporaryPassword: user.currentPassword }, { password: crypto.createHash('sha1').update(user.currentPassword).digest('hex') }]
  }).then(function (udata) {
    if (!udata) {
      cb(null, 'mismatch data');
      return;
    }
    else {
      //update esignature pin
      db.users.findOneAndUpdateAsync({
        username: user.username.toLowerCase()
      },
        {
          $set: {
            verificationCode: crypto.createHash('sha1').update(user.newESignaturePIN).digest('hex'),
            modifiedDate: new Date(),
          }, $unset: {
            forgotESignatureCode: ''
          }
        }, {
          returnOriginal: false
        }).then(function (value) {
          setAuditLogs(value.value, _.cloneDeep(user), currentUser, value.value.ip, 'Reset eSignature PIN');
          cb(null, 'successfully updated');
        }).catch(function (err) {
          debug.err(err);
          cb(err);
        });
    }

  }).catch(function (err) {
    debug.err(err);
    cb(err);
  });
  });
}

function getOneUser(identity, cb) {
  usersResource.getOne(identity, cb);
}

function getAllUsers(paginateOptions, cb) {
  usersResource.getAll(paginateOptions, cb);
}

function finishEmailChange(user, cb) {

  //code,securityQuestion,securityAnswer,password
  debug('Initiating search for entry with email: ' + user.email.underline + ' and code(emailChangeCode): ' + user.code.underline);
  db.users.findOne({
    emailChangeCode: db.ObjectID(user.code),
    newEmail: user.newEmail,
    email: user.email,
    emailChangeCodeExpiresOn: {
      $gt: new Date()
    }}, function (err, currentUser) {
  db.users.findOneAndUpdateAsync({
    emailChangeCode: db.ObjectID(user.code),
    newEmail: user.newEmail,
    email: user.email,
    emailChangeCodeExpiresOn: { $gt: new Date() }
  },
    {
      $set: {
        email: user.newEmail,
        modifiedDate: new Date()
      },
      $unset: {
        newEmail: '',
        emailChangeCode: '',
        emailChangeCodeExpiresOn: ''
      }
    }, {
      returnOriginal: false
    }).then(function usersFindOneAndUpdateThenCallback(value) {
      if (value && value.lastErrorObject && value.lastErrorObject.updatedExisting) {
        delete value.value.securityAnswer;
        delete value.value.password;
        delete value.value.verificationCode;
        setAuditLogs(value.value, _.cloneDeep(user), currentUser, value.value.ip, "Reset My Profile");
        cb(null, value.value);
        debug('Email address changed successfully. Now Sending email to ' + value.value.email.underline);

        _.merge(value.value, settings);
        log.log('inside file: registration.js - function: finishEmailChange - status: sending mail','debug');

        mailer.send('finish-change-email', value.value, value.value.email, function sendMailCallback(e, b) {
          value.value.securityAnswer = '<RETRACTED>'
          value.value.password = '<RETRACTED>'
          if (e) {
            debug.err('Failed sending email to ' + value.value.email.underline);
            debug.err(e);
          } else {
            debug(value.value.email.underline + ' got registered sucessfully.');
          }
        });

        mailer.send('finish-change-email-notify', value.value, user.email, function sendMailCallback(e, b) {
          value.value.securityAnswer = '<RETRACTED>'
          value.value.password = '<RETRACTED>'
          if (e) {
            debug.err('Failed sending email to ' + value.value.email.underline);
            debug.err(e);
          } else {
            debug(value.value.email.underline + ' got registered sucessfully.');
          }
        });
      } else {
        debug.err(user.code.underline + ' is either invalid, or it does not relate to '
          + user.newEmail + ' change or it has ' + 'expired'.underline);
        cb('Please make sure the code is valid.')
      }
    }).catch(function usersFindOneAndUpdateErrorCallback(err) {
      debug.err(err);
      cb(err);
    });
  });
}

// function by surjeet.b@productivet.com for verifying verification code
function verifyVerificationCode(identity, cb) {
  db.users.findOneAsync({
    _id: identity._id,
    verificationCode: crypto.createHash('sha1').update(identity.verificationCode).digest('hex')
  })
    .then(function findOneForverifyVerificationCodeCallback(user) {
      if (!user) {
        cb(null, 'Invalid verification code11');
        return;
      }
      cb(null, _.pick(user, '_id', 'username', 'firstname', 'lastname', 'securityQuestion'));
    })
    .catch(function findOneForverifyVerificationCodeSchemaCallback(err) {
      cb('Invalid verification code');
    });
}

function setAuditLogs(value, user, currentUser, ip, performedAction) {
  var record = _.cloneDeep(_.pick(value, "_id", "modifiedBy", "modifiedByName", "status", "modifiedDate", "ip", "version", "versions", "email"));
  db.users.findOne({_id: (typeof record._id === 'object'? record._id: db.ObjectID(record._id))}, function (err, usr) {
    var valueToChange = record;
    if (usr) {
      valueToChange = Object.assign(usr, record);
    }
    valueToChange.modifiedDate = new Date();
    valueToChange.performedAction = performedAction || 'Reset User Password';
    if (valueToChange.performedAction && typeof valueToChange.performedAction == 'string' && valueToChange.performedAction.indexOf('Change Password')>-1) {
      valueToChange.performedAction = 'Change Password';
    }
    valueToChange.passwordGenerationMethod = user.passwordGenerationMethod;
    valueToChange.modifiedBy = currentUser._id+'';
    if (valueToChange.loggedInDateTime && typeof valueToChange.loggedInDateTime === 'string') {
      valueToChange.loggedInDateTime = new Date(valueToChange.loggedInDateTime);
    }
    valueToChange.modifiedByName = currentUser.firstname+ ' ' +currentUser.lastname + ' ('+currentUser.username+')';
    if (!valueToChange.passwordGenerationMethod) {
        delete valueToChange.passwordGenerationMethod;
      }
      if (valueToChange.performedAction == "Reset My Profile") {
        delete valueToChange.performedAction;
      }
    db['users'].auditThis1Softly(_.cloneDeep(valueToChange), ip);
  });
}

//step1 - reset password by surjeet.b@productivet.com
function beginResetPassword(user, ip, currentUser, cb) {
  var tempPassword = '';
  debug('Initiating search for entry with username: ' + user.username.underline);
  if (user.passwordGenerationMethod === 'System') {
    tempPassword = getRandomPassword();
    user.newPassword = crypto.createHash('sha1').update(tempPassword).digest('hex');
  } else {
    tempPassword = user.newPassword;
    user.newPassword = crypto.createHash('sha1').update(user.newPassword).digest('hex');
  }

  var updatedInfo = {
    passwordGenerationMethod: user.passwordGenerationMethod,
    forgotPasswordCode: db.ObjectID(),
    code: db.ObjectID(),
    passwordExpiresOn: user.passwordExpiresDate,
    modifiedDate: new Date(),
    password: user.newPassword,
    status: 'reseting-password',
    performedAction: 'Reset User Password',
    performedActionOn: new Date()
  };
  if (currentUser) {
    updatedInfo.modifiedBy = currentUser._id + '';
    updatedInfo.modifiedByName = (currentUser.firstname || '').concat(' ').concat(currentUser.lastname || '').concat(' (').concat(currentUser.lastname || '').concat(')');
  }
  db.users.findOneAndUpdateAsync({
    username: user.username.toLowerCase(),
    status: { $in: ['active', 'reseting-password'] }
  },
    {
      $set: updatedInfo
    }, {
      returnOriginal: false
    }).then(function usersFindOneAndUpdateThenCallback(value) {
      if (value && value.lastErrorObject && value.lastErrorObject.updatedExisting) {
        // auditDetails('users', true, value.value, ip);
        setAuditLogs(value.value, _.cloneDeep(user), currentUser, user.auditInfo.ip);
        delete value.value.securityAnswer;
        delete value.value.password;
        //will keep the reset password information in a collection named userResetAuditLogs..
        if (!user.auditInfo) {
          user.auditInfo = {};
        }
        user.auditInfo.resetDate = new Date();
        db.userResetAuditLogs.insertOne(user.auditInfo);
        //end will keep the reset password information in a collection named userResetAuditLogs..
        // setAuditLogs(value.value, _.cloneDeep(user), currentUser, user.auditInfo.ip); 
        delete user.auditInfo;
        cb(null, value.value);

        _.merge(value.value, settings);
        _.merge(value.value, { 'tempPassword': tempPassword });
        debug('User password successfully reset. Now Sending email to ' + value.value.email.underline);

        log.log('inside file: registration.js - function: beginResetPassword - status: sending mail','debug');

        mailer.send('begin-reset-password', value.value, value.value.email, function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email to ' + value.value.email.underline);
            debug.err(e);
          } else {
            debug(value.value.email.underline + ' reset password request sent sucessfully.');
          }
        });
      } else {
        debug.err(user.username.toLowerCase() + ' is either invalid or is ' + 'inactive'.underline);
        cb('Please make sure the username is valid.')
      }
    })
    .catch(function (err) {
      debug.err(err);
      cb(err);
    });
}

function auditDetails(entity, soft, item, ip) {
  if (soft) {
    db[entity].auditThis1Softly(_.cloneDeep(item), ip);
  } else {
    db[entity].auditThis1(_.cloneDeep(item), ip);
  }
}

// step - 2 reset password by surjeet.b@productivet.com
function finishResetPassword(user, cb) {
  if (user.newPassword !== user.confirmPassword) {
    cb('Passwords do not match');
    return;
  }
  debug('Initiating search for entry with username: ' + user.username.underline + ' and code(forgotPasswordCode): ' + user.code.underline);
  debug({
    username: user.username.toLowerCase(),
    forgotPasswordCode: db.ObjectID(user.code),
    securityQuestion: user.securityQuestion,
    securityAnswer: crypto.createHash('sha1').update(user.securityAnswer).digest('hex'),
    status: 'active'
  });
  ////
  debug('Initiating search for entry with email: ' + user.username.underline + ' and code(signingup): ' + user.code.underline);
  return db.users
  .findOneAsync({
    username: user.username.toLowerCase()
  },{_id : 1,username : 1,firstname : 1,lastname : 1})
  .then(function usersFindOneAsyncThenCallback(u) {
    if (!u) {
      debug.warn('Username' + user.username.underline + ' is not found.');
      cb('Uses not found.');
    } else {
/////

  db.users.findOneAndUpdateAsync({
    username: user.username.toLowerCase(),
    forgotPasswordCode: db.ObjectID(user.code),
    securityQuestion: user.securityQuestion,
    securityAnswer: crypto.createHash('sha1').update(user.securityAnswer).digest('hex'),
    status: 'active'
  },
    {
      $set: {
        password: crypto.createHash('sha1').update(user.newPassword).digest('hex'),
        passwordModifiedDate: new Date(),
        passwordExpiresOn: expiryDate(0),
        modifiedDate: new Date(),
        modifiedBy: u._id + '',
        modifiedByName: (u.firstname || '').concat(' ').concat(u.lastname || '').concat(' (').concat(u.lastname || '').concat(')')

      },
      $unset: {
        forgotPasswordCode: '',
        passwordGenerationMethod: ''
      }
    }, {
      returnOriginal: false
    }).then(function usersFindOneAndUpdateThenCallback(value) {
      debug(value);
      if (value && value.lastErrorObject && value.lastErrorObject.updatedExisting) {

        delete value.value.securityAnswer;
        delete value.value.password;
        delete value.value.verificationCode;

        cb(null, value.value);

        _.merge(value.value, settings);
        log.log('inside file: registration.js - function: finishResetPassword - status: sending mail','debug');

        debug('User password successfully reseted. Now Sending email to ' + value.value.email.underline);
        mailer.send('finish-forgot-password', value.value, value.value.email, function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email to ' + value.value.email.underline);
            debug.err(e);
          } else {
            debug(value.value.email.underline + ' password recovered sucessfully.');
          }
        });
      } else {
        debug.err(user.username.toLowerCase() + ' is either invalid or is ' + 'inactive'.underline);
        cb('Please make sure the username is valid.')
      }
    }).catch(function usersFindOneAndUpdateErrorCallback(err) {
      debug.err(err);
      cb(err);
    });
}
})
.catch(function usersFindOneAsyncErrorCallback(err) {
  debug.err(err);
  cb(err);
});
}

function beginResetAccess(user, ip, currentUser, cb) {
  return db.users
    .findOneAsync({
      username: user.username.toLowerCase()
    })
    .then(function usersFindOneAsyncThenCallback(u) {
      if (!u) {
        debug.warn('Username' + user.username.underline + ' is not found.');
        cb('Uses not found.');
      } else {
        // user.code =  db.ObjectID();
        // user.status =  'reseting-password';
        var updatedInfo = {
          code: db.ObjectID(),
          forgotPasswordCode: db.ObjectID(),
          status: 'active',
          modifiedDate: new Date(),
          performedAction: 'Reset User Access',
          performedActionOn: new Date()
        };
        if (currentUser) {
          updatedInfo.modifiedBy = currentUser._id + '';
          updatedInfo.modifiedByName = (currentUser.firstname || '').concat(' ').concat(currentUser.lastname || '').concat(' (').concat(currentUser.lastname || '').concat(')');
        }
        db.users.findOneAndUpdateAsync({
          username: user.username.toLowerCase()
        }, {
            $set: updatedInfo
          }, {
            returnOriginal: false
          })
          .then(function usersfindOneAndUpdateThenCallback(usr) {
            auditDetails('users', true, usr.value, ip);
            cb(null, usr.value);
            _.merge(usr.value, settings);
            mailer.send('begin-reset-access', usr.value, usr.value.email, function sendMailCallback(e, b) {
              if (e) {
                debug.err('Failed sending email - reset-access');
                debug.err(usr);
                debug.err(e);
              } else {
                debug(user.username.underline + ' reset sucessfully');
              }
            });
          })
          .catch(function usersfindOneAndUpdateThenCallback(err) {
            debug.err(err);
            cb(err);
          });
      }
    })
    .catch(function usersFindOneAsyncErrorCallback(err) {
      debug.err(err);
      cb(err);
    });
}


function finishResetAccess(user, ip, currentUser, cb) {
  //code,securityQuestion,securityAnswer,password
  // return;

  debug('Initiating search for entry with email: ' + user.username.underline + ' and code(signingup): ' + user.code.underline);
  return db.users
  .findOneAsync({
    username: user.username.toLowerCase()
  },{_id : 1,username : 1,firstname : 1,lastname : 1})
  .then(function usersFindOneAsyncThenCallback(u) {
    if (!u) {
      debug.warn('Username' + user.username.underline + ' is not found.');
      cb('Uses not found.');
    } else {
     
  //
  var userUpdatingInfo = {
    securityQuestion: user.securityQuestion,
    securityAnswer: crypto.createHash('sha1').update(user.securityAnswer).digest('hex'),
    status: 'active',
    password: crypto.createHash('sha1').update(user.password).digest('hex'),
    temporaryPassword: user.password,
    verificationCode: crypto.createHash('sha1').update(user.verificationCode).digest('hex'),
    verificationCodeModifiedDate: new Date(),
    passwordModifiedDate: new Date(),
    securityAnswerModifiedDate: new Date(),
    //passwordExpiresOn: user.passwordExpiresDate,
    modifiedDate: new Date(),
    performedAction: 'Reset User Access' //,
    // performedActionOn: new Date()

  };
  if (currentUser) {
    userUpdatingInfo.modifiedBy = u._id + '';
    userUpdatingInfo.modifiedByName = (u.firstname || '').concat(' ').concat(u.lastname || '').concat(' (').concat(u.lastname || '').concat(')');
  }
  // QC3-9663 - surjeet - commented for the said bug id
  // if (currentUser) {
  //   userUpdatingInfo.modifiedBy = currentUser._id + '';
  //   userUpdatingInfo.modifiedByName = (currentUser.firstname || '').concat(' ').concat(currentUser.lastname || '').concat(' (').concat(currentUser.lastname || '').concat(')');
  // }

  db.users.findOneAndUpdateAsync({
    username: user.username.toLowerCase(),
    code: db.ObjectID(user.code),
  },
    {
      $set: userUpdatingInfo,
      $unset: {
        code: '',
        passwordGenerationMethod: ''
      }
    }, {
      returnOriginal: true
    }).then(function usersFindOneAndUpdateThenCallback(value) {
      logger.log('Function: finishResetAccess : findOneAndUpdateAsync : success callback response' , 'info', currentFileName);
      if (value && value.lastErrorObject && value.lastErrorObject.updatedExisting) {
        auditDetails('users', true, value.value, ip);

        delete value.value.securityAnswer;
        delete value.value.password;
        delete value.value.verificationCode;

        cb(null, value.value);

        _.merge(value.value, settings);
        log.log('inside file: registration.js - function: finishResetAccess - status: sending mail','debug');

        debug('User has reset successfully. Now Sending email to ' + value.value.email.underline);
        mailer.send('finish-reset-access', value.value, value.value.email, function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email to ' + value.value.email.underline);
            debug.err(e);
          } else {
            debug(value.value.email.underline + ' reset sucessfully.');
          }
        });
      } else {
        debug.err(user.code.underline + ' is either invalid, or it does not belong to ' + user.email.underline + ' or it has ' + 'expired'.underline);
        cb('Please make sure the code is valid.')
      }
    }).catch(function usersFindOneAndUpdateErrorCallback(err) {
      debug.err(err);
      cb(err);
    });
}
})
.catch(function usersFindOneAsyncErrorCallback(err) {
  debug.err(err);
  cb(err);
});
}
////////

// function by surjeet.b@productivet.com to restrict multiple email alert
function emailRestriction(identity, cb) {
  db.users.findOneAsync(
    {
      username: identity.username.toLowerCase(),
      status: { $in: ['active', 'reseting-password'] }
    })
    .then(function findOneUserForEmailRestrictionCallback(user) {
      if (user) {
        debug.err('user found with username in users: ' + identity.username.toLowerCase());
        db.forgotPasswordRecord.findOneAsync(
          {
            username: identity.username.toLowerCase(),
          })
          .then(function findOneForEmailRestrictionCallback(user) {
            if (!user) {
              debug.err('No user found with username in forgotPasswordRecord: ' + identity.username);
              db.forgotPasswordRecord.insertAsync({
                username: identity.username.toLowerCase(),
                date: new Date(),
                linkSentCount: 1
              }).then(function emailRestrictionInsertAsyncThenCallback(log) {
                debug.warn('User email limit inserted successfully');
                cb(null, log);
              }).error(function emailRestrictionInsertAsyncErrorCallback(err) {
                debug.warn('Failed to insert email limit');
              });
            }
            else {
              var cur_date = new Date();
              var curDate = cur_date.setHours(0, 0, 0, 0);
              var userDate = user.date.setHours(0, 0, 0, 0);
              if (user.linkSentCount < 3 && userDate === curDate) {
                debug('Initiating search for email limit entry with email: ' + user._id.underline);
                db.forgotPasswordRecord.findOneAndUpdateAsync({
                  username: user.username.toLowerCase(),
                },
                  {
                    $set: {
                      date: new Date(),
                      linkSentCount: user.linkSentCount + 1,
                    }
                  }).then(function emailRestrictionFindOneAndUpdateThenCallback(value) {
                    cb(null, value);

                  }).catch(function emailRestrictionFindOneAndUpdateErrorCallback(err) {
                    debug.err(err);
                    cb(err);
                  });
              } else if (user.linkSentCount <= 3 && userDate !== curDate) {
                debug.err('Reseting ' + user.username.toLowerCase());
                db.forgotPasswordRecord.findOneAndUpdateAsync({
                  username: user.username.toLowerCase(),
                },
                  {
                    $set: {
                      date: new Date(),
                      linkSentCount: 1,
                    }
                  }).then(function emailRestrictionAnotherFindOneAndUpdateThenCallback(value) {
                    cb(null, value);
                  }).catch(function emailRestrictionAnotherFindOneAndUpdateErrorCallback(err) {
                    debug.err(err);
                    cb(err);
                  });
              }
              else {
                debug.err('Maximum email sending limit reached ' + user.username.toLowerCase());
                cb('Maximum email sending limit reached.');
              }
            }
          })
          .catch(function findOneForverifyVerificationCodeSchemaCallback(err) {
            cb('Something bad happened');
          });
      }
      else {
        debug.err('No such user found with username: ' + identity.username.toLowerCase());
        cb(null, user);
      }
    })
    .catch(function findOneUserForEmailRestrictionSchemaCallback(err) {
      cb('Something bad happened' + identity.username.toLowerCase());
    });
}


function updateUser(identity, user, cb) {
  var isUnverfiedUser = false;
  if(user.newEmail && user.newEmail !== user.email){
    isUnverfiedUser = _.cloneDeep(user.isUnverfiedUser);
    if (isUnverfiedUser) {
      user.code =  db.ObjectID();
    }
    user.emailChangeCode = db.ObjectID(user.code);
    identity = {
      "_id": identity
    };
    user.emailChangeCodeExpiresOn = expiryDate();
  }
  delete user.isUnverfiedUser;

  if (user.securityAnswer) {
    user.securityAnswer = crypto.createHash('sha1').update(user.securityAnswer).digest('hex');
    user.securityAnswerModifiedDate = new Date();
  }

  if (user.currentVerificationCode && user.verificationCode) {
    if (!identity._id) {
      identity = {
        '_id': identity,
        //'password': crypto.createHash('sha1').update(user.passwordForverificationCode).digest('hex'),
        'verificationCode': crypto.createHash('sha1').update(user.currentVerificationCode).digest('hex')
      };
    }

    user.verificationCode = crypto.createHash('sha1').update(user.verificationCode).digest('hex');
    user.verificationCodeModifiedDate = new Date();
    delete user.passwordForverificationCode;
    delete user.currentVerificationCode;
  }
  var tempPassword = '';
  if (user.passwordGenerationMethod) {
    if (!identity._id) {
      identity = {
        '_id': identity
      };
    }
    identity.status = { $ne: 'signingup' };


    user.passwordResetCode = db.ObjectID();

    if (user.passwordGenerationMethod === 'System') {
      user.password = getRandomPassword();
    } else {
      user.password = user.resetPasswordTo;
    }

    tempPassword = user.password;
    user.password = crypto.createHash('sha1').update(user.password).digest('hex');
    debug('new password crypto: ' + user.password);
    user.passwordModifiedDate = new Date();
    user.passwordExpiresOn = user.passwordExpiresDate;

    user.status = 'recovering-password';

  } else if (user.newPassword && user.password && !tempPassword) {
    if (!identity._id) {
      identity = {
        '_id': identity
      };
    }
    identity.status = { $ne: 'signingup' };
    identity['$or'] = [{
      password: crypto.createHash('sha1').update(user.password).digest('hex')
    }, {
      temporaryPassword: user.password
    }];

    user.password = crypto.createHash('sha1').update(user.newPassword).digest('hex');
    delete user.newPassword;
    user.passwordModifiedDate = new Date();
    user.passwordExpiresOn = user.passwordExpiresDate;

  } else {
    delete user.password;
    delete user.newPassword;
  }

  debug('Searching by ...');
  debug(identity);
  debug(user);

  delete user.passwordExpiresDate;

  if (typeof identity != 'object') {
      identity = {
        _id: identity
      };
  }

  // QC3-10968 - surjeet.. searching using identity object
  db.users.findOneAsync({
    identity,
  }).then(function (existUser) {

    if (existUser && existUser.isActive === false && user && user.isActive === true) {
      user.failedLoginAttempts = 0;
    }
    usersResource.update(identity, user, function (err, data) {
      if (err) {
        debug(err);
        cb("Unable to update user. Please make sure that "
          + "information you provided is leget");
        return;
      } else {
        debug.warn('User in update callback');
        delete data.securityAnswer;
        delete data.password;
        delete data.verificationCode;

        cb(null, data);
      }

      if (!tempPassword && identity.password && !user.verificationCode) {
        _.merge(data, settings);
        log.log('inside file: registration.js - function: updateUser - status: sending mail 1','debug');

        mailer.send('change-password', data, data.email, function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email - change-password');
            debug.err(user);
            debug.err(e);
          } else {
            debug(data.email.underline + ' password change confirmation email sent sucessfully');
          }
        });
      }

      if (user.securityAnswer) {

        _.merge(data, settings);
        log.log('inside file: registration.js - function: updateUser - status: sending mail 2','debug');

        mailer.send('change-security-settings', data, data.email, function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email - change-security-settings');
            debug.err(user);
            debug.err(e);
          } else {
            debug(data.email.underline + ' security settings change confirmation email sent sucessfully');
          }
        });
      }



      if (user.verificationCode) {
        _.merge(data, settings);
        log.log('inside file: registration.js - function: updateUser - status: sending mail 3','debug');

        mailer.send('change-verification-code', data, data.email, function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email - change-verification-code');
            debug.err(user);
            debug.err(e);
          } else {
            debug(data.email.underline + ' verification code change confirmation email sent sucessfully');
          }
        });
      }



      if (!tempPassword && data.status === 'recovering-password') {

        usersResource.update(data._id, { status: 'active' }, function nullCB(e, d) { });

        _.merge(data, settings);
        log.log('inside file: registration.js - function: updateUser - status: sending mail 4','debug');

        mailer.send('finish-password-reset', data, data.email, function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email - finish-password-reset');
            debug.err(user);
            debug.err(e);
          } else {
            debug(data.email.underline + ' password reset success mail sent sucessfully');
          }
        });
      }

      if (tempPassword) {
        debug('Reset password request for ' + data.username.toLowerCase()
          + ' by ' + user.passwordGenerationMethod + ' to ' + user.newEmail);

        data.tempPassword = tempPassword;
        //Send verification Email to newEmail
        _.merge(data, settings);
        log.log('inside file: registration.js - function: updateUser - status: sending mail 5','debug');

        mailer.send('begin-password-reset', data, data.email, function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email - begin-password-reset');
            debug.err(user);
            debug.err(e);
          } else {
            debug(data.email.underline + ' password reset mail sent sucessfully');
          }
        });

      }

      if(user.newEmail !== user.email && isUnverfiedUser){
        debug('Welcome link send to new email id' + user.newEmail);
        data.tempPassword = data.temporaryPassword;
        _.merge(data, settings);
        log.log('inside file: registration.js - function: updateUser - status: sending mail 6');

        mailer.send('begin-signup', data,user.newEmail,function sendMailCallback(e,b) {
          if(e){
            debug.err('Failed sending email - begin-signup');
            debug.err(user);
            debug.err(e);
          }else{
            debug('Email has been sent to ' + user.newEmail.underline + ' sucessfully.');
          }
        });
        log.log('inside file: registration.js - function: updateUser - status: sending mail 7');
      }

      if (user.newEmail !== user.email && !isUnverfiedUser) {
        debug('Email change request for ' + data.username.toLowerCase()
          + ' from ' + user.email + ' to ' + user.newEmail);

        _.merge(data, settings);
        //Send verification Email to newEmail
        log.log('inside file: registration.js - function: updateUser - status: sending mail 6','debug');

        mailer.send('begin-change-email', data, user.newEmail, function sendMailCallback(e, b) {
          if (e) {
            debug.err('Failed sending email - begin-change-email');
            debug.err(user);
            debug.err(e);
          } else {
            debug(user.newEmail.underline + ' verification mail sent sucessfully');
          }
        });

        log.log('inside file: registration.js - function: updateUser - status: sending mail 7','debug');

        //Send notification Email to currentEmail
        mailer
          .send('begin-change-email-notify',
          data,
          user.email,
          function sendMailCallback(e, b) {
            if (e) {
              debug.err('Failed sending email - begin-signup');
              debug.err(user);
              debug.err(e);
            } else {
              debug(user.email.underline
                + ' notified sucessfully of the email change'
                + ' to ' + user.newEmail.underline
              );
            }
          });
      }
    });

  });


}

function mapUserIdWithUser(data, cb) {

  var returnOne = !(data instanceof Array);
  if (returnOne) {
    data = [data];
  }

  var ids = _.uniq((data)
  .reduce(function (ret, record, index) {
    if (record.createdBy) {
      ret.push(db.ObjectID(record.createdBy));
    }
    if (record.modifiedBy) {
      ret.push(db.ObjectID(record.modifiedBy));
    }
    if (record.scheduledBy) {
      ret.push(db.ObjectID(record.scheduledBy));
    }

    if (record.path === 'modifiedBy' || record.path === 'createdBy' || record.path === 'scheduledBy') {
      debug(record);
      if (record.old) {
        ret.push(db.ObjectID(record.old));
      }
      if (record.new) {
        ret.push(db.ObjectID(record.new));
      }
    }
    return ret;
  },[]));


  db
    .users
    .find({
      '_id': {
        $in: ids
      }
    })
    .project({ 'firstname': 1, 'lastname': 1, 'username': 1 })
    .toArray(function (e, d) {
      d = _.reduce(d, function (acc, user) {
        acc[user._id] = user;
        return acc;
      }, []);

      _.forEach(data, function (record) {

        if (record.createdBy) {
          record.createdBy = d[record.createdBy];
        }
        if (record.modifiedBy) {
          record.modifiedBy = d[record.modifiedBy];
        }
        if (record.scheduledBy) {
          record.scheduledBy = d[record.scheduledBy];
        }


        if (record.path === 'modifiedBy' || record.path === 'createdBy' || record.path === 'scheduledBy') {
          debug(record);
          if (record.old) {
            record.old = d[record.old];
            if (record.old) {
              record.old = record.old.firstname + ' ' + record.old.lastname
            } else {
              record.old = 'Anonymous'
            }
          }
          if (record.new) {
            record.new = d[record.new];
            if (record.new) {
              record.new = record.new.firstname + ' ' + record.new.lastname
            } else {
              record.new = 'Anonymous'
            }
          }
        }
      });

      cb(null, returnOne ? data[0] : data);
    });
}

function updateLicenseAgreementInfo(req, cb){
    db['users'].findAndModify({
      _id: req._id,
  }, [], {
      $set: {
        licenseAgreementStatus: req.licenseAgreementStatus,
        licenseAcceptanceDateTime : req.licenseAcceptanceDateTime,
        licenseAgreementVersion : req.licenseAgreementVersion
      }
  }, {
      upsert: true
  }, function (err, data) {
      if (err) {
          cb(err, req);

      }else{
        cb(null, data);
      }
  });
}



module.exports = {
  create: beginSignUp,
  beginSignUp: beginSignUp,
  finishSignUp: finishSignUp,
  finishEmailChange: finishEmailChange,
  beginForgetPassword: beginForgetPassword,
  finishForgetPassword: finishForgetPassword,
  beginForgetESignature: beginForgetESignature,
  getUserByUserNameAndForgotESignatureCode: getUserByUserNameAndForgotESignatureCode,
  finishForgetESignature: finishForgetESignature,
  login: login,
  update: updateUser,
  getOne: getOneUser,
  getAll: getAllUsers,
  getChangeLogs: usersResource.getChangeLogs,
  mapUserIdWithUser: mapUserIdWithUser,
  getUserByUserNameAndForgotPasswordCode: getUserByUserNameAndForgotPasswordCode,
  unverifiedUserMailSend: unverifiedUserMailSend,
  resendCode: resendCode,
  abortCode: abortCode,
  verifyVerificationCode: verifyVerificationCode,
  emailRestriction: emailRestriction,
  conCurrentUserUpdate: conCurrentUserUpdate,
  conCurrentAdminUserUpdate: conCurrentAdminUserUpdate,
  beginResetPassword: beginResetPassword,
  beginResetAccess: beginResetAccess,
  finishResetAccess: finishResetAccess,
  updatePasswordLog: updatePasswordLog,
  validateNewPassword: validateNewPassword,
  getExpireDateFromGeneralSettings: getExpireDateFromGeneralSettings,
  finishChangePassword: finishChangePassword,
  conCurrentUserUpdateByUserName: conCurrentUserUpdateByUserName,
  updateLicenseAgreementInfo: updateLicenseAgreementInfo
};
