'use strict'
/**
* @name config
* @author Surjeet Bhadauriya <surjeet.b@productivet.com>
*
* @version 0.0.0
*/
var envconfig = require('../config/config.js');
var settings = {
  _id: 'default',
  title: 'Perfeqta Settings',
  company: 'The PERFEQTA',
  product: 'PERFEQTA',
  support: 'support@beperfeqta.com',
  //web: 'http://localhost:9000'
  web: envconfig.envurl
};
var config = function() {
    return settings;
};

module.exports = {
  config: config
};
