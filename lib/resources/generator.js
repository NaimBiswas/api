'use strict'
var bird = require('bluebird');
var _ = require('lodash');
var db = require('../db');
var libLogger = require('../logger');
var debug = libLogger.debug('lib:resources:generator');
var logger = require('../../logger');
var currentFileName = __filename;

function generate(resource) {
  debug('generating resource: ' + resource.name);
  logger.log('Function: generate - Start', 'info', currentFileName);

  function getAll(options,cb) {
    logger.log('Function: getAll - Start', 'info', currentFileName);
    logger.log('Collection: '+resource.collection+' - Before Apply Call', 'info', currentFileName);
    options.apply(db[resource.collection], function(err, docs) {
      if(err){
        cb(err);
        return;
      }

      docs.toArray(function(e,users) {
        if(e){
          logger.log('Error: - Inside Apply Call toArray : '+ e, 'error', currentFileName);
          cb(e);
          return;
        }
        if(resource.retract && resource.retract.length){
          logger.log('if(resource.retract && resource.retract.length): Started', 'info', currentFileName);
          users = _.map(users,function mapUsersCb(u) {
            _.forEach(resource.retract,function retractFieldCb(field) {
              delete u[field];
            });
            return u;
          });
        }

        cb(null,users);
      });
    });
  }

  function getAllVersions(identity, options,cb) {
    logger.log('Function: getAllVersions - Started', 'info', currentFileName);
    options.q = {
      identity: identity
    };
    options.apply(db[resource.collection].audit,
      function optionsApplyCallback(err, docs) {
        if(err){
          logger.log('Function: optionsApplyCallback - error : ' + err, 'error', currentFileName);
          cb(err);
          return;
        }
        docs.toArray(function versionsToArrayCallback(err,data) {
          cb(err, _.map(data, function versionMapCallback(version) {
            version._id = version.identity;
            delete version.identity;
            if(resource.retract && resource.retract.length){
              logger.log('if(resource.retract && resource.retract.length) - Success', 'info', currentFileName);
              _.forEach(resource.retract,function retractFieldCb(field) {
                delete version[field];
              });
            }
            return version;
          } ));
        });
      });
    }

    function getOne(identity,cb) {
      logger.log('Function : getOne - Started', 'info', currentFileName);
      debug('searching ' + resource.collection + ' for _id = ' + identity + '('+ typeof identity + ')');

      logger.log('Collection : '+resource.collection+' - Before Started', 'info', currentFileName);
      db[resource.collection].findOne({_id: identity},function getOneCb(e,u) {
        if(e){
          cb(e);
          return;
        }
        if(u && resource.retract && resource.retract.length){
          logger.log('if(u && resource.retract && resource.retract.length) - Started', 'info', currentFileName);
          _.forEach(resource.retract,function retractFieldCb(field) {
            delete u[field];
          });
        }

        cb(null,u);
      });
    };

    function getVersion(identity,version,cb) {
      logger.log('Collection : '+resource.collection+' - Before Started', 'info', currentFileName);
      db[resource.collection].audit.findOne({
        identity: identity,
        version: parseFloat(version)
      },
      function findOneVersionCallback(err,data) {
        if(err){
          logger.log('Function: findOneVersionCallback - Failed : ' + err, 'error', currentFileName);
          cb(err);
          return;
        } else if(!data){
          logger.log('Function: findOneVersionCallback - No Content', 'info', currentFileName);
          cb('Version not found');
          return;
        }
        data._id = data.identity;
        delete data.identity;
        if(resource.retract && resource.retract.length){
          logger.log('if(resource.retract && resource.retract.length) - Started', 'info', currentFileName);
          _.forEach(resource.retract,function retractFieldCb(field) {
            delete data[field];
          });
        }
        cb(null,data);
      });
    }

    function getMajorVersion(identity,cb) {
      logger.log('Function: getMajorVersion - Started', 'info', currentFileName);
      db[resource.collection].audit.findOne({
        identity: identity,
        isMajorVersion: true
      },
      function findOneVersionCallback(err,data) {
        logger.log('Function: findOneVersionCallback - Started', 'info', currentFileName);
        data._id = data.identity;
        delete data.identity;
        if(resource.retract && resource.retract.length){
          logger.log('if(resource.retract && resource.retract.length) - Started', 'info', currentFileName);
          _.forEach(resource.retract,function retractFieldCb(field) {
            delete data[field];
          });
        }
        cb(err,data);
      });
    }



    function getChangeLogs(identity, options, cb) {
      logger.log('Function: getChangeLogs - Started', 'info', currentFileName);
      //uncomment following lines to enable "title" mapping mode for logs.
      // var titleExist = true;
      // if (!_.isUndefined(options.q) && options.q.title && options.q.title === 'false') {
      //     titleExist =  false;
      // }
      options.q = {
        identity: identity,
        //uncomment following lines to enable "title" mapping mode for logs.
        // title: { $exists: titleExist }
      };
      options.apply(db[resource.collection].log,
        function optionsApplyCallback(err, docs) {
          if(err){
            logger.log('Function: optionsApplyCallback - Error : ' + err, 'error', currentFileName);
            cb(err);
            return;
          }
          docs.toArray(cb);
        });
      }




      function create(soft, entity, cb) {
        //To enable strict auto generated _id uncomment following line
        //delete entity._id;
        delete entity.version;
        delete entity.versions;

        var ip = entity.__IP__;
        delete entity.__IP__;

        if(entity._id && entity._id.length === 24){
          entity._id = db.ObjectID(entity._id);
        }
        // (resource.collection.length == 24 && db.ObjectID.isValid(resource.collection))
        // as discuss with @JD and @Om we are remove child process for record entry/ercord entity
        // QC3-8959 - Backlog: Implement mechanism for storing app record version which can be utilized for Activity information report -- kajal
        if (resource.collection === 'schema' || resource.collection === 'preSchema' || resource.collection === 'procedures' || resource.collection === 'entities' ) {
            entity.isAuditing = true;
        }
        if (entity.createdDate && resource.collection == 'qcentryactivity') {
          entity.createdDate = new Date(entity.createdDate);
        } else {
          entity.createdDate = new Date();
        }

        entity.modifiedDate = new Date();

        db[resource.collection].insertOne(entity,function createCallback(err,data) {
          if(err){
            cb(err);
            return;
          }

          if(data.ops.length){
            if(db[resource.collection].auditThis){
              if(soft){
                db[resource.collection].auditThisSoftly(data.ops[0], ip);
              }else {
                db[resource.collection].auditThis(data.ops[0], ip);
              }
            }

            var retVal = data.ops[0];

            if(resource.retract && resource.retract.length){
              _.forEach(resource.retract,function retractFieldCb(field) {
                delete retVal[field];
              });
            }
            cb(err,retVal);
          }
        });
      }

      function update(soft, identity, entity,cb) {
        delete entity._id;
        var ip = entity.__IP__;
        delete entity.__IP__;

        if (resource.collection == 'qcentryactivity' && entity.modifiedDate) {
          entity.modifiedDate = new Date(entity.modifiedDate);
        } else {
        entity.modifiedDate = new Date();
        }
        // (resource.collection.length == 24 && db.ObjectID.isValid(resource.collection))
        // as discuss with @JD and @Om we are remove child process for record entry/ercord entity
        // QC3-8959 - Backlog: Implement mechanism for storing app record version which can be utilized for Activity information report -- kajal
        // PQT-965 by Yamuna
        if (resource.collection === 'schema' || resource.collection === 'preSchema'|| resource.collection === 'procedures' || resource.collection === 'entities' ) {
            entity.isAuditing = true;
        }

        if(!identity._id){
          identity = {
            "_id": identity
          }
        }

        debug('Using following filter to search: ');
        debug(identity);

        var updateObj = {
          $set: entity
        };

        if(entity.customHeaders.allowPut){ //PQT-956 by Yamuna
          updateObj = entity;
        }else{
          delete entity.version;
          delete entity.versions;
        }

        delete entity.customHeaders;
        db[resource.collection].findOneAndUpdate(
          identity,updateObj,{
            returnOriginal: false
          },
          function updateOneCallback(err, data) {
            if(err){
              cb(err);
              return;
            }
            if(data.value){
              if(db[resource.collection].auditThis){
                if(soft){
                  db[resource.collection].auditThisSoftly(data.value, ip);
                }else {
                  db[resource.collection].auditThis(data.value, ip);
                }
              }
              var retVal = _.clone(data.value);
              if(resource.retract && resource.retract.length){
                _.forEach(resource.retract,function retractFieldCb(field) {
                  delete retVal[field];
                });
              }
              cb(err,retVal)
            }else {
              cb(data);
            }
          });
        }

        return bird.promisifyAll({
          getAll: getAll,
          getOne: getOne,
          getAllVersions: getAllVersions,
          getVersion: getVersion,
          getMajorVersion: getMajorVersion,
          create: create.bind(this, false),
          softCreate: create.bind(this, true),
          update: update.bind(this,false),
          softUpdate: update.bind(this,true),
          getChangeLogs: getChangeLogs
        });
      }

      module.exports= {
        generate: generate,
        db: db
      };
