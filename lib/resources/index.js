var generator = require('./generator');

module.exports = {
  fetch: generator.generate,
  db: generator.db
};
