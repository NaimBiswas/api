
var config = require('../config/config.js');
var Promise = require('bluebird');
var db = Promise.promisifyAll(require('./db'));
var mailer = require('../lib/mailer/mailer');

function getRandomNumber() {
    var min = Math.ceil(1000);
    var max = Math.floor(9000);
    var _time = new Date();
    var randomNumber = (Math.floor(Math.random() * (max - min + 1)) + min)
    return  "" + randomNumber + _time.getTime()
}

function _module(moduleName,tag,tagObject, instanceNumber){
    var collectionName = moduleName + "InfoLogs";
    tag = tag || "";
    instanceNumber = instanceNumber || getRandomNumber()

    return {
        log: log,
        sendAlert: sendAlert,
        getInstance: getInstance,
        setTag: setTag
    }

    function getInstance(tagInstance, tagInstanceObject){
        if(tagInstance || tagInstanceObject){
            Object.assign(tagInstanceObject || {} , tagObject || {});
        }

        return _module(moduleName, tag + " > " +  tagInstance, tagInstanceObject, instanceNumber);
    }

    function setTag(_tag){
        tag= _tag;
    }

    
    function sendAlert(message, extraObject){
        mailer.simple_independent(config.alertMail, 
                                    config.envurl + " threshold alert",
                                    message + " " + JSON.stringify(message))
    }

    function log(message,extraObject){

        extraObject = extraObject || {}
        tagObject = tagObject || {}
        
        Object.assign(extraObject,tagObject);
        

        var myLogInfo = Object.assign({
            message: tag+" - "+message,
            // extraObject: extraObject,
            instanceNumber: instanceNumber,
            date: new Date()
        }, extraObject)

        if(config.log.level == 'info' || config.log.level == 'debug'){
            console.log(myLogInfo)
        }

        return db.collection(collectionName)
                .insertAsync(myLogInfo);
    }

}

module.exports = {
    _module: _module
}