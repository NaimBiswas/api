'use strict'
/**
* @name db
* @author Dave Amit <dave.a@productivet.com>
*
* @version 0.0.0
*/

var debug = require('../logger').debug('lib:db');

var bird = require('bluebird');
var auditUtils = require('./audit.utils');

var skin = bird.promisifyAll(require('mongoskin'));//require('mongoskin');

//var defaultCnstr = (process.env.DB_PORT ? process.env.DB_PORT.replace('tcp://','mongodb://') : 'mongodb://127.0.0.1:27017') + ('/' + process.env.DB_NAME || '/db');
var config = require('../../config/config.js');
// console.log('certpath: ' + config.db);
var defaultCnstr = (process.env.DB_PORT ? process.env.DB_PORT.replace('tcp://', 'mongodb://') : config.db);
// var defaultCnstr = (process.env.DB_PORT ? process.env.DB_PORT.replace('tcp://','mongodb://') : 'mongodb://localhost:27017') + ('/' + process.env.DB_NAME || '/db');
// var defaultCnstr = (process.env.DB_PORT ? process.env.DB_PORT.replace('tcp://','mongodb://') : 'mongodb://perfeqta:Dev12345@192.168.1.185:27017/dev');
// var defaultCnstr = (process.env.DB_PORT ? process.env.DB_PORT.replace('tcp://','mongodb://') : 'mongodb://love:K1ll$Many@164.58.221.236:27017/po');
//var defaultCnstr = (process.env.DB_PORT ? process.env.DB_PORT.replace('tcp://','mongodb://') : 'mongodb://180.211.96.85:27017/dev');
//var defaultCnstr = (process.env.DB_PORT ? process.env.DB_PORT.replace('tcp://','mongodb://') : 'mongodb://localhost:27017/qc3qa');
//var defaultCnstr = (process.env.DB_PORT ? process.env.DB_PORT.replace('tcp://', 'mongodb://') : 'mongodb://localhost:27017/web');
var dbUri = process.env.MONGOLAB_URI || defaultCnstr;
debug("connected to:" + dbUri);
var db = skin.db(dbUri, { native_parser: true });
var _ = require('lodash');
var diff = require('../diff');
var logger = require('../../logger');
var currentFileName = __filename;
var fs = require('fs');
const threads = require('threads');
var q = require('q');
const spawn = threads.spawn;

// patch for audit trail in quetion screen
var timeTypeMapping = {
  '123456789012345678901257': 'HH:MM',
  '123456789012345678901258': 'HH:MM AM/PM',
  '123456789012345678901259': 'HH:MM:SS',
  '123456789012345678901262': 'HH:MM:SS AM/PM'
};

var entriesUtiliese = require('../utilities/entriesUtiliese')

// Used to create new objectID in from route
db.ObjectID = skin.ObjectID;

/**
* Configuration routine to setup entities and db
*
* @param  {Object},{Object}....,{Object} - Entity with format explained in
*         lib/db/index.js.
* @return {Object} - Configured db instance.
*
* @public
*/
function configure() {
  logger.log('Function: configure - Start', 'info', currentFileName);
  // debug('Dry running db with: ');
  //debug(arguments);

  _.forEach(arguments, function configureEntities(entity) {
    debug('configuring ' + entity.collection + ' ...');
    //logger.log('configuring: '+ entity.collection + ' - Start', 'info', currentFileName);
    db.bind(entity.collection);

    if (entity.index) {
      //logger.log('if(entity.index) - Start', 'info', currentFileName);
      // For ref.
      // https://docs.mongodb.org/manual/core/index-text/#create-text-index
      db[entity.collection].createIndex(entity.index, { default_language: "none" });
      debug('configuring index for ' + entity.collection + ' to ');
      //debug(entity.index);
    }

    // Following has to be done otherwise 'count' behaves wieredly
    logger.log('Collection: Count: ' + entity.collection + ' - Start', 'info', currentFileName);
    db[entity.collection].count(function countCallback(err, count) {
      debug(entity.collection + ' has ' + count + ' records as of now.');
    });

    if (entity.audit) {
      logger.log('if(entity.audit) - Start', 'info', currentFileName);
      entity.auditCollections = entity.auditCollections || entity.collection + 'AuditLogs';
      entity.logsCollection = entity.logsCollection || entity.collection + 'Logs';

      debug(entity.collection + ' will be audited to ' + entity.auditCollections);
      debug(entity.collection + ' will be logged to ' + entity.logsCollection);

      db.bind(entity.auditCollections);
      db.bind(entity.logsCollection);

      var auditIndex = _.clone(entity.index);
      if (!_.isUndefined(auditIndex)) {
        auditIndex.identity = 1;
        auditIndex.version = 1;
      }


      db[entity.auditCollections].createIndex(auditIndex);
      db[entity.auditCollections].count(function countCallback(err, count) {
        //We are attaching audit collection as entity's audit property
        //So just check audit of given entity, if audit is present, do the
        //auditing.
        db[entity.collection].audit = db[entity.auditCollections];
        db[entity.collection].auditThis = auditThis.bind(auditThis, entity, false);
        db[entity.collection].auditThis1 = auditThis1.bind(auditThis1, entity, false);
        db[entity.collection].auditThis1Softly = auditThis1.bind(auditThis1, entity, true);
        db[entity.collection].auditThisSoftly = auditThis.bind(auditThis, entity, true);

        debug(entity.auditCollections + ' has ' + count + ' records as of now.');
      });

      db[entity.logsCollection].createIndex(auditIndex);
      db[entity.logsCollection].count(function countCallback(err, count) {
        //We are attaching audit collection as entity's audit property
        //So just check audit of given entity, if audit is present, do the
        //auditing.
        db[entity.collection].log = db[entity.logsCollection];
        try {
          if (entity.collection.length === 24) {
            logger.log('if(entity.collection.length === 24) - Success', 'info', currentFileName);
            db[entity.collection].logFields = require('./resources/entries');
          } else {
            logger.log('else - Success', 'info', currentFileName);
            if (fs.existsSync(__dirname + '/resources/' + entity.collection + '.js')) {  //remove fs.exist - QC3-11078 Prachi
              db[entity.collection].logFields = require('./resources/' + entity.collection);
            } else {
              db[entity.collection].logFields = {};
            }
          }
          debug('Audit mapping for ' + entity.collection);

          //debug(db[entity.collection].logFields);
        } catch (e) {
          logger.log('Function: configure - Catch ' + e, 'error', currentFileName);
          debug('No audit fields mapping for ' + entity.collection.red);
        }

        debug(entity.logsCollection + ' has ' + count + ' records as of now.');
      });
    }



  });

  return db;
}

function getVersion(resource, identity, version, cb) {
  logger.log('Collection: ' + resource.collection + ' : Audit - Calling', 'info', currentFileName);
  db[resource.collection].audit.findOne({
    identity: identity,
    version: parseFloat(version)
  },
    function findOneVersionCallback(err, data) {
      if (err) {
        logger.log('Function : findOneVersionCallback: Collection: ' + resource.collection + ' : Audit - Error : ' + err, 'error', currentFileName);
        cb(err);
        return;
      } else if (!data) {
        logger.log('Function : findOneVersionCallback: Collection: ' + resource.collection + ' : Audit - Not Found', 'error', currentFileName);
        cb('Version not found');
        return;
      }
      logger.log('Function : findOneVersionCallback: Collection: ' + resource.collection + ' : Audit - Success', 'info', currentFileName);
      data._id = data.identity;
      delete data.identity;

      cb(null, data);
    });
}

function logIsActive(resource, o, n, version, ip) {

  logger.log('Function : logIsActive - Success', 'info', currentFileName);
  delete o.versions;
  delete n.versions;
  
  var set = _.forEach(diff(o, n), function (value) {
    value.identity = n._id;
    value.modifiedDate = n.modifiedDate;
    value.modifiedBy = n.modifiedBy;
    value.version = version;
    value.ip = ip;
    return value;
  });

  if (set.length == 1 && set[0].path == 'isActive') {
    return false;
  } else {
    return true;
  }

}

function filterSet(set, resource){
  if(resource.collection == 'preSchema'){
    set = _.filter(set, function (a) {
      var endswithid = true;
      if (a.path.endsWith("id")) {
          if (_.includes(a.path, "selectedFileFormat") || (_.includes(a.path, "conditionalWorkflows") && _.includes(a.path, "value"))) {
              endswithid = false;
          }
      }
      //Start: QC3-4882 - Audit Trail Issues.
      //Changed By: Jyotil
      //Description: code to remove entry into audit trail logs.
      //QC3-3849 -bug add " || a.path.endsWith('.size') " to restict to go file size in audit trail
      return !((a.path.endsWith('createdBy') || a.path.endsWith('modifiedBy') || a.path === "reviewStatus" || a.path === "loader" || a.path === "displayAssignee"
       
          // PQT-962 by Yamuna
          || ((!a.path.endsWith("getInactiveRecord") && !a.path.endsWith("enableEntityRecord") && !a.path.endsWith("entityRecord") && a.path != "isLockRecord") && a.path.endsWith("Record")) || a.path.endsWith("_id") || a.path.endsWith("s#")
          || a.path === "isAuditing" || a.path.endsWith("comboId") || a.path.endsWith("parentId") || a.path.endsWith("siteId") || a.path.startsWith("auditTrailInfo")
          || a.path.endsWith("levelId") || a.path === "id" || a.path === "password" || a.path === "temporaryPassword"
          || a.path === "loggedInDateTime" || a.path == "usersUUID" || a.path.startsWith("passwordLog") || a.path === "fromToken"
          || a.path.endsWith('isApp') || a.path.endsWith('entryId') || (a.path.endsWith('schemaId') && a.path !== 'schemaID') || a.path.endsWith('entityKeyAttributeComboId')
          || a.path === "restangularCollection" || a.path === "restangularEtag" || a.path === "restangularized" || a.path === "fromServer" || a.path === "route"  //QC3-8743: mahammad
          || a.path.endsWith('.route') || a.path.endsWith('.restangularized') || a.path.endsWith('.fromServer') || a.path.endsWith('.restangularCollection')
          || a.path.startsWith('entries.tempAppDataStore') || a.path.startsWith('entries.errorAppDataStore') || a.path.endsWith('ismapped') || a.path.startsWith('entries.appToExpandMessage') || a.path.endsWith('datetime') || a.path.endsWith('.size')
          || a.path.endsWith('hasSelected') || a.path.endsWith('tried') || a.path === 'appId'
          //Start: QC3-6043 - app/master app: spelling mistakes are found in in audit trail - Jyotil    || (resource.collection==='schema' && a.path === 'allowCreaterObj.id') // QC3-9996 - Jyotil
          // MDA bugs - start
          || (resource.collection === 'preSchema' && (a.path.endsWith('id') || a.path === 'isDeleted' || (a.path.startsWith('attributes') && a.path.indexOf('validation.validationSet.existingSet.versions') >= 0))) // QC3-9996, QC3-10080 - Jyotil
          // MDA bugs - end
          || a.path.startsWith("workflowReviewHistory") || a.path.endsWith("reviewedUserId") || a.path.endsWith("userSelectionType") || (resource.collection === 'entries' && a.path.endsWith('masterQCSettingsId'))
          || a.path === "timeZone" //Yamuna timeZone Remove  at 233
          || a.path == "route" || a.path == "restangularized" || a.path == "fromServer" || a.path == "restangularCollection"
          || a.path.endsWith('id')
          || a.path === 'dmsfile'
          || a.path.startsWith('holdRemovedAttributeSeqIds') //QC3-QC3-6935
          || a.path.endsWith('entityId') || a.path.endsWith('entityKeyAttributeId') || a.path.endsWith('entityKeyAttributeDateFormat')
          || a.path === "windowEndDate" || a.path === "pendingFailure" || a.path === "processFailure"
          || a.path === "unitTestedUnits" || a.path === "remainingUnits" || a.path === "remainingFailures" || a.path === "attributes"
          || a.path.startsWith("firstStageInformation") || a.path.startsWith("countDetails") || a.path.startsWith("default")
          || a.path.startsWith("secondStageStartWithCount") || a.path.startsWith("secondStageInformation")
          || a.path === "displayModules" || a.path === "isConCurrentUserActive" || a.path.endsWith("seqId") || a.path.endsWith("formId") || a.path === "isConCurrentUserAdmin") && endswithid);
  });
  }else{
    set = _.filter(set, function (a) {
      var endswithid = true;
      if (a.path.endsWith("id")) {
        //console.log('selectedFileFormat:: '+ JSON.stringify(a));
        if (_.includes(a.path, "selectedFileFormat") || (_.includes(a.path, "conditionalWorkflows") && _.includes(a.path, "value"))) {
          endswithid = false;
        }
      }
  
      if (resource.collection == 'questions' && a.path.endsWith('datetime')) {
        a.old = timeTypeMapping[a.old] ? timeTypeMapping[a.old] : a.old;
        a.new = timeTypeMapping[a.new] ? timeTypeMapping[a.new] : a.new;
      }
      //QC3-3849 -bug add " || a.path.endsWith('.size') " to restict to go file size in audit trail
      return !((a.path === "createdBy" || a.path === "modifiedBy" || a.path === "reviewStatus" || a.path === "loader" || a.path === "displayAssignee"
        || a.path === 'verificationCodeModifiedDate' || a.path === 'securityAnswerModifiedDate' || a.path === 'passwordModifiedDate'
        || a.path === 'Code' || a.path === 'assignmentEmailNotification'
        || a.path.endsWith("Record") || a.path.endsWith("_id") || a.path.endsWith("s#") || a.path.endsWith("fileStamp")
        || a.path === "isAuditing" || a.path.endsWith("comboId") || a.path.endsWith("parentId") || a.path.endsWith("siteId") || a.path.startsWith("auditTrailInfo")
        || a.path.endsWith("levelId") || a.path === "password" || a.path === "temporaryPassword"
        || a.path === "loggedInDateTime" || a.path == "usersUUID" || a.path.startsWith("passwordLog") || a.path.endsWith('.size')
        || a.path.endsWith('isApp') || a.path.endsWith('entryId') || (a.path.endsWith('schemaId') && a.path !== 'schemaID')
        || a.path.startsWith('entries.tempAppDataStore') || a.path.startsWith('entries.errorAppDataStore') || a.path.endsWith('ismapped') || a.path.startsWith('entries.appToExpandMessage') || a.path.endsWith('.size')
        || (resource.collection === 'thresholds' && a.path.startsWith('schema.') && a.path.endsWith('.title'))
        || a.path.startsWith("workflowReviewHistory") || a.path.endsWith('reviewedUserId') || a.path.endsWith('userSelectionType') || (resource.collection != 'questions' && a.path.endsWith('datetime')) || a.path.endsWith('hasSelected') || a.path.endsWith('tried')
        || a.path === "id" || a.path === "timeZone" //Yamuna timeZone Remove at 229
        || a.path.endsWith('id')
        || (resource.collection != 'questions' && a.path.endsWith('datetime')) || a.path.endsWith('hasSelected') || a.path.endsWith('tried')
        || a.path === "restangularCollection" || a.path === "restangularEtag" || a.path === "restangularized" || a.path === "fromServer" || a.path === "route"  //QC3-8743: mahammad
        //Start: QC3-4882 - Audit Trail Issues.- Jyotil
        || a.path === "id" || a.path === 'appId'
        || (resource.collection === 'users' && (a.path === 'securityAnswer' || a.path === 'code' || a.path === 'status' || a.path === 'forgotPasswordCode' || a.path.startsWith('forgotPasswordRecord') || a.path === 'tempPassword' || a.path === 'emailChangeCode' || a.path.startsWith('emailChangeCode') || a.path === 'passwordExpiresOn'))
        // Mda bugs - Start
 
        || ((resource.collection === 'schema' || resource.collection === 'preSchema') && (a.path.endsWith('id') || a.path === 'isDeleted' || (a.path.startsWith('attributes') && a.path.indexOf('validation.validationSet.existingSet.versions') >= 0))) // QC3-9996, QC3-10080 - Jyotil
        || (resource.collection === 'groupsettings' && (a.path.startsWith("forms") || a.path.startsWith("modules")) && a.path.endsWith('id')) // Jyotil - QC3-10147
        || a.path === 'masterQCSettings.masterQCSettingsId' // QC3-10190 - Jyotil
        || a.path === 'masterQCSettings.sequenceId' // QC3-10190 - Jyotil
        // Mda bugs - End
        || (resource.collection === 'schedules' && a.path === 'masterQCSettingsId')
        // Start:QC3-5375-set frequency: Certain issues in Audit trail - Vaibhav Vora
        || (resource.collection === 'schedules' && a.path === 'frequency.subType')
        || (resource.collection === 'appAccessConfig' && (a.path === 'token' || /^(emails).*.(used)$/.test(a.path) || /^(emails).*.(allowed)$/.test(a.path))) || a.path === "noOfTimeUsed"
        || (resource.collection === 'entries' && a.path === 'fromToken')
        // End:QC3-5375- Vaibhav Vora //End: QC3-4882 - Audit Trail Issues.
        || a.path.endsWith('.route') || a.path.endsWith('.restangularized') || a.path.endsWith('.fromServer') || a.path.endsWith('.restangularCollection')
        || a.path.startsWith('holdRemovedAttributeSeqIds') //QC3-QC3-6935
        || a.path.endsWith('entityId') || a.path.endsWith('entityKeyAttributeId') || a.path.endsWith('entityKeyAttributeDateFormat') || a.path.endsWith('entityKeyAttributeComboId')
        || (resource.collection === 'pluginConfig' && a.path.endsWith("password"))
        || a.path === "windowEndDate" || a.path === "pendingFailure" || a.path === "processFailure" || a.path === "isGeneratedRepeatWindow"
        || a.path === "seondStagePendingFailure" || a.path === "seondStageProcessFailure" || a.path === "seondStageRemainingFailure" || a.path.startsWith("secondStageCountDetails")
        || a.path === "unitTestedUnits" || a.path === "remainingUnits" || a.path === "remainingFailures" || a.path === "attributes" || a.path.endsWith('voidQC') || a.path.endsWith('seaconStageVoidQC')
        || a.path.startsWith("firstStageInformation") || a.path.startsWith("countDetails") || (resource.collection == "windows" && (a.path.startsWith("default") || (a.path.startsWith('filteredAttributes') && a.path.endsWith('type')))) || a.path === "displaySecondStage"
        || a.path.startsWith("secondStageStartWithCount") || a.path.startsWith("secondStageInformation") || a.path === "seondStageUnitTestedUnits"
        || a.path === "displayModules" || a.path === "isConCurrentUserActive" || a.path.endsWith("seqId") || a.path.endsWith("formId") || a.path === "isConCurrentUserAdmin"
        || a.path === "voidQCEntries" || a.path === "voidQC" || a.path === "hasSecondStage" || a.path === "isSecondStageActivated" || a.path === "sampleDetailsForSecondStage" || a.path === "windowTypeEmail"
        // Start:Vishesh Patel - Audit Trail Issues.
        || a.path === 'salesforceId' || a.path === 'instance' || a.path === 'isSyncing' || a.path.includes('-link') || a.path.includes('-type')
        || a.path.endsWith('isDeleted') || a.path.startsWith('child') || a.path.includes('childforms') || a.path.startsWith('calendarverifieduser') || a.path.startsWith('calendarIds')
        || a.path == 'newEmail' || a.path == 'isdst' || a.path.startsWith('createdByInfo')
        || a.path === 'isDeleted') && endswithid);
      // End:Vishesh Patel - Audit Trail Issues.
    });
  }
  return set;
}

function log(resource, o, n, version, ip) {

  logger.log('Function : log - Success', 'info', currentFileName);
  var set = _.forEach(diff(_.cloneDeep(o), n), function (value) {
    var isP = _.find(n.auditTrailInfo, ['path', value.path]);
    if (isP) {
      value.ntype = isP.type;
      value.nformat = isP.format;
    }
    var isoP = _.find(o.auditTrailInfo, ['path', value.path]);
    if (isoP) {
      value.otype = isoP.type;
      value.oformat = isoP.format;
    }
    value.identity = n._id;
    value.modifiedDate = n.modifiedDate;
    value.modifiedBy = n.modifiedBy;
    value.version = version;
    value.ip = ip;
    return value;
  });


  // Code by jaydip
  // Code for remove extra fields from audit trail logs

  // Note :: a.path === "createdBy" || a.path === "modifiedBy" Need to remove this both for valid audit trail
  set = filterSet(set, resource);
  if (set && set.length) { // && (set[0].path == "isActive" && set.length > 1)
    var logFields = db[resource.collection].logFields;
    if (logFields) {
      _.forEach(set, function set(item) {
        item.title = _.map(item.path.split('.'), function (item, index) {
          // item.toString().length < 10 add condition to restrict append # tag on title when item name is timestamp
          // generally item is not reach upto that number so it never create problem when we check like this for timestamp
          if (item.toString().length < 10 && (parseInt(item) + '') === item) {
            return '#' + (parseInt(item) + 1);
          } else {
            return logFields[item] || item;
          }
        }).join(' > ');
      });

      // Code by jaydip
      // Code for remove Entries.status entry from qc entry audit logs
      set = _.filter(set, function (a) {
        return !((a.title.startsWith("Set Value >") && a.title.endsWith("> Name")) || (a.title.endsWith("> Route")) || (a.title.endsWith("> Restangularized")) || (a.title.endsWith("> From Server")) || (a.title.endsWith("> Restangular Collection")) ||   (a.title.endsWith('> fileStamp')) ||
          a.path == 'code' || a.title.startsWith('Code >') ||

          a.path == 'emailChangeCode' || a.title.startsWith('emailChangeCode >') ||
          a.path == 'forgotPasswordCode' || a.title.startsWith('forgotPasswordCode >') ||
          a.path == 'verificationCode' || a.title.startsWith('verificationCode >') ||
          (a.title.startsWith("Entries > status") && a.title !== "Entries > status > status") || a.title.startsWith("Entries > FailMesssageForQCForm") || a.title.startsWith("levelIds >") || a.title.endsWith("draftStatus") || a.path === "verificationCode");
      });
      // End code by jaydip
    }

    if (resource.collection == 'preSchema') {
      if (set.length == 1 && (set[0].path == "isActive" || set[0].path == "restoreStableVersion")) {
        set[0].version = n.version;
      } else if ((set.length == 2 &&
        ((_.find(set, { path: "isActive" }) && (_.find(set, { path: "isDraft" }) || _.find(set, { path: "modifiedDate" }))) ||
        (_.find(set, { path: "isDraft" }) && _.find(set, { path: "restoreStableVersion" })))) ||
        (set.length == 3 &&
          (_.find(set, { path: "isActive" }) && _.find(set, { path: "isDraft" }) && _.find(set, { path: "restoreStableVersion" })))) {
        set.forEach(function (setObj) {
          setObj.version = n.version;
        })
      }
    }

    db[resource.logsCollection].insertMany(set, function (e, data) {
      if (e) {
        debug('Failed to capture log for ' + n._id + ' change log is as follows');
        debug(set);
      }
    });


    entriesUtiliese.logEntrySave_AfterSign(n, resource.collection, set, db)


  }
  return set;
}




//const thread = spawn(function (input, done) {
//    var tdb = require(input.__dirname + '/threading.js');
//    console.log("1");
//    tdb.auditThis1(input.r, input.s, input.rd, input.ipa, done);
//});

//setTimeout(function () {
//    thread
//        .send({ input: 'message', __dirname: __dirname })
//        .on('message', function (message) {
//            console.log("Out of Auditing Thread and about to kill child process");
//            console.log('Work Done', message);
//            thread.kill();
//        });
//}, 1000);


//author : akashdeep.s
//date : 9-feb-2017
//comment : New thread to reduce load on main thread.
function auditThis(resource, soft, record, ip) {
  logger.log('Function : auditThis - Success', 'info', currentFileName);
  // (resource.collection.length == 24 && db.ObjectID.isValid(resource.collection))
  // as discuss with @JD and @Om we are remove child process for record entry/ercord entity
  // QC3-8959 - Backlog: Implement mechanism for storing app record version which can be utilized for Activity information report -- kajal
  //check for the only Schema, Procedure, Entity, and Apps
  // if (resource.collection === 'procedures' || resource.collection === 'entities') {
    if (resource.collection === 'schema' || resource.collection === 'procedures' || resource.collection === 'entities') {

    const thread = spawn(function (input, done) {
      var tdb = require(input.__dirname + '/tdb.js');
      //var tdb = require(input.__dirname + '/threading.js');
      var noAuditArr = ["importfile", "forgotPasswordRecord", "loginattempts", "thresholdsNotification", "securityQuestions", "resources", "settings", "temp", "status", "csventries", "automatedEntries", "versions", "windowQueueFiles"];
      var cObj = {
        collection: input.r.collection,
      };
      if (noAuditArr.indexOf(input.r.collection) == -1) {
        cObj["audit"] = true;
      }
      tdb.configure([cObj], function () {
        tdb.auditThis(input.r, input.s, input.rd, input.ipa, done);
      });
      //setTimeout(function () {
      //tdb.auditThis(input.r, input.s, input.rd, input.ipa, done);
      //}, 2000);
    });
    thread
      .send({ r: resource, s: soft, rd: record, ipa: ip, __dirname: __dirname })
      .on('message', function (message) {
        var id = typeof record._id === 'string' ? db.ObjectID(record._id) : record._id;

        db[resource.collection].updateOne({
          _id: id
        }, { $set: { isAuditing: false } }
          , function (e, d) { if (e) { console.log("Error in Updating AuditTrial...."); } });
        thread.kill();
      });
  }
  else {
    auditThis1(resource, soft, record, ip);
  }
}



function afterVersionIncrement(collection, record, auditOptions) {
  if (collection == "preSchema") {
    if (!record.restoreStableVersion && !record.isDraft) {
      auditUtils.replicateInSchema(db, collection, record, auditOptions);
    }
  }
}

// PQT-1183 by Yamuna
function manipulateAuditingFlag(resource, record){
  if(resource.collection == "preSchema"){
    var recordId = (typeof record._id == 'string') ? db.ObjectID(record._id) : record._id;
    db[resource.collection].updateOne({
      _id: recordId
    }, {
        $set: {
          isAuditing:false
        }
      }
      , function (e, d) {
        if (e) {
          debug(e);
        }
        else {
          //sucess
        }
      })
  }
}

function auditThis1(resource, soft, record, ip) {
  logger.log('Function : auditThis1 - Success', 'info', currentFileName, 'info');

  //by surjeeet.b@producitvet.com.. intrrupted by the BA.. Bug No: QC3-3419
  // if (resource && resource.collection == 'schema') {
  //   if (record && record.entities.length) {
  //     _.forEach(record.entities, function (entity) {
  //       _.remove(entity.questions, { isAppearance: false });
  //     });
  //   }
  // }

  var auditedRecord = _.clone(record);
  if (_.isUndefined(record.version)) {
    auditedRecord.identity = auditedRecord._id;

    delete auditedRecord._id;

    if (auditedRecord.version) {
      logger.log('if(auditedRecord.version) - Success', 'info', currentFileName);
      var newVersion = 0.0;
      if (soft) {
        newVersion = parseFloat((auditedRecord.version + 0.01).toFixed(2));
      } else {
        newVersion = Math.ceil(auditedRecord.version);
        //This means this doc is not in minor version mode
        //ie. 1.0, 3.0, 5.0 and not 1.2, 4.65 etc.
        if (auditedRecord.version === newVersion) {
          debug('is in major version : ' + auditedRecord.version);
          newVersion = auditedRecord.version + 1;
        }
        debug('new version : ' + newVersion);

      }
      auditedRecord.version = newVersion;
      auditedRecord.isMajorVersion = !soft;
    } else {
      if (soft) {
        auditedRecord.version = 0.01;
        auditedRecord.isMajorVersion = false;
      } else {
        auditedRecord.version = 1.0;
        auditedRecord.isMajorVersion = true;
      }

      newVersion = auditedRecord.version;
    }

    //Do field level logging, this is here because it should be after correct
    //versions are calculated and before actual version is commited
    var recordToLog = _.clone(record);
    delete recordToLog.versions;
    if (record.versions && record.versions.length) {
      getVersion(resource, record._id, record.versions[record.versions.length - 1].version, function (e, d) {
        delete d.versions;
        var logs = log(resource, d, recordToLog, auditedRecord.version, ip);
        if (logs.length > 0) {
          var audit = auditUtils.audit(resource, auditedRecord, newVersion, soft, db, record);
          if (audit.getChangeType(logs) == audit.types.behavioral) {
            audit.behaviouralChanges(function () {
              afterVersionIncrement(resource.collection, record, {
                resource: resource,
                soft: soft,
                record: record,
                ip: ip
              })
            })
          } else if (audit.getChangeType(logs) == audit.types.configural) {
            audit.configuralChanges(function () {
              afterVersionIncrement(resource.collection, record, {
                resource: resource,
                soft: soft,
                record: record,
                ip: ip
              })
              // PQT-1008 by Yamuna
            })
          }
        }else{
          manipulateAuditingFlag(resource, record);
        }
      });
    } else {
      var logs = log(resource, {}, recordToLog, auditedRecord.version, ip);
      if (logs.length > 0) {
        var audit = auditUtils.audit(resource, auditedRecord, newVersion, soft, db, record);
        if (audit.getChangeType(logs) == audit.types.behavioral) {
          audit.behaviouralChanges(function () {
            afterVersionIncrement(resource.collection, record, {
              resource: resource,
              soft: soft,
              record: record,
              ip: ip
            })
          })
        } else if (audit.getChangeType(logs) == audit.types.configural) {
          audit.configuralChanges(function () {
            afterVersionIncrement(resource.collection, record, {
              resource: resource,
              soft: soft,
              record: record,
              ip: ip
            })
          })
        }
      }else{
        manipulateAuditingFlag(resource, record);
      }
    }
  } else {
    getVersion(resource, record._id, record.versions[record.versions.length - 1].version, function (ee, dd) {
      auditedRecord.identity = auditedRecord._id;
      delete auditedRecord._id;
      var temp = logIsActive(resource, dd, _.clone(record), auditedRecord.version, ip);
      if (auditedRecord.version) {
        var newVersion = 0.0;
        if (soft || temp == false) {
          newVersion = parseFloat((auditedRecord.version + 0.01).toFixed(2));
        } else {
          newVersion = Math.ceil(auditedRecord.version);
          //This means this doc is not in minor version mode
          //ie. 1.0, 3.0, 5.0 and not 1.2, 4.65 etc.
          if (auditedRecord.version === newVersion) {
            debug('is in major version : ' + auditedRecord.version);
            newVersion = auditedRecord.version + 1;
          }
          debug('new version : ' + newVersion);

        }
        auditedRecord.version = newVersion;
        auditedRecord.isMajorVersion = !soft;
      } else {
        if (soft) {
          auditedRecord.version = 0.01;
          auditedRecord.isMajorVersion = false;
        } else {
          auditedRecord.version = 1.0;
          auditedRecord.isMajorVersion = true;
        }

        newVersion = auditedRecord.version;
      }
      //Do field level logging, this is here because it should be after correct
      //versions are calculated and before actual version is commited
      var recordToLog = _.clone(record);
      delete recordToLog.versions;
      if (record.versions && record.versions.length) {
        getVersion(resource, record._id, record.versions[record.versions.length - 1].version, function (e, d) {
          delete d.versions;
          var logs = log(resource, d, recordToLog, auditedRecord.version, ip);
          if (logs.length > 0) {
            var audit = auditUtils.audit(resource, auditedRecord, newVersion, soft, db, record);
            if (audit.getChangeType(logs) == audit.types.behavioral) {
              audit.behaviouralChanges(function () {
                afterVersionIncrement(resource.collection, record, {
                  resource: resource,
                  soft: soft,
                  record: record,
                  ip: ip
                })
              })
            } else if (audit.getChangeType(logs) == audit.types.configural) {
              audit.configuralChanges(function () {
                afterVersionIncrement(resource.collection, record, {
                  resource: resource,
                  soft: soft,
                  record: record,
                  ip: ip
                })
              })
            }
          }else{
            manipulateAuditingFlag(resource, record);
          }
        });
      } else {
        var logs = log(resource, {}, recordToLog, auditedRecord.version, ip);
        if (logs.length > 0) {
          var audit = auditUtils.audit(resource, auditedRecord, newVersion, soft, db, record);
          if (audit.getChangeType(logs) == audit.types.behavioral) {
            audit.behaviouralChanges(function () {
              afterVersionIncrement(resource.collection, record, {
                resource: resource,
                soft: soft,
                record: record,
                ip: ip
              })
            })
          } else if (audit.getChangeType(logs) == audit.types.configural) {
            audit.configuralChanges(function () {
              afterVersionIncrement(resource.collection, record, {
                resource: resource,
                soft: soft,
                record: record,
                ip: ip
              })
            })
          }
        }else{
          manipulateAuditingFlag(resource, record);
        }
      }
    });
  }
}

module.exports = {
  configure: configure,
  auditThis: auditThis,
  auditThis1: auditThis1
};
