var mapping = {
  '_id':'Id',
  'entityTitle' : 'Entity Name',
  'DefaultApps' : 'Default App',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By'
};

module.exports = mapping;
