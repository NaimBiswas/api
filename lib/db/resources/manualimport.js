'use strict'
/**
* @name db/resources
* @author Jaydipsinh Vaghela <jaydeep.v@productivet.com>
*
* @version 1.0.0
*/

 var mapping = {
   'schema': 'Schema',
   'title' : 'Mapping Name',
   'Schema > title' : 'Schema Name',
   'module > title' : 'Module Name',
   'isAllowHeader': 'Is Allow Header',
   'allowHeader' : 'Allow Header',
   'fileName': 'File Name',
   'createdBy': 'Created By (Id)',
   'modifiedBy' : 'Modified By (Id)',
   'createdByName': 'Created By',
   'modifiedByName' : 'Modified By',
   'createdDate': 'Created Date',
   'modifiedDate': 'Modified Date',
   'Entries > status > status': 'QC Form Status',
   'parentVersion': 'Parent Schema Version'
 };

 module.exports = mapping;
