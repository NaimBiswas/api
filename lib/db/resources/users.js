var mapping = {
  '_id':'Id',
  'username': 'User Name',
  'firstname': 'First Name',
  'lastname' : 'Last Name',
  'email' : 'Email',
  'allowAfterfailedLoginAttempts' : 'Allow user to Login after Failed Attempts',
  'failedLoginAttempts' : 'Failed Login Attempts',//QC3-4882 - Jyotil
  'displayroles' : 'Display Roles',
  'performedAction' : 'Performed Action', // QC3-9624 - Issue 4 - Jyotil
  'temporaryPassword' : 'Temporary Password',
  'verificationCode' : 'Verification Code',
  'createdBy' : 'Created By (Id)',
  'modifiedBy' : 'Modified By (Id)',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By',
  'createdDate' : 'Created Date',
  'modifiedDate' : 'Modified Date',
  'status' : 'Status',
  'ip': 'IP Address',
  '_bsontype' : 'Bson Type',
  'securityQuestion' : 'Security Question',
  'securityAnswer' : 'Security Answer',
  'applicableSites' : 'Sites',
  'title' : 'Title',//QC3-4882 - Jyotil
  'passwordGenerationMethod': 'Password Generation Method',
  'code' : 'Code',
  'roles' : 'Roles',
  'password' : 'Password',
  'isActive': 'Is Active',
  'performedActionOn': 'Action Performed On'
};

module.exports = mapping;
