var mapping = {
  '_id':'Id',
  'token': 'Token',
  'ip': 'IP Address',
  // 'noOfTimeAllowed': 'Allowed',
  'noOfTimeAllowed': 'Total Number of App Submissions',
  // 'noOfTimeUsed': 'Used',
  'app': 'App',
  'module': 'Module',
  'allowMultiple': 'Allow Multiple App Submissions',
  'isForPerEmail':'Number of Submissions per Individual Email Address',
  'message':'Special Message',
  'emails': 'Emails',
  'user': 'User',
  'expireDate': 'Expire Date',
  'lastUsed': 'Last Used Date',
  // 'tried': 'Tried',
  'isActive': 'Is Active',
  'createdBy':'Created By (Id)',
  'modifiedBy' : 'Modified By (Id)',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By',
  'email': 'Email',
  // 'isDeleted': 'Is Deleted',
  'linkShared': 'App Link Shared'
};

module.exports = mapping;
