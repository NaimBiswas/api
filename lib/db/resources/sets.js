var mapping = {
  '_id':'Id',
  'title': 'Sets Name',
  'values.title': 'Set Value Name',
  'ip': 'IP Address',
  'values.value' : 'Set Value',
  'values.isActive' : 'Set Value Is Active',
  'values.isDefaultValue':'Set Value Is Default Value',
  'isActive': 'Is Active',
  'values' : 'Set Value',
  'value': 'Value',
  'isDefaultValue':'Is Default Value',
  'createdBy':'Created By (Id)',
  'modifiedBy' : 'Modified By (Id)',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By'
};

module.exports = mapping;
