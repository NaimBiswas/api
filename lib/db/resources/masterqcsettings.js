var mapping = {
  '_id':'Id',
  //Start: QC3-4882 - Audit Trail Issues.
  //Changed By: Jyotil
  //Description: code to Change the title of Audit trail data.
  'title': 'Master App',
  //End: QC3-4882 - Audit Trail Issues.
  'ip': 'IP Address',
  'allowSpecialSettings': 'Allow Special Settings',
  'isActive': 'Is Active',
  'isAutoFill':'Is Autofill',
  'addSites' : 'Add Sites',
  'hideSites' : 'Hide Sites',
  //Start: QC3-4882 - Audit Trail Issues.
  //Changed By: Jyotil
  //Description: code to Change the title of Audit trail data.
  'schema' : 'App',
  'qcform' : 'App',// QC3-6523 - By Mahammad
  'module' : 'Module',
  'hideQCSingle' : 'Hide App',
  //End: QC3-4882 - Audit Trail Issues.
  'attributes' : 'Attributes',
  'allicableSites' : 'Applicable Sites',
  'createdBy':'Created By (Id)',
  'modifiedBy' : 'Modified By (Id)',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By'
};

module.exports = mapping;
