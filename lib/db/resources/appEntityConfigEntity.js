var mapping = {
  '_id':'Id',
  'token': 'Token',
  'ip': 'IP Address',
  'isActive': 'Is Active',
  'isDefault': 'Is Default',
  'title' : 'Title',
  'entity.title' : 'Entity Title',
  'linkedEntity':'Linked Entity',
  'linkedApp':'Linked App',
  'createdBy':'Created By (Id)',
  'modifiedBy' : 'Modified By (Id)',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By',
  'isDeleted': 'Is Deleted',
};

module.exports = mapping;
