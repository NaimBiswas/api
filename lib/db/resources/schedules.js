var mapping = {
  '_id':'Id',
  'title': 'Frequency Name',
  //Start: QC3-6468 - Set Frequency > Audit Trail > Display incorrrect word.
  //Changed By: Jinal
  //Description: code to Change 'Workflow Review'to 'Workflowre Viwe'.
  'workflowreview' : 'Workflow Review',
  //End: QC3-6468 - Set Frequency > Audit Trail > Display incorrrect word.
  'master' : 'Master App',
  'schema' : 'App',
  //Start: QC3-4882 - Audit Trail Issues.
  //Changed By: Jyotil
  //Description: code to Change 'Schema'to 'App'.
  'parentSchemaVersion' : 'Parent App Version',
  //End: QC3-4882 - Audit Trail Issues.
  'attribute' : 'Attribute',
  'attributeValue' : 'Attribute Value',
  'email' : 'Email Reminder',
  'isActive': 'Is Active',
  'startDate': 'Start Date',
  'endDate' : 'End Date',
  'ismasterQcForm': 'Is Master App',
  'occurenceState' : 'Occurrence State',
  'occurence':'Occurrence',
  'ip': 'IP Address',
  'time' : 'Time',
  'comments' : 'Comments',
  'frequency': 'Frequency',
  'type' : 'Type',
  'subType' : 'Sub Type',
  'daily' : 'Daily',
  'repeats' : 'Repeats',
  'every' : 'Every',
  'weekly'  :'Weekly',
  'days' : 'Days',
  'dayOfMonth' : 'Day Of Month',
  'monthly' : 'Monthly',
  'dayOfWeek' : 'Day Of Week',
  'day' : 'Day',
  'month' : 'Month',
  'enabled' : 'Enabled',
  'Participants' : 'Participants',
  'createdBy':'Created By (Id)',
  'modifiedBy' : 'Modified By (Id)',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By',
  //QC3-6951-Vaibhav
  'requiredTimeFrequencytype':'Required Time to Perform Entry Type',
  'requiredTimeFrequency':'Required Time to Perform Entry',
  'allowToSkip': 'Allow to skip the Scheduled Record'
};

module.exports = mapping;
