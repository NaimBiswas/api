'use strict'
/**
* @name db/resources
* @author
*
* @version 1.0.0
*/

 var mapping = {
   'viewName':'View Name',
   'viewType':'View Type',
   'moduleDetails':'Module Details',
   'projectionFields':'Projection Fields',
   'appDetails':'App Details',
   'joinFields':'Join Fields',
   'createdBy':'Created By (Id)',
   'isActive': 'Is Active',
   'modifiedBy' : 'Modified By (Id)',
   'createdByName':'Created By',
   'modifiedByName' : 'Modified By',
   'createdDate': 'Created Date',
   'modifiedDate': 'Modified Date',
   'isChecked' : 'IsChecked',
    'moduleId' : ' Module Id',
    'title' : 'Title',
    'label' : 'Title',
    'filters':'Filters'
 };

 module.exports = mapping;
