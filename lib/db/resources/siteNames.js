var mapping = {
  '_id':'Id',
  'title': 'Site Level Name',
  'ip': 'IP Address',
  //Start: QC3-4882 - Audit Trail Issues.
  //Changed By: Jyotil
  //Description: code to Change the title of Audit trail data.
  'isActive': 'Is Active',
  //End: QC3-4882 - Audit Trail Issues.
  'createdBy':'Created By (Id)',
  'modifiedBy' : 'Modified By (Id)',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By'
};

module.exports = mapping;
