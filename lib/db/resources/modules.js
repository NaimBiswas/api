var mapping = {
  '_id':'Id',
  'title': 'Name',
  'ip': 'IP Address',
  'allowSpecialSettings': 'Allow Special Settings',
  'isActive': 'Is Active',
  'createdBy':'Created By (Id)',
  'modifiedBy' : 'Modified By (Id)',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By'
};

module.exports = mapping;
