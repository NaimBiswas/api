var mapping = {
'title': 'Group Name',
'module': 'Module',
'displayModules': 'Modules',
'forms': 'Group',
'isActive': 'Is Active',
'isMasterForm': 'Is Master App',
'createdBy':'Created By (Id)',
'modifiedBy' : 'Modified By (Id)',
'createdByName':'Created By',
'modifiedByName' : 'Modified By'
};

module.exports = mapping;
