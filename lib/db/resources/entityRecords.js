'use strict'
/**
* @name db/resources
* @author Prachi <prachi.t@productivet.com>
*
* @version 1.0.0
*/

 var mapping = {
   'entityRecord':'Entity Record',
   'createdBy':'Created By (Id)',
   'entityRecord > isActive' : 'Entity Record > Is Active',
   'isActive': 'Is Active',
   'modifiedBy' : 'Modified By (Id)',
   'createdByName':'Created By',
   'modifiedByName' : 'Modified By',
   'createdDate': 'Created Date',
   'modifiedDate': 'Modified Date',
   'isDeleted':'Is Deleted'
 };

 module.exports = mapping;
