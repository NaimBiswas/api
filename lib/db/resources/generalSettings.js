'use strict'
/**
* @name db/resources
* @author Kajal <kajal.p@productivet.com>
*
* @version 1.0.0
*/

 var mapping = {
   'siteTitle' : 'Standard Report Title',
   'tagline' : 'Standard Report Sub Title',
   'siteLogo' : 'Site Logo',
   'showHideCriteriaDetails' : 'Show Acceptance Criteria Icon in the App',
   'supportContactDetails' : 'Support Contact Details',
   'hideProcTitle':'Show Sequence number of Entity, Procedure and Questions',
   'displayNullValues':'Do not allow to print blank fields',
   'passwordAgingLimit':'Password Aging Limit',
   'passwordAgingMessage':'Password Aging Message',
   'reUsedPasswordsLimit' : 'Prevent Number of Last reused Passwords',
   'reUsedPasswordsMessage':'Prevent Number of Last reused Passwords Message',
   'failedLoginAttemptsLimit' : 'Number of Failed Login Attempts',
   'failedLoginAttemptsLimitMessage' : 'Number of Failed Login Attempts Message',
   'createdBy': 'Created By (Id)',
   'modifiedBy' : 'Modified By (Id)',
   'createdByName': 'Created By',
   'modifiedByName' : 'Modified By',
   'createdDate': 'Created Date',
   'modifiedDate': 'Modified Date',
   'Entries > status > status': 'QC Form Status',
   'parentVersion': 'Parent Schema Version',
   'closeWindowDay': 'Auto Close Window Day'
 };

 module.exports = mapping;
