var mapping = {
  '_id':'Id',
  'title': 'Site Name',
  'level.title': 'Site Level',
  'ip': 'IP Address',
  'parents.title' : 'Parent Site',
  'isActive': 'Is Active',
  'createdBy':'Created By (Id)',
  'modifiedBy' : 'Modified By (Id)',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By',
  'children':'Children',
  'parents': 'Parent',
  'level' : 'Level'
};

module.exports = mapping;
