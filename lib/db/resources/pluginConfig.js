var mapping = {
  '_id':'Id',
  'title' : 'Title',
  'entity' : 'Entity',
  'isActive' : 'Is Active',
  'config' : 'Configuration',
  'url' : 'URL',
  'consumerkey' : 'Consumer Key',
  'consumersecret' : 'Consumer Secret',
  'username' : 'Username',
  'password' : 'Password',
  'sessionid' : 'Sessionid',
  'createdBy':'Created By (Id)',
  'modifiedBy' : 'Modified By (Id)',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By'
};

module.exports = mapping;
