var mapping = {
  '_id':'Id',
  'title':'Friendly Token Title',
  'expirydate':'Expiry date',
  'apps':'Apps',
  'entities':'Entities',
  'apitoken':'API Token',
  'ipaddress':'IP address',
  'isActive': 'Is Active',
  'createdBy':'Created By (Id)',
  'modifiedBy' : 'Modified By (Id)',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By',
  'hash' : 'Hash'
};

module.exports = mapping;
