'use strict'
/**
* @name db/resources
* @author Jaydipsinh Vaghela <jaydeep.v@productivet.com>
*
* @version 1.0.0
*/

 var mapping = {
   'entries':'Entries',
   'eventSaveAndAccept' : 'Save And Accept',
   'isDeleted':'Is Deleted',
   'entityRecord':'Entity Record',
   'entityRecord > isActive' : 'Entity Record > Is Active',
   'isActive': 'Is Active',
   'value' : 'value',
   'comment': 'Comment',
   'islockrecord': 'Is Lock Record',
   'islinkcalendar': 'Is Link Calendar',
   'schmeatitle' : 'App Title',
   'reviewStatus': 'Review Status',
   'ip': 'IP Address',
   'parentVersion': 'App Version',
   'createdDateInfo': 'Created Date', //as we are not passing Created Date 
   'createdByInfo': 'Created By Info',
   'levels': 'Site Levels',
   'verifiedBy': 'Verified By',
   'verificationDate': 'Verification Date',
   'isattributesadded': 'Is Attributes Added',
   'firstname': 'First Name',
   'lastname': 'Last Name',
   'username': 'Username',
   'title': 'Title',
   'workflowreview': 'Reviewer Workflows',
   'setworkflowuser': 'setworkflowuser',
   'roles': 'Roles',
   'user': 'User',
   'reviewstatus': 'Review Status',
   'schemaID': 'App Title',
   'createdBy':'Created By (Id)',
   'modifiedBy' : 'Modified By (Id)',
   'createdByName':'Created By',
   'modifiedByName' : 'Modified By',
   'createdDate': 'Created Date',
   'modifiedDate': 'Modified Date',
   'Entries > status > status': 'QC Form Status',
   'masterQCSettings':'MasterApp',
   //Start: QC3-6081 - Group Settings > Master Apps of blood are displayed when only Equipment module is selected
   //Changed By: Jyotil
   //Description: code to change audit trail data title.
   'batchEntryId':'Batch Entry ID',
   //End: QC3-6081 - Group Settings > Master Apps of blood are displayed when only Equipment module is selected
   'Title':'Title',
   'qcEntriesStatus':'Entry Status',
   //Start: QC3-6043 - app/master app: spelling mistakes are found in in audit trail.
   //Changed By: Jyotil
   //Description: code to change audit trail data title.
   'masterQCSettingsId':'MasterApp SettingsId',
   'revieweduser':'Reviewed User',
   'appMapping':'App Mapping',
   'maapingfield':'Mapping Field',
   'assignment': 'Assignment',
   'assignee': 'Assigned To',
   'assignedBy': 'Assigned By',
   'assignedDate': 'Assigned Date',
   'comments': 'Comment',
  //  QC3-9460 by Yamuna
   'attrValue': 'AttributeValue'
   //End: QC3-6043 - app/master app: spelling mistakes are found in in audit trail.
 };

 module.exports = mapping;
