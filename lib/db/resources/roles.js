var mapping = {
  '_id':'Id',
  'title': 'Name',
  'ip': 'IP Address',
  'isActive': 'Is Active',
  'description':'Description',
  'modules' : 'Modules',
  'permissions' : 'Permissions',
  'entity' : 'Entity',
  'entityType' : 'Entity Type',
  'create' : 'Create',
  'view' : 'View',
  'edit' : 'Edit',
  'createdBy':'Created By (Id)',
  'modifiedBy' : 'Modified By (Id)',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By'
};

module.exports = mapping;
