var mapping = {
  'formTitle': 'App Title',
  'copyNonKeyAttributes': 'Copy Non Key Attributes',
  'copySiteSelections': 'Copy Site Selections',
  'copyNonKeyEntities': 'Copy Non Key Entities Attribute',// QC3-6040 - Jyotil
  'isDefaultBatchMode': 'Start App in Default Batch Mode',// QC3-9409 - Jyotil
  'isActive': 'Is Active',// QC3-4882 - Jyotil
  'batchentry': 'Batch Entry Settings',
  'createdBy':'Created By (Id)',
  'modifiedBy' : 'Modified By (Id)',
  'createdByName':'Created By',
  'modifiedByName' : 'Modified By'
};

module.exports = mapping;
