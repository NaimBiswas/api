﻿'use strict'
/**
* @name db
* @author Dave Amit <dave.a@productivet.com>
*
* @version 0.0.0
*/

var skin = require('mongoskin');
var config = require('../../config/config.js');
var q = require('q');
var defaultCnstr = (process.env.DB_PORT ? process.env.DB_PORT.replace('tcp://', 'mongodb://') : config.db);
var dbUri = process.env.MONGOLAB_URI || defaultCnstr;
var db = skin.db(dbUri, { native_parser: true });
var _ = require('lodash');
var logger = require('../../logger');
var currentFileName = __filename;
var diff = require(__dirname + '/../diff');
var auditUtils = require('./audit.utils');

// Used to create new objectID in from route
db.ObjectID = skin.ObjectID;
var count = 0;
/**
* Configuration routine to setup entities and db
*
* @param  {Object},{Object}....,{Object} - Entity with format explained in
*         lib/db/index.js.
* @return {Object} - Configured db instance.
*
* @public
*/
function configure(arr, cb) {

    _.forEach(arr, function configureEntities(entity) {
        //console.log('configuring ' + entity.collection + ' ...');
        db.bind(entity.collection);

        if (entity.index) {
            // For ref.
            // https://docs.mongodb.org/manual/core/index-text/#create-text-index
            db[entity.collection].createIndex(entity.index, { default_language: "none" });
            //console.log('configuring index for ' + entity.collection + ' to ');
            //console.log(entity.index);
        }

        // Following has to be done otherwise 'count' behaves wieredly
        db[entity.collection].count(function countCallback(err, count) {
            //console.log(entity.collection + ' has ' + count + ' records as of now.');
        });

        if (entity.audit) {
            entity.auditCollections = entity.auditCollections || entity.collection + 'AuditLogs';
            entity.logsCollection = entity.logsCollection || entity.collection + 'Logs';

            console.log(entity.collection + ' will be audited to ' + entity.auditCollections);
            console.log(entity.collection + ' will be logged to ' + entity.logsCollection);

            db.bind(entity.auditCollections);
            db.bind(entity.logsCollection);

            //var auditIndex = _.clone(entity.index);
            //auditIndex.identity = 1;
            // auditIndex.version = 1;

            //db[entity.auditCollections].createIndex(auditIndex);
            db[entity.auditCollections].count(function countCallback(err, count) {
                //We are attaching audit collection as entity's audit property
                //So just check audit of given entity, if audit is present, do the
                //auditing.
                db[entity.collection].audit = db[entity.auditCollections];
                db[entity.collection].auditThis = auditThis.bind(auditThis, entity, false);
                db[entity.collection].auditThisSoftly = auditThis.bind(auditThis, entity, true);

                console.log(entity.auditCollections + ' has ' + count + ' records as of now.');

                //QC3-8888 - mahammad
                //db[entity.logsCollection].createIndex(auditIndex);
                db[entity.logsCollection].count(function countCallback(err, count) {
                    //We are attaching audit collection as entity's audit property
                    //So just check audit of given entity, if audit is present, do the
                    //auditing.
                    db[entity.collection].log = db[entity.logsCollection];
                    try {
                        if (entity.collection.length === 24) {
                            db[entity.collection].logFields = require(__dirname + '/resources/entries');
                        } else {
                            db[entity.collection].logFields = require(__dirname + '/resources/' + entity.collection);
                        }
                        //console.log('Audit mapping for ' + entity.collection.bold.underline);
                        cb("done");
                        //console.log(db[entity.collection].logFields);
                    } catch (e) {
                        //console.log('No audit fields mapping for ' + entity.collection.red.bold.underline);
                        cb("done");
                    }

                    console.log(entity.logsCollection + ' has ' + count + ' records as of now.');
                });
                //QC3-8888 - mahammad
            });
        }



    });

    return db;
}

function getVersion(resource, identity, version, cb) {
    if (typeof identity === 'string') {
        identity = db.ObjectID(identity);
    }
    db[resource.collection].audit.findOne({
        identity: identity,
        version: parseFloat(version)
    },
        function findOneVersionCallback(err, data) {
            if (err) {
                cb(err);
                return;
            } else if (!data) {
                cb('Version not found');
                return;
            }
            data._id = data.identity;
            delete data.identity;

            cb(null, data);
        });
}

function getMfilesToken(username, password, vaultid, cb) {
    var request1 = require('request');
    var options = {
        method: 'POST',
        url: config.dmsproject.dmsUrl + '/server/authenticationtokens',
        headers:
            { 'cache-control': 'no-cache' },
        body: '{"Username":"' + username + '","Password":"' + password + '","VaultGuid":"' + vaultid + '"}'
    };

    request1(options, function (error, response, body) {
        if (error) {
            logger.log("DMS error in getMfilesToken function : " + error, 'error', currentFileName);
            //cb(error, response, body);
            //   throw new Error(error)
        }
        cb(error, response, body);

    });

}
function changeVersionInMfiles(mfilestoken, displayid, idpropertyvalue, versionproperty, schemaid, newVersion, cb) {

    if (versionproperty.indexOf(',') > -1) {
        var tmpidpropertyvalue = idpropertyvalue.split(',');
        var tmpversionproperty = versionproperty.split(',');
        var indexpos = 0;
        _.forEach(tmpidpropertyvalue, function (elementtmpidpropertyvalue) {
            if (schemaid.trim().toLowerCase() == elementtmpidpropertyvalue.trim().toLowerCase()) {
                console.log('newVersion: ' + newVersion + ' tmpversionproperty[indexpos]' + tmpversionproperty[indexpos] + ' =========indexpos ' + indexpos);
                tmpversionproperty[indexpos] = newVersion;
                newVersion = '';
                console.log('2: == newVersion: ' + newVersion + ' tmpversionproperty[indexpos]' + tmpversionproperty[indexpos] + ' =========indexpos ' + indexpos);
                var notprintcomma = tmpversionproperty.length - 1;
                var indexpos1 = 0;
                _.forEach(tmpversionproperty, function (elementtmpversionproperty) {

                    if (notprintcomma !== indexpos1) {
                        newVersion = newVersion + elementtmpversionproperty + ', ';
                    }
                    else {
                        newVersion = newVersion + elementtmpversionproperty;
                        console.log('elementtmpversionproperty newVersion: ' + newVersion);
                        var request1 = require('request');
                        var options = {
                            method: 'POST',
                            url: config.dmsproject.dmsUrl + '/objects/0/' + displayid + '/latest/properties',
                            headers:
                                { 'X-Authentication': mfilestoken },

                            body: '[{"PropertyDef": "' + config.dmsproject.PERFEQTAAPPVERSION + '","TypedValue": { "DataType": "1", "Value":"' + newVersion + '"}}]'
                        };


                        request1(options, function (error, response, body) {
                            if (error) {
                                logger.log("DMS error in changeVersionInMfiles function while POST APP Version preperty with comma : " + error, 'error', currentFileName);
                                //cb(error, response, body);
                                //   throw new Error(error)
                            }
                            cb(error, response, body);

                        });
                    }
                    indexpos1 = indexpos1 + 1;
                })
            }
            indexpos = indexpos + 1;
        });
    }
    else {
        var request1 = require('request');
        var options = {
            method: 'POST',
            url: config.dmsproject.dmsUrl + '/objects/0/' + displayid + '/latest/properties',
            headers: { 'X-Authentication': mfilestoken },
            body: '[{"PropertyDef": "' + config.dmsproject.PERFEQTAAPPVERSION + '","TypedValue": { "DataType": "1", "Value":"' + newVersion + '"}}]'
        };

        request1(options, function (error, response, body) {
            if (error) {
                logger.log("DMS error in changeVersionInMfiles function while POST APP Version preperty without comma : " + error, 'error', currentFileName);
                //cb(error, response, body);
                //   throw new Error(error)
            }
            cb(error, response, body);

        });
    }

}

function getFilesFromMfiles(mfilestoken, schemaid, cb) {
    var request = require("request");
    var options;
    if (schemaid) {
        var qsconfig = '?p' + config.dmsproject.PERFEQTAAPPID + '*=' + schemaid;

        options = {
            method: 'GET',
            url: config.dmsproject.dmsUrl + '/objects/0' + qsconfig,
            headers:
                { 'x-authentication': mfilestoken },
        };
    }
    request(options, function (error, response, body) {
        if (error) {
            logger.log("DMS error in getFilesFromMfiles function", 'error', currentFileName);
            //cb(error, response, body);
        }

        cb(error, response, body);
    });


}

function getFilesFromElementId(mfilestoken, elementid, cb) {
    var request = require("request");
    var options;
    if (elementid) {
        options = {
            method: 'GET',
            url: config.dmsproject.dmsUrl + '/objects/0/' + elementid + '/latest/properties',
            headers:
                { 'x-authentication': mfilestoken },
        };
    }

    request(options, function (error, response, body) {
        if (error) {
            logger.log("DMS error in getFilesFromElementId function : " + error, 'error', currentFileName);
            //cb(error, response, body);
        }

        cb(error, response, body);
    });


}

function getMfilesCredentials(identity, cb) {
    db.collection('documentsettings').findOne({
        perfeqtauserId: identity,

    },
        function findOneVersionCallback(err, data) {
            if (err) {
                logger.log("DMS error in getMfilesCredentials function while getting credentials", 'error', currentFileName);
                //cb(err);
                return;
            } else if (!data) {
                cb('Version not found');
                return;
            }
            cb(null, data);
        });
}

function logIsActive(resource, o, n, version, ip) {

    delete o.versions;
    delete n.versions;

    //displaylog.log('Old Version :: ' + JSON.stringify(o));
    //displaylog.log('New Version :: ' + JSON.stringify(n));

    var set = _.forEach(diff(o, n), function (value) {
        value.identity = n._id;
        value.modifiedDate = new Date(n.modifiedDate);
        value.modifiedBy = n.modifiedBy;
        value.version = version;
        value.ip = ip;
        return value;
    });

    //displaylog.log('======================================');
    //displaylog.log('SET :: ' + JSON.stringify(set));

    if (set.length == 1 && set[0].path == 'isActive') {
        //displaylog.log('Return False');
        return false;
    } else {
        //displaylog.log('Return True');
        return true;
    }

}
function log(resource, o, n, version, ip, don) {
    var set = _.forEach(diff(_.cloneDeep(o), n), function (value) {
        if (typeof n._id === 'string') {
            value.identity = db.ObjectID(n._id);
        }
        else {
            value.identity = n._id;
        }

        var isP = _.find(n.auditTrailInfo, ['path', value.path]);
        if (isP) {
            value.ntype = isP.type;
            value.nformat = isP.format;
        }
        var isoP = _.find(o.auditTrailInfo, ['path', value.path]);
        if (isoP) {
            value.otype = isoP.type;
            value.oformat = isoP.format;
        }

        value.modifiedDate = new Date(n.modifiedDate);
        value.modifiedBy = n.modifiedBy;
        value.version = version;
        value.ip = ip;
        return value;
    });
    // Code by jaydip
    // Code for remove extra fields from audit trail logs

    // Note :: a.path === "createdBy" || a.path === "modifiedBy" Need to remove this both for valid audit trail
    set = _.filter(set, function (a) {
        var endswithid = true;
        if (a.path.endsWith("id")) {
            if (_.includes(a.path, "selectedFileFormat") || (_.includes(a.path, "conditionalWorkflows") && _.includes(a.path, "value"))) {
                endswithid = false;
            }
        }
        //Start: QC3-4882 - Audit Trail Issues.
        //Changed By: Jyotil
        //Description: code to remove entry into audit trail logs.
        //QC3-3849 -bug add " || a.path.endsWith('.size') " to restict to go file size in audit trail
        return !((a.path.endsWith('createdBy') || a.path.endsWith('modifiedBy') || a.path === "reviewStatus" || a.path === "loader" || a.path === "displayAssignee"
            || (a.path != "isLockRecord" && a.path.endsWith("Record")) || a.path.endsWith("_id") || a.path.endsWith("s#")
            || a.path === "isAuditing" || a.path.endsWith("comboId") || a.path.endsWith("parentId") || a.path.endsWith("siteId") || a.path.startsWith("auditTrailInfo")
            || a.path.endsWith("levelId") || a.path === "id" || a.path === "password" || a.path === "temporaryPassword"
            || a.path === 'verificationCodeModifiedDate' || a.path === 'securityAnswerModifiedDate' || a.path === 'passwordModifiedDate'
            || a.path === 'code'|| a.path === 'Code' || a.path === 'assignmentEmailNotification'
            || a.path === "loggedInDateTime" || a.path == "usersUUID" || a.path.startsWith("passwordLog") || a.path === "fromToken"
            || a.path.endsWith('isApp') || a.path.endsWith('entryId') || (a.path.endsWith('schemaId') && a.path !== 'schemaID') || a.path.endsWith('entityKeyAttributeComboId')
            || a.path === "restangularCollection" || a.path === "restangularEtag" || a.path === "restangularized" || a.path === "fromServer" || a.path === "route"  //QC3-8743: mahammad
            || a.path.endsWith('.route') || a.path.endsWith('.restangularized') || a.path.endsWith('.fromServer') || a.path.endsWith('.restangularCollection')
            || a.path.startsWith('entries.tempAppDataStore') || a.path.startsWith('entries.errorAppDataStore') || a.path.endsWith('ismapped') || a.path.startsWith('entries.appToExpandMessage') || a.path.endsWith('datetime') || a.path.endsWith('.size')
            || a.path.endsWith('hasSelected') || a.path.endsWith('tried') || a.path === 'appId'
            //Start: QC3-6043 - app/master app: spelling mistakes are found in in audit trail - Jyotil    || (resource.collection==='schema' && a.path === 'allowCreaterObj.id') // QC3-9996 - Jyotil
            // MDA bugs - start
            || (resource.collection === 'schema' && (a.path.endsWith('id') || a.path === 'isDeleted' || (a.path.startsWith('attributes') && a.path.indexOf('validation.validationSet.existingSet.versions') >= 0))) // QC3-9996, QC3-10080 - Jyotil
            // MDA bugs - end
            || a.path.startsWith("workflowReviewHistory") || a.path.endsWith("reviewedUserId") || a.path.endsWith("userSelectionType") || (resource.collection === 'entries' && a.path.endsWith('masterQCSettingsId'))
            || a.path === "timeZone" //Yamuna timeZone Remove  at 233
            || a.path == "route" || a.path == "restangularized" || a.path == "fromServer" || a.path == "restangularCollection"
            || a.path.endsWith('id')
            || a.path.startsWith('holdRemovedAttributeSeqIds') //QC3-QC3-6935
            || a.path.endsWith('entityId') || a.path.endsWith('entityKeyAttributeId') || a.path.endsWith('entityKeyAttributeDateFormat')
            || a.path === "windowEndDate" || a.path === "pendingFailure" || a.path === "processFailure"
            || a.path === "unitTestedUnits" || a.path === "remainingUnits" || a.path === "remainingFailures" || a.path === "attributes"
            || a.path.startsWith("firstStageInformation") || a.path.startsWith("countDetails") || a.path.startsWith("default")
            || a.path.startsWith("secondStageStartWithCount") || a.path.startsWith("secondStageInformation")
            || a.path === "displayModules" || a.path === "isConCurrentUserActive" || a.path.endsWith("seqId") || a.path.endsWith("formId") || a.path === "isConCurrentUserAdmin") && endswithid);
    });
    //End: QC3-4882 - Audit Trail Issues.
    // End code by jaydip

    if (set && set.length) { // && (set[0].path == "isActive" && set.length > 1)

        var logFields = db[resource.collection].logFields;
        if (logFields) {
            //displaylog.log('<<<<<<<<<<<<<< setttt 22222222222 >>>>>>>>>>>>');
            _.forEach(set, function set(item) {
                item.title = _.map(item.path.split('.'), function (item, index) {
                    // item.toString().length < 10 add condition to restrict append # tag on title when item name is timestamp
                    // generally item is not reach upto that number so it never create problem when we check like this for timestamp
                    if (item.toString().length < 10 && (parseInt(item) + '') === item) {
                        return '#' + (parseInt(item) + 1);
                    } else {
                        return logFields[item] || item;
                    }
                }).join(' > ');
            });

            // Code by jaydip
            // Code for remove Entries.status entry from qc entry audit logs
            set = _.filter(set, function (a) {
                return !((a.title.startsWith("Set Value >") && a.title.endsWith("> Name")) || (a.title.endsWith("> Route")) || (a.title.endsWith("> Restangularized")) || (a.title.endsWith("> From Server")) || (a.title.endsWith("> Restangular Collection")) ||
                    a.path == 'code' || a.title.startsWith('Code >') ||

                    a.path == 'emailChangeCode' || a.title.startsWith('emailChangeCode >') ||
                    a.path == 'forgotPasswordCode' || a.title.startsWith('forgotPasswordCode >') ||
                    a.path == 'verificationCode' || a.title.startsWith('verificationCode >') ||
                    (a.title.startsWith("Entries > status") && a.title !== "Entries > status > status") || a.title.startsWith("Entries > FailMesssageForQCForm") || a.title.startsWith("levelIds >") || a.title.endsWith("draftStatus") || a.path === "verificationCode");
            });
            // End code by jaydip
        }
        db[resource.logsCollection].insertMany(set, function (e, data) {
            if (e) {
                console.log('Failed to capture log for ' + n._id + ' change log is as follows');
                //console.log(set);
            }
            count++;
            if (count == 2) {
                don("2");
            }
        });
    }
    return set;
}

function auditThis(resource, soft, record, ip, don) {

    //by surjeeet.b@producitvet.com.. intrrupted by the BA.. Bug No: QC3-3419
    // if (resource && resource.collection == 'schema') {
    //   if (record && record.entities.length) {
    //     _.forEach(record.entities, function (entity) {
    //       _.remove(entity.questions, { isAppearance: false });
    //     });
    //   }
    // }

    var auditedRecord = _.clone(record);
    if (_.isUndefined(record.version)) {
        if (typeof auditedRecord._id === 'string') {
            auditedRecord.identity = db.ObjectID(auditedRecord._id);
        }
        else {
            auditedRecord.identity = auditedRecord._id;
        }

        delete auditedRecord._id;

        if (auditedRecord.version) {
            var newVersion = 0.0;
            if (soft) {
                newVersion = parseFloat((auditedRecord.version + 0.01).toFixed(2));
            } else {
                newVersion = Math.ceil(auditedRecord.version);
                //This means this doc is not in minor version mode
                //ie. 1.0, 3.0, 5.0 and not 1.2, 4.65 etc.
                if (auditedRecord.version === newVersion) {
                    //console.log('is in major version : ' + auditedRecord.version);
                    newVersion = auditedRecord.version + 1;
                }
                console.log('new version : ' + newVersion);

            }
            auditedRecord.version = newVersion;
            auditedRecord.isMajorVersion = !soft;
        } else {
            if (soft) {
                auditedRecord.version = 0.01;
                auditedRecord.isMajorVersion = false;
            } else {
                auditedRecord.version = 1.0;
                auditedRecord.isMajorVersion = true;
            }

            newVersion = auditedRecord.version;
        }

        //Do field level logging, this is here because it should be after correct
        //versions are calculated and before actual version is commited
        var recordToLog = _.clone(record);
        delete recordToLog.versions;
        if (record.versions && record.versions.length) {
            getVersion(resource, record._id, record.versions[record.versions.length - 1].version, function (e, d) {
                delete d.versions;
                var logs = log(resource, d, recordToLog, auditedRecord.version, ip, don);
                if (logs > 0) {
                    var audit = auditUtils.audit(resource, auditedRecord, newVersion, soft, db, record, columnInfo, d);
                    if (audit.getChangeType(logs) == audit.types.behavioral) {
                        audit.behaviouralChanges(function () {
                            count++;
                            if (count == 2) {
                                don("1");
                            }
                        })
                    } else if (audit.getChangeType(logs) == audit.types.configural) {
                        audit.configuralChanges(function () {
                            count++;
                            if (count == 2) {
                                don("1");
                            }
                        })
                    }
                }
            });
        } else {
            var logs = log(resource, {}, recordToLog, auditedRecord.version, ip, don);
            if (logs.length > 0) {
                var audit = auditUtils.audit(resource, auditedRecord, newVersion, soft, db, record, columnInfo, '');
                if (audit.getChangeType(logs) == audit.types.behavioral) {
                    audit.behaviouralChanges(function () {
                        count++;
                        if (count == 2) {
                            don("2");
                        }
                    })
                } else if (audit.getChangeType(logs) == audit.types.configural) {
                    audit.configuralChanges(function () {
                        count++;
                        if (count == 2) {
                            don("2");
                        }
                    })
                }
            }
        }
    } else {
        getVersion(resource, record._id, record.versions[record.versions.length - 1].version, function (ee, dd) {
            auditedRecord.identity = auditedRecord._id;
            if (typeof auditedRecord._id === 'string') {
                auditedRecord.identity = db.ObjectID(auditedRecord._id);
            }
            else {
                auditedRecord.identity = auditedRecord._id;
            }
            delete auditedRecord._id;
            var temp = logIsActive(resource, dd, _.clone(record), auditedRecord.version, ip);
            //displaylog.log('Is Active :: ' + temp);
            if (auditedRecord.version) {
                var newVersion = 0.0;
                if (soft || temp == false) {
                    newVersion = parseFloat((auditedRecord.version + 0.01).toFixed(2));
                } else {
                    newVersion = Math.ceil(auditedRecord.version);
                    //This means this doc is not in minor version mode
                    //ie. 1.0, 3.0, 5.0 and not 1.2, 4.65 etc.
                    if (auditedRecord.version === newVersion) {
                        // console.log('is in major version : ' + auditedRecord.version);
                        newVersion = auditedRecord.version + 1;
                    }
                    console.log('new version : ' + newVersion);

                }
                auditedRecord.version = newVersion;
                auditedRecord.isMajorVersion = !soft;
            } else {
                if (soft) {
                    auditedRecord.version = 0.01;
                    auditedRecord.isMajorVersion = false;
                } else {
                    auditedRecord.version = 1.0;
                    auditedRecord.isMajorVersion = true;
                }

                newVersion = auditedRecord.version;
            }

            //Do field level logging, this is here because it should be after correct
            //versions are calculated and before actual version is commited
            var recordToLog = _.clone(record);
            delete recordToLog.versions;
            if (record.versions && record.versions.length) {
                getVersion(resource, record._id, record.versions[record.versions.length - 1].version, function (e, d) {
                    delete d.versions;
                    var logs = log(resource, d, recordToLog, auditedRecord.version, ip, don);
                    if (logs.length > 0) {
                        var audit = auditUtils.audit(resource, auditedRecord, newVersion, soft, db, record, columnInfo, d);
                        if (audit.getChangeType(logs) == audit.types.behavioral) {
                            audit.behaviouralChanges(function () {
                                count++;
                                if (count == 2) {
                                    if (config.dmsproject.showDMS) {
                                        getMfilesCredentials(record.modifiedBy, function (e, d) {
                                            if (d) {

                                                if (!_.isUndefined(d.userName) && !_.isUndefined(d.userPassword) && !_.isUndefined(d.vaultId)) {
                                                    getMfilesToken(d.userName, d.userPassword, d.vaultId, function (error, response, body) {

                                                        if (body) {
                                                            logger.log("DMS ecredentials are valid", 'info', currentFileName);
                                                            var objectValue = JSON.parse(body);
                                                            var mfilestoken = objectValue.Value;
                                                            getFilesFromMfiles(mfilestoken, record._id, function (error, response, body) {
                                                                if (body) {
                                                                    var objectValue = JSON.parse(body);
                                                                    var Items = objectValue.Items;
                                                                    var versionpropertyvalue = null;
                                                                    var idpropertyvalue = null;
                                                                    if (Items.length > 0) {
                                                                        logger.log("DMS find some files for schemaid " + record._id, 'info', currentFileName);
                                                                        _.forEach(Items, function (element) {
                                                                            getFilesFromElementId(mfilestoken, element.DisplayID, function (error, response, body) {
                                                                                if (body) {
                                                                                    var objectValue1 = JSON.parse(body);

                                                                                    _.forEach(objectValue1, function (elementinside) {
                                                                                        // console.log('--------------elementinside.PropertyDef  : '+elementinside.PropertyDef);
                                                                                        if (elementinside.PropertyDef === config.dmsproject.PERFEQTAAPPID) {

                                                                                            idpropertyvalue = elementinside.TypedValue.DisplayValue;


                                                                                        }
                                                                                        else {
                                                                                            logger.log("DMS files not found for schemaid " + record._id, 'info', currentFileName);
                                                                                            don("3");
                                                                                        }

                                                                                    });
                                                                                    if (idpropertyvalue !== null && versionpropertyvalue !== null) {
                                                                                        changeVersionInMfiles(mfilestoken, element.DisplayID, idpropertyvalue, versionpropertyvalue, record._id, newVersion, function (error, response, body) {
                                                                                            versionpropertyvalue = null;
                                                                                            idpropertyvalue = null;
                                                                                            if (body) {
                                                                                                logger.log("DMS version property updated successfully for schemaid " + record._id + " and mfiles file id " + element.DisplayID, 'info', currentFileName);
                                                                                                // console.log('=============Last IF============'+ body);
                                                                                            }

                                                                                        });

                                                                                    }
                                                                                }
                                                                            })


                                                                        })
                                                                    }
                                                                    else {
                                                                        logger.log("DMS files not found for schemaid " + record._id, 'info', currentFileName);
                                                                        don("3");
                                                                    }
                                                                }

                                                            });

                                                        }


                                                    });
                                                }
                                                else {
                                                    logger.log("DMS ecredentials not valid", 'info', currentFileName);
                                                    don("3");
                                                }
                                            }
                                            else {
                                                logger.log("DMS ecredentials not found", 'info', currentFileName);
                                                don("3");
                                            }


                                        });
                                    }
                                    else {
                                        logger.log("DMS is OFF", 'info', currentFileName);
                                        don("3");
                                    }
                                }
                            })
                        } else if (audit.getChangeType(logs) == audit.types.configural) {
                            audit.configuralChanges(function () {
                                count++;
                                if (count == 2) {
                                    if (config.dmsproject.showDMS) {
                                        getMfilesCredentials(record.modifiedBy, function (e, d) {
                                            if (d) {

                                                if (!_.isUndefined(d.userName) && !_.isUndefined(d.userPassword) && !_.isUndefined(d.vaultId)) {
                                                    getMfilesToken(d.userName, d.userPassword, d.vaultId, function (error, response, body) {

                                                        if (body) {
                                                            logger.log("DMS ecredentials are valid", 'info', currentFileName);
                                                            var objectValue = JSON.parse(body);
                                                            var mfilestoken = objectValue.Value;
                                                            getFilesFromMfiles(mfilestoken, record._id, function (error, response, body) {
                                                                if (body) {
                                                                    var objectValue = JSON.parse(body);
                                                                    var Items = objectValue.Items;
                                                                    var versionpropertyvalue = null;
                                                                    var idpropertyvalue = null;
                                                                    if (Items.length > 0) {
                                                                        logger.log("DMS find some files for schemaid " + record._id, 'info', currentFileName);
                                                                        _.forEach(Items, function (element) {
                                                                            getFilesFromElementId(mfilestoken, element.DisplayID, function (error, response, body) {
                                                                                if (body) {
                                                                                    var objectValue1 = JSON.parse(body);

                                                                                    _.forEach(objectValue1, function (elementinside) {
                                                                                        // console.log('--------------elementinside.PropertyDef  : '+elementinside.PropertyDef);
                                                                                        if (elementinside.PropertyDef === config.dmsproject.PERFEQTAAPPID) {

                                                                                            idpropertyvalue = elementinside.TypedValue.DisplayValue;


                                                                                        }
                                                                                        else {
                                                                                            logger.log("DMS files not found for schemaid " + record._id, 'info', currentFileName);
                                                                                            don("3");
                                                                                        }

                                                                                    });
                                                                                    if (idpropertyvalue !== null && versionpropertyvalue !== null) {
                                                                                        changeVersionInMfiles(mfilestoken, element.DisplayID, idpropertyvalue, versionpropertyvalue, record._id, newVersion, function (error, response, body) {
                                                                                            versionpropertyvalue = null;
                                                                                            idpropertyvalue = null;
                                                                                            if (body) {
                                                                                                logger.log("DMS version property updated successfully for schemaid " + record._id + " and mfiles file id " + element.DisplayID, 'info', currentFileName);
                                                                                                // console.log('=============Last IF============'+ body);
                                                                                            }

                                                                                        });

                                                                                    }
                                                                                }
                                                                            })


                                                                        })
                                                                    }
                                                                    else {
                                                                        logger.log("DMS files not found for schemaid " + record._id, 'info', currentFileName);
                                                                        don("3");
                                                                    }
                                                                }

                                                            });

                                                        }


                                                    });
                                                }
                                                else {
                                                    logger.log("DMS ecredentials not valid", 'info', currentFileName);
                                                    don("3");
                                                }
                                            }
                                            else {
                                                logger.log("DMS ecredentials not found", 'info', currentFileName);
                                                don("3");
                                            }


                                        });
                                    }
                                    else {
                                        logger.log("DMS is OFF", 'info', currentFileName);
                                        don("3");
                                    }
                                }
                            })
                        }
                    }
                });
            } else {
                //log(resource, {}, recordToLog, auditedRecord.version, ip);
                var logs = log(resource, {}, recordToLog, auditedRecord.version, ip, don);
                if (logs.length > 0) {
                    var audit = auditUtils.audit(resource, auditedRecord, newVersion, soft, db, record, columnInfo, '');
                    if (audit.getChangeType(logs) == audit.types.behavioral) {
                        audit.behaviouralChanges(function () {
                            count++;
                            if (count == 2) {
                                don("4");
                            }
                        })
                    } else if (audit.getChangeType(logs) == audit.types.configural) {
                        audit.configuralChanges(function () {
                            count++;
                            if (count == 2) {
                                don("4");
                            }
                        })
                    }
                }
            }
        });
    }
}

//BI -start

function columnInfo(oldRecord, newRecord, soft, auditData, resource) {
    var deferred = q.defer();
    if (config.buildColumn) {
        if (resource.collection == 'schema') {

            var columnInfoObj = [];
            var dateColumn = [];
            var numberColumn = [];
            var entityColumn = [];
            var mainObj = {}
            var appid = (newRecord.identity) ? newRecord.identity : newRecord._id;
            appid = appid.toString();
            appid = db.ObjectID(appid);
            if (oldRecord) {
                newRecord.version = auditData.ops[0].version
            }
            try {
                db.collection('types').find({}).toArray(function (err, docs) {
                    if (err) {
                        console.log(err);
                    } else {
                        var dateTimeformates = getDateTimeFormats(docs);
                        if (oldRecord) {
                            db.bind('ReportSchemaColumns');
                            db.collection('ReportSchemaColumns').findOne({ appId: appid.toString() }, function (err, oldData) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    if (newRecord && newRecord.attributes && newRecord.attributes.length > 0) {
                                        _.forEach(newRecord.attributes, function (attribute) {
                                            var data = prepareObj(attribute, appid, newRecord, attribute, 'attribute', auditData, null, dateTimeformates);
                                            if (attribute && attribute.type && attribute.type.format && attribute.type.format.title && attribute.type.format.title == 'Date') {
                                                var dateData = prepareDateColumn(attribute, 'attribute', newRecord, attribute, appid, null);
                                                var dateFlag = _.find(oldData.controlchartColumn.dateColumn, function (datecol) {
                                                    if (dateData.questionid == datecol.questionid) {
                                                        datecol.questionTitle = dateData.questionTitle;
                                                        datecol.version = newRecord.version;
                                                        datecol.findFlag = true;
                                                        return datecol;
                                                    }
                                                })
                                                if (!dateFlag) {
                                                    dateData.findFlag = true;
                                                    oldData.controlchartColumn.dateColumn.push(dateData);
                                                }
                                            }
                                            if (checkNumberType(attribute)) {
                                                var numberData = prepareNumberColumn(attribute, 'attribute', newRecord, attribute, appid);
                                                var numberFlag = _.find(oldData.controlchartColumn.numberColumn, function (numbercol) {
                                                    if (numberData.questionid == numbercol.questionid) {
                                                        numbercol.questionTitle = numberData.questionTitle;
                                                        numbercol.version = newRecord.version;
                                                        numbercol.findFlag = true;
                                                        return numbercol;
                                                    }
                                                })
                                                if (!numberFlag) {
                                                    numberData.findFlag = true;
                                                    oldData.controlchartColumn.numberColumn.push(numberData);
                                                }
                                            }
                                            var flag = _.find(oldData.columnInfo, function (oldColumnData) {
                                                if (data.questionid == oldColumnData.questionid) {
                                                    oldColumnData.title = data.title;
                                                    oldColumnData.displayName = data.displayName;
                                                    oldColumnData.versions.push(newRecord.version);
                                                    oldColumnData.extraInformation.info.push(data.extraInformation.info[0]);
                                                    return oldColumnData;
                                                }
                                            });
                                            if (!flag) {
                                                oldData.columnInfo.push(data);
                                            }
                                        })
                                    }
                                    if (newRecord && newRecord.entities && newRecord.entities.length > 0) {
                                        _.forEach(newRecord.entities, function (entity) {
                                            _.forEach(entity.questions, function (entityquestion) {
                                                var data = prepareObj(entityquestion, appid, newRecord, entity, 'entity', auditData, null, dateTimeformates);
                                                if (entityquestion && entityquestion.type && entityquestion.type.format && entityquestion.type.format.title && entityquestion.type.format.title == 'Date') {
                                                    var dateData = prepareDateColumn(entityquestion, 'entity', newRecord, entity, appid, null);
                                                    var dateFlag = _.find(oldData.controlchartColumn.dateColumn, function (datecol) {
                                                        if (dateData.questionid == datecol.questionid) {
                                                            datecol.questionTitle = dateData.questionTitle;
                                                            datecol.version = newRecord.version;
                                                            datecol.findFlag = true;
                                                            return datecol;
                                                        }
                                                    })
                                                    if (!dateFlag) {
                                                        dateData.findFlag = true;
                                                        oldData.controlchartColumn.dateColumn.push(dateData);
                                                    }
                                                }
                                                if (checkNumberType(entityquestion)) { //number question
                                                    var numberData = prepareNumberColumn(entityquestion, 'entity', newRecord, entity, appid);
                                                    var numberFlag = _.find(oldData.controlchartColumn.numberColumn, function (numbercol) {
                                                        if (numberData.questionid == numbercol.questionid) {
                                                            numbercol.questionTitle = numberData.questionTitle;
                                                            numbercol.version = newRecord.version;
                                                            numbercol.findFlag = true;
                                                            return numbercol;
                                                        }
                                                    })
                                                    if (!numberFlag) {
                                                        numberData.findFlag = true;
                                                        oldData.controlchartColumn.numberColumn.push(numberData);
                                                    }
                                                }
                                                if (entityquestion && entityquestion.isKeyValue) { //key entity
                                                    var entityData = prepareEntityColumn(entityquestion, 'entity', newRecord, entity, appid);
                                                    var entityFlag = _.find(oldData.controlchartColumn.entityColumn, function (entitycol) {
                                                        if (entityData.questionid == entitycol.questionid) {
                                                            entitycol.questionTitle = entityData.questionTitle;
                                                            entitycol.version = newRecord.version;
                                                            entitycol.findFlag = true;
                                                            return entitycol;
                                                        }
                                                    })
                                                    if (!entityFlag) {
                                                        entityData.findFlag = true;
                                                        oldData.controlchartColumn.entityColumn.push(entityData);
                                                    }
                                                }

                                                var flag = _.find(oldData.columnInfo, function (oldColumnData) {
                                                    if (data.questionid == oldColumnData.questionid) {
                                                        oldColumnData.title = data.title;
                                                        oldColumnData.displayName = data.displayName;
                                                        oldColumnData.versions.push(newRecord.version);
                                                        oldColumnData.extraInformation.info.push(data.extraInformation.info[0]);
                                                        return oldColumnData;
                                                    }
                                                });

                                                if (!flag) {
                                                    oldData.columnInfo.push(data);
                                                }
                                            })
                                        })
                                    }
                                    if (newRecord && newRecord.procedures && newRecord.procedures.length > 0) {
                                        _.forEach(newRecord.procedures, function (procedure) {
                                            _.forEach(procedure.questions, function (procedurequestion) {
                                                var data = prepareObj(procedurequestion, appid, newRecord, procedure, 'procedure', auditData, null, dateTimeformates);

                                                if (procedurequestion && procedurequestion.type && procedurequestion.type.format && procedurequestion.type.format.title && procedurequestion.type.format.title == 'Date') {
                                                    var format = setFormat(procedurequestion.type.format.metadata.datetime, dateTimeformates);
                                                    var dateData = prepareDateColumn(procedurequestion, 'procedure', newRecord, procedure, appid, format);
                                                    var dateFlag = _.find(oldData.controlchartColumn.dateColumn, function (datecol) {
                                                        if (dateData.questionid == datecol.questionid) {
                                                            datecol.questionTitle = dateData.questionTitle;
                                                            datecol.version = newRecord.version;
                                                            datecol.findFlag = true;
                                                            return datecol;
                                                        }
                                                    })
                                                    if (!dateFlag) {
                                                        dateData.findFlag = true;
                                                        oldData.controlchartColumn.dateColumn.push(dateData);
                                                    }
                                                }
                                                if (checkNumberType(procedurequestion)) {
                                                    var numberData = prepareNumberColumn(procedurequestion, 'procedure', newRecord, procedure, appid);
                                                    var numberFlag = _.find(oldData.controlchartColumn.numberColumn, function (numbercol) {
                                                        if (numberData.questionid == numbercol.questionid) {
                                                            numbercol.questionTitle = numberData.questionTitle;
                                                            numbercol.version = newRecord.version;
                                                            numbercol.findFlag = true;
                                                            return numbercol;
                                                        }
                                                    })
                                                    if (!numberFlag) {
                                                        numberData.findFlag = true;
                                                        oldData.controlchartColumn.numberColumn.push(numberData);
                                                    }
                                                }

                                                var flag = _.find(oldData.columnInfo, function (oldColumnData) {
                                                    if (data.questionid == oldColumnData.questionid) {
                                                        oldColumnData.title = data.title;
                                                        oldColumnData.displayName = data.displayName;
                                                        oldColumnData.versions.push(newRecord.version);
                                                        oldColumnData.extraInformation.info.push(data.extraInformation.info[0]);
                                                        return oldColumnData;
                                                    }
                                                });

                                                if (!flag) {
                                                    oldData.columnInfo.push(data);
                                                }
                                            })
                                        })
                                    }
                                    if (newRecord && newRecord.entities && newRecord.entities.length > 0) {
                                        _.forEach(newRecord.entities, function (entity) {
                                            if (entity && entity.conditionalWorkflows && entity.conditionalWorkflows.length) {
                                                _.forEach(entity.conditionalWorkflows, function (iProcedure) {
                                                    if (iProcedure && iProcedure.procedureToExpand) {
                                                        _.forEach(iProcedure.procedureToExpand.questions, function (iprocedurequestion) {
                                                            var data = prepareObj(iprocedurequestion, appid, newRecord, iProcedure.procedureToExpand, 'procedure', auditData, 'iprocedure', dateTimeformates);

                                                            if (iprocedurequestion && iprocedurequestion.type && iprocedurequestion.type.format && iprocedurequestion.type.format.title && iprocedurequestion.type.format.title == 'Date') {
                                                                var format = setFormat(iprocedurequestion.type.format.metadata.datetime, dateTimeformates);
                                                                var dateData = prepareDateColumn(iprocedurequestion, 'procedure', newRecord, iProcedure.procedureToExpand, appid, format);
                                                                var dateFlag = _.find(oldData.controlchartColumn.dateColumn, function (datecol) {
                                                                    if (dateData.questionid == datecol.questionid) {
                                                                        datecol.questionTitle = dateData.questionTitle;
                                                                        datecol.version = newRecord.version;
                                                                        datecol.findFlag = true;
                                                                        return datecol;
                                                                    }
                                                                })
                                                                if (!dateFlag) {
                                                                    dateData.findFlag = true;
                                                                    oldData.controlchartColumn.dateColumn.push(dateData);
                                                                }
                                                            }
                                                            if (checkNumberType(iprocedurequestion)) {
                                                                var numberData = prepareNumberColumn(iprocedurequestion, 'procedure', newRecord, iProcedure.procedureToExpand, appid);
                                                                var numberFlag = _.find(oldData.controlchartColumn.numberColumn, function (numbercol) {
                                                                    if (numberData.questionid == numbercol.questionid) {
                                                                        numbercol.questionTitle = numberData.questionTitle;
                                                                        numbercol.version = newRecord.version;
                                                                        numbercol.findFlag = true;
                                                                        return numbercol;
                                                                    }
                                                                })
                                                                if (!numberFlag) {
                                                                    numberData.findFlag = true;
                                                                    oldData.controlchartColumn.numberColumn.push(numberData);
                                                                }
                                                            }

                                                            var flag = _.find(oldData.columnInfo, function (oldColumnData) {
                                                                if (data.questionid == oldColumnData.questionid) {
                                                                    oldColumnData.title = data.title;
                                                                    oldColumnData.displayName = data.displayName;
                                                                    oldColumnData.versions.push(newRecord.version);
                                                                    oldColumnData.extraInformation.info.push(data.extraInformation.info[0]);
                                                                    return oldColumnData;
                                                                }
                                                            });

                                                            if (!flag) {
                                                                oldData.columnInfo.push(data);
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        });
                                    }

                                    if (newRecord && newRecord.workflowreview && newRecord.workflowreview.length > 0) {
                                        reviewedArray = getReviewedArray(newRecord, reviewedArray, appid);
                                        _.forEach(reviewedArray, function (d) {
                                            var flag = _.find(oldData.columnInfo, function (olditem) {
                                                if (olditem.questionid == d.questionid) {
                                                    olditem.versions.push(newRecord.version);
                                                    return olditem;
                                                    //return true;
                                                }
                                            })
                                            if (!flag) {
                                                d['versions'].push(newRecord.version);
                                                oldData.columnInfo.push(d);
                                            }
                                        })
                                    }
                                    _.forEach(oldData.controlchartColumn.dateColumn, function (item) {
                                        if (item.questionid == 'Created Date') {
                                            item.findFlag = true;
                                        }
                                    })
                                    _.remove(oldData.controlchartColumn.dateColumn, function (item) {
                                        return !item.findFlag;
                                    });
                                    _.remove(oldData.controlchartColumn.numberColumn, function (item) {
                                        return !item.findFlag;
                                    });
                                    _.remove(oldData.controlchartColumn.entityColumn, function (item) {
                                        return !item.findFlag;
                                    });
                                    oldData.controlchartColumn.numberColumn = _.map(oldData.controlchartColumn.numberColumn, function (item) {
                                        return _.omit(item, 'findFlag');
                                    })
                                    oldData.controlchartColumn.dateColumn = _.map(oldData.controlchartColumn.dateColumn, function (item) {
                                        return _.omit(item, 'findFlag');
                                    })
                                    oldData.controlchartColumn.entityColumn = _.map(oldData.controlchartColumn.entityColumn, function (item) {
                                        return _.omit(item, 'findFlag');
                                    })
                                    db.collection('ReportSchemaColumns').update({ '_id': oldData._id }, oldData, function (err, data) {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            deferred.resolve(data);
                                        }
                                    })
                                }
                            });
                        } else if (newRecord) {
                            var reviewedArray = [];
                            var extraverionArr = getAppVersionExtra(appid);
                            _.forEach(extraverionArr, function (d) {
                                columnInfoObj.push(d);
                            })
                            if (newRecord && newRecord.attributes && newRecord.attributes.length > 0) {
                                _.forEach(newRecord.attributes, function (attribute) {
                                    columnInfoObj.push(prepareObj(attribute, appid, newRecord, attribute, 'attribute', auditData, null, dateTimeformates));
                                    if (attribute && attribute.type && attribute.type.format && attribute.type.format.title && attribute.type.format.title == 'Date') {
                                        dateColumn.push(prepareDateColumn(attribute, 'attribute', newRecord, attribute, appid, null))
                                    } else if (checkNumberType(attribute)) {
                                        numberColumn.push(prepareNumberColumn(attribute, 'attribute', newRecord, attribute, appid))
                                    }
                                })
                            }
                            if (newRecord && newRecord.entities && newRecord.entities.length > 0) {
                                _.forEach(newRecord.entities, function (entity) {
                                    _.forEach(entity.questions, function (entityquestion) {
                                        columnInfoObj.push(prepareObj(entityquestion, appid, newRecord, entity, 'entity', auditData, null, dateTimeformates))
                                        if (entityquestion && entityquestion.isKeyValue) {
                                            entityColumn.push(prepareEntityColumn(entityquestion, 'entity', newRecord, entity, appid));
                                        }
                                        if (entityquestion && entityquestion.type && entityquestion.type.format && entityquestion.type.format.title && entityquestion.type.format.title == 'Date') {
                                            dateColumn.push(prepareDateColumn(entityquestion, 'entity', newRecord, entity, appid, null))
                                        } else if (checkNumberType(entityquestion)) {
                                            numberColumn.push(prepareNumberColumn(entityquestion, 'entity', newRecord, entity, appid))
                                        }
                                    })
                                })
                            }
                            if (newRecord && newRecord.procedures && newRecord.procedures.length > 0) {
                                _.forEach(newRecord.procedures, function (procedure) {
                                    _.forEach(procedure.questions, function (procedurequestion) {
                                        columnInfoObj.push(prepareObj(procedurequestion, appid, newRecord, procedure, 'procedure', auditData, null, dateTimeformates));
                                        if (procedurequestion && procedurequestion.type && procedurequestion.type.format && procedurequestion.type.format.title && procedurequestion.type.format.title == 'Date') {
                                            var format = setFormat(procedurequestion.type.format.metadata.datetime, dateTimeformates);
                                            dateColumn.push(prepareDateColumn(procedurequestion, 'procedure', newRecord, procedure, appid, format))
                                        } else if (checkNumberType(procedurequestion)) {
                                            numberColumn.push(prepareNumberColumn(procedurequestion, 'procedure', newRecord, procedure, appid))
                                        }
                                    })
                                })
                            }
                            if (newRecord && newRecord.entities && newRecord.entities.length > 0) {
                                _.forEach(newRecord.entities, function (entity) {
                                    if (entity && entity.conditionalWorkflows && entity.conditionalWorkflows.length) {
                                        _.forEach(entity.conditionalWorkflows, function (iProcedure) {
                                            if (iProcedure && iProcedure.procedureToExpand) {
                                                _.forEach(iProcedure.procedureToExpand.questions, function (iprocedurequestion) {
                                                    columnInfoObj.push(prepareObj(iprocedurequestion, appid, newRecord, iProcedure.procedureToExpand, 'procedure', auditData, 'iprocedure', dateTimeformates))
                                                    if (iprocedurequestion && iprocedurequestion.type && iprocedurequestion.type.format && iprocedurequestion.type.format.title && iprocedurequestion.type.format.title == 'Date') {
                                                        var format = setFormat(iprocedurequestion.type.format.metadata.datetime, dateTimeformates);
                                                        dateColumn.push(prepareDateColumn(iprocedurequestion, 'procedure', newRecord, iProcedure.procedureToExpand, appid, format))
                                                    } else if (checkNumberType(iprocedurequestion)) {
                                                        numberColumn.push(prepareNumberColumn(iprocedurequestion, 'procedure', newRecord, iProcedure.procedureToExpand, appid))
                                                    }
                                                })
                                            }
                                        })
                                    }
                                });
                            }

                            db.collection('siteNames').find({ isActive: true }).toArray(function (err, sitesdata) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    var sitesArr = getAppSites(sitesdata, appid);
                                    _.forEach(sitesArr, function (d) {
                                        columnInfoObj.push(d);
                                    });
                                    var dataArr = getAppExtra(appid);
                                    _.forEach(dataArr, function (d) {
                                        columnInfoObj.push(d);
                                    });

                                    var batchAssgineeArr = getAssgineeAndBatchColumn(appid);
                                    _.forEach(batchAssgineeArr, function (d) {
                                        columnInfoObj.push(d);
                                    });

                                    reviewedArray = getReviewedArray(newRecord, reviewedArray, appid);
                                    if (reviewedArray && reviewedArray.length > 0) {
                                        _.forEach(reviewedArray, function (d) {
                                            d['versions'].push(newRecord.version);
                                            columnInfoObj.push(d);
                                        });
                                    }
                                    mainObj['title'] = newRecord.title;
                                    mainObj['appId'] = appid.toString();
                                    mainObj['columnInfo'] = columnInfoObj;
                                    mainObj['controlchartColumn'] = {};
                                    var createDateObj = {
                                        appTitle: newRecord.title,
                                        appId: appid.toString(),
                                        questionid: 'Created Date',
                                        parentTitle: 'Created Date',
                                        parentId: 'Created Date',
                                        parentsequenceId: 'Created Date',
                                        questionTitle: 'Created Date',
                                        questionId: 'Created Date',
                                        questionsequenceId: 'Created Date',
                                        versions: '1',
                                        type: 'Date',
                                        maintype: 'Created Date'
                                    }
                                    dateColumn.push(createDateObj);
                                    mainObj['controlchartColumn']['dateColumn'] = dateColumn;
                                    mainObj['controlchartColumn']['numberColumn'] = numberColumn;
                                    mainObj['controlchartColumn']['entityColumn'] = entityColumn;
                                    db.collection('ReportSchemaColumns').insert(mainObj, function (err, data) {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            deferred.resolve(data);
                                        }
                                    });
                                }
                            });


                        } else {
                            deferred.resolve();
                        }
                    }
                })
            } catch (e) {
                console.log(e);
            }
        } else {
            deferred.resolve();
        }
    } else {
        deferred.resolve();
    }
    return deferred.promise;
}

function prepareNumberColumn(currentQuestion, label, schema, parentObject, appId) {
    var obj = {
        appTitle: schema.title,
        appId: appId.toString(),
        questionid: appId.toString() + '_' + parentObject['s#'] + '_' + currentQuestion['s#'],
        parentTitle: parentObject.title,
        parentId: parentObject._id,
        parentsequenceId: parentObject['s#'],
        questionTitle: currentQuestion.title,
        //for entity questionid = entityId + questionsequenceId
        questionId: currentQuestion._id ? currentQuestion._id : parentObject._id + '_' + currentQuestion['s#'],
        questionsequenceId: currentQuestion['s#'],
        versions: schema.version,
        type: 'Number',
        maintype: label
    };
    return obj;
}

function prepareDateColumn(currentQuestion, label, schema, parentObject, appId, format) {

    var obj = {
        appTitle: schema.title,
        appId: appId.toString(),
        questionid: appId.toString() + '_' + parentObject['s#'] + '_' + currentQuestion['s#'],
        parentTitle: parentObject.title,
        parentId: parentObject._id,
        parentsequenceId: parentObject['s#'],
        questionTitle: currentQuestion.title,
        //for entity questionid = entityId + questionsequenceId
        questionId: currentQuestion._id ? currentQuestion._id : parentObject._id + '_' + currentQuestion['s#'],
        questionsequenceId: currentQuestion['s#'],
        versions: schema.version,
        formate: currentQuestion.type.format.metadata.format ? currentQuestion.type.format.metadata.format : format,
        type: 'Date',
        maintype: label
    };
    return obj;
}

function prepareEntityColumn(currentQuestion, label, schema, parentObject, appId) {
    var obj = {
        appTitle: schema.title,
        appId: appId.toString(),
        questionid: appId.toString() + '_' + parentObject['s#'] + '_' + currentQuestion['s#'],
        parentTitle: parentObject.title,
        parentId: parentObject._id,
        parentsequenceId: parentObject['s#'],
        questionTitle: currentQuestion.title,
        questionId: parentObject._id,
        questionsequenceId: currentQuestion['s#'],
        versions: schema.version,
        formate: currentQuestion.type.format.metadata.format,
        type: currentQuestion.type.format.title
    };
    return obj;
}

function getReviewedArray(schema, arr, appid) {
    if (schema.workflowreview && schema.workflowreview.length > 0) {
        arr = arr || [];
        //for each workflow review there will be three column.
        _.forEach(schema.workflowreview, function (wfr, idx) {
            //check if this index is in previous array
            if ((arr.length / 3) <= idx) {
                arr = arr.concat(getReviewdByColumn(appid, idx));
            }
        });
    }
    return arr;
}

function getReviewdByColumn(appId, idx) {
    var arr = [];
    //1: ReviewBy
    var reviewedObj = {};
    reviewedObj.title = 'reviewBy' + idx;
    reviewedObj.indx = idx;
    reviewedObj.displayName = 'Review By-' + (idx + 1);
    reviewedObj.questionid = 'reviewBy' + idx;
    reviewedObj.dataType = 'Text';
    reviewedObj.versions = [];
    reviewedObj.dataField = 'reviewBy' + idx;
    reviewedObj.caption = 'Review By-' + (idx + 1);
    reviewedObj.entriesid = 'workflowreview.' + idx + '.revieweduser';
    reviewedObj.reviewid = 'workflowreview.revieweduser';
    reviewedObj.extraInformation = {
        appId: appId,
        maintype: 'reviewer workflow',
        subtype: 'secondExtra',
        label: 'extra',
    }
    arr.push(reviewedObj);
    //2 : reviewed Date
    var reviewedDateObj = {};
    reviewedDateObj.title = 'reviewDate' + idx;
    reviewedDateObj.indx = idx;
    reviewedDateObj.displayName = 'Review Date-' + (idx + 1);
    reviewedDateObj.questionid = 'reviewDate' + idx;
    reviewedDateObj.dataType = 'Date';
    reviewedDateObj.formateId = 'MM/dd/yyyy hh:mm:ss';
    reviewedDateObj.versions = [];
    reviewedDateObj.dataField = 'reviewDate' + idx;
    reviewedDateObj.caption = 'Review Date-' + (idx + 1);
    reviewedDateObj.entriesid = 'workflowreview.' + idx + '.verificationDate';
    reviewedDateObj.reviewid = 'workflowreview.verificationDate';
    reviewedDateObj.format = 'MM/dd/yyyy hh:mm:ss';
    reviewedDateObj.extraInformation = {
        appId: appId,
        maintype: 'reviewer workflow',
        subtype: 'secondExtra',
        label: 'extra',
    }
    arr.push(reviewedDateObj);

    //3 ReviewRemark
    var reviewedRemarkObj = {};
    reviewedRemarkObj.title = 'reviewRemark' + idx;
    reviewedRemarkObj.indx = idx;
    reviewedRemarkObj.displayName = 'Review Remark-' + (idx + 1);
    reviewedRemarkObj.questionid = 'reviewRemark' + idx;
    reviewedRemarkObj.dataType = 'Text';
    reviewedRemarkObj.versions = [];
    reviewedRemarkObj.dataField = 'reviewRemark' + idx;
    reviewedRemarkObj.caption = 'Review Remark-' + (idx + 1);
    reviewedRemarkObj.entriesid = 'workflowreview.' + idx + '.comment';
    reviewedRemarkObj.reviewid = 'workflowreview.comment';
    reviewedRemarkObj.format = '';
    reviewedRemarkObj.extraInformation = {
        appId: appId,
        maintype: 'reviewer workflow',
        subtype: 'secondExtra',
        label: 'extra',
    }
    arr.push(reviewedRemarkObj);

    return arr;
}

function getAssgineeAndBatchColumn(appId) {
    var colArr = [{
        title: 'batchEntryId',
        displayName: 'Batch Entry ID',
        questionid: 'batchEntryId',
        dataType: 'Text',
        versions: [],
        caption: 'Batch Entry ID',
        dataField: 'batchEntryId',
        entriesid: 'batchEntryId',
        format: '',
        extraInformation: {
            appId: appId,
            maintype: 'zother',
            subtype: 'secondExtra',
            label: 'extra',
        }
    },
    {
        title: 'displayAssignee',
        displayName: 'Current Assignee',
        questionid: 'displayAssignee',
        dataType: 'Text',
        versions: [],
        caption: 'Current Assignee',
        dataField: 'displayAssignee',
        entriesid: 'displayAssignee',
        extraInformation: {
            appId: appId,
            maintype: 'zother',
            subtype: 'secondExtra',
            label: 'extra',
        }
    },
    {
        title: 'curentAssigneeComment',
        displayName: 'Comment for Current Assignee',
        questionid: 'curentAssigneeComment',
        dataType: 'Text',
        versions: [],
        caption: 'Comment for Current Assignee',
        dataField: 'curentAssigneeComment',
        entriesid: 'curentAssigneeComment',
        extraInformation: {
            appId: appId,
            maintype: 'zother',
            subtype: 'secondExtra',
            label: 'extra',
        }
    }
    ];
    return colArr;
}

function prepareObj(questions, appid, newRecord, currenttypedetails, currenttype, auditData, type, dateTimeformates) {

    var obj = {
        displayName: questions.title,
        questionid: '',
        title: '',
        dataType: '',
        formateId: '',
        versions: [],
        extraInformation: {
            'type': currenttype,
            'maintype': currenttype,
            'appId': appid.toString(),
            'info': [{
                'entitySeq': currenttypedetails['s#'],
                's#': questions['s#'],
                'questionType': questions.type.title,
                'appVersion': newRecord.version,
                'appId': auditData.ops[0]._id
            }]
        }
    };
    if (currenttype == 'attribute') {
        obj['title'] = questions.title.replace(/[@^!|&\/\\,+()$~%'":*?<>{.#}]/g, '-');
        obj['caption'] = questions.title;
        obj['entriesid'] = questions['s#'] + '.attributes';
        obj['questionid'] = appid.toString() + '_' + currenttypedetails._id;
        obj['dataField'] = obj.questionid;
        obj['dataType'] = questions.type.format.title == 'None' ? 'Text' : questions.type.format.title;
        if (questions.type && questions.type.format && questions.type.format.metadata && questions.type.format.metadata.datetime) {
            obj['formateId'] = setFormat(questions.type.format.metadata.datetime, dateTimeformates);
            obj['format'] = setFormat(questions.type.format.metadata.datetime, dateTimeformates);
            obj['dataType'] = questions.type.format.title;
        }
    } else if (currenttype == "entity") {
        obj['title'] = questions.title.replace(/[@^!|&\/\\,+()$~%'":*?<>{.#}]/g, '-').replace(/ /g, '');
        obj['caption'] = questions.title;
        obj['entriesid'] = questions['s#'] + '-' + questions['parentId'] + '.' + questions['parentId'];
        obj['questionid'] = appid.toString() + '_' + currenttypedetails['s#'] + '_' + questions['s#'];
        obj['dataField'] = obj.questionid;
        if (questions.type && questions.type.format) {
            if (questions.type.format.metadata == 'Decimal') {
                obj['dataType'] = config.numberDataType;
                obj['formateId'] = config.numberDataTypeFormat;
                obj['format'] = config.numberDataTypeFormat;

            } else if (questions.type.format.metadata == 'power') {
                obj['dataType'] = config.numberDataType;
                obj['formateId'] = 'Scientific';
                obj['format'] = 'Scientific';

            } else if (questions.type.format.metadata && questions.type.format.metadata.datetime) {
                obj['dataType'] = questions.type.format.title;
                obj['formateId'] = setFormat(questions.type.format.metadata.datetime, dateTimeformates);
                obj['format'] = setFormat(questions.type.format.metadata.datetime, dateTimeformates);
            } else {
                obj['dataType'] = questions.type.format.title == 'None' ? 'Text' : questions.type.format.title;
            }
        }
    } else if (currenttype == "procedure") {
        obj['questionid'] = appid.toString() + '_' + currenttypedetails['s#'] + '_' + questions['s#'];
        obj['caption'] = questions.title;
        obj['entriesid'] = questions['s#'] + '-' + questions['parentId'] + '.' + questions['parentId'];
        if (questions.type && questions.type.format) {
            if (questions.type.format.metadata == 'Decimal') {
                obj['dataType'] = config.numberDataType;
                obj['formateId'] = config.numberDataTypeFormat;
                obj['format'] = config.numberDataTypeFormat;

            } else if (questions.type.format.metadata == 'power') {
                obj['dataType'] = config.numberDataType;
                obj['formateId'] = 'Scientific';
                obj['format'] = 'Scientific';

            } else if (questions.type.format.metadata && questions.type.format.metadata.datetime) {
                obj['dataType'] = questions.type.format.title;
                obj['formateId'] = setFormat(questions.type.format.metadata.datetime, dateTimeformates);
                obj['format'] = setFormat(questions.type.format.metadata.datetime, dateTimeformates);
            } else {
                obj['dataType'] = questions.type.format.title == 'None' ? 'Text' : questions.type.format.title;
            }
        }
        if (type == "iprocedure") {
            obj['title'] = questions.title;
            obj['dataField'] = obj.questionid;

        } else {
            obj['title'] = questions.title.replace(/[@^!|&\/\\,+()$~%'":*?<>{.#}]/g, '-').replace(/ /g, '');
            obj['dataField'] = obj.questionid;
        }

    } else {
        obj['questionid'] = appid.toString() + '_' + currenttypedetails['s#'] + '_' + questions['s#'];
        obj['title'] = questions.title.replace(/[@^!|&\/\\,+()$~%'":*?<>{.#}]/g, '-').replace(/ /g, '').replace(/\(|\)|Con/g, '');
    }
    obj['versions'].push(newRecord.version);

    return obj;
}

function setFormat(formateId, arrayofFormate) {
    if (formateId && arrayofFormate && arrayofFormate.length > 0) {
        var filtredformate = _.find(arrayofFormate, {
            'Id': formateId
        });
        if (filtredformate && filtredformate.hasOwnProperty('title')) {
            switch (filtredformate.title) {
                case 'MONTH/DD/YYYY':
                    return 'MM/dd/yyyy';
                case 'HH:MM':
                    return 'HH:mm';
                case 'HH:MM AM/PM':
                    return 'hh:mm tt';
                case 'HH:MM:SS':
                    return 'HH:mm:ss';
                case 'HH:MM:SS AM/PM':
                    return 'hh:mm:ss tt';
                //return 'H:mm:ss tt';
                default:
                    return filtredformate.title.replace(/D/g, 'd').replace(/Y/g, 'y');
            }
        }
    }
    return 'MM/dd/yyyy';

}

function getDateTimeFormats(types) {
    var datetimeformats = [];
    if (types.length > 0) {
        _.forEach(types, function (typs, typsIndx) {
            if (typs.hasOwnProperty('formats') && typs['formats'].length > 0) {
                _.forEach(typs['formats'], function (frmts, frmtsindx) {
                    if (frmts.hasOwnProperty('metadata') && frmts['metadata'].length > 0) {
                        _.forEach(frmts['metadata'], function (mtdata, mtdatasindx) {
                            if (mtdata.hasOwnProperty('allowItems') && mtdata['allowItems'].length > 0) {
                                _.forEach(mtdata['allowItems'], function (allwitms, allwitmsIndx) {
                                    var objdateformate = {
                                        'Id': allwitms['_id'],
                                        'title': allwitms['title']
                                    };
                                    datetimeformats.push(objdateformate);
                                });
                            }

                        });
                    }
                });
            }
        });
    }
    return datetimeformats;
}

function checkNumberType(item) {
    if (item && item.type && item.type.format && item.type.format.title && (item.type.format.title == 'Number' || item.type.format.title == 'Exponential' || item.type.format.title == 'Percentage' || item.type.format.title == 'Currency' || item.type.format.title == 'Scientific')) {
        return true;
    } else {
        return false;
    }
}

function getAppSites(siteNames, appId) {
    var arr = [];
    _.forEach(siteNames, function (site, sidx) {
        var obj = {
            displayName: '',
            questionid: '',
            entriesid: '',
            caption: '',
            dataField: '',
            title: '',
            dataType: '',
            formateId: '',
            format: '',
            extraInformation: {
                type: 'site',
                maintype: 'zother',
                label: 'extra',
                subtype: 'site',
                level: sidx,
                appId: appId
            }
        };
        obj.extraInformation.level = site._id;
        obj.title = site.title.replace(/ /g, '');
        obj.caption = site.title;
        obj.dataField = site.title.replace(/ /g, '');
        obj.entriesid = 'levels.' + sidx + '.title';
        obj.format = undefined;
        obj.displayName = site.title;
        obj.questionid = obj.title;
        obj.dataType = undefined;
        obj.formateId = undefined;
        arr.push(obj);
    });
    return arr;
};

function getAppVersionExtra(appId) {
    var arr = [];
    var appNameObj = {};
    appNameObj.title = 'appName';
    appNameObj.displayName = 'App Name';
    appNameObj.questionid = 'appName';
    appNameObj.dataField = 'appName';
    appNameObj.caption = 'App Name';
    appNameObj.entriesid = 'appName';
    appNameObj.format = '';
    appNameObj.dataType = 'Text';
    appNameObj.extraInformation = {
        appId: appId,
        maintype: 'zother',
        subtype: 'firstExtra',
        label: 'extra',
    }
    arr.push(appNameObj);

    var parentVersionObj = {};
    parentVersionObj.title = 'parentVersion';
    parentVersionObj.displayName = 'App Version';
    parentVersionObj.questionid = 'parentVersion';
    parentVersionObj.dataField = 'parentVersion';
    parentVersionObj.caption = 'App Version';
    parentVersionObj.entriesid = 'parentVersion';
    parentVersionObj.dataType = 'General Number';
    parentVersionObj.format = '';
    parentVersionObj.formateId = 'General Number';
    parentVersionObj.extraInformation = {
        appId: appId,
        maintype: 'zother',
        subtype: 'firstExtra',
        label: 'extra',
    }
    arr.push(parentVersionObj);
    return arr;
}

function getAppExtra(appId) {
    var arr = [];
    var statusobj = {}
    statusobj.title = 'status';
    statusobj.displayName = 'Status';
    statusobj.questionid = 'status';
    statusobj.dataField = 'status';
    statusobj.caption = 'Status';
    statusobj.entriesid = 'isActive';
    statusobj.format = undefined;
    statusobj.dataType = undefined;
    statusobj.formateId = undefined;
    statusobj.extraInformation = {
        appId: appId,
        maintype: 'zother',
        subtype: 'secondExtra',
        label: 'extra',
    }
    arr.push(statusobj);

    var createdBy = {};
    createdBy.title = 'createdBy';
    createdBy.displayName = 'Created By';
    createdBy.questionid = 'createdBy';
    createdBy.dataType = 'Text';
    createdBy.dataField = 'createdBy';
    createdBy.caption = 'Created By';
    createdBy.entriesid = 'createdByName';
    createdBy.format = '';
    createdBy.extraInformation = {
        appId: appId,
        maintype: 'zother',
        subtype: 'secondExtra',
        label: 'extra',
    }
    arr.push(createdBy);

    var createdddate = {};
    createdddate.title = 'createdDate';
    createdddate.displayName = "Created Date";
    createdddate.questionid = 'createdDate';
    createdddate.dataType = 'Date';
    createdddate.dataField = 'createdDate';
    createdddate.caption = "Created Date";
    createdddate.entriesid = 'createdDate';
    createdddate.format = 'MM/dd/yyyy HH:mm:ss';
    createdddate.formateId = 'MM/dd/yyyy HH:mm:ss';
    createdddate.extraInformation = {
        appId: appId,
        maintype: 'zother',
        subtype: 'secondExtra',
        label: 'extra',
    }
    arr.push(createdddate);

    var modifiedBy = {};
    modifiedBy.title = 'modifiedBy';
    modifiedBy.displayName = 'Modified By';
    modifiedBy.questionid = 'modifiedBy';
    modifiedBy.dataType = 'Text';
    modifiedBy.dataField = 'modifiedBy';
    modifiedBy.caption = 'Modified By';
    modifiedBy.entriesid = 'modifiedByName';
    modifiedBy.extraInformation = {
        appId: appId,
        maintype: 'zother',
        subtype: 'secondExtra',
        label: 'extra',
    }
    arr.push(modifiedBy);

    var modifieddate = {};
    modifieddate.title = 'modifiedDate';
    modifieddate.displayName = 'Modified Date';
    modifieddate.questionid = 'modifiedDate';
    modifieddate.dataField = 'modifiedDate';
    modifieddate.caption = 'Modified Date';
    modifieddate.entriesid = 'modifiedDate';
    modifieddate.format = 'MM/dd/yyyy HH:mm:ss';
    modifieddate.dataType = 'Date';
    modifieddate.formateId = 'MM/dd/yyyy HH:mm:ss ';
    modifieddate.extraInformation = {
        appId: appId,
        maintype: 'zother',
        subtype: 'secondExtra',
        label: 'extra',
    }
    arr.push(modifieddate);
    return arr;
}
//BI -end

module.exports = {
    configure: configure,
    auditThis: auditThis
};
