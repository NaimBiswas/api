var _ = require('lodash');
// var linkedTbl = require('../linkedTbl');

function replicateInSchema(db, collection, record, auditOptions) {
  var linkedTbl = require('../linkedTbl');
  db.collection("preSchema")
    .findOneAsync({
      _id: db.ObjectID(record._id)
    })
    .then(function (preSchemaRes) {
      if (preSchemaRes.version % 1 === 0) {
        // PQT-959 by Yamuna
        // 
        db.collection("schema")
          .findOneAsync({
            _id: db.ObjectID(preSchemaRes._id)
          })
          .then(function (schemaRes) {
            if (!schemaRes) {
              delete record.version;
              delete record.versions;
              db.collection("schema")
                .insertOneAsync(record).then(function (InsertSchemaRes) {
                  linkedTbl.insertInLinked('schema', record, db);
                  db.schema.auditThis(
                    record,
                    auditOptions.ip);
                });
            } else {
              if (preSchemaRes.version % 1 === 0 && schemaRes.version !== preSchemaRes.version) {
                record.version = preSchemaRes.version;
                record.versions = preSchemaRes.versions;
              }else{
                record.version = schemaRes.version;
                record.versions = schemaRes.versions;
              }
              db.collection("schema").updateOne({
                _id: db.ObjectID(preSchemaRes._id)
              }, {
                  $set: record
                },
                function (res) {
                  linkedTbl.insertInLinked('schema', record, db);
                  db.schema.auditThis(
                    record,
                    auditOptions.ip);
                })
            }

          })
      }
    })

}


function schemaAudit(data, db, resource, auditedRecord, newVersion, soft, cb){
  if (data.ops.length) {
    var objToUpdate = {
      'version': newVersion,
      'isMajorVersion': !soft
    }
    if(resource.collection === 'preSchema'){
      objToUpdate.isAuditing = false;
    }
    db[resource.collection].updateOne({
      _id: auditedRecord.identity
    }, {
        $push: {
          versions: {
            version: newVersion,
            record: data.ops[0]._id
          }
        },
        $set: objToUpdate
      }
      , function (e, d) {
        if (e) {
          debug(e);
        }
        else {
          cb()
        }
      });

  }
}

function audit(resource, auditedRecord, newVersion, soft, db, record, columnInfo, getVersionResponse) {

var types = {
  configural: 'configural',
  behavioral: 'behavioral'
}

  function behaviouralChanges(cb) {
    delete auditedRecord._id;
    db[resource.collection].audit.insertOne(auditedRecord, function auditCallback(err, data) {
      if(columnInfo){
        columnInfo(getVersionResponse, auditedRecord, soft, data, resource).then(function (datacallback) {
          schemaAudit(data, db, resource, auditedRecord, newVersion, soft, cb)
        });
      }else{
        schemaAudit(data, db, resource, auditedRecord, newVersion, soft, cb)
      }
    });

  }

  // User Story PQT-292 by Yamuna
  function configuralChanges(cb) {
    var recordId = (typeof record._id == 'string') ? db.ObjectID(record._id) : record._id;
      db[resource.collection].audit.updateOne({
        identity: recordId,
        version: record.version
      }, {
          $set: {
            isActive: record.isActive,
            isDraft: record.isDraft
          }
        }
        , function (e, d) {
          if (e) {
            debug(e);
          }
          else {
            // PQT-965 by Yamuna
            if(resource.collection == 'preSchema'){
              db[resource.collection].updateOne({
                _id: recordId
              }, {
                  $set: {
                    isAuditing:false
                  }
                }
                , function (e, d) {
                  if (e) {
                    debug(e);
                    cb();
                  }
                  else {
                    cb();
                  }
                });
            }else{
              cb();
            }

          }
        });
      }


  function getChangeType(changes) {
    // PQT-4295: mahammad
    if (resource.collection == 'schema' || resource.collection == 'preSchema') {
      // PQT-964 by Yamuna
      var chagedArray = (changes||[]).filter(function (chan) {
        return ['createdByName', 'modifiedByName', 'createdDate', 'modifiedDate', 'createdBy', 'modifiedBy'].indexOf(chan.path) == -1;
      });
      if(chagedArray.length == 1 && (chagedArray[0].path == "isActive" || chagedArray[0].path == "isDraft" || chagedArray[0].path == "restoreStableVersion")){
        // PQT-1349 by Yamuna
        if(chagedArray[0].path == "isDraft"){
          return types.behavioral;
        }else{
          return types.configural;
        }
      }else if(chagedArray.length == 2 && 
        ((_.find(chagedArray, {path:"isActive"}) && (_.find(chagedArray, {path:"isDraft"}) || _.find(chagedArray, {path:"modifiedDate"}))) || 
        (_.find(chagedArray, {path:"isDraft"}) && _.find(chagedArray, {path:"restoreStableVersion"})))){
        return types.configural;
      }else if(chagedArray.length == 3 && 
        ((_.find(chagedArray, {path:"isActive"}) && _.find(chagedArray, {path:"isDraft"})) && _.find(chagedArray, {path:"restoreStableVersion"}))
      ){
        return types.configural;
      }else{
        return types.behavioral;
      }
    } else {
      return types.behavioral;
    }
  }
  
  return {
    behaviouralChanges: behaviouralChanges,
    configuralChanges: configuralChanges,
    getChangeType: getChangeType,
    types: types
  }
}

module.exports = {
  replicateInSchema: replicateInSchema,
  audit: audit
}