'use strict'
/**
* @name db
* @author Dave Amit <dave.a@productivet.com>
*
* @version 0.0.0
*/

var db = require('./db');

/**
* Set of all entities that will be used throughout the system, with following
* format:
* {
*   collection: <Name Of The Collection>,
*   audit: <true/false> indicates where to enable audit for this entity if this
*           is set true, auditig will be done for Create / Update when "Resource"
*           is used. Auto routes generated using routeBuilder also supports same.
*           POST => Create, PUT (Draft) => Update (Soft), PUT => Update. Soft
*           updates will cause minor-version to increment otherwise, major-version
*           will increment.,
*    index : <Pass index in exactly same format as expected by mongodb>
*           REF: https://docs.mongodb.org/manual/reference/method/db.collection.createIndex/
* }
*
*
*/
var entities = [
  {
    collection:'batchLockCache',
  },
  {
    collection: 'webhookLogs',
  },{
    collection:'autoScheduleCorrectData',
  },{
    collection:'autoBatchCorrectData',
  },{
    collection: 'importfile',
  },
  //akashdeep.s - QC3-2093
  {
    collection: 'allAuthToken',
  },
    //Start:- QC3-6731 --by bhavin for add new database collection
  {
    collection: 'searchFavorites',
    audit: true,
    index: {
      title: 'text'
    }
  },
  //End:- QC3-6731 --by bhavin for add new database collection
  {
    collection : 'appAccessConfig',
    audit: true,
    index: {
      token: 'text'
    }
  },
  //End:- QC3-6731 --by bhavin for add new database collection
  {
    collection : 'appEntityConfigEntity',
    audit: true,
    index: {
      title: 'text'
    }
  },
  {
    collection: 'forgotPasswordRecord',
  },
   //Start:- QC3-6731 --by bhavin for add new database collection
  {
    collection: 'searchFavorites',
    audit: true,
    index: {
      title: 'text'
    }
  },
  //End:- QC3-6731 --by bhavin for add new database collection
  {
    collection: 'test',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'joins',
    audit: true,
    index: {
      viewName: 'text'
    }
  },{
    collection: 'users',
    audit: true,
    index: {
      email: 'text',
      newEmail: 'text',
      firstname: 'text',
      lastname: 'text',
      username: 'text'
    }
  },{
    collection: 'userResetAuditLogs',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'loginattempts',
    audit: false,
    index: {
      email: 'text',
      name: 'text'
    }
  },{
    collection: 'modules',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'tags',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'sites',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'siteNames',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'sets',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'calendar',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'calendarThresholds',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'questions',
    audit: true,
    index: {
      title: 'text',
      tags: 'text',
      'type.title': 'text'
    }
  },{
    collection: 'procedures',
    audit: true,
    index: {
      title: 'text',
      tags: 'text'
    }
  }, {
    collection: 'entities',
    audit: true,
    index: {
      title: 'text',
      tags: 'text'
    }
  },{
    collection: 'entityRecords',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'schema',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'assignment',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'schedules',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'thresholds',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'thresholdsNotification',
    index: {
      title: 'text'
    }
  },{
    collection: 'roles',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'types',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'attributes',
    audit: true,
    index: {
      title: 'text',
      'type.title': 'text'
    }
  },{
    collection: 'attributeAutoGenerateNumber',
    audit: true
  },{
    collection: 'validators',
    audit: true,
    index: {
      title: 'text',
      formatId: 1
    }
  },{
    collection: 'securityQuestions',
    index: {
      title: 'text'
    }
  },{
    collection: 'resources',
    index: {
      title: 'text'
    }
  },{
    collection: 'settings',
    index: {
      title: 'text'
    }
  },{
    collection: 'automated',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'calendarAutomated',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'manualimport',
    audit: true,
    index: {
      'schema.mappingName': 'text'
    }
  },{
    collection: 'temp',
    index: {
      title: 'text'
    }
  },{
    collection: 'status',
    index: {
      title: 'text'
    }
  },{
    collection: 'entries',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'masterQCEntries',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'csventries',
    index: {
      title: 'text'
    }
  },{
    collection: 'automatedEntries',
    index: {
      title: 'text'
    }
  },{
    collection: 'masterqcsettings',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'groupsettings',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'schemaSyncDetails',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'qcentryactivity',
    // Start: QC3-8865 - Jyotil - changing audit false due to no any need to save audit of {qcentryactivity}
    audit: false,
    //End: QC3-8865 - Jyotil
    index: {
      title: 'text'
    }
  },{
    collection: 'generalSettings',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'reportConfiguration',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'reportPermissionConfiguration',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'drilldown',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'controlchart',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'apiAccessConfig',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'customReportColumnDetail',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'customReport',
    audit: false,
    index: {
      title: 'text'
    },
  },{
    collection: 'queueFiles',
    audit: false,
    index: {
      title: 'text'
    }
  },{
    collection: 'ReportSchemaColumns',
    audit: false,
    index: {
      name: 'text'
    }
  },{
    collection: 'batchentrysettings',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'batchEntries',
    audit: true,
    index: {
      title: 'text'
    }
  },
  {
    collection: 'reportTemplates',
    audit: true,
    index: {
      name: 'text'
    }
  },{
    collection: 'controlChartParameters',
    audit: false,
    index: {
      name: 'text'
    }
  },{
    collection: 'versions'
  },{
    collection: 'reviewpendingworkflow'
  },{
    collection:'linkedTbl'
  },{
    collection: 'documentsettings',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'dmsperfeqtamapping',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection:'mappedLinkToEntity'
  },{
    collection: 'plugin',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'autoSync',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'pluginlist'
  },{
    collection: 'autoSyncHistory'
  },{
    collection: 'pluginTempRecords'
  },{
    collection: 'pluginConfig',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'windows',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'windowStatus',
    index: {
      title: 'text'
    }
  },{
    collection: 'calendarWindows',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'calendarWindowsNotified'
  },
  {
    collection: 'windowSamplingPlan'
  },{
    collection: 'windowQueueFiles',
    audit: false,
    index: {
      title: 'text'
    }
  },{
    collection: 'windowReport',
    audit: true,
    index: {
      title: 'text'
    }
  },{
    collection: 'preSchema',
    audit: true,
    index: {
      title: 'text'
    },
  },{
    collection: 'licenseAgreement',
    audit: false,
    index: {
      title: 'text'
    },
  },
  {
    collection: 'calendarNotification',
    audit: false,
    index: {
      title: 'text'
    }
  },{
    collection: 'autoBatchDataCorrectionNotification',
    audit: false,
    index: {
      title: 'text'
    }
  },{
    collection: 'mailLogs',
    audit: false,
    index: {
      title: 'text'
    },
  }

];

/**
*  I have used apply because I wanna pass more then one entity as argument and
*  not as array, using "apply" will fill "arguments" ("Array Like Object").
*/
module.exports = db.configure.apply(this, entities);
