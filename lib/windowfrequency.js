/**
* @name Qulification Performance - Window Frequency Mail Service
* @author Yamuna Sardhara <yamuna.s@productivet.com>
*
* @version 1.0
**/
//var Promise = require('promise');
var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('./logger').configure(__dirname, enableBunyan);
var config = require('../config/config.js');
var CronJob = require('cron').CronJob;
var mailer = require('./mailer');
var _ = require('lodash');
var mapUsers = require('./registration').mapUserIdWithUser;
var db = require('./db');
var handlebars = require('handlebars');
var debug = require('./logger').debug('lib:windowfrequency');
var currentFileName = __filename;
var logger = require('../logger');
var moment = require('moment');

var working = 0;
var machineDate = new Date();
var machineoffset = machineDate.getTimezoneOffset();
var clientOffset = config.timeZones[config.currentTimeZone] || -330;
var clientTZ = config.timeZonesP[config.currentTimeZoneP] || 'America/Chicago';
var mcdiff = 0;
var searchRecords = require('../routes/qulificationperformance/searchRecords.js');
var formattedModelForEmail = require('../routes/qulificationperformance/formattedModelForEmail.js');

var runHourlyJob = false;
var runDailyJob = false;

var job = new CronJob('1 0 * * *', function() {
  runDailyJob = true;
  working = 0;
  initAlerts();

}, function () {
  runDailyJob = false;
},
true
);

var hourlyJob = new CronJob('59 * * * *', function() {
  runHourlyJob = true;
  working = 0;
  initAlerts();

}, function () {
  runHourlyJob = false;
  /* This function is executed when the job stops */
},
true  /* , Start the job right now */
);

handlebars.registerHelper('ifCondition', function (v1, operator, v2, options) {
    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
          }
});

//For getting today's window frequency
function initAlerts(){
  console.log("hello");
  var machineDate = new Date();
  var machineoffset = machineDate.getTimezoneOffset();
  var clientOffset = config.timeZones[config.currentTimeZone] || -330;
  mcdiff = 0;
  mcdiff = -machineoffset + parseInt(clientOffset);

  var today = new Date();
  var start = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
  start.setDate(start.getDate());
  start = new Date(start.getTime() + (mcdiff * 60 * 1000));
  var end = new Date(start.getTime() + (24 * 60 * 60 * 1000));

  db
  .collection('calendarWindows')
  .find({
    'notified': false,
    'date': {
      $gte: new Date(start.toISOString()),
      $lte: new Date(end.toISOString())
    }
  })
  .toArray(function (err, entries) {
    if(!entries || !entries.length ){
      logger.log("collection:calendar error" , 'error', currentFileName);
      debug('No scheduled entries');
      return;
    }else {
      debug(entries.length + " calendarWindows entries");
      manipulateWindowFrequency(entries);
    }
  });
}

//For getting Window data of window frequency
function manipulateWindowFrequency(calendarWindows){
  var windowId;
  mapUsers(calendarWindows, function (e, calendarWindows) {
    calendarWindows.forEach(function(calendarWindow){
      if(calendarWindow.type == 'Hourly' && runHourlyJob == true){
            var startTime = new Date();
            var endTime = new Date(startTime.getTime() + (60*60*1000));
            if (moment.utc((calendarWindow.date).toISOString()).isSameOrAfter(moment.utc((startTime).toISOString())) && moment.utc((calendarWindow.date).toISOString()).isBefore(moment.utc((endTime).toISOString()))) {
            windowId = db.ObjectID(calendarWindow.windowId.toString());
            // QC3-11041 by Yamuna
            db.collection('windows').findOne({'_id': windowId,'windowStatus.title':"Current"},function(err,window){
              if (err) {
                logger.log(`error fetching entities for windows : ${calendarWindow.title}`);
              } else {
                if(window){
                  // for getting counts of First Stage Information (Current) Section
                  var params = {id:window._id, windowId:window.windowId};
                  searchRecords.fetchAppRecords(params,function (err, response) {
                    if(err){
                      logger.log("call:getAppRecordCount error" + err , 'error', currentFileName);
                    }
                    if(response == "No Records Found."){
                      window.countDetails = '0';
                    }else if(_.size(response)){
                      window.countDetails = response;
                    }
                    formattedModelForEmail.getFormattedModel(window).then(function(modelForEmail){
                      modelForEmail.templateType = "windowFrequency";
                      notify(modelForEmail,function(err,result){
                        if(err){
                          logger.log("call:notify error : " + err , 'error', currentFileName);
                          return;
                        }
                      console.log("notify successfully hourly");
                      logger.log("call:notify res" , 'info', currentFileName);
                      //START
                      db.calendarWindows.findAndModify({_id: db.ObjectID(calendarWindow._id)}, {},{ $set: {'notified':true}},
                      {new: true}, function(err, windowData) {
                        if(err) {
                          logger.log("error ::"+err)
                        }
                        if(windowData){
                          console.log("calendarWindows updated successfully");
                        }
                      });
                    });
                      //END
                    });
                  });
                }
              }
            });
          }
        }else if(calendarWindow.type != 'Hourly' && (runDailyJob == true)){
              windowId = db.ObjectID(calendarWindow.windowId.toString());
              db.collection('windows').findOne({'_id': windowId,'windowStatus.title':"Current"},function(err,window){
                if (err) {
                  logger.log(`error fetching entities for windows : ${calendarWindow.title}`);
                } else {
                  if(window){
                    // for getting counts of First Stage Information (Current) Section
                    var params = {id:window._id, windowId:window.windowId};
                    searchRecords.fetchAppRecords(params,function (err, response) {
                      if(err){
                        logger.log("call:getAppRecordCount error" + err , 'error', currentFileName);
                      }
                      if(response == "No Records Found."){
                        window.countDetails = '0';
                      }else if(_.size(response)){
                        window.countDetails = response;
                      }
                      formattedModelForEmail.getFormattedModel(window).then(function(modelForEmail){
                        modelForEmail.templateType = "windowFrequency";
                        notify(modelForEmail,function(err,result){
                          if(err){
                            logger.log("call:notify error : " + err , 'error', currentFileName);
                            return;
                          }
                        console.log("notify successfully weekly or monthly");
                        logger.log("call:notify res" , 'info', currentFileName);
                        //START
                        db.calendarWindows.findAndModify({_id: db.ObjectID(calendarWindow._id)}, {},{ $set: {'notified':true}},
                        {new: true}, function(err, windowData) {
                          if(err) {
                            logger.log("error ::"+err)
                          }
                          if(windowData){
                            console.log("calendarWindows updated successfully");
                          }
                        });
                      });
                        //END
                      });
                    });
                  }
                }
              });
          }
    });
  });
}

//For Sent Email
function notify(entity,cb){
  mailer.send('qp-notify-common', entity, entity.emailAddresses, function sendMailCallback(err, value) {
    if (err) {
      debug.err('Failed sending mail - window-frequency-notify');
      debug.err(err);
      cb(err, entity);
    } else {
      logger.log('Email has been sent to ' + entity.emailAddresses);
      debug('Mail send successfully to: ' + entity.emailAddresses);
      cb(null, entity);
    }
  });
}
