/**
* @author Jyotil Raval <jyotil.r@productivet.com>
* @description Encrypt OR Dcrypt given object.
* @param {Object} file
* @return {Object} encryptedFile or dcryptedFile will be return
* @version 1.0
*/

(function () {
  'use strict'

  var debug = require('../logger').debug('lib:oauth');
  var config = require('../../config/config');
  var crypto = require('crypto');
  var algorithm = config.cryptoProtocols.encryptDecryptOptions.algorithm,
  secret = config.cryptoProtocols.encryptDecryptOptions.secret;



  var currentFileName = __filename;
  var logger = require('../../logger');


  debug('Encryption and Decription enabled for algorithm/secret');
  debug(algorithm);
  debug(secret);

  function encrypt(text) {
    logger.log("function: encrypt - start" , 'info', currentFileName);

    var cipher = crypto.createCipher(algorithm, secret);
    var crypted = cipher.update(text,'utf8','hex');
    crypted += cipher.final('hex');

    logger.log("function: encrypt - end" , 'info', currentFileName);
    return crypted;
  }

  function encryptWithOutSecret(text) {
    logger.log("function: encryptWithOutSecret - start" , 'info', currentFileName);
    var cipher = crypto.createCipher(algorithm,'');
    var crypted = cipher.update(text,'utf8','hex');
    crypted += cipher.final('hex');
    logger.log("function: encryptWithOutSecret - end" , 'info', currentFileName);
    return crypted;
  }

  function decryptWithOutSecret(encrypted) {
    logger.log("function: decryptWithOutSecret - start" , 'info', currentFileName);
    var decipher = crypto.createDecipher(algorithm, '');
    var dec = decipher.update(encrypted,'hex','utf8');
    dec += decipher.final('utf8');
    logger.log("function: decryptWithOutSecret - end" , 'info', currentFileName);
    return dec;
  }

  function decrypt(encrypted) {
    logger.log("function: decrypt - start" , 'info', currentFileName);
    var decipher = crypto.createDecipher(algorithm, secret);
    var dec = decipher.update(encrypted,'hex','utf8');
    dec += decipher.final('utf8');
    logger.log("function: decrypt - end" , 'info', currentFileName);
    return dec;
  }

  module.exports = {
    encrypt: encrypt,
    decrypt: decrypt,
    encryptWithOutSecret: encryptWithOutSecret,
    decryptWithOutSecret: decryptWithOutSecret
  };
})()
