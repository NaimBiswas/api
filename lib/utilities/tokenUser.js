(function () {
    'use strict';
    var q = require('q');
    var db = require("../../lib/db");
    var controller = function () { };

    function getTokenUser(token) {
        var deferred = q.defer()
        db['users'].findOne({
          _id:  db.ObjectID(token.userId || token.user._id)
        }, function (e, user) {
            if (e || !user) {
               deferred.reject(e);
            } else {
                deferred.resolve(user);
            }
        });
        return deferred.promise;
    }

    controller.prototype.getTokenUser = getTokenUser;
    module.exports = new controller();
})()