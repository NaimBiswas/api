var config = require('./../../config/config')
var moment = require('moment');

function updateToken(current) {
	var newDateObj = new Date();
	if(current){
		return newDateObj;
	}else{
		newDateObj.setTime(new Date().getTime() + (config.tokenTimeOut  * 60000));
		return new Date(newDateObj)
	}
}

function checkUserIsAlive(expirationTime,path) {
	var currentDate = new Date().getTime();
	// var alive = false;
	// Object.keys(expirationTime || {}).forEach(time => {
	// 	if (expirationTime[time].getTime() > currentDate) {
	// 		alive = true
	// 	}
	// });
	if(path && expirationTime[path]){
		return	expirationTime[path].getTime() > currentDate;
	}else{
		return false;
	}
  // return alive;
}

module.exports = {
  updateToken: updateToken,
  checkUserIsAlive:checkUserIsAlive
}
