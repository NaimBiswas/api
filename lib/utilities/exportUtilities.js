/**
 * @auther: Mahammad <mahammad.c@productivet.com>
 * @discription: For all export related common function
 */

var exportUtilities = {
    setPdfWidth
}

//QC3-9039:- set width as per column length
function setPdfWidth(dataArray) {

    if ((dataArray || []).length) {
        var columns = dataArray.length;

        if (columns <= 10) {
            return "15in";
        } else if (columns <= 20) {
            return "22in";
        } else if (columns <= 30) {
            return "25in";
        } else if (columns <= 40) {
            return "29in";
        } else if (columns <= 50) {
            return "32in";
        } else if (columns <= 60) {
            return "35in";
        } else if (columns <= 70) {
            return "40in";
        } else {
            return "45in";
        }
    }

    return "20in";
}

module.exports = exportUtilities;



