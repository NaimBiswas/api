

var db;
var _ = require('lodash');
var Promise = require("bluebird");

module.exports = {
  logEntrySave_AfterSign : logEntrySave_AfterSign,
  getFileName: getFileName,
  manageLiteLogsHistory : Promise.promisify(manageLiteLogsHistory)
}

function getFileName(files, value) {
  value = value || '';
  if (_.isArray(files)) {
    var fileNames = files.map(function (fle) {
      var file = fle;
      if (_.isArray(fle)) {
        file= fle[0];
      }
      return (file.name || file.title || '') + ((file.type||file.uploadedBy)?' -'+ (file.type||file.uploadedBy) : '')
    }).join(' | ');
    value = fileNames || '';
  } else if (_.isObject(files) && (files.name || files.title)) {
    value = (files.name || files.title || '') + ((files.type||files.uploadedBy)?' -'+ (file.files||files.uploadedBy) : '');
  }
  return value;
}

function logEntrySave_AfterSign(entryData,schemaId,auditLogList,_db){
  if(isSignedOnce(entryData)){
    db = _db;
    getGeneralSetting(function(data){
      var removeSequenceNumber = false;
      if(data && data.length){
        removeSequenceNumber = !data[0].hideProcTitle;
      }
      if(entryData.appId){
        getSchema(schemaId,function(schema){
          getSiteNames(function(sitesData){
          if(schema && !schema.isLockReviewWorkflow){
            var schemaCache = makeCache(schema, removeSequenceNumber);
            _.forEach(auditLogList,function(logItem){
              getItemWithTitle(logItem,schemaCache,sitesData);
            });
      
            var userVisibleTitle = getUserVisibleQuestions(auditLogList);
            if(userVisibleTitle.length){
              getModifiedByInfo(entryData.modifiedBy,function(modifiedByInfo){
      
                var liteLogs = entryData.liteLogs || [];
                liteLogs.push({
                  updatedBy: modifiedByInfo,
                  modifiedDate : entryData.modifiedDate,
                  updatedFields : userVisibleTitle
                });
      
                db[schemaId].update({_id:entryData._id},{$set : {
                  liteLogs : liteLogs
                }})
              })
            }
          }
        });
        })  
      }
    });
  }
}


function manageLiteLogsHistory(db,req,cb){
    db.schema.findOne({_id:db.ObjectID(req.body.appId)},{isLockReviewWorkflow:1},function(err,schemaData){
        if(schemaData && schemaData.isLockReviewWorkflow  && req.body.liteLogs && req.body.liteLogs.length){
            var liteLogsTemp = _.cloneDeep(req.body.liteLogs);
            db.collection('entryLiteLogHistory').findOne({entryId : db.ObjectID(req.params._id),schemaId : db.ObjectID(req.body.appId)},function(err,data){
              if(data){
                var liteLogHistory = {
                  workflowHistoryIndex : getLastWorkflowReviewHistoryIndex(req.body.workflowReviewHistory),
                  liteLogs : liteLogsTemp
                }
                data.liteLogHistory.push(liteLogHistory);
                db.collection('entryLiteLogHistory').update({entryId : db.ObjectID(req.params._id),schemaId : db.ObjectID(req.body.appId)},{$set : data});
              }else{
                var logsHistory = {
                  entryId : db.ObjectID(req.params._id),
                  schemaId : db.ObjectID(req.body.appId),
                  liteLogHistory : [{
                    workflowHistoryIndex : getLastWorkflowReviewHistoryIndex(req.body.workflowReviewHistory),
                    liteLogs : liteLogsTemp
                  }]
                }
                db.collection('entryLiteLogHistory').insert(logsHistory); 
              }
            })
            
            req.body.liteLogs = [];
            req.items.liteLogs = [];
        }
        cb();
    });
}



// ---Support Functions

function isSignedOnce(entryData){
  if(entryData.workflowreview &&
      entryData.workflowreview[0] && 
      entryData.workflowreview[0].reviews){
        return _.some(entryData.workflowreview[0].reviews,{status:0})
  }

  return false;
}



function getSchema(schemaId,cb){
  db['schema'].findOne({_id: db.ObjectID(schemaId)}, function(e,schema) {
    cb(schema)
  })
}

function getSiteNames(cb) {
  db.collection('siteNames').find({ isActive: true }).toArray(function (err, sitesdata) {
    cb(sitesdata);
  });
}


function makeCache(schema, removeSequenceNumber){
  
    var cache = [];
    var attributes = [];
    // Code for get all question in all procedure including conditional procedure
    cache = _.reduce(schema.procedures, function (a, c) {
      if (c.conditionalWorkflows) {
        _.forEach(_.map(c.conditionalWorkflows, function (w) {
          return w.procedureToExpand;
        }), function (aa) {
          if (!_.isUndefined(aa)) {
            a[aa['s#']] = _.map(aa.questions, function (q) {
              q.title = removeSequence(q.title, removeSequenceNumber);
              return q;
            });
          }
        });
      }
      a[c['s#']] = _.map(c.questions, function (q) {
        q.title = removeSequence(q.title, removeSequenceNumber);
        return q;
      })

      return a;
    }, cache);

    // Start code for Add question of entity into catch
    cache = _.reduce(schema.entities, function (a, c) {
      if (c.conditionalWorkflows) {
        _.forEach(_.map(c.conditionalWorkflows, function (w) {
          return w.procedureToExpand;
        }), function (aa) {
          a[aa['s#']] = _.map(aa.questions, function (q) {
            q.title = removeSequence(q.title, removeSequenceNumber);
            return q;
          });
        });
      }
      a[c['s#']] = _.map(c.questions, function (q) {
        q.title = removeSequence(q.title, removeSequenceNumber);
        return q;
      });
      return a;
    }, cache);




      // Code for add question which are in attributes object in schema
      _.forEach(schema.attributes, function (q) {
        attributes.push(q);
      });

      var entitiesSeqId = [];
      _.forEach(schema.entities, function (entity) {
        entitiesSeqId.push(entity['s#']);
      });

      var procedures = [];
      _.forEach(schema.procedures, function(p){
        p.title = removeSequence(p.title, removeSequenceNumber);
        procedures.push(p);
      })


      return {
        cache : cache,
        attributes : attributes,
        entitiesSeqId : entitiesSeqId,
        procedures : procedures
      }
}

function getItemWithTitle(item,schemaCache,sitesData){

  if ((item.title || '').startsWith("Entries >")) {
    var seqId = (item.title.split(">")[1]).trim();
    
    if (seqId.indexOf('-') > 0) {
      // Case for finding title from procedure's question
      seqId = seqId.split('-');
      _.forEach(schemaCache.cache[seqId[1]], function (question) {
        if (question['s#'] === seqId[0]) {
          if (schemaCache.entitiesSeqId.length && _.includes(schemaCache.entitiesSeqId, question.parentId)) {
            if (!question.isAppearance) {
              item.title = question.title + ' (Hidden Attribute)';
              item.isUserVisible = true;
            } else {
              item.title = question.title;
              item.isUserVisible = true;
            }
          } else {
            item.title = question.title;
            item.isUserVisible = true;
          }
          return false;
        }
      });
    } else {
      // Case for finding title in attributes questions
      _.forEach(schemaCache.attributes, function (question) {
        if (question['s#'] === seqId) {
          item.title = question.title;
          item.isUserVisible = true;
          return false;
        }
      });
    }

    // PQT-211 by Yamuna
    if ((item.title || '').startsWith("Entries > settings")) {
      if(item.old == true && item.new == false){
        var procSeqId = item.title.substring(item.title.lastIndexOf('>')+2);
        var procQuestions = _.find(schemaCache.procedures, {'s#': procSeqId}).questions || [];
        var questionTitle = [];
        procQuestions.forEach(function(question){
          questionTitle.push(question.title);
        });
        item.title = questionTitle.join(', ');
        item.isUserVisible = true;
      }
    }
  }

  // PQT-198 by Yamuna
  if ((item.title || '').startsWith("Site Levels >")) {
      if(sitesData && item.title){
        var indexOfLevelId = parseInt(item.title.substring(item.title.indexOf('#')+1, item.title.indexOf('#')+2));
        item.title = item.title.substr(0,item.title.indexOf('>')+2).concat(sitesData[indexOfLevelId-1].title);
        item.isUserVisible = true;
      }
  }
  // QC3-9460 by Yamuna
  if (((item.title || '').startsWith("AttributeValue >") || (item.title || '').startsWith("attrValue >"))) {
    var seqId = item.title.substring(item.title.lastIndexOf('>') + 2);
    seqId = seqId.split('-');
    _.forEach(attributes, function (question) {
      if (question._id == seqId) {
        item.title = ((item.title).substring(0, (item.title).lastIndexOf('>') + 1)) + ' ' + question.title;
        item.isUserVisible = true;
        return false;
      }
    });
  }

}



function removeSequence(strTitle, removeSequenceNumber) {
  if (removeSequenceNumber) {
    var str = strTitle;
    var lastIndex = str.lastIndexOf(" ");

    strTitle = str.substring(0, lastIndex);
  }
  return strTitle;
}


function getUserVisibleQuestions(auditLogList){
  return _.reduce(auditLogList,function(res,val){
    if(val.isUserVisible && res.indexOf(val.title) == -1){
      res.push(val.title)
    }
    return res;
  },[])
}

function getModifiedByInfo(modifiedByUserId,cb){
  
  modifiedByUserId = getObjectId(modifiedByUserId);

  db['users'].findOne({_id: modifiedByUserId}, function(err,modifiedByUserInfo) {
    if(err){
      cb(null)
      return;
    }
    cb({
      userId : modifiedByUserInfo._id.toString(),
      userObjectId : modifiedByUserInfo._id,
      username : modifiedByUserInfo.username,
      firstname : modifiedByUserInfo.firstname,
      lastname : modifiedByUserInfo.lastname,
      email : modifiedByUserInfo.email,
      fullName : getFullName(modifiedByUserInfo)
    })
  })
}

function getFullName(userInfo){
  return userInfo.firstname + " " + userInfo.lastname + " ("+userInfo.username+")";
}

function isObjectId(id){
  return !(typeof id == "string")
}

function getObjectId(id){
  return isObjectId(id) ? db.ObjectID(id) : id;
}

function getLastWorkflowReviewHistoryIndex(workflowReviewHistory){
    return workflowReviewHistory && workflowReviewHistory.length ? workflowReviewHistory.length - 1 : -1;
}

function getGeneralSetting(cb){
  db['generalSettings'].find().toArray().then(function(data){
    cb(data)
  });
}
