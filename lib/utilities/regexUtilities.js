/**
 * @auther: Vivek <vivek.t@productivet.com>
 * @discription: Regex Operation
 */

var regexUtilities = {
    removeWithIdentifier
}

/* 
    * Description : Replace text with given identitifier and position.
*/
function removeWithIdentifier(text,identifier,position) {
    identifier = identifier.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')
    var regex;
    if(position == 'startWith'){
        regex = new RegExp("^("+identifier+")");
    }else if(position == 'endWith'){
        regex = new RegExp("("+identifier+")$");
    }
    return text.replace(regex,'');;
}

module.exports = regexUtilities;



