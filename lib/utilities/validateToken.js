(function () {
    'use strict';
    var q = require('q');
    var db = require("../../lib/db");
    var controller = function () { };

    function validateToken(xToken) {
        var deferred = q.defer();
        db['allAuthToken'].findOne({
            token: xToken
        }, function (e, token) {
            if (e || !token) {
                deferred.reject(e);
            } else {
                deferred.resolve(token);
            }
        });
        return deferred.promise;
    }

    controller.prototype.validateToken = validateToken;
    module.exports = new controller();
})()