var excel = require('node-excel-export');
var fs = require('fs-extra');
var config = require('../../config/config');
var path = require('path');

module.exports = function (fileName, data, cb) {
    // Create the excel report.
    // This function will return Buffer
    var report = excel.buildExport(data);
    // [ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
    //     {
    //         name: 'Report', // <- Specify sheet name (optional)
    //         heading: heading, // <- Raw heading array (optional)
    //         merges: merges, // <- Merge cell ranges
    //         specification: specification, // <- Report specification
    //         data: dataset // <-- Report data
    //     }
    // ]

    try { 
        fs.writeFile(config.staticpath + fileName + '.xlsx', report, function (err, xlsx) {
            if (err) throw err;
            cb(null, {
                filePath: path.join(config.staticpath, fileName + '.xlsx'),
                fileName: fileName + '.xlsx'
            });
        }); 
    } catch (error) {
        log.log(`Error on writing file ${fileName}`); 
        log.log(error);
        cb(error, null);
    }

}