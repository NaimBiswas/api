var _ = require('lodash');
var db = require('./db');

function insertInLinked(linkedToType, data) {

  if(!db.collection){
    db = require('./db');
  }
  var linkedArray = [];
  var objLinked;
  switch (linkedToType) {
    case 'attributes':
      if (data && data.validation && data.validation.validationSet) {
        objLinked = {
          linkedOfId: data.validation.validationSet.existingSet._id,
          linkedOfName: data.validation.validationSet.existingSet.title,
          linkedOfType: 'sets',
          linkedToId: data._id.toString(),
          linkedToType: 'attributes',
          linkedToName: data.title
        };
        linkedArray.push(objLinked);
      }
      break;

    case 'questions':
      if (data.validation.validationSet && data.validation.validationSet.existingSet) {
        objLinked = {
          linkedOfId: data.validation.validationSet.existingSet._id,
          linkedOfName: data.validation.validationSet.existingSet.title,
          linkedOfType: 'sets',
          linkedToId: data._id.toString(),
          linkedToType: 'questions',
          linkedToName: data.title,
          linkedToStatus: data.isActive
        };
        linkedArray.push(objLinked);
      }
      break;

    case 'procedures':
      (data.questions || []).forEach(function (result) {
        objLinked = {
          linkedOfId: result._id,
          linkedOfName: result.title,
          linkedOfVersion: result.version,
          linkedOfType: 'questions',
          linkedToId: data._id.toString(),
          linkedToType: 'procedures',
          linkedToName: data.title,
          linkedToStatus: data.isActive
        };
        linkedArray.push(objLinked);
      });
      break;

    case 'entities':
      (data.questions || []).forEach(function (que) {
        if (que.validation && que.validation.validationSet) {
          objLinked = {
            linkedOfId: que.validation.validationSet.existingSet._id,
            linkedOfName: que.validation.validationSet.existingSet.title,
            linkedOfType: 'sets',
            linkedToId: data._id.toString(),
            linkedToType: 'entities',
            linkedToName: data.title
          };
          linkedArray.push(objLinked);
        }
      });
      break;

    case 'schema':
      (data.attributes || []).forEach(function (result) {
        objLinked = {
          linkedOfId: result._id.toString(),
          linkedOfName: result.title,
          linkedOfVersion: result.version,
          linkedOfType: 'attributes',
          linkedToId: data._id.toString(),
          linkedToType: 'schema',
          linkedToName: data.title,
          linkedToStatus: data.isActive
        };
        linkedArray.push(objLinked);
      });

      (data.entities || []).forEach(function (result) {
        objLinked = {
          linkedOfId: result._id.toString(),
          linkedOfName: result.title,
          linkedOfVersion: result.version,
          linkedOfType: 'entities',
          linkedToId: data._id.toString(),
          linkedToType: 'schema',
          linkedToName: data.title,
          linkedToStatus: data.isActive
        };
        linkedArray.push(objLinked);
        (result.conditionalWorkflows || []).forEach(function (conWorkflows) {
          if (conWorkflows.procedureToExpand) {
            objLinked = {
              linkedOfId: conWorkflows.procedureToExpand._id.toString(),
              linkedOfName: conWorkflows.procedureToExpand.title,
              linkedOfVersion: conWorkflows.procedureToExpand.version,
              linkedOfType: 'procedures', //?
              linkedToId: data._id.toString(),
              linkedToType: 'schema',
              linkedToName: data.title,
              linkedToStatus: data.isActive
            };
            linkedArray.push(objLinked);
          }
        });
      });

      (data.procedures || []).forEach(function (result) {
        objLinked = {
          linkedOfId: result._id.toString(),
          linkedOfName: result.title,
          linkedOfVersion: result.version,
          linkedOfType: 'procedures',
          linkedToId: data._id.toString(),
          linkedToType: 'schema',
          linkedToName: data.title,
          linkedToStatus: data.isActive
        };
        linkedArray.push(objLinked);
      });
      break;

    case 'masterqcsettings':
      (data.schema || []).forEach(function (result) {
        objLinked = {
          linkedOfId: result._id,
          linkedOfName: result.title,
          linkedOfType: 'schema',
          linkedToId: data._id.toString(),
          linkedToType: 'masterqcsettings',
          linkedToName: data.title,
          linkedToStatus: data.isActive
        };
        linkedArray.push(objLinked);
      });
      break;

    default:
      break;
  }

  if (data._id.toString()) {
    db.collection("linkedTbl").removeMany({
      linkedToId: data._id.toString()
    }, function (data) {
      //PQT-4311 if condition hiren
      if(linkedArray.length){
        db.collection("linkedTbl").insertMany(_.uniqBy(linkedArray, 'linkedOfId'), function (e, data) { //?
          if (e) {
            console.log(e);
          }
        });
      }
    });
  }
}


module.exports = {
  insertInLinked: insertInLinked
};