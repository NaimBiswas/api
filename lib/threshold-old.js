//by lokesh.b@productivet.com
//changes for QC3-7954

/* some qiuck info... by surjeet.b@productivet.com

1. To test scheduler set crone job
var job = new CronJob('* * * * * *', function() {
Runs every second
}

2. To run every day 12 AM !!
var job = new CronJob('0 0 0 * * *', function() {
Runs every day
at 12:00:00 AM.
}
*/
var Promise = require('bluebird');

var colors = require('colors');
var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('./logger').configure(__dirname, enableBunyan);
require('./promise-me');
var config = require('../config/config.js');
var CronJob = require('cron').CronJob;
var mailer = require('./mailer');
var settings = require('./settings');
var log = require('../logger');
var _ = require('lodash');
var mapUsers = require('./registration').mapUserIdWithUser;
var db = Promise.promisifyAll(require('./db')); //require('./db');
var debug = require('./logger').debug('lib:scheduler');
var handlebars = require('handlebars');
var fs = require('fs');
var pdf = require('html-pdf');
var moment = require('moment-timezone');
var exportUtilty = require('./utilities/exportUtilities.js');
var generateExcel = require('./utilities/generateExcel.js');
var entriesUtility = require('./utilities/entriesUtiliese.js');

var field = {};
var mailObj = [];
var tempFileName;
var working = 0;
var machineDate = new Date();
var machineoffset = machineDate.getTimezoneOffset();
var clientOffset = config.timeZones[config.currentTimeZone] || -330;
//QC3-8465 by surjeet.. clientTZ is used to solve timezone issue
var clientTZ = config.timeZonesP[config.currentTimeZoneP] || 'America/Chicago';
var mcdiff = 0;
// PQT-1974 by Yamuna
handlebars.registerHelper('toJSON', function(obj) {
  return JSON.stringify(obj, null, 3);
});
handlebars.registerHelper('ifCondition', function (v1, operator, v2, options) {
  switch (operator) {
      case '==':
          return (v1 == v2) ? options.fn(this) : options.inverse(this);
        }
});

var appliedConditions = '';
var operators = {};

var boolMeaning = {
  "AND": '&&',
  "OR": '||',
  "*": '*',
  "/": '/',
  "+": '+',
  "-": '-',
  "%": '%'
};


var today = new Date();
var start = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
var end;
var type;

var cronString = '5 0 * * *';
var cronType = 'normal';
if (process.env.args == 'type=hourly') {
  cronString = '0 * * * *';
  cronType = 'hourly';
}

var job = new CronJob(cronString, function () {
    working = 0;
    initAlerts();
  }, function () {},
  true
);

/**
 * Method: initAlerts
 * Purpose : Function to start smart alerts
 */

function initAlerts() {
  log.log('starting... ' + moment(new Date(), 'YYYY-MMM-DD HH:mm'));
  var initiationMailObj = {
    'templateType': 'mailProcessInitiation',
    'date': moment(moment.utc(new Date()).toDate()).format('MM/DD/YYYY HH:mm:ss')
  }
  notify('thresoldMailNotification', initiationMailObj, function (e, d) {
    if(e){
      log.log('mailProcessInitiation mail not sent..!!');
    }else{
      log.log('mailProcessInitiation mail sent sucessfully..!!');
    }
  });
  machineDate = new Date();
  machineoffset = machineDate.getTimezoneOffset();
  clientOffset = config.timeZones[config.currentTimeZone] || -330;
  mcdiff = 0;
  mcdiff = -machineoffset + parseInt(clientOffset);
  start.setDate(start.getDate());
  start = new Date(start.getTime() + (mcdiff * 60 * 1000));

  if (cronType == 'hourly') {
    start = new Date(new Date().setHours(new Date().getHours(), 0, 0, 0));
    end = new Date(start.getTime() + (60 * 60 * 1000));
    type = {
      '$eq': 'Hourly'
    };
  } else {
    end = new Date(start.getTime() + (24 * 60 * 60 * 1000));
    type = {
      '$ne': 'Hourly'
    };
  }

  db.collection('calendarThresholds')
    .aggregateAsync([{
        $match: {
          'isActive': true,
          'type': type,
          'notified': false,
          'date': {
            $gte: new Date(start.toISOString()),
            $lt: new Date(end.toISOString())
          }
        }
      },
      {
        $lookup: {
          from: "thresholds",
          localField: "thresholdId",
          foreignField: "_id",
          as: "threshold"
        }
      },
      {
        "$unwind": "$threshold"
      }
    ]).then(function (result) {
      var noOfNotificationShouldGoes = {
        'numberOfEntries': (!result || !result.length) ? 0 : result.length,
        'entriesObj': (!result || !result.length) ? '' : (thresoldMailNotification(result) || []),
        'entriesJson': result,
        'templateType': 'noOfNotificationShouldGoes',
        'date': moment(moment.utc(new Date()).toDate()).format('MM/DD/YYYY HH:mm:ss')
      }
      notify('thresoldMailNotification', noOfNotificationShouldGoes, function (e, d) {
        if(e){
          log.log('noOfNotificationShouldGoes mail not sent..!!');
        }else{
          log.log('noOfNotificationShouldGoes mail sent sucessfully..!!');
        }
      });
      if (!result || !result.length) {
        log.log('No scheduled entries');
        return;
      } else {
        debug(entries.length + " Scheduled Entries Found For Today!!");
        manipulateThresholdWithSchemaAndEntity(result);
      }
    }).catch(function (err) {
      log.log(err);
    });
}

/**
 * Method: manipulateThresholdWithSchemaAndEntity
 * Purpose : Function to attach schemas and entities to the thresholds
 * @param {Array} calendarThresholds
 */
function manipulateThresholdWithSchemaAndEntity(calendarThresholds) {
  var j = 0;

  function processCalendarThresholds(j) {
    if (j >= calendarThresholds.length) {
      log.log('Alert Finished On : '.concat((new Date()).toUTCString()));
    } else {

      var calendarThreshold = calendarThresholds[j];

      working = 0;
      field = field || {};
      mailObj = [];
      var ids;

      if (calendarThreshold.thresholdType == 'entity') {
        ids = _.map((calendarThreshold.schema || []), function (schem) {
          return db.ObjectID(schem._id.toString());
        });
        db
          .collection('entities')
          .find({
            '_id': {
              '$in': ids
            }
          })
          .toArray(function (error, schema) {
            if (error) {
              log.log(`error fetching entities for threshold : ${calendarThreshold.title}`);
              j++;
              processCalendarThresholds(j);
            } else {

              _.forEach(schema, function (schem) {
                field[schem.title] = [];
                _.forEach(schem.questions, function (que) {
                  field[schem.title].push({
                    title: que.title,
                    's#': que['s#'],
                    type: que.type
                  });
                });
              });
              getMatchedConditionData(_.cloneDeep(calendarThreshold), field, calendarThreshold.threshold, function () {
                j++;
                processCalendarThresholds(j);
              });
            }
          });
      } else {
        ids = _.map((calendarThreshold.schema || []), function (schem) {
          return db.ObjectID(schem._id.toString());
        });
        db
          .collection('schema')
          .find({
            '_id': {
              '$in': ids
            }
          })
          .toArray(function (err, schemas) {
            if (err) {
              log.log(`error fetching schema for threshold : ${calendarThreshold.title}`);
              j++;
              processCalendarThresholds(j);
            } else {
              _.forEach(schemas, function (schem) {
                field[schem.title] = [];
                _.forEach(schem.attributes, function (attr) {
                  field[schem.title].push({
                    _id: attr._id,
                    title: attr.title,
                    's#': attr['s#'],
                    type: attr.type
                  });
                });

                _.forEach(schem.entities, function (entity) {
                  _.forEach(entity.questions, function (que) {
                    if (que.isAppearance) {
                      field[schem.title].push({
                        _id: que._id,
                        title: que.title,
                        's#': que['s#'],
                        parentId: que.parentId,
                        type: que.type,
                        isEntity: true
                      });
                    }
                  });
                });

                _.forEach(schem.procedures, function (attr) {
                  _.forEach(attr.questions, function (que) {
                    field[schem.title].push({
                      _id: que._id,
                      title: que.title,
                      's#': que['s#'],
                      parentId: que.parentId,
                      type: que.type,
                      isEntity: false
                    });
                  });
                });
              });
              getMatchedConditionData(_.cloneDeep(calendarThreshold), field, calendarThreshold.threshold, function () {
                j++;
                processCalendarThresholds(j);
              });
            }
          });
      }
    }

  }

  processCalendarThresholds(j);
}

/**
 * Method: getMatchedConditionData
 * Purpose : Function to get matched entity records and app records, based on entities and schemas
 * @param {Object} calendarThreshold
 * @param {Object} f
 * @param {Object} threshold
 * @param {Callback Function} callback
 */
function getMatchedConditionData(calendarThreshold, f, threshold, callback) {
  var formAllEntries = [];
  var entitySelection = [];
  var conditionsAndSchema = {
    conditions: [],
    schemas: []
  };

  _.forEach(threshold.conditions, function (c) {
    if (c && c.leftOperand) {
      conditionsAndSchema.conditions.push(c.leftOperand.title + ' ' + c.comparison + ' ' + c.rightOperand);
    }
  });

  var filter = {};
  if (threshold.thresholdType == 'entity') {
    filter['entityRecord.isActive'] = true;
  }

  if (threshold.reportconfiguration.whichMatchingRecord != 'all') {
    var modifiedEnd = start;
    var modifiedStart;
    if (threshold.frequency.type == 'Hourly'){
      modifiedStart = new Date(modifiedEnd.getTime() - (60 * 60 * 1000));
    } else {
      modifiedStart = new Date(modifiedEnd.getTime() - (24 * 60 * 60 * 1000));
    }
    filter.modifiedDate = {
      $gte: new Date(modifiedStart.toISOString()),
      $lt: new Date(modifiedEnd.toISOString())
    };
  }

  var i = 0;

  function processMatchedData(i) {
    if (i >= threshold.schema.length) {
      var entryCount = (entitySelection || []).reduce(function (acc, curr) {
        return acc + (curr || []).length;
      }, 0);

      if (entitySelection && entitySelection.length) {
        switch (threshold.matchingtrcord.operator) {
          case 'Equal to':
            entitySelection = entryCount == parseInt(threshold.matchingtrcord.value) ? entitySelection : [];
            break;

          case 'Not equal to':
            entitySelection = entryCount != parseInt(threshold.matchingtrcord.value) ? entitySelection : [];
            break;

          case 'Greater than':
            entitySelection = entryCount > parseInt(threshold.matchingtrcord.value) ? entitySelection : [];
            break;

          case 'Less than':
            entitySelection = entryCount < parseInt(threshold.matchingtrcord.value) ? entitySelection : [];
            break;

          case 'Greater than or equal to':
            entitySelection = entryCount >= parseInt(threshold.matchingtrcord.value) ? entitySelection : [];
            break;

          case 'Less than or equal to':
            entitySelection = entryCount <= parseInt(threshold.matchingtrcord.value) ? entitySelection : [];
            break;

          default:
            return [];
        }
      }
    
      if (entitySelection.length || threshold.reportconfiguration.exatmatchrecord === 'Send Full Report') {
        generateMailObj(entitySelection, f, conditionsAndSchema, calendarThreshold, formAllEntries, threshold.reportconfiguration.exatmatchrecord, threshold.thresholdType, function () {
          callback();
        });
      } else {
        callback();
      }
    } else {
      var s = threshold.schema[i];
      db
        .collection(db.ObjectID(s._id).toString())
        .find(filter)
        .toArray(function (err, entries) {
          if (err) {
            log.log(`error fetching records from ${s._id} for threshold : ${calendarThreshold.title}`);
          } else if (entries && entries.length) {
            var keepAllEntites = entries;
            conditionsAndSchema.schemas.push(s.title);
            if (threshold.thresholdType == 'entity') {
              log.log('Entity TD ID:: ' + threshold._id);
              _.forEach(entries, function (entity) {
                var transformedModel = _.reduce(entity.entityRecord, function (a, value, question) {
                  var procIds = question.split('-');
                  if (procIds.length > 1) {
                    if (value[procIds[1]] || value[procIds[1]] === 0) {
                      a[question] = value[procIds[1]];
                    }
                  } else {
                    if (procIds[0] && procIds[0] !== undefined && procIds[0] !== 'isActive' && procIds[0] !== 'entityvalueinactivereason') {
                      a[question] = value;
                    }
                  }
                  return a;
                }, {});

                var format = 'Number';
                var proc = '';
                threshold.conditions = parseConditions(threshold.conditions);
                var lamda = '(function(model){ return ' + parseCriteria(proc, threshold.conditions, format, entity.entityRecord) + '})(' + JSON.stringify(transformedModel) + ');';
                lamda = lamda.replace('(function(model){ return   &&', '(function(model){ return  ').replace('(function(model){ return   ||', '(function(model){ return  ');
                var res = eval(lamda);
    
                if (res) {
                  entitySelection[i] = entitySelection[i] || [];
                  entitySelection[i].push(entity);
                }
              });
            } else {
              log.log('threshold ID:: ' + threshold._id);
              _.forEach(entries, function (entity) {
                var transformedModel = _.reduce(entity.entries, function (a, value, question) {
                  var procIds = question.split('-');
                  if (procIds.length > 1) {
                    if (value[procIds[1]] || value[procIds[1]] === 0) {
                      a[question] = value[procIds[1]];
                    }
                  } else {
                    if (procIds[0] && procIds[0] !== 'FailMesssageForQCForm' && procIds[0] !== 'draftStatus' && procIds[0] !== 'isattributesadded' && procIds[0] !== 'status' && procIds[0] !== 'settings' && procIds[0] !== 'reviewStatus' &&
                      procIds[0] !== 'verifiedBy' && procIds[0] !== 'createdBy' && procIds[0] !== 'modifiedBy' && procIds[0] !== 'createdDate' && procIds[0] !== 'modifiedDate' && procIds[0] !== 'version' && procIds[0] !== 'versions' && procIds[0] !== 'isMajorVersion') {
                      a[question] = value.attributes;
                    }
                  }
                  return a;
                }, {});
                var format = 'Number';
                var proc = '';
                threshold.conditions = parseConditions(threshold.conditions);
                var lamda = '(function(model){ return ' + parseCriteria(proc, threshold.conditions, format, entity.entries) + '})(' + JSON.stringify(transformedModel) + ');';
                lamda = lamda.replace('(function(model){ return   &&', '(function(model){ return  ').replace('(function(model){ return   ||', '(function(model){ return  ');
                var res = eval(lamda);

                if (res) {
                  entitySelection[i] = entitySelection[i] || [];
                  entitySelection[i].push(entity);
                }
              });
            }

            formAllEntries.push(keepAllEntites);

          }
          i++;
          processMatchedData(i);

        });

    }
  }

  processMatchedData(i);
}

/**
 * Method: parseCriteria
 * Purpose : Function to parse records on desired formates
 * @param {String} procId
 * @param {Object} criteria
 * @param {String} format
 * @param {Object} model
 */
function parseCriteria(procId, criteria, format, model) {
  var val = '';
  _.forEach(criteria, function (c) {
    if (c.conditions) { //check group conditions
      val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + '(' + parseCriteria(procId, c.conditions, format, model) + ')';
    } else {
      if (!_.isUndefined(c.comparison) && c.comparison !== null) {
        val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + operators[c.comparison](c.leftOperand, c.rightOperand, c.rightEndOperand ? c.rightEndOperand : '', _.isObject(c.leftOperand) ? c.leftOperand.type.format.title : format);
      } else {
        if (!_.isUndefined(c.leftOperand) && c.leftOperand !== null) {
          if (c.leftOperand['type']['format']['title'] === 'Exponential') {
            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + '(model[\'' + c.leftOperand['s#'] + '-' + c.leftOperand['parentId'] + '\'] * (Math.pow(10, ' + c.leftOperand['type']['format']['metadata']['power'] + ')))';
          } else {
            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + '(model[\'' + c.leftOperand['s#'] + '-' + c.leftOperand['parentId'] + '\'])';
          }
        } else if (!_.isUndefined(c.rightOperand) && c.rightOperand !== null) {
          if (_.isObject(c.rightOperand) && c.rightOperand['type']['format']['title'] === 'Exponential') {
            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + (_.isObject(c.rightOperand) ? '(model[\'' + c.rightOperand['s#'] + '-' + c.rightOperand['parentId'] + '\'] * (Math.pow(10, ' + c.rightOperand['type']['format']['metadata']['power'] + ')))' : parseFloat(c.rightOperand) ? parseFloat(c.rightOperand) : 0);
          } else if (_.isObject(c.rightOperand) && c.rightOperand['type']['format']['title'] === 'Date') {
            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + (_.isObject(c.rightOperand) ? '(model[\'' + c.rightOperand['s#'] + '-' + c.rightOperand['parentId'] + '\'])' : new Date(c.rightOperand) ? new Date(c.rightOperand) : 0);
          } else {
            val = val + ' ' + (c.bool ? ' ' + boolMeaning[c.bool] + ' ' : '') + (_.isObject(c.rightOperand) ? '(model[\'' + c.rightOperand['s#'] + '-' + c.rightOperand['parentId'] + '\'])' : new Date(c.rightOperand) ? new Date(c.rightOperand) : 0);
          }
        }
      }
    }
  });
  return val;
}

/**
 * Method: generateMailObj
 * Purpose : Function for mapping object as per pdf or mail
 * @param {Array} entitySelectionParams
 * @param {Object} fieldParams
 * @param {Object} conditionsAndSchema
 * @param {Object} calendarThreshold
 * @param {Array} formAllEntries
 * @param {Object} texatmatchrecord
 * @param {Object} thresholdType
 * @param {Callback Function} callback
 */
function generateMailObj(entitySelectionParams, fieldParams, conditionsAndSchema, calendarThreshold, formAllEntries, texatmatchrecord, thresholdType, callback) {
  var entitySelectionOrAllEntries = entitySelectionParams;
  var comboOrUniqueID;
  var measurementUnit = '';
  var power = '';
  var currency = '';

  if (texatmatchrecord === 'Send Full Report') {
    entitySelectionOrAllEntries = formAllEntries;
  }
  if (entitySelectionOrAllEntries.length === 0) {
    //START:: PQT-2485 by Yamuna
    var emptyThresoldNotifyFunctions = [];
    _.forEach(entries, function (entity) {
      entity.noRecordsFound = true;
      emptyThresoldNotifyFunctions.push(thresoldNotify(entity));
    });
    q.all(emptyThresoldNotifyFunctions).then(function (response) {
      log.log("noRecordsFoundEntries notify completed");
    });
    return;
  }
  if (entitySelectionOrAllEntries.length) {
    _.forEach(entitySelectionOrAllEntries, function (entitySelectionParam, parentIndex) {
      mailObj[parentIndex] = [];
      _.forEach(entitySelectionParam, function (entry, childIndex) {
        mailObj[parentIndex][childIndex] = [];
        _.forEach(fieldParams[calendarThreshold.schema[0].title], function (field) {
          if (field) {
            if (field.parentId) {
              comboOrUniqueID = field['s#'] + '-' + field.parentId;
            } else {
              comboOrUniqueID = field['s#'];
            }
            if (thresholdType == 'entity') {
              if (entry && entry.entityRecord && entry.entityRecord.hasOwnProperty(comboOrUniqueID)) {
                if (comboOrUniqueID.indexOf('-') === -1) {
                  if (field.type && field.type.format && field.type.format.title == 'Date') {
                    mailObj[parentIndex][childIndex].push({
                      title: field['title'],
                      value: moment(new Date(entry.entityRecord[comboOrUniqueID])).utc().tz(clientTZ).format('MM/DD/YYYY')
                    });
                  } else if (field.type && field.type.format && field.type.format.title == 'Time') {
                    mailObj[parentIndex][childIndex].push({
                      title: field['title'],
                      value: moment(new Date(entry.entityRecord[comboOrUniqueID])).utc().tz(clientTZ).format('HH:mm:ss')
                    });
                  } else if (field.type && field.type.format && field.type.title == 'File Attachment') {
                      var value = (entry.entityRecord[comboOrUniqueID] || '').toString().split('-')[1];
                      value = entriesUtility.getFileName(entry.entityRecord[comboOrUniqueID], value);

                      mailObj[parentIndex][childIndex].push({
                        title: field['title'],
                        value: value
                      });
                  } else {
                    measurementUnit = '';
                    power = '';
                    currency = '';
                    if (field.type && field.type.format && field.type.format.metadata) {
                      if (field.type.format.metadata.MeasurementUnit) {
                        measurementUnit = ' (' + field.type.format.metadata.MeasurementUnit + ')';
                      }
                      if (field.type.format.metadata.power) {
                        power = ' (10^' + field.type.format.metadata.power.toString() + ')';
                      }
                    }
                    if (field.type && field.type.format && field.type.format.title == 'Currency') {
                      currency = '$';
                    }
                    mailObj[parentIndex][childIndex].push({
                      title: field['title'],
                      value: currency + '' + entry.entityRecord[comboOrUniqueID] + '' + power + measurementUnit
                    });
                  }
                } else {
                  mailObj[parentIndex][childIndex].push({
                    title: field['title'],
                    value: entry.entityRecord[comboOrUniqueID][field.parentId]
                  });
                }
              } else {
                mailObj[parentIndex][childIndex].push({
                  title: field['title'],
                  value: ''
                });
              }
            } else {
              if (entry && entry.entries && entry.entries.hasOwnProperty(comboOrUniqueID)) {
                if (comboOrUniqueID.indexOf('-') === -1) {
                  if (field.type && field.type.format && field.type.format.title == 'Date') {
                    mailObj[parentIndex][childIndex].push({
                      title: field['title'],
                      value: moment(new Date(entry.entries[comboOrUniqueID].attributes)).utc().tz(clientTZ).format('MM/DD/YYYY')
                    });
                  } else {
                    mailObj[parentIndex][childIndex].push({
                      title: field['title'],
                      value: entry.entries[comboOrUniqueID].attributes
                    });
                  }
                } else {
                  if (entry.entries[comboOrUniqueID] && entry.entries[comboOrUniqueID][field.parentId]) {
                    if (field.type && field.type.format && field.type.format.title == 'Date') {
                      mailObj[parentIndex][childIndex].push({
                        title: field['title'],
                        value: moment(new Date(entry.entries[comboOrUniqueID][field.parentId])).utc().tz(clientTZ).format('MM/DD/YYYY')
                      });
                    } else if (field.type && field.type.format && field.type.format.title == 'Time') {
                      mailObj[parentIndex][childIndex].push({
                        title: field['title'],
                        value: moment(new Date(entry.entries[comboOrUniqueID][field.parentId])).utc().tz(clientTZ).format('HH:mm:ss')
                      });
                    } else if (field.type && field.type.title == 'File Attachment') {
                        var value = (entry.entries[comboOrUniqueID][field.parentId] || '').toString().split('-')[1];
                        value = entriesUtility.getFileName(entry.entries[comboOrUniqueID][field.parentId], value);
  
                        mailObj[parentIndex][childIndex].push({
                          title: field['title'],
                          value: value
                        });
                    } else {
                      measurementUnit = '';
                      power = '';
                      currency = '';
                      if (field.type && field.type.format && field.type.format.metadata) {
                        if (field.type.format.metadata.MeasurementUnit) {
                          measurementUnit = ' (' + field.type.format.metadata.MeasurementUnit + ')';
                        }
                        if (field.type.format.metadata.power) {
                          power = ' (10^' + field.type.format.metadata.power.toString() + ')';
                        }
                      }
                      if (field.type && field.type.format && field.type.format.title == 'Currency') {
                        currency = '$';
                      }
                      mailObj[parentIndex][childIndex].push({
                        title: field['title'],
                        value: currency + '' + entry.entries[comboOrUniqueID][field.parentId] + '' + power + measurementUnit
                      });
                    }
                  } else {
                    mailObj[parentIndex][childIndex].push({
                      title: field['title'],
                      value: ''
                    });
                  }
                }
              } else {
                mailObj[parentIndex][childIndex].push({
                  title: field['title'],
                  value: ''
                });
              }
            }
          }
        });
      });
    });
  }

  if (mailObj.length) {
    // ================ QC3-11395: mahammad ================
    // if (working) {
    //   debug('returning ... ');
    //   return;
    // }
    // ================ QC3-11395: mahammad ================

    if (!calendarThreshold) {
      debug('No scheduled entries');
      return;
    } else {
      debug(calendarThreshold + " scheduled entries");
    }

    // PDF
    // setTimeout(function () {
    //   generatePdf(mailObj, conditionsAndSchema, calendarThreshold, callback);
    // }, (Math.floor(Math.random() * (25000 - 2000 + 1)) + 2000));

    // EXCEL
    generateExcelData(mailObj, conditionsAndSchema, calendarThreshold, callback);
  } else {
    callback();
  }
}

function generateExcelData(mailObj, conditionsAndSchema, calendarThreshold, callback) {
  var dataset = (mailObj || []).map(function (array, index) {
    return {
      name: conditionsAndSchema.schemas[index],
      specification: (array[0] || []).reduce(function (obj, value) {
        obj[value.title] = {
          displayName: value.title,
          headerStyle: {},
          width: 150
        };
        return obj;
      }, {}),
      data: (array || []).map(function (item) {
        return (item || []).reduce(function (obj, item) {
          obj[item.title] = item.value;
          return obj;
        }, {});
      })
    };
  });

  tempFileName = (new Date()).getTime() + '-' + (calendarThreshold.threshold['title'] || '').replace(/[^a-zA-Z0-9 ]/g, "");
  calendarThreshold.tempFileName = tempFileName;

  generateExcel(tempFileName, dataset, function (error, result) {
    afterCreatingFile(calendarThreshold, result.fileName, callback);
  });
}

function afterCreatingFile(calendarThreshold, tempFileName, callback) {
  mapUserIdWithUser(calendarThreshold, function (e, calendarThreshold) {
    // nofity(calendarThreshold, tempFileName, function (e, d) {
    //   log.log(`Threshold ${calendarThreshold.title} is successfully sent`);
    //   working--;
    //   if (e) {
    //     log.log(`error in notification for threshold : ${calendarThreshold.title}`);
    //     return;
    //   }

    //   d.notified = true;
    //   d.scheduledBy = d.scheduledBy._id.toString();
    //   db.calendarThresholds.save(d);
    //   // FOR REMOVEING FILE AFTER SENDING TO MAIL 
    //   // try {
    //   //   fs.unlinkSync(unlinkFilePath);
    //   // } catch (error) {
    //   //   log.log(`Error on deleting file ${unlinkFilePath}`);
    //   //   log.log(error);
    //   // }
    //   callback();
    // });
    var noOfNotificationSent = {
      'numberOfEntries': (!calendarThreshold || !calendarThreshold.length) ? 0 : calendarThreshold.length,
      'entriesObj': (!calendarThreshold || !calendarThreshold.length) ? '' : (thresoldMailNotification(calendarThreshold) || []),
      'entriesJson': calendarThreshold,
      'templateType': 'noOfNotificationSent',
      'date': moment(moment.utc(new Date()).toDate()).format('MM/DD/YYYY HH:mm:ss')
    }
    notify('thresoldMailNotification', noOfNotificationSent, function (e, d) {
      if(e){
        log.log('noOfNotificationSent mail not sent..!!');
      }else{
        log.log('noOfNotificationSent mail sent sucessfully..!!');

        var thresoldNotifyFunctions = [];
        _.forEach(entries, function (entity) {
          thresoldNotifyFunctions.push(thresoldNotify(entity, tempFileName))
        });
 
        q.all(thresoldNotifyFunctions).then(function(err, res){
          var mailProcessCompletion = {
            'numberOfEntries': (!entries || !entries.length) ? 0 : entries.length,
            'entriesObj': (!entries || !entries.length) ? '' : (thresoldMailNotification(entries) || []),
            'entriesJson': entries,
            'templateType': 'mailProcessCompletion',
            'date': moment(moment.utc(new Date()).toDate()).format('MM/DD/YYYY HH:mm:ss')
          }
              notify('thresoldMailNotification', mailProcessCompletion, function (e, d) {
            if(e){
              log.log('mailProcessCompletion mail not sent..!!');
            }else{
              log.log('mailProcessCompletion mail sent sucessfully..!!');
            }
          });
        })
      }
  });
});
}

/**
 * Method: generatePdf
 * Purpose : Function for generating pdf for threshold mail
 * @param {Object} mailObj
 * @param {Object} conditionsAndSchema
 * @param {Object} calendarThreshold
 * @param {Callback Function} callback
 */
function generatePdf(mailObj, conditionsAndSchema, calendarThreshold, callback) {
  if (mailObj.length === 0) {
    return;
  }
  var titleArr = [];
  _.forEach(mailObj, function (mo, parentIndex) {
    titleArr[parentIndex] = [];
    _.forEach(mo, function (m, childIndex) {
      if (childIndex === 0) {
        _.forEach(m, function (obj) {
          titleArr[parentIndex].push(obj.title);
        });
      }
    });
  });

  var unique = conditionsAndSchema.conditions.filter(function (elem, index, self) {
    return index == self.indexOf(elem);
  });

  conditionsAndSchema.conditions = unique;
  var finalObj = (conditionsAndSchema.schemas || []).map(function (sch, index) {
    return {
      'schema': sch,
      'titleArr': titleArr[index],
      'mailObj': mailObj[index] //QC3-11404: mahammad - mailObj[0] to mailObj[i]
    };
  });

  if (!finalObj.length || !finalObj[0].mailObj.length) {
    return;
  }

  //TBD
  _.forEach(finalObj[0].mailObj, function (ob) {
    _.forEach(ob, function (mo) {
      if (mo.value && mo.value.length === 24 && mo.value.charAt(23) === 'Z' && mo.value.charAt(10) === 'T' && mo.value.charAt(19) === '.') {
        mo.value = moment(moment.utc(new Date(mo.value)).toDate()).format('MM/DD/YYYY HH:mm:ss');
      }
    });
  });

  var jjj = {
    title: "Smart Alert",
    author: "ProT",
    finalObj: finalObj,
    conditionsAndSchema: conditionsAndSchema
  };

  fs.readFile(config.hbspath + 'threshold-notify/html.hbs', function read(err, data) {
    if (err) {
      throw err;
    }
    var template = handlebars.compile(data.toString());
    var html = template(jjj);

    var options = {
      "format": 'Tabloid',
      "height": "10.5in",
      "width": exportUtilty.setPdfWidth(finalObj[0].titleArr),
      "timeout": 900000
    };

    working++;
    tempFileName = (calendarThreshold.threshold['title'] || '')
      .replace(/[^a-zA-Z0-9 ]/g, "")
      .concat('-', (new Date()).getTime(), '.pdf');
    // tempFileName = 'record' + working + '.pdf';
    calendarThreshold.tempFileName = tempFileName;
    var unlinkFilePath = config.staticpath + tempFileName;
    pdf.create(html, options).toFile(config.staticpath + tempFileName, function (err, res) {
      if (err) log.log(err);
      afterCreatingFile(calendarThreshold, tempFileName, callback);
    });
  });
}

/**
 * Method: nofity
 * Purpose : Function for sending threshold mail
 * @param {Object} entity
 * @param {Callback Function} cb
 */
function nofity(entity, tempFileName, cb) {
  var schemaTitles = '';
  _.forEach(entity.schema, function (schema, index) {
    schemaTitles += index == 0 ? schema.title : ', ' + schema.title;
  });

  appliedConditions = '';
  // _.forEach(entity.thresholdCondition, function (c) {
  //   if (c) {
  //     c.bool = c.bool ? c.bool : '';
  //     if (c.leftOperand && c.leftOperand.type && c.leftOperand.type.format && c.leftOperand.type.format.title == 'Date' && c.dateSelection && c.dateSelection != 'Specific Date') {
  //       appliedConditions += `${c.bool} ${c.leftOperand.title} ${c.comparison} ${c.dateSelection}`;
  //     } else {
  //       if (c.leftOperand && c.leftOperand.type && c.leftOperand.type.format && c.leftOperand.type.format.title == 'Date') {
  //         appliedConditions += `${c.bool} ${c.leftOperand.title} ${c.comparison} ${moment(new Date(c.rightOperand)).utc().tz(clientTZ).format('MM/DD/YYYY')}`;
  //       } else {
  //         appliedConditions += `${c.bool} ${c.leftOperand.title} ${c.comparison} ${c.rightOperand}`;
  //       }
  //     }
  //   }
  // });

  // ================ QC3-11396 + QC3-11402: mahammad ================

  rConditionStringify(entity.thresholdCondition);

  function rConditionStringify(conditions) {
    (conditions || []).forEach(function (c) {
      if (c.conditions) {
        if (c.bool && appliedConditions) {
          appliedConditions += ' ' + c.bool + ' ';
        }
        appliedConditions += '(';
        rConditionStringify(c.conditions);
        appliedConditions += ')';
      } else {
        if (c.leftOperand && c.leftOperand.type.format.title == 'Date' && c.dateSelection) {
          if (c.bool && appliedConditions) {
            appliedConditions += ' ' + c.bool + ' ';
          }
          appliedConditions += (c.leftOperand.title + ' ' + c.comparison + ' ' + (c.dateSelection === 'Specific Date' ? moment(c.rightOperand).format('MM/DD/YYYY') : c.dateSelection));
        } else {
          if (c && c.leftOperand) {
            if (c.bool && appliedConditions) {
              appliedConditions += ' ' + c.bool + ' ';
            }
            appliedConditions += (c.leftOperand.title + ' ' + c.comparison + ' ' + c.rightOperand);
          }
        }
      }
    })
  }

  // ================ QC3-11396 + QC3-11402: mahammad ================

  debug('Sending notification to ' + entity.email);
  var thresholdType = '';
  if (!entity.thresholdType || entity.thresholdType == 'app') {
    thresholdType = 'appThreshold';
  } else {
    thresholdType = 'entityThreshold';
  }
  _.merge(entity, settings);
  if(entity.noRecordsFound){
    _.merge(entity, {'schemaTitles': schemaTitles, [thresholdType]: true, 'appliedConditions': appliedConditions});
  }else{
    _.merge(entity, {'schemaTitles': schemaTitles, [thresholdType]: true, 'appliedConditions': appliedConditions, 'fileInfo': [{path: config.staticpath + entity.tempFileName}]});
  }  
  log.log('inside file: threshold.js - function: notify - status: sending mail');

  mailer.send('threshold-notify-mail-content', entity, entity.email, function sendMailCallback(err, value) {
    if (err) {
      debug.err('Failed sending mail - review-scheduler-notify');
      debug.err(err);

      // ================ QC3-11395: mahammad ================

      setTimeout(function () {
        mailer.send('threshold-notify-mail-content', entity, entity.email, function sendMailCallback(err, value) {
          if (err) {
            debug.err('Failed sending mail - review-scheduler-notify');
            debug.err(err);

            setTimeout(function () {
              mailer.send('threshold-notify-mail-content', entity, entity.email, function sendMailCallback(err, value) {
                if (err) {
                  debug.err('Failed sending mail - review-scheduler-notify');
                  debug.err(err);

                  setTimeout(function () {
                    mailer.send('threshold-notify-mail-content', entity, entity.email, function sendMailCallback(err, value) {
                      if (err) {
                        debug.err('Failed sending mail - review-scheduler-notify');
                        debug.err(err);
                        cb(err, entity);
                      } else {
                        log.log('Email has been resent3 to ' + entity.email);
                        entity.fileInfo = [];
                        delete entity.fileInfo;
                        debug('Mail send successfully to: ' + entity.email.underline);
                        addToThresholdNotification(entity)
                        cb(err, entity);
                      }
                    })
                  }, (Math.floor(Math.random() * (25000 - 2000 + 1)) + 2000));
                } else {
                  log.log('Email has been resent2 to ' + entity.email);
                  entity.fileInfo = [];
                  delete entity.fileInfo;
                  debug('Mail send successfully to: ' + entity.email.underline);
                  addToThresholdNotification(entity)
                  cb(err, entity);
                }
              })
            }, (Math.floor(Math.random() * (25000 - 2000 + 1)) + 2000));
          } else {
            log.log('Email has been resent1 to ' + entity.email);
            entity.fileInfo = [];
            delete entity.fileInfo;
            debug('Mail send successfully to: ' + entity.email.underline);
            addToThresholdNotification(entity)
            cb(err, entity);
          }
        })
      }, (Math.floor(Math.random() * (25000 - 2000 + 1)) + 2000));

      // ================ QC3-11395: mahammad ================

    } else {
      log.log('Email has been sent to ' + entity.email);
      entity.fileInfo = [];
      delete entity.fileInfo;
      debug('Mail send successfully to: ' + entity.email.underline);
      addToThresholdNotification(entity, tempFileName);
      cb(err, entity);
    }
  }, true);
}

/**
 * Method: addToThresholdNotification
 * Purpose : Function to add notification after sending threshold
 * @param {Object} entry
 */
function addToThresholdNotification(entry, tempFileName) {
  var d = {
    thresholdId: (entry.thresholdId).toString(),
    title: entry.title,
    email: entry.email,
    logdate: new Date(),
    fileName: tempFileName
  };
  db.thresholdsNotification.save(d);
}

/**
 * Method: parseConditions
 * Purpose : Function to parse conditions
 * @param {Array} conditions
 */
//merged Surjeet's HOTFIX for mda-preval
// function parseConditions(conditions) {
//   var startEndDate;
//   _.forEach(conditions, function (condition) {
//     if (condition && condition.dateSelection) {
//       startEndDate = getStartEndDate(condition);
//       if (condition.comparison.toLowerCase() == ('Greater than').toLowerCase() || (condition.comparison).toLowerCase() == ('Less than or equal to').toLowerCase()) {
//         condition.rightOperand = startEndDate.lastDay.toISOString();
//         condition.rightEndOperand = undefined;
//       } else {
//         condition.rightOperand = startEndDate.firstDay.toISOString();
//         condition.rightEndOperand = (startEndDate.lastDay == undefined) ? undefined : startEndDate.lastDay.toISOString();
//         if (condition.dateSelection != 'Today' && condition.dateSelection != 'Specific Date') {
//           if (condition.comparison == 'Equal to') {
//             condition.comparison = 'Between';
//           } else if (condition.comparison == 'Not equal to') {
//             condition.comparison = 'Not Between';
//           }
//         }
//       }
//     }
//   });
//   return conditions;
// }

function parseConditions(conditions) {
  var startEndDate;
  _.forEach(conditions, function (condition) {
    if (condition.conditions) {
      parseConditions(condition.conditions);
    } else {
      if (condition && condition.dateSelection) {
          startEndDate = getStartEndDate(condition);
          if (condition.comparison.toLowerCase() == ('Greater than').toLowerCase() || (condition.comparison).toLowerCase() == ('Less than or equal to').toLowerCase()) {
            condition.rightOperand = startEndDate.lastDay.toISOString();
            condition.rightEndOperand = undefined ;
          }else {
            condition.rightOperand = startEndDate.firstDay.toISOString();
            condition.rightEndOperand = startEndDate.lastDay == undefined ? undefined : startEndDate.lastDay.toISOString();
            if (condition.dateSelection != 'Today' && condition.dateSelection != 'Specific Date') {
              if (condition.comparison == 'Equal to') {
                condition.comparison = 'Between';
              }else if(condition.comparison == 'Not equal to') {
                condition.comparison = 'Not Between';
              }
            }
          }
      }
    }
  });
  return conditions;
 }

/**
 * Method: getStartEndDate
 * Purpose : Function to get start and end date on behalf of the condition
 * @param {Object} condition
 */
function getStartEndDate(condition) {
  var startEndDate;
  var today = new Date();
  today = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
  today.setDate(today.getDate());
  today = new Date(today.getTime() + (mcdiff * 60 * 1000));

  switch (condition.dateSelection) {
    case 'This Week':
      startEndDate = {
        firstDay: moment().startOf('week'),
        lastDay: moment().endOf('week')
      };
      break;

    case 'This Month':
      startEndDate = {
        firstDay: moment().startOf('month'),
        lastDay: moment().endOf('month')
      };
      break;

    case 'This Year':
      startEndDate = {
        firstDay: moment().startOf('year'),
        lastDay: moment().endOf('year')
      };
      break;

    case 'Previous Week':
      startEndDate = {
        firstDay: moment().day(-7),
        lastDay: moment().day(-1)
      };
      break;

    case 'Previous Month':
      var thisMonth = today.getMonth();
      today.setMonth(thisMonth - 1);
      if (today.getMonth() != thisMonth - 1 && (today.getMonth() != 11 || (thisMonth == 11 && today.getDate() == 1))) {
        today.setDate(0);
      }
      startEndDate = {
        firstDay: moment(today).startOf('month'),
        lastDay: moment(today).endOf('month')
      };
      break;

    case 'Previous Year':
      var thisYear = today.getFullYear();
      today.setYear(thisYear - 1);
      if (today.getFullYear() != thisYear - 1 && (today.getFullYear() != 11 || (thisYear == 11 && today.getDate() == 1))) {
        today.setDate(0);
      }
      startEndDate = {
        firstDay: moment(today).startOf('year'),
        lastDay: moment(today).endOf('year')
      };
      break;

    case 'Specific Date':
      startEndDate = {
        firstDay: new Date(condition.rightOperand),
        lastDay: new Date(condition.rightOperand)
      };
      break;

    default:
      startEndDate = {
        firstDay: new Date(new Date(moment().add(-1, 'days'))),
        lastDay: new Date(new Date(moment().add(-1, 'days')))
      };
  }
  return startEndDate;
}

/**
 * Method: mapUserIdWithUser
 * Purpose : Function to map createdBy, modifiedBy and scheduledBy with user name
 * @param {Object/Array} data
 * @param {Callback Function} cb
 */
function mapUserIdWithUser(data, cb) {
  var returnOne = !(data instanceof Array);
  if (returnOne) {
    data = [data];
  }
  var ids = _
    .chain(data)
    .map(function (record) {
      var ret = [];

      if (record.createdBy) {
        ret.push(db.ObjectID(record.createdBy));
      }
      if (record.modifiedBy) {
        ret.push(db.ObjectID(record.modifiedBy));
      }
      if (record.scheduledBy) {
        ret.push(db.ObjectID(record.scheduledBy));
      }

      if (record.path === 'modifiedBy' || record.path === 'createdBy' || record.path === 'scheduledBy') {
        debug(record);
        if (record.old) {
          ret.push(db.ObjectID(record.old));
        }
        if (record.new) {
          ret.push(db.ObjectID(record.new));
        }
      }
      return ret;
    })
    .filter(function (record) {
      return record.length;
    }).reduce(function (a, n) {
      return _.union(a, n);
    }, [])
    .value();

  db
    .users
    .find({
      '_id': {
        $in: ids
      }
    }, {
      'firstname': 1,
      'lastname': 1,
      'username': 1
    })
    .toArray(function (e, d) {
      d = _.reduce(d, function (acc, user) {
        acc[user._id] = user;
        return acc;
      }, {});

      _.forEach(data, function (record) {

        if (record.createdBy) {
          record.createdBy = d[record.createdBy.toString()];
        }
        if (record.modifiedBy) {
          record.modifiedBy = d[record.modifiedBy.toString()];
        }
        if (record.scheduledBy) {
          record.scheduledBy = d[record.scheduledBy.toString()];
        }


        if (record.path === 'modifiedBy' || record.path === 'createdBy' || record.path === 'scheduledBy') {
          debug(record);
          if (record.old) {
            record.old = d[record.old];
            if (record.old) {
              record.old = record.old.firstname + ' ' + record.old.lastname
            } else {
              record.old = 'Anonymous'
            }
          }
          if (record.new) {
            record.new = d[record.new];
            if (record.new) {
              record.new = record.new.firstname + ' ' + record.new.lastname
            } else {
              record.new = 'Anonymous'
            }
          }
        }
      });

      cb(null, returnOne ? data[0] : data);
    });
}

/**
 * Methods: operators validations
 * Purpose : Functions for perform operator functionalities
 * @param {String/Object} format
 * @param {String/Object} op, op1, op2, op3
 */
operators['format'] = function (format, op) {
  switch (format) {
    case 'None':
      return '\'' + op + '\'';
    default:
      return op;
  }
}

operators['>'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isAfter(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')))';
    } else {
      return 'model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  } else {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isAfter(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')) )';
    } else {
      return 'model[\'' + op1['s#'] + '\'] > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['>='] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSameOrAfter(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')))';
    } else {
      return 'model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  } else {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSameOrAfter(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')) )';
    } else {
      return 'model[\'' + op1['s#'] + '\'] >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['<'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isBefore(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')))';
    } else {
      return 'model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  } else {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isBefore(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')) )';
    } else {
      return 'model[\'' + op1['s#'] + '\'] < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['<='] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSameOrBefore(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')))';
    } else {
      return 'model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  } else {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSameOrBefore(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')) )';
    } else {
      return 'model[\'' + op1['s#'] + '\'] <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['contains'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    return '(\'\' + model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ') !== -1';
  } else {
    return '(\'\' + model[\'' + op1['s#'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ') !== -1';
  }
}

operators['does not contain'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    return '(\'\' + model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ') === -1';
  } else {
    return '(\'\' + model[\'' + op1['s#'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ') === -1';
  }
}

operators['starts with'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    return '(\'\' + model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']).startsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
  } else {
    return '(\'\' + model[\'' + op1['s#'] + '\']).startsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
  }
}

operators['ends with'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    return '(\'\' + model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']).endsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
  } else {
    return '(\'\' + model[\'' + op1['s#'] + '\']).endsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
  }
}

operators['Contains'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    return '(\'\' + model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ') !== -1';
  } else {
    return '(\'\' + model[\'' + op1['s#'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ') !== -1';
  }
}

operators['Does Not Contain'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    return '(\'\' + model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ') === -1';
  } else {
    return '(\'\' + model[\'' + op1['s#'] + '\']).indexOf(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ') === -1';
  }
}

operators['Starts With'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    return '(\'\' + model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']).startsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
  } else {
    return '(\'\' + model[\'' + op1['s#'] + '\']).startsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
  }
}

operators['Ends With'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    return '(\'\' + model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']).endsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
  } else {
    return '(\'\' + model[\'' + op1['s#'] + '\']).endsWith(' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
  }
}

operators['='] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(_.isArray(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ' )) == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + '))';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(_.isArray(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ' )) == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '))';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(_.isArray(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + '))';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSame(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')))';
    } else {
      return '(_.isArray(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '))';
    }
  } else {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(_.isArray(model[\'' + op1['s#'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ' )) == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + '))';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(_.isArray(model[\'' + op1['s#'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ' )) == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '))';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(_.isArray(model[\'' + op1['s#'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '\'] == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + '))';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSame(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')) )';
    } else {
      return '(_.isArray(model[\'' + op1['s#'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '\'] == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '))';
    }
  }
}

operators['+'] = function (op1, op2, op3, format) {
  if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
    if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) + ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
    } else if (op1['type']['format']['title'] === 'Exponential') {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) + ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
    } else if (op2['type']['format']['title'] === 'Exponential') {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] + ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
    }
  } else {
    return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] + ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
  }
}

operators['*'] = function (op1, op2, op3, format) {
  if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
    if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) * ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
    } else if (op1['type']['format']['title'] === 'Exponential') {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) * ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
    } else if (op2['type']['format']['title'] === 'Exponential') {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
    }
  } else {
    return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
  }
}

operators['/'] = function (op1, op2, op3, format) {
  if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
    if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) / ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
    } else if (op1['type']['format']['title'] === 'Exponential') {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) / ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
    } else if (op2['type']['format']['title'] === 'Exponential') {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] / ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
    }
  } else {
    return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] / ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
  }
}

operators['%'] = function (op1, op2, op3, format) {
  if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
    if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) % ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
    } else if (op1['type']['format']['title'] === 'Exponential') {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) % ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
    } else if (op2['type']['format']['title'] === 'Exponential') {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] % ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
    }
  } else {
    return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] % ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
  }
}

operators['-'] = function (op1, op2, op3, format) {
  if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
    if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) - ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
    } else if (op1['type']['format']['title'] === 'Exponential') {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) - ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
    } else if (op2['type']['format']['title'] === 'Exponential') {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] - ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
    }
  } else {
    return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] - ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
  }
}

operators['!='] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '!(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSame(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')))';
    } else {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
    }
  } else {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '!(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSame(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')) )';
    } else {
      return '(model[\'' + op1['s#'] + '\'] != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
    }
  }
}

operators['Greater than'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isAfter(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')))';
    } else {
      return 'model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  } else {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isAfter(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')) )';
    } else {
      return 'model[\'' + op1['s#'] + '\'] > ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['Greater than or equal to'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSameOrAfter(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')))';
    } else {
      return 'model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  } else {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSameOrAfter(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')) )';
    } else {
      return 'model[\'' + op1['s#'] + '\'] >= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['Less than'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isBefore(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')))';
    } else {
      return 'model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  } else {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isBefore(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')) )';
    } else {
      return 'model[\'' + op1['s#'] + '\'] < ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['Less than or equal to'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSameOrBefore(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')))';
    } else {
      return 'model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2));
    }
  } else {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSameOrBefore(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')) )';
    } else {
      return 'model[\'' + op1['s#'] + '\'] <= ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2));
    }
  }
}

operators['Equal to'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(_.isArray(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ' )) == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + '))';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(_.isArray(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ' )) == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '))';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(_.isArray(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + '))';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSame(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')))';
    } else {
      return '(_.isArray(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '))';
    }
  } else {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(_.isArray(model[\'' + op1['s#'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ' )) == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + '))';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(_.isArray(model[\'' + op1['s#'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ' )) == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '))';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(_.isArray(model[\'' + op1['s#'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '\'] == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + '))';
      }
    } else if (format == 'Date') {
      return '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSame(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')) )';
    } else {
      return '(_.isArray(model[\'' + op1['s#'] + '\']) ? (_.includes(model[\'' + op1['s#'] + '\'],' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')) : (model[\'' + op1['s#'] + '\'] == ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '))';
    }
  }
}

operators['Not equal to'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '!(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSame(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')))';
    } else {
      return '(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'] != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + ')';
    }
  } else {
    if (op1['type']['format']['title'] === 'Exponential' || (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
      if (op1['type']['format']['title'] === 'Exponential' && (_.isObject(op2) && op2['type']['format']['title'] === 'Exponential')) {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10 ,' + op1['type']['format']['metadata']['power'] + ')) != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10 ,' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      } else if (op1['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] * (Math.pow(10, ' + op1['type']['format']['metadata']['power'] + ')) != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
      } else if (op2['type']['format']['title'] === 'Exponential') {
        return '(model[\'' + op1['s#'] + '\'] != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\'] * (Math.pow(10, ' + op2['type']['format']['metadata']['power'] + '))' : operators['format'](format, op2)) + ')';
      }
    } else if (format == 'Date') {
      return '!(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSame(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\')) )';
    } else {
      return '(model[\'' + op1['s#'] + '\'] != ' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + ')';
    }
  }
}

operators['Between'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    return '((moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSameOrAfter(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\'))) &&' +
      '(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSameOrBefore(moment.utc(new Date(\'' + (_.isObject(op3) ? 'model[\'' + op3['s#'] + '-' + op3['parentId'] + '\']' : operators['format'](format, op3)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\'))))';
  } else {
    return '((moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSameOrAfter(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\'))) && ' +
      '(moment.utc(new Date(model[\'' + op1['s#'] + '\'])).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\').isSameOrBefore(moment.utc(new Date(\'' + (_.isObject(op3) ? 'model[\'' + op3['s#'] + '\']' : operators['format'](format, op3)) + '\')).set({hour:0,minute:0,second:0,millisecond:0}).tz(\'UTC\'))))';
  }
}

operators['Not Between'] = function (op1, op2, op3, format) {
  if (op1.parentId) {
    return '!((moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']).setHours(0,0,0,0)).isSameOrAfter(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '-' + op2['parentId'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0)))) &&' +
      '(moment.utc(new Date(model[\'' + op1['s#'] + '-' + op1['parentId'] + '\']).setHours(0,0,0,0)).isSameOrBefore(moment.utc(new Date(\'' + (_.isObject(op3) ? 'model[\'' + op3['s#'] + '-' + op3['parentId'] + '\']' : operators['format'](format, op3)) + '\').setHours(0,0,0,0)))))';
  } else {
    return '!((moment.utc(new Date(model[\'' + op1['s#'] + '\']).setHours(0,0,0,0)).isSameOrAfter(moment.utc(new Date(\'' + (_.isObject(op2) ? 'model[\'' + op2['s#'] + '\']' : operators['format'](format, op2)) + '\').setHours(0,0,0,0)))) && ' +
      '(moment.utc(new Date(model[\'' + op1['s#'] + '\']).setHours(0,0,0,0)).isSameOrBefore(moment.utc(new Date(\'' + (_.isObject(op3) ? 'model[\'' + op3['s#'] + '\']' : operators['format'](format, op3)) + '\').setHours(0,0,0,0)))))';
  }
}

// PQT-2485 by Yamuna
function thresoldNotify(entity, tempFileName) {
  var deffer = q.defer();
  if (!entity) {
    deffer.reject(entity);
    return;
  }
  setTimeout(function () {
    
    //ChangeOnline
    log.log("sending notification to - "+entity.email);
    nofity(entity, tempFileName, function (e, d) {
      log.log('sent');
      working--;
      if (e) {
        return;
      }
      var calendarDate = new Date(d.date).setHours(0,0,0,0);
      d.notified = true;

      //ChangeOnline
      if(d.scheduledBy){
        if(d.scheduledBy._id){
          d.scheduledBy = d.scheduledBy._id.toString();
        } else {
          d.scheduledBy = d.scheduledBy
        }
      }
      db.calendarThresholds.save(d);

      //OnlineChange
      log.log('-0-0-0-0-0-0-0-0-0 -   No matching mail sent    -0-0-0-0-0-0-0-0-0 -')
      log.log("mail sent for - "+d.title+"")      
      log.log("mail sent for - "+JSON.stringify(d)+"")
      log.log('-0-0-0-0-0-0-0-0-0 -   No matching mail sent    -0-0-0-0-0-0-0-0-0 -')

      deffer.resolve('success')
    });
  }, (Math.floor(Math.random() * (25000 - 2000 + 1)) + 2000));

  return deffer.promise;
}


function notify(mailTemplateName, entity, cb){
  mailer.send(mailTemplateName, entity, config.notifyLogUrl,function sendMailCallback(e,b) {
      if(e){
          console.log('Mail not send',e);
          cb(e, entity)
      }else{
          console.log('Mail send');
          cb(null, entity)
      }
    });

}

function thresoldMailNotification(entries){
  var entriesArray = [];
  var entryObj = {};
  var schemaTitles;
  entriesArray = entries.map(function(entry){
     schemaTitles = '';
    _.forEach(entry.schema, function (schema, index) {
      schemaTitles += index == 0 ? schema.title : ', ' + schema.title ;
    });
    if(entry){
      entryObj = {
        id: entry._id,
        title: entry.title,
        module: entry.module.title,
        schemaTitle: schemaTitles,
        thresholdType: entry.thresholdType,
        type: entry.type,
        date: moment(moment.utc(entry.date).toDate()).format('MM/DD/YYYY HH:mm:ss'),
        email: entry.email
      } 
      return entryObj;
    }else{
      return {}
    }
  })
  return entriesArray;
  }

debug('********************* | Thats All :)  | *********************');

