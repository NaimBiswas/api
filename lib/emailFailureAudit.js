/**
* @name Email Failure Audit (PQT-1962)
* @author Yamuna Sardhara <yamuna.s@productivet.com>
*
* @version 1.0
*/

var colors = require('colors');
var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('./logger').configure(__dirname, enableBunyan);
var CronJob = require('cron').CronJob;
var mailer = require('./mailer');
var log = require('../logger');
var config = require('../config/config.js');
var moment = require('moment-timezone');
var _ = require('lodash');
var db = require('./db');

var machineDate = new Date();
var machineoffset = machineDate.getTimezoneOffset();
var clientOffset = config.timeZones[config.currentTimeZone] || -330;
mcdiff = 0;
mcdiff = -machineoffset + parseInt(clientOffset);
var today = new Date();
var start = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
start.setDate(start.getDate());
start = new Date(start.getTime() + (mcdiff * 60 * 1000));
var end = new Date(start.getTime() + (24 * 60 * 60 * 1000));


var job = new CronJob('0 */2 * * *', function() {
  init();
}, function () {
  /* This function is executed when the job stops */
},
true  /* , Start the job right now */
/* timeZone /* OPTIONAL Time zone of this job. */
);

function init(){
    db
    .collection('mailLogs')
    .find({
      'status': 'Failed',
      'timestemp': {
        $gte: new Date(start.toISOString()),
        $lte: new Date(end.toISOString())
      },
    })
    .toArray(function (e, entries) {
        if(!entries || !entries.length){
            log.log("All emails are sent sucessfully !!")
        }else{
            log.log("Oops, There are some emails which are not sent !!");
            console.log(entries);

            var formattedForMail = {
                'entriesObj': getFormattedEntryObject(entries),
                'date': moment(moment.utc(new Date()).toDate()).format('MM/DD/YYYY HH:mm:ss')
            }
            notify(formattedForMail, function (e, d) {
                if(e){
                  log.log('emailFailureAudit mail not sent..!!');
                }else{
                  log.log('emailFailureAudit mail sent sucessfully..!!');
                }
              });
        }
    });
}

function getFormattedEntryObject(entries){
    var entriesArray = [];
    var attachments = '';
    entriesArray = entries.map(function(entry){

      if(entry && entry.attachments && entry.attachments.length){
        attachments = entry.attachments[0].path;
        if(attachments){
            entry.attachments = attachments.substr(attachments.lastIndexOf('/') + 1);
            // if file attachments is only file name then comment the above line and uncomment below line of code.
            // entry.attachments = attachments;
        }
        return entry;
      }else{
        delete entry.attachments;
        return entry;
      }
    });
    console.log(entriesArray)
    return entriesArray;
}

function notify(entity, cb){
    mailer.send('emailFailureAudit', entity, config.notifyLogUrl,function sendMailCallback(e,b) {
        if(e){
            console.log('Mail not send',e);
            cb(e, entity)
        }else{
            console.log('Mail send');
            cb(null, entity)
        }
      }, true);
  
  }