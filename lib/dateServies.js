var moment = require('moment-timezone');
var config = require('../config/config.js');

function fromZone(zone) {
    zone = config.timeZonesP[zone];

    return {
        date: date,
        today: today,
        today_FloorHour: today_FloorHour,
        getDateOnly: getDateOnly
    }

    function date(year, month, date, hour, minute, second) {
        return moment.tz({
            years: year,
            months: month,
            date: date,
            hours: hour,
            minutes: minute,
            seconds: second
        }, zone);
    }

    function today_FloorHour() {
        var cDate = new Date();
        return date(cDate.getFullYear(),
            cDate.getMonth(),
            cDate.getDate(),
            cDate.getHours(),
            0, 0)
    }

    function today() {
        var cDate = new Date();
        return date(cDate.getFullYear(), cDate.getMonth(), cDate.getDate(), 0, 0, 0);
    }

    function getDateOnly(dDate) {
        return moment(dDate).startOf('day');
    }



}

module.exports = {
    fromZone: fromZone
};