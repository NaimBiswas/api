'use strict'
var bird = require('bluebird');
bird.promisifyAll(require('mongoskin'));
bird.promisifyAll(require('./registration'));
bird.promisifyAll(require('./mailer'));
bird.promisifyAll(require('./query'));
