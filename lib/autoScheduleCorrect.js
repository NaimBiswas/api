var colors = require('colors');
var enableBunyan = process.env.enableBunyan === 'true';
var session_logger = require('./logger').configure(__dirname, enableBunyan);
var moment = require('moment');
var CronJob = require('cron').CronJob;
var mailer = require('./mailer');
var settings = require('./settings');
var log = require('../logger');
var config = require('../config/config.js');
var db = require('./db');
var _ = require('lodash');

// new CronJob('0 */6 * * *', function() {
//     init(); 
// })

var job = new CronJob('0 */3 * * *', function() {
    init();
  }, function () {
    log.log('Job Done..!!');
    /* This function is executed when the job stops */
  },
  true  /* , Start the job right now */
  /* timeZone /* OPTIONAL Time zone of this job. */
  );
//init();
function init(){
    console.log('Start Date Init',new Date());
    var isCompleted=true;
    var schemaPendingArray=[];
    var schemaArray=[];
    var calVerifiedUser={};
    var calworkflowReview={};
    var CorrectedData={}; 
    var finalArray = [];
    var schemaSkipArrayLength=0;
    var notCheckCalender = ["5a14a7329dd6791158b8b0ff","5a14a76d9dd6791158b8b224","5a14a76d9dd6791158b8b22f"];
    
    db.collection('calendar').find({"ismasterQcForm":true,"entry":{"$exists":1}},{"modifiedByName":1,"entry":1,"master":1,"skippedMasterApps":1,"title":1,"date":1}).toArray(function(err,cal){
        function outerSync(){
            if(i < cal.length){
                if(notCheckCalender.indexOf(cal[i]._id.toString()) == -1){
                    console.log('i----------------',i,cal[i]._id.toString())
                    CorrectedData.calendarId=cal[i]._id.toString();
                    CorrectedData.calendarTitle=cal[i].title; 
                    CorrectedData.scheduleDate=cal[i].date;
                    if(cal[i].entry && cal[i].entry.masterschemaIds && cal[i].entry.masterschemaIds.length>0){
                        isCompleted=true;
                        schemaSkipArrayLength=(cal[i].skippedMasterApps && typeof cal[i].skippedMasterApps == 'object') ? Object.keys(cal[i].skippedMasterApps).length : 0;
                        db.collection('masterqcsettings').find(cal[i].master._id,{"schema":1,"title":1}).toArray(function(err,schemas){
                            
                            function tempInnerSync(){                      
                                if(y < schemas.length){
                                    CorrectedData.masterqcsettingsId=schemas[y]._id.toString();
                                    CorrectedData.mastername=schemas[y].title;
                                    var x = 0;                        
                                    innerSync(schemas[y].schema,cal[i],function(callBackRes){
                                        if(callBackRes.flag && callBackRes.seqId){
                                            CorrectedData.seqId=callBackRes.seqId
                                            z = 0;
                                            innerSyncSecond(schemas,cal[i],callBackRes.seqId,function(dataInnerSyncSecond){
                                                y++;
                                                tempInnerSync();
                                            });
                                        }else{
                                            isCompleted = false;
                                            y++;
                                            tempInnerSync();
                                        }
                                    });
                                }else{
                                    if(isCompleted==true && cal[i].entry.schedulestatus!='Completed'){
                                        
                                        
                                    }
                                    CorrectedData={}; 
                                    CorrectedData.startDate=new Date();
                                    i++;
                                    outerSync();
                                }
                            }
                            var y = 0;
                            tempInnerSync();
                            
                        })
                    }else{
                        CorrectedData={}; 
                        CorrectedData.startDate=new Date();
                        i++;
                        outerSync();
                    }  
                }else{
                    CorrectedData={}; 
                    CorrectedData.startDate=new Date();
                    i++;
                    outerSync();
                }
            }else{
                nofity(finalArray);
                isCompleted=true;
                schemaPendingArray = [];
                schemaArray = [];
                calVerifiedUser= calworkflowReview = CorrectedData = {};
                i = x = y = z = h = 0;
            }
        }
    
        var i = 0;
        var x = 0;
        var y = 0;
        var z = 0;
        var h = 0;
        CorrectedData={};
        CorrectedData.startDate=new Date(); 
        outerSync();
    
        function innerSync(schema,cal,cb){
            if(x < schema.length){
                var seqId;
                var firstEntry;
                var calentryID = typeof cal.entry.entryId == 'string' ? db.ObjectID(cal.entry.entryId) : cal.entry.entryId; 
                db.collection(schema[x]._id).findOne(calentryID, function (err, data){
                    if(err){
                        x++;                 
                        innerSync(schema,cal,cb);
                    }else{
                        firstEntry = data;
                        if(firstEntry){
                            if(firstEntry.calendarIds && firstEntry.entries.status.status!=4){
                                seqId= firstEntry.masterQCSettings.sequenceId;
                                x = 0;
                                cb({flag : true,seqId : seqId})
                            }else{
                                x++;
                                innerSync(schema,cal,cb);
                            }
                        }else{
                            x++;
                            innerSync(schema,cal,cb); 
                        }
                        
                    }
                });
            }else{
                //console.log('T--------------1---false')
                x = 0;
                cb({flag : false,seqId : null});
            }
        }
    
        function innerSyncSecond(schemas,cal,seqId,cb){
            
            if(z < schemas.length){
                isCompleted=true;
                schemaPendingArray=[];
                schemaArray=[];//cal.entry.masterschemaIds ? cal.entry.masterschemaIds : [];
                calVerifiedUser= cal.entry.calendarverifieduser ? cal.entry.calendarverifieduser : {};
                calworkflowReview=cal.entry.workflowreview ? cal.entry.workflowreview : {};
                CorrectedData.apps=[];
                CorrectedData.updatedentries=[];
                h = 0;
                //voidSchemas = cal.entry.voidSchemas ? cal.entry.masterschemaIds : [];
                outerSyncSecond(schemas[z].schema,cal,seqId,function(dataOuterSyncSecond){
                    CorrectedData.updatedCalendar={};
                    //console.log('schemaPendingArray---',schemaPendingArray)
                    if(schemaPendingArray.length>0){
                         if((schemaArray.length + schemaSkipArrayLength) == schemas[z].schema.length){
                            schemaArray = _.uniq(cal.entry.masterschemaIds.concat(schemaArray));
                             console.log('--->1',cal._id,'---',{"$set":{"entry.masterschemaIds":schemaArray,"entry.calendarverifieduser":calVerifiedUser,"entry.workflowreview":calworkflowReview,"entry.schedulestatus":'Completed',"entry.voidSchemas" : cal.entry.voidSchemas,"entry.schemaCompletedDate":cal.entry.schemaCompletedDate,"entry.completedDate":new Date(),"entry.isUpdatedByScript":true}})
                             db.collection('calendar').update({"_id":cal._id},{"$set":{"entry.masterschemaIds":schemaArray,
                            "entry.calendarverifieduser":calVerifiedUser,"entry.workflowreview":calworkflowReview,
                            "entry.schedulestatus":'Completed',"entry.voidSchemas" : cal.entry.voidSchemas,"entry.schemaCompletedDate":cal.entry.schemaCompletedDate,"entry.completedDate":new Date(),"entry.isUpdatedByScript":true}});
                            CorrectedData.updatedCalendar={"calendarid":cal._id.toString(),"entry_masterschemaIds":schemaArray,
                            "entry_calendarverifieduser":calVerifiedUser,"entry_workflowreview":calworkflowReview,
                            "entry_schedulestatus":'Completed',"entry_voidSchemas" : cal.entry.voidSchemas,"entry_schemaCompletedDate":cal.entry.schemaCompletedDate,"entry_completedDate":new Date(),"entry_isUpdatedByScript":true};
                        }
                        
                        else{
                            schemaArray = _.uniq(cal.entry.masterschemaIds.concat(schemaArray));
                            console.log('---->2',cal._id,'---',{"$set":{"entry.masterschemaIds":schemaArray,"entry.calendarverifieduser":calVerifiedUser,"entry.workflowreview":calworkflowReview,"entry.schedulestatus":'Incomplete',"entry.voidSchemas" : cal.entry.voidSchemas,"entry.schemaCompletedDate":cal.entry.schemaCompletedDate,"entry.isUpdatedByScript":true}})
                            db.collection('calendar').update({"_id":cal._id},{"$set":{"entry.masterschemaIds":schemaArray,
                            "entry.calendarverifieduser":calVerifiedUser,"entry.workflowreview":calworkflowReview,
                            "entry.schedulestatus":'Incomplete',"entry.voidSchemas" : cal.entry.voidSchemas,"entry.schemaCompletedDate":cal.entry.schemaCompletedDate,"entry.isUpdatedByScript":true}});
                            CorrectedData.updatedCalendar={"calendarid":cal._id.toString(),"entry_masterschemaIds":schemaArray,
                            "entry_calendarverifieduser":calVerifiedUser,"entry_workflowreview":calworkflowReview,
                            "entry_schedulestatus":'Incomplete',"entry_voidSchemas" : cal.entry.voidSchemas,"entry_schemaCompletedDate":cal.entry.schemaCompletedDate,"entry_isUpdatedByScript":true};
                        } 
                        CorrectedData.endDate=new Date();
                        db.collection('autoScheduleCorrectData').insert(CorrectedData); 
                        CorrectedData.updatedentries = JSON.stringify(CorrectedData.updatedentries);
                        CorrectedData.schemaPendingArray = JSON.stringify(CorrectedData.schemaPendingArray);
                        delete CorrectedData.apps;
                        delete CorrectedData.schemaArray;
                        delete CorrectedData.updatedCalendar;
                        finalArray.push(CorrectedData)
                    }
                    z++;
                    innerSyncSecond(schemas,cal,seqId,cb);
                });
            }else{
                z = 0;
                cb(true);
            }
        }
    
        function outerSyncSecond(schema,cal,seqId,cb){
           //
            if(h < schema.length){
//                console.log('-->schema[h]._id',schema[h]._id,seqId)
                db.collection(schema[h]._id).findOne({"masterQCSettings.sequenceId":seqId}, function (err, data){
                    //console.log('-------------->data',data,seqId,schema[h])
                    if(err){
                        h++;
                        outerSyncSecond(schema,cal,seqId,cb);
                    }else{
                        var entry = data;
                        CorrectedData.apps.push({"_id":schema[h]._id,"title":schema[h].title});
                        /* console.log('--entry.entries.status.status','--',schema[h],'--')
                        console.log('isCompleted--',isCompleted)
                        if(schema[h]._id=='5a68a4805d1cd424eaa1ab94'){
                            console.log('entry',entry,seqId)
                        } */
                        if(entry && entry.entries.status.status!=4 && isCompleted==true){
                            if(entry.calendarIds.length == 0){
                                var tempEntry={};
                                tempEntry.calendarIds=[];
                                tempEntry.calendarverifieduser=[];
                                tempEntry.workflowreview=[];
                                tempEntry.calendarIds.push(cal._id.toString());
                                tempEntry.calendarverifieduser.push({});
                                var createdByName=entry.createdByInfo.firstname +' '+entry.createdByInfo.lastname+'('+entry.createdByInfo.username+')';
                                tempEntry.calendarverifieduser[0][cal._id.valueOf()]=entry.modifiedByName ? entry.modifiedByName : createdByName;
                                console.log("tempEntry.calendarverifieduser[0][cal._id.valueOf()]",entry.modifiedByName,tempEntry.calendarverifieduser[0][cal._id.valueOf()],createdByName);
                                //console.log('tempEntry.calendarverifieduser--->',schema[h]._id,seqId,cal.entry.masterschemaIds)
                               // tempEntry.calendarverifieduser.push(entry.createdByInfo.firstname +' '+entry.createdByInfo.lastname+'('+entry.createdByInfo.username+')');
                                tempEntry.workflowreview.push(entry.workflowreview);
                                entry.calendarIds=tempEntry.calendarIds;
                                entry.calendarverifieduser=tempEntry.calendarverifieduser;
                                entry.workflowreview=tempEntry.workflowreview;
                                CorrectedData.updatedentries.push({"entryId":entry._id,"title":schema[h].title,"sequenceId":seqId,"calendarIds":entry.calendarIds,"calendarverifieduser":entry.calendarverifieduser})

                                 db.collection(schema[h]._id).update({"masterQCSettings.sequenceId":seqId},
                                 {"$set":{"calendarIds":entry.calendarIds,
                                    "calendarverifieduser":entry.calendarverifieduser
                                   }});
                                //CorrectedData.updatedentries
                            }
                            
                            cal.entry.schemaCompletedDate = cal.entry.schemaCompletedDate ? cal.entry.schemaCompletedDate : {}; 
                            cal.entry.schemaCompletedDate[schema[h]._id] = entry.modifiedDate;
                            schemaArray.push(schema[h]._id);
                            calVerifiedUser[schema[h]._id]=entry.calendarverifieduser;
                            //calVerifiedUser.push(entry.calendarverifieduser);
                            calworkflowReview[schema[h]._id]={"workflowreview" : entry.workflowreview};
                            if(cal.entry.masterschemaIds.indexOf(schema[h]._id) <0 ){
                                schemaPendingArray.push(schema[h]._id);
                            }
                            CorrectedData.schemaArray=schemaArray;
                            CorrectedData.schemaPendingArray=schemaPendingArray;
                            h++;
                            outerSyncSecond(schema,cal,seqId,cb);
                        }else{
                            /* if(entry && entry.entries && entry.entries.status.status == 3){
                                console.log('T--------------2---false')
                                cal.entry.voidSchemas.push(schema[h]._id)
                            } */
                            if(cal.skippedMasterApps&& !cal.skippedMasterApps[schema[h]._id]){
                                isCompleted = false;
                            }
                            h++;
                            outerSyncSecond(schema,cal,seqId,cb); 
                        }
                    }
                });
            }else{
                cb(true);
            }
        }
    
    });
}


function nofity(finalArray){
    console.log('End Date',new Date());
    console.log('final::',finalArray)


    if(!finalArray.length){
        finalArray = [{NoRecord : 'No record found'}];
    }
    mailer.send('autoScheduleCorrect', finalArray, config.staticURL,function sendMailCallback(e,b) {
        finalArray = [];
        if(e){
            console.log('Mail not send',e);
        }else{
            console.log('Mail send');
        }
    });

}
